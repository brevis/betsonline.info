<?php

defined('_JEXEC') or die('Restricted access');

include_once (JPATH_ROOT . '/components/com_billing/useraccount.php');
include_once (JPATH_ROOT . '/administrator/components/com_billing/admin.billing.min.php');

if(BILLING_EXT)
{
	if(file_exists(JPATH_ROOT . '/administrator/components/com_billing/userpoints.php'))
	{
		include_once (JPATH_ROOT . '/components/com_billing/userpoints.php');
	}
	if(file_exists(JPATH_ROOT . '/administrator/components/com_billing/admin.billing.ext.php'))
	{
		include_once (JPATH_ROOT . '/administrator/components/com_billing/admin.billing.ext.php');
	}
}
include_once (JPATH_ROOT . '/components/com_billing/account.php');
include_once (JPATH_ROOT . '/components/com_billing/plugin.php');
include_once (JPATH_ROOT . '/components/com_billing/billing.func.php');
include_once ('addons.inc');

$document = JFactory::getDocument();
$document->addStyleSheet('/components/com_billing/tooltip_style.css');
$document->addStyleSheet('/components/com_billing/custom.css');
$document->addStyleSheet('/components/com_billing/tabs.css');
$document->addScript('/components/com_billing/tooltip.js');

jimport('joomla.application.component.modellist');
jimport('joomla.html.pagination');
jimport('joomla.utilities.xmlelement');
jimport( 'joomla.factory' );
jimport( 'joomla.filesystem.archive' );

function GetK2CategoriesCB($parent = 0, $n = 0)
{
	$out = '';
	$item_value = explode(',', GetOption('catsk2'));
	$db = JFactory::getDBO();
	$query = "select * from `#__k2_categories` where parent = $parent and published = 1 and trash = 0 order by `name`";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList();
	if(count($rows) > 0)
	{
		$level = '|';
		for($i = 0; $i < $n; $i++)
		{
			$level .= '- ';
		}
		foreach($rows as $row)
		{
			$s = '';
			for($i = 0; $i < count($item_value); $i++)
			{
				if($item_value[$i] == $row->id) {$s = 'checked';} 
			}
			if($parent > 0)
			{
				$out .= "<input type='checkbox' value='$row->id' name=catsk2[] $s> $level $row->name ($row->language)<br>";
			}
			else
			{
				$out .= "<input type='checkbox' value='$row->id' name=catsk2[] $s> $row->name ($row->language)<br>";
			}
			$query2 = "select * from `#__k2_categories` where parent = $row->id order by `name`";
			$result = $db->setQuery($query2);   
			$rows2 = $db->loadObjectList();
			if(count($rows) > 0)
			{
				$n ++;
				$out .= GetK2CategoriesCB($row->id, $n);
			}
		}
		return $out;
	}
	else
	{
		return '';
	}
}

function ShowOptions()
{
	$db = JFactory::getDBO();
	$UA = new UserAccount();

	echo '<h1>'.JText::_('COM_BILLING_OPTIONS').'</h1>';

	JToolBarHelper::custom( 'saveoptions', 'save', 'save', JText::_('COM_BILLING_SAVE'), false );
	
	$partner = GetOption('partner');
	$partnerpercent = GetOption('partnerpercent');
	$partnerpayment = GetOption('partnerpayment');
	$subscr = GetOption('subscr');
	$files = GetOption('files');
	$music = GetOption('music');
	$articles = GetOption('articles');
	$partner_tab = GetOption('partner_tab');
	$service = GetOption('service');
	$service1 = GetOption('service1');
	$service2 = GetOption('service2');
	$usecoupon = GetOption('usecoupon');
	$couponprice = GetOption('couponprice');
	$useaccounts = GetOption('useaccounts');
	$notify = GetOption('notify');
	$accountstab = GetOption('accountstab');
	$usegroups = GetOption('usegroups');
	$logenabled = GetOption('logenabled');
	$pointsenabled = GetOption('pointsenabled');
	$userpointstab = GetOption('userpointstab');
	$redirect = GetOption('redirect');
	$baltab = GetOption('baltab');
	$addtab = GetOption('addtab');
	$projectstab = GetOption('projectstab');
	$exchange = GetOption('exchange');
	$vkontakte = GetOption('vkontakte');
	$facebook = GetOption('facebook');
	$withdrawal = GetOption('withdrawal');
	$showip = GetOption('showip');
	$artcounter = GetOption('artcounter');
	$showintro = GetOption('showintro');
	$slider = GetOption('slider');
	$movemoney = GetOption('movemoney');
	$author = GetOption('author');
	$authorpercent = GetOption('authorpercent');
	$copyright = GetOption('copyright');
	$maketariff = GetOption('maketariff');
	$changetariff = GetOption('changetariff');
	$hideservices = GetOption('hideservices');
	$publisher = GetOption('publisher');
	$invite = GetOption('invite');
	$invite_text = GetOption('invite_text', true);	
	$invite_subj = GetOption('invite_subj');	
	$sendsubnotif = GetOption('sendsubnotif');
	$paysum = GetOption('paysum');
	$paysumreadonly = GetOption('paysumreadonly');
	$smsadmin = GetOption('smsadmin');
	$adminphone = GetOption('adminphone');
	$smsuser = GetOption('smsuser');
	$smsalladmin = GetOption('smsalladmin');
	$smsprice = GetOption('smsprice');
	$smspayd = GetOption('smspayd');
	$comission = GetOption('comission');
	$mv_comission = GetOption('mv_comission');
	$admin_percent = GetOption('admin_percent');
	$hidetariff = GetOption('hidetariff');
	$moderation = GetOption('moderation');
	$hidesubs = GetOption('hidesubs');
	$hidefroze = GetOption('hidefroze');
	$easyurl = GetOption('easyurl');
	$minsum = GetOption('minsum');
	$maxsum = GetOption('maxsum');
	$cashouttab = GetOption('cashouttab');
	$title = GetOption('title');
	$info = GetOption('info');
	$vote = GetOption('vote');
	$voteprice = GetOption('voteprice');
	$partpoint = GetOption('partpoint');
	
	$p_percent = GetOption('p_percent');
	$p_depth = GetOption('p_depth');
	
	if($invite_text == '')
	{
		$invite_text = '%partner_url%<br>%username%';
	}
	
	if ($partner == '') {$partner = false;} if ($partner == true){$checked = 'checked';} else {$checked = '';}
	
	if ($partnerpercent == '')
	{
		$partnerpercent = 5;
	}
	if ($authorpercent == '')
	{
		$authorpercent = 90;
	}
	
	//jimport('joomla.html.pane');
	$tabs = '';
	//$pane = JPane::getInstance('tabs', array('startOffset'=>0)); 
	$tabs .= JHtml::_('tabs.start', 'tabs', array('useCookie'=> 0, 'startOffset'=>0));
	//$tabs .= $pane->startPane( 'pane' );

	//$tabs .= $pane->startPanel( JText::_('COM_BILLING_GENERAL'), 'general' );
	$tabs .= JHtml::_('tabs.panel', JText::_('COM_BILLING_GENERAL'), 'general');
	if(BILLING_EXT)
	{
		/*if ($useaccounts == '') {$useaccounts = false;} if ($useaccounts == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='useaccounts' name='useaccounts' value='$useaccounts' type='checkbox' $checked onclick=\"document.getElementById('useaccounts').value = document.getElementById('useaccounts').checked;\"> &nbsp;".JText::_('COM_BILLING_ENABLE_MULTIACCOUNTS'). ' '. Tooltip(JText::_('COM_BILLING_HELP_MULTIACCOUNT')) . "<br><br>";	
		*/
	}
	if ($notify == '') {$notify = false;} if ($notify == true){$checked = 'checked';} else {$checked = '';}
	$tabs .= "<input class='inputbox' id='notify' name='notify' value='$notify' type='checkbox' $checked onclick=\"document.getElementById('notify').value = document.getElementById('notify').checked;\"> &nbsp;".JText::_('COM_BILLING_SEND_NOTIFY_TO_ADMIN')."<br><br>";	
	if ($usegroups == '') {$usegroups = false;} if ($usegroups == true){$checked = 'checked';} else {$checked = '';}
	$tabs .= "<input class='inputbox' id='usegroups' name='usegroups' value='$usegroups' type='checkbox' $checked onclick=\"document.getElementById('usegroups').value = document.getElementById('usegroups').checked;\"> &nbsp;".JText::_('COM_BILLING_ENABLE_GROUPS')."<br><br>";	
	if ($logenabled == '') {$logenabled = false;} if ($logenabled == true){$checked = 'checked';} else {$checked = '';}
	$tabs .= "<input class='inputbox'id='logenabled' name='logenabled' value='$logenabled' type='checkbox' $checked onclick=\"document.getElementById('logenabled').value = document.getElementById('logenabled').checked;\"> &nbsp;".JText::_('COM_BILLING_LOGGING_ON')."<br><br>";	
	if(BILLING_EXT)
	{	
		if ($pointsenabled == '') {$pointsenabled = false;} if ($pointsenabled == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input class='inputbox' id='pointsenabled' name='pointsenabled' value='$pointsenabled' type='checkbox' $checked onclick=\"document.getElementById('pointsenabled').value = document.getElementById('pointsenabled').checked;\"> &nbsp;".JText::_('COM_BILLING_POINTS_ON')."<br><br>";	
		if ($exchange == '') {$exchange = false;} if ($exchange == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input class='inputbox' id='exchange' name='exchange' value='$exchange' type='checkbox' $checked onclick=\"document.getElementById('exchange').value = document.getElementById('exchange').checked;\"> &nbsp;".JText::_('COM_BILLING_POINTS_EXCHANGE')."<br><br>";	
	}
	if ($withdrawal == '') {$withdrawal = false;} if ($withdrawal == true){$checked = 'checked';} else {$checked = '';}
	$tabs .= "<input class='inputbox' id='withdrawal' name='withdrawal' value='$withdrawal' type='checkbox' $checked onclick=\"document.getElementById('withdrawal').value = document.getElementById('withdrawal').checked;\"> &nbsp;".JText::_('COM_BILLING_MASSPAY2')."<br><br>";	
	if ($showip == '') {$showip = false;} if ($showip == true){$checked = 'checked';} else {$checked = '';}
	$tabs .= "<input class='inputbox' id='showip' name='showip' value='$showip' type='checkbox' $checked onclick=\"document.getElementById('showip').value = document.getElementById('showip').checked;\"> &nbsp;".JText::_('COM_BILLING_SHOWIP')."<br><br>";	
	if(BILLING_EXT)
	{
		if ($artcounter == '') {$artcounter = false;} if ($artcounter == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input class='inputbox' id='artcounter' name='artcounter' value='$artcounter' type='checkbox' $checked onclick=\"document.getElementById('artcounter').value = document.getElementById('artcounter').checked;\"> &nbsp;".JText::_('COM_BILLING_SHOWCOUNTER')."<br><br>";	
		if ($showintro == '') {$showintro = false;} if ($showintro == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input class='inputbox' id='showintro' name='showintro' value='$showintro' type='checkbox' $checked onclick=\"document.getElementById('showintro').value = document.getElementById('showintro').checked;\"> &nbsp;".JText::_('COM_BILLING_SHOWINTRO')."<br><br>";	
	}
//	if ($slider == '') {$slider = false;} if ($slider == true){$checked = 'checked';} else {$checked = '';}
//	$tabs .= "<input class='inputbox' id='slider' name='slider' value='$slider' type='checkbox' $checked onclick=\"document.getElementById('slider').value = document.getElementById('slider').checked;\"> &nbsp;".JText::_('COM_BILLING_SLIDER')."<br><br>";	
	if ($movemoney == '') {$movemoney = false;} if ($movemoney == true){$checked = 'checked';} else {$checked = '';}
	$tabs .= "<input class='inputbox' id='movemoney' name='movemoney' value='$movemoney' type='checkbox' $checked onclick=\"document.getElementById('movemoney').value = document.getElementById('movemoney').checked;\"> &nbsp;".JText::_('COM_BILLING_MOVEMONEY')."<br><br>";	
	$tabs .= "<input class='inputbox' id='comission' name='comission' value='$comission' type='text' size='5'>&nbsp; % &nbsp; ".JText::_('COM_BILLING_MOVEMONEY_COMISSION')."<br><br>";	
	if(BILLING_EXT)
	{
		if ($publisher == '') {$publisher = false;} if ($publisher == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input class='inputbox' id='publisher' name='publisher' value='$publisher' type='checkbox' $checked onclick=\"document.getElementById('publisher').value = document.getElementById('publisher').checked;\"> &nbsp;".JText::_('COM_BILLING_PUBLISHER')."<br><br>";		
		
		if ($sendsubnotif == '') {$sendsubnotif = false;} if ($sendsubnotif == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='sendsubnotif' name='sendsubnotif' value='$sendsubnotif' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;".JText::_('COM_BILLING_SUB_NOTIF')."<br>";
		
	}
	//$tabs .= $pane->endPanel();
	
	
	//$tabs .= $pane->startPanel(  JText::_('COM_BILLING_PAYMENT'), 'payment' );
	$tabs .= JHtml::_('tabs.panel', JText::_('COM_BILLING_PAYMENT'), 'payment');
	$tabs .= "<table>";
	$tabs .= "<tr><td>".JText::_('COM_BILLING_AFTERPAY').":</td><td> <input class='inputbox' name='afterpay' value='".GetOption('afterpay')."' type='text' size='100'></td></tr>";
	$tabs .= "<tr><td>".JText::_('COM_BILLING_PAYMENT_SUM').":</td><td> <input class='inputbox' name='paysum' value='".GetOption('paysum')."' type='text' size='5'> ".GetDefaultCurrencyAbbr()."</td></tr>";
	if ($paysumreadonly == '') {$paysumreadonly = false;} if ($paysumreadonly == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<tr><td><input id='paysumreadonly' name='paysumreadonly' value='$paysumreadonly' type='checkbox' $checked onclick=\"this.value = this.checked;\"> </td><td>".JText::_('COM_BILLING_READONLY_PAY')." </td></tr>";
	if(BILLING_EXT)
	{
		if ($author == '') {$author = false;} if ($author == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<tr><td><input id='author' name='author' value='$author' type='checkbox' $checked onclick=\"document.getElementById('author').value = document.getElementById('author').checked;\"> </td><td>".JText::_('COM_BILLING_AUTHOR_PAYOUT')."</td></tr>";	

		if ($moderation == '') {$moderation = false;} if ($moderation == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<tr><td><input id='moderation' name='moderation' value='$moderation' type='checkbox' $checked onclick=\"document.getElementById('moderation').value = document.getElementById('moderation').checked;\"> </td><td>".JText::_('COM_BILLING_MODERATE')."</td></tr>";	

		if ($hidesubs == '') {$hidesubs = false;} if ($hidesubs == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<tr><td><input id='hidesubs' name='hidesubs' value='$hidesubs' type='checkbox' $checked onclick=\"this.value = this.checked;\"> </td><td>".JText::_('COM_BILLING_HIDESUBS')."</td></tr>";	

		if ($hidefroze == '') {$hidefroze = false;} if ($hidefroze == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<tr><td><input id='hidefroze' name='hidefroze' value='$hidefroze' type='checkbox' $checked onclick=\"this.value = this.checked;\"> </td><td>".JText::_('COM_BILLING_HIDEFROZE')."</td></tr>";	

		$tabs .= "<tr><td>".JText::_('COM_BILLING_AUTHOR_PERCENT')."</td> <td><input class='inputbox' type='text' name='authorpercent' value='$authorpercent' size='5'> %</td></tr>";
		
		$tabs .= "<tr><td>".JText::_('COM_BILLING_MINSUM')."</td> <td><input class='inputbox' type='text' name='minsum' value='$minsum' size='5'> ".GetDefaultCurrencyAbbr()."</td></tr>";
		
		$tabs .= "<tr><td>".JText::_('COM_BILLING_MAXSUM')."</td> <td><input class='inputbox' type='text' name='maxsum' value='$maxsum' size='5'> ".GetDefaultCurrencyAbbr()."</td></tr>";
	
		if ($cashouttab == '') {$cashouttab = false;} if ($cashouttab == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<tr><td><input id='cashouttab' name='cashouttab' value='$cashouttab' type='checkbox' $checked onclick='this.value = this.checked;'> </td><td>".JText::_('COM_BILLING_USECASHOUTTAB')."</td></tr>";	

		if ($vote == '') {$vote = false;} if ($vote == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<tr><td><input id='vote' name='vote' value='$vote' type='checkbox' $checked onclick='this.value = this.checked;'> </td><td>".JText::_('COM_BILLING_ENABLEVOTE')."</td></tr>";	

		$tabs .= "<tr><td>".JText::_('COM_BILLING_VOTE_PRICE')."</td> <td><input class='inputbox' type='text' name='voteprice' value='$voteprice' size='5'> ".GetDefaultCurrencyAbbr()."</td></tr>";
	}
	$tabs .= "</table>";
	//$tabs .= $pane->endPanel();

	//$tabs .= $pane->startPanel(  JText::_('COM_BILLING_FORMATS'), 'format' );
	$tabs .= JHtml::_('tabs.panel', JText::_('COM_BILLING_FORMATS'), 'format');
	$tabs .= "<table>";
	if(GetOption('date_format') == '')
	{
		$df = 'd.m.y H:i:s';
	}
	else
	{
		$df = GetOption('date_format');
	}
	$tabs .= "<tr><td>".JText::_('COM_BILLING_FORMAT_DATE').":</td><td> <input class='inputbox' name='date_format' value='$df' type='text' size='50'></td></tr>";
	if(GetOption('linesperrow') == '')
	{
		$ln = '15';
	}
	else
	{
		$ln = GetOption('linesperrow');
	}

	$fd = '';
	if(GetOption('formatdecimals') == '')
	{
		$fd = '2';
	}
	else
	{
		$fd = GetOption('formatdecimals');
	}
	$tabs .= "<tr><td>".JText::_('COM_BILLING_FORMAT_ROWS').":</td><td> <input class='inputbox' name='linesperrow' value='$ln' type='text' size='5'></td></tr>";

	$tabs .= "<tr><td>".JText::_('COM_BILLING_FORMAT_DECIMALS').":</td><td> <input class='inputbox' name='formatdecimals' value='$fd' type='text' size='5'></td></tr>";
	
	$sitelang = GetOption('sitelang');
	if ($sitelang == '') {$sitelang = false;} if ($sitelang == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<tr><td><input id='sitelang' name='sitelang' value='$sitelang' type='checkbox' $checked onclick=\"this.value = this.checked;\"> </td><td>".JText::_('COM_BILLING_USE_SITE_LANGUAGE')." </td></tr>";
		
	$tabs .= "<tr><td>".JText::_('COM_BILLING_TITLECAB').":</td><td> <input class='inputbox' name='title' value='$title' type='text' size='100'></td></tr>";	
	$tabs .= "<tr><td>".JText::_('COM_BILLING_INFO').":</td><td> <textarea name='info' cols='50' rows='5'>$info</textarea></td></tr>";	
	
	$tabs .= "</table>";
	//$tabs .= $pane->endPanel();

	
	if(BILLING_EXT)
	{
		//$tabs .= $pane->startPanel(  JText::_('COM_BILLING_CAT_OPTIONS'), 'cats' );
		$tabs .= JHtml::_('tabs.panel', JText::_('COM_BILLING_CAT_OPTIONS'), 'cats');
		$tabs .= "<h2>" . JText::_('COM_BILLING_CAT_OPTIONS1') . "</h2>";
		
		$item_value = explode(',', GetOption('cats'));
		if(JoomlaVersion() <= '1.5')
		{
			$query = "select distinct id, title, language from `#__categories` where published = 1 order by title";
		}
		else
		{
			$query = "select distinct id, title, language from `#__categories` where extension = 'com_content' and published = 1 order by title";
		}
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList();
		if(count($rows) > 0)
		{
			foreach ( $rows as $row )
			{
				$s = '';
				for($i = 0; $i < count($item_value); $i++)
				{
					if($item_value[$i] == $row->id) {$s = 'checked';} 
				}
				$tabs .= "<input type='checkbox' value='$row->id' name=cats[] $s>$row->title ($row->language)<br>"; 
			}
		}
		$item_value = explode(',', GetOption('catsk2'));
		if(file_exists(JPATH_ROOT . '/components/com_k2/k2.php'))
		{
			$tabs .= GetK2CategoriesCB();
		}
		
		$tabs .= JHtml::_('tabs.panel', JText::_('COM_BILLING_SMS_KEY'), 'sms1');
		$tabs .= "<p>".JText::_('COM_BILLING_SMS_KEY2')."</p><table>";
		$tabs .= "<tr><td>".JText::_('COM_BILLING_SMS_KEY3').":</td><td> <input class='inputbox' name='smskey' value='".GetOption('smskey')."' type='text' size='100'></td><td> ".JText::_('COM_BILLING_SMS_KEY4')."</td></tr></table>";
		//$tabs .= $pane->endPanel();
	}
	
	if(BILLING_EXT)
	{
		//$tabs .= $pane->startPanel( JText::_('COM_BILLING_PARTNERSHIP'), 'partners' );
		$tabs .= JHtml::_('tabs.panel', JText::_('COM_BILLING_PARTNERSHIP'), 'partners');
		if ($partner == '') {$partner = false;} if ($partner == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='partner' name='partner' value='$partner' type='checkbox' $checked onclick=\"document.getElementById('partner').value = document.getElementById('partner').checked;\"> &nbsp;".JText::_('COM_BILLING_USE_REFERAL')."<br><br>";
		if ($admin_percent == '') {$admin_percent = false;} if ($admin_percent == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='admin_percent' name='admin_percent' value='$admin_percent' type='checkbox' $checked onclick='this.value = this.checked;'> &nbsp;".JText::_('COM_BILLING_ADMIN_PARTNER')."<br>";		
		if ($partpoint == '') {$partpoint = false;} if ($partpoint == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='partpoint' name='partpoint' value='$partpoint' type='checkbox' $checked onclick='this.value = this.checked;'> &nbsp;".JText::_('COM_BILLING_ADMIN_PARTNERPOINT')."<br>";		
					
		$tabs .= "<table border='0'><tr><td>".JText::_('COM_BILLING_PARTNER_PERCENT')."</td> <td><input class='inputbox' type='text' name='partnerpercent' value='$partnerpercent' size='5'> &nbsp;%</td></tr>";
		$tabs .= "<tr><td>".JText::_('COM_BILLING_ONE_TIME_PAY')."</td> <td><input class='inputbox' type='text' name='partnerpayment' value='$partnerpayment' size='5'> &nbsp;" . GetDefaultCurrencyAbbr() . "</td></tr>";
		$tabs .= "<tr><td>".JText::_('COM_BILLING_REDIRECT')."</td> <td><input class='inputbox' type='text' name='redirect' value='$redirect' size='100'></td></tr>";
		$tabs .= "<tr><td>".JText::_('COM_BILLING_EASYURL')."</td> <td><input class='inputbox' type='text' name='easyurl' value='$easyurl' size='100'></td></tr>";
		$tabs .= "<tr><td>Vkontakte ID</td> <td><input class='inputbox' type='text' name='vkontakte' value='$vkontakte' size='30'> (<a href='http://vk.com/developers.php?oid=-1&p=Comments' target='_blank'>http://vk.com/developers.php?oid=-1&p=Comments</a>)</td></tr>";
		$tabs .= "<tr><td>Facebook ID</td> <td><input class='inputbox' type='text' name='facebook' value='$facebook' size='30'> (<a href='http://developers.facebook.com/docs/reference/plugins/like/' target='_blank'>http://developers.facebook.com/docs/reference/plugins/like/</a>)</td></tr>";
		$tabs .="</table>";
		if ($invite == '') {$invite = false;} if ($invite == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='invite' name='invite' value='$invite' type='checkbox' $checked onclick=\"document.getElementById('invite').value = document.getElementById('invite').checked;\"> &nbsp;".JText::_('COM_BILLING_USE_INVITE')."<br><br>";
		$tabs .= "<table>";
		$tabs .= "<tr><td>".JText::_('COM_BILLING_USE_INVITE_SUBJ').":</td><td> <input class='inputbox' name='invite_subj' value='$invite_subj' type='text' size='100'></td></tr></table>";
		$tabs .= JText::_('COM_BILLING_USE_INVITE_TEXT'). ": <br>";
		$editor = JFactory::getEditor();
		$tabs .= $editor->display( 'invite_text', $invite_text, '600', '160', '20', '20', false) . '<br><br>';

		if ($copyright == '') {$copyright = false;} if ($copyright == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='copyright' name='copyright' value='$copyright' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;".JText::_('COM_BILLING_COPYRIGHT')."<br>";
		
		if($UA->ExtensionExists('extraregistration'))
		{
			$tabs .= "<table>";
			$tabs .= "<tr><td>".JText::_('COM_BILLING_ML_PARTNER_PERCENT').":</td><td> <input class='inputbox' name='p_percent' value='$p_percent' type='text' size='5'> %</td></tr>";
			$tabs .= "<tr><td>".JText::_('COM_BILLING_ML_PARTNER_DEPTH').":</td><td> <input class='inputbox' name='p_depth' value='$p_depth' type='text' size='5'> </td></tr>";
			$tabs .= "</table>";
		}
		//$tabs .= $pane->endPanel();
	}

	if(BILLING_EXT)
	{
		//$tabs .= $pane->startPanel(  JText::_('COM_BILLING_SERVICES'), 'services' );
		$tabs .= JHtml::_('tabs.panel', JText::_('COM_BILLING_SERVICES'), 'services');
		if ($service1 == '') {$service1 = false;} if ($service1 == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='service1' name='service1' value='$service1' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;".JText::_('COM_BILLING_ADMIN_NOTIFY')." <br><br>";
		if ($service2 == '') {$service2 = false;} if ($service2 == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='service2' name='service2' value='$service2' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;".JText::_('COM_BILLING_USER_NOTIFY')." <br><br>";
		if ($maketariff == '') {$maketariff = false;} if ($maketariff == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='maketariff' name='maketariff' value='$maketariff' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;".JText::_('COM_BILLING_MAKE_TARIFF')." <br><br>";
		if ($changetariff == '') {$changetariff = false;} if ($changetariff == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='changetariff' name='changetariff' value='$changetariff' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;".JText::_('COM_BILLING_CHANGE_TARIFF')." <br><br>";
		if ($hideservices == '') {$hideservices = false;} if ($hideservices == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='hideservices' name='hideservices' value='$hideservices' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;".JText::_('COM_BILLING_HIDE_SERVICES')." <br><br>";
		if ($hidetariff == '') {$hidetariff = false;} if ($hidetariff == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='hidetariff' name='hidetariff' value='$hidetariff' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;".JText::_('COM_BILLING_HIDE_TARIFF')." <br><br>";
		//$tabs .= $pane->endPanel();
	}
	
	///SMS
	if(file_exists(JPATH_ROOT . '/components/com_jsms/jsmsapi.php'))
	{
		include_once (JPATH_ROOT . '/components/com_jsms/jsmsapi.php');
		//$tabs .= $pane->startPanel( 'SMS', 'sms' );
		$tabs .= JHtml::_('tabs.panel', 'SMS', 'sms');
		
		if ($smsadmin == '') {$smsadmin = false;} if ($smsadmin == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='smsadmin' name='smsadmin' value='$smsadmin' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;".JText::_('COM_BILLING_SMS_ADMIN_NOTIFY')." <input type='text' name='adminphone' value='$adminphone' size='14' style='float: none'><br><br>";
		if ($smsalladmin == '') {$smsalladmin = false;} if ($smsalladmin == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='smsalladmin' name='smsalladmin' value='$smsalladmin' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;".JText::_('COM_BILLING_SMS_ALLADMIN_NOTIFY')."<br><br>";
		if ($smsuser == '') {$smsuser = false;} if ($smsuser == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='smsuser' name='smsuser' value='$smsuser' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;".JText::_('COM_BILLING_SMS_USER_NOTIFY')."<br><br>";		
		if ($smspayd == '') {$smspayd = false;} if ($smspayd == true){$checked = 'checked';} else {$checked = '';}
		$tabs .= "<input id='smspayd' name='smspayd' value='$smspayd' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;".JText::_('COM_BILLING_SMS_PRICE')." <input type='text' name='smsprice' value='$smsprice' size='4' style='float: none'> ".GetDefaultCurrencyAbbr()."<br><br>";

		//$tabs .= $pane->endPanel();	
	}
	
	if(multy)
	{
		$tabs .= JHtml::_('tabs.panel', JText::_('COM_BILLING_MULTY'), 'multy');
		$tabs .= "<table border='0'>";
		$tabs .= "<tr><td>Level 1</td><td><input type='text' size='5' value='".GetOption('level1')."' name='level1'> %</td></tr>";
		$tabs .= "<tr><td>Level 2</td><td><input type='text' size='5' value='".GetOption('level2')."' name='level2'> %</td></tr>";
		$tabs .= "<tr><td>Level 3</td><td><input type='text' size='5' value='".GetOption('level3')."' name='level3'> %</td></tr>";
		$tabs .= "<tr><td>Level 4</td><td><input type='text' size='5' value='".GetOption('level4')."' name='level4'> %</td></tr>";
		$tabs .= "<tr><td>Level 5</td><td><input type='text' size='5' value='".GetOption('level5')."' name='level5'> %</td></tr>";
		$m_sub = GetOption('m_sub');
		$tabs .= "<tr><td>".JText::_('COM_BILLING_MULTI_SUB')."</td><td><input type='checkbox' value='$m_sub' name='m_sub' onclick='this.value=this.checked'></td></tr>";
		$m_serv = GetOption('m_serv');
		$tabs .= "<tr><td>".JText::_('COM_BILLING_MULTI_SERV')."</td><td><input type='checkbox' value='$m_serv' name='m_serv' onclick='this.value=this.checked'></td></tr>";
		$m_buy = GetOption('m_buy');
		$tabs .= "<tr><td>".JText::_('COM_BILLING_MULTI_BUY')."</td><td><input type='checkbox' value='$m_buy' name='m_buy' onclick='this.value=this.checked'></td></tr>";
		$tabs .= "</table>";
	}
	
	if ($logenabled)
	{
		//$tabs .= $pane->startPanel( JText::_('COM_BILLING_VIEW_LOG'), 'log' );
		$tabs .= JHtml::_('tabs.panel', JText::_('COM_BILLING_VIEW_LOG'), 'log');
		$t_filter = JRequest::getCmd('t_filter');
		$o_filter = JRequest::getCmd('o_filter');
		$tabs .= ShowLog($t_filter, $o_filter);
		//$tabs .= $pane->endPanel();	
	}
	
	$db = JFactory::getDBO();
	$query = 'SELECT VERSION()';
	$result = $db->setQuery($query);  
	$mysql = $db->loadResult();
	
	//$tabs .= $pane->startPanel( 'System Information', 'sysInfo' );
	$tabs .= JHtml::_('tabs.panel', 'System Information', 'sysInfo');
	$tabs .= "<strong>Joomla Version</strong>: ".JoomlaVersion(TRUE)."<br>";	
	$tabs .= "<strong>Billing Version</strong>: ".BILLING_VER."<br>";	
	$tabs .= "<strong>MySQL version</strong>: $mysql<br>";	
	//$tabs .= "<strong></strong>: <br>";	
	//$tabs .= $pane->endPanel();	
	
	//$tabs .= $pane->endPane();
	$tabs .= JHtml::_('tabs.end');
	echo $tabs;

}

function SaveOptions()
{
	$smskey = JRequest::getVar('smskey');
	$partner = JRequest::getVar('partner');
	$partnerpercent = JRequest::getVar('partnerpercent');
	$partnerpayment = JRequest::getVar('partnerpayment');
	$service1 = JRequest::getVar('service1');
	$service2 = JRequest::getVar('service2');
	$usecoupon = JRequest::getVar('usecoupon');
	$couponprice = JRequest::getVar('couponprice');
	$useaccounts = JRequest::getVar('useaccounts');
	$notify = JRequest::getVar('notify');
	$usegroups = JRequest::getVar('usegroups');
	$logenabled = JRequest::getVar('logenabled');
	$pointsenabled = JRequest::getVar('pointsenabled');
	$redirect = JRequest::getVar('redirect');
	$exchange = JRequest::getVar('exchange');
	$vkontakte = JRequest::getVar('vkontakte');
	$facebook = JRequest::getVar('facebook');
	$withdrawal = JRequest::getVar('withdrawal');
	$showip = JRequest::getVar('showip');
	$artcounter = JRequest::getVar('artcounter');
	$showintro = JRequest::getVar('showintro');
	$slider = JRequest::getVar('slider');	
	$afterpay = JRequest::getVar('afterpay');
	$movemoney = JRequest::getVar('movemoney');	
	$author = JRequest::getVar('author');	
	$authorpercent = JRequest::getVar('authorpercent');
	$copyright = JRequest::getVar('copyright');
	$maketariff = JRequest::getVar('maketariff');
	$changetariff = JRequest::getVar('changetariff');	
	$hideservices = JRequest::getVar('hideservices');
	$publisher = JRequest::getVar('publisher');
	$invite = JRequest::getVar('invite');
	$invite_text = JRequest::getVar( 'invite_text', '', 'post', 'string', JREQUEST_ALLOWHTML );
	$invite_subj = JRequest::getVar('invite_subj');
	$sendsubnotif = JRequest::getVar('sendsubnotif');
	$date_format = JRequest::getVar('date_format');
	$linesperrow = JRequest::getInt('linesperrow', 15);
	$formatdecimals = JRequest::getInt('formatdecimals', 2);
	$sitelang = JRequest::getVar('sitelang');
	$paysum = JRequest::getVar('paysum', 100);
	$paysumreadonly = JRequest::getVar('paysumreadonly', false);
	$smsadmin = JRequest::getVar('smsadmin');
	$adminphone = JRequest::getCmd('adminphone');
	$smsuser = JRequest::getVar('smsuser');
	$smsalladmin = JRequest::getVar('smsalladmin');
	$smspayd = JRequest::getVar('smspayd');
	$smsprice = JRequest::getFloat('smsprice', 0);
	$comission = JRequest::getFloat('comission', 0);
	$mv_comission = JRequest::getFloat('mv_comission', 0);
	$admin_percent = JRequest::getVar('admin_percent');
	$hidetariff = JRequest::getVar('hidetariff');
	$moderation = JRequest::getVar('moderation');
	$hidesubs = JRequest::getVar('hidesubs');
	$hidefroze = JRequest::getVar('hidefroze');
	$p_percent = JRequest::getFloat('p_percent');
	$p_depth = JRequest::getInt('p_depth');
	$easyurl = JRequest::getVar('easyurl');
	$minsum = JRequest::getVar('minsum');
	$maxsum = JRequest::getVar('maxsum');
	$cashouttab = JRequest::getVar('cashouttab');
	$title = JRequest::getVar('title');
	$info = JRequest::getVar('info');
			
	SaveOption('smskey', $smskey);
	SaveOption('partner', $partner);
	SaveOption('partnerpercent', $partnerpercent);
	SaveOption('partnerpayment', $partnerpayment);
	SaveOption('service1', $service1);
	SaveOption('service2', $service2);
	SaveOption('usecoupon', $usecoupon);
	SaveOption('couponprice', $couponprice);
	SaveOption('useaccounts', $useaccounts);
	SaveOption('notify', $notify);
	SaveOption('usegroups', $usegroups);
	SaveOption('logenabled', $logenabled);
	SaveOption('pointsenabled', $pointsenabled);
	SaveOption('redirect', $redirect);
	SaveOption('exchange', $exchange);
	SaveOption('vkontakte', $vkontakte);
	SaveOption('facebook', $facebook);
	SaveOption('withdrawal', $withdrawal);
	SaveOption('showip', $showip);
	SaveOption('artcounter', $artcounter);
	SaveOption('showintro', $showintro);
	SaveOption('slider', $slider);
	SaveOption('afterpay', $afterpay);	
	SaveOption('movemoney', $movemoney);	
	SaveOption('author', $author);		
	SaveOption('authorpercent', $authorpercent);
	SaveOption('copyright', $copyright);
	SaveOption('maketariff', $maketariff);
	SaveOption('changetariff', $changetariff);
	SaveOption('hideservices', $hideservices);	
	SaveOption('publisher', $publisher);	
	SaveOption('invite', $invite);	
	SaveOption('invite_text', $invite_text, true);	
	SaveOption('invite_subj', $invite_subj);
	SaveOption('sendsubnotif', $sendsubnotif);	
	SaveOption('date_format', $date_format);	
	SaveOption('linesperrow', $linesperrow);
	SaveOption('formatdecimals', $formatdecimals);
	SaveOption('sitelang', $sitelang);	
	SaveOption('paysum', $paysum);
	SaveOption('paysumreadonly', $paysumreadonly);
	SaveOption('smsadmin', $smsadmin);
	SaveOption('adminphone', $adminphone);
	SaveOption('smsuser', $smsuser);
	SaveOption('smsalladmin', $smsalladmin);
	SaveOption('smspayd', $smspayd);
	SaveOption('smsprice', $smsprice);
	SaveOption('comission', $comission);
	SaveOption('mv_comission', $mv_comission);
	SaveOption('admin_percent', $admin_percent);	
	SaveOption('hidetariff', $hidetariff);
	SaveOption('moderation', $moderation);	
	SaveOption('hidesubs', $hidesubs);
	SaveOption('hidefroze', $hidefroze);
	SaveOption('p_percent', $p_percent);
	SaveOption('p_depth', $p_depth);
	SaveOption('easyurl', $easyurl);	
	SaveOption('minsum', $minsum);	
	SaveOption('maxsum', $maxsum);	
	SaveOption('cashouttab', $cashouttab);
	SaveOption('title', $title);	
	SaveOption('info', $info);	
	
	SaveOption('level1', JRequest::getVar('level1'));	
	SaveOption('level2', JRequest::getVar('level2'));	
	SaveOption('level3', JRequest::getVar('level3'));	
	SaveOption('level4', JRequest::getVar('level4'));	
	SaveOption('level5', JRequest::getVar('level5'));	
	
	SaveOption('m_sub', JRequest::getVar('m_sub'));	
	SaveOption('m_serv', JRequest::getVar('m_serv'));	
	SaveOption('m_buy', JRequest::getVar('m_buy'));	
	SaveOption('vote', JRequest::getVar('vote'));	
	SaveOption('voteprice', JRequest::getVar('voteprice'));	
	SaveOption('partpoint', JRequest::getVar('partpoint'));		
	

	$items = JRequest::getVar("cats");
	$value = '';
	for($i = 0; $i <count($items); $i++)
	{
		if($value != '')
		{
			$value .= ','; 
		}
		$value .= $items[$i];
	}
	SaveOption('cats', $value);
	
	$items = JRequest::getVar("catsk2");
	$value = '';
	for($i = 0; $i <count($items); $i++)
	{
		if($value != '')
		{
			$value .= ','; 
		}
		$value .= $items[$i];
	}
	SaveOption('catsk2', $value);
	
	
	JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLING_SAVED_SUCCESS') );
}

$view = JRequest::getCmd('view');  
if ($view != '')
{
	$task = $view;
}
$task = JRequest::getCmd('task');  
$uid =  JRequest::getInt('uid');
$id = JRequest::getInt('id');
$sid = JRequest::getInt('sid');
$days = JRequest::getInt('days');
$val = JRequest::getFloat('val');
$order = JRequest::getCmd('order');
$acc_id = JRequest::getCmd('acc_id');

$Section = JRequest::getVar('Section');
$Category = JRequest::getVar('Category');
$p1 = JRequest::getVar('p1'); 
$p2 = JRequest::getVar('p2');
$p3 = JRequest::getVar('p3');
$search = JRequest::getVar('search');
$limitstart = JRequest::getVar('limitstart');
$limit = JRequest::getVar('limit');
$group = (int)JRequest::getCmd('group_id');
$t_filter = JRequest::getVar('t_filter'); 
$o_filter = JRequest::getVar('o_filter'); 
$from_time = JRequest::getVar('from_time'); 
$to_time = JRequest::getVar('to_time'); 
$k2 = JRequest::getInt('k2'); 



/************************	Toolbar		***************/

$imagebase = JURI::base() . 'components/com_billing/images/';
$imagebase = str_replace('administrator/', '', $imagebase);
$users = $imagebase . 'users.png';
$analytics = $imagebase . 'analytics.png';
$subscribe = $imagebase . 'subscribe.png';
$plugins = $imagebase . 'plugins.png';
$article = $imagebase . 'article.png';
$service = $imagebase . 'service.png';
$currency = $imagebase . 'currency.png';
$banker = $imagebase . 'banker.png';
$options = $imagebase . 'options.png';
$points = $imagebase . 'points.png';
$tabs = $imagebase . 'tab.png';
$projects = $imagebase . 'projects.png';
$pin = $imagebase . 'pin.png';


echo '<div class="m">';
echo '<table align="center" border="0" style="padding: 4px; font-szie: 12px;"><tr>';
if ($task == 'list' or $task == '') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
echo ' <a href="index.php?option=com_billing&task=list"><p align="center"><img src="'.$users.'" alt="'.JText::_('COM_BILLING_USERS').'"><br/>'.JText::_('COM_BILLING_USERS').'</p></a>';
if ($task == 'list' or $task == '') {echo '</strong></td>';} else {echo '</td>';}

if ($task == 'groups') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
echo '<a href="index.php?option=com_billing&task=groups"><p align="center"><img src="'.$users.'" alt="'.JText::_('COM_BILLING_GROUPS').'"><br/>'.JText::_('COM_BILLING_GROUPS').'</p></a> ';
if ($task == 'groups') {echo '</strong></td>';} else {echo '</td>';}

if(BILLING_EXT)
{
	if ($task == 'points') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
	echo '<a href="index.php?option=com_billing&task=points"><p align="center"><img src="'.$points.'" alt="'.JText::_('COM_BILLING_POINTS').'"><br/>'.JText::_('COM_BILLING_POINTS').'</p></a> ';
	if ($task == 'points') {echo '</strong></td>';} else {echo '</td>';}

	if ($task == 'pin') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
	echo '<a href="index.php?option=com_billing&task=pin"><p align="center"><img src="'.$pin.'" alt="'.JText::_('COM_BILLING_PIN').'"><br/>'.JText::_('COM_BILLING_PIN').'</p></a> ';
	if ($task == 'pin') {echo '</strong></td>';} else {echo '</td>';}
}
	if ($task == 'analytics') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
	echo '<a href="index.php?option=com_billing&task=analytics"><p align="center"><img src="'.$analytics.'" alt="'.JText::_('COM_BILLING_ANALYTICS').'"><br/>'.JText::_('COM_BILLING_ANALYTICS').'</p></a> ';
	if ($task == 'analytics') {echo '</strong></td>';} else {echo '</td>';}
if(BILLING_EXT)
{
	if ($task == 'subs') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
	echo '<a href="index.php?option=com_billing&task=subs"><p align="center"><img src="'.$subscribe.'" alt="'.JText::_('COM_BILLING_SUBS').'"><br/>'.JText::_('COM_BILLING_SUBS').'</p></a> ';
	if ($task == 'subs') {echo '</strong></td>';} else {echo '</td>';}
}
if ($task == 'plugins') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
echo '<a href="index.php?option=com_billing&task=plugins"><p align="center"><img src="'.$plugins.'" alt="'.JText::_('COM_BILLING_PLUGINS').'"><br/>'.JText::_('COM_BILLING_PLUGINS').'</p></a> ';
if ($task == 'plugins') {echo '</strong></td>';} else {echo '</td>';}


	if ($task == 'tabs') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
	echo '<a href="index.php?option=com_billing&task=tabs"><p align="center"><img src="'.$tabs.'" alt="'.JText::_('COM_BILLING_TABS').'"><br/>'.JText::_('COM_BILLING_TABS').'</p></a> ';
	if ($task == 'tabs') {echo '</strong></td>';} else {echo '</td>';}

if(BILLING_EXT)
{
	if ($task == 'articles') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
	echo '<a href="index.php?option=com_billing&task=articles"><p align="center"><img src="'.$article.'" alt="'.JText::_('COM_BILLING_ARTICLES').'"><br/>'.JText::_('COM_BILLING_ARTICLES').'</p></a> ';
	if ($task == 'articles') {echo '</strong></td>';} else {echo '</td>';}

	if ($task == 'services') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
	echo '<a href="index.php?option=com_billing&task=services"><p align="center"><img src="'.$service.'" alt="'.JText::_('COM_BILLING_SERVICES').'"><br/>'.JText::_('COM_BILLING_SERVICES').'</p></a> ';
	if ($task == 'services') {echo '</strong></td>';} else {echo '</td>';}
}
if ($task == 'currency') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
echo '<a href="index.php?option=com_billing&task=currency"><p align="center"><img src="'.$currency.'" alt="'.JText::_('COM_BILLING_CURRENCIES').'"><br/>'.JText::_('COM_BILLING_CURRENCIES').'</p></a> ';
if ($task == 'currency') {echo '</strong></td>';} else {echo '</td>';}

if(BILLING_EXT)
{
	if ($task == 'accounts') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
	echo '<a href="index.php?option=com_billing&task=accounts"><p align="center"><img src="'.$banker.'" alt="'.JText::_('COM_BILLING_ACCOUNTS').'"><br/>'.JText::_('COM_BILLING_ACCOUNTS').'</p></a> ';
	if ($task == 'accounts') {echo '</strong></td>';} else {echo '</td>';}

	if ($task == 'projects') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
	echo '<a href="index.php?option=com_billing&task=projects"><p align="center"><img src="'.$projects.'" alt="'.JText::_('COM_BILLING_PROJECTS').'"><br/>'.JText::_('COM_BILLING_PROJECTS').'</p></a> ';
	if ($task == 'projects') {echo '</strong></td>';} else {echo '</td>';}
}
if ($task == 'options') {echo '<td width="60" style="background: #D3D1F7;
padding: 0px;
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
padding-top: 10px;"><strong>';} else {echo '<td width="60">';}
echo '<a href="index.php?option=com_billing&task=options"><p align="center"><img src="'.$options.'" alt="'.JText::_('COM_BILLING_OPTIONS').'"><br/>'.JText::_('COM_BILLING_OPTIONS').'</p></a> ';
if ($task == 'options') {echo '</strong></td>';} else {echo '</td>';}
echo '</tr></table>';
echo '</div><hr>';

/************************	Toolbar	end	***************/

switch ($task)
{
		case 'list':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			UserList($order, $limitstart, $limit, $search, $group);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="list" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'view':
			UserHistory($uid);
		break;
		case 'partners':
			UserPartners($uid);
		break;
		case 'partnerhistory':
			ShowPartnerHistory($id, $uid);
		break;
		case 'add':
			AddSomeMoney($uid);
		break;
		case 'withdraw':
			WithdrawSomeMoney($uid);
		break;
		case 'add2':
			AddMoney2($uid, $val);
		break;
		case 'withdraw2':
			WithdrawMoney2($uid, $val);
		break;
		case 'subs':
			ShowSubscriptions();
		break;
		case 'plugins':
			ShowPlugins();
		break;
		case 'tabs':
			ShowTabs();
		break;
		case 'plugin':
			ShowPlugin($id);
		break;
		case 'addplugin':
			AddPlugin();
			ShowPlugins();
		break;
		case 'update':
			UpdatePlugin($id);
			ShowPlugins();
	    break;
	    case 'delplug':
			DeletePlugin($id);
			ShowPlugins();
		break;
	    case 'delsub':
			DeleteSubscription($id);
			ShowSubscriptions();
	    break;
	    case 'activesub':
			ActivateSubscription($id);
			ShowSubscriptions();
	    break;
	    case 'editsub':
			EditSubscription($id);
	    break;
	    case 'addsub':
			EditSubscription();
	    break;
	    case 'savesub':
			SaveSubscription($id);
			ShowSubscriptions();
		break;
	    case 'active':
			ActivatePlugin($id);
			ShowPlugins();
	    break;
	    case 'addusersub':
			AddSubscription($id, $uid, $days);
			EditSubscription($id);
		break;  
	    case 'delusersub':
			DelUserSubscription($id, $uid);
			EditSubscription($id);
	    break;
		case 'delsubitem':
			DeleteSubItem($id);
			EditSubscription($sid);
		break;
		case 'addsubitem':
			AddSubItem($id);
		break;
		case 'saveitem':
			SaveItem($id);
			EditSubscription($id);
		break;
		case 'articles':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowArticles($order, $Section, $Category, $p1, $p2, $p3, $search, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="articles" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'editart':
			if($k2 == 1)
			{
				EditArticle($id, true);
			}
			else
			{
				EditArticle($id, false);
			}
		break;
		case 'saveart':
			SaveArticle($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowArticles($order, $Section, $Category, $p1, $p2, $p3, $search, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="articles" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'activeart':
			ActivateArticle($id);
		break;
		case 'activesms':
			ActivateSMS($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowArticles($order, $Section, $Category, $p1, $p2, $p3, $search, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="articles" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'suball':
			SubAll();
		break;
		case 'sms':
			SMSAll();
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowArticles($order, $Section, $Category, $p1, $p2, $p3, $search, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="articles" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'assign':
			AddPrice();
		break;
		case 'saveall':
			SaveSubAll();
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowArticles($order, $Section, $Category, $p1, $p2, $p3, $search, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="articles" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'priceall':
			SavePriceAll();
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowArticles($order, $Section, $Category, $p1, $p2, $p3, $search, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="articles" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'savesubper':
			SavePeriod($id);
			EditSubscription($id);
		break;
		case 'delsubitemper':
			DeletePeriod($id);
			EditSubscription($sid);
		break;
		case 'deluserart':
			DeleteUserAccess($id, $uid);
			EditArticle($id);
		break;
		case 'adduserart':
			AddUserAccess($id, $uid);
			EditArticle($id);
		break;
		case 'options':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowOptions();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="saveoptions" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';						
		break;
		case 'saveoptions':
			SaveOptions();
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowOptions();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="saveoptions" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';						
		break;
		case 'confirm':
			ConfirmForm($id);
		break;
		case 'confirm2':
			ConfirmPayment($id);
			UserHistory($uid);
		break;
		case 'services':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowServices($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="services" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'service':
			EditService();
		break;
		case 'editservice':
			EditService($id);
		break;
		case 'saveservice':
			SaveService($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowServices($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="saveservice" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'activateservice':
			ActivateItem($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowServices($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="activateservice" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'deleteservice':
			DeleteService($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowServices($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="deleteservice" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'serviceusers':
			ServiceUsers($id);
		break;
		case 'checkservice':
			CheckService($id, $uid);
			ShowServices($order, $limitstart, $limit);
		break;
		case 'deleteserviceuser':
			DeleteUserService($id, $uid);
			ShowServices($order, $limitstart, $limit);
		break;
		case 'currency':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowCurrency();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'activatecurrency':
			ActivateCurrency($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowCurrency();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'addcurrency':
			EditCurrency($id);
		break;
		case 'editcurrency':
			EditCurrency($id);
		break;
		case 'savecurrency':
			SaveCurrency($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowCurrency();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'deletecurrency':
			DeleteCurrency($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowCurrency();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'accounts':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowAccounts();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'activeaccount':
			ActivateAccount($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowAccounts();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';	
		break;
		case 'addaccount':
			EditAccount();
		break;
		case 'editaccount':
			EditAccount($id);
		break;
		case 'saveaccount':
			SaveAccount();
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowAccounts();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
		break;
		case 'showaccounts':
			ShowUserAccounts($uid);
		break;
		case 'deleteaccount':
			DeleteAccount($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowAccounts();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'plusaccusr':
			PlusAcc($uid, $acc_id);
		break;
		case 'addmoneyacc':
			AddMoneyAcc($uid, $acc_id);
		break;
		case 'showaccusr':
			ShowUserAccount($uid, $acc_id);
		break;
		case 'minusaccusr':
			MinusAcc($uid, $acc_id);
		break;
		case 'witmoneyacc':
			WithMoneyAcc($uid, $acc_id);
		break;
		case 'groups':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowGroups($order, $limitstart='', $limit = '');
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="groups" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
		break;
		case 'points':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowUserPoints();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
		break;
		case 'analytics':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowAnalytics($limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
		break;
		case 'addgroup':
			EditGroup();
		break;
		case 'editgroup':
			EditGroup($id);
		break;
		case 'savegroup':
			SaveGroup($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowGroups($order, $limitstart='', $limit = '');
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
		break;
		case 'activategroup':
			ActivateGroup($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowGroups($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="activategroup" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'deletegroup':
			DeleteGroup($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowGroups($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="deletegroup" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'groupusers':
			GroupUsers($id);
		break;
		case 'deleteuserfromgroup':
			DeleteUserGroup($uid);
			GroupUsers($id);
		break;
		case 'addtogroup':
			AddToGroup();
		break;
		case 'savetogroup':
			SaveToGroup();
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowGroups($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="savetogroup" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'disablediscount':
			SwitchDiscount($uid, 0);
			GroupUsers($id);
		break;
		case 'enablediscount':
			SwitchDiscount($uid, 1);
			GroupUsers($id);
		break;
		case 'logdelall':
			LogDelAll();
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowOptions();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="logdelall" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';						
		break;
		case 'logsaveall':
			LogSaveAll();
		break;
		case 'dellogrec':
			DelLogRec($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowOptions();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="dellogrec" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';						
		break;
		case 'logtoday':
			///	
		break;
		case 'editpoint':
			AddPoint($id);
		break;
		case 'addpoints':
			AddPoint();
		break;
		case 'addadmpoints':
			AddPoints($uid);
		break;
		case 'savepoint':
			SavePoints($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowUserPoints();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
		break;
		case 'savepoints':
			SaveAdmPoints($uid);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowUserPoints();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
		break;
		case 'activepoint':
			ActivateUPAction($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowUserPoints();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
		break;
		case 'savemasspay':
			MassPayment();
		break;
		case 'delmasspay':
			DelMassPay();
		break;
		case 'reject':
			RejectPayment($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowAnalytics($limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
		break;
		case 'rejectwait':
			RejectWait($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			UserHistory($uid);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';		
		break;
		case 'accept':
			AcceptPayment($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowAnalytics($limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
		break;
		case 'addtab':
			AddTab();
			ShowTabs();
		break;
		case 'activetab':
			ActivateTab($id);
			ShowTabs();
		break;
		case 'edittab':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			EditTab($id);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="list" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'savetab':
			SaveTab($id);
			ShowTabs();
		break;
		case 'canceltab':
			ShowTabs();
		break;
		case 'deltab':
			DeleteTab($id);
			ShowTabs();
		break;
		case 'delfile':
			$ii = DelArticleFile($id);
			EditArticle($ii);
		break;
		case 'pin':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowPins();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="pin" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'cancelpin':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowPins();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="pin" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'editpin':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			EditPins($id);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="projects" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';				
		break;
		case 'savepin':
			SavePin($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowPins();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="projects" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';
		break;
		case 'importpin':
			ImportPin();
		break;
		case 'pinimport':
			ImportPin2();
		break;
		case 'exportpin':
			ExportPin();
		break;
		case 'generate':
			GeneratePin();
		break;
		case 'generatego':
			GeneratePinGo();
		break;
		case 'activepin':
			ActivatePin($id);
		break;
		case 'projects':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowProjects($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="projects" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'cancelprojects':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowProjects($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="projects" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'addproject':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			EditProject();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="addproject" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'editproject':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			EditProject($id);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="addproject" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'saveproject':
			SaveProject();
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowProjects($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="saveproject" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'activeproject':
			ActivateProject($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowProjects($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'actab':
			SetTabState($id);
			ShowTabs();
		break;
		case 'freeze':
			Freeze($id, $uid);
			EditSubscription($id);
		break;
		case 'exporthistory':
			ExportHistory();
		break;
		case 'savesublist':
			AddSubListItem($id);
			EditSubscription($id);
		break;
		case 'orderup':
			OrderUp($id);
		break;
		case 'orderdown':
			OrderDown($id);
		break;
		case 'delsublist':
			DelSubListItem($id);
			$sid = JRequest::getVar('sid');
			EditSubscription($sid);
		break;
		case 'editusersub':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			EditUserSub($id);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="list" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';				
		break;
		case 'saveusersub':
			SaveUserSub($id);
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			UserList($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="list" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';				
		break;
		case 'cancelusersub':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			UserList($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="list" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';				
		break;
		case 'sendsms':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			SendSMS_billing();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="list" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'masssend':
			MassSend();
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			UserList($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="list" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'cancelmass':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			UserList($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="list" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
		case 'addpartner':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			AddPartner();
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
		break;
		case 'savepartner':
			SavePartner();
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowAnalytics($limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
		break;
		case 'cancelpartner':
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			ShowAnalytics($limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
		break;
		default:
			echo '<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">';
			UserList($order, $limitstart, $limit);
			echo '<input type="hidden" name="option" value="com_billing" />';
			echo '<input type="hidden" name="task" id="task" value="list" />';
			echo '<input type="hidden" name="boxchecked" value="0" /> ';
			echo '</form>';			
		break;
}


  
?>