<?php

define( '_JEXEC', 1 );

//error_reporting(E_ALL) ;
//ini_set('display_errors', 'On');

define('DS', DIRECTORY_SEPARATOR);
define( 'JPATH_BASE', realpath(dirname(__FILE__).'/../..' ));

	require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
	$mainframe = JFactory::getApplication('site');
	$mainframe->initialise(); 
	
include_once (JPATH_ROOT .'/components/com_billing/useraccount.php');
include_once (JPATH_ROOT . '/components/com_billing/account.php');	

$lang =  JFactory::getLanguage();
$lang->load('com_billing', JPATH_ADMINISTRATOR);	
	
$task = JRequest::getVar('task');	

if($task == 'updatecashout')
{
		$tabs = '';
		$paysys_id = JRequest::getInt('paysys_id');
		$db = JFactory::getDBO();
		$query = "select b.*, ".
			"(select username from `#__users` where id = b.uid) as username, ".
			"(select name from `#__users` where id = b.uid) as user, ".
			"(select paysystem from `#__billing_settings` where id = b.order_id) as paysystem, ".
			"(select cashout_title from `#__billing_settings` where id = b.order_id) as cashout_title ".
			" from  `#__billing` b where data3 = '1' and val < 0 and order_id = $paysys_id order by id";
		$result = $db->setQuery($query);
		$rows = $db->loadObjectList();
		$tabs .= '<table width="100%" class="adminlist table" cellspacing="1">';
		$tabs .= "<tr>";
		$tabs .= "  <th>Id</th>";
		$tabs .= '  <th class="title" width="20"><input id="checkbox" type="checkbox" onclick="checkAll(1000);" name="toggle"></th>';
		$tabs .= "  <th>".JText::_('COM_BILLING_USER')."</th>";
		$tabs .= "  <th>".JText::_('COM_BILLING_SUM')."</th>";
		$tabs .= "  <th>".JText::_('COM_BILLING_PAYSYSTEM')."</th>";
		$tabs .= "  <th>".JText::_('COM_BILLING_WALLET')."</th>";
		$tabs .= "  <th>".JText::_('COM_BILLING_PHONE')."</th>";
		$tabs .= "  <th>".JText::_('COM_BILLING_DATE')."</th>";
		$tabs .= "  <th></th>";
		$tabs .= "</tr>";		
		$coid = GetCashOutCurrency();
		$dcid = GetDefaultCurrency();
		$need_convert = false;
		if ($coid != $dcid)
		{
			$need_convert = true;
		}
		$cab = GetCurrencyAbbr($coid);
		if (count($rows) > 0)	
		{
			foreach ( $rows as $row ) 
			{
				$tabs .= "<tr>";
				$tabs .= "<td>$row->id</td>";
				$tabs .= "<td><input type='checkbox' id='cb$row->id' name='cid[]' value='$row->id' onclick='isChecked(this.checked);'></td>";
				$tabs .= "<td>$row->username ($row->user)</td>";
				if ($need_convert)
				{
					$tabs .= "<td>". FormatCurrency($coid, round(ConvertCurrency($dcid, $coid, -$row->val),2)). "</td>";
				}
				else
				{
					$tabs .= "<td>". FormatCurrency($coid, -$row->val). "</td>";
				}
				if($row->cashout_title != '')
				{
					$tabs .= "<td>$row->cashout_title</td>";
				}
				else
				{
					$tabs .= "<td>$row->paysystem</td>";
				}
				$tabs .= "<td>$row->data4</td>";
				$tabs .= "<td>$row->data5</td>";
				$tabs .= "<td>$row->adate</td>";
				$tabs .= "<td><a href='index.php?option=com_billing&task=reject&id=$row->id'>".JText::_('COM_BILLING_REJECT')."</a><br><a href='index.php?option=com_billing&task=accept&id=$row->id'>".JText::_('COM_BILLING_CASH_APPLY')."</a></td>";
				$tabs .= "</tr>";			
			}
		}
		$query = "select sum(val) from `#__billing` b where data3 = '1' and val < 0 order by id";
		$result = $db->setQuery($query);
		$sum = $db->loadResult();
		if ($need_convert)
		{
			$tabs .= "<tr><td></td><td></td><td></td><td><strong>". FormatCurrency($coid, round(ConvertCurrency($dcid, $coid, -$sum),2))."</strong></td><td></td><td></td><td></td><td></td><td></td></tr>";
		}
		else
		{
			$tabs .= "<tr><td></td><td></td><td></td><td><strong>". FormatCurrency($coid, -$sum)."</strong></td><td></td><td></td><td></td><td></td><td></td></tr>";
		}
		$tabs .= '</table>';
		
		echo $tabs;
		return;
}
	
?>