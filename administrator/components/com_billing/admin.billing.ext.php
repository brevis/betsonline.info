<?php

defined('_JEXEC') or die('Restricted access');

include_once (JPATH_ROOT . '/components/com_billing/useraccount.php');
include_once (JPATH_ROOT . '/components/com_billing/userpoints.php');
include_once (JPATH_ROOT . '/components/com_billing/account.php');
include_once (JPATH_ROOT . '/components/com_billing/plugin.php');
include_once (JPATH_ROOT . '/components/com_billing/billing.func.php');
include_once ('addons.inc');

$document = JFactory::getDocument();
$document->addStyleSheet('/components/com_billing/tooltip_style.css');
$document->addScript('/components/com_billing/tooltip.js');
$document->addScript('/administrator/components/com_billing/admin.ajax.js');

function GetTablesList()
{
	$db = JFactory::getDBO();
	$db->setQuery("SHOW TABLES");
	$rows = $db->loadResultArray();
	$out = '';
    $out .= '<select name="table">';
    foreach ( $rows as $row )
    {
		$out .= "<option value='$row'>$row</option>"; 
    }
    $out .= '</select>';
    return $out;
}

function GetJSGroupList($id = '')
{
	$db = JFactory::getDBO();
	$db->setQuery("select * from `#__community_groups` order by name");
    $rows = $db->loadObjectList();
    $out = '';
    $out .= '<select name="jsgroup_id">';
    foreach ( $rows as $row )
    {
		if ($row->id == $id) {$s = 'selected';} else {$s = '';}
		$out .= "<option value='$row->id' $s>$row->name</option>"; 
    }
    $out .= '</select>';
    return $out;	
}

function GetK2CategoryList($id = '')
{
	$db = JFactory::getDBO();
	$db->setQuery("select * from `#__k2_categories` order by name");
    $rows = $db->loadObjectList();
    $out = '';
    $out .= '<select name="k2catid_id">';
    foreach ( $rows as $row )
    {
		if ($row->id == $id) {$s = 'selected';} else {$s = '';}
		$out .= "<option value='$row->id' $s>$row->name</option>"; 
    }
    $out .= '</select>';
    return $out;	
}
/*
function GetSubCategoryList($id, $tab)
{
	$db = JFactory::getDBO();
	$db->setQuery("select * from `#__adsmanager_categories` where parent = $id");
    $cats = $db->loadObjectList();
	$out = '';
	foreach($cats as $cat)
	{
		
	}
}

function GetAdsCategoryList($id = '')
{
	$db = JFactory::getDBO();
	$db->setQuery("select * from `#__adsmanager_categories` where parent = 0");
    $cats = $db->loadObjectList();
	
	for($i = 0; $i < count($cats); $i++)
	{
		if($cats[$i]->parent == 0)
		{
			
		}
	}
	
    $out = '';
    $out .= '<select name="ads_id">';
}*/

function GetJSGroupName($id)
{
	$db = JFactory::getDBO();
	$db->setQuery("select `name` from `#__community_groups` where id = $id");
	return $db->loadResut();
}

function GetK2CategoryName($id)
{
	$db = JFactory::getDBO();
	$db->setQuery("select * from `#__k2_categories` where id = $id");
	$x = $db->loadAssoc();
	return $x['name'];
}

function GetBillingUserGroup($id)
{
	$db = JFactory::getDBO();
	$query = 'select group_name from `#__billing_group` where id = ' . $id;
	$result = $db->setQuery($query); 
	return $db->loadResut();
}

function GetCustomItemName($row_id, $item_id)
{
	$db = JFactory::getDBO();
	$query = 'select * from `#__billing_subscription_access` where id = ' . $row_id;
    $result = $db->setQuery($query);   
	$row = $db->loadAssoc();
	$field_name = $row['file_path'];
	$id_name = $row['data3'];
	$table = $row['data4'];
	$query = "select $field_name from $table where $id_name = $item_id";
	$result = $db->setQuery($query);   
	return $db->loadResult();
}

function GetServiceName($id)
{
	$db = JFactory::getDBO();
    $query = 'select * from `#__billing_services` where id = ' . $id;
    $result = $db->setQuery($query);   
	$row = $db->loadAssoc();
	return $row['service_name'];
}

function GetAuthor($id)
{
	$db = JFactory::getDBO();
    $query = 'select * from `#__users` where id in (select created_by from `#__content` where id = ' . $id . ')';
    $result = $db->setQuery($query);   
	$row = $db->loadAssoc();
	return $row['name'] . ' (' . $row['username'] . ')'; 
}

function GetTitle($id, $k2 = '')
{
	$db = JFactory::getDBO();
	if($k2 == 1)
	{
		$query = 'select title from `#__k2_items` where id = ' . $id;
	}
	else
	{
		$query = 'select title from `#__content` where id = ' . $id;
	}
	$result = $db->setQuery($query);   
	$result = $db->loadResult();
	return $result; 
}

function GetK2ItemName($id)
{
	$db = JFactory::getDBO();
	$query = 'select title from `#__k2_items` where id = ' . $id;
	$result = $db->setQuery($query);   
	$result = $db->loadResult();
	return $result; 
}

function GetCategoryName($id)
{
	$db = JFactory::getDBO();
	$query = 'select title from `#__categories` where id = ' . $id;
	$result = $db->setQuery($query);   
	$result = $db->loadResult();
	return $result; 
}

function HideMaterial($id)
{
      $db = JFactory::getDBO();
      $query = 'update `#__content` set state = 0 where id = ' . $id;
      $result = $db->setQuery($query);   
	  $result = $db->query();	
}

function GetCategoryList($id = '')
{
/*	$db = JFactory::getDBO();
	$query = "select distinct * from `#__categories` where published = 1 and extension = 'com_content' order by title";
	$result = $db->setQuery($query);   
    $rows = $db->loadObjectList();
    $out = '';
    $out .= '<select name="category_id">';
    foreach ( $rows as $row )
    {
		if ($row->id == $id) {$s = 'selected';} else {$s = '';}
		$out .= "<option value='$row->id' $s>$row->title</option>"; 
    }
    $out .= '</select>';
*/	

		$category_options = JHtml::_('category.options', 'com_content');
		$cats = '<select name="category_id" id="category_id">';
		$cats .= JHTML::_('select.options', $category_options);;
		$cats .= '</select>';

    return $cats;	
}

function GetServiceList($id = '')
{
	$db = JFactory::getDBO();
	$query = 'select distinct * from `#__billing_services` where is_active = 1 order by service_name';
	$result = $db->setQuery($query);   
    $rows = $db->loadObjectList();
    $out = '';
    $out .= '<select name="service_id">';
    foreach ( $rows as $row )
    {
		if ($row->id == $id) {$s = 'selected';} else {$s = '';}
		$out .= "<option value='$row->id' $s>$row->service_name</option>"; 
    }
    $out .= '</select>';
    return $out;
}

function GetSubList($id = '')
{
	$db = JFactory::getDBO();
	$query = 'select distinct * from `#__billing_subscription_type` where is_active = 1 order by subscription_type';
	$result = $db->setQuery($query);   
    $rows = $db->loadObjectList();
    $out = '';
    $out .= '<select name="sub_id">';
    foreach ( $rows as $row )
    {
		if ($row->id == $id) {$s = 'selected';} else {$s = '';}
		$out .= "<option value='$row->id' $s>$row->subscription_type</option>"; 
    }
    $out .= '</select>';
    return $out;
}

function GetK2ContentList($id = '', $all = '')
{
    $db = JFactory::getDBO();
	$k2 = false;
	if(file_exists(JPATH_ROOT . '/components/com_k2/k2.php'))
	{
		$k2 = true;
	}
	$out = '';
	$out .= '<select name="content_id">';
	if($k2)
	{
		if ($all == 1)
		{
			$query = 'select id, title from `#__k2_items` order by title';
		}
		else
		{
			$query = 'select id, title from `#__k2_items` where published = 1 order by title';
		}
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList();
		foreach ( $rows as $row )
		{
			if ($row->id == $id and $is_k2) {$s = 'selected';} else {$s = '';}
			$out .= "<option value='$row->id' $s>$row->title</option>"; 
		}
	}
	$out .= '</select>';
	return $out;
}

function GetContentList($id = '', $all = '', $k2 = false)
{
    $db = JFactory::getDBO();
	if ($all == 1)
	{
		$query = 'select id, title from `#__content` where state >= 0 order by title';
	}
	else
	{
		$query = 'select id, title from `#__content` where state = 1 order by title';
	}
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList();
	$out = '';
	$out .= '<select name="content_id" readonly>';
	foreach ( $rows as $row )
	{
		if ($row->id == $id and !$k2) {$s = 'selected';} else {$s = '';}
		$out .= "<option value='$row->id' $s>$row->title</option>"; 
	}
	if($k2)
	{
		if ($all == 1)
		{
			$query = 'select id, title from `#__k2_items` order by title';
		}
		else
		{
			$query = 'select id, title from `#__k2_items` where published = 1 order by title';
		}
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList();
		foreach ( $rows as $row )
		{
			if ($row->id == $id) {$s = 'selected';} else {$s = '';}
			$out .= "<option value='$row->id' $s>$row->title</option>"; 
		}
	}
	$out .= '</select>';
	return $out;
}

function UserPartners($uid)
{
	$db = JFactory::getDBO();
    $query = 'select * from `#__users` where id=' . $uid;
    $result = $db->setQuery($query);   
    $row = $db->loadAssoc();      
	echo "<h1>".JText::_('COM_BILLING_USER')." ".$row['name']." ".JText::_('COM_BILLING_AND_REFFERALS')."</h1>";

	echo '<table width="100%" class="adminlist table">';
	echo "<tr>";
	echo "  <td>Id</td>";
	echo "  <td>".JText::_('COM_BILLING_USER')."</td>";
	echo "  <td>".JText::_('COM_BILLING_DATE_OF_REGISTER')."</td>";
	echo "  <td>".JText::_('COM_BILLING_EARNED')."</td>";
	echo "</tr>";

	$query = 'select bp.*, (select name from `#__users` where id = bp.partner_id) as username, (select username from `#__users` where id = bp.partner_id) as user from `#__billing_partner` bp where uid =' . $uid;
    $result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 
	
	if (count($rows) > 0)
	{
		foreach ( $rows as $row ) 
		{
			echo '<tr>';
			echo "<td>$row->id</td>";
			echo "<td>$row->username ($row->user)</td>";
			echo "<td>". FormatBillingDate($row->enter_date) ."</td>";
			echo "<td><a href='index.php?option=com_billing&task=partnerhistory&id=$row->partner_id&uid=$uid'>".GetPartnerSum($row->partner_id)."</a></td>";
			echo '</tr>';
		}
	}
	echo '</table>';
}

function ShowSubscriptions()
{
	 echo '<h1>'.JText::_('COM_BILLING_SUBS').'</h1>';
	$db = JFactory::getDBO();
	$query = 'select * from `#__billing_subscription_type` order by id';
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 	
   
	echo '<table width="100%" class="adminlist table" cellspacing="1">';
	echo "<tr>";
	echo "  <th class='title'>Id</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_TITLE')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_DAYS')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_COST')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_STATUS')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_DELETE')."</th>";
	echo "</tr>";
		
	if (count($rows) > 0)
	{	
		foreach ( $rows as $row ) 
		{
			echo '<tr>';
			echo "<td>$row->id</td>";
			echo "<td><a href='index.php?option=com_billing&task=editsub&id=$row->id'>$row->subscription_type</a></td>";
			echo "<td>$row->subscription_days</td>";
			echo "<td> ".FormatDefaultCurrency($row->price)."</td>";
			if($row->is_active == 0)
			{		
				echo "<td><a href='index.php?option=com_billing&task=activesub&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_ACTIVATE')."' title='".JText::_('COM_BILLING_ACTIVATE')."' src='".publish_x."'></a></td>";
			}
			else
			{
				echo "<td><a href='index.php?option=com_billing&task=activesub&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_HIDE')."' title='".JText::_('COM_BILLING_HIDE')."' src='". tick ."'></a></td>";
			}
			echo "<td><a href='index.php?option=com_billing&task=delsub&id=$row->id'>".JText::_('COM_BILLING_DELETE')."</a></td>";
			echo '</tr>';
        }
	}
	echo '</table><hr>';
	
	echo '<form action="index.php?option=com_billing&task=addsub" method="post">';		
	echo JText::_('COM_BILLING_SUB_TITLE'). ': <input class="inputbox" type="text" name="subscription_type" value="" size="100"> ';
	echo JText::_('COM_BILLING_DAYS'). ': <input class="inputbox" type="text" name="subscription_days" value="30" size="10"> ';
	echo JText::_('COM_BILLING_PRICE'). ': <input class="inputbox" type="text" name="price" value="0" size="10"> ' . GetDefaultCurrencyAbbr();
	echo ' <input type="submit" name="sb" value="'.JText::_('COM_BILLING_ADD').'">';
	echo '</form><br>';	
}

function DeleteSubscription($id)
{
	$db = JFactory::getDBO();
	$query = 'select * from `#__billing_subscription` where subscription_id = ' . $id;
	$result = $db->setQuery($query);
	$rows = $db->loadObjectList();   
	if(count($rows) > 0)
	{
		foreach($rows as $row)
		{
			$query = 'delete from `#__billing_subscription_access` where subscription_id = ' . $row->id;
			$result = $db->setQuery($query);
			$result = $db->query();   

			$query = 'delete from `#__billing_mailhistory` where subscription_id = ' . $row->id;
			$result = $db->setQuery($query);
			$result = $db->query();  

			$query = 'delete from `#__billing_subscription` where id = ' . $row->id;
			$result = $db->setQuery($query);
			$result = $db->query();			
		}
	}	

	$query = 'delete from `#__billing_subscription_type` where id = ' . $id;
	$result = $db->setQuery($query);
	$result = $db->query();   
	
	$query = 'delete from `#__billing_subscription_access` where subscription_id not in (select id from `#__billing_subscription_type`)';
	$result = $db->setQuery($query);
	$result = $db->query(); 
}

function ActivateSubscription($id)
{
	$db = JFactory::getDBO();
	$query = 'select * from `#__billing_subscription_type` where id = ' . $id;
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();
	if ($row['is_active'] == 0)
	{
	    $a = 1;
	}
	else
	{
	    $a = 0;
	}
	$query = "update `#__billing_subscription_type` set is_active = $a where id = ". $id;
	$result = $db->setQuery($query);
	$result = $db->query();   
}

function SubGroup($sub_id, $group_id)
{
	$db = JFactory::getDBO();
	$query = "select count(id) from `#__billing_sub_group` where sub_id = $sub_id and group_id = $group_id";
	$result = $db->setQuery($query);   
	if($db->loadResult() > 0)
	{
		return true;
	}
	return false;
}

function EditSubscription($id = '')
{
   if ($id != '')
   {
	$db = JFactory::getDBO();
	$query = 'select * from `#__billing_subscription_type` where id = ' . $id;
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();
	$subscription_type = $row['subscription_type'];
	$subscription_days = $row['subscription_days'];
	$price = $row['price'];
	$subscription_description = $row['subscription_description'];
	$alarm_text = $row['alarm_text'];
	$alarm_1 = $row['alarm_1'];
	$alarm_2 = $row['alarm_2'];
	$alarm_3 = $row['alarm_3'];
	$tag = $row['data1'];
	$prolong = $row['data2'];
	
   }
   else
   {
	$subscription_type = JRequest::getVar('subscription_type');
	$subscription_days = JRequest::getVar('subscription_days');
	$price = JRequest::getVar('price');
	$subscription_description = '';
	$alarm_text = JText::_('COM_BILLING_ALARM');
	$alarm_1 = 0;
	$alarm_2 = 0;
	$alarm_3 = 0;
	$tag = 'paid';
	$prolong = 'false';
   }

	$editor = JFactory::getEditor();
	$editor2 = JFactory::getEditor();
	$db = JFactory::getDBO();			


	echo '<table width="100%" border="0"><tr><td valign="top" width="45%">';		
	echo '<h1>'.JText::_('COM_BILLING_SUB_PARAMS').'</h1>';	
	echo "<form name='subs' method='post' action='index.php?option=com_billing&task=savesub&id=$id'>";
		   echo '<table>';
		   echo "<tr><td>".JText::_('COM_BILLING_SUB_TITLE')."</td><td><input class='inputbox' type='text' name='subscription_type' value='$subscription_type' size=80></td></tr>";
		   echo "<tr><td>".JText::_('COM_BILLING_DAYS_COUNT')."</td><td><input class='inputbox' type='text' name='subscription_days' value='$subscription_days' size='5'></td></tr>";
			if ($prolong == 'true')
			{
				$checked = 'checked';
			}
			else
			{
				$checked = '';
				$prolong = 'false';
			}
		   echo "<tr><td>".JText::_('COM_BILLING_DAYS_PROLONG')."</td><td><input type='checkbox' id='prolong' name='prolong' value='$prolong' $checked onclick=\"document.getElementById('prolong').value = document.getElementById('prolong').checked;\"></td></tr>";
		   echo "<tr><td>".JText::_('COM_BILLING_SUB_COST')."</td><td><input class='inputbox' type='text' name='price' value='$price' size='5'> ".GetDefaultCurrencyAbbr()."</td></tr>";
		   echo "<tr><td>".JText::_('COM_BILLING_DAYS_ALARM1')."</td><td><input class='inputbox' type='text' name='alarm_1' value='$alarm_1' size='5'></td></tr>";
		   echo "<tr><td>".JText::_('COM_BILLING_DAYS_ALARM2')."</td><td><input class='inputbox' type='text' name='alarm_2' value='$alarm_2' size='5'></td></tr>";
		   echo "<tr><td>".JText::_('COM_BILLING_DAYS_ALARM3')."</td><td><input class='inputbox' type='text' name='alarm_3' value='$alarm_3' size='5'></td></tr>";
		   //echo "<tr><td>".JText::_('COM_BILLING_SUB_USE_TAG')."</td><td><input class='inputbox' type='text' name='tag' value='$tag'></td></tr>";
		   echo "<tr><td>".JText::_('COM_BILLING_SUB_EMAIL')."</td><td>".$editor->display( 'alarm_text', $alarm_text, '200', '140', '20', '20', false)."</td></tr>";
		   echo "<tr><td>".JText::_('COM_BILLING_SUB_DESCRIPTION')."</td><td>".$editor2->display( 'subscription_description', $subscription_description, '200', '140', '20', '20', false)."</td></tr>";
		  
		   echo '</table>';
		   
		if(JoomlaVersion() > '1.5' and $id != '')
		{
			$query = "select * from `#__usergroups` order by id";
			$result = $db->setQuery($query);   
			$rows = $db->loadObjectList();
	
			echo JText::_('COM_BILLING_THIS_SUB_ACCESS')." <br><br>";
			echo "<table border='0'>";
			foreach($rows as $row)
			{
				echo "<tr>";
				if(SubGroup($id, $row->id))
				{
					echo "<td><input type='checkbox' name='cb_$row->id' value='true' onclick='this.value = this.checked;' checked='true' style='float:none;'> </td><td> $row->title</td>";
				}
				else
				{
					echo "<td><input type='checkbox' name='cb_$row->id' value='false' onclick='this.value = this.checked;' style='float:none;'> </td><td> $row->title</td>";
				}
				echo "</tr>";
			}
			echo "</table>";
		}		   
	echo '<table width="100%" border="0"><tr><td valign="top" width="45%">';
	echo "<tr><td WIDTH='45%'></td><td align='right'><p align='right'><input type='submit' value='".JText::_('COM_BILLING_SAVE')."'></p></td></tr>";	
	echo "</table>";
	echo '</form>';
	
	   
	echo '</td><td width="5%"></td><td valign="top">';
	
	if ($id != '')
	{
	
			echo '<h1>'.JText::_('COM_BILLING_SUB_ITEMS').'</h1>';	
			echo "<form name='subs' method='post' action='index.php?option=com_billing&task=addsubitem&id=$id'>";
			echo "<select name='item_type'>";
			echo "	<option value='1'>".JText::_('COM_BILLING_ARTICLE')."</option>";
			echo "	<option value='2'>".JText::_('COM_BILLING_FILE')."</option>";
			echo "	<option value='3'>".JText::_('COM_BILLING_MP3')."</option>";
			echo "	<option value='4'>".JText::_('COM_BILLING_COMPONENT')."</option>";
			if (JoomlaVersion() == '1.5')
			{
				echo "	<option value='6'>".JText::_('COM_BILLING_SECTION')."</option>";
			}
			echo "	<option value='7'>".JText::_('COM_BILLING_CATEGORY')."</option>";
			echo "	<option value='8'>".JText::_('COM_BILLING_SERVICE')."</option>";
			if (JoomlaVersion() > '1.6')
			{
				echo "	<option value='9'>".JText::_('COM_BILLING_USERGROUP')."</option>";
			}
			echo "	<option value='10'>".JText::_('COM_BILLING_CUSTOM_ITEM')."</option>";
			echo "	<option value='11'>".JText::_('COM_BILLING_K2_ITEM')."</option>";
			echo "	<option value='12'>".JText::_('COM_BILLING_JS_GROUP')."</option>";
			echo "	<option value='13'>".JText::_('COM_BILLING_K2_CATEGORY')."</option>";
			echo "	<option value='14'>".JText::_('COM_BILLING_USERGROUP')." Billing</option>";
			echo "	<option value='15'>".JText::_('COM_BILLING_ADS_CATEGORY')." Billing</option>";
			echo "	<option value='16'>".JText::_('COM_BILLING_K2_FILES')." Billing</option>";
			echo "	<option value='17'>".JText::_('COM_BILLING_K2_ITEM_FILES')."</option>";
			echo "</select> ";
			echo "<input type='submit' value='".JText::_('COM_BILLING_ADD')."'>";
			echo "</form><br>";
			echo '<table  width="100%" class="adminlist table" cellspacing="1">';
			echo '<tr><td>'.JText::_('COM_BILLING_TITLE').'</td><td>'.JText::_('COM_BILLING_ITEM_TYPE').'</td><td>'.JText::_('COM_BILLING_DESCRIPTION').'</td><td></td></tr>';
			
			$query = 'select * from `#__billing_subscription_access` where subscription_id = ' . $id;
			$result = $db->setQuery($query);   
			$rows = $db->loadObjectList(); 	
			if (count($rows) == 0)
			{
				echo '<tr><td align="center">'.JText::_('COM_BILLING_NO_OBJECTS').'</td><td></td><td></td><td></td></tr>';
			}
			else
			{
				foreach ( $rows as $row ) 
				{
					echo '<tr>';
					switch($row->item_type)
					{
						case 1:
							echo "   <td>". GetTitle($row->item_id)."</td>";
						break;
						case 2:
							echo "   <td>". basename($row->file_path) ."</td>";
						break;
						case 3:
							echo "   <td>". basename($row->file_path) ."</td>";
						break;
						case 4:
							echo "   <td>". basename($row->file_path) ."</td>";
						break;
						case 5:
							echo "   <td>". basename($row->file_path) ."</td>"; //video
						break;
						case 6:
							echo "   <td>". GetSectionName($row->item_id) ."</td>";
						break;
						case 7:
							echo "   <td>". GetCategoryName($row->item_id) ."</td>";
						break;
						case 8:
							echo "   <td>". GetServiceName($row->item_id) ."</td>";
						break;
						case 9:
							echo "   <td>". GetUserGroupName($row->item_id) ."</td>";
						break;
						case 10:
							echo "   <td>". GetCustomItemName($row->id, $row->item_id) ."</td>";
						break;
						case 11:
							echo "   <td>". GetK2ItemName($row->item_id) ."</td>";
						break;
						case 12:
							echo "   <td>". GetJSGroupName($row->item_id) ."</td>";
						break;
						case 13:
							echo "   <td>". GetK2CategoryName($row->item_id) ."</td>";
						break;
						case 14:
							echo "   <td>". GetBillingUserGroup($row->item_id) ."</td>";
						break;
						case 15:
							echo "   <td>". GetAdsManagerCategory($row->item_id) ."</td>";
						break;
						case 16:
							echo "   <td>". GetK2CategoryName($row->item_id) ."</td>";
						break;
						case 17:
							echo "   <td>". GetK2ItemName($row->item_id) ."</td>";
						break;
					}
					switch($row->item_type)
					{
						case 1:
							echo "   <td>".JText::_('COM_BILLING_ARTICLE')."</td>";
						break;
						case 2:
							echo "   <td>".JText::_('COM_BILLING_FILE')."</td>";
						break;
						case 3:
							echo "   <td>".JText::_('COM_BILLING_MP3')."</td>";
						break;
						case 4:
							echo "   <td>".JText::_('COM_BILLING_COMPONENT')."</td>";
						break;
						case 5:
							echo "   <td>".JText::_('COM_BILLING_VIDEO')."</td>";
						break;
						case 6:
							echo "   <td>".JText::_('COM_BILLING_SECTION')."</td>";
						break;
						case 7:
							echo "   <td>".JText::_('COM_BILLING_CATEGORY')."</td>";
						break;
						case 8:
							echo "   <td>".JText::_('COM_BILLING_SERVICE')."</td>";
						break;
						case 9:
							echo "   <td>".JText::_('COM_BILLING_USERGROUP')."</td>";
						break;
						case 10:
							echo "   <td>".JText::_('COM_BILLING_CUSTOM_ITEM')."</td>";
						break;
						case 11:
							echo "   <td>".JText::_('COM_BILLING_K2_ITEM')."</td>";
						break;
						case 12:
							echo "   <td>".JText::_('COM_BILLING_JS_GROUP')."</td>";
						break;
						case 13:
							echo "   <td>".JText::_('COM_BILLING_K2_CATEGORY')."</td>";
						break;
						case 14:
							echo "   <td>".JText::_('COM_BILLING_USERGROUP')." Billing</td>";
						break;
						case 15:
							echo "   <td>".JText::_('COM_BILLING_ADS_CATEGORY')." Billing</td>";
						break;
						case 16:
							echo "   <td>".JText::_('COM_BILLING_K2_FILES')." Billing</td>";
						break;
						case 17:
							echo "   <td>".JText::_('COM_BILLING_K2_ITEM_FILES')."</td>";
						break;
						}
					echo "   <td>$row->description</td>";
					echo "   <td><a href='index.php?option=com_billing&task=delsubitem&id=$row->id&sid=$id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_DELETE')."' title='".JText::_('COM_BILLING_DELETE')."' src='".publish_x."'></a></td>";
					echo '</tr>';
				}
			}
			
		echo '</table>';
	
		echo '<br><br><h2>'.JText::_('COM_BILLING_SUB_LIST').' '. Tooltip(JText::_('COM_BILLING_SUB_LIST_DESC')) .'</h2>';	
		$query = 'select * from `#__billing_subscription_list` where subscription_id = ' . $id . ' order by title';
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList(); 	
		echo '<table width="100%" class="adminlist table" cellspacing="1">';
		echo "<tr><td>".JText::_('COM_BILLING_TITLE')."</td><td>".JText::_('COM_BILLING_DELETE')."</td></tr>"; //<td>".JText::_('COM_BILLING_COST')."</td>
		if(count($rows) > 0)
		{
			foreach ( $rows as $row ) 
			{
				echo "<tr><td>$row->title</td><td><a href='index.php?option=com_billing&task=delsublist&id=$row->id&sid=$id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_DELETE')."' title='".JText::_('COM_BILLING_DELETE')."' src='".publish_x."'></a></td></tr>";//<td>$row->price</td>
			}
		}		
		echo '</table>';
		echo "<form name='subs2' method='post' action='index.php?option=com_billing&task=savesublist&id=$id'>";
		echo '<table>';
		echo "<tr><td>".JText::_('COM_BILLING_TITLE')."</td><td><input class='inputbox' type='text' name='title' value=''></td></tr>";
		//echo "<tr><td>".JText::_('COM_BILLING_SUB_PERIOD_COST')."</td><td><input class='inputbox' type='text' name='price' value=''> ".GetDefaultCurrencyAbbr()."</td></tr>";
		echo "<tr><td></td><td align='right'><p align='right'><input type='submit' value='".JText::_('COM_BILLING_ADD')."'></p></td></tr>";
		echo '</table>';
		echo '</form>';

		echo '<br><br><h2>'.JText::_('COM_BILLING_SUB_PERIODS').' '. Tooltip(JText::_('COM_BILLING_SUB_PERIOD_DESC')) .'</h2>';	
		$query = 'select * from `#__billing_subscription_period` where subscription_id = ' . $id . ' order by days';
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList(); 	
		echo '<table width="100%" class="adminlist table" cellspacing="1">';
		echo "<tr><td>".JText::_('COM_BILLING_SUB_PERIOD_DAYS')."</td><td>".JText::_('COM_BILLING_COST')."</td><td>".JText::_('COM_BILLING_DELETE')."</td></tr>";
		if(count($rows) > 0)
		{
			foreach ( $rows as $row ) 
			{
				echo "<tr><td>$row->days</td><td>$row->price</td><td><a href='index.php?option=com_billing&task=delsubitemper&id=$row->id&sid=$id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_DELETE')."' title='".JText::_('COM_BILLING_DELETE')."' src='".publish_x."'></a></td></tr>";
			}	   
		}
		echo '</table>';
		echo "<form name='subs2' method='post' action='index.php?option=com_billing&task=savesubper&id=$id'>";
		echo '<table>';
		echo "<tr><td>".JText::_('COM_BILLING_DAYS_COUNT')."</td><td><input class='inputbox' type='text' name='subscription_days' value=''></td></tr>";
		echo "<tr><td>".JText::_('COM_BILLING_SUB_PERIOD_COST')."</td><td><input class='inputbox' type='text' name='price' value=''> ".GetDefaultCurrencyAbbr()."</td></tr>";
		echo "<tr><td></td><td align='right'><p align='right'><input class='inputbox' type='submit' value='".JText::_('COM_BILLING_ADD')."'></p></td></tr>";
		echo '</table>';
		echo '</form>';
	}
	else
	{
		echo '<p>'.JText::_('COM_BILLING_SUB_YOU_CAN_ADD').'</p>';
	}
    echo '</td></tr></table>';
   
   
   if ($id != '')
   {    
		$query = 'select bs.*, (select name from `#__users` where id = bs.uid) as name, '.
			'(select email from `#__users` where id = bs.uid) as email, '.
			'(select username from `#__users` where id = bs.uid) as username '.
			'from `#__billing_subscription` bs where subscription_id = ' . $id;
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList(); 	
		$n = 1;
			
		echo '<h1>'.JText::_('COM_BILLING_SUB_USERS').'</h1>';		
			
		echo '<table width="100%" class="adminlist table" cellspacing="1">';
		echo "<tr>";
		echo "  <th class='title'>Id</th>";
		echo "  <th class='title'>".JText::_('COM_BILLING_USER')."</th>";
		echo "  <th class='title'>".JText::_('COM_BILLING_COMMENT')."</th>";
		echo "  <th class='title'>".JText::_('COM_BILLING_SUB_EXPIRED')."</th>";
		echo "  <th class='title'>".JText::_('COM_BILLING_SUB_FROZEN')."</th>";
		echo "  <th class='title'>".JText::_('COM_BILLING_DELETE')."</th>";
		echo "</tr>";
			
		if (count($rows) > 0)
		{
			foreach ( $rows as $row ) 
			{
				echo '<tr>';
				echo "<td>$row->id</td>";
				echo "<td>$row->name ($row->username) [<a href='mailto:$row->email'>$row->email</a>]</td>";
				echo "<td>$row->data3</td>";
				if (date('Y-m-d') > $row->enddate)
				{
					echo "<td style='background-color:red; color: white;'>". FormatBillingDate($row->enddate)."</td>";
				}
				else
				{
					echo "<td>". FormatBillingDate($row->enddate)."</td>";
				}
				if($row->frozen == 1) //TODO: freeze action
				{
					echo "<td><a href='index.php?option=com_billing&task=freeze&id=$id&uid=$row->uid'>".JText::_('COM_BILLING_SUB_UNFREEZE')."</a></td>";
				}
				else
				{
					echo "<td><a href='index.php?option=com_billing&task=freeze&id=$id&uid=$row->uid'>".JText::_('COM_BILLING_SUB_FREEZE')."</a></td>";
				}
				echo "<td><a href='index.php?option=com_billing&task=delusersub&id=$id&uid=$row->uid'>".JText::_('COM_BILLING_UNSUB')."</a></td>";
				echo '</tr>';
			}
		}
		  echo '</table><br><hr>';
		  
		  echo '<h2>'.JText::_('COM_BILLING_SUB_USER').'</h2>';
		  
		  echo '<form action="index.php?option=com_billing&task=addusersub&id='.$id.'" method="post">';		
		  echo JText::_('COM_BILLING_USERNAME').' : ' . GetUserList();
		  echo JText::_('COM_BILLING_DAYS_COUNT').' : <input class="inputbox" type="text" name="days" value="30" size="20"> ';
		  echo ' <input type="submit" name="sb" value="'.JText::_('COM_BILLING_SUB_RENEW').'">';
		  echo '</form>';
	}
}

function SaveSubscription($id = '')
{
	AdminLog("Saving $id");
	$subscription_type = JRequest::getVar('subscription_type');
	$subscription_days = JRequest::getInt('subscription_days', 0);
	$price = JRequest::getFloat('price', 0.0);
	$alarm_1 = JRequest::getInt('alarm_1', 0);
	$alarm_2 = JRequest::getInt('alarm_2', 0);
	$alarm_3 = JRequest::getInt('alarm_3', 0);
	$tag = JRequest::getVar('tag');
	$prolong = JRequest::getVar('prolong');
	$data = JRequest::get( 'post');
	$subscription_description = JRequest::getVar( 'subscription_description', '', 'post', 'string', JREQUEST_ALLOWHTML );
	$subscription_description = addslashes($subscription_description);
	$alarm_text = JRequest::getVar( 'alarm_text', '', 'post', 'string', JREQUEST_ALLOWHTML );
	$alarm_text = addslashes($alarm_text);
	$db = JFactory::getDBO();
	
	if ($id != '')
	{
	    $query = "update `#__billing_subscription_type` set subscription_type = '$subscription_type', subscription_days = $subscription_days, ".
	    "price = $price, alarm_1 = $alarm_1, alarm_2 = $alarm_2, alarm_3 = $alarm_3, subscription_description = '$subscription_description', alarm_text = '$alarm_text', data1 = '$tag', data2 = '$prolong' where id = ". $id;
	}
	else
	{
	    $query = "insert into `#__billing_subscription_type` (subscription_type, subscription_days, price, alarm_1, alarm_2, alarm_3, subscription_description, alarm_text, data1, data2) ".
	    "values('$subscription_type', $subscription_days, $price, $alarm_1, $alarm_2, $alarm_3, '$subscription_description', '$alarm_text', '$tag', '$prolong')";
	}
	$result = $db->setQuery($query);
	$result = $db->query();   
	AdminLog("saved");

	if ($id != '')
	{
		$query = "delete from `#__billing_sub_group` where sub_id = $id";
		$result = $db->setQuery($query);   
		$result = $db->query();
		
		$query = "select * from `#__usergroups` order by id";
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList();
		AdminLog('Groups query: '. $query);	
		foreach($rows as $row)
		{	
			$val = JRequest::getVar("cb_$row->id");
			AdminLog('Checkbox: '. "cb_$row->id = $val");	
			if( $val == true)
			{
				$query = "insert into `#__billing_sub_group` (sub_id, group_id) values ($id, $row->id)";
				AdminLog('Insert query: '. $query);	
				$result = $db->setQuery($query);   
				$result = $db->query();
			}
		}
	}
}

function AddSubscription($id, $uid, $days)
{
   $UA = new UserAccount;
   $UA->AddSubscription($uid, $days, $id, true);
}

function DelUserSubscription($id, $uid)
{
	$db = JFactory::getDBO();
	$query = "delete from `#__billing_subscription` where subscription_id = $id and uid = $uid";
	$result = $db->setQuery($query);
	$result = $db->query();   	
}

function DeleteSubItem($id)
{
	$db = JFactory::getDBO();

	$query = "select * from `#__billing_subscription_access` where id = $id";
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();
	switch ($id)
	{
		case 1:
		break;
		case 2:
			unlink($row['file_path']);
		break;
		case 3:
			unlink($row['file_path']);
		break;
		case 4:
		break;
		case 5:
			unlink($row['file_path']);
		break;
		case 6:
		break;
		case 7:
		break;
		case 8:
		break;
		case 9:
		break;
		case 10:
		break;
		case 11:
		break;
		case 12:
		break;
		case 13:
		break;
		case 14:
		break;
		case 15:
		break;
	}

	
	$query = "delete from `#__billing_subscription_access` where id = $id";
	$result = $db->setQuery($query);
	$result = $db->query();	
}

function AddSubItem($id)
{
	$item_type = JRequest::getCmd('item_type');  
	echo "<form action='index.php?option=com_billing&task=saveitem&id=$id' method='post' name='saveitem' enctype='multipart/form-data'>";
	echo "<input type='hidden' name='item_type' value='$item_type'>";
	echo '<table>';
	switch ($item_type)
	{
		case 1:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_ARTICLE')."</td><td>".GetContentList()."</td></tr>";
		break;
		case 2:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_FILE')."</td><td><input class='inputbox' type='file' name='filename' value='' size='80'></td></tr>";
		break;
		case 3:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_MP3')."</td><td><input class='inputbox' type='file' name='filename' value='' size='80'></td></tr>";
			echo "<p><strong>".JText::_('COM_BILLING_MP3_MODULE')."</strong> <a href='http://extensions.joomla.org/extensions/multimedia/audio-players-a-gallery/8338' target='_blank'>Simple MP3 Player</a>. ".JText::_('COM_BILLING_MP3_MODULE2')."</p>";
		break;
		case 4:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_COMPONENT')."</td><td>".GetComponentList()."</td></tr>";
		break;
		case 5:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_VIDEO')."</td><td><input class='inputbox' type='file' name='filename' value='' size='80'></td></tr>";
			echo "<p><strong>".JText::_('COM_BILLING_VIDEO_MODULE')."</strong> <a href='http://extensions.joomla.org/extensions/multimedia/video-players-a-gallery/3955' target='_blank'>AllVideos Reloaded</a>.</p>";
		break;
		case 6:
			if (JoomlaVersion() == '1.5')
			{
				echo "<tr><td>".JText::_('COM_BILLING_SELECT_SECTION')."</td><td>".GetSectionList()."</td></tr>";			
			}
		break;
		case 7:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_CATEGORY')."</td><td>".GetCategoryList()."</td></tr>";						
		break;
		case 8:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_SERVICE')."</td><td>".GetServiceList()."</td></tr>";						
		break;
		case 9:
			if (JoomlaVersion() > '1.6')
			{
				echo "<tr><td>".JText::_('COM_BILLING_SELECT_USERGROUP')."</td><td>".GetUserGroupList()."</td></tr>";	
			}
		break;
		case 10:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_TABLE')."</td><td>".GetTablesList()." ".JText::_('COM_BILLING_FIELD')." <input class='inputbox' type='text' name='field' value=''> ".JText::_('COM_BILLING_FIELD_ID')." <input class='inputbox' type='text' name='field_id' value=''> ".JText::_('COM_BILLING_ITEM_ID')." <input class='inputbox' type='text' name='item_id' value=''></td></tr>";
		break;
		case 11:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_ARTICLE')."</td><td>".GetK2ContentList()."</td></tr>";
		break;
		case 12:	
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_USERGROUP')."</td><td>".GetJSGroupList()."</td></tr>";	
		break;
		case 13:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_CATEGORY')."</td><td>".GetK2CategoryList()."</td></tr>";
		break;
		case 14:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_USERGROUP')."</td><td>".GetUserBillingGroupList()."</td></tr>";
		break;
		case 15:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_CATEGORY')."</td><td>".GetAdsCategoryList()."</td></tr>";
		break;
		case 16:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_CATEGORY')."</td><td>".GetK2CategoryList()."</td></tr>";
		break;
		case 17:
			echo "<tr><td>".JText::_('COM_BILLING_SELECT_ARTICLE')."</td><td>".GetK2ContentList()."</td></tr>";
		break;
	}
	echo "<tr><td>".JText::_('COM_BILLING_DESCRIPTION')."</td><td><input class='inputbox' type='text' name='description' value='' size='80'></td></tr>";
	echo "<tr><td><input type='submit' value='".JText::_('COM_BILLING_ADD')."'></td><td><input type='button' value='".JText::_('CANCEL')."' onclick='javascript:history.back();'></td></tr>";
	echo '</table>';
	echo "</form>";
}

function SaveItem($id)
{
	$item_type = JRequest::getCmd('item_type');
	$content_id = JRequest::getCmd('content_id');
	$description = JRequest::getVar('description');
	$description = addslashes($description);
	$db = JFactory::getDBO();
	switch ($item_type)
	{
		case 1:
			$query = "select count(*) from `#__billing_subscription_access` where subscription_id = $id and item_type = $item_type and item_id = $content_id";
			$result = $db->setQuery($query);
			$n = $db->loadResult();
			if ($n == 0)
			{
				$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description) values ($id, $item_type, $content_id, '$description')";
				$result = $db->setQuery($query);
				$n = $db->query();
//				HideMaterial($content_id);
			}
		break;
		case 2:
			$tmp_name = $_FILES['filename']['tmp_name'];
			$source = $_FILES['filename']['name'];
			$path_info = pathinfo($source);
			$ext = $path_info['extension'];
			$basename = $path_info['basename'];
			$unq = md5(uniqid(rand()));
			$filename = JPATH_ROOT . '/components/com_billing/files/' . $basename .'_' . $unq . '.' .  $ext;
			move_uploaded_file($tmp_name, $filename);
			$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description, file_path, data1) values ($id, $item_type, 0, '$description', '$filename', '$source')";
			$result = $db->setQuery($query);
			$n = $db->query();
		break;
		case 3:
			$tmp_name = $_FILES['filename']['tmp_name'];
			$source = $_FILES['filename']['name'];
			$path_info = pathinfo($source);
			$ext = $path_info['extension'];
			$basename = $path_info['basename'];
			$unq = md5(uniqid(rand()));
			$filename = JPATH_ROOT . '/components/com_billing/files/' . $basename .'_' . $unq . '.' .  $ext;
			move_uploaded_file($tmp_name, $filename);
			$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description, file_path, data1) values ($id, $item_type, 0, '$description', '$filename', '$source')";
			$result = $db->setQuery($query);
			$n = $db->query();
		break;
		case 4:
			$component_id = JRequest::getCmd('component_id');
			if (JoomlaVersion() == '1.5')
			{
				$query = 'select * from `#__components` where id = '. $component_id;
				$result = $db->setQuery($query);   
				$row = $db->loadAssoc();
				$filename = $row['option'];
				$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description, file_path) values ($id, $item_type, $component_id, '$description', '$filename')";
				$result = $db->setQuery($query);
				$n = $db->query();
			}
			else
			{
				$query = 'select * from `#__extensions` where extension_id = '. $component_id;
				$result = $db->setQuery($query);   
				$row = $db->loadAssoc();
				$filename = $row['name'];
				$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description, file_path) values ($id, $item_type, $component_id, '$description', '$filename')";
				$result = $db->setQuery($query);
				$n = $db->query();
			}
		break;
		case 5:
			$tmp_name = $_FILES['filename']['tmp_name'];
			$source = $_FILES['filename']['name'];
			$path_info = pathinfo($source);
			$ext = $path_info['extension'];
			$basename = $path_info['basename'];
			$unq = md5(uniqid(rand()));
			$filename = JPATH_ROOT . '/components/com_billing/video/' . $basename .'_' . $unq . '.' .  $ext;
			move_uploaded_file($tmp_name, $filename);
			$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description, file_path) values ($id, $item_type, 0, '$description', '$filename')";
			$result = $db->setQuery($query);
			$n = $db->query();
		break;		
		case 6:
			if (JoomlaVersion() == '1.5')
			{
				$section_id = JRequest::getCmd('section_id');
				$filename = GetSectionName($section_id);
				$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description, file_path) values ($id, $item_type, $section_id, '$description', '$filename')";
				$result = $db->setQuery($query);
				$n = $db->query();
			}
		break;		
		case 7:
			$category_id = JRequest::getCmd('category_id');
			$filename = GetCategoryName($category_id);
			$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description, file_path) values ($id, $item_type, $category_id, '$description', '$filename')";
			$result = $db->setQuery($query);
			$n = $db->query();
		break;		
		case 8:
			$service_id = JRequest::getCmd('service_id');
			$filename = GetServiceName($service_id);
			$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description, file_path) values ($id, $item_type, $service_id, '$description', '$filename')";
			$result = $db->setQuery($query);
			$n = $db->query();
		break;
		case 9:
			$group_id = JRequest::getCmd('group_id');
			$query = "select count(*) from `#__billing_subscription_access` where subscription_id = $id and item_type = $item_type and item_id = $group_id";
			$result = $db->setQuery($query);
			$n = $db->loadResult();
			if($n > 0)
			{
				JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLING_UGALREADYEXISTS') );
			}
			else
			{
				$filename = GetUserGroupName($group_id);
				$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description, file_path) values ($id, $item_type, $group_id, '$description', '$filename')";
				$result = $db->setQuery($query);
				$n = $db->query();
				$UA = new UserAccount();
				$UA->UpdateGroupSub($id);
			}
		break;
		case 10:
			$field = JRequest::getCmd('field');
			$field_id = JRequest::getCmd('field_id');
			$item_id = JRequest::getCmd('item_id');
			$table = JRequest::getCmd('table');
			$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description, file_path, data3, data4) values ($id, $item_type, $item_id, '$description', '$field', '$field_id', '$table')";
			$result = $db->setQuery($query);
			$n = $db->query();
		break;
		case 11:
			$query = "select count(*) from `#__billing_subscription_access` where subscription_id = $id and item_type = $item_type and item_id = $content_id";
			$result = $db->setQuery($query);
			$n = $db->loadResult();
			if ($n == 0)
			{
				$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description) values ($id, $item_type, $content_id, '$description')";
				$result = $db->setQuery($query);
				$n = $db->query();
			}
		break;
		case 12:
			$content_id = JRequest::getInt('jsgroup_id');
			$query = "select count(*) from `#__billing_subscription_access` where subscription_id = $id and item_type = $item_type and item_id = $content_id";
			$result = $db->setQuery($query);
			$n = $db->loadResult();
			if ($n == 0)
			{
				$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description) values ($id, $item_type, $content_id, '$description')";
				$result = $db->setQuery($query);
				$n = $db->query();
			}
		break;
		case 13:
			$content_id = JRequest::getInt('k2catid_id');
			$query = "select count(*) from `#__billing_subscription_access` where subscription_id = $id and item_type = $item_type and item_id = $content_id";
			$result = $db->setQuery($query);
			$n = $db->loadResult();
			if ($n == 0)
			{
				$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description) values ($id, $item_type, $content_id, '$description')";
				$result = $db->setQuery($query);
				$n = $db->query();
			}
		break;
		case 14:
			$group_id = JRequest::getCmd('group_id');
			$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description) values ($id, $item_type, $group_id, '$description')";
			$result = $db->setQuery($query);
			$n = $db->query();
		break;
		case 15:
			$ads_id = JRequest::getCmd('ads_id');
			$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description) values ($id, $item_type, $ads_id, '$description')";
			$result = $db->setQuery($query);
			$n = $db->query();
		break;		
		case 16:
			$content_id = JRequest::getInt('k2catid_id');
			$query = "select count(*) from `#__billing_subscription_access` where subscription_id = $id and item_type = $item_type and item_id = $content_id";
			$result = $db->setQuery($query);
			$n = $db->loadResult();
			if ($n == 0)
			{
				$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description) values ($id, $item_type, $content_id, '$description')";
				$result = $db->setQuery($query);
				$n = $db->query();
			}
		break;		
		case 17:
			$query = "select count(*) from `#__billing_subscription_access` where subscription_id = $id and item_type = $item_type and item_id = $content_id";
			$result = $db->setQuery($query);
			$n = $db->loadResult();
			if ($n == 0)
			{
				$query = "insert into `#__billing_subscription_access` (subscription_id, item_type, item_id, description) values ($id, $item_type, $content_id, '$description')";
				$result = $db->setQuery($query);
				$n = $db->query();
			}
		break;
	}
}

function ShowFilter($search, $Section)
{
	echo '<table border="0">';
	echo '	<tr>';
	echo '		<td>'.JText::_('COM_BILLING_FILTER').':
						<input class="inputbox" name="search" id="search" value="'.$search.'" class="text_area" onchange="document.adminForm.submit();" title="'.JText::_('COM_BILLING_FILTER_TITLE_ID').'" type="text"><button onclick="this.form.getElementById(\'task\').value=\'articles\';this.form.submit();">'.JText::_('COM_BILLING_APPLY').'</button>
						<button onclick="document.getElementById(\'search\').value=\'\';this.form.getElementById(\'filter_sectionid\').value=\'-1\';this.form.getElementById(\'catid\').value=\'0\';this.form.submit();">'.JText::_('COM_BILLING_RESET').'</button></td>';
	$db = JFactory::getDBO();
	echo '<td>';
	echo JText::_('COM_BILLING_SECTION') . ': <select id="filter_sectionid" name="Section" onchange="document.adminForm.submit();">';
	if (JoomlaVersion() == '1.5')
	{
		$query = 'select * from `#__sections` order by title';
	}
	else
	{
		$query = "select * from `#__categories` where extension = 'com_content' order by title";
	}
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList();    
	$selected = 0;
	foreach ( $rows as $row ) 
	{
		if ($Section == $row->id)
		{
			echo "<option value='$row->id' selected>$row->title</option>";
			$selected = 1;
		}
		else
		{
			echo "<option value='$row->id'>$row->title</option>";
		}
	}
	if ($selected == 0)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	
	if(K2Enabled())
	{
		$query = "select * from `#__k2_categories` order by name";
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList();
		foreach ( $rows as $row ) 
		{
			if ($Section == 'k_'.$row->id)
			{
				echo "<option value='k_$row->id' selected>$row->name [K2]</option>";
				$selected = 1;
			}
			else
			{
				echo "<option value='k_$row->id'>$row->name [K2]</option>";
			}
		}
	}
	if ($selected == 0)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	
	echo "<option value='-1' $sel>[".JText::_('COM_BILLING_IGNORE_SECTION')."]</option>";
	echo '</select>';
	echo '</td>';
	echo '	</tr>';
	echo '</table>';
}

function TableExists($table)
{
	$db = JFactory::getDBO();
	$db->setQuery("SHOW TABLES LIKE '$table'");
	$db->query();
	$num_rows = $db->getNumRows();
	
	if($num_rows > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
	
	if($n != '')
	{
		return true;
	}
	else
	{
		return false;
	}
}

function K2Enabled()
{
	$db = JFactory::getDBO();
	if (JoomlaVersion() > '1.5')
	{
		$query = "select count(*) from `#__extensions` where name='com_k2' or name='k2'";
	}
	else
	{
		$query = "select count(*) from `#__components` where name='K2' or name='k2'";
	}
	$result = $db->setQuery($query);   
	$n1 = $db->loadResult(); 

	if($n1 > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function ShowArticles($order = '', $Section = '', $Category = '', $p1 = '', $p2 = '', $p3 = '', $search='', $limitstart='', $limit = '')
{
	echo "<input type='hidden' name='search' value='$search'>";
	JToolBarHelper::custom( 'assign', 'assign', 'assign', JText::_('COM_BILLING_FOR_FEE'), false );
	JToolBarHelper::custom( 'suball', 'assign', 'assign', JText::_('COM_BILLING_BY_SUB'), false );
	JToolBarHelper::custom( 'sms', 'assign', 'assign', JText::_('COM_BILLING_BY_SMS'), false );

	$wherek2 = '';
	if ($limitstart == '')
	{
		$limitstart = 0;
	}
   if ($limit == '')
	{
		$limit = 20;
	}
	if ($limitstart == 0 and $limit == 0)
	{
		$lim = '';
	}
	else
	{
		$lim = "limit $limitstart, $limit";
	}
	echo '<h1>'.JText::_('COM_BILLING_ARTICLES').'</h1>';
	
	ShowFilter($search, $Section);	
	
	$where = '';
	if ($search != '')
	{
		$where = " and (title LIKE '%$search%' or id = '$search')";
		$wherek2 = " where (title LIKE '%$search%' or id = '$search')";
		if ($Section != '' and $Section != -1)
		{
			if(K2Enabled() and $Section[0] == 'k')
			{
				$xx = explode('_', $Section);
				$cat_id = $xx[1];
				$wherek2 .= " and k.catid = $cat_id";
			}
			else
			{
				if (JoomlaVersion() == '1.5')
				{
					$where .= ' and sectionid = ' . $Section;
				}
				else
				{
					$where .= ' and catid = ' . $Section;
				}
			}
		}
	}
	else
	{
		if ($Section != '' and $Section != -1)
		{
			if(K2Enabled() and $Section[0] == 'k')
			{
				$xx = explode('_', $Section);
				$cat_id = $xx[1];
				$wherek2 = " where k.catid = $cat_id";
			}
			else
			{
				if (JoomlaVersion() == '1.5')
				{
					$where .= ' and sectionid = ' . $Section;
				}
				else
				{
					$where .= ' and catid = ' . $Section;
				}
			}
		}
		else
		{
			$where = '';
		}
	}
	
	if ($order == '')
	{
		$order = 'title';
	}
	
	$db = JFactory::getDBO();
    $query = 'select count(*) from `#__content` a where state > 0  '. $where;
	$result = $db->setQuery($query);   
	$total = $db->loadResult();
	
	if ($Section[0] == 'k'){
		$q = "select k.id, k.title, k.published as state, 1 as k2, ".
		'(select price from `#__billing_articles` where article_id = k.id and k2 = 1) as price, '.
		'(select data1 from `#__billing_articles` where article_id = k.id and k2 = 1) as counter, '.
		'(select lockbefore from `#__billing_articles` where article_id = k.id and k2 = 1) as lockbefore, '.
		'(select is_active from `#__billing_articles` where article_id = k.id and k2 = 1) as is_active, '.
		'(select counter from `#__billing_articles` where article_id = k.id and k2 = 1) as counter, '.
		'(select is_active from `#__billing_articles_sms` where article_id = k.id and k2 = 1) as sms_active, '.
		'(select count(*) from `#__billing_articles_user` where article_id = k.id and k2 = 1) as pay_count, '.
		'(select count(*) from `#__billing_subscription_access` where item_id = k.id and item_type = 11) as sub '.
		'from `#__k2_items` k ' . $wherek2;
		
		$query = 'select count(*) from `#__k2_items`' . str_replace('k.catid', 'catid', $wherek2);
		//echo $query;
		$result = $db->setQuery($query);   
		$total2 = $db->loadResult();  
		$total = $total2;
		
		$query = $q;
	}
	else {
		if(K2Enabled())
		{
			$k2 = " union select k.id, k.title, k.published as state, 1 as k2, ".
			'(select price from `#__billing_articles` where article_id = k.id and k2 = 1) as price, '.
			'(select data1 from `#__billing_articles` where article_id = k.id and k2 = 1) as counter, '.
			'(select lockbefore from `#__billing_articles` where article_id = k.id and k2 = 1) as lockbefore, '.
			'(select is_active from `#__billing_articles` where article_id = k.id and k2 = 1) as is_active, '.
			'(select counter from `#__billing_articles` where article_id = k.id and k2 = 1) as counter, '.
			'(select is_active from `#__billing_articles_sms` where article_id = k.id and k2 = 1) as sms_active, '.
			'(select count(*) from `#__billing_articles_user` where article_id = k.id and k2 = 1) as pay_count, '.
			'(select count(*) from `#__billing_subscription_access` where item_id = k.id and item_type = 11) as sub '.
			'from `#__k2_items` k ' . $wherek2;
			
			$query = 'select count(*) from `#__k2_items`' . str_replace('k.catid', 'catid', $wherek2);
			$result = $db->setQuery($query);   
			$total2 = $db->loadResult();  
			$total = $total2 + $total;
		}
		else
		{
			$k2 = '';
		}
	    $query = 'select a.id, a.title, a.state, 0 as k2,'.
			'(select price from `#__billing_articles` where article_id = a.id and k2 = 0) as price, '.
			'(select data1 from `#__billing_articles` where article_id = a.id and k2 = 0) as counter, '.
			'(select lockbefore from `#__billing_articles` where article_id = a.id and k2 = 0) as lockbefore, '.
			'(select is_active from `#__billing_articles` where article_id = a.id and k2 = 0) as is_active, '.
			'(select counter from `#__billing_articles` where article_id = a.id and k2 = 0) as counter, '.
			'(select is_active from `#__billing_articles_sms` where article_id = a.id and k2 = 0) as sms_active, '.
			'(select count(*) from `#__billing_articles_user` where article_id = a.id and k2 = 0) as pay_count, '.
			'(select count(*) from `#__billing_subscription_access` where item_id = a.id and item_type = 1) as sub '.
			'from `#__content` a where state >= 0 '.$where. ' ' . $k2;
	    $query .= ' order by ' . $order . " $lim";
	}

	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 	
	
	//echo $query;
	
	echo '<table width="100%" class="adminlist table" cellspacing="1">';
	echo "<tr>";
	echo "  <th class='title'><a href='index.php?option=com_billing&task=articles&order=id&Section=$Section'>Id</a></th>";
	echo '  <th class="title" width="20"><input type="checkbox" id="checkbox" name="toggle" onclick="checkAll(100000);"></th>';
	echo "  <th class='title'><a href='index.php?option=com_billing&task=articles&order=title&Section=$Section'>".JText::_('COM_BILLING_TITLE')."</a></th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_PAYS')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_PUBLISHED')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_COST')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_FOR_SALE')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_AVAILABLE_FOR_SUB')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_AVAILABLE_FOR_SMS')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_HITS')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_CLOSED_UNTIL')."</th>";
	echo "</tr>";
		
	if (count($rows) > 0)	
	{
		foreach ( $rows as $row ) 
		{
			echo '<tr>';
			echo "<td>$row->id</td>";
			echo "<td><input type='checkbox' id='cb$row->id' name='cid[]' value='$row->id' onclick='isChecked(this.checked);'></td>";
			echo "<td><a href='index.php?option=com_billing&task=editart&id=$row->id&k2=$row->k2'>$row->title</a></td>";
			if ($row->pay_count > 0)
			{
				echo "<td><strong>$row->pay_count</strong> (".FormatDefaultCurrency($row->price * $row->pay_count).")</td>";//!!
			}
			else
			{
				echo "<td></td>";
			}
			if($row->state != 0)
			{		
				echo "<td><img width='16' height='16' border='0' src='". tick ."'></td>";
			}
			else
			{
				echo "<td><img width='16' height='16' border='0' src='".publish_x."'></td>";
			}
			if ($row->price != '')
			{
				echo "<td> ".FormatDefaultCurrency($row->price)."</td>";
			}
			else
			{
				echo "<td></td>";
			}
			if($row->is_active == 0)
			{		
				echo "<td><a href='index.php?option=com_billing&task=activeart&id=$row->id&k2=$row->k2'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_MAKE_PAID')."' title='".JText::_('COM_BILLING_MAKE_PAID')."' src='".publish_x."'></a></td>";
			}
			else
			{
				echo "<td><a href='index.php?option=com_billing&task=activeart&id=$row->id&k2=$row->k2'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_MAKE_FREE')."' title='".JText::_('COM_BILLING_MAKE_FREE')."' src='". tick ."'></a></td>";
			}
			if($row->sub != 0)
			{		
				echo "<td><img width='16' height='16' border='0' src='". tick ."'></td>";
			}
			else
			{
				echo "<td><img width='16' height='16' border='0' src='".publish_x."'></td>";
			}
			if($row->sms_active != 0)
			{		
				echo "<td><a href='index.php?option=com_billing&task=activesms&id=$row->id&k2=$row->k2'><img width='16' height='16' border='0' src='". tick ."'></a></td>";
			}
			else
			{
				echo "<td><a href='index.php?option=com_billing&task=activesms&id=$row->id&k2=$row->k2'><img width='16' height='16' border='0' src='".publish_x."'></a></td>";
			}		
			echo "<td>$row->counter</td>";
			echo "<td>$row->lockbefore</td>";
			echo '</tr>';        
		}
	}
		
	echo '</table><hr>';	
	 
	$pagenav = new JPagination ( $total, $limitstart,  $limit);
	echo $pagenav->getListFooter();
}

function EditArticle($id, $_k2 = false)
{
	echo '<h2>'.JText::_('COM_BILLING_ARTICLE_EDIT').'</h2>';
    JHTML::_('behavior.calendar');
		
	$is_k2 = '';
	$k2 = JRequest::getInt('k2', 0);
	if($k2 == 1)
	{
		$is_k2 = ' and k2 = 1 ';
		$k2 = true;
	}
	else
	{
		$k2 = false;
	}
	
	$db = JFactory::getDBO();
	$query = 'select count(*) from `#__billing_articles` where article_id = ' . $id . ' ' . $is_k2;
	$result = $db->setQuery($query);   
	$exists = $db->loadResult(); 
	if($exists > 0)
	{
		$exists = true;
	}
	else
	{
		$exists = false;
	}

	$query = 'select * from `#__billing_articles` where article_id = ' . $id . ' ' . $is_k2;
	$result = $db->setQuery($query);   
	$row = $db->loadAssoc(); 

	if(!$exists and !$k2)
	{
		$query = "select * from `#__content` where id = $id";
		$result = $db->setQuery($query);   
		$article = $db->loadAssoc(); 
		if ($article['fulltext'] != '')
		{
			$annotation = $article['introtext'];
		}
		else
		{
			$annotation = '';
		}
	}
	if(!$exists and $k2)
	{
		$query = "select * from `#__k2_items` where id = $id";
		$result = $db->setQuery($query);   
		$article = $db->loadAssoc(); 
		if ($article['fulltext'] != '')
		{
			$annotation = $article['introtext'];
		}
		else
		{
			$annotation = '';
		}
		
	}
	if($exists and !$k2)
	{
		$annotation = $row['annotation'];
	}

	$editor = JFactory::getEditor();

	echo "<form action='index.php?option=com_billing&task=saveart&id=$id' method='post' name='saveitem' enctype='multipart/form-data'>";
	echo "<input type='hidden' name='k2' value='$k2'>";
	echo '<table>';
	echo "<tr><td>".JText::_('COM_BILLING_SELECT_ARTICLE')."</td><td>".GetContentList($id, 1, $k2)."</td></tr>";
	if (isset($row['price']))
	{
		echo "<tr><td>".JText::_('COM_BILLING_ENTER_PRICE')."</td><td><input class='inputbox' type='text' name='price' value='$row[price]'>".GetDefaultCurrencyAbbr()."</td></tr>";
	}
	else
	{
		echo "<tr><td>".JText::_('COM_BILLING_ENTER_PRICE')."</td><td><input class='inputbox' type='text' name='price' value='10.00'>".GetDefaultCurrencyAbbr()."</td></tr>";
	}
	if ($row['is_active'] == 1)
	{
		echo "<tr><td>".JText::_('COM_BILLING_FOR_FEE')."</td><td><input type='checkbox' name='is_active' value='$row[is_active]' checked></td></tr>";
	}
	else
	{
		echo "<tr><td>".JText::_('COM_BILLING_FOR_FEE')."</td><td><input type='checkbox' name='is_active' value='$row[is_active]'></td></tr>";
	}	
	echo "<tr><td>".JText::_('COM_BILLING_SHOW_TIMES')."</td><td><input class='inputbox' type='text' name='data3' value='".$row['data3']."'></td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_SALE_TIMES')."</td><td><input class='inputbox' type='text' name='counter' value='".$row['counter']."'></td></tr>";
	$cal_var = $row['lockbefore'];
	echo "<tr><td>".JText::_('COM_BILLING_CLOSED_UNTIL')."</td><td>". JHTML::_('calendar', $cal_var, 'cal_var', 'cal_var', '%Y-%m-%d %H:%M:%S', array('class'=>'inputbox', 'size'=>'25',  'maxlength'=>'19')) ."</td></tr>";
	if(GetOption('author'))
	{
		echo "<tr><td>".JText::_('COM_BILLING_AUTHOR')."</td><td>".GetAuthor($id)."</td></tr>";
		echo "<tr><td>".JText::_('COM_BILLING_AUTHOR_PERCENT')."</td><td><input class='inputbox' type='text' name='data2' value='$row[data2]'> %</td></tr>";
	}
	echo "<tr><td>".JText::_('COM_BILLING_ARTICLE_ANN')."</td><td>".$editor->display( 'desc', $annotation, '600', '200', '20', '20', false )."</td></tr>";

	$query = "select * from `#__billing_articles_file` where article_id = $id";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 
	if(count($rows) > 0)
	{
		foreach($rows as $row)
		{
			echo "<tr><td>".JText::_('COM_BILLING_FILE').":</td><td><br><a href='$row->fileurl'>$row->filename</a> <a href='index.php?option=com_billing&task=delfile&id=$row->id'><img src='".publish_x."' border='0' alt='".JText::_('COM_BILLING_DELETE')."' title='".JText::_('COM_BILLING_DELETE')."'></a> ($row->description)</td></tr>";
		}
	}
	if(!$k2)
	{
		echo "<tr><td>".JText::_('COM_BILLING_ADDFILE')."</td><td><br><input class='inputbox' type='file' name='file1'> ".JText::_('COM_BILLING_DESCRIPTION')." <input class='inputbox' type='text' name='desc1' value='' size='100'></td></tr>";
		echo "<tr><td>".JText::_('COM_BILLING_ADDFILE')."</td><td><br><input class='inputbox' type='file' name='file2'> ".JText::_('COM_BILLING_DESCRIPTION')." <input class='inputbox' type='text' name='desc2' value='' size='100'></td></tr>";
		echo "<tr><td>".JText::_('COM_BILLING_ADDFILE')."</td><td><br><input class='inputbox' type='file' name='file3'> ".JText::_('COM_BILLING_DESCRIPTION')." <input class='inputbox' type='text' name='desc3' value='' size='100'></td></tr>";
		echo "<tr><td>".JText::_('COM_BILLING_ADDFILE')."</td><td><br><input class='inputbox' type='file' name='file4'> ".JText::_('COM_BILLING_DESCRIPTION')." <input class='inputbox' type='text' name='desc4' value='' size='100'></td></tr>";
		echo "<tr><td>".JText::_('COM_BILLING_ADDFILE')."</td><td><br><input class='inputbox' type='file' name='file5'> ".JText::_('COM_BILLING_DESCRIPTION')." <input class='inputbox' type='text' name='desc5' value='' size='100'></td></tr>";
	}
	echo "<tr><td></td><td><br><input type='submit' value='".JText::_('COM_BILLING_SAVE')."'></td></tr>";
	echo '</table>';

    
	$query = 'select bs.*, (select name from `#__users` where id = bs.uid) as name, '.
		'(select email from `#__users` where id = bs.uid) as email, '.
		'(select username from `#__users` where id = bs.uid) as username '.
		'from `#__billing_articles_user` bs where article_id = ' . $id;
	if($k2)
	{
		$query .= " and k2 = 1";
	}
	else
	{
		$query .= " and k2 = 0";
	}
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 		
	
	echo '<h1>'.JText::_('COM_BILLING_PAID_USERS').'</h1>';		
	echo '<table width="100%" class="adminlist table" cellspacing="1">';
	echo "<tr>";
	echo "  <th class='title'>Id</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_USER')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_PURCHASE_DATE')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_HITS')."</th>";
	echo "  <th class='title'>".JText::_('COM_BILLING_REVOKE_ACCESS')."</th>";
	echo "</tr>";

	if (count($rows) > 0)
	{
	    foreach ( $rows as $row ) 
	    {
		    echo '<tr>';
		    echo "<td>$row->id</td>";
		    echo "<td>$row->name [<a href='mailto:$row->email'>$row->email</a>]</td>";
			echo "<td>". FormatBillingDate($row->paymentdate)."</td>";
			echo "<td>$row->data1</td>";
		    echo "<td><a href='index.php?option=com_billing&task=deluserart&id=$id&uid=$row->uid'>".JText::_('COM_BILLING_UNSUB')."</a></td>";
		    echo '</tr>';
	    }
	}
	echo '</table><br><hr></form>';
	
	  echo '<h2>'.JText::_('COM_BILLING_SUB_USER').'</h2>';
	  
      echo '<form action="index.php?option=com_billing&task=adduserart&id='.$id.'" method="post">';		
      echo JText::_('COM_BILLING_USERNAME').': ' . GetUserList();
	  echo "<input type='hidden' name='k2' value='".JRequest::getInt('k2', 0)."'>";
      echo ' <input type="submit" name="sb" value="'.JText::_('COM_BILLING_SUB_RENEW').'">';
      echo '</form>';	
}

function SaveArticle($id)
{
	$is_active = JRequest::getVar('is_active');
	$desc1 = JRequest::getVar('desc1');
	$desc2 = JRequest::getVar('desc2');
	$desc3 = JRequest::getVar('desc3');
	$desc4 = JRequest::getVar('desc4');
	$desc5 = JRequest::getVar('desc5');
	$cal_var = JRequest::getVar('cal_var');
	$data3 = JRequest::getInt('data3', 0);
	$counter = JRequest::getInt('counter', 0);
	$data2 = JRequest::getFloat('data2', 0);
	$k2 = JRequest::getBool('k2');
	$UA = new UserAccount;	
	if($_FILES["file1"]["error"] == UPLOAD_ERR_OK)
	{
		$tmp_name = $_FILES['file1']['tmp_name'];
		$file_name = $_FILES['file1']['name'];
		$file_size = $_FILES['file1']['size'];
		$r = $UA->AddFileToArticle($id, $tmp_name, $file_name, $file_size, $desc1);
		//AdminLog('Added file: '. $r);	
	}
	if($_FILES["file2"]["error"] == UPLOAD_ERR_OK)
	{
		$tmp_name = $_FILES['file2']['tmp_name'];
		$file_name = $_FILES['file2']['name'];
		$file_size = $_FILES['file2']['size'];
		$r = $UA->AddFileToArticle($id, $tmp_name, $file_name, $file_size, $desc2);
		//AdminLog('Added file: '. $r);	
	}
	if($_FILES["file3"]["error"] == UPLOAD_ERR_OK)
	{
		$tmp_name = $_FILES['file3']['tmp_name'];
		$file_name = $_FILES['file3']['name'];
		$file_size = $_FILES['file3']['size'];
		$r = $UA->AddFileToArticle($id, $tmp_name, $file_name, $file_size, $desc3);
		//AdminLog('Added file: '. $r);	
	}
	if($_FILES["file4"]["error"] == UPLOAD_ERR_OK)
	{
		$tmp_name = $_FILES['file4']['tmp_name'];
		$file_name = $_FILES['file4']['name'];
		$file_size = $_FILES['file4']['size'];
		$r = $UA->AddFileToArticle($id, $tmp_name, $file_name, $file_size, $desc4);
		//AdminLog('Added file: '. $r);	
	}
	if($_FILES["file5"]["error"] == UPLOAD_ERR_OK)
	{
		$tmp_name = $_FILES['file5']['tmp_name'];
		$file_name = $_FILES['file5']['name'];
		$file_size = $_FILES['file5']['size'];
		$r = $UA->AddFileToArticle($id, $tmp_name, $file_name, $file_size, $desc5);
		//AdminLog('Added file: '. $r);	
	}
	
	if (!isset($is_active))
	{
		$is_active = 0;
	}
	else
	{
		$is_active = 1;
	}
	$price = JRequest::getVar('price');
	$content_id = JRequest::getVar('content_id');
	$desc = JRequest::getVar( 'desc', '', 'post', 'string', JREQUEST_ALLOWHTML );
	$desc = addslashes($desc);
	
	$db = JFactory::getDBO();
	$query = 'select count(*) as c from `#__billing_articles` where article_id = ' . $id;	
	if($k2)
	{
		$query .= " and k2 = 1";
		$k = 1;
	}
	else
	{
		$query .= " and k2 = 0";
		$k = 0;
	}
	$result = $db->setQuery($query);   
	$n = $db->loadResult();
	if ($n == 0)
	{
		$query = "insert into `#__billing_articles` (article_id, annotation, is_active, price, lockbefore, data3, data2, counter, k2) values ($content_id, '$desc', $is_active, $price, '$cal_var', '$data3', '$data2', $counter, $k)";
	}
	else
	{
		$query = "update `#__billing_articles` set annotation = '$desc', is_active = $is_active, price = $price, lockbefore = '$cal_var', data3 = '$data3', data2 = '$data2', counter = $counter, k2 = $k where article_id = $id";
	}
	
	$result = $db->setQuery($query);  
	$result = $db->query();  
	
	$query = "select count(*) from `#__billing_subscription_access` where item_id = $id and item_type = 1";
	$result = $db->setQuery($query);   
	$n = $db->loadResult();
}

function ActivateArticle($id)
{
	$db = JFactory::getDBO();
	$k2 = JRequest::getInt('k2');
	$query = 'select count(*) from `#__billing_articles` where article_id = ' . $id;
	if($k2 == 1)
	{
		$query .= " and k2 = 1";
		$_k2 = true;
	}
	else
	{
		$query .= " and k2 = 0";
		$_k2 = false;
	}	
	$result = $db->setQuery($query);   
	$n = $db->loadResult();
	if ($n == 0)
	{
		EditArticle($id, $_k2);
		return;
	}
	
	$query = 'select is_active from `#__billing_articles` where article_id = ' . $id;
	if($k2 == 1)
	{
		$query .= " and k2 = 1";
	}
	else
	{
		$query .= " and k2 = 0";
	}		
	$result = $db->setQuery($query);   
	$n = $db->loadResult();
	
	if ($n == 1)
	{
		$n = 0;
	}
	else
	{
		$n = 1;
	}
	$query = "update `#__billing_articles` set is_active = $n where article_id = $id";
	if($k2 == 1)
	{
		$query .= " and k2 = 1";
	}
	else
	{
		$query .= " and k2 = 0";
	}	
	$result = $db->setQuery($query);  
	$result = $db->query(); 
	ShowArticles($order, $Section, $Category, $p1, $p2, $p3, $search, $limitstart, $limit);
}

function ActivateSMS($id)
{
	$db = JFactory::getDBO();
	$k2 = JRequest::getInt('k2');
	$query = 'select count(*) as c from `#__billing_articles_sms` where article_id = ' . $id;	
	if($k2 == 1)
	{
		$query .= " and k2 = 1";
	}
	else
	{
		$query .= " and k2 = 0";
	}	
	$result = $db->setQuery($query);   
	$n = $db->loadResult();
	if ($n == 0)
	{
		$query = "insert into `#__billing_articles_sms` (article_id, is_active, k2) values ($id, 0, $k2)";
		$result = $db->setQuery($query);   
		$result = $db->query();
	}
	
	$query = 'select is_active from `#__billing_articles_sms` where article_id = ' . $id;	
	if($k2 == 1)
	{
		$query .= " and k2 = 1";
	}
	else
	{
		$query .= " and k2 = 0";
	}	
	$result = $db->setQuery($query);   
	$n = $db->loadResult();
	
	if ($n == 1)
	{
		$n = 0;
	}
	else
	{
		$n = 1;
	}
	$query = "update `#__billing_articles_sms` set is_active = $n where article_id = $id";
	if($k2 == 1)
	{
		$query .= " and k2 = 1";
	}
	else
	{
		$query .= " and k2 = 0";
	}		
	$result = $db->setQuery($query);  
	$result = $db->query(); 
}

function SubAll()
{
	$db = JFactory::getDBO();
	$query = 'select * from `#__billing_subscription_type` order by id';
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 	
	
	echo "<form name='xx' action='index.php?option=com_billing&task=saveall' method='post'><table>";
	echo '<tr><td>'.JText::_('COM_BILLING_SELECT_SUB').':</td><td> <select name="sub_id">';
	foreach ( $rows as $row ) 
	{	
		echo "<option value='$row->id'>$row->subscription_type</option>";
	}
	echo '</select></td></tr>';
	echo '<tr><td>'.JText::_('COM_BILLING_SELECTED_ARTICLES').':</td><td> <select name=cid[] MULTIPLE size="10">';	
	foreach($_POST['cid'] as $checkbox) 
	{
		$id = $checkbox;
		echo "<option value='$id' selected>". GetTitle($id) ."</option>";
	}
	echo '</select></td></tr>';
	echo '<tr><td></td><td><input value="'.JText::_('COM_BILLING_ADD_TO_SUB').'" name="aaadd" type="submit"></td></tr>';	
	echo "</table></form>";	
}

function AddPrice()
{
	JHTML::_('behavior.calendar');
	echo "<form name='xx' action='index.php?option=com_billing&task=priceall' method='post'><table>";
	echo '<tr><td>'.JText::_('COM_BILLING_SELECT_PRICE').':</td><td> <input class="inputbox" type="text" value="100" name="price"> '. GetDefaultCurrencyAbbr() .'</td></tr>';
	echo '<tr><td>'.JText::_('COM_BILLING_END_OF_PAID_PERIOD').':</td><td> '. JHTML::_('calendar', $cal_var, 'cal_var', 'cal_var', '%Y-%m-%d %H:%M:%S', array('class'=>'inputbox', 'size'=>'25',  'maxlength'=>'19'))  .'</td></tr>';
	echo '<tr><td>'.JText::_('COM_BILLING_SELECTED_ARTICLES').':</td><td> <select name=cid[] MULTIPLE size="10">';	
	foreach($_POST['cid'] as $checkbox) 
	{
		$id = $checkbox;
		echo "<option value='$id' selected>". GetTitle($id) ."</option>";
	}
	echo '</select></td></tr>';
	echo '<tr><td></td><td><input value="'.JText::_('COM_BILLING_MAKE_PAID').'" name="aaadd" type="submit"></td></tr>';	
	echo "</table></form>";	
}

function SMSAll()
{
	foreach($_POST['cid'] as $checkbox) 
	{
		$id = $checkbox;
		ActivateSMS($id);
	}
}

function SaveSubAll()
{
	$db = JFactory::getDBO();
	$sub_id = JRequest::getVar('sub_id');
	foreach($_POST['cid'] as $checkbox) 
	{
		$id = $checkbox;
		$query = "select count(*) from `#__billing_subscription_access` where item_id = $id and item_type = 1 and subscription_id = $sub_id";
		$result = $db->setQuery($query);   
		$n = $db->loadResult();
		
		if ($n == 0)
		{
			$query = "insert into `#__billing_subscription_access` (subscription_id, item_id, item_type) values ($sub_id, $id, 1)";
			$result = $db->setQuery($query);   
			$n = $db->query();
		}
	}
}

function SavePriceAll()
{
	$db = JFactory::getDBO();
	$price = JRequest::getVar('price');
	$cal_var = JRequest::getVar('cal_var');
	foreach($_POST['cid'] as $checkbox) 
	{
		$id = $checkbox;
		$query = "select count(*) from `#__billing_articles` where article_id = $id";
		$result = $db->setQuery($query);   
		$n = $db->loadResult();
		
		if ($n > 0)
		{
			$query = "update `#__billing_articles` set price = $price, is_active = 1, lockbefore = '$cal_var' where article_id = $id";
		}
		else
		{
			$query = "insert into `#__billing_articles` (article_id, is_active, price, lockbefore) values ($id, 1, $price, '$cal_var')";
		}
		
		$result = $db->setQuery($query);   
		$result = $db->query();
	}	
}

function SavePeriod($id)
{
	$db = JFactory::getDBO();
	$price = JRequest::getVar('price');
	$days = JRequest::getVar('subscription_days');	
	
	$query = "insert into `#__billing_subscription_period` (subscription_id, days, price) values ($id, $days, $price)";
	$result = $db->setQuery($query);   
	$result = $db->query();
}

function DeletePeriod($id)
{
	$db = JFactory::getDBO();

	$query = "delete from `#__billing_subscription_period` where id = $id";
	$result = $db->setQuery($query);   
	$result = $db->query();
}

function DeleteUserAccess($id, $uid)
{
	$db = JFactory::getDBO();

	$query = "delete from `#__billing_articles_user` where article_id = $id and uid = $uid";
	$result = $db->setQuery($query);   
	$result = $db->query();
}

function AddUserAccess($id, $uid)
{
	$db = JFactory::getDBO();

	$k2 = JRequest::getInt('k2', 0);	
	$date = date('Y-m-d H:i:s');	
	$query = "insert into  `#__billing_articles_user` (article_id, uid, paymentdate, price, k2) values($id, $uid, '$date', 0, $k2)";
	$result = $db->setQuery($query);   
	$result = $db->query();	
}

function GetPartnerSum($id)
{
	$db = JFactory::getDBO();
    $query = "select sum(val) from `#__billing_partner_history` where partner_id = " . $id;
    $result = $db->setQuery($query);   
	$result = $db->loadResult();
	return $result; 	
}

function ShowPartnerHistory($id, $uid)
{
	$db = JFactory::getDBO();
    $query = 'select * from `#__users` where id =' . $id;
    $result = $db->setQuery($query);   
    $row = $db->loadAssoc();      
	$user = JFactory::getUser($uid);
	echo "<h2>". JText::_('COM_BILLING_PROCEEDS') . " ". $row['name']." ". JText::_('COM_BILLING_BY_ORDERS') . " $user->name</h2>";

	echo '<table width="100%" class="adminlist table">';
	echo "<tr>";
	echo "  <td>Id</td>";
	echo "  <td>".JText::_('COM_BILLING_USER')."</td>";
	echo "  <td>".JText::_('COM_BILLING_DATE')."</td>";
	echo "  <td>".JText::_('COM_BILLING_SUM')."</td>";
	echo "  <td>".JText::_('COM_BILLING_DESCRIPTION')."</td>";
	echo "</tr>";

	$query = "select bp.*, (select name from `#__users` where id = bp.partner_id) as username from `#__billing_partner_history` bp where partner_id = $id and uid = $uid";
    $result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 
	foreach ( $rows as $row ) 
	{
		echo '<tr>';
		echo "<td>$row->id</td>";
		echo "<td>$row->username ($row->name)</td>";
		echo "<td>". FormatBillingDate($row->oper_date)."</td>";
		echo "<td>$row->val</td>";
		echo "<td>$row->data1</td>";
		echo '</tr>';
	}
	echo '</table>';
}

function ShowServices($order, $limitstart= '', $limit = '')
{
	echo '<h1>'.JText::_('COM_BILLING_SERVICES').'</h1>';

	if ($order == '')
	{
		$order = 'id';
	}
	
	if ($limitstart == '')
	{
		$limitstart = 0;
	}
	if ($limit == '')
	{
		$limit = 20;
	}	
	
	if ($limitstart == 0 or $limit == 0)
	{
		$lim = '';
	}
	elseif ($limitstart == '' or $limit == '')
	{
		$lim = '';
	}
	else
	{
		$lim = "limit $limitstart, $limit";
	}
	
	JToolBarHelper::custom( 'service', 'new', 'new', JText::_('COM_BILLING_ADD'), false );
	
	$db = JFactory::getDBO();
    $query = 'select count(*) from `#__billing_services` ';
	$result = $db->setQuery($query);   
	$total = $db->loadResult();    
	
	$db = JFactory::getDBO();
    $query = 'select bc.*, (select count(*) from `#__billing_services_user` where service_id = bc.id and state = 0) as srv from `#__billing_services` bc order by ' . $order . " $lim";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 	
	
	echo '<table width="100%" class="adminlist table" cellspacing="1">';
    echo '<tr>'.
		'<th class="title">ID</th> '.
		'<th class="title">'.JText::_('COM_BILLING_TITLE').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_PRICE').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_SUB_USERS').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_IS_ACTIVE').'</th> '.
		'<th class="title" width="30">'.JText::_('COM_BILLING_DELETE').'</th></tr>';
		
	if (count($rows) > 0)	
	{
		foreach( $rows as $row ) 
		{
			echo "<tr>";
				echo "<td>$row->id</td>";
				echo "<td><a href='index.php?option=com_billing&task=editservice&id=$row->id'>$row->service_name</a></td>";
				echo "<td>". FormatDefaultCurrency($row->price) ."</td>";
				echo "<td><a href='index.php?option=com_billing&task=serviceusers&id=$row->id'>".JText::_('COM_BILLING_VIEW_LIST')."</a> (".JText::_('COM_BILLING_UNPROCESSED_ORDERS').": <strong>$row->srv</strong>)</td>";
				if ($row->is_active == 1)
				{
					echo "<td><a href='index.php?option=com_billing&task=activateservice&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_HIDE')."' title='".JText::_('COM_BILLING_HIDE')."' src='". tick ."'></a></td>";
				}
				else
				{
					echo "<td><a href='index.php?option=com_billing&task=activateservice&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_ACTIVATE')."' title='".JText::_('COM_BILLING_ACTIVATE')."' src='".publish_x."'></a></td>";
				}
				echo "<td><a href='index.php?option=com_billing&task=deleteservice&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_DELETE')."' title='".JText::_('COM_BILLING_DELETE')."' src='images/".publish_x."'></a></td>";
			echo "</tr>";
			
		}
	}
	echo '</table>';

	$pagenav = new JPagination ( $total, $limitstart,  $limit);
	echo $pagenav->getListFooter();
	
	echo '<br><h1>'.JText::_('COM_BILLING_TARIFFS').'</h1>';
	
	$query = "select * from `#__billing_subscription_type` where id in (select subscription_id from `#__billing_subscription_access` where item_type = 8)";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 	
	
	echo '<table width="100%" class="adminlist table" cellspacing="1">';
    echo '<tr>'.
		'<th class="title">ID</th> '.
		'<th class="title">'.JText::_('COM_BILLING_TITLE').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_PRICE').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_SERVICES').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_IS_ACTIVE1').'</th> '.
		'<th class="title" width="30">'.JText::_('COM_BILLING_DELETE').'</th></tr>';
	
	if (count($rows) > 0)	
	{
		foreach( $rows as $row ) 
		{
			echo "<tr>";
				echo "<td>$row->id</td>";
				echo "<td><a href='index.php?option=com_billing&task=editsub&id=$row->id'>$row->subscription_type</a></td>";
				echo "<td>". FormatDefaultCurrency($row->price) ."</td>";
				$query = "select * from `#__billing_services` where id in (select item_id from `#__billing_subscription_access` where item_type = 8 and subscription_id = $row->id) order by service_name";
				$result = $db->setQuery($query);   
				$srvs = $db->loadObjectList(); 	
				echo '<td>';
				if(count($srvs) > 0)
				{
					foreach($srvs as $srv)
					{
						echo "<a href='index.php?option=com_billing&task=editservice&id=$srv->id'>$srv->service_name</a> <br>";
					}
				}
				echo '</td>';
				if($row->is_active == 0)
				{		
					echo "<td><a href='index.php?option=com_billing&task=activesub&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_ACTIVATE')."' title='".JText::_('COM_BILLING_ACTIVATE')."' src='".publish_x."'></a></td>";
				}
				else
				{
					echo "<td><a href='index.php?option=com_billing&task=activesub&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_HIDE')."' title='".JText::_('COM_BILLING_HIDE')."' src='". tick ."'></a></td>";
				}
				echo "<td><a href='index.php?option=com_billing&task=delsub&id=$row->id'>".JText::_('COM_BILLING_DELETE')."</a></td>";
			echo '</tr>';	
		}
	}
	echo '</table>';
}

function EditService($id = '')
{
	if ($id != '')
	{
		$db = JFactory::getDBO();
		$query = 'select * from `#__billing_services` where id = ' .$id;
		$result = $db->setQuery($query);  
		$row = $db->loadAssoc();
		$service_name = $row['service_name'];
		$price = $row['price'];
		$annotation = $row['annotation'];
		$free_price = $row['data1'];
	}
	else
	{
		$service_name = '';
		$price = '';
		$annotation = '';
		$free_price = false;
	}
	
	$editor = JFactory::getEditor();

	echo "<form name='frm' method='post' action='index.php?option=com_billing&task=saveservice&id=$id'>";
	echo '<table border=0>';
	echo '<tr>';
	echo '	<td>'.JText::_('COM_BILLING_TITLE').'</td>';
	echo "	<td><input class='inputbox' name='service_name' value='$service_name' size='100' type='text'></td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td>'.JText::_('COM_BILLING_PRICE').'</td>';
	echo "	<td><input class='inputbox' name='price' value='$price' size='10' type='text'> " . GetDefaultCurrencyAbbr() ."</td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td>'.JText::_('COM_BILLING_FREE_PRICE').'</td>';
	if($free_price) {$ch = 'checked';} else {$ch = '';}
	echo "	<td><input name='free_price' value='$free_price' size='10' type='checkbox' onclick='this.value=this.checked;' $ch>".Tooltip(JText::_('COM_BILLING_FREE_PRICE_INFO'))."</td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td>'.JText::_('COM_BILLING_DESCRIPTION').'</td>';
	echo "	<td>".$editor->display( 'desc', $annotation, '600', '400', '20', '20', true)."</td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td></td>';
	echo "	<td><input name='sub' value='".JText::_('COM_BILLING_SAVE')."' type='submit'></td>";
	echo '</tr>';
	echo '</table>';
}

function SaveService($id = '')
{
	$service_name = JRequest::getVar('service_name');
	$price = JRequest::getFloat('price');
	$free_price = JRequest::getVar('free_price');
	$annotation = JRequest::getVar( 'desc', '', 'post', 'string', JREQUEST_ALLOWHTML );
	$annotation = addslashes($annotation);
	
	if ($id != '')
	{
		$query = "update `#__billing_services` set service_name = '$service_name', price = $price, annotation = '$annotation', data1 = '$free_price'  where id = $id";
	}
	else
	{
		$query = "insert into `#__billing_services` (service_name, price, annotation, is_active, data1) values ('$service_name', $price, '$annotation', 1, '$free_price')";
	}
	$db = JFactory::getDBO();
	$result = $db->setQuery($query);
	$code = $db->query();
}

function ActivateItem($id)
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_services` where id = $id";
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();	
	if ($row['is_active'] == 1)
	{
		$query = "update `#__billing_services` set is_active = 0 where id = $id";
	}
	else
	{
		$query = "update `#__billing_services` set is_active = 1 where id = $id";
	}
	$result = $db->setQuery($query);
	$result = $db->query();
}

function DeleteService($id)
{
	$db = JFactory::getDBO();
	$query = "delete from `#__billing_services` where id = $id";
	$result = $db->setQuery($query);
	$result = $db->query();
}

function ServiceUsers($id)
{
	$db = JFactory::getDBO();
    $query = 'select * from `#__billing_services_user` where service_id = ' . $id . ' order by paymentdate desc';
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 	
	
	echo '<table width="100%" class="adminlist table" cellspacing="1">';
    echo '<tr>'.
		'<th class="title">ID</th> '.
		'<th class="title">'.JText::_('COM_BILLING_USER').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_PRICE').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_COMMENT').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_SUB_TIME').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_SERVICE_DONE').'</th> '.
		'<th class="title" width="30">'.JText::_('COM_BILLING_DELETE').'</th></tr>';
    foreach( $rows as $row ) 
    {
		echo "<tr>";
			echo "<td>$row->id</td>";
			echo "<td><a href='index.php?option=com_users&view=user&task=edit&cid[]=$row->uid'>".JText::_('COM_BILLING_USER').": " .GetFullUserName($row->uid)."</a></td>";
			echo "<td>". FormatDefaultCurrency($row->price) ."</td>";
			echo "<td>$row->comments</td>";
			echo "<td>". FormatBillingDate($row->paymentdate)."</td>";
			if ($row->state == 1)
			{
				echo "<td><a href='index.php?option=com_billing&task=checkservice&id=$row->id&uid=$row->uid'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_HIDE')."' title='".JText::_('COM_BILLING_HIDE')."' src='". tick ."'></a> ".JText::_('COM_BILLING_SERVICE_DATE').": ". FormatBillingDate($row->servicedate)."</td>";
			}
			else
			{
				echo "<td><a href='index.php?option=com_billing&task=checkservice&id=$row->id&uid=$row->uid'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_ACTIVATE')."' title='".JText::_('COM_BILLING_ACTIVATE')."' src='".publish_x."'></a></td>";
			}
			echo "<td><a href='index.php?option=com_billing&task=deleteserviceuser&id=$row->id&uid=$row->uid'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_DELETE')."' title='".JText::_('COM_BILLING_DELETE')."' src='".publish_x."'></a></td>";
		echo "</tr>";
		
    }
	echo '</table><br>';	
	echo '<a href="index.php?option=com_billing&task=services">'.JText::_('COM_BILLING_BACK').'</a>';
}

function DeleteUserService($id, $uid)
{
	$db = JFactory::getDBO();
	$query = "delete from `#__billing_services_user` where id = $id and uid = $uid";
	$result = $db->setQuery($query);
	$result = $db->query();	
}

function CheckService($id, $uid)
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_services_user` where id = $id and uid = $uid";
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();	
	if ($row['state'] == 1)
	{
		$query = "update `#__billing_services_user` set state = 0 where id = $id and uid = $uid";
	}
	else
	{
		$date = date('Y-m-d');
		$query = "update `#__billing_services_user` set state = 1, servicedate = '$date' where id = $id and uid = $uid";
	}
	$result = $db->setQuery($query);
	$result = $db->query();
	
	$query = "select * from `#__billing_services` where id = ". $row['service_id'];
	$result = $db->setQuery($query);
	$service = $db->loadAssoc();

	if ($row['state'] != 1)
	{
		$service2 = GetOption('service2');
		if ($service2 == '') {$service2 = false;} 
		if ($service2 == true)
		{
			$query = "select * from `#__billing_services` where id = $id";
			$result = $db->setQuery($query);   
			$row = $db->loadAssoc();
			EmailToUser1($uid, JText::_('COM_BILLING_INFORM_YOU'). " ". $service['service_name']. " ". JText::_('COM_BILLING_PROVIDED').". <br>".
			$row['comments'] . '<br>'.
			JText::_('COM_BILLING_THANKS').".", JText::_('COM_BILLING_INFROM'));
		}
	}
}

function ShowAccounts()
{
	echo '<h1>'.JText::_('COM_BILLING_ACCOUNTS').'</h1>';
	
	JToolBarHelper::custom( 'addaccount', 'new', 'new', JText::_('COM_BILLING_ADD'), false );
	
	$db = JFactory::getDBO();
    $query = 'select * from `#__billing_account_type` ';
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 	
	
	echo '<table width="100%" class="adminlist table" cellspacing="1">';
    echo '<tr>'.
		'<th class="title">ID</th> '.
		'<th class="title">'.JText::_('COM_BILLING_TITLE').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_IS_ACTIVE1').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_CURRENCY').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_CAN_ADD').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_CAN_PAY').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_PARTNER_CHARGE').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_CASH_OUT').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_DEPOSIT').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_CREDIT').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_GET_MONEY').'</th> '.
		'<th class="title">'.JText::_('COM_BILLING_FOR_SUB').'</th> '.
		'<th class="title" width="30">'.JText::_('COM_BILLING_DELETE').'</th></tr>';
		
	if (count($rows) > 0)	
	{
		foreach( $rows as $row ) 
		{
			echo "<tr>";
				echo "<td>$row->id</td>"; 
				echo "<td><a href='index.php?option=com_billing&task=editaccount&id=$row->id'>$row->account_type_name</a></td>";
				$checked = '';
				if ($row->active == 1)
				{
					echo "<td><a href='index.php?option=com_billing&task=activeaccount&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_HIDE')."' title='".JText::_('COM_BILLING_HIDE')."' src='". tick ."'></a></td>";
				}			
				else
				{
					echo "<td><a href='index.php?option=com_billing&task=activeaccount&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_ACTIVATE')."' title='".JText::_('COM_BILLING_ACTIVATE')."' src='".publish_x."'></td>";
				}
				echo "<td>" . GetCurrency($row->currency_id) . "</td>";
				if ($row->income_flag == 0) { echo "<td><input type='checkbox'  disabled='disabled'></td>";}  else { echo "<td><input type='checkbox' checked disabled='disabled'></td>";}
				if ($row->out_flag == 0) { echo "<td><input type='checkbox' disabled='disabled'></td>";}  else { echo "<td><input type='checkbox' checked disabled='disabled'></td>";}
				if ($row->partner_flag == 0) { echo "<td><input type='checkbox' disabled='disabled'></td>";}  else { echo "<td><input type='checkbox' checked disabled='disabled'></td>";}
				if ($row->money_flag == 0) { echo "<td><input type='checkbox' disabled='disabled'></td>";}  else { echo "<td><input type='checkbox' checked disabled='disabled'></td>";}
				if ($row->deposit_flag == 0) { echo "<td><input type='checkbox' disabled='disabled'></td>";}  else { echo "<td><input type='checkbox' checked disabled='disabled'></td>";}
				if ($row->credit_flag == 0) { echo "<td><input type='checkbox' disabled='disabled'></td>";}  else { echo "<td><input type='checkbox' checked disabled='disabled'></td>";}
				if ($row->fromuser_flag == 0) { echo "<td><input type='checkbox' disabled='disabled'></td>";}  else { echo "<td><input type='checkbox' checked disabled='disabled'></td>";}
				if ($row->subscribe_flag == 0) { echo "<td><input type='checkbox' disabled='disabled'></td>";}  else { echo "<td><input type='checkbox' checked disabled='disabled'></td>";}
				echo "<td><a href='index.php?option=com_billing&task=deleteaccount&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_DELETE')."' title='".JText::_('COM_BILLING_DELETE')."' src='".publish_x."'></a></td>";
			echo "</tr>";
			
		}
	}
	echo '</table>';
	
}

function ActivateAccount($id)
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_account_type` where id = $id";
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();	
	if ($row['is_active'] == 1)
	{
		$query = "update `#__billing_account_type` set `active` = 0 where id = $id";
	}
	else
	{
		$query = "update `#__billing_account_type` set `active` = 1 where id = $id";
	}
	$result = $db->setQuery($query);
	$result = $db->query();
}

function EditAccount($id = '')
{
	$db = JFactory::getDBO();
	if ($id != '')
	{
		$query = 'select * from `#__billing_account_type` where id = ' .$id;
		$result = $db->setQuery($query);  
		$row = $db->loadAssoc();
		$account_type_name = $row['account_type_name'];
		$currency_id = $row['currency_id'];
		$income_flag = $row['income_flag'];
		$out_flag = $row['out_flag'];
		$partner_flag = $row['partner_flag'];
		$money_flag = $row['money_flag'];
		$deposit_flag = $row['deposit_flag'];
		$credit_flag = $row['credit_flag'];
		$fromuser_flag = $row['fromuser_flag'];
		$subscribe_flag = $row['subscribe_flag'];
		$credit_period = $row['credit_period'];
		$deposit_period = $row['deposit_period'];
		$credit_percent = $row['credit_percent'];
		$deposit_percent = $row['deposit_percent'];
	}
	else
	{
		$account_type_name = '';
		$currency_id = 0;
		$income_flag = 0;
		$out_flag = 0;
		$partner_flag = 0;
		$money_flag = 0;
		$deposit_flag = 0;
		$credit_flag = 0;
		$fromuser_flag = 0;
		$subscribe_flag = 0;
		$credit_period = 0;
		$deposit_period = 0;
		$credit_percent = 0;
		$deposit_percent = 0;
	}
	
	echo "<form name='frm' method='post' action='index.php?option=com_billing&task=saveaccount&id=$id' enctype='multipart/form-data'>";
	echo '<table border=0>';
	echo '<tr>';
	echo '	<td>'.JText::_('COM_BILLING_TITLE').'</td>';
	echo "	<td><input class='inputbox' name='account_type_name' value='$account_type_name' size='100' type='text'></td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td>'.JText::_('COM_BILLING_ACCOUNT_CURRENCY').'</td>';
	echo "	<td> <select name='currency_id'>";
	
		$query = 'select * from `#__billing_currency`';
		$result = $db->setQuery($query);
		$rows = $db->loadObjectList();
		foreach( $rows as $rowx ) 
		{
			if ($rowx->id == $currency_id)
			{
				echo "<option value='$rowx->id' selected>$rowx->curency_name</option>";
			}
			else
			{
				echo "<option value='$rowx->id'>$rowx->curency_name</option>";
			}
		}

	echo "</select></td>";
	echo '</tr>';

	echo '<tr>';
	echo '	<td></td>';
	$val = 0; $checked = ''; if ($income_flag == 1) {$checked = 'checked';  $val = 1;}
	echo "	<td><input name='income_flag' id='income_flag' value='$val' $checked type='checkbox' onclick=\"if (document.getElementById('income_flag').checked) {document.getElementById('income_flag').value = 1;} else {document.getElementById('income_flag').value = 0;}\"> ".JText::_('COM_BILLING_CAN_ADD')."</td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td></td>';
	$val = 0; $checked = ''; if ($out_flag == 1) {$checked = 'checked';  $val = 1;}
	echo "	<td><input name='out_flag' id='out_flag' value='$val' $checked type='checkbox' onclick=\"if (document.getElementById('out_flag').checked) {document.getElementById('out_flag').value = 1;} else {document.getElementById('out_flag').value = 0;}\"> ".JText::_('COM_BILLING_CAN_PAY')."</td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td></td>';
	$val = 0; $checked = ''; if ($partner_flag == 1) {$checked = 'checked';  $val = 1;}
	echo "	<td><input name='partner_flag' id='partner_flag' value='$val' $checked type='checkbox' onclick=\"if (document.getElementById('partner_flag').checked) {document.getElementById('partner_flag').value = 1;} else {document.getElementById('partner_flag').value = 0;}\"> ".JText::_('COM_BILLING_AFFILATE_INCOME')."</td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td></td>';
	$val = 0; $checked = ''; if ($money_flag == 1) {$checked = 'checked';  $val = 1;}
	echo "	<td><input name='money_flag' id='money_flag' value='$val' $checked type='checkbox' onclick=\"if (document.getElementById('money_flag').checked) {document.getElementById('money_flag').value = 1;} else {document.getElementById('money_flag').value = 0;}\"> ".JText::_('COM_BILLING_CAN_WITHDRAW')."</td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td></td>';
	$val = 0; $checked = ''; if ($deposit_flag == 1) {$checked = 'checked'; $val = 1;}
	echo "	<td><input name='deposit_flag' id='deposit_flag' value='$val' $checked type='checkbox' onclick=\"if (document.getElementById('deposit_flag').checked) {document.getElementById('deposit_flag').value = 1;} else {document.getElementById('deposit_flag').value = 0;}\"> ".JText::_('COM_BILLING_DEPOSIT2')."</td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td>'.JText::_('COM_BILLING_DEPOSIT_DAYS').'</td>';
	echo "	<td><input class='inputbox' name='deposit_period' value='$deposit_period' type='text' size='4'></td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td>'.JText::_('COM_BILLING_DEPOSIT_PERCENT').'</td>';
	echo "	<td><input class='inputbox' name='deposit_percent' value='$deposit_percent' type='text' size='4'> %</td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td></td>';
	$val = 0; $checked = ''; if ($credit_flag == 1) {$checked = 'checked';  $val = 1;}
	echo "	<td><input name='credit_flag' id='credit_flag' value='$val' $checked type='checkbox' onclick=\"if (document.getElementById('credit_flag').checked) {document.getElementById('credit_flag').value = 1;} else {document.getElementById('credit_flag').value = 0;}\"> ".JText::_('COM_BILLING_CREDIT2')."</td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td>'.JText::_('COM_BILLING_CREDIT_DAYS').'</td>';
	echo "	<td><input class='inputbox' name='credit_period' value='$credit_period' type='text' size='4'></td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td>'.JText::_('COM_BILLING_CREDIT_PERCENT').'</td>';
	echo "	<td><input class='inputbox' name='credit_percent' value='$credit_percent' type='text' size='4'> %</td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td></td>';
	$val = 0; $checked = ''; if ($fromuser_flag == 1) {$checked = 'checked';  $val = 1;}
	echo "	<td><input name='fromuser_flag' id='fromuser_flag' value='$val' $checked type='checkbox' onclick=\"if (document.getElementById('fromuser_flag').checked) {document.getElementById('fromuser_flag').value = 1;} else {document.getElementById('fromuser_flag').value = 0;}\"> ".JText::_('COM_BILLING_USER_TRANSFER')."</td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td></td>';
	$val = 0; $checked = ''; if ($subscribe_flag == 1) {$checked = 'checked';  $val = 1;}
	echo "	<td><input name='subscribe_flag' id='subscribe_flag' value='$val' $checked type='checkbox' onclick=\"if (document.getElementById('subscribe_flag').checked) {document.getElementById('subscribe_flag').value = 1;} else {document.getElementById('subscribe_flag').value = 0;}\"> ".JText::_('COM_BILLING_SUB_ACCOUNT')."</td>";
	echo '</tr>';
	echo '<tr>';
	echo '	<td>'.JText::_('COM_BILLING_PIN_PLUGIN').'</td>';
	echo "	<td><input class='inputbox' name='plugin' value='' type='file' size='100'></td>";
	echo '</tr>';
	
	echo '<tr>';
	echo '	<td></td>';
	echo "	<td><input name='sub' value='".JText::_('COM_BILLING_SAVE')."' type='submit'></td>";
	echo '</tr>';
	echo '</table>';
	echo "</form>";
}	

function InstallAccountPlugin($tmp_name, $file_name)
{
	$s = uniqid(rand (), true);
	$new_name = JPATH_ROOT . '/components/com_billing/plugins/' . $s . '.zip';
	
	$x = move_uploaded_file($tmp_name, $new_name);
	if ($x)
	{
		$path_to_pluginXML = UnArchive($new_name, $file_name, 'plugins');
		unlink($new_name);
		$sql = JPATH_ROOT . "/components/com_billing/plugins/" . JFile::stripExt($file_name) . '/install.sql';
		$install = JPATH_ROOT . "/components/com_billing/plugins/" . JFile::stripExt($file_name) . '/install.php';
	}
	if (!file_exists($path_to_pluginXML))
	{
		echo '<p>Plugin file not found</p>';
		return;
	}
	
}

function SaveAccount()
{
	$id = JRequest::getInt('id');
	$account_type_name = JRequest::getVar('account_type_name');
	$currency_id = JRequest::getInt('currency_id');
	$income_flag = JRequest::getInt('income_flag');
	$out_flag = JRequest::getInt('out_flag');
	$partner_flag = JRequest::getInt('partner_flag');
	$money_flag = JRequest::getInt('money_flag');
	$deposit_flag = JRequest::getInt('deposit_flag');
	$credit_flag = JRequest::getInt('credit_flag');
	$fromuser_flag = JRequest::getInt('fromuser_flag');
	$subscribe_flag = JRequest::getInt('subscribe_flag');
	$credit_period = JRequest::getInt('credit_period');;
	$deposit_period = JRequest::getInt('deposit_period');
	$credit_percent = JRequest::getVar('credit_percent');
	$deposit_percent = JRequest::getVar('deposit_percent');
	
	if($_FILES['plugin']['tmp_name'] != '')
	{
		InstallAccountPlugin($_FILES['plugin']['tmp_name'], $_FILES['plugin']['name']);
	}
	
	$db = JFactory::getDBO();
		
	if ($id != '')
	{
		$query = "update `#__billing_account_type` set account_type_name = '$account_type_name', currency_id = $currency_id, income_flag = $income_flag, out_flag = $out_flag, partner_flag = $partner_flag, money_flag = $money_flag, deposit_flag = $deposit_flag, credit_flag = $credit_flag, fromuser_flag = $fromuser_flag, subscribe_flag = $subscribe_flag, credit_period = $credit_period, deposit_period = $deposit_period,  credit_percent = $credit_percent, deposit_percent = $deposit_percent where id = $id";
		$result = $db->setQuery($query);
		$code = $db->query();
	}
	else
	{
		$query = "insert into  `#__billing_account_type` (account_type_name, currency_id, income_flag, out_flag, partner_flag, money_flag, deposit_flag, credit_flag, fromuser_flag, subscribe_flag, credit_period, deposit_period, credit_percent, deposit_percent) values ('$account_type_name', $currency_id, $income_flag, $out_flag, $partner_flag, $money_flag, $deposit_flag, $credit_flag, $fromuser_flag, $subscribe_flag, $credit_period, $deposit_period, $credit_percent, $deposit_percent)";
		$result = $db->setQuery($query);
		$code = $db->query();
	}
}

function ShowUserAccounts($uid)
{
	$user = JFactory::getUser($uid);

	echo "<h3>".JText::_('COM_BILLING_USER_ACCOUNTS')." $user->name</h3>";

	$db = JFactory::getDBO();
    $query = "select distinct * from `#__billing_account_type` where id in (select account_type from `#__billing_account` where uid = $uid)";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 
		
	echo '<table width="100%" class="adminlist table" cellspacing="1">';
	echo "<tr>";
	echo "  <td>".JText::_('COM_BILLING_ACCOUNT')."</td>";
	echo "  <td>".JText::_('COM_BILLING_ACCOUNT_BALANCE')."</td>";
	echo "  <td>".JText::_('COM_BILLING_ACCOUNT_ACTIVITY')."</td>";
	echo "  <td>".JText::_('COM_BILLING_FILL_UP')."</td>";
	echo "  <td>".JText::_('COM_BILLING_WRITTEN_OFF')."</td>";
	echo "</tr>";

	if (count($rows) > 0)
		{
			foreach ( $rows as $row ) 
			{
				echo  '<tr>';
				echo  "<td>$row->account_type_name</td>";
				echo  "<td>".FormatCurrency($row->currency_id, GetUserAccountSum($row->id, $uid))."</td>";
				echo  "<td><a href='index.php?option=com_billing&task=showaccusr&uid=$uid&acc_id=".GetUserAccount($row->id, $uid)."'>".JText::_('COM_BILLING_VIEW')."</a></td>";
				echo  "<td><a href='index.php?option=com_billing&task=plusaccusr&uid=$uid&acc_id=".GetUserAccount($row->id, $uid)."'>".JText::_('COM_BILLING_FILL_UP')."</a></td>";
				echo  "<td><a href='index.php?option=com_billing&task=minusaccusr&uid=$uid&acc_id=".GetUserAccount($row->id, $uid)."'>".JText::_('COM_BILLING_WRITTEN_OFF')."</a></td>";
				echo  '</tr>';
			}
		} 
	echo '</table>';
}

function PlusAcc($uid, $acc_id)
{
	if ($uid == 0)
	{
		return;
	}

	$db = JFactory::getDBO();
    $query = 'select * from `#__users` where id=' . $uid;
    $result = $db->setQuery($query);   
    $row = $db->loadAssoc();        

	echo '<form name="addmoneyacc" method="post" action="index.php?option=com_billing&task=addmoneyacc&acc_id='.$acc_id.'&uid='.$uid.'">';
	echo '<table>';
	echo '<tr>';
	echo "<td>".JText::_('COM_BILLING_ADD_TO_ACCOUNT')." ". $row['name']." (".$row['username'].")</td>";
	echo '<td></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td>'.JText::_('COM_BILLING_ADD_TO_DEPOSIT_SUM').', '.GetDefaultCurrencyAbbr($acc_id).'</td>';
	echo '<td><input class="inputbox" type="text" name="val" value="10"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="submit" name="sub" value="'.JText::_('COM_BILLING_FILL_UP').'"></td>';
	echo '<td><input type="button" name="sub" value="'.JText::_('COM_BILLING_CANCEL').'" onclick="javascript:history.back();"></td>';
	echo '</tr>';
	echo '</table>';
	echo '</form>';	
}

function MinusAcc($uid, $acc_id)
{
	if ($uid == 0)
	{
		return;
	}

	$db = JFactory::getDBO();
    $query = 'select * from `#__users` where id=' . $uid;
    $result = $db->setQuery($query);   
    $row = $db->loadAssoc();        

	echo '<form name="addmoneyacc" method="post" action="index.php?option=com_billing&task=witmoneyacc&acc_id='.$acc_id.'&uid='.$uid.'">';
	echo '<table>';
	echo '<tr>';
	echo "<td>".JText::_('COM_BILLING_WRITE_OFF_ACCOUNT')." ". $row['name']." (".$row['username'].")</td>";
	echo '<td></td>';
	echo '</tr>';
	echo '<tr>';
	echo "<td>".JText::_('COM_BILLING_REASON_OF_CANCELATION')."</td>";
	echo '<td><input class="inputbox" type="text" name="reason" value="" size="100"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td>'.JText::_('COM_BILLING_WRITTEN_OFF').', '.GetDefaultCurrencyAbbr($acc_id).'</td>';
	echo '<td><input class="inputbox" type="text" name="val" value="10"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="submit" name="sub" value="'.JText::_('COM_BILLING_WRITTEN_OFF').'"></td>';
	echo '<td><input type="button" name="sub" value="'.JText::_('COM_BILLING_CANCEL').'" onclick="javascript:history.back();"></td>';
	echo '</tr>';
	echo '</table>';
	echo '</form>';	
}

function AddMoneyAcc($uid, $acc_id)
{
	$val = JRequest::getVar('val');  
	$UA = new UserAccount;
	$pid = date('ymdhis');
	$user = JFactory::getUser();
	$UA->AddMoney($uid, $val, $pid, JText::_('COM_BILLING_BY_ADMIN') . ' ' . $user->username, 0, $acc_id);
	ShowUserAccounts($uid);
}

function WithMoneyAcc($uid, $acc_id)
{
	$val = JRequest::getVar('val');
	$reason = JRequest::getVar('reason');
	$UA = new UserAccount;
	$pid = date('ymdhis');
	$UA->WithdrawMoney($uid, $val, $pid, $reason, $acc_id);
	ShowUserAccounts($uid);
}

function ShowUserAccount($uid, $acc_id)
{
	echo "<h3>".JText::_('COM_BILLING_USERS').": ".JText::_('COM_BILLING_ACCOUNT_ACTIVITY')."</h3>";

	$db = JFactory::getDBO();
    $query = "select distinct * from `#__billing` where uid = $uid and acc= $acc_id order by id";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 
		
	echo '<table width="100%" class="adminlist table" cellspacing="1">';
	echo "<tr>";
	echo "  <td>".JText::_('COM_BILLING_DATE')."</td>";
	echo "  <td>".JText::_('COM_BILLING_ACCOUNT_BALANCE')."</td>";
	echo "  <td>".JText::_('COM_BILLING_ACCOUNT_ACTIVITY')."</td>";
	echo "</tr>";

	if (count($rows) > 0)
	{
		foreach ( $rows as $row ) 
		{
			echo  '<tr>';
			echo  "<td>". FormatBillingDate($row->adate)."</td>";
			echo  "<td>".FormatDefaultCurrency($row->val)."</td>";
			echo  "<td>$row->data1</a></td>";
			echo  '</tr>';
		}
	}

}

function DeleteAccount($id)
{
	$db = JFactory::getDBO();
	$query = "select count(*) from `#__billing_account` where account_type = $id";
	$result = $db->setQuery($query);
	$n = $db->loadResult();		
	if($n > 0 )	
	{
		JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLING_CANT_DELETE_ACCOUNT') );
		return;
	}
	$query = "delete from `#__billing_account_type` where id = $id";
	$result = $db->setQuery($query);
	$result = $db->query();		
}

function SalesReport()
{
	$db = JFactory::getDBO();

	$out = '';
	$out .= '<table width="100%" class="adminlist table" cellspacing="1">';
	$out .= "<tr><th>".JText::_('COM_BILLING_PRODUCT')."</th>";
	$out .= "  <th>".JText::_('COM_BILLING_JANUARY')."</th>";
	$out .= "  <th>".JText::_('COM_BILLING_FEBRUARY')."</th>";
	$out .= "  <th>".JText::_('COM_BILLING_MARCH')."</th>";
	$out .= "  <th>".JText::_('COM_BILLING_APRIL')."</th>";
	$out .= "  <th>".JText::_('COM_BILLING_MAY')."</th>";
	$out .= "  <th>".JText::_('COM_BILLING_JUNE')."</th>";
	$out .= "  <th>".JText::_('COM_BILLING_JULY')."</th>";
	$out .= "  <th>".JText::_('COM_BILLING_AUGUST')."</th>";
	$out .= "  <th>".JText::_('COM_BILLING_SEPTEMBER')."</th>";
	$out .= "  <th>".JText::_('COM_BILLING_OCTOBER')."</th>";
	$out .= "  <th>".JText::_('COM_BILLING_NOVEMBER')."</th>";
	$out .= "  <th>".JText::_('COM_BILLING_DECEMBER')."</th>";
	$out .= "  <th>".JText::_('COM_BILLING_ALLYEAR')."</th></tr>";
	
	$sum1 = 0;
	$sum2 = 0;
	$sum3 = 0;
	$sum4 = 0;
	$sum5 = 0;
	$sum6 = 0;
	$sum7 = 0;
	$sum8 = 0;
	$sum9 = 0;
	$sum10 = 0;
	$sum11 = 0;
	$sum12 = 0;
	
	$query = "select * from `#__billing_articles` order by id";
	$result = $db->setQuery($query);
	$arts = $db->loadObjectList();
	$Y = date('Y');
	if (count($arts) > 0)
	{
		foreach($arts as $art)
		{
			$aid = $art->article_id;
			$k2 = $art->k2;
			$title = GetTitle($aid, $k2);
			$out .= '<tr>';	
			$out .= "<td>$title</td>";	
			
			$sum = 0;
			
			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-01-01' and '$Y-01-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum1 = $sum1 + $res['sum'];
			
			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-02-01' and '$Y-02-28' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum2 = $sum2 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-03-01' and '$Y-03-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum3 = $sum3 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-04-01' and '$Y-04-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum4 = $sum4 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-05-01' and '$Y-05-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum5 = $sum5 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-06-01' and '$Y-06-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum6 = $sum6 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-07-01' and '$Y-07-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum7 = $sum7 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-08-01' and '$Y-08-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum8 = $sum8 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-09-01' and '$Y-09-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum9 = $sum9 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-10-01' and '$Y-10-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum10 = $sum10 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-11-01' and '$Y-11-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum11 = $sum11 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-12-01' and '$Y-12-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum12 = $sum12 + $res['sum'];

			$out .= "<td><strong>".FormatDefaultCurrency($sum)."</strong></td>";
			$out .= '</tr>';	
		}
	}
	
	$query = "select * from `#__billing_subscription_type` order by id";
	$result = $db->setQuery($query);
	$subs = $db->loadObjectList();
	if (count($subs) > 0)
	{
		foreach($subs as $sub)
		{
			$sid = $sub->id;
			$out .= '<tr>';	
			$out .= "<td>$sub->subscription_type</td>";	
			
			$sum = 0;
			
			$query = "select sum(payment_sum) as sum, count(*) as cnt from `#__billing_sub_history` where subscription_id = $sid and payment_date BETWEEN '$Y-01-01' and '$Y-01-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum1 = $sum1 + $res['sum'];
			
			$query = "select sum(payment_sum) as sum, count(*) as cnt from `#__billing_sub_history` where subscription_id = $sid and payment_date BETWEEN '$Y-02-01' and '$Y-02-28' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum2 = $sum2 + $res['sum'];

			$query = "select sum(payment_sum) as sum, count(*) as cnt from `#__billing_sub_history` where subscription_id = $sid and payment_date BETWEEN '$Y-03-01' and '$Y-03-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum3 = $sum3 + $res['sum'];

			$query = "select sum(payment_sum) as sum, count(*) as cnt from `#__billing_sub_history` where subscription_id = $sid and payment_date BETWEEN '$Y-04-01' and '$Y-04-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum4 = $sum4 + $res['sum'];

			$query = "select sum(payment_sum) as sum, count(*) as cnt from `#__billing_sub_history` where subscription_id = $sid and payment_date BETWEEN '$Y-05-01' and '$Y-05-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum5 = $sum5 + $res['sum'];

			$query = "select sum(payment_sum) as sum, count(*) as cnt from `#__billing_sub_history` where subscription_id = $sid and payment_date BETWEEN '$Y-06-01' and '$Y-06-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum6 = $sum6 + $res['sum'];

			$query = "select sum(payment_sum) as sum, count(*) as cnt from `#__billing_sub_history` where subscription_id = $sid and payment_date BETWEEN '$Y-07-01' and '$Y-07-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum7 = $sum7 + $res['sum'];

			$query = "select sum(payment_sum) as sum, count(*) as cnt from `#__billing_sub_history` where subscription_id = $sid and payment_date BETWEEN '$Y-08-01' and '$Y-08-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum8 = $sum8 + $res['sum'];

			$query = "select sum(payment_sum) as sum, count(*) as cnt from `#__billing_sub_history` where subscription_id = $sid and payment_date BETWEEN '$Y-09-01' and '$Y-09-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum9 = $sum9 + $res['sum'];

			$query = "select sum(payment_sum) as sum, count(*) as cnt from `#__billing_sub_history` where subscription_id = $sid and payment_date BETWEEN '$Y-10-01' and '$Y-10-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum10 = $sum10 + $res['sum'];

			$query = "select sum(payment_sum) as sum, count(*) as cnt from `#__billing_sub_history` where subscription_id = $sid and payment_date BETWEEN '$Y-11-01' and '$Y-11-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum11 = $sum11 + $res['sum'];

			$query = "select sum(payment_sum) as sum, count(*) as cnt from `#__billing_sub_history` where subscription_id = $sid and payment_date BETWEEN '$Y-12-01' and '$Y-12-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum12 = $sum12 + $res['sum'];

			$out .= "<td><strong>".FormatDefaultCurrency($sum)."</strong></td>";
		}
	}
	
	$query = "select * from `#__billing_services` order by id";
	$result = $db->setQuery($query);
	$servs = $db->loadObjectList();
	if (count($servs) > 0)
	{
		foreach($servs as $serv)
		{
			$sid = $serv->id;
			$out .= '<tr>';	
			$out .= "<td>$serv->service_name</td>";	
			
			$sum = 0;
			
			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_services_user` where service_id = $sid and paymentdate BETWEEN '$Y-01-01' and '$Y-01-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum1 = $sum1 + $res['sum'];		
					
			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_services_user` where service_id = $sid and paymentdate BETWEEN '$Y-02-01' and '$Y-02-28' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum2 = $sum2 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_services_user` where service_id = $sid and paymentdate BETWEEN '$Y-03-01' and '$Y-03-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum3 = $sum3 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_services_user` where service_id = $sid and paymentdate BETWEEN '$Y-04-01' and '$Y-04-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum4 = $sum4 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_services_user` where service_id = $sid and paymentdate BETWEEN '$Y-05-01' and '$Y-05-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum5 = $sum5 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_services_user` where service_id = $sid and paymentdate BETWEEN '$Y-06-01' and '$Y-06-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum6 = $sum6 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_services_user` where service_id = $sid and paymentdate BETWEEN '$Y-07-01' and '$Y-07-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum7 = $sum7 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_services_user` where service_id = $sid and paymentdate BETWEEN '$Y-08-01' and '$Y-08-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum8 = $sum8 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_services_user` where service_id = $sid and paymentdate BETWEEN '$Y-09-01' and '$Y-09-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum9 = $sum9 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_services_user` where service_id = $sid and paymentdate BETWEEN '$Y-10-01' and '$Y-10-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum10 = $sum10 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_services_user` where service_id = $sid and paymentdate BETWEEN '$Y-11-01' and '$Y-11-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum11 = $sum11 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_services_user` where service_id = $sid and paymentdate BETWEEN '$Y-12-01' and '$Y-12-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum12 = $sum12 + $res['sum'];

			$out .= "<td><strong>".FormatDefaultCurrency($sum)."</strong></td>";
		}
	}
	
	$out .= '<tr>';	
	$out .= "<td><strong>".JText::_('COM_BILLING_TOTAL')."</strong></td>";	
	$out .= "<td><strong>".FormatDefaultCurrency($sum1)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum2)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum3)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum4)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum5)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum6)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum7)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum8)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum9)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum10)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum11)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum12)."</strong></td>";
	$sum = $sum1 + $sum2 + $sum3 + $sum4 + $sum5 + $sum6 + $sum7 + $sum8 + $sum9 + $sum10 +$sum11 + $sum12;
	$out .= "<td><strong>".FormatDefaultCurrency($sum)."</strong></td>";
	
	$out .= '</tr>';	
	
	$out .= '</table>';
	return $out;
}

function ShowUserPoints()
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_point_actions` order by id";
	$result = $db->setQuery($query);
	$rows = $db->loadObjectList();
	
	JToolBarHelper::custom( 'addpoints', 'new', 'new', JText::_('COM_BILLING_ADDPOINTS'), false );

	
	$tabs = '';
	//$pane = JPane::getInstance('tabs', array('startOffset'=>0)); 
	//$tabs .= $pane->startPane( 'pane' );
	$tabs .= JHtml::_('tabs.start', 'up_tabs', array('startOffset'=>0));
	
	//$tabs .= $pane->startPanel( JText::_('COM_BILLING_POINTS'), 'points' );
	$tabs .= JHtml::_('tabs.panel', JText::_('COM_BILLING_POINTS'), 'points');

	$tabs .= '<table width="100%" class="adminlist table" cellspacing="1">';
	$tabs .= "<tr>";
	$tabs .= "  <th>Id</th>";
	$tabs .= "  <th>".JText::_('COM_BILLING_POINTS_ACTION')."</th>";
	$tabs .= "  <th>".JText::_('COM_BILLING_DESCRIPTION')."</th>";
	$tabs .= "  <th>".JText::_('COM_BILLING_POINTS')."</th>";
	$tabs .= "  <th>".JText::_('COM_BILLING_IS_ACTIVE')."</th>";
	$tabs .= "</tr>";
	
	if (count($rows) > 0)	
		{
			foreach ( $rows as $row ) 
			{
				$tabs .= '<tr>';
					$tabs .= "<td>$row->id</td>";
					$tabs .= "<td><a href='index.php?option=com_billing&task=editpoint&id=$row->id'>$row->action_name</a></td>";
					$tabs .= "<td>$row->description</td>";
					$tabs .= "<td>$row->points</td>";
					if ($row->enabled == 0)
					{
						$tabs .= "<td><a href='index.php?option=com_billing&task=activepoint&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_ACTIVATE')."' title='".JText::_('COM_BILLING_ACTIVATE')."' src='".publish_x."'></a></td>";
					}
					else
					{
						$tabs .= "<td><a href='index.php?option=com_billing&task=activepoint&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_HIDE')."' title='".JText::_('COM_BILLING_HIDE')."' src='". tick ."'></a></td>";
					}
				$tabs .= '</tr>';
			}
		}
	$tabs .= "</table>";
	
	//$tabs .= $pane->endPanel();		
	$tabs .= JHtml::_('tabs.end');
	echo $tabs;
}

function AddPoint($id = '')
{
	$db = JFactory::getDBO();
	if ($id != '')
	{
		$query = "select * from `#__billing_point_actions` where id = $id";
		$result = $db->setQuery($query);
		$row = $db->loadAssoc();
		$action_name = $row['action_name'];
		$description = $row['description'];
		$points = $row['points'];
		$enabled = $row['enabled'];
		$code = $row['data1'];
	}
	else
	{
		$action_name = '';
		$description = '';
		$points = 5;
		$enabled = 0;
		$code = '';
	}
	
	echo "<form method='post' action='index.php?option=com_billing&task=savepoint&id=$id'>";
	echo "<table border='0'>";
	echo "<tr><td>".JText::_('COM_BILLING_POINTS_ACTION')."</td><td><input class='inputbox' type='text' name='action_name' value='$action_name' size='100'></td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_DESCRIPTION')."</td><td><input class='inputbox' type='text' name='description' value='$description' size='100'></td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_CODE')."</td><td><input class='inputbox' type='text' name='code' value='$code' size='30'></td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_POINTS')."</td><td><input class='inputbox' type='text' name='points' value='$points' size='5'></td></tr>";
	if ($enabled == 1)
	{
		echo "<tr><td><input id='enabled' name='enabled' value='$enabled' type='checkbox' checked='on' onclick=\"document.getElementById('enabled').value = document.getElementById('enabled').checked;\"></td><td>".JText::_('COM_BILLING_IS_ACTIVE')."</td></tr>";
	}
	else
	{
		echo "<tr><td><input id='enabled' name='enabled' value='$enabled' type='checkbox' checked='false' onclick=\"document.getElementById('enabled').value = document.getElementById('enabled').checked;\"></td><td>".JText::_('COM_BILLING_IS_ACTIVE')."</td></tr>";
	}
	echo "<tr><td></td><td><input type='submit' value='".JText::_('COM_BILLING_SAVE')."'></td></tr>";
	echo "";
	echo "</table>";
	echo "</form>";
}

function AddPoints($uid)
{
	echo "<form method='post' action='index.php?option=com_billing&task=savepoints&uid=$uid'>";
	echo "<table border='0'>";
	echo "<tr><td>".JText::_('COM_BILLING_DESCRIPTION')."</td><td><input class='inputbox' type='text' name='description' value='' size='100'></td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_POINTS')."</td><td><input class='inputbox' type='text' name='points' value='' size='5'></td></tr>";
	echo "<tr><td></td><td><input type='submit' value='".JText::_('COM_BILLING_SAVE')."'></td></tr>";
	echo "</table>";
	echo "</form>";
}

function OutPoints($uid)
{
	echo "<form method='post' action='index.php?option=com_billing&task=outadmpoints&uid=$uid'>";
	echo "<table border='0'>";
	echo "<tr><td>".JText::_('COM_BILLING_DESCRIPTION')."</td><td><input class='inputbox' type='text' name='description' value='' size='100'></td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_POINTS')."</td><td><input class='inputbox' type='text' name='points' value='' size='5'></td></tr>";
	echo "<tr><td></td><td><input type='submit' value='".JText::_('COM_BILLING_SAVE')."'></td></tr>";
	echo "</table>";
	echo "</form>";
}

function SavePoints($id = '')
{
	$action_name = JRequest::getVar('action_name');
	$description = JRequest::getVar('description');
	$code = JRequest::getVar('code');
	$points = JRequest::getInt('points', 0);
	$enabled = JRequest::getInt('enabled', 0);
	
	$db = JFactory::getDBO();
	if ($id != '')
	{
		$query = "update `#__billing_point_actions` set `action_name` = '$action_name', `description` = '$description', `points` = $points, `enabled` = $enabled, `data1` = '$code' where id = $id";
	}
	else
	{
		$query = "insert into `#__billing_point_actions` (`action_name`, `description`, `points`, `enabled`, `data1`) values ('$action_name', '$description', $points, $enabled, '$code')";
	}
	$result = $db->setQuery($query);
	$row = $db->query();
}

function SaveAdmPoints($uid)
{
	$description = JRequest::getVar('description');
	$points = JRequest::getInt('points', 0);
	AddUserPoints($uid, '', $description, $points);
}

function ShowPins()
{
	$db = JFactory::getDBO();
	JToolBarHelper::custom( 'editpin', 'new', 'new', JText::_('COM_BILLING_ADD_PIN'), false );
	$query = "select p.*, (select count(*) from `#__billing_pin` where p.id = pin_type_id) as qty from `#__billing_pin_type` p order by id";
	$result = $db->setQuery($query);
	$rows = $db->loadObjectList();
	echo '<table class="adminlist table">';
	echo '<tr><th>Id</th><th>'.JText::_('COM_BILLING_ADD_PIN_NAME').'</th><th>'.JText::_('COM_BILLING_ITEM_TYPE').'</th><th>'.JText::_('COM_BILLING_COUNT').'</th><th>'.JText::_('COM_BILLING_IS_ACTIVE1').'</th></tr>';
	if(count($rows) > 0)
	{
		foreach($rows as $row)
		{
			echo '<tr>';
			echo "<td>$row->id</td>";
			echo "<td><a href='index.php?option=com_billing&task=editpin&id=$row->id'>$row->pin_name</a></td>";
			if($row->pin_type == 1)
			{
				echo "<td>". JText::_('COM_BILLING_PIN_INCREASE')."</td>";
			}
			elseif($row->pin_type == 2)
			{
				echo "<td>". JText::_('COM_BILLING_PIN_KEY')."</td>";
			}
			elseif($row->pin_type == 3)
			{
				echo "<td>". JText::_('COM_BILLING_PIN_PLUGINFILE')."</td>";
			}
			elseif($row->pin_type == 4)
			{
				echo "<td>". JText::_('COM_BILLING_PIN_SUB')."</td>";
			}
			else
			{
				echo "<td></td>";
			}
			echo "<td>$row->qty</td>";
			if($row->is_active == 0)
			{		
				echo "<td><a href='index.php?option=com_billing&task=activepin&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_BY_DEFAULT')."' title='".JText::_('COM_BILLING_BY_DEFAULT')."' src='".publish_x."'></a></td>";
			}
			else
			{
				echo "<td><a href='index.php?option=com_billing&task=activepin&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_HIDE')."' title='".JText::_('COM_BILLING_HIDE')."' src='". tick ."'></a></td>";
			}
			echo '</tr>';
		}
	}
	echo '</table>';

}

function EditPins($id = '')
{
	$db = JFactory::getDBO();
	if ($id != '')
	{
		$query = "select * from  `#__billing_pin_type` where id = $id";
		$result = $db->setQuery($query);
		$row = $db->loadAssoc();
		$pin_name = $row['pin_name'];
		$pin_type = $row['pin_type'];
		$license = $row['license'];
		$instructions = $row['instructions'];
		$price = $row['price'];
		$best_before = $row['best_before'];
		$data1 = $row['data1'];
		$data2 = $row['data2'];
		echo "<input type='hidden' value='$id' name='id'>";
	}
	else
	{
		$pin_name = '';
		$pin_type = 0;
		$best_before = '';
		$license = '';
		$instructions = '';
		$price = 0;
		$data1 = '';
		$data2 = '30';
	}
	JHTML::_('behavior.calendar');
	$editor = JFactory::getEditor();
	$editor2 = JFactory::getEditor();

	JToolBarHelper::custom( 'savepin', 'save', 'save', JText::_('COM_BILLING_SAVE'), false );
	if($id != '')
	{
		JToolBarHelper::custom( 'importpin', 'featured', 'featured', JText::_('COM_BILLING_IMPORT'), false );
		JToolBarHelper::custom( 'exportpin', 'export', 'export', JText::_('COM_BILLING_EXPORT'), false );
		JToolBarHelper::custom( 'generate', 'refresh', 'refresh', JText::_('COM_BILLING_GENERATE'), false );
	}
	JToolBarHelper::custom( 'cancelpin', 'cancel', 'cancel', JText::_('COM_BILLING_CANCEL'), false );

	echo "<table border='0'>";
	echo "<tr><td>".JText::_('COM_BILLING_ADD_PIN_NAME')."</td> <td><input class='inputbox' name='pin_name' value='$pin_name' type='text'></td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_PRICE')."</td> <td><input class='inputbox' name='price' value='$price' type='text' size='4'> ". GetDefaultCurrencyAbbr() ."</td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_INSTRUCTIONS')."</td> <td>".$editor->display( 'desc', $instructions, '600', '200', '20', '20', false )."</td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_LICENSE')."</td> <td>".$editor2->display( 'desc2', $license, '600', '200', '20', '20', false )."</td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_PIN_DATE')."</td> <td>".JHTML::_('calendar', $best_before, 'best_before', 'best_before', '%Y-%m-%d %H:%M:%S', array('class'=>'inputbox', 'size'=>'25',  'maxlength'=>'19'))."</td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_PIN_TYPE')."</td> <td>";
	echo '<select name="pin_type" onchange="if(this.value == 4) {document.getElementById(\'subs\').hidden = false;} else {document.getElementById(\'subs\').hidden = true;}">';
	if($pin_type == 1) {$sel = 'selected';} else {$sel = '';}
	echo "<option value='1' $sel>".JText::_('COM_BILLING_PIN_INCREASE')."</option>";
	if($pin_type == 2) {$sel = 'selected';} else {$sel = '';}
	echo "<option value='2' $sel>".JText::_('COM_BILLING_PIN_KEY')."</option>";
	if($pin_type == 3) {$sel = 'selected';} else {$sel = '';}
	echo "<option value='3' $sel>".JText::_('COM_BILLING_PIN_PLUGINFILE')."</option>";
	if($pin_type == 4) {$sel = 'selected';} else {$sel = '';}
	echo "<option value='4' $sel>".JText::_('COM_BILLING_PIN_SUB')."</option>";
	echo '</select>';
	if($pin_type == 4)
	{
		echo "<div id='subs'>";
	}
	else
	{
		echo "<div id='subs' hidden='true'>";
	}
	echo GetSubList($data1)." <input type='text' name='days' value='$data2' size='4'> ".JText::_('COM_BILLING_DAYS')."</div>";
	echo "</td> </tr>";
	echo "<tr><td>".JText::_('COM_BILLING_PIN_PLUGIN')."</td> <td><input class='inputbox' name='plugin' value='' type='file'></td></tr>";
	echo '</table><br>';
	
	if($id != '')
	{
		$query = "select p.* from `#__billing_pin` p where pin_type_id = $id order by id";
		$result = $db->setQuery($query);
		$rows = $db->loadObjectList();
		if(count($rows) > 0)
		{
			echo '<table class="adminlist table">';
			echo '<tr><th>Id</th><th>'.JText::_('COM_BILLING_PINCODE').'</th><th>'.JText::_('COM_BILLING_USER').'</th><th>'.JText::_('COM_BILLING_PRICE').'</th><th>'.JText::_('COM_BILLING_DATE').'</th></tr>';
			foreach($rows as $row)
			{
				echo '<tr>';
				echo "<td>$row->id</td>";
				echo "<td>$row->pin_code</td>";
				if($row->uid != '')
				{
					$user = JFactory::getUser($row->uid);
					echo "<td>$user->name [<a href='mailto:$user->email'>$user->email</a>]</td>";
				}
				else
				{
					echo "<td></td>";
				}
				if($row->price != '')
				{
					echo "<td>$row->price ".GetDefaultCurrencyAbbr()."</td>";
				}
				else
				{
					echo "<td></td>";
				}
				echo "<td>". FormatBillingDate($row->activation_date)."</td>";
				echo '</tr>';
			}
			echo '</table>';
		}
	}	
}

function SavePin($id = '')
{
	$db = JFactory::getDBO();
	$pin_name = JRequest::getVar('pin_name');
	$description = JRequest::getVar( 'desc', '', 'post', 'string', JREQUEST_ALLOWHTML );	
	$description = addslashes($description);
	$license = JRequest::getVar( 'desc2', '', 'post', 'string', JREQUEST_ALLOWHTML );	
	$license = addslashes($license);
	$best_before = JRequest::getVar('best_before');
	$pin_type = JRequest::getInt('pin_type');
	$data1 = JRequest::getInt('sub_id');
	$data2 = JRequest::getInt('days');
	$price = JRequest::getFloat('price', 0);

	if($_FILES['plugin']['tmp_name'] != '')
	{
		$tmp_name = $_FILES['plugin']['tmp_name'];
		$file_name = $_FILES['plugin']['name'];
		$s = uniqid(rand (), true);
		$new_name = JPATH_ROOT . '/components/com_billing/pins/' . $s . '.zip';
		
		$x = move_uploaded_file($tmp_name, $new_name);
		if ($x)
		{
			$path_to_pluginXML = UnArchive($new_name, $file_name, 'pins');
			unlink($new_name);
		}
		if (!file_exists($path_to_pluginXML))
		{
			JFactory::getApplication()->enqueueMessage( 'Plugin file not found' );
			return;
		}	
		//$xml = new JSimpleXML;
		$xml = JFactory::getXML($path_to_pluginXML);
			$name = $xml->document->name[0];
			$pluginfile = $xml->document->pluginfile[0];
			$pluginclass = $xml->document->pluginclass[0];
/*		{
			$name = '';
			$pluginfile = '';
			$pluginclass = '';
		}*/
	}
	else
	{
		$name = '';
		$pluginfile = '';
		$pluginclass = '';
	}
	
	if ($id != '')
	{
		$query = "update `#__billing_pin_type` set pin_name = '$pin_name', instructions = '$description', best_before = '$best_before', pin_type = $pin_type, price = $price, php_entry = '$name', php_class = '$pluginclass', php_filename = '$pluginfile', license = '$license', data1 = '$data1', data2 = '$data2' where id = $id";
	}
	else
	{
		$query = "insert into `#__billing_pin_type` (pin_name, instructions, best_before, pin_type, price, php_entry, php_class, php_filename, license, data1, data2) values ('$pin_name', '$description', '$best_before', $pin_type, $price, '$name', '$pluginclass', '$pluginfile', '$license', '$data1', '$data2')";
	}
	$result = $db->setQuery($query);
	$result = $db->query();
}

function ImportPin()
{
	$id = JRequest::getInt('id');
	echo '<br><form action="index.php?option=com_billing&task=pinimport" method="post" enctype="multipart/form-data">';
	echo "<input type='hidden' value='$id' name='id'>";
	echo JText::_('COM_BILLING_SELECT_FILE') ." CSV <input class='inputbox' type='file' name='csvfile' size='100'> <input type='submit' value='".JText::_('COM_BILLING_ADD')."'>";
	echo "<br>". JText::_('COM_BILLING_CSV_DESC');
	echo '</form>';
}

function ImportPin2()
{
	$id = JRequest::getInt('id');
	$uploadfile = JPATH_ROOT.'/components/com_billing/'.rand(1000, 10000).'pincodes.csv';
	move_uploaded_file($_FILES['csvfile']['tmp_name'], $uploadfile);
	$csvx = file($uploadfile);
	$db = JFactory::getDBO();
	foreach($csvx as $csv)
	{
		$arr = explode(';', $csv);
		//PIN_CODE;STATE(1 РёР»Рё 0);PRICE;DATE;EMAIL
		$pincode = trim($arr[0]);
		$state = trim($arr[1]);
		if($state == '')
		{
			$state = 0;
		}
		$price = trim($arr[2]);
		if($price == '')
		{
			$price = 0;
		}
		$date = trim($arr[3]);
		$query = "select count(*) from `#__billing_pin` where pin_code = '$pincode'";
		$result = $db->setQuery($query);
		$n = $db->loadResult();
		if ($n == 0)
		{
			$query = "insert into `#__billing_pin` (pin_type_id, pin_code, activated, price, activation_date) values ($id, '$pincode', $state, $price, '$date')";
			$result = $db->setQuery($query);
			$n = $db->query();
		}
	}
	unlink($uploadfile);
	EditPins($id);
}

function ExportPin()
{
	$id = JRequest::getInt('id');
	$db = JFactory::getDBO();
	$query = "select p.* from `#__billing_pin` p where pin_type_id = $id order by id";
	$result = $db->setQuery($query);
	$rows = $db->loadObjectList();
	if(count($rows) > 0)
	{
	   $d = JPATH_ROOT.'/components/com_billing/'.rand(1000, 10000).'pincodes.csv';
	   $f = fopen($d , "w");
	   $s = "Id;Pin-Code;Activated;User Id;Activation date;Price\n";
	   fwrite($f, $s);
	   foreach ( $rows as $row ) 
	   {
			$s = $row->id . ';' . $row->pin_code . ';' . $row->activated . ';' . $row->uid . ';' . $row->activation_date . ';' . $row->price . "\n";
			fwrite($f, $s);
	   }
		fclose($f);
		$file = $d;
		
		$xml = JFactory::getXML($file);
		
		if (file_exists($file)) 
		{
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=pincodes.csv');
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			ob_clean();
			flush();
			readfile($file);
		exit;
		}		
	}
}

function GeneratePin()
{
	$id = JRequest::getInt('id');
	echo '<form method="post" action="index.php?option=com_billing&task=generatego">';
	echo "<input type='hidden' value='$id' name='id'>";
	echo "<table border='0'>";
	echo "<tr><td>".JText::_('COM_BILLING_SERIES')."</td><td><input class='inputbox' type='text' name='series' value='4' size='4'></td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_SECTIONS')."</td><td><input class='inputbox' type='text' name='section' value='4' size='4'></td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_SEPARATOR')."</td><td><input class='inputbox' type='text' name='separator' value='-' size='4'></td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_COUNT')."</td><td><input class='inputbox' type='text' name='qty' value='100' size='4'></td></tr>";
	echo "<tr><td></td><td><input type='submit' name='sub' value='".JText::_('COM_BILLING_GENERATE')."'></td></tr>";
	echo "</table>";
	echo '</form>';
}

function GeneratePinGo()
{
	$db = JFactory::getDBO();
	$id = JRequest::getInt('id');
	$series = JRequest::getInt('series', 4);
	$section = JRequest::getInt('section', 4);
	$separator = JRequest::getVar('separator');
	$qty = JRequest::getInt('qty', 100);
	$start = '';
	for($i = 1; $i <= ($series - 1); $i++)
	{
		$start .= '9';
	}
	$start = $start + 1;
	$finish = '';
	for($i = 1; $i <= $series; $i++)
	{
		$finish .= '9';
	}
	$finish = $finish + 0;
	
	for($i = 1; $i <= $qty; $i++)
	{
		$s = '';
		for($j = 1; $j <= $section; $j++)
		{
			if($s == '')
			{
				$s = rand($start, $finish);
			}
			else
			{
				$s .= $separator . rand($start, $finish);
			}
		}
		//echo $s . '<br>';
		$query = "select count(*) from `#__billing_pin` where pin_code = '$s'";
		$result = $db->setQuery($query);
		$result = $db->loadResult();
		if($result == 0)
		{
			$query = "insert into `#__billing_pin` (pin_type_id, pin_code, activated) values ($id, '$s', 0)";
			$result = $db->setQuery($query);
			$result = $db->query();
		}
	}
	EditPins($id);
}

function ActivatePin($id)
{
	//
}

function ShowProjects($order, $limitstart, $limit)
{
	echo '<p>' . JText::_('COM_BILLING_ABOUT_PROJECT') . '</p><br><br>';
	$db = JFactory::getDBO();
	JToolBarHelper::custom( 'addproject', 'new', 'new', JText::_('COM_BILLING_ADD_PROJECT'), false );
	$query = "select p.*, (select sum(psum) from `#__billing_project_user` where project_id = p.id) as psum from  `#__billing_project` p order by id";
	$result = $db->setQuery($query);
	$rows = $db->loadObjectList();
	echo '<table class="adminlist table">';
	echo '<tr><th>Id</th><th>'.JText::_('COM_BILLING_PROJECT_NAME').'</th><th>'.JText::_('COM_BILLING_PROJECT_SUM2').'</th><th>'.JText::_('COM_BILLING_START_PROJECT').'</th><th>'.JText::_('COM_BILLING_END_PROJECT').'</th> <th>'.JText::_('COM_BILLING_IS_ACTIVE1').'</th></tr>';
	if(count($rows) > 0)
	{
		foreach($rows as $row)
		{
			echo '<tr>';
			echo "<td>$row->id</td>";
			echo "<td><a href='index.php?option=com_billing&task=editproject&id=$row->id'>$row->project_name</a></td>";
			echo "<td>$row->psum ".GetDefaultCurrencyAbbr()." [$row->project_sum ".GetDefaultCurrencyAbbr()."]</td>";
			echo "<td>". FormatBillingDate($row->start_date)."</td>";
			echo "<td>". FormatBillingDate($row->finish_date)."</td>";
			if($row->is_active == 0)
			{		
				echo "<td><a href='index.php?option=com_billing&task=activeproject&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_BY_DEFAULT')."' title='".JText::_('COM_BILLING_BY_DEFAULT')."' src='".publish_x."'></a></td>";
			}
			else
			{
				echo "<td><a href='index.php?option=com_billing&task=activeproject&id=$row->id'><img width='16' height='16' border='0' alt='".JText::_('COM_BILLING_HIDE')."' title='".JText::_('COM_BILLING_HIDE')."' src='". tick ."'></a></td>";
			}
			echo '</tr>';
		}
	}
	echo '</table>';
}

function EditProject($id = '')
{
	$db = JFactory::getDBO();
	if ($id != '')
	{
		$query = "select * from  `#__billing_project` where id = $id";
		$result = $db->setQuery($query);
		$row = $db->loadAssoc();
		$project_name = $row['project_name'];
		$description = $row['description'];
		$start_date = $row['start_date'];
		$finish_date = $row['finish_date'];
		$project_sum = $row['project_sum'];
		echo "<input type='hidden' value='$id' name='id'>";
	}
	else
	{
		$project_name = '';
		$description = '';
		$start_date = date('Y-m-d H:i:s');
		$finish_date = '';
		$project_sum = 0;
	}
	JHTML::_('behavior.calendar');
	$editor = JFactory::getEditor();

	JToolBarHelper::custom( 'saveproject', 'save', 'save', JText::_('COM_BILLING_SAVE'), false );
	JToolBarHelper::custom( 'cancelprojects', 'cancel', 'cancel', JText::_('COM_BILLING_CANCEL'), false );
	echo "<table border='0'>";
	echo "<tr><td>".JText::_('COM_BILLING_PROJECT_NAME')."</td> <td><input class='inputbox' name='project_name' value='$project_name' type='text'></td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_DESCRIPTION')."</td> <td>".$editor->display( 'desc', $description, '600', '200', '20', '20', false)."</td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_START_PROJECT')."</td> <td>".JHTML::_('calendar', $start_date, 'start_date', 'start_date', '%Y-%m-%d %H:%M:%S', array('class'=>'inputbox', 'size'=>'25',  'maxlength'=>'19'))."</td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_END_PROJECT')."</td> <td>".JHTML::_('calendar', $finish_date, 'finish_date', 'finish_date', '%Y-%m-%d %H:%M:%S', array('class'=>'inputbox', 'size'=>'25',  'maxlength'=>'19'))."</td></tr>";
	echo "<tr><td>".JText::_('COM_BILLING_PROJECT_SUM')."</td> <td><input class='inputbox' name='project_sum' value='$project_sum' type='text'> ".GetDefaultCurrencyAbbr()."</td></tr>";
	//echo "<tr><td>".JText::_('COM_BILLING_CANCEL')."</td> <td><input name='' value='' type='text'></td></tr>";
	echo '</table>';
	
	if($id != '')
	{
		$query = "select * from  `#__billing_project_user` where project_id = $id order by send_date desc";
		$result = $db->setQuery($query);
		$rows = $db->loadObjectList();
		if(count($rows) > 0)
		{
			echo '<table class="adminlist table">';
			echo '<tr><th>Id</th><th>'.JText::_('COM_BILLING_USER').'</th><th>'.JText::_('COM_BILLING_SUM').'</th><th>'.JText::_('COM_BILLING_DATE').'</th></tr>';
			foreach($rows as $row)
			{
				echo '<tr>';
				echo "<td>$row->id</td>";
				$user = JFactory::getUser($row->uid);
				echo "<td><a href=''>$user->name</a> [<a href='mailto:$user->email'>$user->email</a>]</td>";
				echo "<td>$row->psum ".GetDefaultCurrencyAbbr()."</td>";
				echo "<td>". FormatBillingDate($row->send_date)."</td>";
				echo '</tr>';
			}
			echo '</table>';
		}
	}
}

function SaveProject()
{
	$id = (int)JRequest::getCmd('id');
	$db = JFactory::getDBO();
	$project_name = JRequest::getVar('project_name');
	$description = JRequest::getVar( 'desc', '', 'post', 'string', JREQUEST_ALLOWHTML );
	$description = addslashes($description);	
	$start_date = JRequest::getVar('start_date');
	$finish_date = JRequest::getVar('finish_date');
	$project_sum = JRequest::getFloat('project_sum', 0);	

	if ($id != '')
	{
		$query = "update `#__billing_project` set project_name = '$project_name', description = '$description', start_date = '$start_date', finish_date = '$finish_date', project_sum = $project_sum where id = $id";
	}
	else
	{
		$query = "insert into `#__billing_project` (project_name, description, start_date, finish_date, project_sum) values ('$project_name', '$description', '$start_date', '$finish_date', $project_sum)";
	}
	$result = $db->setQuery($query);
	$result = $db->query();
}

function ActivateProject($id)
{
	$db = JFactory::getDBO();
	$query = 'select * from `#__billing_project` where id = ' . $id;
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();
	if ($row['is_active'] == 0)
	{
	    $a = 1;
	}
	else
	{
	    $a = 0;
	}
	$query = "update `#__billing_project` set is_active = $a where id = ". $id;
	$result = $db->setQuery($query);
	$result = $db->query();   
}

function DelArticleFile($id)
{
	$db = JFactory::getDBO();
	$query = 'select * from `#__billing_articles_file` where id = ' . $id;
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();
	unlink(JPATH_ROOT . '/components/com_billing/files/' . $row['newname']);
	$aid = $row['article_id'];
	$query = "delete from `#__billing_articles_file` where id = $id";	
	$result = $db->setQuery($query);
	$result = $db->query();
	return $aid;
}

function SetTabState($id)
{
/*	switch($id)
	{
		case -1:
			SaveOption('baltab', !GetOption('baltab'));
		break;
		case -2:
			SaveOption('addtab', !GetOption('addtab'));
		break;
		case -3:
			SaveOption('subscr', !GetOption('subscr'));
		break;
		case -4:
			SaveOption('files', !GetOption('files'));
		break;
		case -5:
			SaveOption('music', !GetOption('music'));
		break;
		case -6:
			SaveOption('articles', !GetOption('articles'));
		break;
		case -7:
			SaveOption('partner_tab', !GetOption('partner_tab'));
		break;
		case -8:
			SaveOption('accountstab', !GetOption('accountstab'));
		break;
		case -9:
			SaveOption('projectstab', !GetOption('projectstab'));
		break;
		case -10:
			SaveOption('userpointstab', !GetOption('userpointstab'));
		break;		
		case -11:
			SaveOption('service', !GetOption('service'));
		break;
	}
*/
}
  
function Freeze($id, $uid)
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_subscription` where subscription_id = $id and uid = $uid";
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();
	if($row['frozen'] == 1)
	{
		$n = 0;
	}
	else
	{
		$n = 1;
	}
	$query = "update `#__billing_subscription` set frozen = $n where subscription_id = $id and uid = $uid";
	$result = $db->setQuery($query);
	$res = $db->query();
}

function AddSubListItem($id)
{
	$title = JRequest::getVar('title');
	$db = JFactory::getDBO();
	$query = "insert into `#__billing_subscription_list` (subscription_id, title) values ($id, '$title')";
	$result = $db->setQuery($query);
	$row = $db->query();
}

function DelSubListItem($id)
{
	$db = JFactory::getDBO();
	$query = "delete from `#__billing_subscription_list` where id = $id";	
	$result = $db->setQuery($query);
	$row = $db->query();
}  

function SortTabs()
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_tabs` order by taborder";
	$result = $db->setQuery($query);
	$rows = $db->loadObjectList();
	$n = 1;
	foreach($rows as $row)
	{
		$query = "update `#__billing_tabs` set taborder = $n where id = $row->id";
		$n++;
		$result = $db->setQuery($query);
		$row = $db->query();
	}
}

function OrderUp($id)
{	
	SortTabs();
	$db = JFactory::getDBO();
	$query = "select taborder from `#__billing_tabs` where id = $id";
	$result = $db->setQuery($query);
	$taborder = $db->loadResult();
	$tabordern1 = $taborder - 1;
	
	$query = "update `#__billing_tabs` set taborder = $taborder where taborder = $tabordern1";
	$result = $db->setQuery($query);
	$row = $db->query();
	
	$query = "update `#__billing_tabs` set taborder = $tabordern1 where id = $id";
	$result = $db->setQuery($query);
	$row = $db->query();
	ShowTabs();
}

function OrderDown($id)
{
	SortTabs();
	$db = JFactory::getDBO();
	$query = "select taborder from `#__billing_tabs` where id = $id";
	$result = $db->setQuery($query);
	$taborder = $db->loadResult();
	$tabordern1 = $taborder + 1;
	
	$query = "update `#__billing_tabs` set taborder = $taborder where taborder = $tabordern1";
	$result = $db->setQuery($query);
	$row = $db->query();
	
	$query = "update `#__billing_tabs` set taborder = $tabordern1 where id = $id";
	$result = $db->setQuery($query);
	$row = $db->query();
	ShowTabs();	
}

function EditUserSub($id)
{
	$uid = JRequest::getInt('uid');
	$db = JFactory::getDBO();
	
	$query = "select * from `#__billing_subscription` where id = $id";
	$result = $db->setQuery($query);
	$sub = $db->loadObject();

	$query = "select * from `#__billing_subscription_type` where id = $sub->subscription_id";
	$result = $db->setQuery($query);
	$sub_type = $db->loadObject();
	
	JToolBarHelper::custom( 'saveusersub', 'save', 'save', JText::_('COM_BILLING_ADD'), false );
	JToolBarHelper::custom( 'cancelusersub', 'cancel', 'cancel', JText::_('COM_BILLING_ADD'), false );
	
	echo "<input type='hidden' name='id' value='$id'>";
	echo "<input type='hidden' name='uid' value='$uid'>";
	JHTML::_('behavior.calendar');
	echo "<h2 class='sub'>" . JText::_('COM_BILLING_SUB_TITLE') . " $sub_type->subscription_type</h2>";
	echo "<p class='sub'>" . JText::_('COM_BILLING_SUB_COST') . " ". FormatDefaultCurrency($sub_type->price). "</p>";
	echo "<p class='sub'>" . JText::_('COM_BILLING_SUB_PERIOD_DAYS') . " $sub_type->subscription_days</p>";
	
	echo "<p class='sub'>" . JText::_('COM_BILLING_SUB_END') . " ". JHTML::_('calendar', $sub->enddate, 'enddate', 'enddate', '%Y-%m-%d', array('class'=>'inputbox', 'size'=>'25',  'maxlength'=>'19')) ."</p>";
	echo "<p class='sub'>" . JText::_('COM_BILLING_SUB_RECALC') . " <input type='checkbox' name='recalc' value='false' onclick='this.value=this.checked;'></p>";
}

function SaveUserSub($id)
{
	$uid = JRequest::getInt('uid');
	$id = JRequest::getInt('id');
	$enddate = JRequest::getVar('enddate');
	$recalc = JRequest::getVar('recalc');
	
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_subscription` where id = $id";
	$result = $db->setQuery($query);
	$sub = $db->loadObject();
	
	$query = "update `#__billing_subscription` set `enddate` = '$enddate' where id = $id";
	$result = $db->setQuery($query);
	$result = $db->query();
	
	if($recalc)
	{
		$query = "select * from `#__billing_subscription_type` where id = $sub->subscription_id";
		$result = $db->setQuery($query);
		$sub_type = $db->loadObject();
		$day_price = $sub_type->subscription_days / $sub_type->price;

		$UA = new UserAccount();
		$pid = date('His') . $uid;
		
		if($enddate > $sub->enddate)
		{
			$end = strtotime($enddate);
			$start = strtotime($sub->enddate);
			$days = round(($end - $start)/ 86400);
			$price = $days * $day_price;
			$descr = JText::_( 'COM_BILLING_WRITING_OFF') . " Продление подписки";
			$UA->WithdrawMoney($uid, $price, $pid, $descr);
		}
		if($enddate < $sub->enddate)
		{
			$end = strtotime($sub->enddate);
			$start = strtotime($enddate);
			$days = round(($end - $start)/ 86400);
			$price = $days * $day_price;
			$descr = JText::_( 'COM_BILLING_ADD2_MONEY') . " Сокращение подписки";
			$UA->AddMoney($uid, $price, $pid, $descr);
		}
	}
}

function GetAdsManagerCategory($id)
{
	if(file_exists(JPATH_ROOT . '/components/com_adsmanager/adsmanager.php'))
	{
		$db = JFactory::getDBO();
		$query = "select `name` from `#__adsmanager_categories` where id = $id";
		$result = $db->setQuery($query);
		return $db->loadResult();
	}
	else
	{
		return "AdsManager not found";
	}
}

function GetAdsCatChildren($id, $level, $sel)
{
	$out = "";
	$db = JFactory::getDBO();
	$db->setQuery("select * from `#__adsmanager_categories` where parent=$id order by name");
	$items = $db->loadObjectList();

	foreach ($items as $item)
	{
		$item_title = str_repeat('- ', $level) . $item->name;
		if ($item->id == $sel) {$s = 'selected';} else {$s = '';}
		$out .= "<option value='$item->id' $s>$item_title</option>"; 
		$out .= GetAdsCatChildren($item->id,$level+1,$sel);
	}
	return $out;
}

function GetAdsCategoryList($id = '')
{
	$out = '';
	$out .= '<select name="ads_id">';
	$out .= GetAdsCatChildren(0,0,$id);
	$out .= '</select>';
	return $out;	
}  
JToolBarHelper::title( JText::_( 'COM_BILLING_BILLING' ), 'generic.png' );  
?>
