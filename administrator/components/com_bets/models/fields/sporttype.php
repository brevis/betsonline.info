<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

JFormHelper::loadFieldClass('list');

class JFormFieldSportType extends JFormFieldList {

	protected $type = 'SportType';

	public function getOptions() {
		$app = JFactory::getApplication();

		/*
		$country = $app->input->get('country'); //country is the dynamic value which is being used in the view
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('a.cityname')->from('`#__tablename` AS a')->where('a.country = "'.$country.'" ');
		$rows = $db->setQuery($query)->loadObjectlist();
		foreach($rows as $row){
			$cities[] = $row->cityname;
		}
		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $cities);
		*/

		$options = array();
		$options[''] = JText::_('COM_BETS_FILTER_SPOT_TYPE_ALL');
		foreach (ResultsHelper::getAllTypes() as $id=>$t)
		{
			$options[$id] = $t;
		}

		return $options;
	}
}