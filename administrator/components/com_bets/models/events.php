<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Events model class.
 *
 * @package     Bets
 * @subpackage  Models
 */
class BetsModelEvents extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query = $db->getQuery(true);

		$query->select('b.*, b.1 as p1, b.2 as p2, b.12 as p12, b.1X as X1, b.1_s as s_1, b.1_s as s_2');
		$query->from('bets b');

		//$query->select('bs.title as section_title');
		$query->join('LEFT', 'bets_sections bs ON (b.sectionid=bs.sectionid)');

		//$query->select('(SELECT COUNT(*) FROM bets b2 WHERE b2.parent=b.id) as child');
		//$query->select('(SELECT COUNT(*) FROM bets_details bd WHERE bd.fonbetid=b.fonbetid) as adtresults');
		$query->select('0 as child');
		$query->select('0 as adtresults');

		$query->where('b.parent = 0');

		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (is_numeric($search))
			{
				$query->where('b.id = ' . $search);
			}
			else
			{
				$like = $db->quote('%' . $search . '%');
				$query->where('b.title LIKE ' . $like );
			}
		}

		$type = (int)$this->getState('filter.type');
		if ($type > 0)
		{
			$sections = ResultsHelper::getSectionsByType($type, true);
			if (is_array($sections) && count($sections) > 0)
			{
				$sid = array();
				foreach($sections as $s)
				{
					$sid[] = $s->sectionid;
				}

				$query->where('b.sectionid IN (' . implode(',', $sid) . ')');
			}
		}

		$date_from = $this->getState('filter.date_from');
		if (!empty($date_from))
		{
			$query->where('STR_TO_DATE(b.`datetime`, \'%d.%m.%Y %H:%i\') >= \'' . $db->escape($date_from) . ' 00:00:00\'');
		}

		$date_to = $this->getState('filter.date_to');
		if (!empty($date_to))
		{
			$query->where('STR_TO_DATE(b.`datetime`, \'%d.%m.%Y %H:%i\') <= \'' . $db->escape($date_to) . ' 23:59:59\'');
		}

		// Add the list ordering clause.
		$orderCol	= $this->state->get('list.ordering', 'b.id');
		$orderDirn 	= $this->state->get('list.direction', 'desc');

		$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));

		return $query;
	}
}
