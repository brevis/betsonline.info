<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Bets model class.
 *
 * @package     Bets
 * @subpackage  Models
 */
class BetsModelBets extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
				->select('b.*, b.1 as p1, b.2 as p2, b.12 as p12, b.1X as X1, b.1_s as s_1, b.1_s as s_2, ub.id as bet_id, ub.uid as bet_uid, ub.created as bet_created, ub.type as bet_type, ub.bet_sum as bet_sum, ub.title as bet_title, ub.coef as bet_coef, ub.processed as bet_processed, ub.win as bet_win, b2.title as parent_title, s.title as section_title, r.status as result_status, r.result as result_result, u.username')
				->from('users_bets ub');

		$query->join('LEFT', 'bets b ON (ub.event = b.id)');
		$query->join('LEFT', 'bets b2 ON (b2.id = b.parent)');
		$query->join('LEFT', 'bets_sections s ON (b.sectionid = s.sectionid)');
		$query->join('LEFT', 'results r ON (b.id = r.parser_betid)');
		$query->join('LEFT', '#__users u ON (ub.uid = u.id)');

		$query->group('b.id');

		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (is_numeric($search))
			{
				$query->where('ub.id = ' . $search);
			}
			elseif (preg_match('/\d{4}-\d{2}-\d{2}/', $search))
			{
				if (preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/', $search))
				{
					$d = new DateTime($search);
					$d = $d->format('Y-m-d H:i:s');
					$query->where('ub.created = \'' . $d . '\'');
				}
				elseif (preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}/', $search))
				{
					$d = new DateTime($search);
					$d = $d->format('Y-m-d H:i');
					$query->where('ub.created >= \'' . $d . ':00\' AND ub.created <= \'' . $d . ':59\'');
				}
				else
				{
					$d = new DateTime($search);
					$d = $d->format('Y-m-d');
					$query->where('ub.created >= \'' . $d . ' 00:00:00\' AND ub.created <= \'' . $d . ' 23:59:59\'');
				}
			}
			else
			{
				$like = $db->quote('%' . $search . '%');
				$query->where('b.title LIKE ' . $like . ' OR ub.title LIKE ' . $like . ' OR b2.title LIKE ' . $like);
			}
		}

		$processed = $this->getState('filter.processed');
		if (is_numeric($processed))
		{
			if ($processed == 1)
			{
				$query->where('ub.processed <> 0');
			}
			else
			{
				$query->where('ub.processed = 0');
			}
		}

		$type = (int)$this->getState('filter.type');
		if ($type > 0)
		{
			$sections = ResultsHelper::getSectionsByType($type, true);
			if (is_array($sections) && count($sections) > 0)
			{
				$sid = array();
				foreach($sections as $s)
				{
					$sid[] = $s->sectionid;
				}

				$query->where('b.sectionid IN (' . implode(',', $sid) . ')');
			}
		}

		$date_from = $this->getState('filter.date_from');
		if (!empty($date_from))
		{
			$query->where('ub.created >= \'' . $db->escape($date_from) . ' 00:00:00\'');
		}

		$date_to = $this->getState('filter.date_to');
		if (!empty($date_to))
		{
			$query->where('ub.created <= \'' . $db->escape($date_to) . ' 23:59:59\'');
		}

		// Add the list ordering clause.
		$orderCol	= $this->state->get('list.ordering', 'ub.created');
		$orderDirn 	= $this->state->get('list.direction', 'desc');

		$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));

		return $query;
	}
}
