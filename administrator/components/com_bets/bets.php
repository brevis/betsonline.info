<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Set some global property
$document = JFactory::getDocument();

// Access check: is this user allowed to access the backend of this component?
if (!JFactory::getUser()->authorise('core.manage', 'com_bets'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Require helper file
JLoader::register('BetsHelper', JPATH_COMPONENT . '/helpers/bets.php');

require_once JPATH_ROOT . '/components/com_bets/helpers/bet.php';
require_once JPATH_ROOT . '/components/com_bets/helpers/line.php';
require_once JPATH_ROOT . '/components/com_bets/helpers/results.php';

// Get an instance of the controller prefixed by Bets
$controller = JControllerLegacy::getInstance('Bets');

// Perform the Request task
$controller->execute(JFactory::getApplication()->input->get('task'));;

// Redirect if set by the controller
$controller->redirect();