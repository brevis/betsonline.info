DROP TABLE IF EXISTS `users_bets`;
CREATE TABLE `users_bets` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `event` int NOT NULL,
  `type` int NOT NULL,
  `uid` int NOT NULL,
  `bet_sum` decimal(10,4) NOT NULL,
  `created` datetime NOT NULL,
  `processed` int(1) tinyint NOT NULL,
  `result` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
	`coef` decimal(10,4) NOT NULL,
	`event_date` varchar(20) NOT NULL,
	`win` tinyint(4) NOT NULL
);

ALTER TABLE `users_bets`
ADD INDEX `event_result` (`event`, `result`);