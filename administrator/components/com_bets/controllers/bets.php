<?php defined('_JEXEC') or die('Restricted access');

/**
 * Bets Controller
 *
 * @since  1.0.0
 */
class BetsControllerBets extends JControllerAdmin
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string $name   The model name. Optional.
	 * @param   string $prefix The class prefix. Optional.
	 * @param   array  $config Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Bet', $prefix = 'BetsModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function returnbets()
	{
		$app = JFactory::getApplication();

		$cid = $app->input->get('cid', array(), 'ARRAY');
		$cid = array_filter($cid, function($item){
			return is_numeric($item);
		});

		$message = null;
		if (count($cid) > 0)
		{
			$ua = new UserAccount();
			$db = JFactory::getDbo();

			foreach($cid as $id)
			{
				$bet = BetHelper::getBetById($id, true);
				if (!$bet || $bet->bet_processed == 1) continue;

				$betTitle = '#' . $bet->bet_id . ': ' . $bet->datetime . ', '
						. LineHelper::getEventTitle($bet) . ', ' . $bet->bet_title;
				$m = JText::sprintf('COM_BETS_RETURN_BET_NOTIFY', $betTitle);
				$pid = $this->getPid($bet->bet_uid);
				$ua->AddMoneyEasy($bet->bet_uid, $bet->bet_sum, $pid, $m);

				$db->setQuery("UPDATE users_bets SET processed = 1, win = 2 WHERE id =" . (int) $bet->bet_id)->execute();
			}

			$message = JText::_('COM_BETS_BETS_WAS_RETURNED');
		}

		$this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list, false), $message);
	}

	protected function getPid($uid)
	{
		list($whole, $decimal) = explode(' ', microtime());
		$decimal = substr($decimal, strlen($decimal) - 5, 5);
		return $decimal . $uid;
	}
}