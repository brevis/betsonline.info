<?php defined('_JEXEC') or die('Restricted access');

/**
 * Event Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_bets
 * @since       1.0.0
 */
class BetsControllerEvent extends JControllerForm
{
	/**
	 * Implement to allowAdd or not
	 *
	 * Not used at this time (but you can look at how other components use it....)
	 * Overwrites: JControllerForm::allowAdd
	 *
	 * @param array $data
	 *
	 * @return bool
	 */
	protected function allowAdd($data = array())
	{
		return parent::allowAdd($data);
	}

	/**
	 * Implement to allow edit or not
	 * Overwrites: JControllerForm::allowEdit
	 *
	 * @param array  $data
	 * @param string $key
	 *
	 * @return bool
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		$id = isset($data[$key]) ? $data[$key] : 0;
		if (!empty($id))
		{
			return JFactory::getUser()->authorise("core.edit", "com_bets.message." . $id);
		}
	}


	public function save($key = null, $urlVar = null)
	{
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();

		$context = "$this->option.edit.$this->context";
		$task = $this->getTask();
		$urlVar = 'id';

		$betId = $app->input->getInt('id');
		if ($betId == 0)
		{
			$betId = $this->createNewBetByRequest();
		}
		else
		{
			$coef = $app->input->get('coef', array(), 'ARRAY');
			foreach($coef as $id=>$data)
			{
				$query = array();
				foreach($data as $k=>$v)
				{
					$v = floatval($v);
					if ($v == 0)
					{
						$v = '';
					}
					$query[] = '`' . $k . '` = \'' . $v . '\'';
				}

				if (count($query) > 0)
				{
					$query = "UPDATE bets SET " . implode(", ", $query) . " WHERE id = " . (int) $id;
					$db->setQuery($query)->execute();
				}
			}
		}

		switch ($task)
		{
			case 'apply':
				// Redirect back to the edit screen.
				$this->setRedirect(
						JRoute::_(
								'index.php?option=' . $this->option . '&view=' . $this->view_item
								. $this->getRedirectToItemAppend($betId, $urlVar), false
						),
						JText::_('COM_BETS_EVENT_SAVED')
				);
				break;

			default:
				// Redirect to the list screen.
				$this->setRedirect(
						JRoute::_(
								'index.php?option=' . $this->option . '&view=' . $this->view_list
								. $this->getRedirectToListAppend(), false
						),
						JText::_('COM_BETS_EVENT_SAVED')
				);
				break;
		}
	}

	protected function createNewBetByRequest()
	{
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();

		$event = $app->input->get('event', array(), 'ARRAY');
		$coef = $app->input->get('coef', array(), 'ARRAY');
		$data = isset($coef[0]) ? $coef[0] : array();
		if (count($data) < 1) return null;

		$query = array(
			"`title` = '".$db->escape($event['title'])."'",
			"`datetime` = '".$db->escape($event['datetime'])."'",
			"`sectionid` = '".$db->escape($event['sectionid'])."'",
		);
		foreach($data as $k=>$v)
		{
			$v = floatval($v);
			if ($v == 0)
			{
				$v = '';
			}
			$query[] = '`' . $k . '` = \'' . $v . '\'';
		}

		$db->setQuery("INSERT INTO bets SET " . implode(", ", $query))->execute();

		return $db->insertid();
	}
}