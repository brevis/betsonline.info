<?php defined('_JEXEC') or die('Restricted access');

/**
 * Bet Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_bets
 * @since       1.0.0
 */
class BetsControllerBet extends JControllerForm
{
	/**
	 * Implement to allowAdd or not
	 *
	 * Not used at this time (but you can look at how other components use it....)
	 * Overwrites: JControllerForm::allowAdd
	 *
	 * @param array $data
	 *
	 * @return bool
	 */
	protected function allowAdd($data = array())
	{
		return parent::allowAdd($data);
	}

	/**
	 * Implement to allow edit or not
	 * Overwrites: JControllerForm::allowEdit
	 *
	 * @param array  $data
	 * @param string $key
	 *
	 * @return bool
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		$id = isset($data[$key]) ? $data[$key] : 0;
		if (!empty($id))
		{
			return JFactory::getUser()->authorise("core.edit", "com_bets.message." . $id);
		}
	}


	public function process()
	{
		$app = JFactory::getApplication();

		$betId = $app->input->getInt('id');
		$bet = BetHelper::getBetById($betId, true);
		if (!$bet)
		{
			$app->redirect(JRoute::_('index.php?option=com_bets'), JText::_('COM_BETS_BET_NOT_FOUND'));
		}

		$betStatus = $app->input->getString('bet_status');
		if ($bet->bet_processed != 1 && in_array($betStatus, array('win', 'lost'), true))
		{
			$unprocessedBets = $this->getUnprocessedBetsLike($bet);
			if (is_array($unprocessedBets) && count($unprocessedBets) > 0)
			{
				$db = JFactory::getDbo();
				$ua = new UserAccount();

				foreach($unprocessedBets as $i=>$ub)
				{
					if ($betStatus == 'win')
					{
						$winValue = $this->getBetWin($ub);
						$betTitle = '#' . $ub->bet_id . ': ' . $ub->datetime . ', '
							. LineHelper::getEventTitle($ub) . ', ' . $ub->bet_title;
						$message = JText::sprintf('COM_BETS_WIN_ADD_MONEY_NOTIFY', $betTitle);
						$pid = $this->getPid($ub->bet_uid);
						$ua->AddMoneyEasy($ub->bet_uid, $winValue, $pid, $message);
					}

					$db->setQuery("UPDATE users_bets SET processed = 1, win = " . ($betStatus == 'win' ? 1 : 0) . " WHERE id=" . (int)$ub->bet_id)->execute();
				}
			}
		}

		$this->setRedirect(
			JRoute::_(
				'index.php?option=' . $this->option . '&view=' . $this->view_list
				. $this->getRedirectToListAppend(), false
			),
			JText::_('COM_BETS_BET_WAS_PROCESSED')
		);
	}

	protected function getUnprocessedBetsLike($bet)
	{
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query
			->select('b.*, b.1 as p1, b.2 as p2, b.12 as p12, b.1X as X1, b.1_s as s_1, b.1_s as s_2, ub.id as bet_id, ub.uid as bet_uid, ub.created as bet_created, ub.type as bet_type, ub.result as bet_result, ub.bet_sum as bet_sum, ub.title as bet_title, ub.coef as bet_coef, b2.title as parent_title, s.title as section_title, r.status as result_status, r.result as result_result')
			->from('users_bets ub');

		$query->join('LEFT', 'bets b ON (ub.event = b.id)');
		$query->join('LEFT', 'bets b2 ON (b2.id = b.parent)');
		$query->join('LEFT', 'bets_sections s ON (b.sectionid = s.sectionid)');
		$query->join('LEFT', 'results r ON (b.id = r.parser_betid)');

		$query->where('ub.processed = 0');
		$query->where('ub.event = ' . (int) $bet->id);
		$query->where('ub.type = ' . (int) $bet->bet_type);
		$query->where('ub.result = \'' . $db->escape($bet->bet_result) . '\'');

		return $db->setQuery($query)->loadObjectList();
	}

	protected function getBetWin($bet)
	{
		if ($bet->bet_type == BetHelper::BET_TYPE_ADDITIONAL)
		{
			$data = BetHelper::getAdditionalResultData($bet->id, $bet->bet_result);
			if (is_array($data))
			{
				return floatval($bet->bet_sum) * floatval($data['coef']);
			}
		}
		else
		{
			return floatval($bet->bet_sum) * floatval($bet->bet_coef);
		}

		return false;
	}

	protected function getPid($uid)
	{
		list($whole, $decimal) = explode(' ', microtime());
		$decimal = substr($decimal, strlen($decimal) - 5, 5);
		return $decimal . $uid;
	}
}