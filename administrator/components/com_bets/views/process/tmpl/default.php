<?php defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.formvalidation');
?>
<form action="<?php echo JRoute::_('index.php?option=com_bets&task=bet.process&id=' . $this->betId); ?>" class="user_data" id="adminForm" method="post">

	<table class="table">
		<tr>
			<th style="width: 50%"><?php echo JText::_('COM_BETS_EVENT'); ?></th>
			<th style="width: 50%"><?php echo JText::_('COM_BETS_BET_TITLE'); ?></th>
		</tr>
		<tr>
			<td>
				<div>
					<?php echo LineHelper::formatDate($this->bet->datetime); ?><br>
					<?php echo htmlspecialchars(LineHelper::getEventTitle($this->bet)); ?>
				</div>

				<p>&nbsp;</p>
				<div>
					<?php echo JText::_('COM_BETS_EVENT_STATUS'); ?>:
					<?php if ($this->bet->result_status == BetHelper::RESULT_END) : ?>
						<span class="label label-success"><?php echo JText::_('COM_BETS_STATUS_END'); ?></span>
					<?php else: ?>
						<?php echo LineHelper::stringifyEventStatus($this->bet); ?>
					<?php endif; ?>
				</div>

				<p>&nbsp;</p>
				<div>
					<?php echo JText::_('COM_BETS_EVENT_RESULT'); ?>:
					<?php echo $this->betResult; ?><br>
					<a href="https://www.google.ru/#q=<?php echo urlencode(LineHelper::getEventTitle($this->bet) . ' ' . LineHelper::formatDate($this->bet->datetime)); ?>" target="_blank"><?php echo JText::_('COM_BETS_CHECK_RESULT_IN_GOOGLE'); ?></a>
				</div>

				<p>&nbsp;</p>
				<div>
					<?php echo JText::_('COM_BETS_BET_STATUS'); ?>:
					<select name="bet_status">
						<option value=""><?php echo JText::_('COM_BETS_UNPROCESSED'); ?></option>
						<option value="win"<?php if ($this->betStatus == 'win') echo ' selected="selected"'; ?>><?php echo JText::_('COM_BETS_WIN'); ?></option>
						<option value="lost"<?php if ($this->betStatus == 'lost') echo ' selected="selected"'; ?>><?php echo JText::_('COM_BETS_LOST'); ?></option>
					</select>
				</div>

			</td>
			<td>
				<div>
					<?php echo $this->bet->bet_title; ?><br>
					<span class="bet_sum">
						<span><?php echo JText::_('COM_BETS_BET_SUM'); ?>:</span> <b><?php echo FormatDefaultCurrency($this->bet->bet_sum); ?></b>
					</span>
				</div>

				<p>&nbsp;</p>
				<div>
					<?php echo JText::_('COM_BETS_USER'); ?>:
					<a href="<?php echo JRoute::_('index.php?option=com_billing&task=view&uid=' . $this->bet->bet_uid); ?>"><?php echo $this->bet->username; ?></a>
				</div>
			</td>
		</tr>
	</table>

	<input type="hidden" name="task" value="bet.process" />
	<?php echo JHtml::_('form.token'); ?>
</form>