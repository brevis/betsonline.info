<?php defined('_JEXEC') or die('Restricted access');

/**
 * Process View
 *
 * @since  1.0.0
 */
class BetsViewProcess extends JViewLegacy
{
	protected $bet;

	protected $betStatus = '';

	protected $betResult = '';

	/**
	 * Display the view
	 *
	 * @param   string $tpl The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return mixed|void
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();

		$this->betId     = $app->input->getInt('id');
		$this->bet = BetHelper::getBetById($this->betId, true);
		if (!$this->bet)
		{
			throw new Exception(JText::_('COM_BETS_BET_NOT_FOUND'), 404);
		}

		if ((int) $this->bet->result_id > 0)
		{
			$this->betResult = $this->bet->result_result . '<br>';

			$betResults = $db->setQuery("SELECT * FROM results WHERE parent = " . (int) $this->bet->result_id)->loadObjectList();
			if (is_array($betResults) && count($betResults) > 0)
			{
				foreach ($betResults as $br)
				{
					$this->betResult .= $br->title . ': ' . $br->result . '<br>';
				}
			}
		}

		if ($this->bet->bet_processed == 1)
		{
			$this->betStatus = $this->bet->bet_win == 1 ? 'win' : 'lose';
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors), 500);
		}

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
		$input = JFactory::getApplication()->input;

		// Hide Joomla Administrator Main menu
		$input->set('hidemainmenu', true);

		JToolBarHelper::title(JText::_('COM_BETS_BET_PROCESS'), 'bets');
		// Build the actions for new and existing records.
		JToolBarHelper::apply('bet.process', 'JTOOLBAR_APPLY');
		JToolBarHelper::cancel('bet.cancel', 'JTOOLBAR_CLOSE');
	}

	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument()
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_BETS_BET_PROCESS'));
	}
}