<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

require_once JPATH_COMPONENT.'/helpers/bets.php';

/**
 * Bets list view class.
 *
 * @package     Bets
 * @subpackage  Views
 */
class BetsViewBets extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;
	protected $isFilter = false;

	public $filter_order;
	public $filter_order_Dir;
	public $filterForm;
	public $activeFilters;

	public function display($tpl = null)
	{
		$app     = JFactory::getApplication();
		$context = "bets.list.admin.orders";

		$this->items            = $this->get('Items');
		$this->pagination       = $this->get('Pagination');
		$this->state            = $this->get('State');
		$this->filter_order     = $app->getUserStateFromRequest($context . 'filter_order', 'filter_order', 'greeting', 'cmd');
		$this->filter_order_Dir = $app->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', 'asc', 'cmd');
		$this->filterForm       = $this->get('FilterForm');
		$this->activeFilters    = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
			return false;
		}


		$sections = array();
		foreach($this->items as $bet)
		{
			$sections[] = $bet->sectionid;
		}
		$types = ResultsHelper::getTypeBySection($sections);
		$typesTitles = ResultsHelper::getAllTypes();
		foreach($this->items as $bet)
		{
			$bet->type_title = isset($types[$bet->sectionid]) ? $typesTitles[$types[$bet->sectionid]] : '';
			$bet->type_id = isset($types[$bet->sectionid]) ? $types[$bet->sectionid] : '';
		}

		BetsHelper::addSubmenu('bets');

		$filter = $app->input->get('filter', array(), 'ARRAY');
		foreach($filter as $f)
		{
			if ($f !== '')
			{
				$this->isFilter = true;
				break;
			}
		}

		// We don't need toolbar in the modal window.
		if ($this->getLayout() !== 'modal')
		{
			$this->addToolbar();
			$this->sidebar = JHtmlSidebar::render();
		}
		
		parent::display($tpl);
	}
	
	/**
	 *	Method to add a toolbar
	 */
	protected function addToolbar()
	{
		$state	= $this->get('State');
		$canDo	= BetsHelper::getActions();
		$user	= JFactory::getUser();

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');
		
		JToolBarHelper::title(JText::_('COM_BETS'));
				
		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_bets');
		}

		if ($canDo->get('core.delete'))
		{
			JToolBarHelper::deleteList('', 'bets.delete', 'JTOOLBAR_DELETE');
			JToolBarHelper::custom('bets.returnbets', 'leftarrow', '', 'COM_BETS_RETURN_BETS');
		}
	}
}
