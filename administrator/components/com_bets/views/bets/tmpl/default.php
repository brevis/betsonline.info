<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// necessary libraries
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

// sort ordering and direction
$user	= JFactory::getUser();
$userId	= $user->get('id');

$listOrder = $this->escape($this->filter_order);
$listDirn  = $this->escape($this->filter_order_Dir);
?>
<form action="<?php echo JRoute::_('index.php?option=com_bets'); ?>" method="post" id="adminForm" name="adminForm">
	<div class="row-fluid">
		<div class="span12">
			<?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
		</div>
	</div>
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="2%">
				<?php echo JHtml::_('grid.checkall'); ?>
			</th>
			<th width="1%">
				#
			</th>
			<th>
				<?php echo JText::_('COM_BETS_USER'); ?>
			</th>
			<th>
				<?php echo JText::_('COM_BETS_SPORT_TYPE'); ?>
			</th>
			<th>
				<?php echo JText::_('COM_BETS_BET_TITLE'); ?>
			</th>
			<th style="width: 70px;">
				<?php echo JText::_('COM_BETS_BET_SUM'); ?>
			</th>
			<th>
				<?php echo JText::_('COM_BETS_EVENT_STATUS'); ?>
			</th>
			<th>
				<?php echo JText::_('COM_BETS_BET_STATUS'); ?>
			</th>
			<th style="width: 100px;">
				<?php echo JText::_('COM_BETS_BET_IS_POSSIBLE_PROCESSED_AUTO'); ?>
			</th>
			<th style="width: 70px;">
				<?php echo JHtml::_('grid.sort', 'COM_BETS_CREATED', 'created', $listDirn, $listOrder); ?>
			</th>
			<th>
				<?php echo JText::_('COM_BETS_ACTIONS'); ?>
			</th>
		</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="99">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
		</tfoot>
		<tbody>
		<?php if (!empty($this->items)) : ?>
			<?php foreach ($this->items as $i => $row) :
				$link = JRoute::_('index.php?option=com_bets&task=bet.edit&id=' . $row->id);
				?>
				<tr>
					<td>
						<?php echo JHtml::_('grid.id', $i, $row->bet_id); ?>
					</td>
					<td>
						<?php echo $row->bet_id; ?>
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_billing&task=view&uid=' . $row->bet_uid); ?>"><?php echo $row->username; ?></a>
					</td>
					<td>
						<?php echo $row->type_title; ?>
					</td>
					<td>
						<?php echo LineHelper::formatDate($row->datetime); ?><br>
						<?php echo htmlspecialchars(LineHelper::getEventTitle($row)); ?><br><br>
						<?php echo $row->bet_title; ?>
					</td>
					<td>
						<?php echo FormatDefaultCurrency($row->bet_sum); ?>
					</td>
					<td>
						<?php if ($row->result_status == BetHelper::RESULT_END) : ?>
							<span class="label label-success"><?php echo JText::_('COM_BETS_STATUS_END'); ?></span>
						<?php else: ?>
								<?php echo LineHelper::stringifyEventStatus($row); ?>
						<?php endif; ?>
					</td>
					<td>
						<?php if ($row->bet_processed == 1) : ?>
							<?php if ($row->bet_win == 1) : ?>
								<span class="label label-success"><?php echo JText::_('COM_BETS_WIN'); ?></span>
							<?php elseif ($row->bet_win == 2) : ?>
								<span class="label label-default" style="background: #777;"><?php echo JText::_('COM_BETS_RETURNED'); ?></span>
							<?php else: ?>
								<span class="label label-default" style="background: #d9534f;"><?php echo JText::_('COM_BETS_LOST'); ?></span>
							<?php endif; ?>
						<?php else: ?>
							<span class="label label-default"><?php echo JText::_('COM_BETS_UNPROCESSED'); ?></span>
						<?php endif; ?>
					</td>
					<td>
						<?php if ($row->bet_type == BetHelper::BET_TYPE_ADDITIONAL) : ?>
							<span class="label label-danger"><?php echo JText::_('COM_BETS_PROCESSED_MANUAL'); ?></span>
						<?php else: ?>
							<span class="label label-success"><?php echo JText::_('COM_BETS_PROCESSED_AUTO'); ?></span>
						<?php endif; ?>
					</td>
					<td>
						<?php echo $row->bet_created; ?>
					</td>
					<td style="text-align: center">
						<?php if ($row->bet_processed == 0) : ?>
							<a href="<?php echo JRoute::_('index.php?option=com_bets&view=process&id=' . $row->bet_id); ?>" class="btn btn-small btn-success"><?php echo JText::_('COM_BETS_ACTION_PROCESS'); ?></a>
						<?php endif; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		</tbody>
	</table>
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	<?php echo JHtml::_('form.token'); ?>
</form>
<script>
	jQuery(function(){
		jQuery('#toolbar-delete button').addClass('btn-danger');

		jQuery('#filter_date_to').parent().before('–&nbsp;&nbsp;');
		jQuery('#filter_date_to').parent().parent().after('<div class="btn-wrapper"><button type="submit" class="btn btn-primary" style="margin-top: -10px;">Filter</button></div>');

		<?php if ($this->isFilter) : ?>
			jQuery('.btn.hasTooltip.js-stools-btn-filter').click();
		<?php endif; ?>
	});
</script>