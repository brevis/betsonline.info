<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

require_once JPATH_COMPONENT.'/helpers/bets.php';

/**
 * Event view class.
 *
 * @package     Bets
 * @subpackage  Views
 */
class BetsViewEvent extends JViewLegacy
{
	protected $eventId;

	protected $event;

	protected $sections;

	protected $adtEvents;

	protected $adtResults;


	public function display($tpl = null)
	{
		$app     = JFactory::getApplication();
		$db = JFactory::getDbo();
		$context = "bets.list.admin.orders";

		$this->eventId = $app->input->getInt('id');
		if ($this->eventId > 0)
		{
			$this->event = LineHelper::getEventById($app->input->getInt('id'));
			if (!$this->event)
			{
				throw new Exception("Event not found");
				return false;
			}
		}
		else
		{
			$this->event = $this->getEmptyEvent();
		}


		$this->adtEvents = LineHelper::getLineByParent($this->eventId);

		$this->adtResults = LineHelper::getAdditionalResultsByEvent($this->eventId);

		$this->sections = $db->setQuery("SELECT * FROM bets_sections ORDER BY title ASC")->loadObjectList();

		// We don't need toolbar in the modal window.
		if ($this->getLayout() !== 'modal')
		{
			$this->addToolbar();
			$this->sidebar = JHtmlSidebar::render();
		}
		
		parent::display($tpl);
	}

	protected function getEmptyEvent()
	{
		$event = new stdClass();
		$event->id = '';
		$event->title = '';
		$event->datetime = '';
		$event->p1 = '';
		$event->X = '';
		$event->p2 = '';
		$event->X1 = '';
		$event->p12 = '';
		$event->X2 = '';
		$event->fora = '';
		$event->s_1 = '';
		$event->fora_s = '';
		$event->s_2 = '';
		$event->total = '';
		$event->B = '';
		$event->M = '';
		$event->adtEvents = '';
		$event->sectionid = '';

		return $event;
	}

	protected function getEventFromRequest()
	{
		$app = JFactory::getApplication();

		$event = $this->getEmptyEvent();

		return $event;
	}
	
	/**
	 *	Method to add a toolbar
	 */
	protected function addToolbar()
	{
		$state	= $this->get('State');
		$canDo	= BetsHelper::getActions();
		$user	= JFactory::getUser();

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');
		
		JToolBarHelper::title(JText::_('COM_BETS_SUBMENU_EVENTS'));
				
		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_bets');
		}

		if ($canDo->get('core.delete'))
		{
			JToolBarHelper::apply('event.apply');
			JToolBarHelper::save('event.save');
			JToolBarHelper::cancel('event.cancel');
		}
	}
}
