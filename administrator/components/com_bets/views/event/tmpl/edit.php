<?php defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '#event_sectionid');
?>
<style>
	.coefedit{
		width: 50px;
	}
	.table thead th{
		text-align: center;
	}
</style>
<script>
	(function($) {
		$(function(){
			var cancelBtnClick = false;

			$('#toolbar-cancel button').click(function(){
				cancelBtnClick = true;
				$('#adminForm').submit();
			});
			<?php if (empty($this->event->id)) : ?>
			$('#adminForm').submit(function(){
				if (cancelBtnClick) {
					return true;
				}
				var $form = $(this),
					$title = $form.find('input[name="event\[title\]"]'),
					$sectionid = $form.find('input[name="event\[sectionid\]"]'),
					$datetime = $form.find('input[name="event\[datetime\]"]'),
					error = false;

				$title.css('border', 'none');
				$datetime.css('border', 'none');
				$form.find('input.coefedit').css('border', 'none');

				$title.val($.trim($title.val()));
				if ($title.val() == '') {
					$title.css('border', '1px solid red');
					error = true;
				}

				$datetime.val($.trim($datetime.val()));
				if ($datetime.val() == '' || /^\d{2}\.\d{2}\.\d{4} \d{2}:\d{2}$/g.test($datetime.val()) == false) {
					$datetime.css('border', '1px solid red');
					error = true;
				}

				var valuesError = true;
				$form.find('input.coefedit').each(function(i, el){
					if ($(el).val() != '') {
						valuesError = false;
					}
				});

				if (valuesError) {
					$form.find('input.coefedit').css('border', '1px solid red');
					error = true;
				}

				if (error) {
					return false;
				}
			});
			<?php endif; ?>
		});
	})(jQuery);
</script>
<form action="<?php echo JRoute::_('index.php?option=com_bets&task=event.edit&id=' . $this->event->id); ?>" class="user_data" id="adminForm" method="post">

	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="1%">
				#
			</th>
			<th class="th_2"></th>
			<th class="th_3"><?php echo JText::_('COM_BETS_LINE_EVENT'); ?></th>
			<th class="th_4">1</th>
			<th class="th_5">X</th>
			<th class="th_6">2</th>
			<th class="th_7">1X</th>
			<th class="th_8">12</th>
			<th class="th_9">X2</th>
			<th class="th_10"><?php echo JText::_('COM_BETS_LINE_FORA'); ?></th>
			<th class="th_11">1</th>
			<th class="th_12"><?php echo JText::_('COM_BETS_LINE_FORA'); ?></th>
			<th class="th_13">2</th>
			<th class="th_14"><?php echo JText::_('COM_BETS_LINE_TOTAL'); ?></th>
			<th class="th_15"><?php echo JText::_('COM_BETS_LINE_B'); ?></th>
			<th class="th_16"><?php echo JText::_('COM_BETS_LINE_M'); ?></th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>
				<?php echo $this->event->id; ?>
			</td>
			<td class="td_2">
				<a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
			</td>
			<td class="td_3">
				<?php if ($this->event->id) : ?>
					<?php echo $this->escape($this->event->title); ?>
				<?php else: ?>
					<label>Событие:</label>
					<input type="text" name="event[title]" value="<?php echo $this->escape($this->event->title); ?>" required>

					<label>Категория:</label>
					<select name="event[sectionid]" id="event_sectionid" required>
					<?php foreach ($this->sections as $section) : ?>
						<option value="<?php echo $this->escape($section->sectionid); ?>"<?php if ($this->event->sectionid == $section->sectionid) : ?> selected="selected"<?php endif; ?>><?php echo $this->escape($section->title); ?></option>
					<?php endforeach; ?>
					</select>
				<?php endif; ?>

				<div class="item_info_wrapper">
					<?php if ($this->event->id) : ?>
						<?php echo $this->escape($this->event->datetime); ?>
					<?php else: ?>
						<label>Дата:</label>
						<input type="text" name="event[datetime]" value="<?php echo $this->escape($this->event->datetime); ?>" required>(Формат: 21.12.2016 12:45)
					<?php endif; ?>

				</div>
			</td>
			<td class="td_4"><input type="text" class="coefedit" name="coef[<?php echo (int)$this->event->id; ?>][1]" value="<?php echo $this->escape($this->event->p1); ?>"></td>
			<td class="td_5"><input type="text" class="coefedit" name="coef[<?php echo (int)$this->event->id; ?>][X]" value="<?php echo $this->escape($this->event->X); ?>"></td>
			<td class="td_6"><input type="text" class="coefedit" name="coef[<?php echo (int)$this->event->id; ?>][2]" value="<?php echo $this->escape($this->event->p2); ?>"></td>
			<td class="td_7"><input type="text" class="coefedit" name="coef[<?php echo (int)$this->event->id; ?>][1X]" value="<?php echo $this->escape($this->event->X1); ?>"></td>
			<td class="td_8"><input type="text" class="coefedit" name="coef[<?php echo (int)$this->event->id; ?>][12]" value="<?php echo $this->escape($this->event->p12); ?>"></td>
			<td class="td_9"><input type="text" class="coefedit" name="coef[<?php echo (int)$this->event->id; ?>][X2]" value="<?php echo $this->escape($this->event->X2); ?>"></td>
			<td class="td_10">
				<?php if ($this->event->id) : ?>
					<?php echo $this->escape($this->event->fora); ?>
				<?php else: ?>
					<input type="text" class="coefedit" name="coef[<?php echo (int)$this->event->id; ?>][fora]" value="<?php echo $this->escape($this->event->fora); ?>">
				<?php endif; ?>
			</td>
			<td class="td_11"><input type="text" class="coefedit" name="coef[<?php echo (int)$this->event->id; ?>][1_s]" value="<?php echo $this->escape($this->event->s_1); ?>"></td>
			<td class="td_12">
				<?php if ($this->event->id) : ?>
					<?php echo $this->escape($this->event->fora_s); ?>
				<?php else: ?>
					<input type="text" class="coefedit" name="coef[<?php echo (int)$this->event->id; ?>][fora_s]" value="<?php echo $this->escape($this->event->fora_s); ?>">
				<?php endif; ?>
			</td>
			<td class="td_13"><input type="text" class="coefedit" name="coef[<?php echo (int)$this->event->id; ?>][2_s]" value="<?php echo $this->escape($this->event->s_2); ?>"></td>
			<td class="td_14">
				<?php if ($this->event->id) : ?>
					<?php echo $this->escape($this->event->total); ?>
				<?php else: ?>
					<input type="text" class="coefedit" name="coef[<?php echo (int)$this->event->id; ?>][total]" value="<?php echo $this->escape($this->event->total); ?>">
				<?php endif; ?>
			</td>
			<td class="td_15"><input type="text" class="coefedit" name="coef[<?php echo (int)$this->event->id; ?>][B]" value="<?php echo $this->escape($this->event->B); ?>"></td>
			<td class="td_16"><input type="text" class="coefedit" name="coef[<?php echo (int)$this->event->id; ?>][M]" value="<?php echo $this->escape($this->event->M); ?>"></td>
		</tr>
		<?php if (is_array($this->adtEvents) && count($this->adtEvents) > 0) : ?>
			<tr>
				<th colspan="99">
					<?php echo JText::_('COM_BETS_ADDITIONAL_BETS'); ?>
				</th>
			</tr>
			<?php foreach ($this->adtEvents as $i => $event) : ?>
				<tr>
					<td>
						<?php echo $event->id; ?>
					</td>
					<td class="td_2">
						<a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
					</td>
					<td class="td_3">
						<?php echo $this->escape($event->title); ?>

						<div class="item_info_wrapper">
							<?php echo $this->escape($event->datetime); ?>
						</div>
					</td>
					<td class="td_4"><input type="text" class="coefedit" name="coef[<?php echo $event->id; ?>][1]" value="<?php echo $this->escape($event->p1); ?>"></td>
					<td class="td_5"><input type="text" class="coefedit" name="coef[<?php echo $event->id; ?>][X]" value="<?php echo $this->escape($event->X); ?>"></td>
					<td class="td_6"><input type="text" class="coefedit" name="coef[<?php echo $event->id; ?>][2]" value="<?php echo $this->escape($event->p2); ?>"></td>
					<td class="td_7"><input type="text" class="coefedit" name="coef[<?php echo $event->id; ?>][1X]" value="<?php echo $this->escape($event->X1); ?>"></td>
					<td class="td_8"><input type="text" class="coefedit" name="coef[<?php echo $event->id; ?>][12]" value="<?php echo $this->escape($event->p12); ?>"></td>
					<td class="td_9"><input type="text" class="coefedit" name="coef[<?php echo $event->id; ?>][X2]" value="<?php echo $this->escape($event->X2); ?>"></td>
					<td class="td_10"><?php echo $this->escape($event->fora); ?></td>
					<td class="td_11"><input type="text" class="coefedit" name="coef[<?php echo $event->id; ?>][1_s]" value="<?php echo $this->escape($event->s_1); ?>"></td>
					<td class="td_12"><?php echo $this->escape($event->fora_s); ?></td>
					<td class="td_13"><input type="text" class="coefedit" name="coef[<?php echo $event->id; ?>][2_s]" value="<?php echo $this->escape($event->s_2); ?>"></td>
					<td class="td_14"><?php echo $this->escape($event->total); ?></td>
					<td class="td_15"><input type="text" class="coefedit" name="coef[<?php echo $event->id; ?>][B]" value="<?php echo $this->escape($event->B); ?>"></td>
					<td class="td_16"><input type="text" class="coefedit" name="coef[<?php echo $event->id; ?>][M]" value="<?php echo $this->escape($event->M); ?>"></td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		</tbody>
	</table>

	<input type="hidden" name="task" value="event.edit" />
	<?php echo JHtml::_('form.token'); ?>
</form>