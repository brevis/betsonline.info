<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

// necessary libraries
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

// sort ordering and direction
$user	= JFactory::getUser();
$userId	= $user->get('id');

$listOrder = $this->escape($this->filter_order);
$listDirn  = $this->escape($this->filter_order_Dir);
?>
<form action="<?php echo JRoute::_('index.php?option=com_bets&view=events'); ?>" method="post" id="adminForm" name="adminForm">
	<div class="row-fluid">
		<div class="span12">
			<?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
		</div>
	</div>
	<p>&nbsp;</p>

	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="1%">
				<?php echo JHtml::_('grid.sort', 'ID', 'id', $listDirn, $listOrder); ?>
			</th>
			<th>
				<?php echo JText::_('COM_BETS_SPORT_TYPE'); ?>
			</th>
			<th class="th_2"></th>
			<th class="th_3"><?php echo JText::_('COM_BETS_LINE_EVENT'); ?></th>
			<th class="th_4">1</th>
			<th class="th_5">X</th>
			<th class="th_6">2</th>
			<th class="th_7">1X</th>
			<th class="th_8">12</th>
			<th class="th_9">X2</th>
			<th class="th_10"><?php echo JText::_('COM_BETS_LINE_FORA'); ?></th>
			<th class="th_11">1</th>
			<th class="th_12"><?php echo JText::_('COM_BETS_LINE_FORA'); ?></th>
			<th class="th_13">2</th>
			<th class="th_14"><?php echo JText::_('COM_BETS_LINE_TOTAL'); ?></th>
			<th class="th_15"><?php echo JText::_('COM_BETS_LINE_B'); ?></th>
			<th class="th_16"><?php echo JText::_('COM_BETS_LINE_M'); ?></th>
		</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="99">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
		</tfoot>
		<tbody>
		<?php if (!empty($this->items)) : ?>
			<?php foreach ($this->items as $i => $bet) :
				$editLink = JRoute::_('index.php?option=com_bets&task=event.edit&id=' . $bet->id);
				?>
				<tr>
					<td>
						<?php echo $bet->id; ?>
					</td>
					<td>
						<?php echo $this->escape($bet->type_title); ?>
					</td>
					<td class="td_2">
						<a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
					</td>
					<td class="td_3">
						<a href="<?php echo $editLink; ?>"><?php echo $this->escape($bet->title); ?></a>

						<div class="item_info_wrapper"><span class="label label-info"><?php echo LineHelper::formatDate($bet->datetime); ?></span></div>
					</td>
					<td class="td_4"><?php echo $this->escape($bet->p1); ?></td>
					<td class="td_5"><?php echo $this->escape($bet->X); ?></td>
					<td class="td_6"><?php echo $this->escape($bet->p2); ?></td>
					<td class="td_7"><?php echo $this->escape($bet->X1); ?></td>
					<td class="td_8"><?php echo $this->escape($bet->p12); ?></td>
					<td class="td_9"><?php echo $this->escape($bet->X2); ?></td>
					<td class="td_10"><?php echo $this->escape($bet->fora); ?></td>
					<td class="td_11"><?php echo $this->escape($bet->s_1); ?></td>
					<td class="td_12"><?php echo $this->escape($bet->fora_s); ?></td>
					<td class="td_13"><?php echo $this->escape($bet->s_2); ?></td>
					<td class="td_14"><?php echo $this->escape($bet->total); ?></td>
					<td class="td_15"><?php echo $this->escape($bet->B); ?></td>
					<td class="td_16"><?php echo $this->escape($bet->M); ?></td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		</tbody>
	</table>
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	<?php echo JHtml::_('form.token'); ?>
</form>
<script>
	jQuery(function(){
		jQuery('#toolbar-delete button').addClass('btn-danger');

		jQuery('#filter_date_to').parent().before('–&nbsp;&nbsp;');
		jQuery('#filter_date_to').parent().parent().after('<div class="btn-wrapper"><button type="submit" class="btn btn-primary" style="margin-top: -10px;">Filter</button></div>');

		<?php if ($this->isFilter) : ?>
			jQuery('.btn.hasTooltip.js-stools-btn-filter').click();
		<?php endif; ?>
	});
</script>