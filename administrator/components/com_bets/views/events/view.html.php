<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

require_once JPATH_COMPONENT.'/helpers/bets.php';

/**
 * Events list view class.
 *
 * @package     Bets
 * @subpackage  Views
 */
class BetsViewEvents extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;
	protected $isFilter = false;

	public $filter_order;
	public $filter_order_Dir;
	public $filterForm;
	public $activeFilters;

	public function display($tpl = null)
	{
		$app     = JFactory::getApplication();
		$context = "bets.list.admin.orders";

		$this->items            = $this->get('Items');
		$sections = array();
		foreach($this->items as $item)
		{
			$sections[] = $item->sectionid;
		}
		$types = ResultsHelper::getTypeBySection($sections);
		$typesTitles = ResultsHelper::getAllTypes();
		foreach($this->items as $item)
		{
			$item->type_title = isset($types[$item->sectionid]) ? $typesTitles[$types[$item->sectionid]] : '';
			$item->type_id = isset($types[$item->sectionid]) ? $types[$item->sectionid] : '';
		}

		$this->pagination       = $this->get('Pagination');
		$this->state            = $this->get('State');
		$this->filter_order     = $app->getUserStateFromRequest($context . 'filter_order', 'filter_order', 'greeting', 'cmd');
		$this->filter_order_Dir = $app->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', 'asc', 'cmd');
		$this->filterForm       = $this->get('FilterForm');
		$this->activeFilters    = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
			return false;
		}


		$sections = array();
		foreach($this->items as $item)
		{
			$sections[] = $item->sectionid;
		}
		$types = ResultsHelper::getTypeBySection($sections);
		$typesTitles = ResultsHelper::getAllTypes();
		foreach($this->items as $item)
		{
			$item->type_title = isset($types[$item->sectionid]) ? $typesTitles[$types[$item->sectionid]] : '';
			$item->type_id = isset($types[$item->sectionid]) ? $types[$item->sectionid] : '';
		}

		BetsHelper::addSubmenu('events');

		$filter = $app->input->get('filter', array(), 'ARRAY');
		foreach($filter as $f)
		{
			if ($f !== '')
			{
				$this->isFilter = true;
				break;
			}
		}

		// We don't need toolbar in the modal window.
		if ($this->getLayout() !== 'modal')
		{
			$this->addToolbar();
			$this->sidebar = JHtmlSidebar::render();
		}
		
		parent::display($tpl);
	}
	
	/**
	 *	Method to add a toolbar
	 */
	protected function addToolbar()
	{
		$state	= $this->get('State');
		$canDo	= BetsHelper::getActions();
		$user	= JFactory::getUser();

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');
		
		JToolBarHelper::title(JText::_('COM_BETS_SUBMENU_EVENTS'));
				
		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_bets');
		}

		JToolBarHelper::addNew('event.add');

		if ($canDo->get('core.delete'))
		{
			//JToolBarHelper::deleteList('', 'events.delete', 'JTOOLBAR_DELETE');
			//JToolBarHelper::custom('events.returnbets', 'leftarrow', '', 'COM_BETS_RETURN_BETS');
		}
	}
}
