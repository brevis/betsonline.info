<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Bets helper class.
 *
 * @package     Bets
 * @subpackage  Helpers
 */
class BetsHelper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($submenu)
	{
		JSubMenuHelper::addEntry(
				JText::_('COM_BETS_SUBMENU_BETS'),
				'index.php?option=com_bets',
				$submenu == 'bets'
		);

		JSubMenuHelper::addEntry(
				JText::_('COM_BETS_SUBMENU_EVENTS'),
				'index.php?option=com_bets&view=events',
				$submenu == 'events'
		);
	}
	
	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_bets';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
	

}