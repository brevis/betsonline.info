<?php defined('_JEXEC') or die('Restricted access');

/**
 * Event table class
 *
 * @since  1.0.0
 */
class BetsTableEvent extends JTable
{
	/**
	 * Constructor
	 *
	 * @param   JDatabaseDriver &$db A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('bets', 'id', $db);
	}

	/**
	 * Overloaded bind function
	 *
	 * @param       array           array
	 *
	 * @return      null|string     null is operation was satisfactory, otherwise returns an error
	 * @see   JTable:bind
	 * @since 1.5
	 */
	public function bind($array, $ignore = '')
	{
		if (isset($array['params']) && is_array($array['params']))
		{
			// Convert the params field to a string.
			$parameter = new JRegistry;
			$parameter->loadArray($array['params']);
			$array['params'] = (string) $parameter;
		}

		// Bind the rules.
		if (isset($array['rules']) && is_array($array['rules']))
		{
			$rules = new JAccessRules($array['rules']);
			$this->setRules($rules);
		}

		return parent::bind($array, $ignore);
	}

	public function store($updateNulls = false)
	{
		$date   = JFactory::getDate();

		if ($this->id)
		{
			$this->updated = $date->toSql();
		}

		return parent::store($updateNulls);
	}

}