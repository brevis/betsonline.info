<?php
defined('_JEXEC') or die('Restricted access');

class FilterValueType
{
	const TEXT_FIELD = 1;
	const SELECT     = 2;
	const RANGE      = 3;

	public static function all()
	{
		$refl = new ReflectionClass('FilterValueType');
		return $refl->getConstants();
	}
}