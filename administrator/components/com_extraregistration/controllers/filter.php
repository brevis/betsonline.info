<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');
require_once __DIR__ . '/../helper.php';
require_once __DIR__ . '/../FilterComparison.php';
require_once __DIR__ . '/../FilterValueType.php';

class ExtraregistrationControllerFilter extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'filters';
		parent::__construct();
	}

	public function getfields()
	{
		header('Content-Type: text/html; charset=utf-8', true);

		$app    = JFactory::getApplication();
		$jinput = JFactory::getApplication()->input;

		$params         = JComponentHelper::getParams('com_users');
		$defaultGroupId = $params->get('new_usertype');

		$filterId = $jinput->get('filterId', 0, 'INT');
		$filter   = ComExtraregistrationHelper::getFilterById($filterId);
		if (is_object($filter))
		{
			$rules = ComExtraregistrationHelper::getAllRulesByFilter($filterId);
			ob_start();
			foreach ($rules as $rule)
			{
				$field = ComExtraregistrationHelper::getFieldById($rule->field_id);
				if (!is_object($field))
				{
					continue;
				}
				if ($field->groups == '')
				{
					$groupId = $defaultGroupId;
				}
				else
				{
					$groups  = explode(',', $field->groups);
					$groupId = $groups[0];
				}
				$this->printField($rule, $field);
			}
			$output = ob_get_contents();
			ob_end_clean();
		}
		else
		{
			ob_start();
			$this->printField();
			$output = ob_get_contents();
			ob_end_clean();
		}

		echo $output;
		$app->close(0);
	}

	public function getfieldField()
	{
		header('Content-Type: text/html; charset=utf-8', true);

		$app    = JFactory::getApplication();
		$jinput = JFactory::getApplication()->input;
		$view   = $this->getView('filter', 'html');
		$view->setLayout('fieldField');

		$view->groupId = $jinput->get('groupId', 0, 'INT');
		$fields        = ComExtraregistrationHelper::getAllFieldsByGroup($view->groupId);
		$view->field   = null;
		$view->fieldId = null;
		$view->fields  = $fields;
		if (is_array($fields) && count($fields) > 0)
		{
			$view->field   = $fields[0];
			$view->fieldId = $view->field->id;
		}

		$view->display();
		$app->close(0);
	}

	public function getfieldComparison()
	{
		header('Content-Type: text/html; charset=utf-8', true);

		$app    = JFactory::getApplication();
		$jinput = JFactory::getApplication()->input;
		$view   = $this->getView('filter', 'html');
		$view->setLayout('fieldComparison');

		$view->field = ComExtraregistrationHelper::getFieldById($jinput->get('fieldId', 0, 'INT'));
		if (!is_object($view->field))
		{
			$app->close(0);
		}

		$view->comparison  = null;
		$view->comparisons = ComExtraregistrationHelper::getAllComparisons($view->field);
		if (is_array($view->comparisons) && count($view->comparisons) > 0)
		{
			$view->comparison = $view->comparisons[0];
		}

		$view->display();
		$app->close(0);
	}

	public function getfieldValue()
	{
		header('Content-Type: text/html; charset=utf-8', true);

		$app    = JFactory::getApplication();
		$jinput = JFactory::getApplication()->input;
		$view   = $this->getView('filter', 'html');
		$view->setLayout('fieldValue');

		$view->field = ComExtraregistrationHelper::getFieldById($jinput->get('fieldId', 0, 'INT'));
		if (!is_object($view->field))
		{
			$app->close(0);
		}

		$view->comparison = $jinput->get('comparisonId', 0, 'INT');
		$view->valueType  = ComExtraregistrationHelper::getFieldValueType($view->field, $view->comparison);
		if ($view->valueType == null)
		{
			$app->close(0);
		}
		$view->value  = '';
		$view->values = ComExtraregistrationHelper::getFieldValues($view->field);

		$view->display();
		$app->close(0);
	}

	public function trash()
	{
		$app    = JFactory::getApplication();
		$db     = JFactory::getDbo();
		$jinput = JFactory::getApplication()->input;
		$cid = $jinput->get('cid', array(), 'ARRAY');
		foreach ($cid as $id)
		{
			$id = (int) $id;
			$db->setQuery("DELETE FROM #__extrareg_filter_field WHERE filter_id = " . $id);
			$db->execute();
			$db->setQuery("DELETE FROM #__extrareg_filter WHERE id = " . $id);
			$db->execute();
		}

		$app->redirect('index.php?option=com_extraregistration&task=filters');
	}

	public function applyFilter()
	{
		header('Content-Type: text/html; charset=utf-8', true);

		$app    = JFactory::getApplication();
		$jinput = JFactory::getApplication()->input;
		$view   = $this->getView('filter', 'html');
		$view->setLayout('users');

		$view->users = ComExtraregistrationHelper::getUsersByFilterId($jinput->getInt('filterId'));

		$view->display();
		$app->close(0);
	}

	public function callAction()
	{
		header('Content-Type: text/html; charset=utf-8', true);

		$app    = JFactory::getApplication();
		$jinput = JFactory::getApplication()->input;

		$actionName = $jinput->getString('action');
		$actionFileName = __DIR__ . '/../actions/' . $actionName . '.php';
		if (file_exists($actionFileName))
		{
			require_once $actionFileName;
			$actionClassName = 'Action' . $actionName;
			$action = new $actionClassName();
			echo $action->doAction($jinput->getInt('filterId'));
		}
		else
		{
			echo JText::_('COM_EXTRAREGISTRATION_ACTION_NOT_FOUND');
		}

		$app->close(0);
	}

	/**
	 * Filter Save Hook
	 *
	 * @param JModelLegacy $model
	 * @param array        $validData
	 *
	 * @throws Exception
	 */
	public function postSaveHook(JModelLegacy $model, $validData = array())
	{
		$db     = JFactory::getDbo();
		$jinput = JFactory::getApplication()->input;

		$item     = $model->getItem();
		$filterId = $item->get('id');

		$params         = JComponentHelper::getParams('com_users');
		$defaultGroupId = $params->get('new_usertype');

		$filter         = $jinput->get('filter', array(), 'ARRAY');
		$groupsIds      = isset($filter['group']) ? $filter['group'] : array();
		$fieldsIds      = isset($filter['field']) ? $filter['field'] : array();
		$comparisonsIds = isset($filter['comparison']) ? $filter['comparison'] : array();
		$values         = isset($filter['value']) ? $filter['value'] : array();
		$values_from    = isset($filter['value_from']) ? $filter['value_from'] : array();
		$values_to      = isset($filter['value_to']) ? $filter['value_to'] : array();

		// delete old fields
		$db->setQuery("DELETE FROM #__extrareg_filter_field WHERE filter_id = " . (int) $filterId);
		$db->execute();

		// insert new filters
		$regular_i = 0;
		$range_i = 0;
		foreach ($fieldsIds as $n=>$fid)
		{
			$groupId = isset($groupsIds[$n]) ? $groupsIds[$n] : $defaultGroupId;

			$field = ComExtraregistrationHelper::getFieldById($fid);
			if (!is_object($field))
			{
				continue;
			}

			$comparison = isset($comparisonsIds[$n]) ? $comparisonsIds[$n] : 0;
			if (!in_array((int) $comparison, FilterComparison::all(), true))
			{
				continue;
			}

			$valueType = ComExtraregistrationHelper::getFieldValueType($field, $comparison);
			if (!in_array((int) $valueType, FilterValueType::all(), true))
			{
				continue;
			}

			if ($valueType == FilterValueType::RANGE)
			{
				$value = isset($values_from[$range_i]) && isset($values_to[$range_i])
					? $values_from[$range_i] . '::' . $values_to[$range_i]
					: '';
				$range_i++;
			}
			else
			{
				$value = isset($values[$regular_i]) ? $values[$regular_i] : '';
				$regular_i++;
			}

			$rule             = new stdClass();
			$rule->group_id   = $groupId;
			$rule->filter_id  = $filterId;
			$rule->field_id   = $fid;
			$rule->comparison = $comparison;
			$rule->value      = $value;

			$db->insertObject('#__extrareg_filter_field', $rule);
		}
	}

	/*-------------------------------------------------------------------------------*/

	protected function printField($rule = null, $field = null)
	{
		$jinput = JFactory::getApplication()->input;
		$view   = $this->getView('filter', 'html');
		$view->setLayout('field');

		// groups
		if (is_object($rule))
		{
			$groupId = $rule->group_id;
		}
		else
		{
			$groupId = $jinput->get('groupId', 0, 'INT');
			if ($groupId == 0)
			{
				$params  = JComponentHelper::getParams('com_users');
				$groupId = $params->get('new_usertype');
			}
		}
		$view->groupId = $groupId;

		// fields
		$fields = ComExtraregistrationHelper::getAllFieldsByGroup($groupId);
		if (is_object($rule))
		{
			$fieldId = $rule->field_id;
		}
		else
		{
			$fieldId = $jinput->get('fieldId', 0, 'INT');
		}
		$view->field   = null;
		$view->fieldId = $fieldId;
		$view->fields  = $fields;
		if (is_array($fields) && count($fields) > 0)
		{
			foreach ($fields as $f)
			{
				if ($f->id == $fieldId)
				{
					$view->field = $f;
					break;
				}
			}
			if ($view->field == null)
			{
				$view->field = $fields[0];
			}
		}

		// comparison
		$view->comparisons = ComExtraregistrationHelper::getAllComparisons($view->field);
		if (is_object($rule))
		{
			$view->comparison = $rule->comparison;
		}
		else
		{
			$view->comparison  = null;
			if (is_array($view->comparisons) && count($view->comparisons) > 0)
			{
				$view->comparison = $view->comparisons[0];
			}
		}

		// value
		$view->value     = is_object($rule) ? $rule->value : '';
		$view->values    = is_object($rule) ? ComExtraregistrationHelper::getFieldValues($field) : array();
		$view->valueType = ComExtraregistrationHelper::getFieldValueType($view->field, $view->comparison);

		if ($view->comparison == FilterComparison::RANGE)
		{
			$value = explode('::', $view->value);
			$view->value_from = $value[0];
			$view->value_to = $value[1];
		}

		$view->display();
	}

}