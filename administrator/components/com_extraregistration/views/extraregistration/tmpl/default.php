<?php defined('_JEXEC') or die('Access denied');
if (JVERSION >= 3.0)
{
	JHtml::_('bootstrap.tooltip');
}

require_once JPATH_ROOT . '/administrator/components/com_extraregistration/helper.php';

JToolBarHelper::title(JText::_('EXTRAREGISTRATION'), 'generic.png');

$document = JFactory::getDocument();
$document->addStyleSheet('/components/com_extraregistration/tooltip_style.css');
$document->addStyleSheet('/administrator/components/com_extraregistration/style.css');
$document->addScript('/components/com_extraregistration/tooltip.js');
$document->addScript("/components/com_extraregistration/extra.js");

//error_reporting(E_ALL) ;
//ini_set('display_errors', 'On');

if (!JFactory::getUser()->authorise('core.manage', 'com_extraregistration'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

if (JFactory::getUser()->authorise('core.admin', 'com_extraregistration'))
{
	JToolBarHelper::preferences('com_extraregistration');
}

//include_once (JPATH_ROOT . '/administrator/components/com_extraregistration/views/extraregistration/tmpl/filter_mode.php');

function GetSubList($sid, $ctrlname = '')
{
	$db = JFactory::getDBO();
	if ($ctrlname == '')
	{
		$ctrlname = 'sid';
	}
	$query  = 'select distinct * from `#__billing_subscription_type` where is_active = 1 order by subscription_type';
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();
	$out    = '';
	$out .= "<select name='$ctrlname' style='float: none;'>";
	foreach ($rows as $row)
	{
		if ($row->id == $sid)
		{
			$s = 'selected';
		}
		else
		{
			$s = '';
		}
		$out .= "<option value='$row->id' $s>$row->subscription_type</option>";
	}
	$out .= '</select>';

	return $out;
}

function GetListData($field_id, $id)
{
}

function Tooltip($text)
{
	return "<img src='/media/com_extraregistration/images/help.png' onmouseover=\"tooltip.show('$text');\" onmouseout=\"tooltip.hide();\">";
}

function SystemMessage($msg)
{
	JFactory::getApplication()->enqueueMessage($msg);
}

function GetFieldValue($field_id, $uid)
{
	$db     = JFactory::getDBO();
	$query  = "select value from `#__extrareg_fields_values` where field_id = $field_id and uid = $uid";
	$result = $db->setQuery($query);

	return $db->loadResult();
}

function GetFieldsList($field_id = '', $ctrlname = 'field_id')
{
	$db     = JFactory::getDBO();
	$query  = "select * from `#__extrareg_fields` order by order_id";
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();
	$out    = '';
	$out .= "<select name='$ctrlname' style='float: none;'>";
	foreach ($rows as $row)
	{
		if ($row->id == $field_id)
		{
			$s = 'selected';
		}
		else
		{
			$s = '';
		}
		$out .= "<option value='$row->id' $s>$row->title</option>";
	}
	$out .= '</select>';

	return $out;
}

function GetFieldItem($field_id, $uid)
{
	$db     = JFactory::getDBO();
	$query  = "select item_id from `#__extrareg_fields_values` where field_id = $field_id and uid = $uid";
	$result = $db->setQuery($query);

	return $db->loadResult();
}

function GetSubData($id)
{
	$db     = JFactory::getDBO();
	$query  = "select subscription_type from `#__billing_subscription_type` where id = $id";
	$result = $db->setQuery($query);

	return $db->loadResult();
}

function GetCountrySelect($no, $id = '')
{
	$db     = JFactory::getDBO();
	$query  = "select country_id, name from `#__extrareg_country`";
	$result = $db->setQuery($query);
	$cns    = $db->loadObjectList();
	$out    = "<select name='country_$no' id='country_$no' onchange='ajaxfunction(null, $no);'>";
	$out .= "<option value='0'></option>";
	$s = '';
	foreach ($cns as $cn)
	{
		if ($cn->country_id == $id)
		{
			$s = 'selected';
		}
		$out .= "<option value='$cn->country_id' $s>$cn->name</option>";
		if ($s != '')
		{
			$s = '';
		};
	}
	$out .= "</select>";

	return $out;
}

function GetRegionSelect($no, $id = '', $country_id)
{
	if ($country_id == '')
	{
		return '';
	}
	$db     = JFactory::getDBO();
	$query  = "select region_id, name from `#__extrareg_region` where country_id = $country_id";
	$result = $db->setQuery($query);
	$cns    = $db->loadObjectList();
	$out    = "<select name='region_$no' id='region_$no' onchange='ajaxfunction(1, $no);'>";
	//$out .= "<option value='0'></option>";
	$s = '';
	foreach ($cns as $cn)
	{
		if ($cn->region_id == $id)
		{
			$s = 'selected';
		}
		$out .= "<option value='$cn->region_id' $s>$cn->name</option>";
		if ($s != '')
		{
			$s = '';
		};
	}
	$out .= "</select>";

	return $out;
}

function GetCitySelect($no, $id = '', $region_id)
{
	if ($region_id == '')
	{
		return '';
	}
	$db     = JFactory::getDBO();
	$query  = "select city_id, name from `#__extrareg_city` where region_id = $region_id";
	$result = $db->setQuery($query);
	$cns    = $db->loadObjectList();
	$out    = "<select name='field_$no' id='field_$no'>";
	$s      = '';
	foreach ($cns as $cn)
	{
		if ($cn->city_id == $id)
		{
			$s = 'selected';
		}
		$out .= "<option value='$cn->city_id' $s>$cn->name</option>";
		if ($s != '')
		{
			$s = '';
		};
	}
	$out .= "</select>";

	return $out;
}

function GetUserData($id)
{
	$id     = (int) $id;
	$db     = JFactory::getDBO();
	$query  = "select * from `#__users` where id = $id";
	$result = $db->setQuery($query);
	$result = $db->loadAssoc();

	return $result['name'] . ' (' . $result['username'] . ')';
}

function GetUserList($uid = '', $fieldname = 'uid')
{
	$db     = JFactory::getDBO();
	$query  = 'select * from `#__users` order by name';
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();
	$out    = '';
	$out .= "<select name='$fieldname' id='$fieldname'>";
	foreach ($rows as $row)
	{
		if ($row->id == $uid)
		{
			$s = 'selected';
		}
		else
		{
			$s = '';
		}
		$out .= "<option value='$row->id' $s>$row->name</option>";
	}
	$out .= '</select>';

	return $out;
}

function GetBillingGroups($billing_enabled, $id = '')
{
	if (!$billing_enabled)
	{
		return '';
	}
	else
	{
		$db     = JFactory::getDBO();
		$query  = 'select * from `#__billing_group` order by group_name';
		$result = $db->setQuery($query);
		$rows   = $db->loadObjectList();
		$out    = '';
		$out .= "<select name='billgroup_id' id='billgroup_id' style='float:none;'>";
		foreach ($rows as $row)
		{
			if ($row->id == $id)
			{
				$s = 'selected';
			}
			else
			{
				$s = '';
			}
			$out .= "<option value='$row->id' $s>$row->group_name</option>";
		}
		$out .= '</select>';

		return $out;
	}
}

function FieldList()
{
	$app            = JFactory::getApplication();
	$admin_template = $app->getTemplate();

	JToolBarHelper::addNew();
	JToolBarHelper::publishList();
	JToolBarHelper::unpublishList();
	JToolBarHelper::trash();

	$db     = JFactory::getDBO();
	$query  = 'select * from `#__extrareg_fields` order by order_id';
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();

	echo '<table width="100%" class="table table-striped adminlist" cellspacing="1">';
	echo "<thead><tr>";
	echo "  <th class='title' width='1%'>Id</th>";
	echo '  <th class="title" width="20"><input type="checkbox" id="checkbox" name="toggle" onclick="checkAll(1000);"></th>';
	echo "  <th class='title'>" . JText::_('JGLOBAL_TITLE') . "</th>";
	echo "  <th class='title' width='1%'>" . JText::_('JREQUIRED') . "</th>";
	echo "  <th class='title' width='1%'>" . JText::_('JREGISTER') . "</th>";
	echo "  <th class='title' width='1%'>" . JText::_('COM_EXTRAREGISTRATION_CANEDIT') . "</th>";
	echo "  <th class='title' width='1%'>" . JText::_('COM_EXTRAREGISTRATION_FORADMIN') . "</th>";
	echo "  <th class='title' width='1%'>" . JText::_('JFIELD_ORDERING_LABEL') . "</th>";
	echo "  <th class='title' width='1%'>" . JText::_('JPUBLISHED') . "</th>";
	echo "  <th class='title' width='1%'>" . JText::_('JACTION_DELETE') . "</th>";
	echo "</tr></thead>";
	if (count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			echo '<tr>';
			echo "<td>$row->id</td>";
			echo "<td><input type='checkbox' id='cb$row->id' name='cid[]' value='$row->id' onclick='isChecked(this.checked);'></td>";
			echo "<td><a href='index.php?option=com_extraregistration&task=edit&id=$row->id'>$row->title</a></td>";
			if ($row->required == 0)
			{
				echo "<td><a href='index.php?option=com_extraregistration&task=required&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('JREQUIRED') . "' title='" . JText::_('JREQUIRED') . "' src='/administrator/templates/$admin_template/images/admin/publish_x.png'></a></td>";
			}
			else
			{
				echo "<td><a href='index.php?option=com_extraregistration&task=required&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('JREQUIRED') . "' title='" . JText::_('JREQUIRED') . "' src='/administrator/templates/$admin_template/images/admin/tick.png'></a></td>";
			}
			if ($row->reg_form == 0)
			{
				echo "<td><a href='index.php?option=com_extraregistration&task=reg&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('JREQUIRED') . "' title='" . JText::_('JREGISTER') . "' src='/administrator/templates/$admin_template/images/admin/publish_x.png'></a></td>";
			}
			else
			{
				echo "<td><a href='index.php?option=com_extraregistration&task=reg&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('JREQUIRED') . "' title='" . JText::_('JREGISTER') . "' src='/administrator/templates/$admin_template/images/admin/tick.png'></a></td>";
			}
			if ($row->can_edit == 0)
			{
				echo "<td><a href='index.php?option=com_extraregistration&task=canedit&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('COM_EXTRAREGISTRATION_CANEDIT') . "' title='" . JText::_('COM_EXTRAREGISTRATION_CANEDIT') . "' src='/administrator/templates/$admin_template/images/admin/publish_x.png'></a></td>";
			}
			else
			{
				echo "<td><a href='index.php?option=com_extraregistration&task=canedit&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('COM_EXTRAREGISTRATION_CANEDIT') . "' title='" . JText::_('COM_EXTRAREGISTRATION_CANEDIT') . "' src='/administrator/templates/$admin_template/images/admin/tick.png'></a></td>";
			}
			if ($row->for_admin == 0)
			{
				echo "<td><a href='index.php?option=com_extraregistration&task=foradmin&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('COM_EXTRAREGISTRATION_FORADMIN') . "' title='" . JText::_('COM_EXTRAREGISTRATION_FORADMIN') . "' src='/administrator/templates/$admin_template/images/admin/publish_x.png'></a></td>";
			}
			else
			{
				echo "<td><a href='index.php?option=com_extraregistration&task=foradmin&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('COM_EXTRAREGISTRATION_FORADMIN') . "' title='" . JText::_('COM_EXTRAREGISTRATION_FORADMIN') . "' src='/administrator/templates/$admin_template/images/admin/tick.png'></a></td>";
			}
			echo "<td><a href='index.php?option=com_extraregistration&task=orderup&id=$row->id'><img border='0' src='/administrator/templates/$admin_template/images/admin/sort_asc.png'></a> <a href='index.php?option=com_extraregistration&task=orderdown&id=$row->id'><img border='0' src='/administrator/templates/$admin_template/images/admin/sort_desc.png'></a></td>";
			if ($row->published == 0)
			{
				echo "<td><a href='index.php?option=com_extraregistration&task=active&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('JPUBLISHED') . "' title='" . JText::_('JPUBLISHED') . "' src='/administrator/templates/$admin_template/images/admin/publish_x.png'></a></td>";
			}
			else
			{
				echo "<td><a href='index.php?option=com_extraregistration&task=active&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('JUNPUBLISHED') . "' title='" . JText::_('JUNPUBLISHED') . "' src='/administrator/templates/$admin_template/images/admin/tick.png'></a></td>";
			}
			echo "<td><a href='index.php?option=com_extraregistration&task=delete&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('JTOOLBAR_DELETE') . "' title='" . JText::_('JTOOLBAR_DELETE') . "' src='/administrator/templates/$admin_template/images/admin/publish_x.png'></a></td>";
			echo '</tr>';
		}
	}
	echo '</table>';
}

function EditField($id = '', $billing_enabled = false, $sms_enabled = false)
{
	JToolBarHelper::save();
	JToolBarHelper::cancel();
	$db     = JFactory::getDBO();
	$editor = JFactory::getEditor();

	if ($id != '')
	{
		$query         = 'select * from `#__extrareg_fields` where id = ' . $id;
		$result        = $db->setQuery($query);
		$row           = $db->loadAssoc();
		$title         = $row['title'];
		$type          = $row['field_type'];
		$list_id       = $row['list_id'];
		$comment       = $row['comment'];
		$can_edit      = $row['can_edit'];
		$filter_groups = explode(',', $row['groups']);
		echo '<input type="hidden" name="id" value="' . $id . '" />';
	}
	else
	{
		$title         = '';
		$type          = 1;
		$comment       = '';
		$can_edit      = 1;
		$list_id       = -1;
		$filter_groups = array();
	}

	echo '<table border="0">';
	echo "<tr><td>" . JText::_('JGLOBAL_TITLE') . "</td><td><input type='text' name='title' value='$title' size='100'></td></tr>";
	echo "<tr><td>" . JText::_('COM_EXTRAREGISTRATION_COMMENT') . "</td><td><input type='text' name='comment' value='$comment' size='100'></td></tr>";
	echo "<tr><td>" . JText::_('COM_EXTRAREGISTRATION_TYPE') . "</td><td>";
	echo "<select name='field_type' 
		onchange='if(this.value == 7 || this.value == 12 || this.value == 13)
			{document.getElementById(\"list\").hidden = false;} 
			else {document.getElementById(\"list\").hidden = true;} 
			if(this.value == 10){document.getElementById(\"list2\").hidden = false;} 
			else {document.getElementById(\"list2\").hidden = true;}'>";
	if ($type == 1)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	echo "<option value='1' $sel>" . JText::_('COM_EXTRAREGISTRATION_TEXT') . "</option>";
	if ($type == 2)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	echo "<option value='2' $sel>" . JText::_('COM_EXTRAREGISTRATION_NUMBER') . "</option>";
	if ($type == 3)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	echo "<option value='3' $sel>" . JText::_('COM_EXTRAREGISTRATION_IMAGE') . "</option>";
	if ($type == 4)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	echo "<option value='4' $sel>" . JText::_('COM_EXTRAREGISTRATION_DATE') . "</option>";
	if ($type == 5)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	echo "<option value='5' $sel>" . JText::_('COM_EXTRAREGISTRATION_CITY') . "</option>";
	if ($type == 6)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	echo "<option value='6' $sel>" . JText::_('COM_EXTRAREGISTRATION_MONEY') . "</option>";
	if ($type == 7)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	echo "<option value='7' $sel>" . JText::_('COM_EXTRAREGISTRATION_LIST') . "</option>";
	if ($type == 8)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	echo "<option value='8' $sel>" . JText::_('COM_EXTRAREGISTRATION_PASS') . "</option>";
	if ($type == 9)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	echo "<option value='9' $sel>" . JText::_('JGLOBAL_USERNAME') . "</option>";
	if ($billing_enabled)
	{
		if ($type == 10)
		{
			$sel = 'selected';
		}
		else
		{
			$sel = '';
		}
		echo "<option value='10' $sel>" . JText::_('COM_EXTRAREGISTRATION_SUBSCRIPTION') . "</option>";
	}
	if ($sms_enabled)
	{
		if ($type == 11)
		{
			$sel = 'selected';
		}
		else
		{
			$sel = '';
		}
		echo "<option value='11' $sel>" . JText::_('COM_EXTRAREGISTRATION_PHONE') . "</option>";
	}
	if ($type == 12)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	echo "<option value='12' $sel>" . JText::_('COM_EXTRAREGISTRATION_MULTISELECT') . "</option>";

	if ($type == 13)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	echo "<option value='13' $sel>" . JText::_('COM_EXTRAREGISTRATION_MULTISELECT') . " 2</option>";

	if ($type == 14)
	{
		$sel = 'selected';
	}
	else
	{
		$sel = '';
	}
	echo "<option value='14' $sel>" . JText::_('COM_EXTRAREGISTRATION_TEXTAREA') . "</option>";

	echo "</select>";

	$lists  = '';
	$query  = "select * from `#__extrareg_list` order by id";
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();
	if (count($rows) > 0)
	{
		$h = '';
		if ($type != 7 and $type != 12 and $type != 13)
		{
			$h = 'hidden="true"';
		}
		$lists = ' <select name="list" id="list" ' . $h . '>';
		foreach ($rows as $row)
		{
			if ($list_id == $row->id)
			{
				$sel = 'selected';
			}
			else
			{
				$sel = '';
			}
			$lists .= "<option value='$row->id' $sel>$row->title</option>";

		}
		$lists .= '</select>';
	}

	echo $lists;

	$lists2 = '';
	if ($billing_enabled)
	{
		$query  = "select * from `#__billing_subscription_type` where is_active = 1 order by subscription_type";
		$result = $db->setQuery($query);
		$rows   = $db->loadObjectList();
		if (count($rows) > 0)
		{
			$h = '';
			if ($type != 10)
			{
				$h = 'hidden="true"';
			}
			$lists2 = ' <select name="list2" id="list2" ' . $h . '>';
			foreach ($rows as $row)
			{
				if ($list_id == $row->id)
				{
					$sel = 'selected';
				}
				else
				{
					$sel = '';
				}
				$lists2 .= "<option value='$row->id' $sel>$row->subscription_type</option>";

			}
			$lists2 .= '</select>';
		}
	}
	echo $lists2;
	echo "</td></tr>";

	// groups
	echo '<tr><td>' . JText::_('COM_EXTRAREGISTRATION_USER_GROUPS') . '</td><td style="padding: 8px 0;">';
	echo JHTML::_('access.usergroup', 'field_groups[]', $filter_groups, 'multiple="multiple" size="6"');
	echo '</td></tr>';

	echo '</table>';
}

function SaveField($id = '')
{
	$jinput     = JFactory::getApplication()->input;
	$title      = JRequest::getVar('title');
	$comment    = JRequest::getVar('comment');
	$field_type = JRequest::getInt('field_type');
	$list       = JRequest::getInt('list', 0);
	$groups     = array_filter($jinput->get('field_groups', array(), 'ARRAY'), function ($el)
	{
		return $el === '' || (is_numeric($el) && $el > 0);
	});
	$groups     = in_array('', $groups, true) ? '' : implode(',', $groups);
	$db         = JFactory::getDBO();

	if ($id == '' or $id == 0)
	{
		$query = "insert into `#__extrareg_fields` 
			(
				`title`,
				`field_type`,
				`comment`,
				`list_id`,
				`groups`
			)
			values
			(
				'$title',
				$field_type,
				'$comment',
				$list,
				'$groups'
			)";
	}
	else
	{
		$query = "update `#__extrareg_fields` set
			`title` = '$title',
			`field_type` = $field_type,
			`comment` = '$comment',
			`list_id` = $list,
			`groups` = '$groups'
		where id = $id";
	}
	$result = $db->setQuery($query);
	$result = $db->query();
}

function ActivateField($id)
{
	$db = JFactory::getDBO();

	$query  = 'select published from `#__extrareg_fields` where id = ' . $id;
	$result = $db->setQuery($query);
	$n      = $db->loadResult();

	if ($n == 1)
	{
		$n = 0;
	}
	else
	{
		$n = 1;
	}
	$query  = "update `#__extrareg_fields` set published = $n where id = $id";
	$result = $db->setQuery($query);
	$result = $db->query();
}

function ReqireField($id)
{
	$db = JFactory::getDBO();

	$query  = 'select required from `#__extrareg_fields` where id = ' . $id;
	$result = $db->setQuery($query);
	$n      = $db->loadResult();

	if ($n == 1)
	{
		$n = 0;
	}
	else
	{
		$n = 1;
	}
	$query  = "update `#__extrareg_fields` set required = $n where id = $id";
	$result = $db->setQuery($query);
	$result = $db->query();
}

function RegField($id)
{
	$db = JFactory::getDBO();

	$query  = 'select reg_form from `#__extrareg_fields` where id = ' . $id;
	$result = $db->setQuery($query);
	$n      = $db->loadResult();

	if ($n == 1)
	{
		$n = 0;
	}
	else
	{
		$n = 1;
	}
	$query  = "update `#__extrareg_fields` set reg_form = $n where id = $id";
	$result = $db->setQuery($query);
	$result = $db->query();
}

function CanEditField($id)
{
	$db = JFactory::getDBO();

	$query  = 'select can_edit from `#__extrareg_fields` where id = ' . $id;
	$result = $db->setQuery($query);
	$n      = $db->loadResult();

	if ($n == 1)
	{
		$n = 0;
	}
	else
	{
		$n = 1;
	}
	$query  = "update `#__extrareg_fields` set can_edit = $n where id = $id";
	$result = $db->setQuery($query);
	$result = $db->query();
}

function ForAdmin($id)
{
	$db = JFactory::getDBO();

	$query  = 'select for_admin from `#__extrareg_fields` where id = ' . $id;
	$result = $db->setQuery($query);
	$n      = $db->loadResult();

	if ($n == 1)
	{
		$n = 0;
	}
	else
	{
		$n = 1;
	}
	$query  = "update `#__extrareg_fields` set for_admin = $n where id = $id";
	$result = $db->setQuery($query);
	$result = $db->query();
}

function Publish()
{
	$db = JFactory::getDBO();
	foreach ($_POST['cid'] as $checkbox)
	{
		$id     = $checkbox;
		$query  = 'update `#__extrareg_fields` set published = 1 where id =' . $id;
		$result = $db->setQuery($query);
		$result = $db->query();
	}
}

function UnPublish()
{
	$db = JFactory::getDBO();
	foreach ($_POST['cid'] as $checkbox)
	{
		$id     = $checkbox;
		$query  = 'update `#__extrareg_fields` set published = 0 where id =' . $id;
		$result = $db->setQuery($query);
		$result = $db->query();
	}
}

function UserList($order = '', $limitstart = '', $limit = '')
{
	$app            = JFactory::getApplication();
	$admin_template = $app->getTemplate();

	if ($limitstart == '')
	{
		$limitstart = 0;
	}
	if ($limit == '')
	{
		$limit = 5;
	}
	if ($limitstart == 0 and $limit == 0)
	{
		$lim = '';
	}
	else
	{
		$lim = "limit $limitstart, $limit";
	}

	if ($order == '')
	{
		$order = 'id';
	}

	JToolBarHelper::custom('pubuser', 'unblock', 'unblock', JText::_('JTOOLBAR_PUBLISH'), true);
	JToolBarHelper::custom('unpubuser', 'unpublish', 'unpublish', JText::_('JTOOLBAR_UNPUBLISH'), true);
	JToolBarHelper::custom('deluser', 'delete', 'delete', JText::_('JTOOLBAR_DELETE'), true);
	//JToolBarHelper::custom( 'actuser', 'publish', 'publish', JText::_('JTOOLBAR_ENABLE'), true );

	$db     = JFactory::getDBO();
	$query  = "select * from `#__users` order by $order $lim";
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();

	$query  = 'select count(*) from `#__users`';
	$result = $db->setQuery($query);
	$total  = $db->loadResult();

	$query  = 'select * from `#__extrareg_fields` where published = 1 order by order_id';
	$result = $db->setQuery($query);
	$cols   = $db->loadObjectList();

	echo '<table width="100%" class="table table-striped adminlist" cellspacing="1">';
	echo "<thead><tr>";
	echo "  <th class='title' width='1%'>Id</th>";
	echo '  <th class="title" width="20"><input type="checkbox" id="checkbox" name="toggle" onclick="checkAll(1000);"></th>';
	echo "  <th class='title'>" . JText::_('JGLOBAL_USERNAME') . "</th>";
	if (count($cols) > 0)
	{
		foreach ($cols as $col)
		{
			echo "  <th class='title'>$col->title</th>";
		}
	}
	echo "  <th class='title' width='1%'>" . JText::_('JPUBLISHED') . "</th>";
	echo "  <th class='title' width='1%'>" . JText::_('JACTION_DELETE') . "</th>";
	echo "</tr></thead>";
	if (count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			echo '<tr>';
			echo "<td>$row->id</td>";
			echo "<td><input type='checkbox' id='cb$row->id' name='cid[]' value='$row->id' onclick='isChecked(this.checked);'></td>";
			echo "<td><a href='index.php?option=com_extraregistration&task=edituser&id=$row->id'>$row->name ($row->username)</a></td>";

			if (count($cols) > 0)
			{
				foreach ($cols as $col)
				{
					if ($col->field_type == 1 or $col->field_type == 2 or $col->field_type == 4 or $col->field_type == '')
					{
						echo "<td>" . GetFieldValue($col->id, $row->id) . "</td>";
					}
					if ($col->field_type == 3)
					{
						if (GetFieldValue($col->id, $row->id) != '')
						{
							echo "<td><img src='" . GetFieldValue($col->id, $row->id) . "' style='height: 50px !important;'></td>";
						}
						else
						{
							echo "<td></td>";
						}
					}
					if ($col->field_type == 5)
					{
						echo "<td>" . GetFieldValue($col->id, $row->id) . "</td>";
					}
					if ($col->field_type == 6)
					{
						echo "<td>" . GetFieldValue($col->id, $row->id) . " " . GetOption('abbr') . "</td>";
					}
					if ($col->field_type == 7)
					{
						echo "<td>" . GetFieldValue($col->id, $row->id) . "</td>";
					}
					if ($col->field_type == 8)
					{
						echo "<td>[password]</td>";
					}
					if ($col->field_type == 9)
					{
						echo "<td>" . GetUserData(GetFieldValue($col->id, $row->id)) . "</td>";
					}
					if ($col->field_type == 10)
					{
						echo "<td>" . GetSubData(GetFieldValue($col->id, $row->id)) . "</td>";
					}
					if ($col->field_type == 11)
					{
						echo "<td>" . GetFieldValue($col->id, $row->id) . "</td>";
					}
					if ($col->field_type == 12)
					{
						echo "<td>" . GetFieldValue($col->id, $row->id) . "</td>";
					}
					if ($col->field_type == 13)
					{
						echo "<td>" . GetFieldValue($col->id, $row->id) . "</td>";
					}
					if ($col->field_type == 14)
					{
						$b = mb_substr(GetFieldValue($col->id, $row->id), 0, 40);
						//if ($a != $b) 
						//{
						//	$b .= '...';
						//}
						echo "<td>" . $b . "</td>";
					}
				}
			}
			if ($row->block == 1)
			{
				//echo "<td></td>";
				echo "<td><a href='index.php?option=com_extraregistration&task=userpub&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('JPUBLISHED') . "' title='" . JText::_('JPUBLISHED') . "' src='/administrator/templates/$admin_template/images/admin/publish_x.png'></a></td>";
			}
			else
			{
				//echo "<td></td>";
				echo "<td><a href='index.php?option=com_extraregistration&task=userpub&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('JPUBLISHED') . "' title='" . JText::_('JPUBLISHED') . "' src='/administrator/templates/$admin_template/images/admin/tick.png'></a></td>";
			}
			echo "<td><a href='index.php?option=com_extraregistration&task=deleteuser&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('JACTION_DELETE') . "' title='" . JText::_('JACTION_DELETE') . "' src='/administrator/templates/$admin_template/images/admin/publish_x.png'></a></td>";
			echo '</tr>';
		}
	}
	echo '</table>';

	jimport('joomla.html.pagination');
	$pagenav = new JPagination ($total, $limitstart, $limit);
	echo $pagenav->getListFooter();
}

function CheckGroup($grp, $id)
{
	foreach ($grp as $g)
	{
		if ($g == $id)
		{
			return true;
		}
	}

	return false;
}

function CheckSubs($grp, $id)
{
	foreach ($grp as $g)
	{
		if ($g == $id)
		{
			return true;
		}
	}

	return false;
}

function ShowOptions($billing_enabled = false, $sms_enabled = false)
{
	$db = JFactory::getDBO();
	echo '<h1>' . JText::_('COM_EXTRAREGISTRATION_OPTIONS') . '</h1>';
	JToolBarHelper::custom('saveoptions', 'save', 'save', JText::_('JSAVE'), false);
	$abbr          = GetOption('abbr');
	$recaptcha     = GetOption('recaptcha');
	$pass          = GetOption('pass');
	$enable_terms  = GetOption('enable_terms');
	$terms         = GetTextOption('terms');
	$terms_header  = GetOption('terms_header');
	$use_usergroup = GetOption('use_usergroup');
	$integration   = GetOption('integration');
	$grp           = GetOption('usergroup');
	$sbs           = GetOption('excludesub');
	$flt           = GetOption('filters');
	$payreg        = GetOption('payreg');
	//$basesub = GetOption('basesub');
	$pricetype      = GetOption('pricetype');
	$price          = GetOption('price');
	$sid            = GetOption('sid');
	$sid2           = GetOption('sid2');
	$billgroup      = GetOption('billgroup');
	$billgroup_id   = GetOption('billgroup_id');
	$setsub         = GetOption('setsub');
	$smsintegration = GetOption('smsintegration');
	$checkphone     = GetOption('checkphone');
	$origemail      = GetOption('origemail');
	$origemailsub   = GetOption('origemailsub');
	$email          = GetTextOption('email');

	jimport('joomla.html.pane');
	$tabs = '';

# For compatibility with older versions of Joomla 2.5
	if (JVERSION >= '3.0')
	{
		$tabs .= JHtml::_('tabs.start', 'tabs', array('useCookie' => 0, 'startOffset' => 0));
		$tabs .= JHtml::_('tabs.panel', JText::_('COM_EXTRAREGISTRATION_OPTIONS'), 'general');
	}
	else
	{
		$pane = JPane::getInstance('tabs', array('startOffset' => 0));
		$tabs .= $pane->startPane('pane');
		$tabs .= $pane->startPanel(JText::_('COM_EXTRAREGISTRATION_OPTIONS'), 'general');
	}

	$tabs .= '<table border="0">';
	$tabs .= "<tr><td>" . JText::_('COM_EXTRAREGISTRATION_MONEY_ABBR') . "</td><td><input type='text' name='abbr' value='$abbr' size='5'></td></tr>";
	if ($recaptcha == '')
	{
		$recaptcha = false;
	}
	if ($recaptcha == true)
	{
		$checked = 'checked';
	}
	else
	{
		$checked = '';
	}
	$tabs .= "<tr><td>" . JText::_('COM_EXTRAREGISTRATION_USE_RECAPTCHA') . "</td><td><input type='checkbox' name='recaptcha' value='$recaptcha' $checked onclick='this.value = this.checked;'></td></tr>";
	if ($pass == '')
	{
		$pass = false;
	}
	if ($pass == true)
	{
		$checked = 'checked';
	}
	else
	{
		$checked = '';
	}
	$tabs .= "<tr><td>" . JText::_('COM_EXTRAREGISTRATION_CHANGE_PASS') . "</td><td><input type='checkbox' name='pass' value='$pass' $checked onclick='this.value = this.checked;'></td></tr>";
	$tabs .= '</table>';

	if (JVERSION >= 3.0)
	{
		$tabs .= JHtml::_('tabs.panel', JText::_('COM_EXTRAREGISTRATION_TERMS'), 'terms');
	}
	else
	{
		$tabs .= $pane->endPanel();
		$tabs .= $pane->startPanel(JText::_('COM_EXTRAREGISTRATION_TERMS'), 'terms');
	}

	if ($enable_terms == '')
	{
		$enable_terms = false;
	}
	if ($enable_terms == true)
	{
		$checked = 'checked';
	}
	else
	{
		$checked = '';
	}
	$tabs .= "<input id='enable_terms' name='enable_terms' value='$enable_terms' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;" . JText::_('COM_EXTRAREGISTRATION_TERMS_ON') . "<br><br>";
	$x = JText::_('COM_EXTRAREGISTRATION_TERMS_HEADER');
	$tabs .= $x . "<br> <input type='text' name='terms_header' value='$terms_header' size='100'><br><br>";
	$editor = JFactory::getEditor();
	$tabs .= $editor->display('terms', $terms, '600', '200', '20', '20', true);

	if (JVERSION >= '3.0')
	{
		$tabs .= JHtml::_('tabs.panel', JText::_('COM_EXTRAREGISTRATION_USER_GROUPS'), 'usergroups');
	}
	else
	{
		$tabs .= $pane->endPanel();
		$tabs .= $pane->startPanel(JText::_('COM_EXTRAREGISTRATION_USER_GROUPS'), 'usergroups');
	}

	if ($use_usergroup == '')
	{
		$use_usergroup = false;
	}
	if ($use_usergroup == true)
	{
		$checked = 'checked';
	}
	else
	{
		$checked = '';
	}
	$tabs .= "<input id='use_usergroup' name='use_usergroup' value='$use_usergroup' type='checkbox' $checked onclick=\"this.value = this.checked;\"> &nbsp;" . JText::_('COM_EXTRAREGISTRATION_USE_GROUPS') . "<br><br>";

	$query  = "select * from `#__usergroups` order by id";
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();
	foreach ($rows as $row)
	{
		$yes = CheckGroup(explode(',', $grp), $row->id);
		if ($yes)
		{
			$tabs .= "<input type='checkbox' name='cb_$row->id' value='$yes' onclick='this.value = this.checked;' checked='true' style='float:none;'>&nbsp; $row->title <br>";
		}
		else
		{
			$tabs .= "<input type='checkbox' name='cb_$row->id' value='$yes' onclick='this.value = this.checked;' style='float:none;'>&nbsp; $row->title <br>";
		}
	}

	if (JVERSION >= '3.0')
	{
		$tabs .= JHtml::_('tabs.panel', JText::_('COM_EXTRAREGISTRATION_NOTIFY'), 'emails');
	}
	else
	{
		$tabs .= $pane->endPanel();
		$tabs .= $pane->startPanel(JText::_('COM_EXTRAREGISTRATION_NOTIFY'), 'emails');
	}

	$tabs .= '<table border="0">';
	if ($origemail == '')
	{
		$origemail = false;
	}
	if ($origemail == true)
	{
		$checked = 'checked';
	}
	else
	{
		$checked = '';
	}
	$tabs .= "<tr><td>" . JText::_('COM_EXTRAREGISTRATION_ORIG_EMAIL') . "</td><td><input type='checkbox' name='origemail' value='$origemail' $checked onclick='this.value = this.checked;' style='float: none;'></td><td></td></tr>";
	$tabs .= "<tr><td>" . JText::_('COM_EXTRAREGISTRATION_ORIG_EMAIL_SUBJECT') . "</td><td><input type='text' name='origemailsub' value='$origemailsub' style='float: none;' size='80'></td><td></td></tr>";

	$editor2 = JFactory::getEditor();
	$tabs .= "<tr><td colspan='2'><textarea name='email' cols='60' rows='10'>$email</textarea></td><td>";
	$tabs .= JText::_('COM_EXTRAREGISTRATION_MACROS') . ":<br> %id%<br>%sitename%<br>%name%<br>%username%<br>%siteurl%<br>%email%<br>%password%<br>%field_#%";
	$tabs .= "</td></tr>";
	$tabs .= '</table>';

	if (JVERSION < '3.0')
	{
		$tabs .= $pane->endPanel();
	}

	if ($billing_enabled)
	{
		if (JVERSION >= '3.0')
		{
			$tabs .= JHtml::_('tabs.panel', JText::_('COM_EXTRAREGISTRATION_BILLING'), 'billing');
		}
		else
		{
			$tabs .= $pane->startPanel(JText::_('COM_EXTRAREGISTRATION_BILLING'), 'billing');
		}

		if ($integration == '')
		{
			$integration = false;
		}
		if ($integration == true)
		{
			$checked = 'checked';
		}
		else
		{
			$checked = '';
		}
		$tabs .= "<input type='checkbox' name='integration' value='$integration' onclick='this.value = this.checked;' $checked style='float:none;'> " . JText::_('COM_EXTRAREGISTRATION_BILLING_INTEGRATION') . "<br>";
		if ($payreg == '')
		{
			$payreg = false;
		}
		if ($payreg == true)
		{
			$checked = 'checked';
		}
		else
		{
			$checked = '';
		}
		$tabs .= "<input type='checkbox' name='payreg' value='$payreg' style='float:none;' $checked onclick='this.value = this.checked;'> " . JText::_('COM_EXTRAREGISTRATION_PAY_REG') . "<br>";

		if ($pricetype == '1')
		{
			$checked = 'checked';
		}
		else
		{
			$checked = '';
		}
		$tabs .= "<input type='radio' name='pricetype' value='1' $checked style='float:none;' style='float: none;'> " . JText::_('COM_EXTRAREGISTRATION_BASESUB') . " " . GetSubList($sid) . "<br>";
		$tabs .= JText::_('COM_EXTRAREGISTRATION_EXCLUDE_SUB') . ": <br>";

		$query  = "select * from `#__billing_subscription_type` order by subscription_type";
		$result = $db->setQuery($query);
		$rows   = $db->loadObjectList();
		foreach ($rows as $row)
		{
			$yes = CheckSubs(explode(',', $sbs), $row->id);
			if ($yes)
			{
				$tabs .= "&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' name='gx_$row->id' value='$yes' onclick='this.value = this.checked;' checked='true' style='float:none;'>&nbsp; $row->subscription_type <br>";
			}
			else
			{
				$tabs .= "&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' name='gx_$row->id' value='$yes' onclick='this.value = this.checked;' style='float:none;'>&nbsp; $row->subscription_type <br>";
			}
		}

		if ($pricetype == '2')
		{
			$checked = 'checked';
		}
		else
		{
			$checked = '';
		}
		$tabs .= "<input type='radio' name='pricetype' value='2' $checked style='float:none;'> " . JText::_('COM_EXTRAREGISTRATION_BASEPRICE') . " <input type='text' name='price' value='$price' size='5' style='float: none;'> " . GetDefaultCurrencyAbbr() . " <br>";

		if ($billgroup == '')
		{
			$billgroup = false;
		}
		if ($billgroup == true)
		{
			$checked = 'checked';
		}
		else
		{
			$checked = '';
		}
		$tabs .= "<input type='checkbox' name='billgroup' value='$billgroup' $checked style='float:none;' onclick='this.value = this.checked;'> " . JText::_('COM_EXTRAREGISTRATION_BILL_GROUP') . ' ' . GetBillingGroups($billing_enabled, $billgroup_id) . "<br>";

		if ($setsub == '')
		{
			$setsub = false;
		}
		if ($setsub == true)
		{
			$checked = 'checked';
		}
		else
		{
			$checked = '';
		}
		$tabs .= "<input type='checkbox' name='setsub' value='$setsub' $checked style='float:none;' onclick='this.value = this.checked;'> " . JText::_('COM_EXTRAREGISTRATION_SET_SUB') . ' ' . GetSubList($sid2, 'sid2') . "<br>";

		if (JVERSION >= '3.0')
		{
		}
		else
		{
			$tabs .= $pane->endPanel();
		}
	}
	if ($sms_enabled)
	{
		if (JVERSION >= '3.0')
		{
			$tabs .= JHtml::_('tabs.panel', JText::_('COM_EXTRAREGISTRATION_SMS'), 'sms');
		}
		else
		{
			$tabs .= $pane->startPanel(JText::_('COM_EXTRAREGISTRATION_SMS'), 'sms');
		}

		if ($smsintegration == '')
		{
			$smsintegration = false;
		}
		if ($smsintegration == true)
		{
			$checked = 'checked';
		}
		else
		{
			$checked = '';
		}
		$tabs .= "<input type='checkbox' name='smsintegration' value='$smsintegration' onclick='this.value = this.checked;' $checked style='float:none;'> " . JText::_('COM_EXTRAREGISTRATION_SMS_INTEGRATION') . "<br>";
		if ($checkphone == '')
		{
			$checkphone = false;
		}
		if ($checkphone == true)
		{
			$checked = 'checked';
		}
		else
		{
			$checked = '';
		}
		$tabs .= "<input type='checkbox' name='checkphone' value='$checkphone' onclick='this.value = this.checked;' $checked style='float:none;'> " . JText::_('COM_EXTRAREGISTRATION_CHECK_PHONE') . "<br>";
		if (JVERSION >= '3.0')
		{
		}
		else
		{
			$tabs .= $pane->endPanel();
		}
	}
	if ($billing_enabled)
	{
		$ordinar_sub = GetOption('ordinar_sub');
		$vip_sub     = GetOption('vip_sub');
		$sid3        = GetOption('sid3');
		$sid4        = GetOption('sid4');
		$pic_id      = GetOption('pic_id');

		if (JVERSION >= '3.0')
		{
			$tabs .= JHtml::_('tabs.panel', JText::_('COM_EXTRAREGISTRATION_SUBS'), 'sms');
		}
		else
		{
			$tabs .= $pane->startPanel(JText::_('COM_EXTRAREGISTRATION_SUBS'), 'sms');
		}

		if ($ordinar_sub == '')
		{
			$ordinar_sub = false;
		}
		if ($ordinar_sub == true)
		{
			$checked = 'checked';
		}
		else
		{
			$checked = '';
		}
		$tabs .= "<input type='checkbox' name='ordinar_sub' value='$ordinar_sub' onclick='this.value = this.checked;' $checked style='float:none;'> " . JText::_('COM_EXTRAREGISTRATION_ORDINAR_SUB') . ' ' . GetSubList($sid3, 'sid3') . "<br>";
		if ($vip_sub == '')
		{
			$vip_sub = false;
		}
		if ($vip_sub == true)
		{
			$vip_sub = 'checked';
		}
		else
		{
			$checked = '';
		}
		$tabs .= "<input type='checkbox' name='vip_sub' value='$vip_sub' onclick='this.value = this.checked;' $checked style='float:none;'> " . JText::_('COM_EXTRAREGISTRATION_VIP_SUB') . ' ' . GetSubList($sid4, 'sid4') . "<br>";
		$tabs .= JText::_('COM_EXTRAREGISTRATION_PHOTO') . ' ' . GetFieldsList($pic_id, 'pic_id') . "<br>";

		$tabs .= '<br>' . JText::_('COM_EXTRAREGISTRATION_FILTER_FIELDS') . '<br>';
		$query  = "select * from `#__extrareg_fields` order by order_id";
		$result = $db->setQuery($query);
		$rows   = $db->loadObjectList();
		foreach ($rows as $row)
		{
			$yes = CheckGroup(explode(',', $flt), $row->id);
			if ($yes)
			{
				$tabs .= "<input type='checkbox' name='ft_$row->id' value='$yes' onclick='this.value = this.checked;' checked='true' style='float:none;'>&nbsp; $row->title <br>";
			}
			else
			{
				$tabs .= "<input type='checkbox' name='ft_$row->id' value='$yes' onclick='this.value = this.checked;' style='float:none;'>&nbsp; $row->title <br>";
			}
		}

		if (JVERSION >= '3.0')
		{
		}
		else
		{
			$tabs .= $pane->endPanel();
		}

	}

	if (JVERSION >= '3.0')
	{
		$tabs .= JHtml::_('tabs.end');
	}
	else
	{
		$tabs .= $pane->endPane();
	}

	echo $tabs;
}

function SaveOptions($billing_enabled = false, $sms_enabled = false)
{
	$db = JFactory::getDBO();
	SaveOption('abbr', JRequest::getVar('abbr'));
	SaveOption('recaptcha', JRequest::getVar('recaptcha'));
	SaveOption('terms_header', JRequest::getVar('terms_header'));
	SaveOption('enable_terms', JRequest::getVar('enable_terms'));
	SaveOption('use_usergroup', JRequest::getVar('use_usergroup'));
	SaveOption('integration', JRequest::getVar('integration'));
	SaveOption('payreg', JRequest::getVar('payreg'));
	SaveOption('sid', JRequest::getVar('sid'));
	SaveOption('sid2', JRequest::getVar('sid2'));

	SaveOption('pricetype', JRequest::getVar('pricetype'));
	SaveOption('price', JRequest::getVar('price'));
	SaveOption('billgroup', JRequest::getVar('billgroup'));
	SaveOption('billgroup_id', JRequest::getVar('billgroup_id'));
	SaveOption('setsub', JRequest::getVar('setsub'));
	SaveOption('smsintegration', JRequest::getVar('smsintegration'));
	SaveOption('checkphone', JRequest::getVar('checkphone'));

	SaveOption('ordinar_sub', JRequest::getVar('ordinar_sub'));
	SaveOption('vip_sub', JRequest::getVar('vip_sub'));
	SaveOption('sid3', JRequest::getVar('sid3'));
	SaveOption('sid4', JRequest::getVar('sid4'));
	SaveOption('pic_id', JRequest::getVar('pic_id'));

	$terms = JRequest::getVar('terms', '', 'post', 'string', JREQUEST_ALLOWHTML);
	SaveOption('terms', '', $terms);
	SaveOption('pass', JRequest::getVar('pass'));

	SaveOption('origemail', JRequest::getVar('origemail'));
	SaveOption('origemailsub', JRequest::getVar('origemailsub'));
	$email = JRequest::getVar('email', '', 'post', 'string', JREQUEST_ALLOWHTML);
	SaveOption('email', '', $email);

	$grp    = '';
	$query  = "select * from `#__usergroups` order by id";
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();

	foreach ($rows as $row)
	{
		$x = JRequest::getVar("cb_$row->id");
		if ($x)
		{
			if ($grp == '')
			{
				$grp = $row->id;
			}
			else
			{
				$grp .= ",$row->id";
			}
		}
	}

	SaveOption('usergroup', $grp);

	$flt    = '';
	$query  = "select * from `#__extrareg_fields` order by order_id";
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();

	foreach ($rows as $row)
	{
		$x = JRequest::getVar("ft_$row->id");
		if ($x)
		{
			if ($flt == '')
			{
				$flt = $row->id;
			}
			else
			{
				$flt .= ",$row->id";
			}
		}
	}

	SaveOption('filters', $flt);

// Есть таблица с billing    
	$pref   = $db->getPrefix();
	$dbList = $db->getTableList();
	$tbl    = $pref . 'billing_subscription_type';
	if (in_array($tbl, $dbList))
	{
		$sbs   = '';
		$query = "select *
                  from `#__billing_subscription_type` 
                  order by subscription_type
                 ";

		$result = $db->setQuery($query);
		$rows   = $db->loadObjectList();

		foreach ($rows as $row)
		{
			$x = JRequest::getVar("gx_$row->id");
			if ($x)
			{
				if ($sbs == '')
				{
					$sbs = $row->id;
				}
				else
				{
					$sbs .= ",$row->id";
				}
			}
		}
		SaveOption('excludesub', $sbs);
	}

	SystemMessage(JText::_('COM_EXTRAREGISTRATION_OPTIONS_SAVED'));

}

function GetOption($option)
{
	$db     = JFactory::getDBO();
	$query  = "select value from `#__extrareg_options` where `option` = '$option'";
	$result = $db->setQuery($query);
	$result = $db->loadResult();

	//BillingLogMessage('Extra Registration', 'GetOption', "$option = $result");
	return $result;
}

function GetTextOption($option)
{
	$db     = JFactory::getDBO();
	$query  = "select text_data from `#__extrareg_options` where `option` = '$option'";
	$result = $db->setQuery($query);
	$result = $db->loadResult();

	return $result;
}

function SaveOption($option, $value, $text = '')
{
	//BillingLogMessage('Extra Registration', 'SaveOption', "$option = $value");
	$db     = JFactory::getDBO();
	$query  = "select count(*) from `#__extrareg_options` where `option` = '$option'";
	$result = $db->setQuery($query);
	$n      = $db->loadResult();

	$date = date('Y-m-d H:i:s');
	$user = JFactory::getUser();
	$uid  = $user->id;

	if ($n > 0)
	{
		$query = "update `#__extrareg_options` set `value` = '$value', setdate = '$date', uid = $uid, text_data = '$text' where `option` = '$option'";
	}
	else
	{
		$query = "insert into `#__extrareg_options` (`value`, `option`, setdate, uid, text_data) values ('$value', '$option', '$date', $uid, '$text')";
	}

	$result = $db->setQuery($query);
	$result = $db->query();
}

function EditUser($id)
{
	JToolBarHelper::custom('saveuser', 'save', 'save', JText::_('JSAVE'), false);
	JToolBarHelper::custom('canceluser', 'cancel', 'cancel', JText::_('JCANCEL'), false);

	JHTML::_('behavior.modal');
	$db     = JFactory::getDBO();
	$query  = "select * from `#__users` where `id` = '$id'";
	$result = $db->setQuery($query);
	$user   = $db->loadAssoc();
	$uid    = $id;

	$user_groups = JFactory::getUser($id)->get('groups');

	$query  = "select * from `#__extrareg_fields` order by order_id";
	$result = $db->setQuery($query);
	$rows   = ComExtraregistrationHelper::filterFieldsByUserGroups($db->loadObjectList(), $user_groups);
	$r      = '';
	if (count($rows) > 0)
	{
		ComExtraregistrationHelper::printFieldsHtml($rows, $uid, true);
		echo "<input type='hidden' name='id' value='$uid'>";
	}
}

function SetUserValue($field_id, $uid, $value, $item_id = 0)
{
	$db     = JFactory::getDBO();
	$query  = "select count(*) from `#__extrareg_fields_values` where field_id = $field_id and uid = $uid";
	$result = $db->setQuery($query);
	$result = $db->loadResult();
	if ($result > 0)
	{
		$query = "update `#__extrareg_fields_values` set `value` = '$value', item_id = $item_id where field_id = $field_id and uid = $uid";
	}
	else
	{
		$query = "insert into `#__extrareg_fields_values` (uid, field_id, `value`, item_id) values ($uid, $field_id, '$value', $item_id)";
	}
	$result = $db->setQuery($query);
	$result = $db->query();
}

function SaveUser($id)
{
	$db     = JFactory::getDBO();
	$query  = "select * from `#__users` where `id` = '$id'";
	$result = $db->setQuery($query);
	$user   = $db->loadAssoc();
	$uid    = $id;

	$query  = 'select * from `#__extrareg_fields` order by order_id';
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();
	if (count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			if ($row->field_type == 1 or $row->field_type == 2 or $row->field_type == 4 or $row->field_type == 6 or $row->field_type == 14 or $row->field_type == '')
			{
				$val = JRequest::getVar("field_$row->id");
				SetUserValue($row->id, $uid, $val);
			}
			if ($row->field_type == 3)
			{
				if ($_FILES["field_$row->id"]['tmp_name'] != '')
				{
					$url       = '';
					$imageinfo = getimagesize($_FILES["field_$row->id"]['tmp_name']);
					if ($imageinfo['mime'] == 'image/gif' or $imageinfo['mime'] == 'image/jpeg' or $imageinfo['mime'] == 'image/png')
					{
						$tmp_name  = $_FILES["field_$row->id"]['tmp_name'];
						$file_name = $_FILES["field_$row->id"]['name'];
						$path_info = pathinfo($file_name);
						$ext       = $path_info['extension'];
						$s         = uniqid(rand(), true);
						$new_name  = JPATH_ROOT . '/media/com_extraregistration/' . $s . '.' . $ext;
						$x         = move_uploaded_file($tmp_name, $new_name);
						$url       = JURI::base() . 'media/com_extraregistration/' . $s . '.' . $ext;
						$url       = str_replace('administrator/', '', $url);
						SetUserValue($row->id, $uid, $url);
					}
				}
			}
			if ($row->field_type == 5)
			{
				$city_id  = JRequest::getInt("field_$row->id");
				$query    = "select * from `#__extrareg_city` where city_id = $city_id";
				$result   = $db->setQuery($query);
				$city     = $db->loadAssoc();
				$cityname = $city['name'];
				SetUserValue($row->id, $uid, $cityname, $city_id);
			}
			if ($row->field_type == 7)
			{
				$item_id = JRequest::getInt("field_$row->id");
				$query   = "select title from `#__extrareg_list_value` where list_id = $row->list_id and id = $item_id order by id";
				$result  = $db->setQuery($query);
				$value   = $db->loadResult();
				SetUserValue($row->id, $uid, $value, $item_id);
			}
			if ($row->field_type == 12)
			{
				$value = '';
				$items = JRequest::getVar("field_$row->id");
				for ($i = 0; $i < count($items); $i++)
				{
					$query  = "select title from `#__extrareg_list_value` where list_id = $row->list_id and id = $items[$i] order by id";
					$result = $db->setQuery($query);
					$v      = $db->loadResult();
					if ($value != '')
					{
						$value .= ',';
					}
					$value .= $v;
				}
				SetUserValue($row->id, $uid, $value);
			}
			if ($row->field_type == 13)
			{
				$value = '';
				$items = JRequest::getVar("field_$row->id");
				for ($i = 0; $i < count($items); $i++)
				{
					$query  = "select title from `#__extrareg_list_value` where list_id = $row->list_id and id = $items[$i] order by id";
					$result = $db->setQuery($query);
					$v      = $db->loadResult();
					if ($value != '')
					{
						$value .= ',';
					}
					$value .= $v;
				}
				SetUserValue($row->id, $uid, $value);
			}
		}
	}
}

function DeleteUser($id)
{
	$db     = JFactory::getDBO();
	$query  = "delete from `#__users` where `id` = $id";
	$result = $db->setQuery($query);
	$result = $db->query();
}

function DeleteField($id)
{
	$db     = JFactory::getDBO();
	$query  = "delete from `#__extrareg_fields_values` where `field_id` = $id";
	$result = $db->setQuery($query);
	$result = $db->query();
	$query  = "delete from `#__extrareg_fields` where `id` = $id";
	$result = $db->setQuery($query);
	$result = $db->query();
}

function ShowLists()
{
	$app            = JFactory::getApplication();
	$admin_template = $app->getTemplate();

	JToolBarHelper::custom('newlist', 'new', 'new', JText::_('JACTION_CREATE'), false);

	$db     = JFactory::getDBO();
	$query  = 'select * from `#__extrareg_list` order by id';
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();

	echo '<table width="100%" class="table table-striped adminlist" cellspacing="1">';
	echo "<thead><tr>";
	echo "  <th class='title' width='1%'>Id</th>";
	echo "  <th class='title'>" . JText::_('JGLOBAL_TITLE') . "</th>";
	echo "  <th class='title'>" . JText::_('COM_EXTRAREGISTRATION_LIST_ITEMS') . "</th>";
	echo "  <th class='title' width='1%'>" . JText::_('JACTION_DELETE') . "</th>";
	echo "</tr></thead>";
	if (count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			echo '<tr>';
			echo "<td>$row->id</td>";
			if (JVERSION >= 3.0)
			{
				echo "<td><a href='index.php?option=com_extraregistration&task=showlist&id=$row->id'>$row->title</a> <a href='index.php?option=com_extraregistration&task=editlist&id=$row->id'> <i class='icon-edit'></i></a></td>";
			}
			else
			{
				echo "<td><a href='index.php?option=com_extraregistration&task=showlist&id=$row->id'>$row->title</a> <a href='index.php?option=com_extraregistration&task=editlist&id=$row->id'> <img src='/administrator/templates/$admin_template/images/menu/icon-16-edit.png' border='0'></a></td>";
			}
			echo "<td>" . GetItemsList($row->id) . "</td>";
			echo "<td><a href='index.php?option=com_extraregistration&task=deletelist&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('JACTION_DELETE') . "' title='" . JText::_('JACTION_DELETE') . "' src='/administrator/templates/$admin_template/images/admin/publish_x.png'></a></td>";
			echo '</tr>';
		}
	}
	echo '</table>';
}

function DeleteList($id)
{
	$db     = JFactory::getDBO();
	$query  = "delete from `#__extrareg_list_value` where `list_id` = $id";
	$result = $db->setQuery($query);
	$result = $db->query();
	$query  = "delete from `#__extrareg_list` where `id` = $id";
	$result = $db->setQuery($query);
	$result = $db->query();
}

function EditList($id = '')
{
	JToolBarHelper::custom('savelist', 'save', 'save', JText::_('JSAVE'), false);
	JToolBarHelper::custom('cancellist', 'cancel', 'cancel', JText::_('JCANCEL'), false);
	$db = JFactory::getDBO();
	if ($id != '')
	{
		$query  = 'select * from `#__extrareg_list` where id = ' . $id;
		$result = $db->setQuery($query);
		$row    = $db->loadAssoc();
		$title  = $row['title'];
		echo '<input type="hidden" name="id" value="' . $id . '" />';
	}
	else
	{
		$title = '';
	}

	echo '<table border="0">';
	echo "<tr><td>" . JText::_('JGLOBAL_TITLE') . "</td><td><input type='text' name='title' value='$title' size='100'></td></tr>";
	echo '</table>';
}

function SaveList($id = '')
{
	$db    = JFactory::getDBO();
	$title = JRequest::getVar('title');
	if ($id != '')
	{
		$query = "update `#__extrareg_list` set title = '$title' where id = $id";
	}
	else
	{
		$query = "insert into `#__extrareg_list` (title) values ('$title')";
	}
	$result = $db->setQuery($query);
	$result = $db->query();
}

function GetItemsList($id)
{
	$db     = JFactory::getDBO();
	$query  = "select * from `#__extrareg_list_value` where list_id = $id order by id";
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();
	$out    = "";
	if (count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			$out .= $row->title . '<br>';
		}
	}

	return $out;
}

function ShowList($id)
{
	$app            = JFactory::getApplication();
	$admin_template = $app->getTemplate();

	JToolBarHelper::custom('newitem', 'new', 'new', JText::_('JACTION_CREATE'), false);

	$db     = JFactory::getDBO();
	$query  = "select * from `#__extrareg_list_value` where list_id = $id order by id";
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();
	echo "<input type='hidden' name='list_id' value='$id'>";
	echo '<table width="100%" class="table table-striped adminlist" cellspacing="1">';
	echo "<thead><tr>";
	echo "  <th class='title' width='1%'>Id</th>";
	echo "  <th class='title'>" . JText::_('JGLOBAL_TITLE') . "</th>";
	echo "  <th class='title' width='1%'>" . JText::_('JACTION_DELETE') . "</th>";
	echo "</tr></thead>";
	if (count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			echo '<tr>';
			echo "<td>$row->id</td>";
			echo "<td><a href='index.php?option=com_extraregistration&task=edititem&id=$row->id&list_id=$id'>$row->title</a></td>";
			echo "<td><a href='index.php?option=com_extraregistration&task=deleteitem&id=$row->id&list_id=$id'><img width='16' height='16' border='0' alt='" . JText::_('JACTION_DELETE') . "' title='" . JText::_('JACTION_DELETE') . "' src='/administrator/templates/$admin_template/images/admin/publish_x.png'></a></td>";
			echo '</tr>';
		}
	}
	echo '</table>';
}

function DeleteItem($id)
{
	$db     = JFactory::getDBO();
	$query  = "delete from `#__extrareg_list_value` where `id` = $id";
	$result = $db->setQuery($query);
	$result = $db->query();
}

function EditItem($id = '', $list_id)
{
	JToolBarHelper::custom('saveitem', 'save', 'save', JText::_('JSAVE'), false);
	JToolBarHelper::custom('cancelitem', 'cancel', 'cancel', JText::_('JCANCEL'), false);
	$db = JFactory::getDBO();
	if ($id != '')
	{
		$query  = 'select * from `#__extrareg_list_value` where id = ' . $id;
		$result = $db->setQuery($query);
		$row    = $db->loadAssoc();
		$title  = $row['title'];
		echo '<input type="hidden" name="id" value="' . $id . '" />';
	}
	else
	{
		$title = '';
	}
	echo "<input type='hidden' name='list_id' value='$list_id'>";
	echo '<table border="0">';
	echo "<tr><td>" . JText::_('JGLOBAL_TITLE') . "</td><td><input type='text' name='title' value='$title' size='100'></td></tr>";
	echo '</table>';
}

function SaveItem($id = '', $list_id)
{
	$db    = JFactory::getDBO();
	$title = JRequest::getVar('title');
	if ($id != '')
	{
		$query = "update `#__extrareg_list_value` set title = '$title' where id = $id";
	}
	else
	{
		$query = "insert into `#__extrareg_list_value` (title, list_id) values ('$title', $list_id)";
	}
	$result = $db->setQuery($query);
	$result = $db->query();
}

function NewPass($id)
{
	jimport('joomla.mail.helper');
	jimport('joomla.user.helper');

	$uid    = JRequest::getInt('uid');
	$pass   = JUserHelper::genRandomPassword();
	$config = JFactory::getConfig();

	$db          = JFactory::getDBO();
	$query       = "select email from `#__users` where id = $uid";
	$result      = $db->setQuery($query);
	$recipient[] = $db->loadResult();

	$query  = "select title from `#__extrareg_fields` where id = $id";
	$result = $db->setQuery($query);
	$title  = $db->loadResult();
	$query  = "update `#__extrareg_fields_values` set `value` = '" . md5($pass) . "' where field_id = $id and uid = $uid";
	$result = $db->setQuery($query);
	$result = $db->query();

	$subject = JText::_('COM_EXTRAREGISTRATION_PASS_RECOVER');
	$body    = JText::sprintf('COM_EXTRAREGISTRATION_PASS_MES', $title, $pass);
	JFactory::getMailer()->sendMail($config->get('mailfrom'), $config->get('fromname'), $recipient, $subject, $body, 1);

}

function SortFields()
{
	$db     = JFactory::getDBO();
	$query  = "select * from `#__extrareg_fields` order by order_id";
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();
	$n      = 1;
	foreach ($rows as $row)
	{
		$query = "update `#__extrareg_fields` set order_id = $n where id = $row->id";
		$n++;
		$result = $db->setQuery($query);
		$row    = $db->query();
	}
}

function OrderUp($id)
{
	SortFields();
	$db         = JFactory::getDBO();
	$query      = "select order_id from `#__extrareg_fields` where id = $id";
	$result     = $db->setQuery($query);
	$taborder   = $db->loadResult();
	$tabordern1 = $taborder - 1;

	$query  = "update `#__extrareg_fields` set order_id = $taborder where order_id = $tabordern1";
	$result = $db->setQuery($query);
	$row    = $db->query();

	$query  = "update `#__extrareg_fields` set order_id = $tabordern1 where id = $id";
	$result = $db->setQuery($query);
	$row    = $db->query();
	FieldList();
}

function OrderDown($id)
{

	SortFields();
	$db       = JFactory::getDBO();
	$query    = "select order_id from `#__extrareg_fields` where id = $id";
	$result   = $db->setQuery($query);
	$taborder = $db->loadResult();

	$tabordern1 = $taborder + 1;

	$query  = "update `#__extrareg_fields` set order_id = $taborder where order_id = $tabordern1";
	$result = $db->setQuery($query);
	$row    = $db->query();

	$query  = "update `#__extrareg_fields` set order_id = $tabordern1 where id = $id";
	$result = $db->setQuery($query);
	$row    = $db->query();
	FieldList();
}

function GetMailList($id = '')
{
	$db     = JFactory::getDBO();
	$query  = 'select * from `#__extraregistration_mail` where published = 1 order by title';
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();
	$out    = '';
	$out .= '<select name="mail_id">';
	if (count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			if ($row->id == $id)
			{
				$s = 'selected';
			}
			else
			{
				$s = '';
			}
			$out .= "<option value='$row->id' $s>$row->title</option>";
		}
	}
	$out .= '</select>';

	return $out;
}

function GetMailTitleById($id)
{
	$db     = JFactory::getDBO();
	$query  = "select title from `#__extraregistration_mail` where id = $id";
	$result = $db->setQuery($query);

	return $db->loadResult();
}

function GetMailById($id)
{
	$db     = JFactory::getDBO();
	$query  = "select * from `#__extraregistration_mail` where id = $id";
	$result = $db->setQuery($query);

	return $db->loadAssoc();
}

function GetActionList($id = '')
{
	if (file_exists(JPATH_ROOT . '/components/com_djclassifieds/djclassifieds.php'))
	{
		$db     = JFactory::getDBO();
		$query  = 'select * from `#__djcf_items` where published = 1 order by `name`';
		$result = $db->setQuery($query);
		$rows   = $db->loadObjectList();
		$out    = '';
		$out .= '<select name="action_id">';
		if (count($rows) > 0)
		{
			foreach ($rows as $row)
			{
				if ($row->id == $id)
				{
					$s = 'selected';
				}
				else
				{
					$s = '';
				}
				$out .= "<option value='$row->id' $s>$row->name</option>";
			}
		}
		$out .= '</select>';

		return $out;
	}
	else
	{
		return '';
	}
}

function GetCategoriesList($id = '')
{
	if (file_exists(JPATH_ROOT . '/components/com_djclassifieds/djclassifieds.php'))
	{
		$db     = JFactory::getDBO();
		$query  = 'select * from `#__djcf_categories` where published = 1 order by `name`';
		$result = $db->setQuery($query);
		$rows   = $db->loadObjectList();
		$out    = '';
		$out .= '<select name="action_id">';
		if (count($rows) > 0)
		{
			foreach ($rows as $row)
			{
				if ($row->id == $id)
				{
					$s = 'selected';
				}
				else
				{
					$s = '';
				}
				$out .= "<option value='$row->id' $s>$row->name</option>";
			}
		}
		$out .= '</select>';

		return $out;
	}
	else
	{
		return '';
	}
}

function MailList($order = '', $limitstart = '', $limit = '')
{
	JToolBarHelper::title(JText::_('COM_EXTRAREGISTRATION_Mail'), 'generic.png');
	JToolBarHelper::custom('newmail', 'new', 'new', JText::_('JTOOLBAR_NEW'), false);

	$app            = JFactory::getApplication();
	$admin_template = $app->getTemplate();

	if ($limitstart == '')
	{
		$limitstart = 0;
	}
	if ($limit == '')
	{
		$limit = 20;
	}
	if ($limitstart == 0 and $limit == 0)
	{
		$lim = '';
	}
	else
	{
		$lim = "limit $limitstart, $limit";
	}

	if ($order == '')
	{
		$order = 'title';
	}
	$out = '';
	$db  = JFactory::getDBO();

	$query  = 'select count(*) from `#__extraregistration_mail`';
	$result = $db->setQuery($query);
	$total  = $db->loadResult();

	$query = "select * from `#__extraregistration_mail` order by " . $order;
	$query = $query . " $lim";
	$db->setQuery($query);
	$rows = $db->loadObjectList();
	if (count($rows) > 0)
	{
		$out .= "<table width='100%' class='adminlist table' cellspacing='1'>";
		$out .= "<thead><tr>";
		$out .= "<th width='1px'>Id</td>";
		$out .= "<th>" . JText::_('COM_EXTRAREGISTRATION_TITLE') . "</th>";
		$out .= "<th>" . JText::_('COM_EXTRAREGISTRATION_MAIL') . "</th>";
		$out .= "<th width='1px'>" . JText::_('COM_EXTRAREGISTRATION_PUBLISHED') . "</th>";
		$out .= "<th width='1px'>" . JText::_('JTOOLBAR_REMOVE') . "</th>";
		$out .= "</tr></thead>";
		foreach ($rows as $row)
		{
			$out .= "<tr>";
			$out .= "<td>$row->id</td>";
			$out .= "<td><a href='index.php?option=com_extraregistration&view=extraregistration&task=editmail&id=$row->id'>$row->title</a></td>";
			$out .= "<td><a href='index.php?option=com_extraregistration&view=extraregistration&task=showmail&id=$row->id'>" . JText::_('COM_EXTRAREGISTRATION_MAIL') . "</a></td>";
			if ($row->published == 0)
			{
				$out .= "<td><a href='index.php?option=com_extraregistration&task=activemail&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('COM_EXTRAREGISTRATION_PUBLISHED') . "' title='" . JText::_('COM_EXTRAREGISTRATION_PUBLISHED') . "' src='/administrator/templates/$admin_template/images/admin/publish_x.png'></a></td>";
			}
			else
			{
				$out .= "<td><a href='index.php?option=com_extraregistration&task=activemail&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('COM_EXTRAREGISTRATION_NOT_PUBLISHED') . "' title='" . JText::_('COM_EXTRAREGISTRATION_NOT_PUBLISHED') . "' src='/administrator/templates/$admin_template/images/admin/tick.png'></a></td>";
			}
			$out .= "<td><a href='index.php?option=com_extraregistration&view=extraregistration&task=delmail&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('JTOOLBAR_REMOVE') . "' title='" . JText::_('JTOOLBAR_REMOVE') . "' src='/administrator/templates/$admin_template/images/admin/publish_r.png'></a></td>";
			$out .= "</tr>";
		}
		$out .= "</table>";
	}

	jimport('joomla.html.pagination');
	$pagenav = new JPagination ($total, $limitstart, $limit);
	$out .= $pagenav->getListFooter();

	echo $out;
}

function GetFilterValue($field_id, $cid, $no = 1)
{
	if ($cid == '')
	{
		return '';
	}
	$db = JFactory::getDBO();
	if ($no == 1)
	{
		$query = "select from_value from `#__extraregistration_mail_criteria_field` where field_id = $field_id and criteria_id = $cid";
	}
	else
	{
		$query = "select to_value from `#__extraregistration_mail_criteria_field` where field_id = $field_id and criteria_id = $cid";
	}
	$result = $db->setQuery($query);

	return $db->loadResult();
}

function GetChecked($field_id, $cid, $mode = 0)
{
	if ($cid == '')
	{
		if ($mode == 1)
		{
			return 'false';
		}

		return '';
	}
	$db     = JFactory::getDBO();
	$query  = "select count(id) from `#__extraregistration_mail_criteria_field` where field_id = $field_id and criteria_id = $cid";
	$result = $db->setQuery($query);
	$n      = $db->loadResult();
	if ($n > 0)
	{
		if ($mode == 1)
		{
			return 'true';
		}

		return 'checked';
	}
	else
	{
		if ($mode == 1)
		{
			return 'false';
		}

		return '';
	}
}

function GetUserList2($uid = '', $fieldname = 'uid')
{
	$db     = JFactory::getDBO();
	$query  = 'select * from `#__users` order by name';
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();
	$out    = '';
	$out .= "<select name='$fieldname' id='$fieldname'>";
	if (count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			if ($row->id == $uid)
			{
				$s = 'selected';
			}
			else
			{
				$s = '';
			}
			$out .= "<option value='$row->id' $s>$row->name</option>";
		}
	}
	$out .= '</select>';

	return $out;
}

function plural_type($n)
{
	return ($n % 10 == 1 && $n % 100 != 11 ? 0 : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? 1 : 2));
}

function EditMail($id = '')
{
	JToolBarHelper::custom('savemail', 'save', 'save', JText::_('JTOOLBAR_APPLY'), false);
	JToolBarHelper::custom('cancelmail', 'cancel', 'cancel', JText::_('JTOOLBAR_CANCEL'), false);
	$editor = JFactory::getEditor();

	$db = JFactory::getDBO();
	if ($id != '' and $id != 0)
	{
		echo "<input type='hidden' name='id' value='$id'>";
		$query             = "select * from `#__extraregistration_mail` where id = $id";
		$result            = $db->setQuery($query);
		$row               = $db->loadAssoc();
		$title             = $row['title'];
		$comment           = $row['comment'];
		$username_field_id = $row['username_field_id'];
		$start_date        = $row['start_date'];
		$send_pause        = $row['send_pause'];
		$criteria_id       = $row['criteria_id'];
		$action_id         = $row['action_id'];
		$item_id           = $row['item_id'];

		if ($criteria_id != 0 and $criteria_id != '')
		{
			$query    = "select * from `#__extraregistration_mail_criteria_field` where id = $criteria_id";
			$result   = $db->setQuery($query);
			$criteria = $db->loadObject();
		}
	}
	else
	{
		$title             = '';
		$comment           = '<p>' . JText::_('COM_EXTRAREGISTRATION_HELLO') . ', %username%!</p>';
		$username_field_id = '';
		$start_date        = '';
		$send_pause        = 10;
		$criteria_id       = 0;
		$action_id         = '';
		$item_id           = 0;
	}

	$val = $criteria_id;
	echo "<div class='filter_div'>";
	$r      = 'filter';
	$r1     = '';
	$query  = "select * from `#__extrareg_fields` order by order_id";
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();
	$ss     = 0;
	foreach ($rows as $row)
	{
		if ($ss % 2 == 0)
		{
			$open = true;
			echo "<div class='filter_row'>";
		}
		else
		{
			$open = false;
		}

		//JRequest::getVar('filter_'.$row->id);
		echo "<div class='filter_item'>";
		echo "<div class='filter_label'> <input type='checkbox' name='cb_$row->id' " . GetChecked($row->id, $val) . " onclick='this.value=this.checked;' value='" . GetChecked($row->id, $val, 1) . "'> " . $row->title . "</div> <div class='filter_control'>";
		switch ($row->field_type)
		{
			case 1:
				echo "<input type='text' name='filter_$row->id' id='filter_$row->id' class='$r' $r1 value='" . GetFilterValue($row->id, $val) . "' size='5' style='margin-left: 17px;'> ";
				break;
			case 2:
				echo JText::_('COM_EXTRAREGISTRATION_FROM') . " <input type='text' name='filter_$row->id" . "_1' id='filter_$row->id" . "_1' class='$r' $r1 value='" . GetFilterValue($row->id, $val, 1) . "' onblur='ValidateNumber(this);' size='5' style='width: 100px;'> " .
					JText::_('COM_EXTRAREGISTRATION_TO') .
					" <input type='text' name='filter_$row->id" . "_2' id='filter_$row->id" . "_2' class='$r' $r1 value='" . GetFilterValue($row->id, $val, 2) . "' onblur='ValidateNumber(this);' size='5' style='width: 100px;'> ";
				break;
			case 3:
				//echo "<a href='".GetFilterValue($row->id, $val)."' class='modal'><img src='".GetFilterValue($row->id, $val)."' height='80px'></a> <br><input type='file' name='filter_$row->id' id='filter_$row->id' class='$r' $r1 value=''> ";
				break;
			case 4:
				echo JText::_('COM_EXTRAREGISTRATION_FROM') . ' ' . JHTML::_('calendar', GetFilterValue($row->id, $val, 1), "filter_$row->id" . "_1", "filter_$row->id" . "_1", '%Y-%m-%d', array('class' => "inputbox $r", 'size' => '20', 'maxlength' => '19', 'style' => 'width: 100px;')) . ' ' .
					JText::_('COM_EXTRAREGISTRATION_TO') . ' ' .
					JHTML::_('calendar', GetFilterValue($row->id, $val, 2), "filter_$row->id" . "_2", "filter_$row->id" . "_2", '%Y-%m-%d', array('class' => "inputbox $r", 'size' => '20', 'maxlength' => '19', 'style' => 'width: 100px;')) . ' ';
				break;
			case 6:
				echo JText::_('COM_EXTRAREGISTRATION_FROM') . " <input type='text' name='filter_$row->id" . "_1' id='filter_$row->id" . "_1' class='$r' $r1 onblur='ValidateNumber(this);' value='" . GetFilterValue($row->id, $val, 1) . "' size='5' style='width: 100px;'> " .
					JText::_('COM_EXTRAREGISTRATION_TO') .
					" <input type='text' name='filter_$row->id" . "_2' id='filter_$row->id" . "_2' class='$r' $r1 onblur='ValidateNumber(this);' value='" . GetFilterValue($row->id, $val, 2) . "' size='5' style='width: 100px;'> " . GetOption('abbr') . ' ';
				break;
			case 7:
				echo "<select name='filter_$row->id' id='filter_$row->id' class='$r' $r1 style='margin-left: 17px;'>";

				$query  = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
				$result = $db->setQuery($query);
				$items  = $db->loadObjectList();
				if (count($items) > 0)
				{
					foreach ($items as $item)
					{
						if ($val == $item->id and $val != -1)
						{
							$s = 'selected';
						}
						else
						{
							$s = '';
						}
						echo "<option value='$item->id' $s>$item->title</option>";
					}
				}
				echo "</select> ";
				break;
			case 8:
				//echo "<a href='index.php?option=com_extraregistration&view=registrationform&task=newpass&id=$row->id&uid=$uid'>".JText::_('COM_EXTRAREGISTRATION_NEW_PASS')."</a> <br>";
				break;
			case 9:
				echo GetUserList2(GetFilterValue($row->id, $val), "filter_$row->id") . ' ';
				break;
			case 12:
				echo "<br><select multiple='multiple' name='filter_$row->id" . '[]' . "' id='filter_$row->id' style='margin-left: 17px;'>";
				$query  = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
				$result = $db->setQuery($query);
				$items  = $db->loadObjectList();
				if (count($items) > 0)
				{
					foreach ($items as $item)
					{
						$s = '';
						if (count($val) != 0)
						{
							for ($i = 0; $i < count($val); $i++)
							{
								if ($val[$i] == $item->id)
								{
									$s = 'selected';
								}
							}
						}
						echo "<option value='$item->id' $s>$item->title</option>";
					}
				}
				echo "</select> ";
				break;
			case 13:

				echo "<br>";
				$query  = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
				$result = $db->setQuery($query);
				$items  = $db->loadObjectList();
				if (count($items) > 0)
				{
					foreach ($items as $item)
					{
						$s = '';
						if (count($val) > 0)
						{
							foreach ($val as $value)
							{
								if ($value == $item->id)
								{
									$s = 'checked';
								}
							}
						}
						echo "<input type='checkbox' value='$item->id' name='filter_$row->id" . '[]' . "' $s style='margin-left: 17px;'> $item->title <br>";
					}
				}
				echo " ";
				break;
			case 14:
				echo "<textarea name='filter_$row->id' id='filter_$row->id' class='$r' $r1 style='margin-left: 17px;'>" . GetFilterValue($row->id, $val) . "</textarea> <br>";
				break;
			default:
				echo "<input type='text' name='filter_$row->id' id='filter_$row->id' class='$r' $r1 value='" . GetFilterValue($row->id, $val) . "' style='margin-left: 17px;'> ";
				break;
		}
		echo "</div></div>";

		if ($ss % 2 == 1)
		{
			echo "</div>";
		}

		$ss = $ss + 1;
	}

	if ($open)
	{
		echo "</div>";
	}

	//echo "<br><strong>". JText::_('COM_EXTRAREGISTRATION_SORT_BY') . "</strong> " . GetFieldsList(JRequest::getInt('field_id'));	
	//echo "<p align='right'> <input type='submit' value='".JText::_('COM_EXTRAREGISTRATION_APPLY_FILTER')."' class='find_btn'></p>";	//".."
	echo "</div> <br>";

	echo '<table border="0">';
	echo "<tr><td>" . JText::_('COM_EXTRAREGISTRATION_SUBJ') . "</td><td><input type='text' name='title' value='$title' size='100'></td></tr>";
	echo "<tr><td>" . JText::_('COM_EXTRAREGISTRATION_USERNAME_FIELD') . "</td><td>" . GetFieldsList($username_field_id, 'username_field_id') . "</td></tr>";
	if (file_exists(JPATH_ROOT . '/components/com_djclassifieds/djclassifieds.php'))
	{
		echo "<tr><td>" . JText::_('COM_EXTRAREGISTRATION_ACTION') . "</td><td>" . GetCategoriesList($action_id) . "</td></tr>";
	}
	//echo "<tr><td>".JText::_('COM_EXTRAREGISTRATION_START_TIME')."</td><td>".JHTML::_('calendar', $start_date, "start_date", "start_date", '%Y-%m-%d', array('class'=>"inputbox $r", 'size'=>'20',  'maxlength'=>'19'))."</td></tr>";
	//echo "<tr><td>".JText::_('COM_EXTRAREGISTRATION_INTERVAL')."</td><td><input type='text' name='send_pause' value='$send_pause' size='3'></td></tr>";
	echo "<tr><td>" . JText::_('COM_EXTRAREGISTRATION_MAILTEXT') . "</td><td>" . $editor->display('comment', $comment, '600', '300', '20', '20', false) . "</td></tr>";
	echo '</table>';
}

function SaveMail($id = '')
{
	$input             = JFactory::getApplication()->input;
	$title             = $input->getHtml('title');
	$comment           = $input->getHtml('comment');
	$username_field_id = $input->getVar('username_field_id');
	//$start_date = '';
	//$send_pause = 10;
	$criteria_id = $input->getVar('criteria_id', 0, 'INT');
	$action_id   = $input->getVar('action_id', 0, 'INT');
	$item_id     = $input->getVar('item_id', 0, 'INT');

	$db = JFactory::getDBO();
	if ($action_id != 0)
	{
		$comment = '<p>' . JText::_('COM_EXTRAREGISTRATION_HELLO') . ', %username%!</p>';
		$query   = "select * from `#__djcf_items` where id = $action_id";
		$result  = $db->setQuery($query);
		$action  = $db->loadObject();
		$comment .= $action->description;
	}

	$date = date('Y-m-d H:i:s');
	if ($id == '' or $id == 0)
	{
		$query       = "insert into `#__extraregistration_mail_criteria` (`title`, `comment`) values ('criteria', '')";
		$result      = $db->setQuery($query);
		$result      = $db->execute();
		$criteria_id = $db->insertid();

		$query  = "insert into `#__extraregistration_mail`
			(
				`title`,
				`comment`,
				`criteria_id`,
				`username_field_id`,
				`action_id`,
				`item_id`
			)
			values
			(
				'$title',
				'$comment',
				$criteria_id,
				$username_field_id,
				$action_id,
				$item_id
			)";
		$result = $db->setQuery($query);
		$result = $db->execute();
		$id     = $db->insertid();
	}
	else
	{
		$query  = "update `#__extraregistration_mail` set
			`title` = '$title',
			`comment` = '$comment',
			`criteria_id` = $criteria_id,
			`username_field_id` = $username_field_id,
			`action_id` = $action_id,
			`item_id` = $item_id
		where id = $id";
		$result = $db->setQuery($query);
		$result = $db->execute();
	}

	$query  = "select * from `#__extrareg_fields` order by order_id";
	$result = $db->setQuery($query);
	$rows   = $db->loadObjectList();
	if (count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			$sql     = '';
			$checked = JRequest::getVar("cb_$row->id");
			$val     = JRequest::getVar("filter_$row->id");
			$val1    = JRequest::getVar("filter_$row->id" . '_1');
			$val2    = JRequest::getVar("filter_$row->id" . '_2');
			if ($val1 == '' and $val != '')
			{
				$val1 = $val;
			}
			$exists = GetChecked($row->id, $criteria_id) == 'checked';
			if ($exists and !$checked)
			{
				$sql = "delete from `#__extraregistration_mail_criteria_field` where field_id = $row->id and criteria_id = $criteria_id";
			}
			if ($exists and $checked)
			{
				$sql = "update `#__extraregistration_mail_criteria_field` set from_value = '$val1', to_value = '$val2' where field_id = $row->id and criteria_id = $criteria_id";
			}
			if (!$exists and $checked)
			{
				$sql = "insert into `#__extraregistration_mail_criteria_field` (criteria_id, field_id, from_value, to_value) values ($criteria_id, $row->id, '$val1', '$val2')";
			}
			if ($sql != '')
			{
				$result = $db->setQuery($sql);
				$result = $db->execute();
			}
		}
	}

	$query = "select u.*
		from `#__users` u ";

	$and = 'where ';

	if (count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			$checked = JRequest::getVar("cb_$row->id");
			$val     = JRequest::getVar("filter_$row->id");
			$val1    = JRequest::getVar("filter_$row->id" . '_1');
			$val2    = JRequest::getVar("filter_$row->id" . '_2');
			if ($val1 == '' and $val != '')
			{
				$val1 = $val;
			}
			if ($checked and $val1 != '')
			{
				$sql = '';
				switch ($row->field_type)
				{
					case 1:
						$sql = $and . "id in (select uid from `#__extrareg_fields_values` where field_id = $row->id and `value` LIKE '%$val1%')";
						$and = ' and ';
						break;
					case 2:
						$sql = $and . "id in (select uid from `#__extrareg_fields_values` where field_id = $row->id and `value` between $val1 and $val2)";
						$and = ' and ';
						break;
					case 4:
						$sql = $and . "id in (select uid from `#__extrareg_fields_values` where field_id = $row->id and `value` between '$val1' and '$val2')";
						$and = ' and ';
						break;
					case 6:
						$sql = $and . "id in (select uid from `#__extrareg_fields_values` where field_id = $row->id and `value` between $val1 and $val2)";
						$and = ' and ';
						break;
					case 7:
						$sql = $and . "id in (select uid from `#__extrareg_fields_values` where field_id = $row->id and `value` = '$val1')";
						$and = ' and ';
						break;
					case 8:
						$sql = $and . "id in (select uid from `#__extrareg_fields_values` where field_id = $row->id and `value` = '$val1')";
						$and = ' and ';
						break;
					case 9:
						$sql = $and . "id in (select uid from `#__extrareg_fields_values` where field_id = $row->id and `value` = '$val1')";
						$and = ' and ';
						break;
					case 12:
						$sql = $and . "id in (select uid from `#__extrareg_fields_values` where field_id = $row->id and `value` = '$val1')";
						$and = ' and ';
						break;
					case 13:
						$sql = $and . "id in (select uid from `#__extrareg_fields_values` where field_id = $row->id and `value` = '$val1')";
						$and = ' and ';
						break;
					case 14:
						$sql = $and . "id in (select uid from `#__extrareg_fields_values` where field_id = $row->id and `value` = '$val1')";
						$and = ' and ';
						break;

				}
				if ($sql != '')
				{
					$query .= $sql;
				}
			}
		}
	}
	//echo $query;
	$result = $db->setQuery($query);
	$users  = $db->loadObjectList();
	if (count($users) > 0)
	{
		foreach ($users as $user)
		{
			$query  = "select count(*) from `#__extraregistration_mail_history` where uid = $user->id and mail_id = $id";
			$result = $db->setQuery($query);
			if ($db->loadResult() == 0)
			{
				$query  = "insert into `#__extraregistration_mail_history` (uid, mail_id, email, username) values ($user->id, $id, '$user->email', '" . GetFieldValue($username_field_id, $user->id) . "')";
				$result = $db->setQuery($query);
				$result = $db->execute();
			}
		}
	}
}

function DelMail($id)
{
	$db     = JFactory::getDBO();
	$result = 0;

	if ($result == 0)
	{
		$query  = "delete from `#__extraregistration_mail` where id = $id";
		$result = $db->setQuery($query);
		$result = $db->execute();
	}
	else
	{
		SystemMessage(JText::_('COM_EXTRAREGISTRATION_CANTDEL'));
	}
}

function ActivateMail($id)
{
	$db = JFactory::getDBO();

	$query  = 'select published from `#__extraregistration_mail` where id = ' . $id;
	$result = $db->setQuery($query);
	$n      = $db->loadResult();

	if ($n == 1)
	{
		$n = 0;
	}
	else
	{
		$n = 1;
	}
	$query  = "update `#__extraregistration_mail` set published = $n where id = $id";
	$result = $db->setQuery($query);
	$result = $db->execute();
}

function ShowMail($id)
{
	$app            = JFactory::getApplication();
	$admin_template = $app->getTemplate();
	$out            = '';
	$db             = JFactory::getDBO();

	$query = "select * from `#__extraregistration_mail_history` where mail_id = $id order by id";
	$db->setQuery($query);
	$rows = $db->loadObjectList();
	$out .= "<table width='100%' class='adminlist table' cellspacing='1'>";
	$out .= "<thead><tr>";
	$out .= "<th width='1px'>Id</td>";
	$out .= "<th>" . JText::_('COM_EXTRAREGISTRATION_EMAIL') . "</th>";
	$out .= "<th>" . JText::_('COM_EXTRAREGISTRATION_STATUS') . "</th>";
	$out .= "<th width='1px'>" . JText::_('JTOOLBAR_REMOVE') . "</th>";
	$out .= "</tr></thead>";
	if (count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			$out .= "<tr>";
			$out .= "<td>$row->id</td>";
			$out .= "<td>$row->email</td>";
			if ($row->sent_result == 0)
			{
				$out .= "<td>" . JText::_('COM_EXTRAREGISTRATION_WAITING') . "</td>";
				$out .= "<td><a href='index.php?option=com_extraregistration&view=extraregistration&task=delusermail&id=$row->id'><img width='16' height='16' border='0' alt='" . JText::_('JTOOLBAR_REMOVE') . "' title='" . JText::_('JTOOLBAR_REMOVE') . "' src='/administrator/templates/$admin_template/images/admin/publish_r.png'></a></td>";
			}
			else
			{
				//$out .= "<td>".JText::_( 'COM_EXTRAREGISTRATION_SENT' )." $row->sent_date</td>";
				$out = '<td></td>';
				$out = '<td></td>';
			}
			$out .= "</tr>";
		}
		$out .= "</table>";
	}
	else
	{
		$out = "<p align='center'>" . JText::_('COM_EXTRAREGISTRATION_EMPTY') . "</p>";
	}
	echo $out;
}

function DelUserMail($id)
{
	$db = JFactory::getDBO();

	$query = "select mail_id from `#__extraregistration_mail_history` where id = $id order by id";
	$db->setQuery($query);
	$mail_id = $db->loadResult();

	$query = "delete from `#__extraregistration_mail_history` where id = $id and sent_result = 0";
	$db->setQuery($query);
	$db->execute();

	ShowMail($mail_id);
}

JToolBarHelper::title(JText::_('COM_EXTRAREGISTRATION'), 'generic.png');

$billing_enabled = false;
if (file_exists(JPATH_ROOT . '/components/com_billing/useraccount.php'))
{
	include_once(JPATH_ROOT . '/components/com_billing/useraccount.php');
	include_once(JPATH_ROOT . '/components/com_billing/account.php');
	$billing_enabled = true;
}
$sms_enabled = false;
if (file_exists(JPATH_ROOT . '/components/com_jsms/jsmsapi.php'))
{
	include_once(JPATH_ROOT . '/components/com_jsms/jsmsapi.php');
	$sms_enabled = true;
}

$view       = JRequest::getCmd('view');
$task       = JRequest::getCmd('task');
$uid        = JRequest::getInt('uid');
$id         = JRequest::getInt('id');
$limitstart = JRequest::getInt('limitstart');
$limit      = JRequest::getInt('limit');
$order      = JRequest::getCmd('order');
$list_id    = JRequest::getInt('list_id');

?>

<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">

	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2" style="height: 100%;">
		<?php
		echo $this->sidebar;
		?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>


			<?php

			switch ($task)
			{
				case 'add':
					EditField('', $billing_enabled, $sms_enabled);
					break;
				case 'edit':
					EditField($id, $billing_enabled, $sms_enabled);
					break;
				case 'save':
					SaveField($id);
					FieldList();
					break;
				case 'fields':
					FieldList();
					break;
				case 'cancel':
					FieldList();
					break;
				case 'active':
					ActivateField($id);
					FieldList();
					break;
				case 'publish':
					Publish();
					FieldList();
					break;
				case 'unpublish':
					UnPublish();
					FieldList();
					break;
				case 'required':
					ReqireField($id);
					FieldList();
					break;
				case 'reg':
					RegField($id);
					FieldList();
					break;
				case 'canedit':
					CanEditField($id);
					FieldList();
					break;
				case 'foradmin':
					ForAdmin($id);
					FieldList();
					break;
				case 'options':
					ShowOptions($billing_enabled, $sms_enabled);
					break;
				case 'saveoptions':
					SaveOptions($billing_enabled, $sms_enabled);
					ShowOptions($billing_enabled, $sms_enabled);
					break;
				case 'edituser':
					EditUser($id);
					break;
				case 'canceluser':
					UserList($order, $limitstart, $limit);
					break;
				case 'saveuser':
					SaveUser($id);
					UserList($order, $limitstart, $limit);
					break;
				case 'deleteuser':
					DeleteUser($id);
					UserList($order, $limitstart, $limit);
					break;
				case 'delete':
					DeleteField($id);
					FieldList();
					break;
				case 'lists':
					ShowLists();
					break;
				case 'deletelist':
					DeleteList($id);
					ShowLists();
					break;
				case 'newlist':
					EditList();
					break;
				case 'editlist':
					EditList($id);
					break;
				case 'cancellist':
					ShowLists();
					break;
				case 'savelist':
					SaveList($id);
					ShowLists();
					break;
				case 'showlist':
					ShowList($id);
					break;
				case 'deleteitem':
					DeleteItem($id);
					ShowList($list_id);
					break;
				case 'edititem':
					EditItem($id, $list_id);
					break;
				case 'newitem':
					EditItem('', $list_id);
					break;
				case 'saveitem':
					SaveItem($id, $list_id);
					ShowList($list_id);
					break;
				case 'cancelitem':
					ShowLists();
					break;
				case 'newpass':
					NewPass($id);
					break;
				case 'orderup':
					OrderUp($id);
					break;
				case 'orderdown':
					OrderDown($id);
					break;

				case 'mailmode':
					MailList($order, $limitstart, $limit);
					break;
				case 'newmail':
					EditMail();
					break;
				case 'editmail':
					EditMail($id);
					break;
				case 'delmail':
					DelMail($id);
					MailList($order, $limitstart, $limit);
					break;
				case 'activemail':
					ActivateMail($id);
					MailList($order, $limitstart, $limit);
					break;
				case 'savemail':
					SaveMail($id);
					MailList($order, $limitstart, $limit);
					break;
				case 'cancelmail':
					MailList($order, $limitstart, $limit);
					break;
				case 'showmail':
					ShowMail($id);
					break;
				case 'delusermail':
					DelUserMail($id);
					break;

				case 'filtermode':
					FilterList($order, $limitstart, $limit);
					break;
				case 'newfilter':
					EditFilter();
					break;
				case 'editfilter':
					EditFilter($id);
					break;
				case 'delfilter':
					DelFilter($id);
					FilterList($order, $limitstart, $limit);
					break;
				case 'activefilter':
					ActivateFilter($id);
					FilterList($order, $limitstart, $limit);
					break;
				case 'savefilter':
					SaveFilter($id);
					FilterList($order, $limitstart, $limit);
					break;
				case 'cancelfilter':
					FilterList($order, $limitstart, $limit);
					break;

				default:
					UserList($order, $limitstart, $limit);
					break;
			}
			?>

			<input type="hidden" name="option" value="com_extraregistration" />
			<input type="hidden" name="task" id="task" value="<?php echo $task; ?>" />
			<input type="hidden" name="boxchecked" value="0" />

		</div>

</form>


    

