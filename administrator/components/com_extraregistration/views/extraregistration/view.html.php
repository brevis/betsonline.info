<?php defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

if (JVERSION>=3.0){
    JHtml::_('formbehavior.chosen', 'select');
}

# For compatibility with older versions of Joola 2.5
if (!class_exists('JViewLegacy')){
    class JViewLegacy extends JView {

    }
}

class ExtraregistrationViewExtraregistration extends JViewLegacy
{
	function display($tpl = null)
	{

        
        if(JVERSION>=3.0) //Code support for joomla version greater than 3.0
        {
            $task = JFactory::getApplication()->input->get('task');  
            $empty_sider = 'add,edit,edituser,newlist,showlist,newitem,editlist';
            
//            if (empty($task) or strpos($empty_sider, $task)===false)
            {
                $this->addToolbar($task);
                $this->sidebar = JHtmlSidebar::render();      
            }
        }
        else
        {
            JSubMenuHelper::addEntry( JText::_( 'COM_EXTRAREGISTRATION_USERS' ), 'index.php?option= com_extraregistration', 'extra' );
            JSubMenuHelper::addEntry( JText::_( 'COM_EXTRAREGISTRATION_FIELDS' ), 'index.php?option=com_extraregistration&task=fields', 'fields' );
            JSubMenuHelper::addEntry( JText::_( 'COM_EXTRAREGISTRATION_LISTS' ), 'index.php?option=com_extraregistration&task=lists', 'lists' );
			//JSubMenuHelper::addEntry( JText::_( 'COM_EXTRAREGISTRATION_MAIL' ), 'index.php?option=com_extraregistration&task=mailmode', 'mailmode' );
			JSubMenuHelper::addEntry( JText::_( 'COM_EXTRAREGISTRATION_FILTER' ), 'index.php?option=com_extraregistration&view=filters', 'filtermode' );
            JSubMenuHelper::addEntry( JText::_( 'COM_EXTRAREGISTRATION_OPTIONS' ), 'index.php?option=com_extraregistration&task=options', 'options' );
        }
        
		parent::display($tpl);
	}

    
    protected function addToolbar($vName)
    {
        
        JHtmlSidebar::addEntry( JText::_( 'COM_EXTRAREGISTRATION_USERS' ), 'index.php?option=com_extraregistration', 
        in_array($vName, array('','edituser','deleteuser','saveuser','canceluser','userpub')));
        
        JHtmlSidebar::addEntry( JText::_( 'COM_EXTRAREGISTRATION_FIELDS' ), 'index.php?option=com_extraregistration&task=fields', 
        in_array($vName, array('add', 'edit','required', 'fields','save', 'cancel','delete','active','reg',
        'canedit', 'orderup', 'orderdown'))  );
        
        JHtmlSidebar::addEntry( JText::_( 'COM_EXTRAREGISTRATION_LISTS' ), 'index.php?option=com_extraregistration&task=lists', 
        in_array($vName, array('lists', 'newlist', 'newitem', 'edititem','deleteitem', 'showlist', 'editlist', 'savelist', 'saveitem','cancelitem','cancellist','deletelist')) );

        /*
		JHtmlSidebar::addEntry( JText::_( 'COM_EXTRAREGISTRATION_MAIL' ), 'index.php?option=com_extraregistration&task=mailmode', 
        in_array($vName, array('mailmode', 'newmail', 'editmail', 'delmail', 'activemail', 'savemail', 'cancelmail')) );
        */
		
		JHtmlSidebar::addEntry( JText::_( 'COM_EXTRAREGISTRATION_FILTER' ), 'index.php?option=com_extraregistration&task=filters',
        in_array($vName, array('filters', 'newfilter', 'editfilter', 'delfilter', 'activefilter', 'savefilter', 'cancelfilter')) );
        
        JHtmlSidebar::addEntry( JText::_( 'COM_EXTRAREGISTRATION_OPTIONS' ), 'index.php?option=com_extraregistration&task=options', 
        
        in_array($vName, array('options', 'saveoptions')) );        
        

        
        $this->sidebar = JHtmlSidebar::render();
    }    
    

}
?>


<script>
/*
jQuery(document).ready(function(){
jQuery("#submenu li").click(function () {
    jQuery(this).addClass('active');
//alert('click');        
}); 
   

});
*/
</script>