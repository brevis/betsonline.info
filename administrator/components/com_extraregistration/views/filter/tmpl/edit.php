<?php
defined('_JEXEC') or die;
error_reporting(E_ALL);
ini_set('display_errors', 'On');
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');

JToolBarHelper::title(JText::_('EXTRAREGISTRATION'), 'generic.png');
JHTML::_('behavior.calendar');
?>
<style>
	div.rule {
		border-bottom: 1px solid #eeeeee;
		padding: 10px 0 0 0;
		margin: 5px 0;
	}
	div.rule:after,
	div.rule .col {
		float: left;
		width: 220px;
	}
	div.rule .col.value {
		float: left;
		width: 240px;
	}
	div.rule .col input[type="text"],
	div.rule .col select {
		max-width: 160px;
	}
	div.rule .col input.small {
		max-width: 70px;
	}
	div.rule .col label {
		font-size: 8pt;
	}
	a.deleterule {
		display: inline-block;
		color: #333333;
		padding-top: 28px;
	}
	div.rule .input-append button {
		padding-left: 2px;
		padding-right: 0px;
	}
</style>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {

		// calendar
		function setupCalendar() {
			js('div.rule div.value .calendar').each(function(){
				Calendar.setup({
					inputField  : js(this).attr('id'),
					ifFormat    : "%Y-%m-%d",
					button      : js(this).attr('id') + '_img',
					align       : "Tl",
					singleClick : true
				});
			});
		}

		js('.addrule').live('click', function(event){

			js.ajax({
				method: 'POST',
				url   : '<?php echo JRoute::_('index.php?option=com_extraregistration&task=filter.getfields', false); ?>',
				dataType: 'html'
			})
			.done(function (data) {
				js('#rules').append(data);
			});

			return false;
		});

		js('.deleterule').live('click', function(event){
			js(this).parent().parent().remove();
		});

		// group change
		js('div.rule div.group select').live('change', function(event){
			var $rule = js(this).parent().parent();
			js.ajax({
				method: 'POST',
				url   : '<?php echo JRoute::_('index.php?option=com_extraregistration&task=filter.getfieldField', false); ?>',
				dataType: 'html',
				data: {
					groupId: $rule.find('div.group select').val()
				}
			})
			.done(function (data) {
				$rule.find('div.field').html(data);
				$rule.find('div.field select').trigger('change');
			});

			return false;
		});

		// field change
		js('div.rule div.field select').live('change', function(event){
			var $rule = js(this).parent().parent();
			js.ajax({
				method: 'POST',
				url   : '<?php echo JRoute::_('index.php?option=com_extraregistration&task=filter.getfieldComparison', false); ?>',
				dataType: 'html',
				data: {
					fieldId: $rule.find('div.field select').val()
				}
			})
			.done(function (data) {
				$rule.find('div.comparison').html(data);
				$rule.find('div.comparison select').trigger('change');
			});

			return false;
		});

		// comparison change
		js('div.rule div.comparison select').live('change', function(event){
			var $rule = js(this).parent().parent();
			js.ajax({
				method: 'POST',
				url   : '<?php echo JRoute::_('index.php?option=com_extraregistration&task=filter.getfieldValue', false); ?>',
				dataType: 'html',
				data: {
					fieldId: $rule.find('div.field select').val(),
					comparisonId: $rule.find('div.comparison select').val()
				}
			})
			.done(function (data) {
				$rule.find('div.value').html(data);
				setupCalendar();
			});

			return false;
		});

		// apply filter
		js('.apply-filter').live('click', function(){
			var $button = js(this);
			$button.text($button.text() + '...').attr('disabled', 'disabled');
			js('#filterResults').html('...');
			js.ajax({
				method: 'POST',
				url   : '<?php echo JRoute::_('index.php?option=com_extraregistration&task=filter.applyFilter', false); ?>',
				dataType: 'html',
				data: {filterId: <?php echo (int) $this->item->id; ?>}
			})
			.done(function (data) {
				js('#filterResults').html(data);
				$button.text($button.text().replace(/\.\.\.$/, '')).removeAttr('disabled');
			});

			return false;
		});

		// load rules
		js.ajax({
			method: 'POST',
			url   : '<?php echo JRoute::_('index.php?option=com_extraregistration&task=filter.getfields', false); ?>',
			dataType: 'html',
			data: {filterId: <?php echo (int) $this->item->id; ?>}
		})
		.done(function (data) {
			js('#rules').append(data);
		});

	});

	Joomla.submitbutton = function (task) {
		if (task == 'filter.cancel') {
			Joomla.submitform(task, document.getElementById('filter-form'));
		}
		else {
			if (task != 'filter.cancel' && document.formvalidator.isValid(document.id('filter-form'))) {
				Joomla.submitform(task, document.getElementById('filter-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>
<form
	action="<?php echo JRoute::_('index.php?option=com_extraregistration&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="filter-form" class="form-validate">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>
			<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />

			<div class="form-horizontal">
				<div class="row-fluid">
					<div class="span10 form-horizontal">
						<fieldset class="adminform">

							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('title'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('title'); ?></div>
							</div>

						</fieldset>
					</div>
				</div>
				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>
			</div>

			<h4 class="module-title">
				<span style="display: inline-block;"><?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_RULES'); ?></span>
				<a href="#" class="btn btn-success addrule"><span class="icon-new icon-white"></span> <?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_ADD_RULE'); ?></a>
			</h4>
			<hr>
			<div id="rules"></div>
</form>

<div style="padding: 0; margin: 20px 0 0 0;">
	<button class="btn apply-filter"><?php echo JText::_('COM_EXTRAREGISTRATION_FILTER_APPLY'); ?></button>
	(<?php echo JText::_('COM_EXTRAREGISTRATION_FILTER_APPLY_SAVE_NOTICE'); ?>)
</div>
<hr>
<div id="filterResults" style="max-height: 220px;overflow: auto;"></div>

