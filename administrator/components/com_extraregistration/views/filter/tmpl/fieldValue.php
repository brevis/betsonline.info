<?php defined('_JEXEC') or die; ?>

<label><?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_VALUE'); ?></label>
<?php if ($this->valueType) : ?>
	<?php if ($this->valueType == FilterValueType::TEXT_FIELD) : ?>
		<?php
		// Date
		if ($this->field->field_type == 4) : ?>
			<?php echo JHTML::_('calendar', $this->value, 'filter[value][]', 'field' . mt_rand(1, 999999), '%Y-%m-%d', array('class'=>'inputbox calendar', 'size'=>'20',  'maxlength'=>'19')); ?>
		<?php else : ?>
			<input type="text" name="filter[value][]" value="<?php echo htmlspecialchars($this->value); ?>">
		<?php endif; ?>
	<?php endif; ?>
	<?php if ($this->valueType == FilterValueType::RANGE) : ?>
		<?php
		// Date
		if ($this->field->field_type == 4) : ?>
			<?php echo JHTML::_('calendar', $this->value_from, 'filter[value_from][]', 'field' . mt_rand(1, 999999), '%Y-%m-%d', array('class'=>'inputbox calendar small', 'size'=>'20',  'maxlength'=>'19')); ?> –
			<?php echo JHTML::_('calendar', $this->value_to, 'filter[value_to][]', 'field' . mt_rand(1, 999999), '%Y-%m-%d', array('class'=>'inputbox calendar small', 'size'=>'20',  'maxlength'=>'19')); ?>
		<?php else : ?>
			<input type="text" name="filter[value_from][]" class="small" value="<?php echo htmlspecialchars($this->value_from); ?>" placeholder="<?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_FROM'); ?>"> –
			<input type="text" name="filter[value_to][]" class="small" value="<?php echo htmlspecialchars($this->value_to); ?>" placeholder="<?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_TO'); ?>">
		<?php endif; ?>
	<?php endif; ?>
	<?php if ($this->valueType == FilterValueType::SELECT) : ?>
		<select name="filter[value][]">
			<?php foreach ($this->values as $key=>$value) : ?>
				<option value="<?php echo (int) $value->id; ?>"<?php if ((int) $value->id === (int) $this->value) echo ' selected="selected"'; ?>><?php echo htmlspecialchars($value->title); ?></option>
			<?php endforeach; ?>
		</select>
	<?php endif; ?>
<?php endif; ?>