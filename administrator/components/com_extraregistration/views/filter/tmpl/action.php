<?php
defined('_JEXEC') or die;

require_once __DIR__ . '/../../../helper.php';

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');

JToolBarHelper::title(JText::_('EXTRAREGISTRATION'), 'generic.png');

$actionName = JRequest::getString('action');
$filter = ComExtraregistrationHelper::getFilterById(JRequest::getInt('filterId'));
if (!is_object($filter))
{
	JError::raiseError(1, JText::_('COM_EXTRAREGISTRATION_FILTER_NOT_FOUND') );
	$app    = JFactory::getApplication();
	$app->redirect('index.php?option=com_extraregistration&task=filters');
}
$rules = ComExtraregistrationHelper::getAllRulesByFilter(JRequest::getInt('filterId'));
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		js('.apply-filter').live('click', function(){
			var $button = js(this);
			$button.text($button.text() + '...').attr('disabled', 'disabled');
			js('#filterResults').html('...');
			js.ajax({
				method: 'POST',
				url   : '<?php echo JRoute::_('index.php?option=com_extraregistration&task=filter.applyFilter', false); ?>',
				dataType: 'html',
				data: {filterId: <?php echo (int) $filter->id; ?>}
			})
			.done(function (data) {
				js('#filterResults').html(data);
				$button.text($button.text().replace(/\.\.\.$/, '')).removeAttr('disabled');
			});

			return false;
		});
	});

	Joomla.submitbutton = function (task) {
		if (task == 'filter.cancel') {
			Joomla.submitform(task, document.getElementById('filter-form'));
		}
		else {
			if (task != 'filter.cancel' && document.formvalidator.isValid(document.id('filter-form'))) {
				Joomla.submitform(task, document.getElementById('filter-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<?php if (!empty($this->sidebar)): ?>
<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
	<?php else : ?>
	<div id="j-main-container">
		<?php endif; ?>
		<form
			action="<?php echo JRoute::_('index.php?option=com_extraregistration&task=filters'); ?>"
			method="post" enctype="multipart/form-data" name="adminForm" id="filter-form" class="form-validate">
			<input type="hidden" name="task" value="" />
			<?php echo JHtml::_('form.token'); ?>
		</form>

		<h2>
			<span><?php echo htmlspecialchars($filter->title); ?></span>
			<button class="btn apply-filter" style="margin-left: 20px;"><?php echo JText::_('COM_EXTRAREGISTRATION_FILTER_APPLY'); ?></button>
		</h2>

		<div>
			<div id="filterResults" style="max-height: 220px;overflow: auto;"></div>
		</div>
		<hr>

		<?php include 'actions/' . $actionName . '.php'; ?>