<?php
defined('_JEXEC') or die;

$editor = JFactory::getEditor();
?>

<script>
	js = jQuery.noConflict();
	js(document).ready(function () {
		js('#sendEmailForm button.submit').click(function (event) {
			var $button = js(this),
				$form = js('#sendEmailForm'),
				form = document.getElementById('sendEmailForm'),
				formRaw = $form.serializeArray(),
				postData = {};

			if (js('#sendEmailForm input[name="subject"]').val().trim() == '') {
				js('#sendEmailForm input[name="subject"]').focus();
				tinymce.execCommand('mceFocus',false,'id_of_textarea');
				return false;
			}

			if (tinyMCE.get('txtbody').getContent().trim() == '') {
				tinyMCE.execCommand('mceFocus', false, 'txtbody');
				return false;
			}

			$button.text($button.text() + '...').attr('disabled', 'disabled');
			js('#output').html('...');
			$form.find('*').attr('disabled', 'disabled');

			for (var i in formRaw) {
				postData[formRaw[i].name] = formRaw[i].value;
			}
			postData.body = tinyMCE.get('txtbody').getContent({format : 'raw'});

			js.ajax({
				method: 'POST',
				url   : '<?php echo JRoute::_('index.php?option=com_extraregistration&task=filter.callAction', false); ?>',
				dataType: 'html',
				data: postData
			})
			.done(function (data) {
				js('#output').html(data);
				$button.text($button.text().replace(/\.\.\.$/, '')).removeAttr('disabled');
				$form.find('*').removeAttr('disabled');
			});

			return false;
		});

		tinyMCE.init({
			mode : "textareas",
			theme : "simple",
			editor_selector : "mceSimple"
		});
	});
</script>

<form action="<?php echo JRoute::_('index.php?option=com_extraregistration&task=filter.callAction', false); ?>" id="sendEmailForm" method="post" onsubmit="return false;">
	<input type="hidden" name="filterId" value="<?php echo $filter->id; ?>">
	<input type="hidden" name="action" value="<?php echo htmlspecialchars($actionName); ?>">

	<p>
		<label><?php echo JText::_('COM_EXTRAREGISTRATION_FILTER_ACTION_SEND_EMAIL_SUBJECT'); ?></label>
		<input type="text" name="subject" style="width: 608px;" placeholder="<?php echo JText::_('COM_EXTRAREGISTRATION_FILTER_ACTION_SEND_EMAIL_SUBJECT'); ?>" required>
	</p>


	<label><?php echo JText::_('COM_EXTRAREGISTRATION_FILTER_ACTION_SEND_EMAIL_BODY'); ?></label>
	<table>
		<tr>
			<td style="width: 620px;" valign="top">
				<?php echo $editor->display('body', $value, '600', '400', '60', '20', false, 'txtbody'); ?>
			</td>
			<td valign="top">
				<h6><?php echo JText::_('COM_EXTRAREGISTRATION_FILTER_FIELDS'); ?></h6>
				<div>%id%</div>
				<div>%username%</div>
				<?php foreach ($rules as $rule) : ?>
					<div>%field_<?php echo $rule->field_id; ?>%</div>
				<?php endforeach; ?>
			</td>
		</tr>
	</table>

	<button type="button" class="btn btn-success submit"><?php echo JText::_('COM_EXTRAREGISTRATION_FILTERS_GRID_TITLE_ACTION_SEND_EMAIL'); ?></button>
</form>
<div id="output"></div>