<?php defined('_JEXEC') or die; ?>

<?php if (is_array($this->users) && count($this->users) > 0) : ?>
	<h4><?php echo sprintf(JText::_('COM_EXTRAREGISTRATION_FILTER_USERS_FOUND'), count($this->users)); ?></h4>
	<table class="table">
		<tr>
			<th style="width: 30px;">ID</th>
			<th><?php echo JText::_('COM_EXTRAREGISTRATION_FILTER_TITLE_USERNAME'); ?></th>
			<th><?php echo JText::_('COM_EXTRAREGISTRATION_EMAIL'); ?></th>
			<?php foreach ($this->users[0] as $k=>$v) : ?>
				<?php if (!in_array($k, array('id', 'username', 'email'))) : ?><th><?php echo $k ?></th><?php endif; ?>
			<?php endforeach; ?>
		</tr>
	<?php foreach ($this->users as $user) : ?>
		<tr>
			<td><?php echo $user->id; ?></td>
			<td><?php echo htmlspecialchars($user->username); ?></td>
			<td><?php echo htmlspecialchars($user->email); ?></td>
			<?php foreach ($this->users[0] as $k=>$v) : ?>
				<?php if (!in_array($k, array('id', 'username', 'email'))) : ?><td><?php echo htmlspecialchars($v) ?></td><?php endif; ?>
			<?php endforeach; ?>
		</tr>
	<?php endforeach; ?>
	</table>
<?php else : ?>
	<?php echo JText::_('COM_EXTRAREGISTRATION_FILTER_HAS_NO_USERS'); ?>
<?php endif;
