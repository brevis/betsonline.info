<?php defined('_JEXEC') or die; ?>

<label><?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_COMPARISON'); ?></label>
<select name="filter[comparison][]">
	<?php if ($this->comparisons) : ?>
		<?php if (in_array(FilterComparison::EQUALS, $this->comparisons, true)) : ?>
			<option value="<?php echo FilterComparison::EQUALS; ?>"<?php if ($this->comparison == FilterComparison::EQUALS) echo ' selected="selected"'; ?>><?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_COMPARISON_EQUALS'); ?></option>
		<?php endif; ?>
		<?php if (in_array(FilterComparison::NOT_EQUALS, $this->comparisons, true)) : ?>
			<option value="<?php echo FilterComparison::NOT_EQUALS; ?>"<?php if ($this->comparison == FilterComparison::NOT_EQUALS) echo ' selected="selected"'; ?>><?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_COMPARISON_NOT_EQUALS'); ?></option>
		<?php endif; ?>
		<?php if (in_array(FilterComparison::LIKE, $this->comparisons, true)) : ?>
			<option value="<?php echo FilterComparison::LIKE; ?>"<?php if ($this->comparison == FilterComparison::LIKE) echo ' selected="selected"'; ?>><?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_COMPARISON_LIKE'); ?></option>
		<?php endif; ?>
		<?php if (in_array(FilterComparison::LESS_THAN, $this->comparisons, true)) : ?>
			<option value="<?php echo FilterComparison::LESS_THAN; ?>"<?php if ($this->comparison == FilterComparison::LESS_THAN) echo ' selected="selected"'; ?>><?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_COMPARISON_LESS_THAN'); ?></option>
		<?php endif; ?>
		<?php if (in_array(FilterComparison::MORE_THAN, $this->comparisons, true)) : ?>
			<option value="<?php echo FilterComparison::MORE_THAN; ?>"<?php if ($this->comparison == FilterComparison::MORE_THAN) echo ' selected="selected"'; ?>><?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_COMPARISON_MORE_THAN'); ?></option>
		<?php endif; ?>
		<?php if (in_array(FilterComparison::RANGE, $this->comparisons, true)) : ?>
			<option value="<?php echo FilterComparison::RANGE; ?>"<?php if ($this->comparison == FilterComparison::RANGE) echo ' selected="selected"'; ?>><?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_COMPARISON_RANGE'); ?></option>
		<?php endif; ?>
	<?php endif; ?>
</select>
