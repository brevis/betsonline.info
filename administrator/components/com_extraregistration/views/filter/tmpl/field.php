<?php defined('_JEXEC') or die; ?>

<div class="rule">
	<div class="col group">
		<label><?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_GROUP'); ?></label>
		<?php echo JHTML::_('access.usergroup', 'filter[group][]', $this->groupId, '', false); ?>
	</div>
	<div class="col field">
		<?php include __DIR__ . '/fieldField.php'; ?>
	</div>
	<div class="col comparison">
		<?php include __DIR__ . '/fieldComparison.php'; ?>
	</div>
	<div class="col value">
		<?php include __DIR__ . '/fieldValue.php'; ?>
	</div>
	<div class="col" style="width: 30px;">
		<a href="#" class="deleterule"><span class="icon-trash"></span></a>
	</div>
	<div style="clear: both;"></div>
</div>