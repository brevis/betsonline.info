<?php defined('_JEXEC') or die; ?>

<label><?php echo JText::_('COM_EXTRAREGISTRATION_FORM_LBL_FILTER_FIELD'); ?></label>
<select name="filter[field][]">
	<?php foreach ($this->fields as $field) : ?>
		<option value="<?php echo $field->id; ?>"<?php if ($field->id == $this->fieldId) echo ' selected="selected"'; ?>><?php echo htmlspecialchars($field->title); ?></option>
	<?php endforeach; ?>
</select>
