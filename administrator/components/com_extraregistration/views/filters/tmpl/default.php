<?php
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
//$document->addStyleSheet(JUri::root() . 'administrator/components/com_paytest/assets/css/paytest.css');
//$document->addStyleSheet(JUri::root() . 'media/com_paytest/css/list.css');

$user      = JFactory::getUser();
$userId    = $user->get('id');
$canOrder  = $user->authorise('core.edit.state', 'com_extraregistration');

$app = JFactory::getApplication();
$admin_template = $app->getTemplate();

JToolBarHelper::title( JText::_( 'EXTRAREGISTRATION' ), 'generic.png' );
?>
<script type="text/javascript">
	jQuery(document).ready(function () {

	});
</script>

<?php

// Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar))
{
	$this->sidebar .= $this->extra_sidebar;
}

?>

<form action="<?php echo JRoute::_('index.php?option=com_extraregistration&task=filters'); ?>" method="post"
		name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>


			<div class="clearfix"></div>
			<table class="table table-striped" id="questionList">
				<thead>
				<tr>
					<th width="1%">
						<input type="checkbox" name="checkall-toggle" value=""
								title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
					</th>
					<th class='left'>
						<?php echo JText::_('COM_EXTRAREGISTRATION_FILTERS_GRID_TITLE_NAME'); ?>
					</th>
					<th class='left' style="width: 160px;">
						<?php echo JText::_('COM_EXTRAREGISTRATION_FILTERS_GRID_TITLE_ACTIONS'); ?>
					</th>

				</tr>
				</thead>
				<tbody>
				<?php foreach ($this->items as $i => $item) :
					$canCreate  = $user->authorise('core.create', 'com_extraregistration');
					$canEdit    = $user->authorise('core.edit', 'com_extraregistration');
					?>
					<tr class="row<?php echo $i % 2; ?>">
						<td class="hidden-phone">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<td>
							<?php if ($canEdit) : ?>
								<a href="<?php echo JRoute::_('index.php?option=com_extraregistration&task=filter.edit&id=' . (int) $item->id); ?>">
									<?php echo $this->escape(strip_tags($item->title)); ?></a>
							<?php else : ?>
								<?php echo $this->escape(strip_tags($item->title)); ?>
							<?php endif; ?>
						</td>
						<td>
							<div class="dropdown" style="display: inline-block">
								<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									<?php echo JText::_('COM_EXTRAREGISTRATION_FILTERS_GRID_TITLE_ACTION'); ?>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="<?php echo JRoute::_('index.php?option=com_extraregistration&task=filter&view=filter&layout=action&filterId=' . (int) $item->id . '&action=SendEmail'); ?>"><?php echo JText::_('COM_EXTRAREGISTRATION_FILTERS_GRID_TITLE_ACTION_SEND_EMAIL'); ?></a></li>
								</ul>
							</div>
							<a href="<?php echo JRoute::_('index.php?option=com_extraregistration&task=filter.trash&cid[]=' . (int) $item->id); ?>" class="btn btn-danger" style="padding-left: 4px;padding-right: 1px;" onclick="return confirm('<?php echo JText::_('COM_EXTRAREGISTRATION_ARE_YOU_SURE'); ?>');"><span class="icon-trash icon-white"></span></a>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>

			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
