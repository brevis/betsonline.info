<?php
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Filters.
 *
 * @since  1.6
 */
class ExtraregistrationViewFilters extends JViewLegacy
{
	protected $items;

	/**
	 * Display the view
	 *
	 * @param   string $tpl Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->items      = $this->get('Items');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		$vName = JFactory::getApplication()->input->get('view');
		$this->addToolbar($vName);

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @param $vName
	 *
	 * @since    1.6
	 */
	protected function addToolbar($vName)
	{
		JToolBarHelper::addNew('filter.add');
		JToolBarHelper::editList('filter.edit');
		JToolBarHelper::trash('filter.trash');

		JHtmlSidebar::addEntry( JText::_( 'COM_EXTRAREGISTRATION_USERS' ), 'index.php?option=com_extraregistration',
				in_array($vName, array('','edituser','deleteuser','saveuser','canceluser','userpub')));

		JHtmlSidebar::addEntry( JText::_( 'COM_EXTRAREGISTRATION_FIELDS' ), 'index.php?option=com_extraregistration&task=fields',
				in_array($vName, array('add', 'edit','required', 'fields','save', 'cancel','delete','active','reg',
						'canedit', 'orderup', 'orderdown'))  );

		JHtmlSidebar::addEntry( JText::_( 'COM_EXTRAREGISTRATION_LISTS' ), 'index.php?option=com_extraregistration&task=lists',
				in_array($vName, array('lists', 'newlist', 'newitem', 'edititem','deleteitem', 'showlist', 'editlist', 'savelist', 'saveitem','cancelitem','cancellist','deletelist')) );

		/*
		JHtmlSidebar::addEntry( JText::_( 'COM_EXTRAREGISTRATION_MAIL' ), 'index.php?option=com_extraregistration&task=mailmode',
				in_array($vName, array('mailmode', 'newmail', 'editmail', 'delmail', 'activemail', 'savemail', 'cancelmail')) );
		*/

		JHtmlSidebar::addEntry( JText::_( 'COM_EXTRAREGISTRATION_FILTER' ), 'index.php?option=com_extraregistration&task=filters',
				in_array($vName, array('filters', 'newfilter', 'editfilter', 'trash', 'activefilter', 'savefilter', 'cancelfilter')) );

		JHtmlSidebar::addEntry( JText::_( 'COM_EXTRAREGISTRATION_OPTIONS' ), 'index.php?option=com_extraregistration&task=options',

				in_array($vName, array('options', 'saveoptions')) );
		JHtmlSidebar::setAction('index.php?option=com_extraregistration&view=filters');
	}

}
