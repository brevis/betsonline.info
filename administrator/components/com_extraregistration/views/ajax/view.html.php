<?php defined('_JEXEC') or die( 'Restriction access' );

jimport( 'joomla.application.component.view');

# For compatibility with older versions of Joola 2.5
if (!class_exists('JViewLegacy')){
    class JViewLegacy extends JView {

    }
}

class ExtraregistrationViewAjax extends JViewLegacy
{
	function display($tpl = null)
	{
		parent::display($tpl);
	}
}
?>
