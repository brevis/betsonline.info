<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Filter records.
 *
 * @since  1.6
 */
class ExtraregistrationModelFilters extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array $config An optional associative array of configuration settings.
	 *
	 * @see        JController
	 * @since      1.6
	 */
	public function __construct($config = array())
	{
		/*
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
				'modified_by', 'a.`modified_by`',
				'article_id', 'a.`article_id`',
				'question', 'a.`question`',
				'price', 'a.`price`',
				'result', 'a.`result`',
				'article_id', 'a.`article_id`',
			);
		}
		*/

		parent::__construct($config);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		$query = "SELECT * FROM #__extrareg_filter ORDER BY title ASC";
		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		/*
		foreach ($items as $oneItem)
		{

			if (isset($oneItem->article_id))
			{
				$values = explode(',', $oneItem->article_id);

				$textValue = array();
				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db    = JFactory::getDbo();
						$query = "select id, title from #__content where state = 1 HAVING id LIKE '" . $value . "'";
						$db->setQuery($query);
						$results = $db->loadObject();
						if ($results)
						{
							$textValue[] = $results->title;
						}
					}
				}

				$oneItem->article_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->article_id;
			}
		}
		*/
		return $items;
	}
}
