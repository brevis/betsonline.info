<?php
defined('_JEXEC') or die;

class ComExtraregistrationHelper
{
	public static function getChildGroupsIds($group_id)
	{
		// do not use hierarchy
		return array($group_id);

		/*
		$db = JFactory::getDBO();
		$query = $db->getQuery(true)
			->select('n.id, n.parent_id')
			->from('#__usergroups AS n, #__usergroups  AS p')
			->where('n.lft BETWEEN p.lft AND p.rgt')
			->where('p.id = ' . (int) $group_id)
			->order('n.lft');
		$db->setQuery($query);
		$ids = array();
		foreach ($db->loadObjectList() as $r)
		{
			$ids[] = $r->id;
		}
		return $ids;
		*/
	}

	public static function filterFieldsByUserGroups($rows, $user_groups)
	{
		$rows = array_filter($rows, function ($row) use ($user_groups)
		{
			$field_groups = array();
			foreach (explode(',', $row->groups) as $g)
			{
				$field_groups = array_merge($field_groups, self::getChildGroupsIds((int) $g));
			}
			$field_groups = array_filter(array_unique($field_groups));
			$intersect    = array_intersect($user_groups, $field_groups);

			return count($field_groups) == 0 || count($intersect) >= 1;
		});

		return $rows;
	}

	public static function printFieldsHtml($rows, $uid = null, $isAdmin = false, $readOnly = false)
	{
		$language = JFactory::getLanguage();
		$language->load('com_extraregistration', JPATH_SITE, $language->getTag(), true);

		$db = JFactory::getDBO();

		foreach ($rows as $row)
		{
			$r  = '';
			$r1 = '';
			echo "<div class='col-sm-6 form-group'>";
			echo "<label class='label-left' for='field_$row->id'>$row->title : ";
			if ($row->required == 1)
			{
				$r  = 'required';
				$r1 = "required='required'";
				echo ' <span style="color: red;">*</span> ';
			}
			echo " </label>";

			$showEditor = $isAdmin || !$readOnly;
			$editorDisplay = $showEditor ? 'block' : 'none';

			switch ($row->field_type)
			{
				case 1:
					echo "<input type='text' name='field_$row->id' id='field_$row->id' class='$r form-control' $r1 value=\"" . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . "\" style=\"display: $editorDisplay\">";
					if (!$showEditor)
					{
						echo "<span id=\"readonly{$row->id}\">" . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . "</span>";
						if ($row->can_edit == 1)
						{
							echo "<a href='#' id=\"showeditor{$row->id}\" class=\"showeditor\" data-fieldid=\"{$row->id}\">".JText::_('COM_EXTRAREGISTRATION_CHANGE')."</a>";
						}
					}
					break;
				case 2:
					echo "<input type='text' name='field_$row->id' id='field_$row->id' class='$r form-control' $r1 value=\"" . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . "\" onblur='ValidateNumber(this);' style=\"display: $editorDisplay\">";
					if (!$showEditor)
					{
						echo "<span id=\"readonly{$row->id}\">" . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . "</span>";
						if ($row->can_edit == 1)
						{
							echo "<a href='#' id=\"showeditor{$row->id}\" class=\"showeditor\" data-fieldid=\"{$row->id}\">".JText::_('COM_EXTRAREGISTRATION_CHANGE')."</a>";
						}
					}
					break;
				case 3:
					$src = self::GetFieldValue($row->id, $uid);
					if ($src != '')
					{
						$r  = '';
						$r1 = '';
					}

					if ($src != '')
					{
						echo "<a href='$src' class='modal'><img src='$src' style='height: 80px !important;'></a> <br>";
					}

					echo "<input type='file' name='field_$row->id' id='field_$row->id' class='$r' $r1 value=''  style=\"display: $editorDisplay\">";

					if (!$showEditor)
					{
						if ($row->can_edit == 1)
						{
							echo "<a href='#' id=\"showeditor{$row->id}\" class=\"showeditor\" data-fieldid=\"{$row->id}\">".JText::_('COM_EXTRAREGISTRATION_CHANGE')."</a>";
						}
					}
					break;
				case 4:
					echo '<div id="editbox'.$row->id.'" style="display: '.$editorDisplay.'">';
					echo JHTML::_('calendar', self::GetFieldValue($row->id, $uid), "field_$row->id", "field_$row->id", '%Y-%m-%d', array('class' => "form-control $r calendar"/* , 'size' => '20', 'maxlength' => '19' */));
					echo '</div>';

					if (!$showEditor)
					{
						echo "<span id=\"readonly{$row->id}\">" . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . "</span>";
						if ($row->can_edit == 1)
						{
							echo "<a href='#' id=\"showeditor{$row->id}\" class=\"showeditor\" data-fieldid=\"{$row->id}\">".JText::_('COM_EXTRAREGISTRATION_CHANGE')."</a>";
						}
					}
					break;
				case 5:
					if (empty($uid))
					{
						$city_id = '';
					}
					else
					{
						$city_id = self::GetFieldItem($row->id, $uid);
					}
					if ($city_id != '')
					{
						$query      = "select * from `#__extrareg_city` where city_id = $city_id";
						$result     = $db->setQuery($query);
						$city       = $db->loadAssoc();
						$country_id = $city['country_id'];
						$region_id  = $city['region_id'];
					}
					else
					{
						$city_id    = '';
						$country_id = '';
						$region_id  = '';
					}

					echo '<div id="editbox'.$row->id.'" style="display: '.$editorDisplay.'">';
					echo self::GetCountrySelect($row->id, $country_id);
					if ($city_id != '')
					{
						echo " <span id='sregion_$row->id'>" . self::GetRegionSelect($row->id, $region_id, $country_id) . "</span>";
						echo " <span id='scity_$row->id'>" . self::GetCitySelect($row->id, $city_id, $region_id) . "</span> <span id='status'></span>";
					}
					else
					{
						echo " <span id='sregion_$row->id'></span>";
						echo " <span id='scity_$row->id'></span> <span id='status'></span>";
					}
					echo '</div>';

					if (!$showEditor)
					{
						echo "<span id=\"readonly{$row->id}\">";
						if ($city_id != '')
						{
							$query     = "select name from `#__extrareg_city` where city_id = $city_id";
							$result    = $db->setQuery($query);
							$city_name = $db->loadResult();
							echo $city_name;
						}
						echo "</span>";
						if ($row->can_edit == 1)
						{
							echo "<a href='#' id=\"showeditor{$row->id}\" class=\"showeditor\" data-fieldid=\"{$row->id}\">".JText::_('COM_EXTRAREGISTRATION_CHANGE')."</a>";
						}
					}
					break;
				case 6:
					echo '<div id="editbox'.$row->id.'" style="display: '.$editorDisplay.'">';
					echo "<input type='text' name='field_$row->id' id='field_$row->id' class='$r form-control' $r1 onblur='ValidateNumber(this);' value=\"" . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . "\" size='5' style=\"display: $editorDisplay\"> " . htmlspecialchars(self::GetZOption('abbr'));
					echo '</div>';

					if (!$showEditor)
					{
						echo "<span id=\"readonly{$row->id}\">" . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . ' ' . htmlspecialchars(self::GetZOption('abbr')) . "</span>";
						if ($row->can_edit == 1)
						{
							echo "<a href='#' id=\"showeditor{$row->id}\" class=\"showeditor\" data-fieldid=\"{$row->id}\">".JText::_('COM_EXTRAREGISTRATION_CHANGE')."</a>";
						}
					}
					break;
				case 7:
					if ($uid == null)
					{
						echo "<select name='field_$row->id' id='field_$row->id' class='$r form-control' $r1>";
						$query  = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
						$result = $db->setQuery($query);
						$items  = $db->loadObjectList();
						if (count($items) > 0)
						{
							foreach ($items as $item)
							{
								echo "<option value='$item->id'>$item->title</option>";
							}
						}
						echo "</select>";
					}
					else
					{
						echo "<select name='field_$row->id' id='field_$row->id' class='$r form-control' $r1 style='display: $editorDisplay'>";
						$query       = "select * from `#__extrareg_fields_values` where field_id = $row->id and uid = $uid order by id";
						$result      = $db->setQuery($query);
						$field_value = $db->loadAssoc();
						$item_id     = $field_value['item_id'];
						$query       = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
						$result      = $db->setQuery($query);
						$items       = $db->loadObjectList();
						if (count($items) > 0)
						{
							foreach ($items as $item)
							{
								if ($item_id == $item->id)
								{
									$s = 'selected';
								}
								else
								{
									$s = '';
								}
								echo "<option value='$item->id' $s>$item->title</option>";
							}
						}
						echo "</select>";

						if (!$showEditor)
						{
							echo "<span id=\"readonly{$row->id}\">" . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . "</span>";
							if ($row->can_edit == 1)
							{
								echo "<a href='#' id=\"showeditor{$row->id}\" class=\"showeditor\" data-fieldid=\"{$row->id}\">".JText::_('COM_EXTRAREGISTRATION_CHANGE')."</a>";
							}
						}
					}
					break;
				case 8:
					if ($uid == null || $isAdmin)
					{
						echo "<input type='password' name='field_$row->id-1' id='field_$row->id-1' class='$r form-control' $r1 onblur='ValidatePass($row->id);'> <input type='password' name='field_$row->id-2' id='field_$row->id-2' class='$r' $r1 onblur='ValidatePass($row->id);'>";
					}
					else
					{
						if ($row->can_edit == 1)
						{
							echo "<a href='index.php?option=com_extraregistration&view=registrationform&task=newpass&id=$row->id&uid=$uid'>" . JText::_('COM_EXTRAREGISTRATION_NEW_PASS') . "</a>";
						}
						else
						{
							echo "[password]";
						}
					}
					break;
				case 9:
					if ($uid == null || $isAdmin)
					{
						echo "<input type='text' name='field_$row->id' id='field_$row->id' class='$r form-control' $r1 onblur='ValidateUser(this, $row->id, $row->required);'> <span id='img_$row->id'><span>" . self::Tooltip(JText::_('COM_EXTRAREGISTRATION_ENTER_EMAIL'));
					}
					else
					{
						echo '<div id="editbox'.$row->id.'" style="display: '.$editorDisplay.'">';
						echo self::GetUserList2(self::GetFieldValue($row->id, $uid), "field_$row->id");
						echo '</div>';

						if (!$showEditor)
						{
							echo "<span id=\"readonly{$row->id}\">" . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . "</span>";
							if ($row->can_edit == 1)
							{
								echo "<a href='#' id=\"showeditor{$row->id}\" class=\"showeditor\" data-fieldid=\"{$row->id}\">".JText::_('COM_EXTRAREGISTRATION_CHANGE')."</a>";
							}
						}
					}
					break;
				case 11:
					if ($isAdmin)
					{
						echo "<input type='text' name='field_$row->id' id='field_$row->id' value=\"" . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . "\" onblur='ValidateNumber(this);'>";
					}
					elseif ($uid == null)
					{
						echo jsms_get_timezone_select() . " " . self::Tooltip(JText::_('COM_EXTRAREGISTRATION_TIMEZONE')) . "<br><input type='text' name='field_$row->id' id='field_$row->id' class='$r form-control' $r1 value='' onblur='ValidateNumber(this);'><span id='check_$row->id' style='display: none;'><input type='text' name='code_$row->id' id='code_$row->id' value='' size='10' class='form-control'> " . self::Tooltip(JText::_('COM_EXTRAREGISTRATION_ENTER_CODE')) . "</span> <a href='#' onclick='CheckNumber(this, $row->id);'>" . JText::_('COM_EXTRAREGISTRATION_CHECK') . "</a>";
					}
					break;
				case 12:
					if ($uid == null)
					{
						echo "<select multiple='multiple' name='field_$row->id" . '[]' . "' id='field_$row->id' class='$r form-control' $r1>";
						$query  = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
						$result = $db->setQuery($query);
						$items  = $db->loadObjectList();
						if (count($items) > 0)
						{
							foreach ($items as $item)
							{
								echo "<option value='$item->id'>$item->title</option>";
							}
						}
						echo "</select>";
					}
					else
					{
						echo "<select multiple='multiple' class='$r form-control' name='field_$row->id" . '[]' . "' id='field_$row->id' style='display: $editorDisplay'>";
						$query       = "select * from `#__extrareg_fields_values` where field_id = $row->id and uid = $uid";
						$result      = $db->setQuery($query);
						$field_value = $db->loadAssoc();
						$items_id    = $field_value['value'];
						$item_value  = explode(',', $items_id);
						$query       = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
						$result      = $db->setQuery($query);
						$items       = $db->loadObjectList();
						if (count($items) > 0)
						{
							foreach ($items as $item)
							{
								$s = '';
								for ($i = 0; $i < count($item_value); $i++)
								{
									if ($item_value[$i] == $item->title)
									{
										$s = 'selected';
									}
								}
								echo "<option value='$item->id' $s>$item->title</option>";
							}
						}
						echo "</select>";

						if (!$showEditor)
						{
							echo "<span id=\"readonly{$row->id}\">";
							$query       = "select * from `#__extrareg_fields_values` where field_id = $row->id and uid = $uid";
							$result      = $db->setQuery($query);
							$field_value = $db->loadAssoc();
							$items_id    = $field_value['value'];
							$item_value  = explode(',', $items_id);
							$query       = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
							$result      = $db->setQuery($query);
							$items       = $db->loadObjectList();
							$selectedItems = array();
							if (count($items) > 0)
							{
								foreach ($items as $item)
								{
									for ($i = 0; $i < count($item_value); $i++)
									{
										if ($item_value[$i] == $item->title)
										{
											$selectedItems[] = htmlspecialchars($item->title);
										}
									}
								}
								echo implode(', ', $selectedItems);
							}
							echo "</span>";

							if ($row->can_edit == 1)
							{
								echo "<a href='#' id=\"showeditor{$row->id}\" class=\"showeditor\" data-fieldid=\"{$row->id}\">".JText::_('COM_EXTRAREGISTRATION_CHANGE')."</a>";
							}
						}
					}
					break;
				case 13:
					if ($uid == null)
					{
						$query  = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
						$result = $db->setQuery($query);
						$items  = $db->loadObjectList();
						if (count($items) > 0)
						{
							foreach ($items as $item)
							{
								echo "<label><input type='checkbox' class='form-control' value='$item->id' name='field_$row->id" . '[]' . "'> $item->title</label>";
							}
						}
					}
					else
					{
						echo '<div id="editbox'.$row->id.'" style="display: '.$editorDisplay.'">';
						$query       = "select * from `#__extrareg_fields_values` where field_id = $row->id and uid = $uid";
						$result      = $db->setQuery($query);
						$field_value = $db->loadAssoc();
						$items_id    = $field_value['value'];
						$item_value  = explode(',', $items_id);
						$query       = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
						$result      = $db->setQuery($query);
						$items       = $db->loadObjectList();
						if (count($items) > 0)
						{
							foreach ($items as $item)
							{
								$s = '';
								foreach ($item_value as $key => $value)
								{
									if ($value == $item->title)
									{
										$s = 'checked';
									}
								}
								echo "<label><input type='checkbox' value='$item->id' class='$r form-control' name='field_$row->id" . '[]' . "' $s> $item->title</label>";
							}
						}
						echo '</div>';

						if (!$showEditor)
						{
							echo "<span id=\"readonly{$row->id}\">";
							$query       = "select * from `#__extrareg_fields_values` where field_id = $row->id and uid = $uid";
							$result      = $db->setQuery($query);
							$field_value = $db->loadAssoc();
							$items_id    = $field_value['value'];
							$item_value  = explode(',', $items_id);
							$query       = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
							$result      = $db->setQuery($query);
							$items       = $db->loadObjectList();
							$selectedItems = array();
							if (count($items) > 0)
							{
								foreach ($items as $item)
								{
									for ($i = 0; $i < count($item_value); $i++)
									{
										if ($item_value[$i] == $item->title)
										{
											$selectedItems[] = htmlspecialchars($item->title);
										}
									}
								}
								echo implode(', ', $selectedItems);
							}
							echo "</span>";

							if ($row->can_edit == 1)
							{
								echo "<a href='#' id=\"showeditor{$row->id}\" class=\"showeditor\" data-fieldid=\"{$row->id}\">".JText::_('COM_EXTRAREGISTRATION_CHANGE')."</a>";
							}
						}
					}
					break;
				case 14:
					echo "<textarea name='field_$row->id' id='field_$row->id' class='$r form-control' $r1 cols='50' rows='10' style=\"display: " . $editorDisplay . "\">" . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . "</textarea>";
					if (!$showEditor)
					{
						echo "<textarea id=\"readonly{$row->id}\" readonly='true'> " . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . "</textarea>";
						if ($row->can_edit == 1)
						{
							echo "<a href='#' id=\"showeditor{$row->id}\" class=\"showeditor\" data-fieldid=\"{$row->id}\">".JText::_('COM_EXTRAREGISTRATION_CHANGE')."</a>";
						}
					}
					break;
				default:
					echo "<input type='text' name='field_$row->id' id='field_$row->id' class='$r form-control' $r1 value=\"" . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . "\" style=\"display: $editorDisplay\">";
					if (!$showEditor)
					{
						echo "<span id=\"readonly{$row->id}\">" . htmlspecialchars(self::GetFieldValue($row->id, $uid)) . "</span>";
						if ($row->can_edit == 1)
						{
							echo "<a href='#' id=\"showeditor{$row->id}\" class=\"showeditor\" data-fieldid=\"{$row->id}\">".JText::_('COM_EXTRAREGISTRATION_CHANGE')."</a>";
						}
					}
					break;
			}
			if ($row->comment != '')
			{
				echo self::Tooltip($row->comment);
			}
			echo '</div>';
		}
	}

	public static function getAllFieldsByGroup($groupId)
	{
		$db = JFactory::getDbo();
		$db->setQuery("SELECT * FROM #__extrareg_fields WHERE published = 1 AND (groups = '' OR FIND_IN_SET(" . (int) $groupId . ", groups))");

		return $db->loadObjectList();
	}

	public static function getAllComparisons($field)
	{
		if (!is_object($field))
		{
			return array();
		}
		$comparisons = array();

		// EQUALS, NOT_EQUALS: Text field, Number, Date, Money, List, Username, Phone number, Multi select, Text area
		if (in_array((int) $field->field_type, array(1, 2, 4, 6, 7, 9, 11, 12, 13, 14), true))
		{
			$comparisons[] = FilterComparison::EQUALS;
			$comparisons[] = FilterComparison::NOT_EQUALS;
		}

		// LIKE: Text field, Username, Phone number, Text area
		if (in_array((int) $field->field_type, array(1, 6, 9, 11, 14), true))
		{
			$comparisons[] = FilterComparison::LIKE;
		}

		// LESS_THAN, MORE_THAN, RANGE: Number, Date, Money
		if (in_array((int) $field->field_type, array(2, 4, 6), true))
		{
			$comparisons[] = FilterComparison::LESS_THAN;
			$comparisons[] = FilterComparison::MORE_THAN;
			$comparisons[] = FilterComparison::RANGE;
		}

		return $comparisons;
	}

	public static function getFieldValueType($field, $comparison)
	{
		if (!is_object($field))
		{
			return null;
		}
		if (!in_array((int) $comparison, FilterComparison::all(), true))
		{
			return null;
		}

		// TEXT_FIELD: Text field, Number, Date, Money, Username, Phone number, Text area
		if (in_array((int) $field->field_type, array(1, 2, 4, 6, 9, 11, 14), true)
			&& in_array($comparison, array(FilterComparison::EQUALS, FilterComparison::NOT_EQUALS, FilterComparison::LIKE, FilterComparison::LESS_THAN, FilterComparison::MORE_THAN))
		)
		{
			return FilterValueType::TEXT_FIELD;
		}

		// RANGE: Number, Date, Money
		if (in_array((int) $field->field_type, array(2, 4, 6), true)
			&& in_array($comparison, array(FilterComparison::RANGE))
		)
		{
			return FilterValueType::RANGE;
		}

		// SELECT: List, Milty select
		if (in_array((int) $field->field_type, array(7, 12, 13), true))
		{
			return FilterValueType::SELECT;
		}

		return null;
	}

	public static function getFieldById($fieldId, $published = true)
	{
		$fieldId = (int) $fieldId;
		if ($fieldId <= 0)
		{
			return null;
		}

		$db    = JFactory::getDbo();
		$query = "SELECT * FROM #__extrareg_fields WHERE id = " . (int) $fieldId;
		if ($published)
		{
			$query .= " AND published = 1";
		}
		$query .= " LIMIT 1";
		$db->setQuery($query);

		return $db->loadObject();
	}

	public static function getFieldValues($field)
	{
		if (!is_object($field))
		{
			return array();
		}

		$db    = JFactory::getDbo();
		$query = "SELECT v.id, v.title FROM #__extrareg_list_value v LEFT JOIN #__extrareg_fields f ON (v.list_id=f.list_id) WHERE f.id = " . (int) $field->id;
		$db->setQuery($query);

		return $db->loadObjectList();
	}

	public static function getAllRulesByFilter($filterId)
	{
		$filterId = (int) $filterId;
		if ($filterId <= 0)
		{
			return null;
		}

		$db    = JFactory::getDbo();
		$query = "SELECT * FROM #__extrareg_filter_field WHERE filter_id = " . $filterId . " ORDER BY id ASC";
		$db->setQuery($query);

		return $db->loadObjectList();
	}

	public static function getFilterById($filterId, $published = true)
	{
		$filterId = (int) $filterId;
		if ($filterId <= 0)
		{
			return null;
		}

		$db    = JFactory::getDbo();
		$query = "SELECT * FROM #__extrareg_filter WHERE id = " . $filterId;
		if ($published)
		{
			$query .= " AND published = 1";
		}
		$query .= " LIMIT 1";
		$db->setQuery($query);

		return $db->loadObject();
	}

	public static function getUsersByFilterId($filterId)
	{
		$filter = self::getFilterById($filterId);
		if (!is_object($filter))
		{
			return null;
		}

		$rules = self::getAllRulesByFilter($filterId);
		if (is_array($rules) && count($rules) > 0)
		{
			$db = JFactory::getDbo();

			$querySelect = array('u.id', 'u.username', 'u.email');
			$queryWhere  = array();
			$groupQuery  = array();
			$queryJoin   = array();

			foreach ($rules as $rule)
			{
				$field = self::getFieldById($rule->field_id);
				if (!is_object($field))
				{
					continue;
				}

				// select
				$querySelect[] = "v{$rule->id}.value as field_" . $field->id;

				// join
				$queryJoin[] = " LEFT JOIN #__extrareg_fields_values v{$rule->id} ON (v{$rule->id}.uid=u.id AND v{$rule->id}.field_id={$field->id}) ";

				// where

				// TODO: get list value by id;
				$ruleValue = $rule->value;
				if (in_array($field->field_type, array(7, 12, 13)))
				{
					$query = "SELECT v.title FROM #__extrareg_list_value v LEFT JOIN #__extrareg_list l ON (l.id=v.list_id) WHERE l.id = " . (int) $field->list_id . " AND v.id = " . (int) $rule->value;
					$db->setQuery($query);
					$ruleValue = $db->loadResult();
				}

				if ($rule->comparison == FilterComparison::EQUALS)
				{
					$queryWhere[] = " v{$rule->id}.value = '" . addslashes($ruleValue) . "' ";
				}

				if ($rule->comparison == FilterComparison::NOT_EQUALS)
				{
					$queryWhere[] = " v{$rule->id}.value <> '" . addslashes($ruleValue) . "' ";
				}

				if ($rule->comparison == FilterComparison::LIKE)
				{
					$queryWhere[] = " v{$rule->id}.value LIKE '%" . addcslashes(addslashes($ruleValue), "%_") . "%' ";
				}

				if ($rule->comparison == FilterComparison::MORE_THAN)
				{
					if (is_numeric($ruleValue))
					{
						$queryWhere[] = " v{$rule->id}.value >= " . floatval($ruleValue) . " ";
					}
					else
					{
						$queryWhere[] = " STR_TO_DATE(v{$rule->id}.value, '%Y-%m-%d') >= STR_TO_DATE('" . addslashes($ruleValue) . "', '%Y-%m-%d') ";
					}
				}

				if ($rule->comparison == FilterComparison::LESS_THAN)
				{
					if (is_numeric($ruleValue))
					{
						$queryWhere[] = " v{$rule->id}.value <= " . floatval($ruleValue) . " ";
					}
					else
					{
						$queryWhere[] = " STR_TO_DATE(v{$rule->id}.value, '%Y-%m-%d') <= STR_TO_DATE('" . addslashes($ruleValue) . "', '%Y-%m-%d') ";
					}
				}

				if ($rule->comparison == FilterComparison::RANGE)
				{
					$rangeValues = explode('::', $ruleValue);
					if (is_numeric($rangeValues[0]) && is_numeric($rangeValues[1]))
					{
						$queryWhere[] = " v{$rule->id}.value >= " . floatval($rangeValues[0]) . " " .
							"AND v{$rule->id}.value <= " . floatval($rangeValues[1]) . " ";
					}
					else
					{
						$queryWhere[] = " STR_TO_DATE(v{$rule->id}.value, '%Y-%m-%d') >= '" . addslashes($rangeValues[0]) . "' " .
							"AND STR_TO_DATE(v{$rule->id}.value, '%Y-%m-%d') <= STR_TO_DATE('" . addslashes($rangeValues[1]) . "', 'Y-m-d') ";
					}
				}

				// group
				if ($field->groups != '')
				{
					if (!isset($groupQuery[$field->groups]))
					{
						$queryWhere[]               = " FIND_IN_SET(ug.group_id, '" . addslashes($field->groups) . "') ";
						$groupQuery[$field->groups] = true;
					}
				}
			}

			$query = "SELECT " . implode(', ', $querySelect)
				. " FROM #__users u LEFT JOIN #__user_usergroup_map ug ON (u.id=ug.user_id)"
				. implode('', $queryJoin) . ' WHERE ' . implode(' AND ', $queryWhere);
			$db->setQuery($query);

			return $db->loadObjectList();
		}

		return null;
	}

	public static function GetZOption($option)
	{
		$db     = JFactory::getDBO();
		$query  = "select value from `#__extrareg_options` where `option` = '$option'";
		$result = $db->setQuery($query);
		$result = $db->loadResult();

		return $result;
	}

	public static function GetFieldValue($field_id, $uid = null)
	{
		if ((int) $uid <= 0)
		{
			return '';
		}
		$db     = JFactory::getDBO();
		$query  = "select value from `#__extrareg_fields_values` where field_id = $field_id and uid = $uid";
		$result = $db->setQuery($query);

		return $db->loadResult();
	}

	public static function GetFieldItem($field_id, $uid)
	{
		$db     = JFactory::getDBO();
		$query  = "select item_id from `#__extrareg_fields_values` where field_id = $field_id and uid = $uid";
		$result = $db->setQuery($query);

		return $db->loadResult();
	}

	public static function GetCountrySelect($no, $id = '')
	{
		$db     = JFactory::getDBO();
		$query  = "select country_id, name from `#__extrareg_country`";
		$result = $db->setQuery($query);
		$cns    = $db->loadObjectList();
		$out    = "<select name='country_$no' id='country_$no' class='form-control' onchange='ajaxfunction(null, $no);'>";
		$out .= "<option value='0'></option>";
		$s = '';
		if (count($cns) > 0)
		{
			foreach ($cns as $cn)
			{
				if ($cn->country_id == $id)
				{
					$s = 'selected';
				}
				$out .= "<option value='$cn->country_id' $s>$cn->name</option>";
				if ($s != '')
				{
					$s = '';
				};
			}
		}
		$out .= "</select>";

		return $out;
	}

	public static function GetRegionSelect($no, $id = '', $country_id)
	{
		if ($country_id == '')
		{
			return '';
		}
		$db     = JFactory::getDBO();
		$query  = "select region_id, name from `#__extrareg_region` where country_id = $country_id";
		$result = $db->setQuery($query);
		$cns    = $db->loadObjectList();
		$out    = "<select name='region_$no' id='region_$no' class='form-control' onchange='ajaxfunction(1, $no);'>";
		//$out .= "<option value='0'></option>";
		$s = '';
		if (count($cns) > 0)
		{
			foreach ($cns as $cn)
			{
				if ($cn->region_id == $id)
				{
					$s = 'selected';
				}
				$out .= "<option value='$cn->region_id' $s>$cn->name</option>";
				if ($s != '')
				{
					$s = '';
				};
			}
		}
		$out .= "</select>";

		return $out;
	}

	public static function GetCitySelect($no, $id = '', $region_id)
	{
		if ($region_id == '')
		{
			return '';
		}
		$db     = JFactory::getDBO();
		$query  = "select city_id, name from `#__extrareg_city` where region_id = $region_id";
		$result = $db->setQuery($query);
		$cns    = $db->loadObjectList();
		$out    = "<select name='field_$no' id='field_$no'>";
		$s      = '';
		if (count($cns) > 0)
		{
			foreach ($cns as $cn)
			{
				if ($cn->city_id == $id)
				{
					$s = 'selected';
				}
				$out .= "<option value='$cn->city_id' $s>$cn->name</option>";
				if ($s != '')
				{
					$s = '';
				};
			}
		}
		$out .= "</select>";

		return $out;
	}

	public static function GetUserList2($uid = '', $fieldname = 'uid')
	{
		$db     = JFactory::getDBO();
		$query  = 'select * from `#__users` order by name';
		$result = $db->setQuery($query);
		$rows   = $db->loadObjectList();
		$out    = '';
		$out .= "<select name='$fieldname' id='$fieldname'>";
		if (count($rows) > 0)
		{
			foreach ($rows as $row)
			{
				if ($row->id == $uid)
				{
					$s = 'selected';
				}
				else
				{
					$s = '';
				}
				$out .= "<option value='$row->id' $s>$row->name</option>";
			}
		}
		$out .= '</select>';

		return $out;
	}

	public static function Tooltip($text)
	{
		return "<img src='/media/com_extraregistration/images/help.png' onmouseover=\"tooltip.show('$text');\" onmouseout=\"tooltip.hide();\">";
	}
}