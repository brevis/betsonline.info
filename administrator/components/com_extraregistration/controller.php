<?php defined('_JEXEC') or die( 'Restricted access' );
/**
 * @package		Extra Registration
 */
jimport( 'joomla.application.component.controller' );

# For compatibility with older versions of Joola 2.5
if (!class_exists('JControllerLegacy')){
    class JControllerLegacy extends JController {

    }
}

class ExtraregistrationController extends JControllerLegacy
{
	function display($cachable = false, $urlparams = false)
	{
		$task = JFactory::getApplication()->input->get('task');
		if (in_array($task, array('filter', 'filters')))
		{
			$view = JFactory::getApplication()->input->getCmd('view', 'filters');
			JFactory::getApplication()->input->set('view', $view);

			parent::display($cachable, $urlparams);

			return $this;
		}
		else
		{
			parent::display(true);
		}
	}
}
