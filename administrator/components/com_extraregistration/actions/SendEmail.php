<?php
defined('_JEXEC') or die;

class ActionSendEmail
{
	public function doAction($filterId)
	{
		$jinput = JFactory::getApplication()->input;

		$filter = ComExtraregistrationHelper::getFilterById($filterId);
		if (!is_object($filter))
		{
			return JText::_('COM_EXTRAREGISTRATION_FILTER_NOT_FOUND');
		}

		$users = ComExtraregistrationHelper::getUsersByFilterId($filter->id);
		if (!is_array($users) || count($users) < 1)
		{
			return JText::_('COM_EXTRAREGISTRATION_FILTER_HAS_NO_USERS');
		}

		$subject = $jinput->getString('subject');
		$body    = $jinput->get('body', '', 'RAW');
		$body    = nl2br($body);

		foreach ($users as $user)
		{
			$this->sendEmail($user->email, $subject, $this->processBody($body, $user));
		}

		return JText::_('COM_EXTRAREGISTRATION_FILTER_ACTION_DONE');
	}

	/*-----------------------------------------------------------------------------*/

	protected function processBody($body, $user)
	{
		foreach ($user as $field => $value)
		{
			$body = preg_replace('/\%' . preg_quote($field) . '\%/Uis', $value, $body);
		}

		return $body;
	}

	protected function sendEmail($email, $subject, $body)
	{
		$config = JFactory::getConfig();
		$mailer = JFactory::getMailer();
		$mailer->isHTML(true);
		$mailer->Encoding = 'base64';
		$mailer->setSender($config->get('mailfrom'));
		$mailer->addRecipient($email);
		$mailer->setSubject($subject);
		$mailer->setBody($body);

		$mailer->Send();
	}
}