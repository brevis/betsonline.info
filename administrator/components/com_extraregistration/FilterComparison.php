<?php
defined('_JEXEC') or die('Restricted access');

class FilterComparison
{
	const EQUALS     = 1;
	const NOT_EQUALS = 2;
	const LIKE       = 3;
	const MORE_THAN  = 4;
	const LESS_THAN  = 5;
	const RANGE      = 6;

	public static function all()
	{
		$refl = new ReflectionClass('FilterComparison');
		return $refl->getConstants();
	}
}