<?php defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');
jimport('joomla.application.component.helper');

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_extraregistration'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

/*
    $controller = new ExtraregistrationController();
    $controller->execute(JRequest::getVar('task', null, 'default', 'cmd'));
*/

JLoader::registerPrefix('Extraregistration', JPATH_COMPONENT_ADMINISTRATOR);

$task       = JFactory::getApplication()->input->get('task');
$controller = JControllerLegacy::getInstance('Extraregistration');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();