<?php

defined('_JEXEC') or die('Restricted access');

include_once (JPATH_ROOT . '/components/com_billing/useraccount.php');
include_once (JPATH_ROOT . '/components/com_billing/account.php');


Class BillingPlugin 
{
	var $pluginfile = '';
	var $xmlfile = '';
	var $name = '';
	var $description = '';
	var $author = '';
	var $version = '';
	var $pluginclass = '';
	var $amount = 0.0;
	var $instructions = '';
	var $id = 0;
	var $currency_id = 0;
	var $acc_type_id = 0;
	var $uid = 0;
	var $needphone = false;
	var $phone = '';
	
	var $login = '';
	var $email = '';
	var $curr = '';
	var $pass1 = '';
	var $pass2 = '';
	var $res_url = '';
	var $success_url = '';
	var $fail_url = '';
	var $comission = '';
	var $ps_comission = '';
	var $st_comission = '';
	var $testmode = false;
	
	var $ext1 = '';
	var $ext2 = '';
	var $ext3 = '';
	var $ext4 = '';
	var $ext5 = '';
	var $ext6 = '';
	var $ext7 = '';
	var $ext8 = '';
	var $ext9 = '';
	var $ext10 = '';

	var $login_label = '';
	var $email_label= '';
	var $curr_label= '';
	var $pass1_label= '';
	var $pass2_label= '';
	var $res_url_label= '';
	var $success_url_label= '';
	var $fail_url_label= '';
	var $comission_label = '';
	var $ps_comission_label = '';
	var $st_comission_label = '';
	var $testmode_label = '';

	var $ext1_label = '';
	var $ext2_label = '';
	var $ext3_label = '';
	var $ext4_label = '';
	var $ext5_label = '';
	var $ext6_label = '';
	var $ext7_label = '';
	var $ext8_label = '';
	var $ext9_label = '';
	var $ext10_label = '';
	
	var $login_enabled = false;
	var $email_enabled = false;
	var $curr_enabled = false;
	var $pass1_enabled = false;
	var $pass2_enabled = false;
	var $res_url_enabled = false;
	var $success_url_enabled = false;
	var $fail_url_enabled = false;
	var $comission_enabled = false;
	var $ps_comission_enabled = true;
	var $st_comission_enabled = true;
	var $testmode_enabled = false;
	
	var $ext1_enabled = false;
	var $ext2_enabled = false;
	var $ext3_enabled = false;
	var $ext4_enabled = false;
	var $ext5_enabled = false;
	var $ext6_enabled = false;
	var $ext7_enabled = false;
	var $ext8_enabled = false;
	var $ext9_enabled = false;
	var $ext10_enabled = false;
	
	function BillingPlugin($pname)
	{
		$lang =  JFactory::getLanguage();
		$lang->load('com_billing', JPATH_ADMINISTRATOR);
		
		$this->name = $pname;
		$this->xmlfile = JPATH_ROOT . '/components/com_billing/plugins/' . $pname . '/install.xml';
		
		$xml = JFactory::getXML($this->xmlfile);
		{
			$this->name = $xml->name;
			$this->description = $xml->description;
			$this->author = $xml->author;
			$this->pluginfile = $xml->pluginfile;
			$this->version = $xml->version;
			$this->pluginclass = $xml->pluginclass;
			
			foreach($xml->params->param as $param)
			{
				foreach($param->attributes() as $title => $value) 
				{
					if($title == 'name')
					{
						$param_name = $value;
					}	
					if($title == 'enabled')
					{
						$param_enabled = $value;
					}	
				}
				
				$param_text = $param[0];				
				
				switch ($param_name)
				{
					case 'login':
						$this->login_label = $param_text;
						$this->login_enabled = $param_enabled;
					break;
					case 'email':
						$this->email_label = $param_text;
						$this->email_enabled = $param_enabled;
					break;
					case 'curr':
						$this->curr_label = $param_text;
						$this->curr_enabled = $param_enabled;
					break;
					case 'pass1':
						$this->pass1_label = $param_text;
						$this->pass1_enabled = $param_enabled;
					break;
					case 'pass2':
						$this->pass2_label = $param_text;
						$this->pass2_enabled = $param_enabled;
					break;
					case 'res_url':
						$this->res_url_label = $param_text;
						$this->res_url_enabled = $param_enabled;
					break;
					case 'success_url':
						$this->success_url_label = $param_text;
						$this->success_url_enabled = $param_enabled;
					break;
					case 'fail_url':
						$this->fail_url_label = $param_text;
						$this->fail_url_enabled = $param_enabled;
					break;
					case 'comission':
						$this->comission_label = $param_text;	
						$this->comission_enabled = $param_enabled;
					break;
					case 'ps_comission':
						$this->ps_comission_label = '';
						$this->ps_comission_enabled = true;
					break;
					case 'st_comission':
						$this->st_comission_label = '';
						$this->st_comission_enabled = true;
					break;
					case 'testmode':
						$this->testmode_label = $param_text;	
						$this->testmode_enabled = $param_enabled;
					break;
					case 'ext1':
						$this->ext1_label = $param_text;	
						$this->ext1_enabled = $param_enabled;
					break;
					case 'ext2':
						$this->ext2_label = $param_text;	
						$this->ext2_enabled = $param_enabled;
					break;
					case 'ext3':
						$this->ext3_label = $param_text;	
						$this->ext3_enabled = $param_enabled;
					break;
					case 'ext4':
						$this->ext4_label = $param_text;	
						$this->ext4_enabled = $param_enabled;
					break;
					case 'ext5':
						$this->ext5_label = $param_text;	
						$this->ext5_enabled = $param_enabled;
					break;
					case 'ext6':
						$this->ext6_label = $param_text;	
						$this->ext6_enabled = $param_enabled;
					break;
					case 'ext7':
						$this->ext7_label = $param_text;	
						$this->ext7_enabled = $param_enabled;
					break;
					case 'ext8':
						$this->ext8_label = $param_text;	
						$this->ext8_enabled = $param_enabled;
					break;
					case 'ext9':
						$this->ext9_label = $param_text;	
						$this->ext9_enabled = $param_enabled;
					break;
					case 'ext10':
						$this->ext10_label = $param_text;	
						$this->ext10_enabled = $param_enabled;
					break;
				}				
			}
		}
		
		$db = JFactory::getDBO();
	    $query = "select count(*) from `#__billing_settings` where paysystem = '$this->name'";
		$result = $db->setQuery($query);
		$result = $db->loadResult();
		if ($result > 0)
		{
			$query = "select * from `#__billing_settings` where paysystem = '$this->name'";
			$result = $db->setQuery($query);
			$plg = $db->loadAssoc();
			$this->login = $plg['login'];
			$this->email = $plg['email'];
			$this->curr = $plg['curr'];
			$this->pass1 = $plg['pass1'];
			$this->pass2 = $plg['pass2'];
			$this->comission = $plg['data1'];
			$this->ps_comission = $plg['data2'];
			$this->st_comission = $plg['data3'];
			$this->testmode = $plg['option1'];
			$this->res_url = $plg['res_url'];
			$this->success_url = $plg['success_url'];
			$this->fail_url = $plg['fail_url'];
			$this->instructions = $plg['instructions'];
			$this->id = $plg['id'];
			$this->ext1 = $plg['ext1'];
			$this->ext2 = $plg['ext2'];
			$this->ext3 = $plg['ext3'];
			$this->ext4 = $plg['ext4'];
			$this->ext5 = $plg['ext5'];
			$this->ext6 = $plg['ext6'];
			$this->ext7 = $plg['ext7'];
			$this->ext8 = $plg['ext8'];
			$this->ext9 = $plg['ext9'];
			$this->ext10 = $plg['ext10'];
			$this->currency_id = $plg['currency_id'];
			$this->acc_type_id = $plg['acc_type_id'];			
		}
		else
		{
			$this->currency_id = GetDefaultCurrency();
		}
	}
	
	function Log($txt)
	{
		BillingLogMessage('Billing Plugin', $this->name, $txt);
	}
	
    function GetParam($param)  
    {
           $db = JFactory::getDBO();
           $query = "select * from `#__billing_settings` where paysystem = '$pname'";
           $result = $db->setQuery($query);   
           $row = $db->loadAssoc();                              
           
           return $row[$param];
    }
	
	function GetOption($option)
	{
		if(isset($_SESSION[$option]))
		{
			return $_SESSION[$option];
		}
		$db = JFactory::getDBO();
		$query = "select value from `#__billing_options` where `option` = '$option'";
		$result = $db->setQuery($query);  
		$result = $db->loadResult();
		$_SESSION[$option] = $result;
		return $result;
	}
	
	function Init()
	{
		$this->currency_id = $this->GetCurrency();
		$this->acc_type_id = $this->GetAcc();
		return false;
	}

	function StartPayment($amnt, $uid = '')
	{
		$this->amount = $amnt;
		return false;
	}
	
	function Process()
	{
		return false;
	}

	function Pending()
	{
		return false;
	}
	
	function TableExists($table)
	{
		$db = JFactory::getDBO();
		$db->setQuery("SHOW TABLES LIKE '$table'");
		$n = $db->loadResult();
		if($n != '')
		{
			$this->Log("$table exists");
			return true;
		}
		else
		{
			$this->Log("$table does not exists");
			return false;
		}
	}
	
	function GetPartnerIp()
	{
		 if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
		 {
		   $ip=$_SERVER['HTTP_CLIENT_IP'];
		 }
		 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		 {
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		 }
		 else
		 {
		   $ip=$_SERVER['REMOTE_ADDR'];
		 }
		 return $ip;
	}

	function Success($redirect = true)
	{
		if($this->GetOption('notify'))
		{
			$this->EmailToAdmin($this->uid, true);
		}
		if($this->GetOption('afterpay') != '')
		{
			$url = $this->GetOption('afterpay');
		}
		else
		{
			$url = JURI::base() . 'index.php?option=com_billing&view=billing';
		}
		$this->Log('Check payment');
		$UA = new UserAccount();
		if($UA->ExtensionExists('extraregistration'))
		{
			$this->Log("Extra registration enabled");
			$db = JFactory::getDBO();
			$query = "select value from `#__extrareg_options` where `option` = 'integration'";
			$result = $db->setQuery($query);   
			$integration = $db->loadResult();
			$query = "select value from `#__extrareg_options` where `option` = 'payreg'";
			$result = $db->setQuery($query);   
			$payreg = $db->loadResult();
			
			$this->Log("Registration: $payreg and $integration");
			
			if($payreg and $integration)
			{
				$this->Log("Integration and payment checked $this->uid");
				$query = "select value from `#__extrareg_options` where `option` = 'pricetype'";
				$result = $db->setQuery($query);   
				$pricetype = $db->loadResult();
			
				$query = "select * from `#__users` where id = $this->uid";
				$result = $db->setQuery($query);   
				$user = $db->loadAssoc();
				
				$this->Log("Registration:" . $user['block'] . ' ' . $user['activation']);
				
				if($user['block'] == 1 and $user['activation'] != '')
				{
					if($pricetype == 1)
					{
						$sid = $user['activation'];
						$query = "select price from `#__billing_subscription_type` where id = $sid";
						$result = $db->setQuery($query);   
						$price = $db->loadResult();
						if($this->amount == $price)
						{							
							$days = $UA->GetSubscriptionDays($sid, $price);
							$UA->AddSubscription($this->uid, $days, $sid);
							$UA->UnlockUser($this->uid);
							$this->Log("Registration fee: $this->amount");
							$descr = JText::_('COM_BILLING_REGPARTNER_PAY');
							
							$partner_id = $_COOKIE['partner_id'];
							if($partner_id == '')
							{
								$query = "select partner_id from `#__extrareg_partners` where uid = $this->uid";
								$result = $db->setQuery($query);   
								$partner_id = $db->loadResult();
							}
							$this->Log('COOKIE[partner_id]: ' . $partner_id);
							if ($partner_id != '')
							{
								$partner_id = (int)$partner_id;
								$this->Log('Trying to add user partner. id '. $partner_id);
							
								$date = date('Y-m-d H:i:s');
								$ip = $this->GetPartnerIp();
								$url = getenv("HTTP_REFERER");
								$percent = $this->GetOption('partnerpercent');
								if ($percent == '') {$percent = 0;}
								$payment = $this->GetOption('partnerpayment');
								if ($payment == '') {$payment = 0;}
							
								$query = "insert into `#__billing_partner` (uid, partner_id, percent, from_link, from_ip, enter_date) values($partner_id, $this->uid, $percent, '$url', '$ip', '$date')";
								$result = $db->setQuery($query);  
								$result = $db->query();
								/*
									А пригласил В
									в таблице 
									uid - А
									partner_id - B
								*/
								
								$this->Log('User partner added. Percent: ' . $percent);
							}	
							
							$UA->MLPercent($this->uid, $price, $descr);

						}
						else
						{
							$this->Log("Registration fee: $this->amount diff $price");
						}
					}
					else
					{
						$query = "select value from `#__extrareg_options` where `option` = 'price'";
						$result = $db->setQuery($query);   
						$price = $db->loadResult();
						if($this->amount == $price)
						{
							$UA = new UserAccount();
							$pid = date('His'). $this->uid;
							$descr = JText::_('COM_BILLING_REG_PAY');
							$UA->WithdrawMoney($this->uid, $price, $pid, $descr);
							$UA->UnlockUser($this->uid);
							$this->Log("Registration fee: $this->amount");
							$descr = JText::_('COM_BILLING_REGPARTNER_PAY');
							
							$partner_id = $_COOKIE['partner_id'];
							if($partner_id == '')
							{
								$query = "select partner_id from `#__extrareg_partners` where uid = $this->uid";
								$result = $db->setQuery($query);   
								$partner_id = $db->loadResult();
							}
							$this->Log('COOKIE[partner_id]: ' . $partner_id);
							if ($partner_id != '')
							{
								$partner_id = (int)$partner_id;
								$this->Log('Trying to add user partner. id '. $partner_id);
							
								$date = date('Y-m-d H:i:s');
								$ip = $this->GetPartnerIp();
								$url = getenv("HTTP_REFERER");
								$percent = $this->GetOption('partnerpercent');
								if ($percent == '') {$percent = 0;}
								$payment = $this->GetOption('partnerpayment');
								if ($payment == '') {$payment = 0;}
							
								$query = "insert into `#__billing_partner` (uid, partner_id, percent, from_link, from_ip, enter_date) values($partner_id, $this->uid, $percent, '$url', '$ip', '$date')";
								$result = $db->setQuery($query);  
								$result = $db->query();
								/*
									А пригласил В
									в таблице 
									uid - А
									partner_id - B
								*/
								
								$this->Log('User partner added. Percent: ' . $percent);
							}	
							$UA->MLPercent($this->uid, $price, $descr);
						}
						else
						{
							$this->Log("Registration fee: $this->amount diff $price");
						}
					}
				}
			}
		}
		
		if($redirect)
		{
			$msg = JText::_('COM_BILLING_SUCCESS_PAYMENT');
			$app = JFactory::getApplication();
			$app->redirect($url, $msg);
		}
		return;
	}
	
	function Fail()
	{
		if($this->GetOption('notify'))
		{
			$this->EmailToAdmin($this->uid, false);
		}
		return false;
	}
	
	function EmailToAdmin($uid, $success = true)
	{
		if($uid == '')
		{
			return;
		}
		$lang =  JFactory::getLanguage();
		$lang->load('com_billing', JPATH_ADMINISTRATOR);
		$lang->load('com_billing');

		$db = JFactory::getDBO();
		$query = "select * from `#__users` where id = $uid";
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc(); 		
		
		if($success)
		{
			$message = JText::_('COM_BILLING_USER') . " $row[username] aka $row[name] (id: $uid) ".JText::_('COM_BILLING_ADD_MONEY_USER')." ". FormatCurrency($this->currency_id, $this->amount) ." via $this->name<br><br>" . JText::_('COM_BILLING_AUTO_MESSAGE');
			$subject = JText::_('COM_BILLING_ADD2_MONEY');
		}
		else
		{
			$message = JText::_('COM_BILLING_USER') . " $row[username] aka $row[name] (id: $uid) ".JText::_('COM_BILLING_NOT_ADD_MONEY_USER')." ". FormatCurrency($this->currency_id, $this->amount) ." via $this->name ".JText::_('COM_BILLING_NOT_ADD_MONEY_USER2')."<br><br>" . JText::_('COM_BILLING_AUTO_MESSAGE');
			$subject = JText::_('COM_BILLING_ADD3_MONEY');
		}
		$body = $message;
		$mode = 1;	
		
		EmailToAdmin($message, $subject);
		 
		return;
	}
	
	function SetCurrency($currency_id)
	{
		$db = JFactory::getDBO();
		$query = "update `#__billing_settings` set currency_id = $currency_id where paysystem = '$this->name'";
		$result = $db->setQuery($query);  
		$result = $db->query();
		$this->currency_id = $currency_id;
		return $result;
	}

	function GetCurrency()
	{
		$db = JFactory::getDBO();
		$query = "select currency_id from `#__billing_settings` where paysystem = '$this->name'";
		$result = $db->setQuery($query);  
		$result = $db->loadResult();
		return $result;
	}

	function SetAcc($acc_type_id)
	{
		$db = JFactory::getDBO();
		$query = "update `#__billing_settings` set acc_type_id = $acc_type_id where paysystem = '$this->name'";
		$result = $db->setQuery($query);  
		$result = $db->query();
		$this->acc_type_id = $acc_type_id;
		return true;
	}

	function GetAcc()
	{
		$db = JFactory::getDBO();
		$query = "select acc_type_id from `#__billing_settings` where paysystem = '$this->name'";
		$result = $db->setQuery($query);  
		$result = $db->loadResult();
		return $result;
	}

	function GetBalance()
	{
		return 0;
	}
	
	function SendMoney($amount, $wallet, $ext1, $ext2, $ext3)
	{
		return false;
	}
}
?>	