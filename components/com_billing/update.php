<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

function GetBOption($option, $text = false, $default = '')
{
	$db = JFactory::getDBO();
	if($text)
	{
		$query = "select text_value from `#__billing_options` where `option` = '$option'";
	}
	else
	{
		$query = "select value from `#__billing_options` where `option` = '$option'";
	}
	$result = $db->setQuery($query);  
	$result = $db->loadResult();
	
	if($result == '')
	{
		$result = $default;
	}
	return $result;
}

function UpdateLogMessage($text)
{
	$db = JFactory::getDBO();
    $query = "select value from `#__billing_options` where `option` = 'logenabled'";
	$result = $db->setQuery($query);  
	$ok = $db->loadResult();

	if ($ok)
	{
		$type = 'Billing DB update'; 
		$name = 'Database'; 
		$user = JFactory::getUser();
		$uid = $user->id;
		$rt = date('Y-m-d H:i:s');
		$query = "insert into `#__billing_log` (`uid`, `messagetype`, `objectname`, `textmessage`, `description`, `record_time`) values ($uid, '$type', '$name', '$text', '', '$rt')";
		$result = $db->setQuery($query);   
		$row = $db->query();	
	}
}

function FieldExists($table, $field)
{
	$db = JFactory::getDBO();
	$db->setQuery("show columns FROM `$table` where `Field` = '$field'");
	$n = $db->loadResult();
	UpdateLogMessage($n);
	if($n != '')
	{
		return true;
	}
	else
	{
		return false;
	}
}

function _TableExists($table)
{
	$db = JFactory::getDBO();
	$db->setQuery("SHOW TABLES LIKE '$table'");
	$n = $db->loadResult();
	UpdateLogMessage($n);
	if($n != '')
	{
		return true;
	}
	else
	{
		return false;
	}
}

function UpdateBilling($ext = true)
{
	$db = JFactory::getDBO();
	
	if(!_TableExists('#__billing_articles_file') and $ext)
	{
		$db->setQuery("CREATE TABLE IF NOT EXISTS `#__billing_articles_file` ( 
		`id` int(11) NOT NULL AUTO_INCREMENT,       
		`article_id` int(11),
		`description` varchar(255),
		`filename` varchar(255),
		`newname` varchar(255),
		`fileurl` varchar(255),
		`filesize` int,
		`add_date` datetime,
		PRIMARY KEY (`id`) ) ENGINE=MyISAM CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';");
		$n = $db->query();
	}
	
	if(!_TableExists('#__billing_services_user') and $ext)
	{
		$db->setQuery("CREATE TABLE IF NOT EXISTS `#__billing_services_user` ( 
		`id` int(11) NOT NULL AUTO_INCREMENT,       
		`service_id` int(11),
		`state` int,
		`uid` int,
		`paymentdate` date,
		`servicedate` date,
		`enddate` datetime NOT NULL,
		`price` float,
		`payment_id` varchar(250),  
		`data1` varchar(250),
		`data2` varchar(250),
		`data3` varchar(250),
		`data4` varchar(250),
		`data5` varchar(250),
		PRIMARY KEY (`id`) ) ENGINE=MyISAM CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';");
		$n = $db->query();
	}
	
	if(!_TableExists('#__billing_personal_tariff') and $ext)
	{
		$db->setQuery("CREATE TABLE IF NOT EXISTS `#__billing_personal_tariff` ( 
        `id` int(11) NOT NULL AUTO_INCREMENT,
		`enddate` date,
		`uid` int,
		`is_active` int default 1,
		`price` float,
		`data1` varchar(250),
		`data2` varchar(250),
		`data3` varchar(250),
		`data4` varchar(250),
		`data5` varchar(250),
		PRIMARY KEY (`id`) ) ENGINE=MyISAM CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';");
		$n = $db->query();
	}
	
	if(!_TableExists('#__billing_project') and $ext)
	{
		$db->setQuery("CREATE TABLE IF NOT EXISTS `#__billing_project` ( 
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`is_active` int default 1,
		`project_name` varchar(255),
		`description` TEXT,
		`file1` varchar(255),
		`file2` varchar(255),
		`file3` varchar(255),
		`created_by` int,
		`start_date` datetime,
		`finish_date` datetime,
		`project_sum` float,
		PRIMARY KEY (`id`) ) ENGINE=MyISAM CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';");
		$n = $db->query();
	}
	
	if(!_TableExists('#__billing_project_user') and $ext)
	{
		$db->setQuery("CREATE TABLE IF NOT EXISTS `#__billing_project_user` ( 
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`project_id` int,
		`uid` int,
		`psum` float,
		`send_date` datetime,
		`comment` varchar(250),
		PRIMARY KEY (`id`) ) ENGINE=MyISAM CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';");
		$n = $db->query();
	}
	
	if(!_TableExists('#__billing_pin_type') and $ext)
	{
		$db->setQuery("CREATE TABLE IF NOT EXISTS `#__billing_pin_type` (
		`id` int(11) NOT NULL AUTO_INCREMENT,  
		`pin_name`	varchar(250),
		`is_active` int default 1,	
		`pin_type` int,
		`license` TEXT,
		`instructions` TEXT,
		`price` float,
		`php_entry` varchar(250),
		`php_class` varchar(250), 
		`php_filename` varchar(250), 
		`best_before` datetime,
		`comment` varchar(250),
		`data1` varchar(250),
		`data2` varchar(250),
		`data3` varchar(250),
		`data4` varchar(250),
		`data5` varchar(250),
		PRIMARY KEY (`id`) ) ENGINE=MyISAM CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';");
		$n = $db->query();
	}	
	
	if(!_TableExists('#__billing_pin') and $ext)
	{
		$db->setQuery("CREATE TABLE IF NOT EXISTS `#__billing_pin` (
		`id` int(11) NOT NULL AUTO_INCREMENT,  
		`pin_type_id` int,
		`pin_code` varchar(255),
		`pin_text` TEXT,
		`activated` int default 0,
		`activation_date` datetime,
		`uid` int,
		`operation_id` int,
		`ext_data` varchar(255),
		`item_id` varchar(255),
		`price` float,
 		PRIMARY KEY (`id`) ) ENGINE=MyISAM CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';");
		$n = $db->query();
	}		
	
	if(!_TableExists('#__billing_subscription_list') and $ext)
	{	
	    $db->setQuery("CREATE TABLE IF NOT EXISTS `#__billing_subscription_list` ( 
        `id` int(11) NOT NULL AUTO_INCREMENT,
		`is_active` int default 1,
		`subscription_id` int,
		`title` varchar(250),
		`price` float,
		`description` text,
		`data1` varchar(250),
		`data2` varchar(250),
		`data3` varchar(250),
		`data4` varchar(250),
		`data5` varchar(250),
		PRIMARY KEY (`id`) ) ENGINE=MyISAM CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';");
		$n = $db->query();
	}

	if(!_TableExists('#__billing_group_category') and $ext)
	{	
		$db->setQuery("CREATE TABLE IF NOT EXISTS `#__billing_group_category` (
		`id` int(11) NOT NULL AUTO_INCREMENT,       
		`group_id` int,
		`category_id` int,
		PRIMARY KEY (`id`) ) ENGINE=MyISAM CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';");
		$n = $db->query();
	}

	if(!_TableExists('#__billing_sub_group') and $ext)
	{	
		$db->setQuery("CREATE TABLE IF NOT EXISTS `#__billing_sub_group` (
		`id` int(11) NOT NULL AUTO_INCREMENT,       
		`group_id` int,
		`sub_id` int,
		PRIMARY KEY (`id`) ) ENGINE=MyISAM CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';");
		$n = $db->query();
	}
	
	if(!_TableExists('#__billing_sub_history') and $ext)
	{	
		$db->setQuery("CREATE TABLE IF NOT EXISTS `#__billing_sub_history` (
		`id` int(11) NOT NULL AUTO_INCREMENT,	 
		`sub_id` int,
		`subscription_id` int,
		`uid` int,
		`payment_date` datetime,
		`payment_sum` float,
		`payment_id` varchar(250),
		`comment` text,
		`percent` float,
		`percent_sum` float,
		`author_uid` int,
		PRIMARY KEY (`id`) ) ENGINE=MyISAM CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';");
		$n = $db->query();
	}
	
	$db->setQuery("DROP TABLE IF EXISTS `#__billing_user_and_group_t`;");
	$n = $db->query();
	$db->setQuery("ALTER TABLE `#__billing` CHANGE data1 data1 varchar(1024);");
	$n = $db->query();

	if (!FieldExists('#__billing_updates', 'db_ver'))
	{
		$db->setQuery("ALTER TABLE `#__billing_updates` ADD `db_ver` varchar(10)");
		$n = $db->query();
	}
	
	if (!FieldExists('#__billing', 'waiting'))
	{
		$db->setQuery("ALTER TABLE `#__billing` ADD `waiting` int DEFAULT 0");
		$n = $db->query();
	}
	
	if (!FieldExists('#__billing_subscription', 'list_id'))
	{
		$db->setQuery("ALTER TABLE `#__billing_subscription` ADD `list_id` int");
		$n = $db->query();
	}
	
	if (!FieldExists('#__billing', 'item_id'))
	{
		$db->setQuery("ALTER TABLE `#__billing` ADD `item_id` bigint");
		$n = $db->query();
	}
	
	if (!FieldExists('#__billing', 'waiting_date'))
	{
		$db->setQuery("ALTER TABLE `#__billing` ADD `waiting_date` datetime");
		$n = $db->query();
	}

	if (!FieldExists('#__billing', 'waiting_doc'))
	{
		$db->setQuery("ALTER TABLE `#__billing` ADD `waiting_doc` varchar(250)");
		$n = $db->query();
	}

	if (!FieldExists('#__billing', 'waiting_comment'))
	{
		$db->setQuery("ALTER TABLE `#__billing` ADD `waiting_comment` varchar(250)");
		$n = $db->query();
	}

	if (!FieldExists('#__billing', 'waiting_transaction'))
	{
		$db->setQuery("ALTER TABLE `#__billing` ADD `waiting_transaction` varchar(250)");
		$n = $db->query();
	}
	
	if (!FieldExists('#__billing_articles', 'lockbefore') and $ext)
	{
		$db->setQuery("ALTER TABLE `#__billing_articles` ADD `lockbefore` datetime");
		$n = $db->query();
	}	

	if (!FieldExists('#__billing_services', 'lockbefore') and $ext)
	{
		$db->setQuery("ALTER TABLE `#__billing_services` ADD `lockbefore` datetime");
		$n = $db->query();
	}	
	
	if (!FieldExists('#__billing_services_user', 'servicedate') and $ext)
	{
		$db->setQuery("ALTER TABLE `#__billing_services_user` ADD `servicedate` date");
		$n = $db->query();
	}		
	
	if (!FieldExists('#__billing_settings', 'ext1'))
	{
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `ext1` varchar(250)");
		$n = $db->query();
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `ext2` varchar(250)");
		$n = $db->query();
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `ext3` varchar(250)");
		$n = $db->query();
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `ext4` varchar(250)");
		$n = $db->query();
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `ext5` varchar(250)");
		$n = $db->query();
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `ext6` varchar(250)");
		$n = $db->query();
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `ext7` varchar(250)");
		$n = $db->query();
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `ext8` varchar(250)");
		$n = $db->query();
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `ext9 varchar(250)");
		$n = $db->query();
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `ext10` varchar(250)");
		$n = $db->query();
	}	
	
	if (FieldExists('#__billing_options', 'option_name'))
	{
		$db->setQuery("DROP TABLE `#__billing_options`");
		$n = $db->query();
		$db->setQuery("CREATE TABLE IF NOT EXISTS `#__billing_options` (`id` int(11) NOT NULL AUTO_INCREMENT,".
		"`option` varchar(250), `value` varchar(250), `setdate` datetime, `uid` int,".
		"PRIMARY KEY (`id`) ) ENGINE=MyISAM CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'");
		$n = $db->query();
	}		

	if (!FieldExists('#__billing_currency', 'abbr3'))
	{
		$db->setQuery("ALTER TABLE `#__billing_currency` ADD `abbr3` varchar(3)");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_currency` ADD `data1` varchar(250)");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_currency` ADD `data2` varchar(250)");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_currency` ADD `data3` varchar(250)");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_currency` ADD `data4` varchar(250)");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_currency` ADD `data5` varchar(250)");
		$n = $db->query();	
	}

	if (!FieldExists('#__billing_currency', 'CashOut'))
	{
		$db->setQuery("ALTER TABLE `#__billing_currency` ADD `WMW` varchar(3)");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_currency` ADD `WMLETTER` varchar(1)");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_currency` ADD `CashOut` int default 0");
		$n = $db->query();	
	}
	
	if (!FieldExists('#__billing_point_actions', 'actual') and $ext)
	{
		$db->setQuery("ALTER TABLE `#__billing_point_actions` ADD `actual` int default 1");
		$n = $db->query();
		$db->setQuery("ALTER TABLE `#__billing_point_actions` ADD `points` int");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_point_actions` ADD `enabled` int");
		$n = $db->query();	
	}
	
	if (!FieldExists('#__billing_settings', 'currency_id'))
	{
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `currency_id` int");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `acc_type_id` int");
		$n = $db->query();	
	}

	if (!FieldExists('#__billing_services_user', 'enddate') and $ext)
	{
		$db->setQuery("ALTER TABLE `#__billing_services_user` ADD `enddate` datetime");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_services_user` ADD `payment_id` int");
		$n = $db->query();	
	}	
	
	if (!FieldExists('#__billing_subscription', 'froze_date'))
	{
		$db->setQuery("ALTER TABLE `#__billing_subscription` ADD `froze_date` date");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_subscription` ADD `frozen` int default 0");
		$n = $db->query();	
	}

	if (!FieldExists('#__billing_settings', 'logo'))
	{
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `logo` varchar(255)");
		$n = $db->query();	
	}
	
	if (!FieldExists('#__billing_services_user', 'comments'))
	{
		$db->setQuery("ALTER TABLE `#__billing_services_user` ADD `comments` text");
		$n = $db->query();	
	}

	if (!FieldExists('#__billing_settings', 'cashout'))
	{
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `cashout` int default 0");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `cashout_instructions` text");
		$n = $db->query();	
	}
	if (!FieldExists('#__billing_settings', 'cashout_title'))
	{
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `cashout_title` varchar(255)");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_settings` ADD `list_title` varchar(255)");
		$n = $db->query();	
	}

	if (!FieldExists('#__billing_options', 'text_value'))
	{
		$db->setQuery("ALTER TABLE `#__billing_options` ADD `text_value` text");
		$n = $db->query();		
	}

	if (!FieldExists('#__billing_articles', 'counter'))
	{
		$db->setQuery("ALTER TABLE `#__billing_articles` ADD `counter` int");
		$n = $db->query();		
		$db->setQuery("ALTER TABLE `#__billing_articles` ADD `sales` int");
		$n = $db->query();		
	}

	if (!FieldExists('#__billing_articles', 'k2'))
	{
		$db->setQuery("ALTER TABLE `#__billing_articles` ADD `k2` int default 0");
		$n = $db->query();		
		$db->setQuery("ALTER TABLE `#__billing_articles_sms` ADD `k2` int default 0");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_articles_user` ADD `k2` int default 0");
		$n = $db->query();	
	}
	
	if (!FieldExists('#__billing_subscription', 'start_date'))
	{
		$db->setQuery("ALTER TABLE `#__billing_subscription` ADD `start_date` datetime");
		$n = $db->query();		
		$db->setQuery("ALTER TABLE `#__billing_subscription` ADD `last_payment` datetime");
		$n = $db->query();	
		$db->setQuery("ALTER TABLE `#__billing_subscription` ADD `last_payment_sum` float");
		$n = $db->query();	
	}
	
	UpdateTabs();
	
	/*
	$db->setQuery("CREATE INDEX IF NOT EXISTS uid_idx ON `#__billing` (uid);");
	$n = $db->query();	
	$db->setQuery("CREATE INDEX IF NOT EXISTS val_idx ON `#__billing` (val);");
	$n = $db->query();	
	$db->setQuery("CREATE INDEX IF NOT EXISTS waiting_idx ON `#__billing` (waiting);");
	$n = $db->query();	
	*/
}	

function AddBillingData($ext = true)
{
	$db = JFactory::getDBO();

	$db->setQuery("SELECT count(*) FROM `#__billing_currency`");
	$n = $db->loadResult();
	if ($n == 0)
	{
		$db->setQuery("insert into `#__billing_currency` (curency_name, abbreviation, is_base, is_active, before_money, base_rate, abbr3, WMW, WMLETTER, CashOut) ".
			" values ('Российский рубль', 'руб.', 1, 1, 0, 1, 'RUR', 'WMR', 'R', 1)");
		$db->query();
		$db->setQuery("insert into `#__billing_currency` (curency_name, abbreviation, is_base, is_active, before_money, base_rate, abbr3, WMW, WMLETTER, CashOut) ".
			" values ('Доллар США', '$', 0, 1, 1, 30, 'USD', 'WMZ', 'Z', 0)");
		$db->query();
		$db->setQuery("insert into `#__billing_currency` (curency_name, abbreviation, is_base, is_active, before_money, base_rate, abbr3, WMW, WMLETTER, CashOut) ".
			" values ('Евро', 'EUR', 0, 1, 0, 40, 'EUR', 'WME', 'E', 0)");
		$db->query();
		$db->setQuery("insert into `#__billing_currency` (curency_name, abbreviation, is_base, is_active, before_money, base_rate, abbr3, WMW, WMLETTER, CashOut) ".
			" values ('Гривна', 'гр', 0, 1, 0, 3.7, 'UAH', 'WMU', 'U', 0)");
		$db->query();
		$db->setQuery("insert into `#__billing_currency` (curency_name, abbreviation, is_base, is_active, before_money, base_rate, abbr3, WMW, WMLETTER, CashOut) ".
			" values ('Белорусский рубль', 'руб.', 0, 1, 0, 0.03, 'BYR', 'WMB', 'B', 0)");
		$db->query();
		$db->setQuery("insert into `#__billing_currency` (curency_name, abbreviation, is_base, is_active, before_money, base_rate, abbr3, WMW, WMLETTER, CashOut) ".
			" values ('Узбекский сум', 'сум', 0, 1, 0, 0.016, 'UZS', 'WMY', 'Y', 0)");
		$db->query();
		$db->setQuery("insert into `#__billing_currency` (curency_name, abbreviation, is_base, is_active, before_money, base_rate, abbr3, WMW, WMLETTER, CashOut) ".
			" values ('Казахский тенге', 'тенге', 0, 1, 0, 0.2, 'KZT', '', '', 0)");
		$db->query();
		$db->setQuery("insert into `#__billing_currency` (curency_name, abbreviation, is_base, is_active, before_money, base_rate, abbr3, WMW, WMLETTER, CashOut) ".
			" values ('Bitcoin', 'BTC', 0, 1, 0, 30000, 'BTC', '', '', 0)");
		$db->query();
	}
	$db->setQuery("SELECT count(*) FROM `#__billing_currency` where id = 10000");
	$n = $db->loadResult();
	if ($n == 0)
	{
		$db->setQuery("insert into `#__billing_currency` (id, curency_name, abbreviation, is_base, is_active, before_money, base_rate, abbr3) ".
			" values (10000, 'Баллы', 'баллов', 0, 1, 0, 0.01, '')");
		$db->query();
	}

	$db->setQuery("SELECT count(*) FROM `#__billing_point_actions`");
	$n = $db->loadResult();
	if ($n == 0 and $ext)
	{
		$db->setQuery("insert into `#__billing_point_actions` (`action_name`, `description`, `points`, `enabled`, `data1`) ".
			" values ('Регистрация', 'Регистрация на сайте', 10, 0, 'register')");
		$db->query();	
		$db->setQuery("insert into `#__billing_point_actions` (`action_name`, `description`, `points`, `enabled`, `data1`) ".
			" values ('Вход на сайт', 'Вход на сайт', 1, 0, 'enter')");
		$db->query();	
		$db->setQuery("insert into `#__billing_point_actions` (`action_name`, `description`, `points`, `enabled`, `data1`) ".
			" values ('Оплата статьи', 'Оплата статьи', 30, 0, 'payart')");
		$db->query();	
		$db->setQuery("insert into `#__billing_point_actions` (`action_name`, `description`, `points`, `enabled`, `data1`) ".
			" values ('Оплата подписки', 'Оплата подписки', 100, 0, 'paysub')");
		$db->query();	
		$db->setQuery("insert into `#__billing_point_actions` (`action_name`, `description`, `points`, `enabled`, `data1`) ".
			" values ('Добавление статьи', 'Добавление статьи пользователем', 50, 0, 'addart')");
		$db->query();	
		$db->setQuery("insert into `#__billing_point_actions` (`action_name`, `description`, `points`, `enabled`, `data1`) ".
			" values ('Пополнение счета', 'Пополнение счета пользователем', 50, 0, 'addmoney')");
		$db->query();	
		$db->setQuery("insert into `#__billing_point_actions` (`action_name`, `description`, `points`, `enabled`, `data1`) ".
			" values ('Заказ услуги', 'Заказ услуги пользователем', 70, 0, 'payservice')");
		$db->query();	
		$db->setQuery("insert into `#__billing_point_actions` (`action_name`, `description`, `points`, `enabled`, `data1`) ".
			" values ('Привлечение партнера', 'Привлечение партнера', 30, 0, 'addpartner')");
		$db->query();	
		$db->setQuery("insert into `#__billing_point_actions` (`action_name`, `description`, `points`, `enabled`, `data1`) ".
			" values ('Покупка партнером', 'Привлеченный партнер потратил деньги', 50, 0, 'paypartner')");
		$db->query();	
	}
	
	//$db->setQuery("update `#__billing_tabs` set is_active = 0 where tabname = 'accountstab'";
	//$db->query();	
	$db->setQuery("delete from `#__billing_tabs` where tabname = 'accountstab'");
	$db->query();	
}	
	
function UpdateTabs()
{
	$db = JFactory::getDBO();
	$query = "select count(*) from `#__billing_tabs` where id < 0";
	$result = $db->setQuery($query); 
	$n = $db->loadResult();
	if($n == 0)
	{
		$baltab = (int)GetBOption('baltab', '', 1);
		$addtab = (int)GetBOption('addtab', '', 1);
		$subscr = (int)GetBOption('subscr', '', 1);
		$files = (int)GetBOption('files', '', 0);
		$music = (int)GetBOption('music', '', 0);
		$articles = (int)GetBOption('articles', '', 0);
		$partner_tab = (int)GetBOption('partner_tab', '', 0);
		$accountstab = (int)GetBOption('accountstab', '', 0);
		$projectstab = (int)GetBOption('projectstab', '', 0);
		$userpointstab = (int)GetBOption('userpointstab', '', 0);
		$service = (int)GetBOption('service', '', 0);
	
		$query = "insert into `#__billing_tabs` (id, is_active, tabname, tabname_langconst, taborder) values (-1, $baltab, 'baltab', 'baltab', 1);";		
		$result = $db->setQuery($query); $result = $db->query(); 
		$query = "insert into `#__billing_tabs` (id, is_active, tabname, tabname_langconst, taborder) values (-2, $addtab, 'addtab', 'addtab', 2);";
		$result = $db->setQuery($query); $result = $db->query(); 
		$query = "insert into `#__billing_tabs` (id, is_active, tabname, tabname_langconst, taborder) values (-3, $subscr, 'subscr', 'subscr', 3);";
		$result = $db->setQuery($query); $result = $db->query(); 
		$query = "insert into `#__billing_tabs` (id, is_active, tabname, tabname_langconst, taborder) values (-4, $files, 'files', 'files', 4);";
		$result = $db->setQuery($query); $result = $db->query(); 
		$query = "insert into `#__billing_tabs` (id, is_active, tabname, tabname_langconst, taborder) values (-5, $music, 'music', 'music', 5);";
		$result = $db->setQuery($query); $result = $db->query(); 
		$query = "insert into `#__billing_tabs` (id, is_active, tabname, tabname_langconst, taborder) values (-6, $articles, 'articles', 'articles', 6);";
		$result = $db->setQuery($query); $result = $db->query(); 
		$query = "insert into `#__billing_tabs` (id, is_active, tabname, tabname_langconst, taborder) values (-7, $partner_tab, 'partner_tab', 'partner_tab', 7);";
		$result = $db->setQuery($query); $result = $db->query(); 
		$query = "insert into `#__billing_tabs` (id, is_active, tabname, tabname_langconst, taborder) values (-8, $accountstab, 'accountstab', 'accountstab', 8);";
		$result = $db->setQuery($query); $result = $db->query(); 
		$query = "insert into `#__billing_tabs` (id, is_active, tabname, tabname_langconst, taborder) values (-9, $projectstab, 'projectstab', 'projectstab', 9);";
		$result = $db->setQuery($query); $result = $db->query(); 
		$query = "insert into `#__billing_tabs` (id, is_active, tabname, tabname_langconst, taborder) values (-10, $userpointstab, 'userpointstab', 'userpointstab', 10);";
		$result = $db->setQuery($query); $result = $db->query(); 
		$query = "insert into `#__billing_tabs` (id, is_active, tabname, tabname_langconst, taborder) values (-11, $service, 'service', 'service', 11);";
		$result = $db->setQuery($query); $result = $db->query(); 
	}
}

?>