<?php

defined('_JEXEC') or die('Restricted access');

include_once (JPATH_ROOT . '/components/com_billing/useraccount.php');
include_once (JPATH_ROOT . '/components/com_billing/account.php');

Class TabPlugin 
{
	var $pluginfile = '';
	var $xmlfile = '';
	var $name = '';
	var $tabname = '';
	var $description = '';
	var $author = '';
	var $version = '';
	var $pluginclass = '';
	var $price = 0;
	var $period = 0;
	var $instructions = '';
	var $id = 0;

		
	function TabPlugin($pname)
	{
		$this->name = $pname;
		$this->xmlfile = JPATH_ROOT . '/components/com_billing/tabs/' . $pname . '/install.xml';
		
		$xml = JFactory::getXML($this->xmlfile);
        if ($xml)
		{
            $this->name = $xml->name[0];
            $this->tabname = $xml->tabname[0];
            $this->description = $xml->description[0];
            $this->author = $xml->author[0];
            $this->pluginfile = $xml->pluginfile[0];
            $this->version = $xml->version[0];
            $this->pluginclass = $xml->pluginclass[0];
		}

		$db = JFactory::getDBO();
	    $query = "select count(*) from `#__billing_tabs` where tabname = '$this->name'";
		$result = $db->setQuery($query);
		$result = $db->loadResult();
		if ($result > 0)
		{
			$query = "select * from `#__billing_tabs` where tabname = '$this->name'";
			$result = $db->setQuery($query);
			$plg = $db->loadAssoc();
			$this->price = $plg['access_price'];
			$this->period = $plg['access_period_days'];
			$this->instructions = $plg['instructions'];
		}
		$this->Init();
	}
	
	function Log($txt)
	{
		BillingLogMessage('Tab Plugin', $this->name, $txt);
	}
	
    function GetParam($param)  
    {
	   $db = JFactory::getDBO();
	   $query = "select * from `#__billing_tabs` where tabname = '$pname'";
	   $result = $db->setQuery($query);   
	   $row = $db->loadAssoc();                              
	   
	   return $row[$param];
    }	
	
	function Show()
	{
		
	}
	
	function OnLoad()
	{
		// runs before Show() method
	}
	
	function Init()
	{
		
	}

	function ProcessAction($action)	
	{
		
	}

	function TabOptions()
	{
		return '';
	}

	function SaveOptions()
	{
		return false;
	}	
}
?>