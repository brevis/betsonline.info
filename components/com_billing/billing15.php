<?php
defined('_JEXEC') or die('Restricted access');
//define ("BILLING_EXT", true);

include_once 'useraccount.php';
include_once 'billing.min.php';
include_once 'account.php';
include_once 'billing.func.php';
include_once 'tabs.php';
include_once 'pluginfactory.php';

if(BILLING_EXT)
{
	if(file_exists(JPATH_ROOT . '/components/com_billing/userpoints.php'))
	{
		include_once 'userpoints.php';
	}
	if(file_exists(JPATH_ROOT . '/components/com_billing/billing.ext.php'))
	{
		include_once 'billing.ext.php';
	}
}

	if (JoomlaVersion() != '1.5')
	{
		define("tick", JURI::root() . 'components/com_billing/images/tick.png');
		define("publish_x", JURI::root() . 'components/com_billing/images/publish_x.png');
		define("publish_y", JURI::root() . 'components/com_billing/images/publish_y.png');
		define("default", JURI::root() . 'components/com_billing/images/icon-16-default.png');
		define("notdefault", JURI::root() . 'components/com_billing/images/icon-16-notdefault.png');
		define("filesave", JURI::root() . 'components/com_billing/images/filesave.png');
		define("icon-16-add", JURI::root() . 'components/com_billing/images/icon-16-add.png ');
	}


$document = JFactory::getDocument();
$document->addStyleSheet('/components/com_billing/tooltip_style.css');
$document->addScript('/components/com_billing/tooltip.js');
$document->addStyleSheet('/components/com_billing/custom.css');

jimport('joomla.utilities.xmlelement');
jimport( 'joomla.factory' );
jimport( 'joomla.filesystem.archive' );

function Cabinet($tabno = 0, $msg = '')
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	$db = JFactory::getDBO();

	$UA = new UserAccount;
	$balance = $UA->GetBalance($uid);
	
	if($msg != '')
	{
		$msg = urldecode($msg);
		JFactory::getApplication()->enqueueMessage( $msg );
	}

	/*
	echo "<h1 class='billing_h1'>" . GetOption('title') . "</h1>";
	echo "<div class='billing_info'>";
	echo GetOption('info');
	echo "</div>";
	*/
	
	$subscr = GetOption('subscr');
	$files = GetOption('files');
	$music = GetOption('music');
	$articles = GetOption('articles');
	$partner_tab = GetOption('partner_tab');
	$service = GetOption('service');
	$accountstab = GetOption('accountstab');	
	$useaccounts = GetOption('useaccounts');
	$userpointstab = GetOption('userpointstab');
	$baltab = GetOption('baltab');
	$addtab = GetOption('addtab');
	$projectstab = GetOption('projectstab');
	$exchange = GetOption('exchange');
	$slider = GetOption('slider'); 
	$copyright = GetOption('copyright');
	$page = JRequest::getInt('page', 0);
		
	if(!BILLING_EXT)
	{
		$baltab = true;
		$addtab = true;
		//$copyright = true;
	}
	
	$tabs = '';
	$BillingTabs = new BillingTabs();
	$tabs .= $BillingTabs->StartTabs($tabno);

	//if(BILLING_EXT)
	//{
		// include plugins
		$query = "select * from `#__billing_tabs` where is_active = 1 order by taborder";
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList(); 
		if (count($rows) > 0)
		{
			$i = 0;
			foreach ( $rows as $row ) 
			{
				$show = GetOption('showgrouptab_'.$row->id);
				if($show)
				{
					$go = false;
					$grp = GetOption('grouptab_'.$row->id);
					$query = "select count(*) from `#__user_usergroup_map` where user_id = $uid and group_id in ($grp)";
					$result = $db->setQuery($query);   
					$cnt = $db->loadResult();
					if($cnt == 0)
					{
						continue;
					}
				}
			
				$tab_name = GetOption('tab_name'.$row->id);
				if($row->id > 0)
				{
					$tabname = $row->tabname;
					$tabname_lang = $row->tabname_langconst;
					$php_class = $row->php_class;
					$php_entry = $row->php_entry;
					if($tab_name != '')
					{
						$tabname_lang = $tab_name;
					}
					else
					{
						$tabname_lang = $row->tabname_langconst;
					}
					$tabs .= $BillingTabs->StartTab($tabname_lang, 'tab'.$i);
					$i = $i +1;

					/*require_once(JPATH_ROOT . '/components/com_billing/tabs/' . $tabname . '/' . $php_entry);
					
					eval('$Plugin = new '. $php_class . '("'.$tabname.'");');
					$Plugin->OnLoad();*/
					$Plugin = BillingPluginFactory::GetTabPlugin($tabname, $php_class, $php_entry);
					$tabs .= $Plugin->Show();
					$tabs .= $BillingTabs->EndTab();
				}
				else
				{
					$pane = $BillingTabs;
					switch($row->tabname)
					{
						case 'baltab':
							$tabs .= GetBalanceTab($uid, $balance, $pane, $UA, $page);
						break;
						case 'addtab':
							$tabs .= GetAddTab($uid, $balance, $pane);
						break;
						case 'subscr':
							if(BILLING_EXT)
							{
								$tabs .= GetSubscrTab($uid, $pane);
							}
						break;
						case 'files':
							if(BILLING_EXT)
							{
								$tabs .= GetFilesTab($uid, $pane);
							}
						break;
						case 'music':
							if(BILLING_EXT)
							{
								$tabs .= GetMusicTab($uid, $pane);
							}
						break;
						case 'articles':
							if(BILLING_EXT)
							{
								$tabs .= GetArticlesTab($uid, $pane);
							}
						break;
						case 'partner_tab':
							if(BILLING_EXT)
							{
								$tabs .= GetPartnerTab($uid, $pane);
							}
						break;
						case 'accountstab':
							if(BILLING_EXT and $useaccounts)
							{
								//$tabs .= GetAccountsTab($uid, $pane);
							}
						break;
						case 'projectstab':
							if(BILLING_EXT)
							{
								$tabs .= GetProjectsTab($uid, $pane);
							}
						break;
						case 'userpointstab':
							if(BILLING_EXT)
							{
								$tabs .= GetUserPointsTab($uid, $pane);
							}
						break;
						case 'service':
							if(BILLING_EXT)
							{
								$tabs .= GetServiceTab($uid, $pane);
							}
						break;
					}
				}
			}
		}
	//}
		
	$tabs .= $BillingTabs->EndTabs();	
	
	if($copyright)
	{
		$tabs .= "<p style='font-size:xx-small; color:#999999;' align='center'><a href='http://www.joomlaplus.ru' style='color:#999999;'>Joomla Billing</a> - Powered by JoomlaPlus.ru</p>";
	}	

	echo $tabs;	

}

$task = JRequest::getCmd('task');
$k2 = JRequest::getCmd('k2');
if($k2 == 1)
{
	$k2 = true;
}
else
{
	$k2 = false;
}
$url = JRequest::getCmd('url');
$sum = JRequest::getFloat('sum');
$view = JRequest::getCmd('view');
$days = JRequest::getInt('days');
$tabno = JRequest::getInt('tabno', 0);
$id = JRequest::getCmd('id');
$hash = JRequest::getCmd('fn');
$pid = JRequest::getCmd('pid');
$acc_id = JRequest::getCmd('acc_id');
$msg = JRequest::getVar('msg');
$k2 = JRequest::getBool('k2', false);

	if (($task == 'payarticle' or $task == 'payart') and $view == 'base')
	{
		PayArticle($id, $k2);
		return;
	}

$partner = JRequest::getVar('partner');
$partnername = JRequest::getVar('partnername');
$rd = urldecode(JRequest::getVar('rd'));
if($partner == '')
{
	$partner = JRequest::getVar('partner_id', '', 'get');
}

if($partner != '')
{
	setcookie('partner_id', $partner, time() + 3600 * 24 * 14, '/');
	if($rd != '')
	{
		header ("Location: $rd");
	}	
	$url = GetOption('redirect');
	if ($url != '')
	{
		header ("Location: $url");
		return;
	}
	else
	{
		header ("Location: index.php");
		return;
	}	
}
if($partnername != '')
{
	$partnername = urldecode($partnername);
	$db = JFactory::getDBO();
    $query = "select * from `#__users` where username = '$partnername'";
    $result = $db->setQuery($query);   
	$row = $db->loadAssoc();
	$partner = $row['id'];
	
	setcookie('partner_id', $partner, time() + 3600 * 24 * 14, '/');
	$url = GetOption('redirect');
	if($rd != '')
	{
		header ("Location: $rd");
	}		
	if ($url != '')
	{
		header ("Location: $url");
		return;
	}
	else
	{
		header ("Location: index.php");
		return;
	}	
}

/*
$lang = JRequest::getCmd('lang');
if($lang != '')
{
	$_SESSION['lang'] = $lang;
}
else
{
	$lang = $_SESSION['lang'];
	JRequest::setCmd('lang', $lang);
}
*/
//BillingLogMessage('System', 'Language', $lang);
$url_base = JURI::base() .'index.php?option=com_billing&view=billing';
$msgX = '';

switch ($task)
{
		case 'actart':
			ActArt($id);
			JFactory::getApplication()->redirect($url_base, $msgX);
			//Cabinet();
		break;
		case 'tabaction':
			TabAction();
			JFactory::getApplication()->redirect($url_base, $msg);
			//Cabinet($tabno, $msg);
		break;
		case 'show':
			Payment($sum, $id);
		break;
		case 'account':
			ShowAccount();
		break;
		case 'pay':
			SendPayment();
		break;
		case 'subscribe':
			SubscribeForm();
		break;
		case 'getsub':
			SubmitSubscription($id);
		break;
		case 'ok';
			ResultOK($pid);
		break;
		case 'fail';
			ResultFail($pid);
		break;
		case 'process';
			Process($pid);
		break;
		case 'pending';
			Pending($pid);
		break;
		case 'savesub':
			SaveSubscription($id);
			JFactory::getApplication()->redirect($url_base, $msg);
			//Cabinet($tabno, $msg);
		break;
		case 'payarticle':
			PayArticle($id, $k2);
		break;
		case 'payart':
			PayArticle($id, $k2);
		break;
		case 'confirm':
			Confirm($id);
		break;
		case 'ticket':
			Confirm2($id);
			ShowAccount();
		break;
		case 'viewsub':
			ViewSub($id);
		break;
		case 'partnerhistory':
			ShowPartnerHistory();
		break;
		case 'service':
			ShowService($id);
		break;
		case 'payservice':
			PayService($id);
		break;
		case 'checkpayment':
			CheckPayment();
		break;
		case 'coupon':
			SendCoupon();
		break;
		case 'addaccount':
			AddAccount($acc_id);
		break;
		case 'exchange':
			ExchangeUserPoints();
		break;
		case 'withdrawal':
			Cashout($sum);
		break;
		case 'movemoney':
			UserMoveMoney();
			JFactory::getApplication()->redirect($url_base, $msg);
			//Cabinet($tabno, $msg);
		break;
		case 'partner':
			setcookie('partner_id', $id, time() + 3600 * 24 * 14, '/');
			$url = GetOption('redirect');
			if ($url != '')
			{
				header ("Location: $url");
				return;
			}
			else
			{
				header ("Location: index.php");
				return;
			}
			
		break;
		case 'moneytoproject':
			MoneyToProject($id);
			JFactory::getApplication()->redirect($url_base, $msg);
			//Cabinet($tabno, $msg);
		break;
		case 'showproject':
			ShowProject($id);
		break;
		case 'addarticle':
			AddUserArticle();
			$msg = JText::_('COM_BILLING_ADDED_SUCCESS');
			//JFactory::getApplication()->redirect(JURI::base() .'index.php?option=com_billing&view=billing', $msg);
			JFactory::getApplication()->redirect($url_base, $msg);
			//Cabinet($tabno, $msg);
		break;
		case 'cancelcashout':
			CancelCashout($id);
		break;
		case 'getattach':
			GetAttach($id);
		break;
		case 'getpin':
			ProcessPin();
		break;
		case 'getfile':
			GetFile($id);
		break;
		case 'getfilek2':
			GetFileK2($hash);
		break;
		case 'maketariff':
			MakeTariff();
			JFactory::getApplication()->redirect($url_base, JText::_('COM_BILLING_TARIFF_UPDATED'));
			//Cabinet($tabno, JText::_('COM_BILLING_TARIFF_UPDATED'));
		break;
		case 'changesub':
			ChangeTariff($id);
		break;
		case 'changesub2':
			ChangeSub2($id);
		break;
		case 'froze':
			Froze($id);
			JFactory::getApplication()->redirect($url_base, $msg);
			//Cabinet($tabno, $msg);
		break;
		case 'prolong':
			ProlongTariff($id);
		break;
		case 'saveperstariff':
			SavePTariff();
			JFactory::getApplication()->redirect($url_base, $msg);
			//Cabinet($tabno,  JText::_('COM_BILLING_TARIFF_UPDATED'));
		break;
		case 'buypin':
			BuyPin($id);
		break;
		case 'rejectwait':
			RejectWait($id, $uid);
			JFactory::getApplication()->redirect($url_base, $msg);
		break;
		case 'vote':
			$res = VoteArticle($id);
			if($res)
			{
				$msg = JText::_('COM_BILLING_VOTE_SUCCESS');
			}
			JFactory::getApplication()->redirect("index.php?option=com_content&view=article&id=$id", $msg);
		break;
		case 'sendinvite':
			$x = SendInvite($id);
			JFactory::getApplication()->redirect($url_base, JText::_('COM_BILLING_INVITE_SENT'));
			//Cabinet($tabno, JText::_('COM_BILLING_INVITE_SENT'));
		break;
		case 'direct':
			DirectPayment($id, $sum);
		break;
		default: 

		;
}

$format = JRequest::getVar('format');

switch ($view)
{
		case 'account':
			ShowAccount();
		break;
		case 'pay':
			Payment($sum);
		break;
		case 'subscribe':
			SubscribeForm();
		break;
		case 'show':
			Show();
		break;
		case 'show2':
			Show2($id);
		break;
		case 'files':
			ShowFile();
		break;
		case 'mp3':
			ShowFile();
		break;
		case 'base':
		break;
		case 'partnerhistory':
			ShowPartnerHistory();
		break;
		case 'sales':
			UserSalesReport();
		break;
		case 'digital':
			ShowDigital();
		break;
		case 'services':
			ShowServices();
		break;
		case 'articles':
			ShowUserArticles();
		break;
		case 'userpoints':
			UserpointsView();
		break;
		case 'partner':
			PartnerView();
		break;
		case 'cabinet':
			if (JoomlaVersion() == '1.5')
			{
				Cabinet($tabno, $msg);
			}
		break;
		case 'billing':
			if($format != 'raw')
			{
				if (JoomlaVersion() == '1.5')
				{
					Cabinet($tabno, $msg);
				}
			}
		break;
		default:;
}

?>
