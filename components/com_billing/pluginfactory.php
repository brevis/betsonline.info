<?php
defined('_JEXEC') or die('Restricted access');

class BillingPluginFactory {

	static public function GetPaymentPlugin($paysystem, $php_class, $php_entry){
		
		require_once(JPATH_ROOT . '/components/com_billing/plugins/' . $paysystem . '/' . $php_entry);
	
		eval('$Plugin = new '. $php_class . '("'.$paysystem.'");');
		$Plugin->Init();
		return $Plugin;
	}
	
	static public function GetTabPlugin($tabname, $php_class, $php_entry) {
		
		require_once(JPATH_ROOT . '/components/com_billing/tabs/' . $tabname . '/' . $php_entry);
					
		eval('$Plugin = new '. $php_class . '("'.$tabname.'");');
		$Plugin->OnLoad();
		return $Plugin;
	}
	
	static public function GetPinPlugin($pin_type, $php_class, $php_entry, $php_filename) {
		
		require_once(JPATH_ROOT . '/components/com_billing/pins/' . $php_entry . '/' . $php_filename);
		
		eval('$Plugin = new '. $php_class . '('.$pin_type.');');
		return $Plugin;
	}
}

?>