<?php
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');
require_once(JPATH_ROOT . '/components/com_billing/useraccount.php');

/**
 * Billing Component Controller

 */
 if(!class_exists('JControllerLegacy'))
 {
	class JControllerLegacy extends JController
	{
	}
 }
 
	class BillingController extends JControllerLegacy
	{
		/**
		 * Method to display the view
		 *
		 * @access    public
		 */
		function display($cachable = false, $urlparams = array())
		{
			$view = JRequest::getCmd('view');
			
			if($view == 'base')
			{
				$this->execute( JRequest::getVar( 'task' ) );
			}
			else
			{
				parent::display(false);
			}
		}

	}
