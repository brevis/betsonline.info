<?php

defined('_JEXEC') or die('Restricted access');

include_once (JPATH_ROOT . '/components/com_billing/useraccount.php');
include_once (JPATH_ROOT . '/components/com_billing/account.php');

Class PinPlugin 
{
	var $pluginfile = '';
	var $name = '';
	var $pluginclass = '';
		
	function PinPlugin($ptype)
	{
		$db = JFactory::getDBO();
		$query = "select * from `#__billing_pin_type` where id = $ptype";
		$result = $db->setQuery($query);   
	    $row = $db->loadAssoc();
	
		$this->name = $row['php_entry'];
		$this->pluginclass = $row['php_class'];
		$this->pluginfile = $row['php_filename'];
		

		$this->Init();
	}
	
	function Init()
	{
		//
	}
	
	function Log($txt)
	{
		BillingLogMessage('Pin Plugin', $this->name, $txt);
	}
	
	function OnGetCode($code, $id)
	{
		return $code; 
	}
	
}
?>