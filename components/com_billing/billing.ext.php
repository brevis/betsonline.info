<?php
defined('_JEXEC') or die('Restricted access');

include_once 'useraccount.php';
include_once 'account.php';
include_once 'billing.func.php';
include_once 'userpoints.php';
include_once 'pluginfactory.php';

function GetK2ItemName($id)
{
	$db = JFactory::getDBO();
	$query = 'select title from `#__k2_items` where id = ' . $id;
	$result = $db->setQuery($query);   
	$result = $db->loadResult();
	return $result; 
}

function GetK2Categories($parent = 0)
{
	if(GetOption('catsk2') == '')
	{
		return '';
	}
	$out = '';
	$db = JFactory::getDBO();
	$query = "select * from `#__k2_categories` where parent = $parent and published = 1 and trash = 0 and id in (".GetOption('catsk2').") order by `name`";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList();
	if(count($rows) > 0)
	{
		foreach($rows as $row)
		{
			if($parent > 0)
			{
				$out .= "<option value='k2_$row->id'>- $row->name</option>\n";
			}
			else
			{
				$out .= "<option value='k2_$row->id'>$row->name</option>\n";
			}
			$query2 = "select * from `#__k2_categories` where parent = $row->id order by `name`";
			$result = $db->setQuery($query2);   
			$rows2 = $db->loadObjectList();
			if(count($rows) > 0)
			{
				$out .= GetK2Categories($row->id);
			}
		}
		return $out;
	}
	else
	{
		return '';
	}
}

function GetJSGroupName($id)
{
	$db = JFactory::getDBO();
	$db->setQuery("select `name` from `#__community_groups` where id = $id");
	return $db->loadResut();
}

function GetAdsManagerCategoryName($id)
{
	if(file_exists(JPATH_ROOT . '/components/com_adsmanager/adsmanager.php'))
	{
		$db = JFactory::getDBO();
		$query = "select `name` from `#__adsmanager_categories` where id = $id";
		$result = $db->setQuery($query);
		return $db->loadResult();
	}
	else
	{
		return "AdsManager not found";
	}
}

function GetK2CategoryName($id)
{
	$db = JFactory::getDBO();
	$db->setQuery("select * from `#__k2_categories` where id = $id");
	$x = $db->loadAssoc();
	return $x['name'];
}

function GetUserURLName($uid)
{
	$db = JFactory::getDBO();
    $query = 'select * from `#__users` where id = ' . $uid;
    $result = $db->setQuery($query);   
	$row = $db->loadAssoc();
	return urlencode($row['username']);
}

function GetUserNameX($uid)
{
	$db = JFactory::getDBO();
    $query = 'select * from `#__users` where id = ' . $uid;
    $result = $db->setQuery($query);   
	$row = $db->loadAssoc();
	return $row['name'];
}

function GetCustomItemName($row_id, $item_id)
{
	$db = JFactory::getDBO();
	$query = 'select * from `#__billing_subscription_access` where id = ' . $row_id;
    $result = $db->setQuery($query);   
	$row = $db->loadAssoc();
	$field_name = $row['file_path'];
	$id_name = $row['data3'];
	$table = $row['data4'];
	$query = "select $field_name from $table where $id_name = $item_id";
	$result = $db->setQuery($query);   
	return $db->loadResult();
}

function GetServiceName($id)
{
	$db = JFactory::getDBO();
    $query = 'select * from `#__billing_services` where id = ' . $id;
    $result = $db->setQuery($query);   
	$row = $db->loadAssoc();
	return $row['service_name'];
}

function GetCategoryList($id = '')
{
	$db = JFactory::getDBO();
	$query = "select distinct id, title from `#__categories` where published = 1 and extension = 'com_content' and id in (".GetOption('cats').") order by title";
	$result = $db->setQuery($query);   
    $rows = $db->loadObjectList();
    $out = '';
//    $out .= '<select class="b_select" name="category_id">';
	if(count($rows) > 0)
	{
		foreach ( $rows as $row )
		{
			if ($row->id == $id) {$s = 'selected';} else {$s = '';}
			$out .= "<option value='$row->id' $s>$row->title</option>"; 
		}
	}
//    $out .= '</select>';
    return $out;	
}

function GetLanguageList()
{
	if (JoomlaVersion() <= '1.7')
	{
		return '';
	}
	$db = JFactory::getDBO();
	$query = "select * from `#__languages` where published = 1 order by title";
	$result = $db->setQuery($query);   
    $rows = $db->loadObjectList();
    $out = '';
    $out .= '<select class="b_select" name="lang_id">';
	$out .= "<option value=''>".JText::_('JALL_LANGUAGE')."</option>"; 
	if(count($rows) > 0)
	{		
		foreach ( $rows as $row )
		{
			$out .= "<option value='$row->lang_code'>$row->title</option>"; 
		}
	}
    $out .= '</select>';
    return $out;	
}

function GetPartnerSum($id)
{
	$db = JFactory::getDBO();
    $query = "select sum(val) from `#__billing_partner_history` where partner_id = " . $id;
    $result = $db->setQuery($query);   
	$result = $db->loadResult();
	return $result; 	
}

function GetCategoryName($id)
{
      $db = JFactory::getDBO();
      $query = 'select title from `#__categories` where id = ' . $id;
      $result = $db->setQuery($query);   
	  $result = $db->loadResult();
	  return $result; 
}

function GetTitle($id)
{
      $db = JFactory::getDBO();
      $query = 'select title from `#__content` where id = ' . $id;
      $result = $db->setQuery($query);   
	  $result = $db->loadResult();
	  return $result; 
}

function SubDate($uid, $sid, $list_id = 0)
{
	$db = JFactory::getDBO();
	$query = "select count(*) from `#__billing_subscription` where uid = $uid and subscription_id = $sid and list_id = $list_id";
	$result = $db->setQuery($query);   
	$n = $db->loadResult();
	if ($n == 0)
	{
		return '';
	}
	else
	{
		$query = "select * from `#__billing_subscription` where uid = $uid and subscription_id = $sid and list_id = $list_id";
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc();
		return $row['enddate'];
	}
}

function SubName($id)
{
	$db = JFactory::getDBO();
	$query = 'select * from `#__billing_subscription_type` where id = ' . $id;
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();
	$subscription_type = $row['subscription_type'];
	return $subscription_type;
}

function SubscribeForm()
{
	$uid = CheckLogin();
	if ($uid == 0 or $uid == '') return;
	
	$discount = GetUserGroupDiscount($uid);
	
    $db = JFactory::getDBO();
    $query = 'select * from `#__billing_subscription_type` where is_active = 1';
    $result = $db->setQuery($query);   
    $rows = $db->loadObjectList(); 
	if (count($rows) > 0)
	{
		echo '<table width="100%" class="adminlist table billing_table">';
		echo "<thead>";
		echo "  <th class='b_title'>".JText::_('COM_BILLING_TITLE')."</th>";
		echo "  <th class='b_title'>".JText::_('COM_BILLING_DAYS')."</th>";
		echo "  <th class='b_title'>".JText::_('COM_BILLING_COST')."</th>";
		echo "  <th class='b_title'>".JText::_('COM_BILLING_ACTIONS')."</th>";
		echo "  <th class='b_title'>".JText::_('COM_BILLING_EXPIRES')."</th>";
		echo "</thead>";
		if(count($rows) > 0)
		{
			foreach ( $rows as $row ) 
			{
				$subdate = SubDate($uid, $row->id);
				echo '<tr>';
				echo "<td>$row->subscription_type</td>";
				echo "<td>$row->subscription_days</td>";
				if ($discount > 0 and $row->price > 0)
				{
					$price = $row->price -  $row->price/100 * $discount;
					$price = round($price, 2);
					echo "<td>".FormatDefaultCurrency($price)." (".JText::_('COM_BILLING_DISCOUNT')." $discount %)</td>";
				}
				else
				{
					echo "<td>".FormatDefaultCurrency($row->price)."</td>";
				}
				if ($subdate == '')
				{
					echo "<td><a class='billing_a' href='".JURI::base() ."index.php?option=com_billing&task=getsub&id=$row->id&view='>".JText::_('COM_BILLING_SUBSCRIBE')."</a></td>";
				}
				else
				{
					echo "<td><a class='billing_a' href='".JURI::base() ."index.php?option=com_billing&task=getsub&id=$row->id&view='>".JText::_('COM_BILLING_EXTEND')."</a></td>";
				}
				//$subdate2 = date('Y-m-d' , $subdate);
				$now = date('Y-m-d');
				if ($now > $subdate)
				{
					$style = "style='font-color: red;'";
				}
				else
				{
					$style = '';
				}
				echo "<td $style>". FormatBillingDate($subdate) ."</td>";
				echo '</tr>';
			}
		}
		echo "</table>";
	}
	else
	{
		echo '<p class="billing_p" align="center">'.JText::_('COM_BILLING_NO_SUBSCRIPTIONS').'</p>';
	}

}

function SubmitSubscription($id)
{
	$uid = CheckLogin();
	if ($uid == 0) return;
	$list_id = JRequest::getInt('list_id');
	$sub_comment = JRequest::getVar('sub_comment');
	
	$discount = GetUserGroupDiscount($uid);
	
	$db = JFactory::getDBO();
	$query = 'select * from `#__billing_subscription_type` where id = ' . $id;
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();
	$subscription_type = $row['subscription_type'];
	$days = $row['subscription_days'];
	$price = $row['price'];
	if ($discount > 0 and $row['price'] > 0)
	{
		$price = $row['price'] - $row['price'] / 100 * $discount;
		$price = round($price, 2);
	}
	$subscription_description = $row['subscription_description'];
	$alarm_text = $row['alarm_text'];
	$alarm_1 = $row['alarm_1'];
	$alarm_2 = $row['alarm_2'];
	$alarm_3 = $row['alarm_3'];
	
	
	$UA = new UserAccount;
	$bal = $UA->GetBalanceEx($uid, 0);
	if ($bal < $price and $price > 0)
	{
		echo '<p class="billing_p">'. JText::_('COM_BILLING_YOU_NEED_MONEY') .'. ' . JText::_('COM_BILLING_YOU_NEED_MONEY2') . ' ' . 
			 FormatDefaultCurrency($price) . ' <a class="billing_a" href="'.JURI::base() .'index.php?option=com_billing&view=pay">'.JText::_('COM_BILLING_ADD_MONEY').'</a></p>';
		return;
	}
	echo '<p class="billing_p" align="center"> '.JText::_('COM_BILLING_AT_YOUR_ACCOUNT') . ' <strong>' . FormatDefaultCurrency($bal) . 
		' </strong> '.JText::_('COM_BILLING_YOU_CAN').' <a class="billing_a" href="'.JURI::base() .'index.php?option=com_billing&view=pay">'.JText::_('COM_BILLING_ADD_MONEY').'</a></p>';
	echo "<p class='billing_p' align='center'>".JText::_('COM_BILLING_EXTEND_SUB')." '$subscription_type'</p> <br>";
	echo $row['subscription_description'];
	echo "<div align='center'><div style='margin:0 auto; text-align: center;'><form method='post' action='".JURI::base() ."index.php?option=com_billing&task=savesub&id=$id&view='>";
	echo JText::_('COM_BILLING_FOR_DAYS') . " <select class='select' name='days'>";
	
	
	$query = 'select * from `#__billing_subscription_period` where subscription_id = ' . $id . ' order by days';
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 	
	if (count($rows) > 0 )
	{
		foreach ( $rows as $row ) 
		{
			if ($discount > 0 and $row->price > 0)
			{
				$xprice = $row->price - $row->price / 100 * $discount;
				$xprice = round($xprice, 2);
				echo "<option value='". $row->days ."'>". $row->days." ".JText::_('COM_BILLING_DAYS')." - ". FormatDefaultCurrency($xprice) ." (".JText::_('COM_BILLING_DISCOUNT')." $discount %)</option>";
			}
			else
			{
				echo "<option value='". $row->days ."'>". $row->days." ".JText::_('COM_BILLING_DAYS')." - ". FormatDefaultCurrency($row->price) ."</option>";
			}
		}
	}
	else
	{
		for($i=1; $i<13; $i++)
		{
			if ($bal < ($price * $i))
			{
				break;
			}
			echo "<option value='". ($days * $i) ."'>". ($days * $i)." ".JText::_('COM_BILLING_DAYS')." - ". FormatDefaultCurrency($price * $i) . "</option>";
		}
	}
	echo "</select> <br>"; //".JText::_('COM_BILLING_DAYS')."
	
	$query = 'select * from `#__billing_subscription_list` where subscription_id = ' . $id . ' order by title';
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 	
	if (count($rows) > 0 )
	{
		echo JText::_('COM_BILLING_OBJECT_TYPE') . ' ' ;
		echo "<select class='select' name='list_id'>";
		foreach($rows as $row)
		{
			$s = '';
			if($list_id = $row->id)
			{
				$s = 'checked';
			}
			echo "<option value='$row->id' $s>$row->title</option>";
		}
		echo "</select>";
	}
	else
	{
		echo "<input type='hidden' name='list_id' value='0'>";
	}
	echo "<input type='hidden' name='sub_comment' value='$sub_comment'>";
	echo '<br><br><input type="submit" value="'.JText::_('COM_BILLING_EXTEND').'">';
	echo "</form></div></div>";	
}

function SaveSubscription($id)
{
	$uid = CheckLogin();
	if ($uid == 0) return;
	
	$discount = GetUserGroupDiscount($uid);
	
	$days = JRequest::getInt('days');
	$list_id = JRequest::getInt('list_id');
	$sub_comment = JRequest::getVar('sub_comment');

	$db = JFactory::getDBO();
	$query = 'select * from `#__billing_subscription_type` where id = ' . $id;
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();
	$days2 = $row['subscription_days'];
	$price = $row['price'];
	$sname = $row['subscription_type'];
	if ($discount > 0 and $price > 0)
	{
		$price = $price - $price/ 100 * $discount;
		$price = round($price, 2);
	}

	$query = 'select count(*) from `#__billing_subscription_period` where subscription_id = ' . $id;
	$result = $db->setQuery($query);   
	$n = $db->loadResult();
	
	if ($n > 0 )
	{
		$query = 'select price from `#__billing_subscription_period` where subscription_id = ' . $id . ' and days = '. $days;
		$result = $db->setQuery($query);   
		$price2 = $db->loadResult();
	}
	else
	{
		$price2 = '';
	}
	if ($discount > 0 and $price2 > 0)
	{
		$price2 = $price2 - $price2 / 100 * $discount;
		$price2 = round($price2, 2);
	}
	if ($price2 != '')
	{
		$x = $price2;
	}
	else
	{
		if ($days2 == 0) {return;}	
		$x = ($days / $days2) * $price;
	}
	
	$UA = new UserAccount;
	$bal = $UA->GetBalanceEx($uid, 0);

	if ($bal >= $x)
	{
		   $UA->AddSubscription($uid, $days, $id, false, $list_id, $sub_comment);
		   if(GetOption('sendsubnotif'))
		   {
				$message = JText::sprintf('COM_BILLING_SUB_NOTIF_TEXT', GetUserNameX($uid), $sname, $days, $price); 
				$subj = JText::_('COM_BILLING_SUB_NOTIF_SUBJ');
				EmailToAdmin($message, $subj);
		   }
		   return 1;
	}	
	else {
		echo '<p class="billing_p">'. JText::_('COM_BILLING_YOU_NEED_MONEY') .'. ' . JText::_('COM_BILLING_YOU_NEED_MONEY2') . ' ' . 
			 FormatDefaultCurrency($x) . ' <a class="billing_a" href="'.JURI::base().'index.php?option=com_billing&view=pay">'.JText::_('COM_BILLING_ADD_MONEY').
			 '</a>. <a class="billing_a" href="'.
			 JURI::base().
			 'index.php?option=com_billing&view=billing">'.
			 JText::_('COM_BILLING_BACK_TO_CABINET').
			 '</a></p>';
			 return 0;
	}
	
}	

function Show()
{
	$uid = CheckLogin();
	if ($uid == 0) return;
	$id = JRequest::getInt('id');
	
	$go = false;
	
	/*$m = JSite::getMenu();
	$menuitem =$m->getActive();
    $ini = $menuitem->params;
    $params = new JParameter( $ini );	
	$id = $params->get('material');
	$newdate = date('Y-m-d');*/
	
    $db = JFactory::getDBO();
    $query = 'select * from `#__billing_subscription_access` where item_id = '.$id.
		' and subscription_id in (select subscription_id from `#__billing_subscription` where uid = '.$uid.' and frozen = 0)';
    $result = $db->setQuery($query);   
    $rows = $db->loadObjectList(); 
	if (count($rows) > 0)
	{
	    foreach ( $rows as $row ) 
	    {
			$query = "select count(*) from `#__billing_subscription` where enddate > '$newdate' and subscription_id = $row->subscription_id and frozen = 0";
			$result = $db->setQuery($query);
			$n = $db->loadResult();
			if ($n > 0)
			{
				$go = true;
				$item_type = $row->item_type;
				break;
			}
		}
	}
	else
	{
		echo '<p class="billing_p" align="center">'.JText::_('COM_BILLING_YOU_ARE_NOT_COVERED').'</p>';
	}
	if ($go)
	{
		switch($item_type)
		{
			case 1:
				$URL = JURI::base() ."index.php?option=com_content&view=article&id=$id";
				header ("Location: $URL");
				break;
		}
	}
	else
	{
		
	}
}

function ShowFile()
{
	$uid = CheckLogin();
	if ($uid == 0) return;
	
	$go = false;
	
	/*$m = JSite::getMenu();
	$menuitem =$m->getActive();
    $ini = $menuitem->params;
    $params = new JParameter( $ini );	
	$filename = $params->get('protectedfile');
	if ($params->get('mp3') != '')
	{
		$filename = $params->get('mp3');
	}*/
	$url = JURI::base(). 'components/com_billing/files/' . $filename;
	$filename = JPATH_ROOT . '/components/com_billing/files/' . $filename;
	$newdate = date('Y-m-d');
	
    $db = JFactory::getDBO();
    $query = "select * from `#__billing_subscription_access` where file_path = '$filename' and subscription_id in (select subscription_id from `#__billing_subscription` where uid = $uid and frozen = 0)";
    $result = $db->setQuery($query);   
    $rows = $db->loadObjectList(); 
	if (count($rows) > 0)
	{
	    foreach ( $rows as $row ) 
	    {
			$description = $row->description;
			$query = "select count(*) from `#__billing_subscription` where enddate > '$newdate' and subscription_id = $row->subscription_id and frozen = 0";
			$result = $db->setQuery($query);
			$n = $db->loadResult();
			if ($n > 0)
			{
				$go = true;
				$item_type = $row->item_type;
				break;
			}
		}
	}
	else
	{
		echo '<p class="billing_p" align="center">'.JText::_('COM_BILLING_YOU_ARE_NOT_COVERED').'</p>';
	}
	if ($go)
	{
		echo '<p class="billing_p">'.JText::_('COM_BILLING_RESTRICTED').'</p>';
		switch($item_type)
		{
			case 2:
				echo "<p class='billing_p'>".JText::_('COM_BILLING_FILE_DESCRIPTION').": $description</p>";
				echo "<p class='billing_p' align='center'>".JText::_('COM_BILLING_FILE_URL').": <a class='billing_a' href='$url'>".JText::_('COM_BILLING_DOWNLOAD')."</a></p>";
			break;
			case 3:
				echo "<p class='billing_p'>".JText::_('COM_BILLING_FILE_DESCRIPTION').": $description</p>";
				echo GetMP3Player($url);
			break;
			case 5:
				echo "<p class='billing_p'>".JText::_('COM_BILLING_FILE_DESCRIPTION').": $description</p>";
				echo InsertPlayer($url);
			break;
		}
	}
	
}

function GetMP3Player($url)
{
$out =  '<!--[if !IE]> -->';
if (JoomlaVersion() == '1.5')
{
	$out .= '<object type="application/x-shockwave-flash" data="'.JURI::base().'modules/mod_simple_mp3_player/simple_mp3_player.swf" width="220" height="20">';
}
else
{
	$out .= '<object type="application/x-shockwave-flash" data="'.JURI::base().'modules/mod_simple_mp3_player/flashplayers/simple_mp3_player_rounded.swf" width="220" height="20">';
}
$out .= '<!-- <![endif]-->
<!--[if IE]>
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="220" height="20">';
if (JoomlaVersion() == '1.5')
{
	$out .= '<param name="movie" value="'.JURI::base().'modules/mod_simple_mp3_player/simple_mp3_player.swf" />';
}
else
{
	$out .= '<param name="movie" value="'.JURI::base().'modules/mod_simple_mp3_player/flashplayers/simple_mp3_player_rounded.swf" />';
}
$out .= '<!-->
<param name="bgcolor" value="#FFFFFF">
<param name="wmode" value="opaque">
<param name="FlashVars" value="mp3='.$url.'&amp;title=Example&amp;width=220&amp;height=100&amp;showinfo=1&amp;showvolume=1&amp;volume=75&amp;volumewidth=40&amp;volumeheight=8&amp;autoplay=0&amp;loop=0&amp;shuffle=0&amp;showloading=autohide&amp;loadingcolor=AFC400&amp;showlist=0&amp;showplaylistnumbers=0&amp;playlistcolor=323232&amp;playlistalpha=60&amp;showslider=1&amp;sliderwidth=6&amp;sliderheight=6&amp;slidercolor1=848484&amp;slidercolor2=555555&amp;sliderovercolor=E0E0DC&amp;bgcolor=FFFFFF&amp;bgcolor1=333333&amp;bgcolor2=000000&amp;textcolor=555555&amp;currentmp3color=AFC400&amp;buttonwidth=20&amp;buttoncolor=555555&amp;buttonovercolor=E0E0DC&amp;scrollbarcolor=848484&amp;scrollbarovercolor=E0E0DC">
</object><!-- <![endif]-->';

	return $out;
}

function InsertPlayer($url)
{
	$mediacode = "{avi}{/avi}";
	$output = '';
	if (JPluginHelper::importPlugin('content', 'avreloaded')) {
    $app =  JFactory::getApplication();
    $res = $app->triggerEvent('onAvReloadedGetVideo', array($mediacode));
    if (is_array($res) && (count($res) == 1)) {
        $output = $res[0];
    }
	return $output;
}
 
}

function PayArticle($id, $k2 = false)
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	$UA = new UserAccount;
	$res = $UA->AddArticleAccess($uid, $id, false, '', '', $k2);
	if ($res)
	{
		Show2($id, $k2);
	}
}

function Show2($id, $k2 = false)
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	if($k2)
	{
		$is_k2 = 1;
		$w_k2 = ' and k2 = 1';
	}
	else
	{
		$is_k2 = 0;
		$w_k2 = '';
	}
		
/*	$m = JSite::getMenu();
	$menuitem =$m->getActive();
	$ini = $menuitem->params;
    if(JoomlaVersion() > '2.5')
	{
		$params = json_decode($menuitem->params);
		$mid = $params->material;
	}
	else
	{
		$params = new JParameter( $ini );	
		$mid = $params->get('material');
	}	
	
	//$newdate = date('Y-m-d');
	
	if ($mid != '')
	{
		$id = $mid;
	}
*/	
	$db = JFactory::getDBO();

	$query = "select count(*) from `#__billing_articles_user` where article_id = $id and uid = $uid $w_k2";
	$result = $db->setQuery($query);
	$n = $db->loadResult();

	if ($n > 0)
	{
		if($k2)
		{
			$URL = JURI::base() . "index.php?option=com_k2&view=item&id=$id";
		}
		else
		{
			$URL = JURI::base() . "index.php?option=com_content&view=article&id=$id";
		}
		header ("Location: $URL");		
	}
	else
	{
		$query = "select * from `#__billing_articles` where article_id = $id $w_k2";
		$result = $db->setQuery($query);
		$row = $db->loadAssoc();
		$price = $row['price'];
		
		$s = '';
		
		$discount = GetUserGroupDiscount($uid);
		if ($discount > 0 and $price > 0)
		{
			$s = " (".JText::_('COM_BILLING_DISCOUNT')." $discount %)";
			$price = $price - $price / 100 * $discount;
			$price = round($price, 2);
		}
		
		echo $row['annotation'];
		echo '<p class="billing_p">'.JText::_('COM_BILLING_YOU_CAN_BUY').' '. FormatDefaultCurrency($price).  
		 $s .' <a class="billing_a" href="'.JURI::base().'index.php?option=com_billing&task=payart&id='.$id.'">'.JText::_('COM_BILLING_BUY_ACCESS').'</a></p>';
	}	
}

function GetUserGroups()
{
	$res = '';
	$uid = CheckLogin();
	$db = JFactory::getDBO();
	$query = "select * from `#__user_usergroup_map` where user_id = $uid";
	$result = $db->setQuery($query);   
    $rows = $db->loadObjectList(); 
	if (count($rows) > 0)
	{	
		foreach($rows as $key => $row)
		{
			if($res == '')
			{
				$res = $row->group_id;
			}
			else
			{
				$res .= ',' . $row->group_id;
			}
		}
	}
	return $res;
}

function GetSubscrTab($uid, $pane)
{
	$db = JFactory::getDBO();
	$discount = GetUserGroupDiscount($uid);
	$tabs = '';
	$tab_name = GetOption('tab_name-3');
	if($tab_name == '')
	{
		$tab_name = JText::_('COM_BILLING_SUBSCRIPTIONS');
	}

	$tabs .= $pane->StartTab($tab_name , 'subs');

    $query = 'select * from `#__billing_subscription_type` where is_active = 1';
    $result = $db->setQuery($query);   
    $rows = $db->loadObjectList(); 
	
	$usergroups = GetUserGroups();
	//FrontLog("User groups: $usergroups");
	$ug = explode(',', $usergroups);
	
	if (count($rows) > 0)
	{	
		foreach($rows as $key => $row)
		{
			$query = "select count(id) from `#__billing_sub_group` where sub_id = $row->id";
			$result = $db->setQuery($query);   
			if($db->loadResult() > 0)
			{
				FrontLog("Exists groups for: $row->id");
				$x = false;
				$query = "select * from `#__billing_sub_group` where sub_id = $row->id";
				$result = $db->setQuery($query); 
				$grps = $db->loadObjectList(); 
				if (count($grps) > 0)
				{
					FrontLog("Groups: " . print_r($grps, true));
					FrontLog("UG: " . print_r($ug, true));	
					foreach($grps as $grp)
					{
						if(in_array($grp->group_id, $ug))
						{
							$x = true;
							break;
						}
					}
				}
				FrontLog("X: $x");
				if($x != true)
				{
					FrontLog("Unset: $key");
					unset($rows[$key]);
				}
			}
		}

		$n = count($rows);
		$divs = '';
		$tabs .= "<form method='post' action='".JURI::base()."index.php?option=com_billing&task=getsub&view='>";
		$tabs .=  "<script>";
		$tabs .= "	var arr = []; ";
		$i = 1;
		foreach($rows as $row)
		{
			$tabs .= "arr[$i] = $row->id; ";
			$i++;
		}
		$tabs .= "</script>";
		$tabs .= "<select class='select' name='id' onchange='for(i=1; i<=arr.length;i++){if(arr[i] == this.value) {document.getElementById(\"div_\" + arr[i]).style.display = \"block\";} else {document.getElementById(\"div_\" + arr[i]).style.display = \"none\";}}'>";
		$first = true;
		foreach($rows as $row)
		{
			$xx = '';
			if ($discount > 0 and $row->price > 0)
			{
				$xp = $row->price - $row->price / 100 * $discount;
				$xp = round($xp, 2);
				$xx .= FormatDefaultCurrency($xp) ." (".JText::_('COM_BILLING_DISCOUNT')." $discount %)" .' - ' .$row->subscription_days . ' ' . JText::_('COM_BILLING_DAYS');
			}
			else
			{
				$xx .= FormatDefaultCurrency($row->price) .' - ' .$row->subscription_days . ' ' . JText::_('COM_BILLING_DAYS');
			}
			$tabs .= "<option value='$row->id'>$row->subscription_type ($xx)</option>";
			if($first)
			{
				$first = false;
				$divs .= "<div style='display: block;' id='div_$row->id'>$row->subscription_description</div>";
			}
			else
			{
				$divs .= "<div style='display: none;' id='div_$row->id'>$row->subscription_description</div>";
			}
		}
		$tabs .= "</select> <br> ".JText::_('COM_BILLING_ADD_YOUR_COMMENT').": <br><textarea name='sub_comment' cols='40' rows='3'></textarea><br><input type='submit' value='".JText::_('COM_BILLING_SUBSCRIBE')."'>";
		$tabs .= "</form><br>";
		$tabs .= $divs;
		$tabs .= '<br>';
		
		$query = "select count(*) from `#__billing_pin_type` where is_active = 1 and pin_type = 1";
		$result = $db->setQuery($query);   
		$enabled = $db->loadResult();
		if($enabled > 0)
		{
			$tabs .= '<p>'.JText::_('COM_BILLING_PIN_STR3'). ':</p>';
			$url = JURI::base() . "index.php?option=com_billing&task=getpin";
			$tabs .= "<form method='post' action='$url'>";
			$tabs .= "<input type='text' name='pin' size='50' value=''> <input type='submit' value='".JText::_('COM_BILLING_NEXT')."'>";
			$tabs .= '</form>';
		}
		
	}
	
	$tabs .=  '<h2>'. JText::_('COM_BILLING_MY_SUBSCRIPTIONS') . '</h2>';
	
	//$query = "select * from `#__billing_subscription_type` where is_active = 1 and id in (select subscription_id from `#__billing_subscription` where uid = $uid)";
	$query = "select s.*, ";
	$query .= "(select subscription_type from `#__billing_subscription_type` where id = s.subscription_id) as subscription_type, ";
	$query .= "(select subscription_days from `#__billing_subscription_type` where id = s.subscription_id) as subscription_days, ";
	$query .= "(select id from `#__billing_subscription_type` where id = s.subscription_id) as type_id, ";
	$query .= "(select price from `#__billing_subscription_type` where id = s.subscription_id) as price, ";
	$query .= "(select title from `#__billing_subscription_list` where id = s.list_id) as list ";
	$query .= " from `#__billing_subscription` s where uid = $uid order by s.id desc";
    $result = $db->setQuery($query);   
    $rows = $db->loadObjectList(); 
	if (count($rows) > 0)
	{
		$tabs .= '<table width="100%" class="adminlist table billing_table">';
		$tabs .= "<thead>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_TITLE')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_COMMENT')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_MATERIALS')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_DAYS')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_COST')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_ACTIONS')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_EXPIRES')."</th>";
		$tabs .= "</thead>";
		$UA = new UserAccount();
		if(count($rows) > 0)
		{
			foreach ( $rows as $row ) 
			{
				//определим кол-во оплаченных дней
				//$days = $row->selected_days;
				$days = round((strtotime($row->enddate)-strtotime($row->data1))/86400);
				$query = 'select count(*) from `#__billing_subscription_period` where subscription_id = ' . $row->id;
				$result = $db->setQuery($query);   
				$n = $db->loadResult();
				$price2 = 0;
				if ($n > 0 )
				{
					$query = "select price from `#__billing_subscription_period` where subscription_id = " . $row->type_id . " and days = '$days'";
					$result = $db->setQuery($query);   
					$price2 = $db->loadResult();
					FrontLog("price: $price2 q: $query");

				}

				$subdate = SubDate($uid, $row->type_id, $row->list_id);
				$tabs .= '<tr>';
				$tabs .= "<td>$row->subscription_type</td>";
				$tabs .= "<td>$row->data3</td>";
				$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=viewsub&id=$row->type_id'>".JText::_('COM_BILLING_MATERIALS')."</a></td>";
//				$tabs .= "<td>$row->subscription_days</td>";
				$tabs .= "<td>$days</td>";
				$price = $row->price;
				if ($price2 > 0) {
					$price = $price2;
				}
				if ($discount > 0 and $row->price > 0)
				{
					$xp = $price - $price / 100 * $discount;
					$xp = round($xp, 2);
					$tabs .= "<td> ". FormatDefaultCurrency($xp) ." (".JText::_('COM_BILLING_DISCOUNT')." $discount %)</td>";
				}
				else
				{
					$tabs .= "<td> ". FormatDefaultCurrency($price) ."</td>";
				}
				if ($subdate == '' or $subdate < date('Y-m-d'))
				{
					$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=getsub&id=$row->type_id&list_id=$row->list_id&view='>".JText::_('COM_BILLING_SUBSCRIBE')."</a></td>";
				}
				else
				{
					$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=getsub&id=$row->type_id&list_id=$row->list_id&view='>".JText::_('COM_BILLING_EXTEND')."</a> <br>";
					if(!GetOption('hidefroze') or GetOption('hidefroze') == '')
					{
						if($UA->IsSubscriptionFrozen($uid, $row->type_id, $row->list_id))
						{
							$tabs .= "<a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=froze&id=$row->type_id&list_id=$row->list_id&view='>".JText::_('COM_BILLING_UNFREEZE')."</a></td>";
						}
						else
						{
							if(!GetOption('hidefroze'))
							{
								$tabs .= "<a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=froze&id=$row->type_id&list_id=$row->list_id&view='>".JText::_('COM_BILLING_FROZE')."</a></td>";
							}
						}
					}
				}
				$now = date('Y-m-d');
				if ($now > $subdate)
				{
					$style = "style='color: red;'";
				}
				else
				{
					$style = '';
				}
				$tabs .= "<td $style>". FormatBillingDate($subdate) ."</td>";
				$tabs .= '</tr>';
			}
		}
		$tabs .= "</table>";
	}
	else
	{
		if(count($rows) == 0)
		{
			$tabs .= '<p class="billing_p" align="center">'.JText::_('COM_BILLING_NO_SUBSCRIPTIONS').'</p>';
		}
	}	
	
	$pane->endTab();
	return $tabs;
}

function GetFilesTab($uid, $pane)
{
	$db = JFactory::getDBO();
	$tabs = '';
	$tab_name = GetOption('tab_name-4');
	if($tab_name == '')
	{
		$tab_name = JText::_('COM_BILLING_FILES');
	}
	$tabs .= $pane->StartTab($tab_name , 'files');
	$today = date('Y-m-d');
	$query = "select distinct * from `#__billing_subscription_access` where item_type = 2 and subscription_id in (select distinct subscription_id from `#__billing_subscription` where enddate >= '$today' and uid = $uid and frozen = 0)";
    $result = $db->setQuery($query);   
    $rows = $db->loadObjectList(); 
	if (count($rows) > 0)
	{
		$tabs .= '<table  width="100%" class="adminlist table billing_table">';
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_TITLE').'</td><td>'.JText::_('COM_BILLING_DESCRIPTION').'</td></tr>';
		if(count($rows) > 0)
		{
			foreach ( $rows as $row ) 
			{
				$tabs .= '<tr>';
				$x = explode('_', basename($row->file_path));
				$tabs .= "   <td><a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=getfile&id=$row->id'>". $x[0] ."</a></td>";
				$tabs .= "<td>$row->description</td>";
				$tabs .= '</tr>';
			}
		}
		$tabs .= '</table>';
	}
	else
	{
		$tabs .= '<p class="billing_p" align="center">'.JText::_('COM_BILLING_THERE_ARE_NO_FILES').'</p>';
	}	
	$pane->endTab();
	return $tabs;
}

function GetMusicTab($uid, $pane)
{
	$db = JFactory::getDBO();
	$tabs = '';
	$tab_name = GetOption('tab_name-5');
	if($tab_name == '')
	{
		$tab_name = JText::_('COM_BILLING_AUDIO');
	}
	$tabs .= $pane->StartTab($tab_name , 'music');
	$today = date('Y-m-d');
	$query = "select distinct * from `#__billing_subscription_access` where item_type = 3 and subscription_id in (select distinct subscription_id from `#__billing_subscription` where enddate >= '$today' and uid = $uid and frozen = 0)";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 
	if (count($rows) > 0)
	{
		$tabs .= '<table  width="100%" class="adminlist table billing_table">';
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_TITLE').'</td><td>'.JText::_('COM_BILLING_DESCRIPTION').'</td></tr>';
		if(count($rows) > 0)
		{
			foreach ( $rows as $row ) 
			{
				$tabs .= '<tr>';
				$url = JURI::base(). 'components/com_billing/files/' . basename($row->file_path);
				$tabs .= "<td>";
				$tabs .= GetMP3Player($url);
				$tabs .= "</td>";
				$tabs .= "<td>$row->description</td>";
				$tabs .= '</tr>';
			}
		}
		$tabs .= '</table>';
	}
	else
	{
		$tabs .= '<p class="billing_p" align="center">'.JText::_('COM_BILLING_THERE_ARE_NO_MUSIC_FILES').'</p>';
	}	
	$pane->endTab();
	return $tabs;
}

function GetArticlesTab($uid, $pane)
{
	$discount = GetUserGroupDiscount($uid);
	$db = JFactory::getDBO();
	$tabs = '';
	$tab_name = GetOption('tab_name-6');
	if($tab_name == '')
	{
		$tab_name = JText::_('COM_BILLING_ARTICLES');
	}
	$tabs .= $pane->StartTab($tab_name , 'art');
	$UA = new UserAccount();
	if(GetOption('publisher'))
	{
		$pgroup = $UA->GetPublisherGroupId();	
		if($UA->IsUserInGroup($uid, $pgroup))
		{
			$pub = true;
		}
		else
		{
			$pub = false;
		}
	}
	else
	{
		$pub = true;
	}
	
	$query = "select count(*) from `#__billing_group`  where enabled = 1";
	$result = $db->setQuery($query);
	$res = $db->loadResult();
	if($res > 0)
	{
		$gid = GetUserGroupId($uid);
		if($gid != '')
		{
			$query = "select * from `#__billing_group` where id = $gid";
			$result = $db->setQuery($query);   
			$row = $db->loadAssoc();
			$can = $row['option1'];
			if($can != 1)
			{
				$pub = false;
			}
		}
	}
	
	if(GetOption('author') and $pub)
	{
		$editor = JFactory::getEditor();
		$editor2 = JFactory::getEditor();
		$category_options = JHtml::_('category.options', 'com_content');
		//print_r($category_options);
		$cats = '<select name="category_id" id="category_id">';
		//$cats .= JHTML::_('select.options', $category_options);
		$cats .= GetCategoryList();
		$cats .= GetK2Categories(0);		
		$cats .= '</select>';

	
		$tabs .= "<form action='".JURI::base()."index.php?option=com_billing&task=addarticle' name='addarticle' method='post' enctype='multipart/form-data'>";
		$tabs .= '<h3>' . JText::_('COM_BILLING_MYARTICLE') . '</h3>';
		$tabs .= '<table width="100%" border="0" class="adminlist table billing_table_invisible">';
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_TITLE') . "</td><td><input class='inputbox' type='text' name='title' value='' size='80'></td></tr>";
		$tabs .= "<tr><td>".JText::_('COM_BILLING_INTRO')."</td><td>".$editor->display( 'artintro', '', '500', '200', '20', '20', false ) ."</td></tr>";
		$tabs .= "<tr><td>".JText::_('COM_BILLING_FULLTEXT')."</td><td>".$editor2->display( 'artbody', '', '500', '200', '20', '20', false ) ."</td></tr>";
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_CATEGORY') . '</td><td>' . $cats . " ".JText::_('JFIELD_LANGUAGE_LABEL') . ' ' . 
		GetLanguageList(). "</td></tr>";		
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_PRICE') . "</td><td><input class='inputbox' type='text' name='price' value='10.00' size='5'> ".GetDefaultCurrencyAbbr().'  (' .JText::_('COM_BILLING_YOUR_PERCENT'). ' ' .$UA->GetAuthorPercent($uid) ."% )</td><tr>";
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_SOLD_COUNT') . "</td><td><input class='inputbox' type='text' name='cnt' value='' size='2'></td></tr>";
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_ADDFILE') ."</td><td><input class='inputbox' type='file' name='file1'> ".JText::_('COM_BILLING_DESCRIPTION')." <input class='inputbox' type='text' name='desc1' value='' size='40'></td></tr>";
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_ADDFILE') ."</td><td><input class='inputbox' type='file' name='file2'> ".JText::_('COM_BILLING_DESCRIPTION')." <input class='inputbox' type='text' name='desc2' value='' size='40'></td></tr>";
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_ADDFILE') ."</td><td><input class='inputbox' type='file' name='file3'> ".JText::_('COM_BILLING_DESCRIPTION')." <input class='inputbox' type='text' name='desc3' value='' size='40'></td></tr>";
		
		$tabs .= "</table><p align='right'><input type='submit' value='".JText::_('COM_BILLING_SAVE')."'></p>";
		$tabs .= "</form>";
	}
	
	if($UA->ExtensionExists('com_k2'))
	{
		$query = 'select distinct a.*, '.
		'(select state from `#__billing_articles_user` where article_id = a.article_id and uid = '.$uid.') as state, '.
		'(select title from `#__content` where id = a.article_id) as title, '.
		'(select title from `#__k2_items` where id = a.article_id) as k2title, '.
		'(select created_by from `#__content` where id = a.article_id) as author '.
		" from `#__billing_articles` a where article_id in (select id from `#__content` where created_by = $uid)";
	}
	else
	{
		$query = 'select distinct a.*, '.
		'(select state from `#__billing_articles_user` where article_id = a.article_id and uid = '.$uid.') as state, '.
		'(select title from `#__content` where id = a.article_id) as title, '.
		"'' as k2title, ".
		'(select created_by from `#__content` where id = a.article_id) as author '.
		" from `#__billing_articles` a where article_id in (select id from `#__content` where created_by = $uid)";
	}
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 	
	
	$tabs .= '<h2>' . JText::_('COM_BILLING_MY_ARTICLES') . '</h2>';
   
	$tabs .= '<table width="100%" class="adminlist table billing_table">';
	$tabs .= "<thead>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_TITLE')."</th>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_COST')."</th>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_STATE')."</th>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_VIEW')."</th>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_ACTIONS')."</th>";
	$tabs .= "</thead>";
		
	if(count($rows) > 0)	
	{
		foreach ( $rows as $row ) 
		{
			$tabs .= '<tr>';
			if($row->k2 == 1)
			{
				$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_k2&view=item&id=$row->article_id'>$row->k2title</a></td>";
			}
			else
			{
				$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_content&view=article&id=$row->article_id'>$row->title</a></td>";
			}
			if ($row->price != '')
			{
				if ($discount > 0 and $row->price > 0)
				{
					$xp = $row->price - $row->price/ 100 * $discount;
					$xp = round($xp, 2);
					$tabs .= "<td>". FormatDefaultCurrency($xp) ." (".JText::_('COM_BILLING_DISCOUNT')." $discount %)</td>";
				}
				else
				{
					$tabs .= "<td>". FormatDefaultCurrency($row->price) ."</td>";
				}
			}
			else
			{
				$tabs .= "<td></td>";
			}
			if($row->state == 1)
			{		
				$tabs .= "<td><img width='16' height='16' border='0' src='".tick."'></td>";
			}
			else
			{
				$tabs .= "<td><img width='16' height='16' border='0' src='".publish_x."'></td>";
			}
			if($row->k2 == 1)
			{
				$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_k2&view=item&id=$row->article_id'>".JText::_('COM_BILLING_VIEW')."</a></td>";
			}
			else
			{
				$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_content&view=article&id=$row->article_id'>".JText::_('COM_BILLING_VIEW')."</a></td>";
			}
			if($row->author == $uid)
			{
				if($row->is_active == 1)
				{
					$tabs .= "<td><a href='".JURI::base()."index.php?option=com_billing&task=actart&id=$row->id' title='".JText::_('COM_BILLING_HIDE')."'><img width='16' height='16' border='0' src='".tick."'>";
				}
				else
				{
					$tabs .= "<td><a href='".JURI::base()."index.php?option=com_billing&task=actart&id=$row->id' title='".JText::_('COM_BILLING_PUBLISH')."'><img width='16' height='16' border='0' src='".publish_x."'>";
				}
				$tabs .= "</a></td>";
			}
			else
			{
				$tabs .= "<td></td>";
			}
			$tabs .= '</tr>';
		}
	}
	$tabs .= '</table><hr>';			
		
	$pane->endTab();
	return $tabs;
}

function GetServiceTab($uid, $pane)
{
	$discount = GetUserGroupDiscount($uid);
	$db = JFactory::getDBO();
	$tabs = '';
	$tab_name = GetOption('tab_name-11');
	if($tab_name == '')
	{
		$tab_name = JText::_('COM_BILLING_SERVICES');
	}
	$tabs .= $pane->StartTab($tab_name , 'services');
		
	if(GetOption('hideservices') == false)
	{
		$query = 'select * from `#__billing_services` where is_active = 1';
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList(); 
		
		if (count($rows) > 0)
		{	
			$tabs .= '<p class="billing_p"><strong>'.JText::_('COM_BILLING_WARNING_1').'</strong></p>';
		
			$n = count($rows);
			$divs = '';
			$tabs .= "<form method='post' action='".JURI::base()."index.php?option=com_billing&task=payservice&view='>";
			$tabs .=  "<script>";
			$tabs .= "	var arr1 = []; var arr2 = [];";
			$i = 1;
			foreach($rows as $row)
			{
				$tabs .= "arr1[$i] = $row->id; ";
				$tabs .= "arr2[$i] = '$row->data1'; ";
				$i++;
			}
			$tabs .= "</script>";
			$tabs .= "<select class='select' name='id' onchange='
			for(i=1; i<=arr1.length;i++)
			{
				if(arr1[i] == this.value) 
				{document.getElementById(\"div2_\" + arr1[i]).style.display = \"block\";} 
				else 
				{document.getElementById(\"div2_\" + arr1[i]).style.display = \"none\";}

				if(arr1[i] == this.value) 
				{
					if(arr2[i] == \"true\")
					
						{document.getElementById(\"free_price\").style.display = \"inline\";} 
						else 
						{document.getElementById(\"free_price\").style.display = \"none\";}
					
				}
			}'>";
			$first = true;
			foreach($rows as $row)
			{
				$xx = '';
				if($row->data1 == 'true')
				{
					$xx = '';
				}
				else
				{
					if ($discount > 0 and $row->price > 0)
					{
						$xp = $row->price - $row->price / 100 * $discount;
						$xp = round($xp, 2);
						$xx .= FormatDefaultCurrency($xp) ." (".JText::_('COM_BILLING_DISCOUNT')." $discount %)";
					}
					else
					{
						$xx .= ' (' . FormatDefaultCurrency($row->price) .')';
					}
				}
				$tabs .= "<option value='$row->id'>$row->service_name $xx</option>";
				if($first)
				{
					$first = false;
					$divs .= "<div style='display: block;' id='div2_$row->id'>$row->annotation</div>";
				}
				else
				{
					$divs .= "<div style='display: none;' id='div2_$row->id'>$row->annotation</div>";
				}
			}
			$tabs .= "</select> ";
			if($rows[0]->data1 == 'true')
			{
				$tabs .= "<span id='free_price' style='display: inline;'><input class='inputbox' type='text' name='free_price' value='' size='4'> ".GetDefaultCurrencyAbbr()."</span> ";
			}
			else
			{
				$tabs .= "<span style='display: none;' id='free_price'><input class='inputbox' type='text' name='free_price' value='' size='4'> ".GetDefaultCurrencyAbbr()."</span> ";
			}
			$tabs .= "<br>".JText::_('COM_BILLING_ADD_YOUR_COMMENT')."<br><textarea name='comments' cols='50' rows='3'></textarea><br>";
			$tabs .=" <input type='submit' value='".JText::_('COM_BILLING_PAY')."'>";
			$tabs .= "</form><br>";
			$tabs .= $divs;
			$tabs .= '<br>';
		}		
		
		//$query = "select * from `#__billing_services` where is_active = 1";
		$query = "select s.*, ";
		$query .=  "(select service_name from `#__billing_services` where id = s.service_id) as service_name, ";
		$query .=  "(select id from `#__billing_services` where id = s.service_id) as sid ";
		$query .=  " from `#__billing_services_user` s ";
		$query .= " where uid = $uid";
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList(); 
		
		$tabs .= '<table width="100%" class="adminlist table billing_table">';
		$tabs .= "<thead>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_TITLE')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_COST')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_STATE')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_PAY')."</th>";
		$tabs .= "</thead>";
		if(count($rows) > 0)
		{	
			foreach ( $rows as $row ) 
			{
				if($row->service_name == '' and $row->sid == '')
				{
					continue;
				}
				$tabs .= '<tr>';
				$tabs .= "<td>$row->service_name (<a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=service&id=$row->service_id'>".JText::_('COM_BILLING_MORE')."</a>)<br/>$row->comments</td>";
				if ($row->price != '')
				{
					$tabs .= "<td>". FormatDefaultCurrency($row->price) ."</td>";
				}
				else
				{
					$tabs .= "<td></td>";
				}
			
				if($row->state == 1)
				{
					$tabs .= "<td>".JText::_('COM_BILLING_SERVICE_PROVIDED')." ". FormatBillingDate($row->servicedate)."</td>";
				}
				else
				{
					$tabs .= "<td>".JText::_('COM_BILLING_SERVICE_NOT_PROVIDED')."</td>";
				}
				
				$tabs .= "<td>".JText::_('COM_BILLING_PAID')." ". FormatBillingDate($row->paymentdate)."</td>"; //$paymentdate
				$tabs .= '</tr>';
			}
		}
		$tabs .= '</table>';	
	}	
	
	if(GetOption('hidetariff') == false)
	{
		$tabs .= '<br><h2>'.JText::_('COM_BILLING_TARIFFS').'</h2>';
		
		$query = "select * from `#__billing_subscription_type` where id in (select subscription_id from `#__billing_subscription_access` where item_type = 8) order by id";
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList(); 	
		
		$tabs .= '<table width="100%" class="adminlist table billing_table" cellspacing="1">';
		$tabs .= '<thead>'.
			'<th class="b_title">ID</th> '.
			'<th class="b_title">'.JText::_('COM_BILLING_TITLE').'</th> '.
			'<th class="b_title">'.JText::_('COM_BILLING_PRICE').'</th> '.
			'<th class="b_title">'.JText::_('COM_BILLING_SERVICES').'</th> '.
			'<th class="b_title">'.JText::_('COM_BILLING_ACTIONS').'</th>'.
			'<th class="b_title">'.JText::_('COM_BILLING_EXPIRES').'</th></thead>';
		
		$UA = new UserAccount();
		if (count($rows) > 0)	
		{
			foreach( $rows as $row ) 
			{
				$subdate = SubDate($uid, $row->id);
				$tabs .= "<tr>";
					$tabs .= "<td>$row->id</td>";
					$tabs .= "<td><a href='".JURI::base()."index.php?option=com_billing&task=viewsub&id=$row->id'>$row->subscription_type</a></td>";
					$tabs .= "<td>". FormatDefaultCurrency($row->price) ."</td>";
					$query = "select * from `#__billing_services` where id in (select item_id from `#__billing_subscription_access` where item_type = 8 and subscription_id = $row->id) order by service_name";
					$result = $db->setQuery($query);   
					$srvs = $db->loadObjectList(); 	
					$tabs .= '<td>';
					if(count($srvs) > 0)
					{
						foreach($srvs as $srv)
						{
							if($srv->id == '')
							{
								continue;
							}
							$tabs .= "<a href='".JURI::base()."index.php?option=com_billing&task=service&id=$srv->id'>$srv->service_name</a><br>";
						}
					}
					$tabs .= '</td>';
					if($subdate != '' and $subdate >= date('Y-m-d'))
					{
						$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=getsub&id=$row->id&view='>".JText::_('COM_BILLING_EXTEND')."</a><br>";
						if($UA->IsSubscriptionFrozen($uid, $row->id))
						{
							$tabs .= "<a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=froze&id=$row->id&view='>".JText::_('COM_BILLING_UNFREEZE')."</a><br>";
						}
						else
						{
							if(!GetOption('hidefroze'))
							{
								$tabs .= "<a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=froze&id=$row->id&view='>".JText::_('COM_BILLING_FROZE')."</a><br>";
							}
						}				
						if(GetOption('changetariff') and $UA->IsSubscriptionActual($uid, $row->id))
						{
							$tabs .= "<a href='".JURI::base()."index.php?option=com_billing&task=changesub&id=$row->id&view='>".JText::_('COM_BILLING_CAHNGE_SUBSCRIBE')."</a>";
						}
					}
					else
					{
						$tabs .= "<td><a href='".JURI::base()."index.php?option=com_billing&task=getsub&id=$row->id&view='>".JText::_('COM_BILLING_SUBSCRIBE')."</a><br>";
					}
					$tabs .= "</td>";
					if (date('Y-m-d') > $subdate)
					{
						$style = "style='color: red;'";
					}
					else
					{
						$style = '';
					}
					if($subdate == '')
					{
						$tabs .= "<td></td>";
					}
					else
					{
						$tabs .= "<td $style>" .FormatBillingDate($subdate)."</td>";
					}
				$tabs .= '</tr>';	
			}
		
		}
		
		$query = "select count(*) from `#__billing_services_user` where uid = $uid";
		$result = $db->setQuery($query);
		$n = $db->loadResult();
		if($n > 0 and GetOption('maketariff'))
		{
			$query = "select sum(price) from `#__billing_services_user` where uid = $uid";
			$result = $db->setQuery($query);
			$sum = $db->loadResult();
			
			$query = "select enddate from `#__billing_personal_tariff` where uid = $uid";
			$result = $db->setQuery($query);
			$enddate = $db->loadResult();
			
			$query = "select * from `#__billing_services_user` where uid = $uid";
			$result = $db->setQuery($query);
			$rows = $db->loadObjectList();
			$tabs .= '<tr>';
			$tabs .= "<td></td>";
			$tabs .= "<td>".JText::_('COM_BILLING_PERSONAL_TARIFF')."</td>";
			$tabs .= "<td>$sum ". GetDefaultCurrencyAbbr() ."</td>";
			$tabs .= '<td>';
			if(count($rows) > 0)
			{
				foreach($rows as $row)
				{
					$query = "select service_name from `#__billing_services` where id = $row->service_id and is_active = 1";
					$result = $db->setQuery($query);
					$title = $db->loadResult();
					$tabs .= "<a href='".JURI::base()."index.php?option=com_billing&task=service&id=$row->service_id'>" . $title . '</a><br>';
				}
			}
			$tabs .= '</td>';
			$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=prolong&id=$row->id'>".JText::_('COM_BILLING_SUBSCRIBE')."</a></td>";
			if (date('Y-m-d') > $enddate)
			{
				$style = "style='color: red;'";
			}
			else
			{
				$style = '';
			}
			if($enddate == '')
			{
				$tabs .= "<td></td>";
			}
			else
			{
				$tabs .= "<td $style>". FormatBillingDate($enddate)."</td>";
			}
			$tabs .= '</tr>';
		}
		
		$tabs .= '</table>';
	}
	
	$query = "select * from `#__billing_services` where is_active = 1 order by service_name";
	$result = $db->setQuery($query);   
	$srvs = $db->loadObjectList();
	if(GetOption('maketariff') and !GetOption('hidetariff') and count($srvs) > 0)
	{
		$query = "select sum(price) from `#__billing_services` where id in ( select service_id from `#__billing_services_user` where uid = $uid)";
		$result = $db->setQuery($query); 
		$sum = $db->loadResult();	
		if($sum == '')		
		{
			$sum = 0;
		}
		$tabs .= '<br><h2>'.JText::_('COM_BILLING_TARIFFS_CONSTRUCTOR').'</h2>';
		$tabs .= "<p>". JText::_('COM_BILLING_SUM'). ": <span id='sum1'>$sum</span> ".GetDefaultCurrencyAbbr()."</p>";
		$tabs .= "<form action='".JURI::base()."index.php?option=com_billing&task=maketariff' method='post'><input type='hidden' id='sum' name='sum' value='$sum' onchange='document.getElementById(\"sum1\").innerHTML = this.value;'><table border='0' class='adminlist table billing_table_invisible'>";
		if(count($srvs) > 0)
		{	
			foreach($srvs as $srv)
			{
				$query = "select count(*) from `#__billing_services_user` where uid = $uid and service_id = $srv->id and service_id in (select id from `#__billing_services`)";
				$result = $db->setQuery($query); 
				$x = $db->loadResult();
				if($x > 0) 
				{
					$f = 'true';
					$ch = 'checked';
				} 
				else 
				{
					$ch = '';
					$f = 'false';
				}
				$tabs .= "<tr><td width='1%'><input type='checkbox' name='option_$srv->id' value='$f' $ch onclick='this.value = this.checked;if(this.checked){document.getElementById(\"sum\").value = parseFloat(document.getElementById(\"sum\").value) + parseFloat($srv->price);}else{document.getElementById(\"sum\").value = parseFloat(document.getElementById(\"sum\").value) - parseFloat($srv->price);} document.getElementById(\"sum1\").innerHTML = document.getElementById(\"sum\").value;'><input type='hidden' name='price_$srv->id' value='$srv->price'></td>";
				$tabs .= "<td>$srv->service_name</td></tr>";
			}
		}
		$tabs .= "<tr><td></td><td><input type='submit' value='".JText::_('COM_BILLING_SAVE')."'></td></tr>";
		$tabs .= "</table></form>";
	}	
	$pane->endTab();
	return $tabs;
}

function ShowServices()
{
	$user = JFactory::getUser();
	$uid = $user->id;
	if ($uid == 0 or $uid == '') return;
	$discount = GetUserGroupDiscount($uid);
	$db = JFactory::getDBO();
	$tabs = '';
	
	if(GetOption('hideservices') == false)
	{
		$query = 'select * from `#__billing_services` where is_active = 1';
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList(); 
		
		if (count($rows) > 0)
		{	
			$tabs .= '<p class="billing_p"><strong>'.JText::_('COM_BILLING_WARNING_1').'</strong></p>';
		
			$n = count($rows);
			$divs = '';
			$tabs .= "<form method='post' action='".JURI::base()."index.php?option=com_billing&task=payservice&view='>";
			$tabs .=  "<script>";
			$tabs .= "	var arr1 = []; var arr2 = [];";
			$i = 1;
			foreach($rows as $row)
			{
				$tabs .= "arr1[$i] = $row->id; ";
				$tabs .= "arr2[$i] = '$row->data1'; ";
				$i++;
			}
			$tabs .= "</script>";
			$tabs .= "<select class='select' name='id' onchange='
			for(i=1; i<=arr1.length;i++)
			{
				if(arr1[i] == this.value) 
				{document.getElementById(\"div2_\" + arr1[i]).style.display = \"block\";} 
				else 
				{document.getElementById(\"div2_\" + arr1[i]).style.display = \"none\";}

				if(arr1[i] == this.value) 
				{
					if(arr2[i] == \"true\")
					
						{document.getElementById(\"free_price\").style.display = \"inline\";} 
						else 
						{document.getElementById(\"free_price\").style.display = \"none\";}
					
				}
			}'>";
			$first = true;
			foreach($rows as $row)
			{
				$xx = '';
				if($row->data1 == 'true')
				{
					$xx = '';
				}
				else
				{
					if ($discount > 0 and $row->price > 0)
					{
						$xp = $row->price - $row->price / 100 * $discount;
						$xp = round($xp, 2);
						$xx .= FormatDefaultCurrency($xp) ." (".JText::_('COM_BILLING_DISCOUNT')." $discount %)";
					}
					else
					{
						$xx .= ' (' . FormatDefaultCurrency($row->price) .')';
					}
				}
				$tabs .= "<option value='$row->id'>$row->service_name $xx</option>";
				if($first)
				{
					$first = false;
					$divs .= "<div style='display: block;' id='div2_$row->id'>$row->annotation</div>";
				}
				else
				{
					$divs .= "<div style='display: none;' id='div2_$row->id'>$row->annotation</div>";
				}
			}
			$tabs .= "</select> ";
			if($rows[0]->data1 == 'true')
			{
				$tabs .= "<span id='free_price' style='display: inline;'><input class='inputbox' type='text' name='free_price' value='' size='4'> ".GetDefaultCurrencyAbbr()."</span> ";
			}
			else
			{
				$tabs .= "<span style='display: none;' id='free_price'><input class='inputbox' type='text' name='free_price' value='' size='4'> ".GetDefaultCurrencyAbbr()."</span> ";
			}
			$tabs .= "<br>".JText::_('COM_BILLING_ADD_YOUR_COMMENT')."<br><textarea name='comments' cols='50' rows='3'></textarea><br>";
			$tabs .=" <input type='submit' value='".JText::_('COM_BILLING_PAY')."'>";
			$tabs .= "</form><br>";
			$tabs .= $divs;
			$tabs .= '<br>';
		}		
		
		//$query = "select * from `#__billing_services` where is_active = 1";
		$query = "select s.*, ";
		$query .=  "(select service_name from `#__billing_services` where id = s.service_id) as service_name ";
		$query .=  " from `#__billing_services_user` s ";
		$query .= " where uid = $uid";
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList(); 
		
		$tabs .= '<table width="100%" class="adminlist table billing_table">';
		$tabs .= "<thead>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_TITLE')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_COST')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_STATE')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_PAY')."</th>";
		$tabs .= "</thead>";
		if(count($rows) > 0)
		{	
			foreach ( $rows as $row ) 
			{
				$tabs .= '<tr>';
				$tabs .= "<td>$row->service_name (<a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=service&id=$row->service_id'>".JText::_('COM_BILLING_MORE')."</a>)<br/>$row->comments</td>";
				if ($row->price != '')
				{
					$tabs .= "<td>". FormatDefaultCurrency($row->price) ."</td>";
				}
				else
				{
					$tabs .= "<td></td>";
				}
			
				if($row->state == 1)
				{
					$tabs .= "<td>".JText::_('COM_BILLING_SERVICE_PROVIDED')." ". FormatBillingDate($row->servicedate)."</td>";
				}
				else
				{
					$tabs .= "<td>".JText::_('COM_BILLING_SERVICE_NOT_PROVIDED')."</td>";
				}
				
				$tabs .= "<td>".JText::_('COM_BILLING_PAID')." ". FormatBillingDate($row->paymentdate)."</td>"; //$paymentdate
				$tabs .= '</tr>';
			}
		}
		$tabs .= '</table>';	
	}	
	
	
	if(GetOption('hidetariff') == false)
	{
		$tabs .= '<br><h2>'.JText::_('COM_BILLING_TARIFFS').'</h2>';
		
		$query = "select * from `#__billing_subscription_type` where id in (select subscription_id from `#__billing_subscription_access` where item_type = 8) order by id";
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList(); 	
		
		$tabs .= '<table width="100%" class="adminlist table billing_table" cellspacing="1">';
		$tabs .= '<thead>'.
			'<th class="b_title">ID</th> '.
			'<th class="b_title">'.JText::_('COM_BILLING_TITLE').'</th> '.
			'<th class="b_title">'.JText::_('COM_BILLING_PRICE').'</th> '.
			'<th class="b_title">'.JText::_('COM_BILLING_SERVICES').'</th> '.
			'<th class="b_title">'.JText::_('COM_BILLING_ACTIONS').'</th>'.
			'<th class="b_title">'.JText::_('COM_BILLING_EXPIRES').'</th></thead>';
		
		$UA = new UserAccount();
		if (count($rows) > 0)	
		{
			foreach( $rows as $row ) 
			{
				$subdate = SubDate($uid, $row->id);
				$tabs .= "<tr>";
					$tabs .= "<td>$row->id</td>";
					$tabs .= "<td><a href='".JURI::base()."index.php?option=com_billing&task=viewsub&id=$row->id'>$row->subscription_type</a></td>";
					$tabs .= "<td>". FormatDefaultCurrency($row->price) ."</td>";
					$query = "select * from `#__billing_services` where id in (select item_id from `#__billing_subscription_access` where item_type = 8 and subscription_id = $row->id) order by service_name";
					$result = $db->setQuery($query);   
					$srvs = $db->loadObjectList(); 	
					$tabs .= '<td>';
					if(count($srvs) > 0)
					{
						foreach($srvs as $srv)
						{
							$tabs .= "<a href='".JURI::base()."index.php?option=com_billing&task=service&id=$srv->id'>$srv->service_name</a><br>";
						}
					}
					$tabs .= '</td>';
					if($subdate != '' and $subdate >= date('Y-m-d'))
					{
						$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=getsub&id=$row->id&view='>".JText::_('COM_BILLING_EXTEND')."</a><br>";
						if($UA->IsSubscriptionFrozen($uid, $row->id))
						{
							$tabs .= "<a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=froze&id=$row->id&view='>".JText::_('COM_BILLING_UNFREEZE')."</a><br>";
						}
						else
						{
							if(!GetOption('hidefroze'))
							{
								$tabs .= "<a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=froze&id=$row->id&view='>".JText::_('COM_BILLING_FROZE')."</a><br>";
							}
						}				
						if(GetOption('changetariff') and $UA->IsSubscriptionActual($uid, $row->id))
						{
							$tabs .= "<a href='".JURI::base()."index.php?option=com_billing&task=changesub&id=$row->id&view='>".JText::_('COM_BILLING_CAHNGE_SUBSCRIBE')."</a>";
						}
					}
					else
					{
						$tabs .= "<td><a href='".JURI::base()."index.php?option=com_billing&task=getsub&id=$row->id&view='>".JText::_('COM_BILLING_SUBSCRIBE')."</a><br>";
					}
					$tabs .= "</td>";
					if (date('Y-m-d') > $subdate)
					{
						$style = "style='color: red;'";
					}
					else
					{
						$style = '';
					}
					if($subdate == '')
					{
						$tabs .= "<td></td>";
					}
					else
					{
						$tabs .= "<td $style>" .FormatBillingDate($subdate)."</td>";
					}
				$tabs .= '</tr>';	
			}
		
		}
	}
	
	$query = "select count(*) from `#__billing_services_user` where uid = $uid";
	$result = $db->setQuery($query);
	$n = $db->loadResult();
	if($n > 0 and GetOption('maketariff'))
	{
		$query = "select sum(price) from `#__billing_services_user` where uid = $uid";
		$result = $db->setQuery($query);
		$sum = $db->loadResult();
		
		$query = "select enddate from `#__billing_personal_tariff` where uid = $uid";
		$result = $db->setQuery($query);
		$enddate = $db->loadResult();
		
		$query = "select * from `#__billing_services_user` where uid = $uid";
		$result = $db->setQuery($query);
		$rows = $db->loadObjectList();
		$tabs .= '<tr>';
		$tabs .= "<td></td>";
		$tabs .= "<td>".JText::_('COM_BILLING_PERSONAL_TARIFF')."</td>";
		$tabs .= "<td>$sum ". GetDefaultCurrencyAbbr() ."</td>";
		$tabs .= '<td>';
		if(count($rows) > 0)
		{
			foreach($rows as $row)
			{
				$query = "select service_name from `#__billing_services` where id = $row->service_id and is_active = 1";
				$result = $db->setQuery($query);
				$title = $db->loadResult();
				$tabs .= "<a href='".JURI::base()."index.php?option=com_billing&task=service&id=$row->service_id'>" . $title . '</a><br>';
			}
		}
		$tabs .= '</td>';
		$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=prolong&id=$row->id'>".JText::_('COM_BILLING_SUBSCRIBE')."</a></td>";
		if (date('Y-m-d') > $enddate)
		{
			$style = "style='color: red;'";
		}
		else
		{
			$style = '';
		}
		if($enddate == '')
		{
			$tabs .= "<td></td>";
		}
		else
		{
			$tabs .= "<td $style>". FormatBillingDate($enddate)."</td>";
		}
		$tabs .= '</tr>';
	}
	
	$tabs .= '</table>';
	
	$query = "select * from `#__billing_services` where is_active = 1 order by service_name";
	$result = $db->setQuery($query);   
	$srvs = $db->loadObjectList();
	if(GetOption('maketariff') and count($srvs) > 0)
	{
		$query = "select sum(price) from `#__billing_services` where id in ( select service_id from `#__billing_services_user` where uid = $uid)";
		$result = $db->setQuery($query); 
		$sum = $db->loadResult();	
		if($sum == '')		
		{
			$sum = 0;
		}
		$tabs .= '<br><h2>'.JText::_('COM_BILLING_TARIFFS_CONSTRUCTOR').'</h2>';
		$tabs .= "<p>". JText::_('COM_BILLING_SUM'). ": <span id='sum1'>$sum</span> ".GetDefaultCurrencyAbbr()."</p>";
		$tabs .= "<form action='".JURI::base()."index.php?option=com_billing&task=maketariff' method='post'><input type='hidden' id='sum' name='sum' value='$sum' onchange='document.getElementById(\"sum1\").innerHTML = this.value;'><table border='0' class='adminlist table billing_table_invisible'>";
		if(count($srvs) > 0)
		{	
			foreach($srvs as $srv)
			{
				$query = "select count(*) from `#__billing_services_user` where uid = $uid and service_id = $srv->id";
				$result = $db->setQuery($query); 
				$x = $db->loadResult();
				if($x > 0) 
				{
					$f = 'true';
					$ch = 'checked';
				} 
				else 
				{
					$ch = '';
					$f = 'false';
				}
				$tabs .= "<tr><td width='1%'><input type='checkbox' name='option_$srv->id' value='$f' $ch onclick='this.value = this.checked;if(this.checked){document.getElementById(\"sum\").value = parseFloat(document.getElementById(\"sum\").value) + parseFloat($srv->price);}else{document.getElementById(\"sum\").value = parseFloat(document.getElementById(\"sum\").value) - parseFloat($srv->price);} document.getElementById(\"sum1\").innerHTML = document.getElementById(\"sum\").value;'><input type='hidden' name='price_$srv->id' value='$srv->price'></td>";
				$tabs .= "<td>$srv->service_name</td></tr>";
			}
		}
		$tabs .= "<tr><td></td><td><input type='submit' value='".JText::_('COM_BILLING_SAVE')."'></td></tr>";
		$tabs .= "</table></form>";
	}	
	echo $tabs;
}

function u_name($id)
{
	$db = JFactory::getDBO();
	$query = "select username from `#__users` where id = $id";
	$result = $db->setQuery($query);   
	return $db->loadResult();
}

function GetRow($uid, $deep, $nullid = '')
{
	$db = JFactory::getDBO();
	$out = '<tr>';
	/*if($nullid != '')
	{
		$out .= "<td><img src='/components/com_billing/images/user.png'> ".u_name($nullid)."</td>";
	}
	else
	{
		$out .= "<td></td>";
	}*/
	$out .= "<td><img src='/components/com_billing/images/user.png'> ".u_name($uid)."</td>";
	$ids = $uid;
	$deep = $deep - 1;//2
	while($deep > 0)
	{	
		$out .= "<td>";
		if($ids != '')
		{
			$query = "select * from `#__billing_partner` where uid in ( $ids )";
			$result = $db->setQuery($query);   
			$rows = $db->loadObjectList();				
			$ids = '';
			if (count($rows) > 0)
			{
				foreach($rows as $row)
				{
					if($ids == '')
					{
						$ids = $row->partner_id;
					}
					else
					{
						$ids = $ids . ', ' . $row->partner_id;
					}
					$out .= " <img src='/components/com_billing/images/user.png'> ".u_name($row->partner_id)." (".u_name($row->uid).")<br> ";
				}
			}
		}
		$deep = $deep - 1;
		$out .= "</td>";
	}	
	$out .= '</tr>';
	return $out;
}

function GetPartnerTab($uid, $pane, $is_tab = true)
{
	$db = JFactory::getDBO();
	$tabs = '';
	$tab_name = GetOption('tab_name-7');
	if($tab_name == '')
	{
		$tab_name = JText::_('COM_BILLING_PARTNERS');
	}

	if($is_tab)
	{
		$tabs .= $pane->StartTab($tab_name , 'partner');
	}	
	
	$partner = GetOption('partner');
	if ($partner == true) 
	{
		$partnerpercent = GetOption('partnerpercent');
		$partnerpayment = GetOption('partnerpayment');

		//$url = JRoute::_( JURI::base() . "index.php?option=com_billing&partner=$uid", true);
		if(GetOption('easyurl') != '')
		{
			$url2 = JURI::base() . "?" . GetOption('easyurl') . "=" . GetUserURLName($uid);
		}
		else
		{
			$url2 = JRoute::_( JURI::base() . "index.php?option=com_billing&partnername=". GetUserURLName($uid), true);
		}
		$tabs .= "<h2>".JText::_('COM_BILLING_YOUR_PARTNER_URL').":</h2> <p class='billing_p'>". $url2 ."</p>"; // '<br>'. $url2 .
		$tabs .= '<p class="billing_p">'.JText::_('COM_BILLING_ABOUT_PARTNER').'</p>';
		if($partnerpercent > 0 and $partnerpercent < 100 and $partnerpercent != '')
		{
			$tabs .= "<p class='billing_p'>".JText::_('COM_BILLING_PARTNER_PHRASE_2')." $partnerpercent % ".JText::_('COM_BILLING_PARTNER_PHRASE_3').".</p>";
		}
		if ($partnerpayment != '' and $partnerpayment > 0)
		{
			$tabs .= "<p class='billing_p'>".JText::_('COM_BILLING_PARTNER_PHRASE_4')."  ". FormatDefaultCurrency($partnerpayment) ."</p>";
		}
		if(GetOption('invite'))
		{
			$tabs .= "<form action='".JURI::base()."index.php?option=com_billing&task=sendinvite&id=$uid' method='post'>";
			$tabs .= JText::_('COM_BILLING_INVITE_EMAIL')." <input type='text' name='email' value='' size='35'> <input type='submit' value='".JText::_('COM_BILLING_INVITE')."'>";
			$tabs .= "</form>";
		}
		
		$tabs .= '<table border="0" width="100%" class="adminlist table billing_table_invisible"><tr height="80px">';
		if (GetOption('vkontakte') != '')
		{
			$tabs .= '<td><!-- Put this script tag to the <head> of your page -->'.
				'<script type="text/javascript" src="http://userapi.com/js/api/openapi.js?47"></script>'.
				'<script type="text/javascript">'.
				'VK.init({apiId: '. GetOption('vkontakte') .', onlyWidgets: true});'.
				'</script>'.
				'<!-- Put this div tag to the place, where the Like block will be -->'.
				'<div id="vk_like"></div>'.
				'<script type="text/javascript">'.
				'VK.Widgets.Like("vk_like", {type: "button"}); </script></td>';
		}
		
		if (GetOption('facebook') != '')
		{
			$tabs .= '<td><div id="fb-root"></div>'.
				'<script>(function(d, s, id) {'.
				'var js, fjs = d.getElementsByTagName(s)[0];'.
				'if (d.getElementById(id)) return;'.
				'js = d.createElement(s); js.id = id;'.
				'js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId='. GetOption('facebook') .'";'.
				'fjs.parentNode.insertBefore(js, fjs);'.
				'}(document, \'script\', \'facebook-jssdk\'));</script>';
			
			$tabs .= '<iframe src="https://www.facebook.com/plugins/like.php?href='.JURI::root().'"
        scrolling="no" frameborder="0"
        style="border:none; width:450px; height:80px"></iframe></td>'; 		
			//$tabs .= '<div class="fb-like" data-href="'.JURI::root() .'" data-send="true" data-width="450" data-show-faces="false" style="width: 60px;"></div></td>';
			
		}
		$tabs .= '</tr></table>';
		
		$query = "select uid from `#__billing_partner` where partner_id = $uid";
		$result = $db->setQuery($query);  
		$daddy = $db->loadResult();
		
		if($daddy != '')
		{
			$tabs .= "<h3>" . JText::_('COM_BILLING_INVITE_BY') . ": ".u_name($daddy)."</h3>";
		}
		else
		{
			$tabs .= "<h3>" . JText::_('COM_BILLING_NO_SPONSOR') . "</h3>";
		}
		$tabs .= "<h3>" . JText::_('COM_BILLING_INVITE_USER') . ": ".u_name($uid)."</h3>";
		
		$p_percent = GetOption('p_percent');
		$p_depth = GetOption('p_depth');
		$deep = $p_depth;
		if(multy)
		{
			$p_depth = 5;
			$deep = $p_depth;
		}
		if((($p_percent != '' and $p_percent > 0) or multy) and $p_depth != '' and $p_depth > 1)
		{
			$ids = $uid;
			$tabs .= "<table border='0' width='100%'>";
			$tabs .= "<thead><tr>";
			for($i = 1;$i <=5; $i++)
			{
				$tabs .= "<th>".JText::_('COM_BILLING_LEVELN')." $i</th>"; 
			}
			$tabs .= "</tr></thead>";

			$query = "select * from `#__billing_partner` where uid = $uid";
			$result = $db->setQuery($query);   
			$rows = $db->loadObjectList();
			if(count($rows) > 0)
			{
				$n = 0;
				foreach($rows as $row)
				{
					if($n == 0)
					{
						$tabs .= GetRow($row->partner_id, $deep, $uid);
					}
					else
					{
						$tabs .= GetRow($row->partner_id, $deep);
					}
					$n++;
				}
			}

			$tabs .= "</table>";
		}
		$tabs .= '<hr />';
	
		$tabs .= '<table width="100%" class="adminlist table billing_table">';
		$tabs .= "<tr><thead>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_USER')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_REGISTER_DATE')."</th>";
		$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_USER_SUM')."</th>";
		$tabs .= "</thead></tr>";

		$query = 'select bp.*, (select name from `#__users` where id = bp.partner_id) as user_name, (select username from `#__users` where id = bp.partner_id) as user from `#__billing_partner` bp where uid =' . $uid;
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList(); 
		if (count($rows) > 0)
		{
			foreach ( $rows as $row ) 
			{
				if($row->user_name != '' or $row->user != '')
				{
					$tabs .= '<tr>';
					$tabs .= "<td>$row->user_name ($row->user)</td>";
					$tabs .= "<td>". FormatBillingDate($row->enter_date)."</td>";
					$tabs .= "<td align='center'><a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=partnerhistory&partner_id=$row->partner_id&view=billing'>".GetPartnerSum($row->partner_id)."</a></td>";
					$tabs .= '</tr>';
				}
			}
		}
		else
		{
			$tabs .= '<tr><td></td><td>'.JText::_('COM_BILLING_NO_PARTNERS').'</td><td></td></tr>';
		}
		$tabs .= '</table>';

	}
	else
	{
		$tabs .= '<p class="billing_p">'.JText::_('COM_BILLING_PARTNER_DISABLED').'</p>';
	}
	if($is_tab)
	{
		$pane->endTab();
	}
	return $tabs;
}

function GetAccountsTab($uid, $pane)
{
	$db = JFactory::getDBO();
	$tabs = '';
	$tab_name = GetOption('tab_name-8');
	if($tab_name == '')
	{
		$tab_name = JText::_('COM_BILLING_ACCOUNTS');
	}
	$tabs .= $pane->StartTab($tab_name , 'accounts');
	
	$tabs .= "<form action='".JURI::base()."index.php?option=com_billing&task=addaccount' method='post' name='acc'>";
	$query = 'select * from `#__billing_account_type` where active = 1';
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 
	$tabs .= JText::_('COM_BILLING_ADDACCOUNT') .': <select class="b_select" name="acc_id">';
	if (count($rows) > 0)
	{
		foreach ( $rows as $row ) 
		{
			$tabs .= "<option value='$row->id'>$row->account_type_name</option>";
		}
	}
	$tabs .= '</select><br><br>';
	$tabs .= '<input type="submit" name="sb" value="'. JText::_('COM_BILLING_NEXT').'">';
	$tabs .= "</form><br><hr><br>";
	
	$tabs .= '<table width="100%" class="adminlist table billing_table">';
	$tabs .= "<thead>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_TITLE')."</th>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_SUM')."</th>";
	$tabs .= "</thead>";
	
	
	$query = "select distinct * from `#__billing_account_type` where id in (select account_type from `#__billing_account` where uid = $uid)";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 
	if (count($rows) > 0)
	{
		foreach ( $rows as $row ) 
		{
			$tabs .= '<tr>';
			$tabs .= "<td>$row->account_type_name</td>";
			$tabs .= "<td>".GetUserAccountSum($row->id, $uid)."</td>";
			$tabs .= '</tr>';
		}
	}
	
	$tabs .= '</table>';
	$pane->endTab();
	return $tabs;	
}

function GetUserPointsTab($uid, $pane, $is_tab = true)
{
	$db = JFactory::getDBO();
	$tabs = '';
	$tab_name = GetOption('tab_name-10');
	if($tab_name == '')
	{
		$tab_name = JText::_('COM_BILLING_USERPOINTS');
	}
	if($is_tab)
	{
		$tabs .= $pane->StartTab($tab_name , 'userpoints');
	}
	
	$query = "select sum(points) from `#__billing_user_point` where uid = $uid and actual = 1 order by id desc";
	$result = $db->setQuery($query);   
	$points = $db->loadResult();
	
	$tabs .= '<h2>'.JText::_('COM_BILLING_USERPOINTS').': '.$points.'</h2>';
	$exchange = GetOption('exchange');
	if ($exchange)
	{
		$tabs .= "<a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=exchange'>".JText::_('COM_BILLING_USERPOINTS_MONEY')." 1:". GetCurencyRate(10000) ."</a>";
	}
	
	$query = "select up.*, (select action_name from `#__billing_point_actions` where id = up.action_id) as action_name from `#__billing_user_point` up where uid = $uid order by id desc";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 

	$tabs .= '<table width="100%" class="adminlist table billing_table">';
	$tabs .= "<thead>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_TITLE')."</th>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_SUM')."</th>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_DATE')."</th>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_ACTUAL')."</th>";
	$tabs .= "</thead>";

	if (count($rows) > 0)
	{
		foreach ( $rows as $row ) 
		{
			$tabs .= '<tr>';
			$tabs .= "<td>$row->action_name</td>";
			$tabs .= "<td>$row->points</td>";
			$tabs .= "<td>". FormatBillingDate($row->oper_date)."</td>";
			if ($row->actual == 1)
			{
				$tabs .= "<td><img width='16' height='16' border='0' src='".tick."'></td>";
			}
			else
			{
				$tabs .= "<td><img width='16' height='16' border='0' src='".publish_x."'></td>";
			}
			$tabs .= '</tr>';
		}
	}
	$tabs .= '</table>';
	if($is_tab)
	{
		$pane->endTab();
	}	
	return $tabs;
}

function GetProjectsTab($uid, $pane)
{
	$db = JFactory::getDBO();
	$tabs = '';
	$tab_name = GetOption('tab_name-9');
	if($tab_name == '')
	{
		$tab_name = JText::_('COM_BILLING_PROJECTS');
	}
	$tabs .= $pane->StartTab($tab_name , 'projects');
	
	$query = "select p.*, (select sum(psum) from `#__billing_project_user` where project_id = p.id) as psum from  `#__billing_project` p where is_active = 1 order by id";
	$result = $db->setQuery($query);
	$rows = $db->loadObjectList();
	$tabs .= '<table class="adminlist table billing_table">';
	$tabs .= '<thead><th>Id</th><th>'.JText::_('COM_BILLING_PROJECT_NAME').'</th><th>'.JText::_('COM_BILLING_PROJECT_SUM2').'</th><th>'.JText::_('COM_BILLING_START_PROJECT').'</th><th>'.JText::_('COM_BILLING_END_PROJECT').'</th></thead>';
	if(count($rows) > 0)
	{
		foreach($rows as $row)
		{
			$tabs .= '<tr>';
			$tabs .= "<td>$row->id</td>";
			$tabs .= "<td><a href='".JURI::base()."index.php?option=com_billing&task=showproject&id=$row->id'>$row->project_name</a></td>";
			$tabs .= "<td>$row->psum ".GetDefaultCurrencyAbbr()." [$row->project_sum ".GetDefaultCurrencyAbbr()."]</td>";
			$tabs .= "<td>". FormatBillingDate($row->start_date)."</td>";
			$tabs .= "<td>". FormatBillingDate($row->finish_date)."</td>";
			$tabs .= '</tr>';
		}
	}
	$tabs .= '</table>';
	$pane->endTab();
	return $tabs;	
}

function ViewSub($id)
{
	$db = JFactory::getDBO();
	
	$user = JFactory::getUser();
	$uid = $user->id;	
	$subdate = SubDate($uid, $id);
	
			echo '<h1>'.JText::_('COM_BILLING_SUB_OBJECTS').' "'. SubName($id).'"</h1>';	
			echo '<table  width="100%" class="adminlist table billing_table">';
			echo '<tr><td>'.JText::_('COM_BILLING_TITLE').'</td><td>'.JText::_('COM_BILLING_OBJECT_TYPE').'</td><td>'.JText::_('COM_BILLING_DESCRIPTION').'</td></tr>';
			
			$query = 'select * from `#__billing_subscription_access` where subscription_id = ' . $id;
			$result = $db->setQuery($query);   
			$rows = $db->loadObjectList(); 	
			if (count($rows) == 0)
			{
				echo '<tr><td align="center">'.JText::_('COM_BILLING_NO_OBJECTS').'</td><td></td><td></td></tr>';
			}
			else
			{
				if(count($rows) > 0)
				{
					foreach ( $rows as $row ) 
					{
						echo '<tr>';
						switch($row->item_type)
						{
							case 1:
								if ($subdate != '')
								{
									echo "   <td><a class='billing_a' href='".JURI::base()."index.php?option=com_content&view=article&id=$row->item_id'>". GetTitle($row->item_id) ."</a></td>";
								}
								else
								{
									echo "   <td>".GetTitle($row->item_id)."</td>";
								}
							break;
							case 2:
								if ($subdate != '')
								{
									$url = JURI::base(). 'components/com_billing/files/' . basename($row->file_path);
									$x = explode('_', basename($row->file_path));
									echo "   <td><a class='billing_a' href='".JURI::base()."index.php?option=com_billing&task=getfile&id=$row->id'>". $x[0]."</a></td>";
								}
								else
								{
									echo '<td>'.JText::_('COM_BILLING_DOWNLOAD_AFTER_SUB').'</td>';
								}
							break;
							case 3:
								if ($subdate != '')
								{
									$url = JURI::base(). 'components/com_billing/files/' . basename($row->file_path);
									echo "   <td>";
									echo GetMP3Player($url);
									echo "</td>";
								}
								else
								{
									echo '<td>'.JText::_('COM_BILLING_MP3_AFTER_SUB').'</td>';
								}
							break;
							case 4:
								echo "   <td>". basename($row->file_path) ."</td>";
							break;
							case 5:
								echo "   <td>". basename($row->file_path) ."</td>"; //video
							break;
							case 6:
								echo "   <td>". GetSectionName($row->item_id) ."</td>";
							break;
							case 7:
								echo "   <td>". GetCategoryName($row->item_id) ."</td>";
							break;
							case 8:
								echo "   <td>". GetServiceName($row->item_id) ."</td>";
							break;
							case 9:
								echo "   <td>". GetUserGroupName($row->item_id) ."</td>";
							break;
							case 10:
								echo "   <td>". GetCustomItemName($row->id, $row->item_id) ."</td>";
							break;
							case 11:
								echo "   <td>". GetK2ItemName($row->item_id) ."</td>";								
							break;
							case 12:
								echo "   <td>". GetJSGroupName($row->item_id) ."</td>";								
							break;
							case 13:
								echo "   <td>". GetK2CategoryName($row->item_id) ."</td>";								
							break;
							case 14:
								echo "   <td>". GetBillingGroupName($row->item_id) ."</td>";								
							break;							
							case 15:
								echo "   <td>". GetAdsManagerCategoryName($row->item_id) ."</td>";								
							break;							
						}
						switch($row->item_type)
						{
							case 1:
								echo "   <td>".JText::_('COM_BILLING_MATERIAL')."</td>";
							break;
							case 2:
								echo "   <td>".JText::_('COM_BILLING_FILE')."</td>";
							break;
							case 3:
								echo "   <td>".JText::_('COM_BILLING_MP3FILE')."</td>";
							break;
							case 4:
								echo "   <td>".JText::_('COM_BILLING_COMPONENT')."</td>";
							break;
							case 5:
								echo "   <td>".JText::_('COM_BILLING_VIDEOFILE')."</td>";
							break;
							case 6:
								echo "   <td>".JText::_('COM_BILLING_SECTION')."</td>";
							break;
							case 7:
								echo "   <td>".JText::_('COM_BILLING_CATEGORY')."</td>";
							break;
							case 8:
								echo "   <td>".JText::_('COM_BILLING_SERVICE')."</td>";
							break;
							case 9:
								echo "   <td>".JText::_('COM_BILLING_USERGROUP')."</td>";
							break;
							case 10:
								echo "   <td>".JText::_('COM_BILLING_CUSTOM_ITEM')."</td>";
							break;
							case 11:
								echo "   <td>".JText::_('COM_BILLING_K2_ITEM')."</td>";
							break;
							case 12:
								echo "   <td>".JText::_('COM_BILLING_JS_GROUP')."</td>";
							break;
							case 13:
								echo "   <td>".JText::_('COM_BILLING_K2_CATEGORY')."</td>";
							break;
							case 14:
								echo "   <td>".JText::_('COM_BILLING_USERGROUP')." Billing</td>";
							break;
							case 15:
								echo "   <td>".JText::_('COM_BILLING_ADS_CATEGORY')." Billing</td>";
							break;
						}
						echo "   <td>$row->description</td>";
						echo '</tr>';
					}
				}
			}
			
			echo '</table>';
	echo '<p class="billing_p" align="center"><a class="billing_a" href="javascript: history.back(1)">'.JText::_('COM_BILLING_BACK').'</a></p>';
}

function ShowPartnerHistory()
{
	$db = JFactory::getDBO();
	$uid = CheckLogin();
	if ($uid == 0) return;
	echo "<h2>".JText::_('COM_BILLING_PARTNER_INCOMING')."</h2>";

	echo '<table width="100%" class="adminlist table billing_table">';
	echo "<tr>";
	echo "  <td>Id</td>";
	echo "  <td>".JText::_('COM_BILLING_USER')."</td>";
	echo "  <td>".JText::_('COM_BILLING_DATE')."</td>";
	echo "  <td>".JText::_('COM_BILLING_SUM')."</td>";
	echo "  <td>".JText::_('COM_BILLING_DESCRIPTION')."</td>";
	echo "</tr>";

	$query = "select bp.*, (select name from `#__users` where id = bp.partner_id) as username, (select username from `#__users` where id = bp.partner_id) as user from `#__billing_partner_history` bp where uid = $uid";
    $result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 
	if(count($rows) > 0)
	{
		foreach ( $rows as $row ) 
		{
			echo '<tr>';
			echo "<td>$row->id</td>";
			echo "<td>$row->username ($row->user)</td>";
			echo "<td>". FormatBillingDate($row->oper_date)."</td>";
			echo "<td>$row->val</td>";
			echo "<td>$row->data1</td>";
			echo '</tr>';
		}
	}
	echo '</table>';
}	
	
function ShowService($id)
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_services` where id = $id";
	$result = $db->setQuery($query);   
	$row = $db->loadAssoc(); 	
	
	echo "<h2>$row[service_name]</h2>";
	echo $row['annotation'];
	echo "<p class='billing_p' align='center'>";
	//if (!GetOption('hideservices')) 
	//{
	//	echo "<a class='billing_a' href='index.php?option=com_billing&task=payservice&view=id=$id'>".JText::_('COM_BILLING_PAY')."</a> <br>";
	//}
	echo "<a href='javascript: history.back(1);'>".JText::_('COM_BILLING_BACK')."</a></p>";
	
}	

function PayService($id)
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	$discount = GetUserGroupDiscount($uid);
	$free_price = JRequest::getFloat('free_price');
	$comments = JRequest::getVar('comments');
	$comments = addslashes($comments);

	$db = JFactory::getDBO();

		$query = "select * from `#__billing_services` where id = $id";
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc();
		
		if ($row['is_active'] != 1)
		{
			echo '<p class="billing_p">'.JText::_('COM_BILLING_SERVICE_NOT_ACTIVE').'</p>';
			return;
		}
		if($row['data1'])
		{
			$price = $free_price;
		}
		else
		{
			$price = $row['price'];
		}
	
		$UA = new UserAccount;	
		$b = $UA->GetBalance($uid);
		if ($discount > 0 and $price > 0)
		{
			$price = $price - $price / 100 * $discount;
			$price = round($price, 2);
		}
				
		if ($b >= $price and $price >= 0)
		{
			$pid = date('ymdhis') . $uid;
			$UA->WithdrawMoney($uid, $price, $pid, JText::_('COM_BILLING_SERVICE_PAYMENT')." $row[service_name]");
			$date = date('Y-m-d H:i:s');
			$query = "insert into `#__billing_services_user` (service_id, uid, paymentdate, price, state, comments) values ($id, $uid, '$date', $price, 0, '$comments')";
			$result = $db->setQuery($query);   	
			$x = $db->query(); 
			
			if (UserPointsEnabled())
			{
				$action_id = GetActionIdByCode('payservice');
				AddUserPoints($uid, $action_id);
			}
			
			echo '<p class="billing_p">'.JText::_('COM_BILLING_SERVICE_PAID').' '. $row['service_name']. '. '.JText::_('COM_BILLING_SERVICE_NOTIFICATION').
				'  <a class="billing_a" href="'.JURI::base().'index.php?option=com_billing&view=billing">'.JText::_('COM_BILLING_BACK_TO_CABINET').'</a></p>';
			
			$service1 = GetOption('service1');
			if ($service1)
			{
				$user = JFactory::getUser($uid);
				EmailToAdmin(JText::_('COM_BILLING_USER') ." $uid ($user->username, $user->name)". JText::_('COM_BILLING_SERVICE_PAID2')." $row[service_name]. " . JURI::base() . "administrator/index.php?option=com_billing&task=serviceusers&id=$row[id] <br>". JText::_('COM_BILLING_USER') . ' :<br>' . $comments , JText::_('COM_BILLING_SERVICE_NOTIFICATION2'));
			}
			
			return;
		}		
		else
		{
			$y = $price - $b;
			echo '<p class="billing_p">'.JText::_('COM_BILLING_YOU_HAVE_NO_MONEY').' '.JText::_('COM_BILLING_YOU_NEED_MONEY2').' '. FormatDefaultCurrency($y) .
			' <a class="billing_a" href="'.JURI::base().'index.php?option=com_billing&view=pay&sum='.$y.'">'.JText::_('COM_BILLING_ADD_MONEY').'</a></p>';			
		}
	//}
	
}

function GetServiceState($id, $uid)
{
	$db = JFactory::getDBO();
	$query = "select state from `#__billing_services_user` where service_id = $id and uid = $uid";
	$result = $db->setQuery($query);   	
	$x = $db->loadResult(); 
	return $x;
}

function GetCoupon($sum)
{
	$user = JFactory::getUser();
	$uid = $user->id;
	
	echo "<h2>".JText::_('COM_BILLING_INPUT_SUM_AND_METHOD')."</h2>";
	echo '<p class="billing_p">'.JText::_('COM_BILLING_WILL_BE_ADD').'</p>';
	echo '<form action="'.JURI::base().'index.php?option=com_billing&task=coupon&view=" name="coupon" method="post">';	
	echo '<p class="billing_p" align="center"><input class="inputbox" type="text" name="sum" value="'.$sum.'" readonly> <span id="curlab">'. GetDefaultCurrencyAbbr() . '</span> ' . 
		GetPlugins('', 'curlab') . ' <input type="submit" name="SB" value="'.JText::_('COM_BILLING_NEXT').'"></p>';
	echo '</form>';	
	
}

function SendCoupon()
{
	if (GetOption('usecoupon') == false)
	{
		echo '<p class="billing_p">'.JText::_('COM_BILLING_COUPON_STOPPED').'</p>';
		return;
	}
	
	$couponprice = GetOption('couponprice');

	$sum = JRequest::getVar('sum');
	if ($sum == '') {return;}
	if ($sum < 0) {return;}

}

function AddAccount($acc_id)
{
	$user = JFactory::getUser();
	$uid = $user->id;
	
	if ($uid == 0) return;

	$db = JFactory::getDBO();
	$query = "select count(*) from `#__billing_account` where account_type = $acc_id and uid = $uid";
	$result = $db->setQuery($query);   
	$n = $db->loadResult();
	if ($n > 0)
	{
		echo '<p class="billing_p">'.JText::_('COM_BILLING_ALREADY_HAS').'</p>';
		Cabinet(); 
		return;
	}

	$date = date('Y-m-d H:i:s');
	$query = "insert into `#__billing_account` (account_type, creation_date, uid) values ($acc_id, '$date', $uid)";
	$result = $db->setQuery($query);   
	$result = $db->query();
	echo '<p class="billing_p">'.JText::_('COM_BILLING_ADDED').'</p>';
	Cabinet(); 
}

function ExchangeUserPoints()
{
	FrontLog('Exchage started');
	$exchange = GetOption('exchange');
	FrontLog('Exchage enabled: ' . $exchange);
	if(!$exchange)
	{
		echo '<p class="billing_p">Disabled</p>';
		Cabinet();
		return;
	}
	
	$user = JFactory::getUser();
	$uid = $user->id;

	if ($uid == 0) return;

	FrontLog('user ' . $uid);
	
	$db = JFactory::getDBO();
	$query = "select sum(points) from `#__billing_user_point` where uid = $uid and actual = 1 order by id desc";
	$result = $db->setQuery($query);   
	$points = $db->loadResult();
	
	FrontLog('points ' . $points);
	
	if ($points > 0)
	{
		$query = "update `#__billing_user_point` set actual = 0 where uid = $uid";
		$result = $db->setQuery($query);   
		$result = $db->query();
		$rate = GetCurencyRate(10000);
		$money = $rate * $points;
		FrontLog('money ' . $money);
		$UA = new UserAccount;
		$pid = date('his') . $uid;
		$UA->AddMoney($uid, $money, $pid, JText::_('COM_BILLING_USERPOINTS_ADD'), 0, '', true);		
	}
	Cabinet();
}

function TabAction()
{
	$action = JRequest::getCmd('action');
	$tabname = JRequest::getCmd('tabname');
	$user = JFactory::getUser();
	$uid = $user->id;

	$db = JFactory::getDBO();
	$query = "select * from `#__billing_tabs` where tabname = '$tabname'";
	$result = $db->setQuery($query);   
	$row = $db->loadAssoc(); 
	$php_class = $row['php_class'];
	$php_entry = $row['php_entry'];
	/*require_once(JPATH_ROOT . '/components/com_billing/tabs/' . $tabname . '/' . $php_entry);
				
	eval('$Plugin = new '. $php_class . '("'.$tabname.'");');*/
	$Plugin = BillingPluginFactory::GetTabPlugin($tabname, $php_class, $php_entry);
	$Plugin->ProcessAction($action);
}

function MoneyToProject($id)
{
	$uid = CheckLogin();
	if ($uid == 0) return;
	
	$sum = JRequest::getFloat('sum');
	FrontLog("MoneyToProject: $sum");
	$comment = JRequest::getVar('comment');
	FrontLog("MoneyToProject: $comment");
	$UA = new UserAccount;
	$UA->SendMoneyToProject($uid, $id, $sum, $comment);
	FrontLog("MoneyToProject: done");
}

function ShowProject($id)
{
	$db = JFactory::getDBO();
	$query = "select * from  `#__billing_project` where id = $id";
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();
	$project_name = $row['project_name'];
	$description = $row['description'];
	$start_date = $row['start_date'];
	$finish_date = $row['finish_date'];
	$project_sum = $row['project_sum'];
	
	echo "<h1>$project_name</h1>";
	echo $description;
	
	echo "<form action='".JURI::base()."index.php?option=com_billing&task=moneytoproject&id=$id' method='post'>";
	echo JText::_('COM_BILLING_MOVESUM') . " <input class='inputbox' type='text' name='sum' value='100' size='5'> " . GetDefaultCurrencyAbbr();
	echo '<br>' . JText::_('COM_BILLING_ADD_YOUR_COMMENT') . " <input class='inputbox' type='text' name='comment' value='' size='80'> ";
	echo "<br><br><input type='submit' value='".JText::_('COM_BILLING_PAY')."'>";
	echo "</form><br>";
	
	echo '<h3>'. JText::_('COM_BILLING_PAYMENTLIST') . '</h3>';
	
	$query = "select * from  `#__billing_project_user` where project_id = $id order by send_date desc";
	$result = $db->setQuery($query);
	$rows = $db->loadObjectList();
	if(count($rows) > 0)
	{
		echo '<table class="adminlist table billing_table">';
		echo '<thead><th class="b_title">Id</th><th class="b_title">'.JText::_('COM_BILLING_USER').'</th><th class="b_title">'.JText::_('COM_BILLING_SUM').'</th><th class="b_title">'.JText::_('COM_BILLING_DATE').'</th><th class="b_title">'.JText::_('COM_BILLING_DESCRIPTION').'</th></thead>';
		foreach($rows as $row)
		{
			echo '<tr>';
			echo "<td>$row->id</td>";
			$user = JFactory::getUser($row->uid);
			echo "<td>$user->username</td>";// [<a href='mailto:$user->email'>$user->email</a>]
			echo "<td>$row->psum ".GetDefaultCurrencyAbbr()."</td>";
			echo "<td>". FormatBillingDate($row->send_date)."</td>";
			echo "<td>$row->comment</td>";
			echo '</tr>';
		}
		echo '</table>';
	}
	
	$comments = JPATH_ROOT . '/components/com_jcomments/jcomments.php';
	if (file_exists($comments)) {
		require_once($comments);
		echo JComments::showComments($id, 'com_billing', $project_name);
	}
}

function AddUserArticle()
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	$desc1 = JRequest::getVar('desc1');
	$desc2 = JRequest::getVar('desc2');
	$desc3 = JRequest::getVar('desc3');
	$data = JRequest::get( 'post');
	$artintro =  JRequest::getVar( 'artintro', '', 'post', 'string', JREQUEST_ALLOWHTML );
	$artbody =  JRequest::getVar( 'artbody', '', 'post', 'string', JREQUEST_ALLOWHTML );
	$title = JRequest::getVar('title');
	$price = JRequest::getFloat('price');
	$lang_id = JRequest::getVar('lang_id');
	$cnt = JRequest::getInt('cnt', 0);
	$alias = '';
	if($price < 0)
	{
		JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLING_INVALID_PRICE') );
		return;
	}
	$catid = JRequest::getVar('category_id');
	if($catid < 0 or $catid == '')
	{
		JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLING_INVALID_CATEGORY') );
		return;
	}
	$db = JFactory::getDBO();
	$UA = new UserAccount;

	if(strpos($catid, 'k2') === false )
	{
		$id = $UA->CreateArticle($title, $artintro, $catid, $uid, $price, $artbody, !GetOption('moderation'), $alias, $lang_id, $cnt);
	}
	else
	{
		$XX = explode('_', $catid);
		$catid = $XX[1];
		$id = $UA->AddK2Article($title, $artintro, $catid, $uid, $price, $artbody, !GetOption('moderation'), $alias, $lang_id, $cnt);
	}
	if($id == false)
	{
		return;
	}
	if($_FILES["file1"]["error"] == UPLOAD_ERR_OK)
	{
		$tmp_name = $_FILES['file1']['tmp_name'];
		$file_name = $_FILES['file1']['name'];
		$file_size = $_FILES['file1']['size'];
		$r = $UA->AddFileToArticle($id, $tmp_name, $file_name, $file_size, $desc1);
		FrontLog('Added file: '. $r);	
	}
	if($_FILES["file2"]["error"] == UPLOAD_ERR_OK)
	{
		$tmp_name = $_FILES['file2']['tmp_name'];
		$file_name = $_FILES['file2']['name'];
		$file_size = $_FILES['file2']['size'];
		$r = $UA->AddFileToArticle($id, $tmp_name, $file_name, $file_size, $desc2);
		FrontLog('Added file: '. $r);	
	}
	if($_FILES["file3"]["error"] == UPLOAD_ERR_OK)
	{
		$tmp_name = $_FILES['file3']['tmp_name'];
		$file_name = $_FILES['file3']['name'];
		$file_size = $_FILES['file3']['size'];
		$r = $UA->AddFileToArticle($id, $tmp_name, $file_name, $file_size, $desc3);
		FrontLog('Added file: '. $r);	
	}	
}

function GetAttach($id)
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	$db = JFactory::getDBO();
	$query = "select * from `#__billing_articles_file` where id = $id";
	$result = $db->setQuery($query);   
	$row = $db->loadAssoc();
	
	$aid = $row['article_id'];
	
	$query = "select count(*) from `#__billing_articles_user` where article_id = $aid and uid = $uid";
	$result = $db->setQuery($query);   
	$n = $db->loadResult();
	
	$file = (JPATH_ROOT . '/components/com_billing/files/' .$row['newname']);
	if($n > 0 and file_exists($file))
	{
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.$row['filename']);
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		ob_clean();
		flush();
		readfile($file);
		exit;
	}
	else
	{
		JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLIING_AUTH_NEED') );		
	}
}

function GetFileK2($hash)
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	$db = JFactory::getDBO();
	$query = "select * from `#__billing_k2_files` where hash = '$hash' and user_id=$uid";
    $result = $db->setQuery($query);   
    $row = $db->loadAssoc();
	$fn = $row['fn'];
	
	if($fn != '')
	{
		$path = "./media/k2/attachments/";
		$file = $path.$fn;
		$filename = $fn;
		FrontLog('Get file '. $file);
		if(file_exists($file))
		{
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.$filename);
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			ob_clean();
			flush();
			readfile($file);
			exit;
		}
		else
		{
			JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLIING_AUTH_NEED') );		
		}
	}
}

function GetFile($id)
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	$db = JFactory::getDBO();
	$query = "select * from `#__billing_subscription_access` where id = $id";
    $result = $db->setQuery($query);   
    $row = $db->loadAssoc();
	$sid = $row['subscription_id'];
	
	$UA = new UserAccount;
	if($UA->IsSubscriptionActual($uid, $sid))
	{
		$file = $row['file_path'];
		if($row['data1'] != '')
		{
			$filename = $row['data1'];
		}
		else
		{
			$base = explode('_', $row['file_path']);
			//$path_info = pathinfo($row['file_path']);
			$filename = $base[0];//$path_info['basename'];
		}
		FrontLog('Get file '. $file);
		if(file_exists($file))
		{
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.$filename);
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			ob_clean();
			flush();
			readfile($file);
			exit;
		}
		else
		{
			JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLIING_AUTH_NEED') );		
		}
	}
}

function ProcessPin()
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	$db = JFactory::getDBO();
	$pin = trim(JRequest::getVar('pin'));
	
	$query = "select * from `#__billing_settings` where paysystem = 'pincodes'";
	$result = $db->setQuery($query);
	$settings = $db->loadAssoc();
	$comission = $settings['data1'];
	
	if ($comission == '' or $comission <= 0 or $comission >= 100 )
	{
		$comission = 0;
	}
	FrontLog("comission: $comission");
	$query = "select id from `#__billing_pin` where pin_type_id in (select id from `#__billing_pin_type` where pin_type = 1) and activated = 0 and pin_code = '$pin'";
	$result = $db->setQuery($query);
	$id = $db->loadResult();
	if($id != '')
	{
		include_once (JPATH_ROOT . '/components/com_billing/plugins/pincodes/pincodes.php');
		$query = "select * from `#__billing_pin` where id = $id";
		$result = $db->setQuery($query);
		$pincode = $db->loadAssoc();
		if($pincode['price'] != '' and $pincode['price'] > 0)
		{
			$price = $pincode['price'];
		}
		else
		{
			$query = "select * from `#__billing_pin_type` where id = $pincode[pin_type_id]";
			$result = $db->setQuery($query);
			$pint = $db->loadAssoc();
			$price = $pint['price'];
		}
		if($price == '')
		{
			$price = 0;
		}
		FrontLog("price: $price");
		if($comission != 0)
		{
			$price = $price - $price/ 100 * $comission;
		}
		FrontLog("price after comission: $price");
		$pid = date('His').$uid;
		$Plugin = new PinCodes('pincodes');
		$UA = new UserAccount;
		$UA->AddMoneyEasyCurr($uid, $price, $pid, JText::_('COM_BILLING_PIN_CODE_ADDM') . " $pin" , $Plugin->currency_id);
		$UA->ActivatePinCode($pin, $uid, $price);
		require_once( JPATH_BASE .DS.'components'.DS.'com_billing'.DS.'plugins'.DS.'pincodes'.DS.'pincodes.php' );
		$Plugin->amount = $price;
		$Plugin->uid = $uid;
		$Plugin->Success();
		
		$date = date('Y-m-d H:i:s');
		$query = "update `#__billing_pin` set activated = 1, uid = $uid, activation_date = '$date', price = $price, operation_id = $pid where id = $id";
		$result = $db->setQuery($query);
		$result = $db->query();
		
		$url = JURI::base().'index.php?option=com_billing&view=billing';
		$msg =  JText::_('COM_BILLING_PIN_CODE_ADDM');
		$app = JFactory::getApplication();
		$app->redirect($url, $msg);		
		return;
	}
	
	$query = "select id from `#__billing_pin` where pin_type_id in (select id from `#__billing_pin_type` where pin_type = 4) and activated = 0 and pin_code = '$pin'";
	$result = $db->setQuery($query);
	$id = $db->loadResult();
	if($id != '')
	{
		$query = "select * from `#__billing_pin` where id = $id";
		$result = $db->setQuery($query);
		$pincode = $db->loadAssoc();
		
		$query = "select * from `#__billing_pin_type` where id = " .$pincode['pin_type_id'];
		$result = $db->setQuery($query);
		$pint = $db->loadAssoc();
		$dt = $pint['best_before'];
		$sub_id = intval($pint['data1']);
		$days = intval($pint['data2']);
		
		$UA = new UserAccount;
		$UA->AddSubscription($uid, $days, $sub_id, true, 0, '');
		$UA->ActivatePinCode($pin, $uid, 0);
		
		$url = JURI::base().'index.php?option=com_billing&view=billing';
		$msg =  JText::_('COM_BILLING_SUB_ADD');
		$app = JFactory::getApplication();
		$app->redirect($url, $msg);
		return;
	}
	
	{
		JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLING_PIN_INVALID') );	
	}
}

function MakeTariff()
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	$UA = new UserAccount();
	$db = JFactory::getDBO();
	
	$query = "select sum(price) from `#__billing_services` where id in ( select service_id from `#__billing_services_user` where uid = $uid)";
	$result = $db->setQuery($query); 
	$oldprice = $db->loadResult();  
	$dayprice = $oldprice / 30;
	
	$query = "select * from `#__billing_services` where is_active = 1 order by service_name";
	$result = $db->setQuery($query);   
	$srvs = $db->loadObjectList();
	if(GetOption('maketariff') and count($srvs) > 0)
	{
		$query = "delete from `#__billing_services_user` where uid = $uid";
		$result = $db->setQuery($query);   
		$result = $db->query();
		$i = 1;
		$sum = 0;
		if(count($srvs) > 0)
		{
			foreach($srvs as $srv)
			{
				$opt = JRequest::getBool('option_' . $i);
				if($opt)
				{
					$sum = $srv->price + $sum;
					$query = "insert into `#__billing_services_user` (service_id, uid, price) values ($srv->id, $uid, $srv->price)";
					$result = $db->setQuery($query);   
					$result = $db->query();
				}
				$i++;
			}
		}
	}	
	
	$query = "select * from `#__billing_personal_tariff` where uid = $uid";
	$result = $db->setQuery($query);
	$row = $db->loadAssoc();
	$enddate = $row['enddate'];
	$paydate = $row['data1'];
	if($enddate != '' and $enddate > date('Y-m-d'))
	{
		$query = "select sum(price) from `#__billing_services` where id in ( select service_id from `#__billing_services_user` where uid = $uid)";
		$result = $db->setQuery($query); 
		$newprice = $db->loadResult();  
		$newdayprice = $newprice / 30;
		
		if($newdayprice > 0)
		{	
			$enddate = strtotime($enddate);
			$nowdate = strtotime(date('Y-m-d'));
			$days = ($enddate - $nowdate)/ 86400;
			$days = round($days); // дней до конца подписки
			$sum = $dayprice * $days;  // стоимость по старой цене
			$newdays = round($sum / $newdayprice); // дней по новой цене
			$enddate = $enddate - $days * 86400 + $newdays * 86400; // убираем старые дни и прибавляем новые
			$enddate = date('Y-m-d', $enddate);
			$query = "update `#__billing_personal_tariff` set enddate = '$enddate' where uid = $uid";
			$result = $db->setQuery($query); 
			$res = $db->query();  
		}
	}
	
}		

function ChangeTariff($id)
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	$UA = new UserAccount();
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_subscription_type` where id not in (select subscription_id from `#__billing_subscription` where uid = $uid and subscription_id <> $id and enddate > NOW() and frozen = 0)";
	$result = $db->setQuery($query);   
    $rows = $db->loadObjectList();
	if(count($rows) > 0)
	{
		$query = "select * from `#__billing_subscription` where subscription_id = $id and uid = $uid and frozen = 0";
		$result = $db->setQuery($query);   
		$sub = $db->loadAssoc();  
		$start = $sub['data1'];
		$finish = $sub['enddate'];
		$sum = $sub['data2'];
		$now = date('Y-m-d H:i:s');
		if($start != '' and $sum != '')
		{
			
			if($sum == 0)
			{
				echo '';
			}
			else
			{
				$finish = strtotime($finish);
				$start = strtotime($start);
				$now = strtotime($now);
				$days = ($finish - $now)/86400;
				$days = round($days);
				$sumaday = $sum/$days;
				$sum1 = $sumaday * $days;
				echo '<h3>' . JText::_('COM_BILLING_UNUSED_BALANCE'). ' ' . FormatDefaultCurrency($sum1) . '</h3>'; 
			}
		}
		else
		{
			
		}			

		echo '<table border="0" class="adminlist table billing_table">';
		if(count($rows) > 0)
		{
			foreach($rows as $row)
			{
				if($row->id == $id) continue;
				echo "<tr>";
				echo "<td>";
				echo "$row->subscription_type";
				echo "</td>";
				echo "<td>";
				$query = "select * from `#__billing_services` where id in (select item_id from `#__billing_subscription_access` where item_type = 8 and subscription_id = $row->id) order by service_name";
					$result = $db->setQuery($query);   
					$srvs = $db->loadObjectList(); 	
					if(count($srvs) > 0)
					{
						foreach($srvs as $srv)
						{
							echo "<a href='".JURI::base()."index.php?option=com_billing&task=service&id=$srv->id'>$srv->service_name</a><br>";
						}
					}
				echo "</td>";
				echo "<td>";
				echo "<a href='".JURI::base()."index.php?option=com_billing&task=changesub2&id=$row->id&oldid=$id&view='>".JText::_('COM_BILLING_CAHNGE_SUBSCRIBE')."</a>";
				echo "</td>";
				echo "</tr>";
			}
		}
		echo '</table>';
	}
	echo '<br><p class="billing_p" align="center"><a class="billing_a" href="'.JURI::base().'index.php?option=com_billing&view=billing">'.JText::_('COM_BILLING_BACK_TO_CABINET').'</a></p>';
}

function ChangeSub2($id)
{
	$oldid = JRequest::getInt('oldid');
	
	$uid = CheckLogin();
	if ($uid == 0) return;

	$UA = new UserAccount();
	$db = JFactory::getDBO();	
	
	$query = "select * from `#__billing_subscription` where subscription_id = $oldid and uid = $uid and frozen = 0";
	$result = $db->setQuery($query);   
	$sub = $db->loadAssoc();  
	$start = $sub['data1'];
	$finish = $sub['enddate'];
	$sum = $sub['data2'];
	$now = date('Y-m-d H:i:s');
	$sum1 = 0;
	if($start != '' and $sum != '')
	{
		$finish = strtotime($finish);
		$start = strtotime($start);
		$now = strtotime($now);
		$days = ($finish - $now)/86400;
		$days = round($days);
		$sumaday = $sum/$days;
		$sum1 = $sumaday * $days;
	}	
	$UA->StopSubscription($uid, $oldid);
	$days = $UA->GetSubscriptionDays($oldid, $sum1);
	if($sum1 > 0)
	{
		$pid = date('His') . $uid;
		$UA->AddMoney($uid, $sum1, $pid, JText::_('COM_BILLING_REFUND'), 0, '', true);
	}
	$UA->AddSubscription($uid, $days, $id);
	Cabinet();
}

function UserSalesReport()
{
	$uid = CheckLogin();
	if ($uid == 0) return;
	
$db = JFactory::getDBO();

	$out = '';
	$out .= '<table width="100%" class="adminlist table" cellspacing="1" border="1">';
	$out .= "<thead><th>".JText::_('COM_BILLING_PRODUCT')."</th>";
	$out .= "  <th class='b_title'>".JText::_('COM_BILLING_JANUARY')."</th>";
	$out .= "  <th class='b_title'>".JText::_('COM_BILLING_FEBRUARY')."</th>";
	$out .= "  <th class='b_title'>".JText::_('COM_BILLING_MARCH')."</th>";
	$out .= "  <th class='b_title'>".JText::_('COM_BILLING_APRIL')."</th>";
	$out .= "  <th class='b_title'>".JText::_('COM_BILLING_MAY')."</th>";
	$out .= "  <th class='b_title'>".JText::_('COM_BILLING_JUNE')."</th>";
	$out .= "  <th class='b_title'>".JText::_('COM_BILLING_JULY')."</th>";
	$out .= "  <th class='b_title'>".JText::_('COM_BILLING_AUGUST')."</th>";
	$out .= "  <th class='b_title'>".JText::_('COM_BILLING_SEPTEMBER')."</th>";
	$out .= "  <th class='b_title'>".JText::_('COM_BILLING_OCTOBER')."</th>";
	$out .= "  <th class='b_title'>".JText::_('COM_BILLING_NOVEMBER')."</th>";
	$out .= "  <th class='b_title'>".JText::_('COM_BILLING_DECEMBER')."</th>";
	$out .= "  <th class='b_title'>".JText::_('COM_BILLING_ALLYEAR')."</th></thead>";
	
	$sum1 = 0;
	$sum2 = 0;
	$sum3 = 0;
	$sum4 = 0;
	$sum5 = 0;
	$sum6 = 0;
	$sum7 = 0;
	$sum8 = 0;
	$sum9 = 0;
	$sum10 = 0;
	$sum11 = 0;
	$sum12 = 0;
	
	$query = "select * from `#__billing_articles` where article_id in (select id from `#__content` where created_by = $uid) order by id";
	$result = $db->setQuery($query);
	$arts = $db->loadObjectList();
	$Y = date('Y');
	if (count($arts) > 0)
	{
		foreach($arts as $art)
		{
			$aid = $art->article_id;
			$title = GetTitle($aid);
			$out .= '<tr>';	
			$out .= "<td>$title</td>";	
			
			$sum = 0;
			
			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-01-01' and '$Y-01-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum1 = $sum1 + $res['sum'];
			
			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-02-01' and '$Y-02-28' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum2 = $sum2 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-03-01' and '$Y-03-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum3 = $sum3 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-04-01' and '$Y-04-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum4 = $sum4 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-05-01' and '$Y-05-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum5 = $sum5 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-06-01' and '$Y-06-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum6 = $sum6 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-07-01' and '$Y-07-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum7 = $sum7 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-08-01' and '$Y-08-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum8 = $sum8 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-09-01' and '$Y-09-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum9 = $sum9 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-10-01' and '$Y-10-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum10 = $sum10 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-11-01' and '$Y-11-30' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum11 = $sum11 + $res['sum'];

			$query = "select sum(price) as sum, count(*) as cnt from `#__billing_articles_user` where article_id = $aid and paymentdate BETWEEN '$Y-12-01' and '$Y-12-31' order by id";
			$result = $db->setQuery($query);
			$res = $db->loadAssoc();
			$out .= "<td>$res[cnt]: ".FormatDefaultCurrency($res['sum'])."</td>";	
			$sum = $sum + $res['sum'];
			$sum12 = $sum12 + $res['sum'];

			$out .= "<td><strong>".FormatDefaultCurrency($sum)."</strong></td>";
			$out .= '</tr>';	
		}
	}
	
	$out .= '<tr>';	
	$out .= "<td><strong>".JText::_('COM_BILLING_TOTAL')."</strong></td>";	
	$out .= "<td><strong>".FormatDefaultCurrency($sum1)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum2)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum3)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum4)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum5)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum6)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum7)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum8)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum9)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum10)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum11)."</strong></td>";
	$out .= "<td><strong>".FormatDefaultCurrency($sum12)."</strong></td>";
	$sum = $sum1 + $sum2 + $sum3 + $sum4 + $sum5 + $sum6 + $sum7 + $sum8 + $sum9 + $sum10 +$sum11 + $sum12;
	$out .= "<td><strong>".FormatDefaultCurrency($sum)."</strong></td>";
	
	$out .= '</tr>';	
	
	$out .= '</table>';
	echo $out;
	
}

function ActArt($id)
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	$db = JFactory::getDBO();	
	$query = "select * from `#__billing_articles` where id = $id";
	$result = $db->setQuery($query);   
	$bilart = $db->loadAssoc(); 
	$aid = $bilart['article_id'];
	$query = "select * from `#__content` where id = $aid";
	$result = $db->setQuery($query);   
	$art = $db->loadAssoc(); 
	if($art['created_by'] != $uid)
	{
		return;
	}
	if($bilart['is_active'] == 1)
	{
		$n = 0;
	}
	else
	{
		$n = 1;
	}
	$query = "update `#__billing_articles` set is_active = $n where id = $id";
	$result = $db->setQuery($query);   
	$art = $db->query();
}

function Froze($id)
{
	$uid = CheckLogin();
	if ($uid == 0) return;
	$UA = new UserAccount();
	$list_id = JRequest::getInt('list_id');
	
	if($UA->IsSubscriptionFrozen($uid, $id, $list_id))
	{
		$UA->UnfreezeSubscription($uid, $id, $list_id);
	}
	else
	{
		$UA->FrozeSubscription($uid, $id, $list_id);
	}
}

function ProlongTariff($id)
{
	$uid = CheckLogin();
	if ($uid == 0) return;
	
	$db = JFactory::getDBO();
	$query = "select sum(price) from `#__billing_services` where id in ( select service_id from `#__billing_services_user` where uid = $uid)";
	$result = $db->setQuery($query); 
	$price = $db->loadResult();

	$UA = new UserAccount;
	$bal = $UA->GetBalanceEx($uid, 0);
	if ($bal < $price and $price > 0)
	{
		echo '<p class="billing_p">'. JText::_('COM_BILLING_YOU_NEED_MONEY') .'. ' . JText::_('COM_BILLING_YOU_NEED_MONEY2') . ' ' . 
			 FormatDefaultCurrency($price) . ' <a class="billing_a" href="'.JURI::base().'index.php?option=com_billing&view=pay">'.JText::_('COM_BILLING_ADD_MONEY').'</a></p>';
		return;
	}
	echo '<p class="billing_p" align="center"> '.JText::_('COM_BILLING_AT_YOUR_ACCOUNT') . ' <strong>' . FormatDefaultCurrency($bal) . 
		' </strong> '.JText::_('COM_BILLING_YOU_CAN').' <a class="billing_a" href="'.JURI::base().'index.php?option=com_billing&view=pay">'.JText::_('COM_BILLING_ADD_MONEY').'</a></p>';
	echo "<p class='billing_p' align='center'>".JText::_('COM_BILLING_EXTEND_SUB')." ". JText::_('COM_BILLING_PERSONAL_TARIFF') ."</p> <br>";
	echo "<div align='center'><div style='margin:0 auto; text-align: center;'><form method='post' action='".JURI::base()."index.php?option=com_billing&task=saveperstariff&id=$id&view='>";
	echo JText::_('COM_BILLING_FOR_DAYS') . " <select class='select' name='days'>";
	for($i=1; $i<13; $i++)
		{
			if ($bal < ($price * $i))
			{
				break;
			}
			echo "<option value='". (30 * $i) ."'>". (30 * $i)." ".JText::_('COM_BILLING_DAYS')." - ". FormatDefaultCurrency($price * $i) . "</option>";
		}
	echo "</select> ".JText::_('COM_BILLING_DAYS')."<br><br>";
	echo '<input type="submit" value="'.JText::_('COM_BILLING_EXTEND').'">';
	echo "</form></div></div>";		
}
	
function SavePTariff()
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	$UA = new UserAccount();	
	
	$days = JRequest::getInt('days');	
	$UA->ProlongPersonalTariff($uid, $days);
}	

function ShowDigital()
{
	$user = JFactory::getUser();
	$uid = $user->id;

	$db = JFactory::getDBO();
	$query = "select * from `#__billing_pin_type` where is_active = 1 and (best_before = '0000-00-00 00:00:00' or best_before > NOW()) and pin_type = 2 order by pin_name";
	$result = $db->setQuery($query); 
	$rows = $db->loadObjectList();
	if(count($rows) > 0)
	{
		echo "<table border='0' width='100%' class='digital'>";
		echo "<thead class='dhead'>";
		echo "<tr style='border: none;' class='digitaltr'>";
		echo "<th style='border: none;' class='digitalth'>".JText::_('COM_BILLING_TITLE')."</th>";
		echo "<th style='border: none;' width='3%' class='digitalth'></th>";
		echo "<th style='border: none;' class='digitalth'>".JText::_('COM_BILLING_PRICE')."</th>";
		echo "<th style='border: none;' width='3%' class='digitalth'></th>";
		echo "<th style='border: none;' class='digitalth'>".JText::_('COM_BILLING_DESCRIPTION')."</th>";
		echo "<th style='border: none;' width='3%' class='digitalth'></th>";
		echo "<th style='border: none;' class='digitalth'>".JText::_('COM_BILLING_ACTIONS')."</th>";
		echo "</tr>";
		echo "</thead>";
		foreach($rows as $row)
		{
			echo "<tr style='border: none;' class='digitaltr'>";
			echo "<td class='digitaltd' style='border: none;'>$row->pin_name</td>";
			echo "<td class='digitaltd' width='3%' style='border: none;'></td>";
			echo "<td class='digitaltd' style='border: none;'>".FormatDefaultCurrency($row->price)."</td>";
			echo "<td class='digitaltd' style='border: none;' width='3%'></td>";
			echo "<td class='digitaltd' style='border: none;'>$row->instructions</td>";
			echo "<td class='digitaltd' style='border: none;' width='3%'></td>";
			if($uid != 0)
			{
				echo "<td class='digitaltd' style='border: none;'><a href='".JURI::base()."index.php?option=com_billing&view=billing&task=buypin&id=$row->id'>".JText::_('COM_BILLING_BUY_ACCESS')."</a></td>";
			}
			else
			{
				echo "<td class='digitaltd' style='border: none;'><a href='".JURI::base()."index.php?option=com_users&view=login'>".JText::_('COM_BILLING_ENTERANDBUY')."</a></td>";
			}
			echo "</tr>";
		}
		echo "</table>";
	}
}

function BuyPin($id)
{
	$uid = CheckLogin();
	if ($uid == 0) return;

	$db = JFactory::getDBO();	
	$query = "select * from `#__billing_pin_type` where id = $id";
	$result = $db->setQuery($query);   
	$pin = $db->loadAssoc(); 

	if($pin['is_active'] == 1 and $pin['pin_type'] == 2 and ($pin['best_before']=='0000-00-00 00:00:00' or $pin['best_before'] > date('Y-m-d H:i:s')))
	{
		$UA = new UserAccount();	
		$bal = $UA->GetBalance($uid);
		if($bal >= $pin['price'])
		{
			$code = $UA->GetNextPinCode($id);
			if($code != '')
			{
				$UA->ActivatePinCode($code, $uid);
				$pid = date('His') . $uid;
				$UA->WithdrawMoney($uid, $pin['price'], $pid, JText::_('COM_BILLING_PIN_CODE').": $code " . $pin['pin_name']);
				echo "<p>".JText::_('COM_BILLING_TITLE').": ".$pin['pin_name']."</p>";
				echo "<p>".JText::_('COM_BILLING_PIN_CODE').": $code</p>";
				echo "<p>".JText::_('COM_BILLING_PIN_CODE_SENT')."</p>";
				$message = JText::sprintf('COM_BILLING_PIN_CODE_MES', $pin['pin_name'], JURI::base()) .
					"<p>".JText::_('COM_BILLING_PIN_CODE').": $code</p>";
				$subj = JText::_('COM_BILLING_SERVICE_NOTIFICATION2');
				EmailToUser1($uid, $message, $subj);
				//EmailToAdmin($message, $subj);
			}
			else
			{
				$subj = JText::_('COM_BILLING_PIN_CODE_ERR');
				$message = JText::_('COM_BILLING_PIN_CODE_ERR2') . ' ' . $id;
				echo "<p>".JText::_('COM_BILLING_PIN_CODE_GET')."</p>";
				EmailToAdmin($message, $subj);
			}			
		}
		else
		{
			$UA->SiteMessage(JText::_('COM_BILLING_NO_MONEY_ACCESS'));
			ShowDigital();
		}
	}
	else
	{
		$UA->SiteMessage(JText::_('COM_BILLING_PIN_CODE_GET'));
		ShowDigital();
	}
	
}
	
	
function SendInvite($uid)
{
	$uid1 = CheckLogin();
	if ($uid1 == 0) return;
	
	if($uid != $uid1) return;
	
	if(!GetOption('invite')) return;
	
	$text = GetOption('invite_text', true);
	$subj = GetOption('invite_subj');
	$email = JRequest::getVar('email');
	if (eregi("^[a-zA-Z0-9_]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$]", $email))
	{
		return FALSE;
	}
	
	$db = JFactory::getDBO();	
	$query = "select * from `#__users` where id = $uid";
	$result = $db->setQuery($query);   
	$user = $db->loadAssoc(); 
	$fromname = $user['name'];
	$from = $user['email'];
	$recipient[] = $email; 
	
	$url2 = JRoute::_( JURI::base() . "index.php?option=com_billing&partnername=". GetUserURLName($uid), true);
	$text = str_replace('%partner_url%', $url2, $text);
	$text = str_replace('%username%', GetUserNameX($uid), $text);
	
	if (JoomlaVersion() == '1.5')
	{
		return JUtility::sendMail($from, $fromname, $recipient, $subj, $text, 1);
	}
	else
	{
		return JFactory::getMailer()->sendMail($from, $fromname, $recipient, $subj, $text, 1);
	}
	
}

function VoteArticle($id)
{
	$uid = CheckLogin();
	if ($uid == 0) return false;
	if(!isset($id))	return false;
	
	if(GetOption('vote'))
	{
		$price = GetOption('voteprice');
		if($price > 0)
		{
			$UA = new UserAccount();
			$b = $UA->GetBalance($uid);
			if($price > $b)
			{
				$UA->SiteMessage(JText::_('COM_BILLING_VOTE_NO'));
				return false;
			}
			$descr = JText::_('COM_BILLING_VOTE_MES') . " ID $id";
			$UA->WithdrawMoney($uid, $price, '', $descr);
			
			$db = JFactory::getDBO();	
			$query = "select count(*) from `#__billing_articles` where article_id = $id";
			$result = $db->setQuery($query);   
			$count = $db->loadResult(); 
			if($count == 0)
			{
				$query = "insert into `#__billing_articles` (article_id, is_active, price) values ($id, 1, $price)";
				$result = $db->setQuery($query);   
				$result = $db->query(); 
			}
			
			$dt = date('Y-m-d H:i:s');
			$query = "insert into `#__billing_articles_user` (article_id, uid, paymentdate, price) values ($id, $uid, '$dt', $price)";
			$result = $db->setQuery($query);   
			$result = $db->query(); 
			return true;
		}
	}
	else
	{
		$UA->SiteMessage(JText::_('COM_BILLING_VOTE_DISABLED'));
		return false;
	}
}


function DirectPayment($id, $sum)
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_articles` where article_id = $id";
	$result = $db->setQuery($query);   
	$art = $db->loadObject();	
	$sum = $art->price;
	$_SESSION["price_$id"] = $sum;
	
	$query = 'select * from `#__billing_settings` where is_active = 1 and currency_id in 
		(select id from `#__billing_currency` where is_base = 1)  
			order by list_title, description';
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList();
	
	$out = '<select class="b_select" name="plugin_id" id="plugin_id">';
	if(count($rows) > 0)
	{
	  foreach ( $rows as $row )
	  {
		 if ($row->id == $id) {$s = 'selected';} else {$s = '';}
		 if($row->list_title != '')
		 {
			$out .= "<option value='$row->id' $s >$row->list_title</option>"; 	
		 }
		 else
		 {
			$out .= "<option value='$row->id' $s >$row->description</option>"; 
		 }
	  }
	}
	$out .= '</select>';
	
	echo '<form action="'.JURI::base() .'index.php?option=com_billing&task=pay&view=billing" name="payment" method="post">';	
	echo '<p class="billing_p" align="center"><input class="inputbox" type="hidden" name="sum" value="'.$sum.'" id="sm""> <span id="curlab">'. $sum . ' ' . GetDefaultCurrencyAbbr() . '</span> ' . $out . ' <input type="submit" name="SB" value="'.JText::_('COM_BILLING_NEXT').'"></p>';
	echo '</form>';	

}

function ShowUserArticles()
{
	$uid = CheckLogin();
	if ($uid == 0) return false;
	if(!isset($uid))	return false;
	
	$discount = GetUserGroupDiscount($uid);
	$db = JFactory::getDBO();
	$tabs = '';
	$UA = new UserAccount();
	if(GetOption('publisher'))
	{
		$pgroup = $UA->GetPublisherGroupId();	
		if($UA->IsUserInGroup($uid, $pgroup))
		{
			$pub = true;
		}
		else
		{
			$pub = false;
		}
	}
	else
	{
		$pub = true;
	}
	
	$query = "select count(*) from `#__billing_group`  where enabled = 1";
	$result = $db->setQuery($query);
	$res = $db->loadResult();
	if($res > 0)
	{
		$gid = GetUserGroupId($uid);
		if($gid != '')
		{
			$query = "select * from `#__billing_group` where id = $gid";
			$result = $db->setQuery($query);   
			$row = $db->loadAssoc();
			$can = $row['option1'];
			if($can != 1)
			{
				$pub = false;
			}
		}
	}
	
	if(GetOption('author') and $pub)
	{
		$editor = JFactory::getEditor();
		$editor2 = JFactory::getEditor();
		$category_options = JHtml::_('category.options', 'com_content');
		//print_r($category_options);
		$cats = '<select name="category_id" id="category_id">';
		//$cats .= JHTML::_('select.options', $category_options);
		$cats .= GetCategoryList();
		$cats .= GetK2Categories(0);		
		$cats .= '</select>';

	
		$tabs .= "<form action='".JURI::base()."index.php?option=com_billing&task=addarticle' name='addarticle' method='post' enctype='multipart/form-data'>";
		$tabs .= '<h3>' . JText::_('COM_BILLING_MYARTICLE') . '</h3>';
		$tabs .= '<table width="100%" border="0" class="adminlist table billing_table_invisible">';
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_TITLE') . "</td><td><input class='inputbox' type='text' name='title' value='' size='80'></td></tr>";
		$tabs .= "<tr><td>".JText::_('COM_BILLING_INTRO')."</td><td>".$editor->display( 'artintro', '', '500', '200', '20', '20', false ) ."</td></tr>";
		$tabs .= "<tr><td>".JText::_('COM_BILLING_FULLTEXT')."</td><td>".$editor2->display( 'artbody', '', '500', '200', '20', '20', false ) ."</td></tr>";
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_CATEGORY') . '</td><td>' . $cats . " ".JText::_('JFIELD_LANGUAGE_LABEL') . ' ' . 
		GetLanguageList(). "</td></tr>";		
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_PRICE') . "</td><td><input class='inputbox' type='text' name='price' value='10.00' size='5'> ".GetDefaultCurrencyAbbr().'  (' .JText::_('COM_BILLING_YOUR_PERCENT'). ' ' .$UA->GetAuthorPercent($uid) ."% )</td><tr>";
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_SOLD_COUNT') . "</td><td><input class='inputbox' type='text' name='cnt' value='' size='2'></td></tr>";
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_ADDFILE') ."</td><td><input class='inputbox' type='file' name='file1'> ".JText::_('COM_BILLING_DESCRIPTION')." <input class='inputbox' type='text' name='desc1' value='' size='40'></td></tr>";
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_ADDFILE') ."</td><td><input class='inputbox' type='file' name='file2'> ".JText::_('COM_BILLING_DESCRIPTION')." <input class='inputbox' type='text' name='desc2' value='' size='40'></td></tr>";
		$tabs .= '<tr><td>'.JText::_('COM_BILLING_ADDFILE') ."</td><td><input class='inputbox' type='file' name='file3'> ".JText::_('COM_BILLING_DESCRIPTION')." <input class='inputbox' type='text' name='desc3' value='' size='40'></td></tr>";
		
		$tabs .= "</table><p align='right'><input type='submit' value='".JText::_('COM_BILLING_SAVE')."'></p>";
		$tabs .= "</form>";
	}
	
	if($UA->ExtensionExists('com_k2'))
	{
		$query = 'select distinct a.*, '.
		'(select state from `#__billing_articles_user` where article_id = a.article_id and uid = '.$uid.') as state, '.
		'(select title from `#__content` where id = a.article_id) as title, '.
		'(select title from `#__k2_items` where id = a.article_id) as k2title, '.
		'(select created_by from `#__content` where id = a.article_id) as author '.
		" from `#__billing_articles` a where article_id in (select id from `#__content` where created_by = $uid)";
	}
	else
	{
		$query = 'select distinct a.*, '.
		'(select state from `#__billing_articles_user` where article_id = a.article_id and uid = '.$uid.') as state, '.
		'(select title from `#__content` where id = a.article_id) as title, '.
		"'' as k2title, ".
		'(select created_by from `#__content` where id = a.article_id) as author '.
		" from `#__billing_articles` a where article_id in (select id from `#__content` where created_by = $uid)";
	}
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 	
	
	$tabs .= '<h2>' . JText::_('COM_BILLING_MY_ARTICLES') . '</h2>';
   
	$tabs .= '<table width="100%" class="adminlist table billing_table">';
	$tabs .= "<thead>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_TITLE')."</th>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_COST')."</th>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_STATE')."</th>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_VIEW')."</th>";
	$tabs .= "  <th class='b_title'>".JText::_('COM_BILLING_ACTIONS')."</th>";
	$tabs .= "</thead>";
		
	if(count($rows) > 0)	
	{
		foreach ( $rows as $row ) 
		{
			$tabs .= '<tr>';
			if($row->k2 == 1)
			{
				$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_k2&view=item&id=$row->article_id'>$row->k2title</a></td>";
			}
			else
			{
				$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_content&view=article&id=$row->article_id'>$row->title</a></td>";
			}
			if ($row->price != '')
			{
				if ($discount > 0 and $row->price > 0)
				{
					$xp = $row->price - $row->price/ 100 * $discount;
					$xp = round($xp, 2);
					$tabs .= "<td>". FormatDefaultCurrency($xp) ." (".JText::_('COM_BILLING_DISCOUNT')." $discount %)</td>";
				}
				else
				{
					$tabs .= "<td>". FormatDefaultCurrency($row->price) ."</td>";
				}
			}
			else
			{
				$tabs .= "<td></td>";
			}
			if($row->state == 1)
			{		
				$tabs .= "<td><img width='16' height='16' border='0' src='".tick."'></td>";
			}
			else
			{
				$tabs .= "<td><img width='16' height='16' border='0' src='".publish_x."'></td>";
			}
			if($row->k2 == 1)
			{
				$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_k2&view=item&id=$row->article_id'>".JText::_('COM_BILLING_VIEW')."</a></td>";
			}
			else
			{
				$tabs .= "<td><a class='billing_a' href='".JURI::base()."index.php?option=com_content&view=article&id=$row->article_id'>".JText::_('COM_BILLING_VIEW')."</a></td>";
			}
			if($row->author == $uid)
			{
				if($row->is_active == 1)
				{
					$tabs .= "<td><a href='".JURI::base()."index.php?option=com_billing&task=actart&id=$row->id' title='".JText::_('COM_BILLING_HIDE')."'><img width='16' height='16' border='0' src='".tick."'>";
				}
				else
				{
					$tabs .= "<td><a href='".JURI::base()."index.php?option=com_billing&task=actart&id=$row->id' title='".JText::_('COM_BILLING_PUBLISH')."'><img width='16' height='16' border='0' src='".publish_x."'>";
				}
				$tabs .= "</a></td>";
			}
			else
			{
				$tabs .= "<td></td>";
			}
			$tabs .= '</tr>';
		}
	}
	$tabs .= '</table><hr>';			

	echo $tabs;
}

function UserpointsView()
{
	$uid = CheckLogin();
	if ($uid == 0) return false;
	if(!isset($uid))	return false;
	$pane = '';
	echo GetUserPointsTab($uid, $pane, false);
}

function PartnerView()
{
	$uid = CheckLogin();
	if ($uid == 0) return false;
	if(!isset($uid))	return false;
	$pane = '';
	echo GetPartnerTab($uid, $pane, false);
}

?>
