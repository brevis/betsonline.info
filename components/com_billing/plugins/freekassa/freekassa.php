<?php

defined('_JEXEC') or die('Restricted access');

include_once (JPATH_ROOT . '/components/com_billing/useraccount.php');
include_once (JPATH_ROOT . '/components/com_billing/account.php');
include_once (JPATH_ROOT . '/components/com_billing/plugin.php');

Class Freekassa extends BillingPlugin 
    {
        function Freekassa($pname)
	{
            BillingPlugin::BillingPlugin($pname);
			if ($this->res_url == '')
            {
                $this->res_url = JURI::base() . "index.php?option=com_billing&view=billing&format=raw&task=process&pid=$this->id";
				$this->res_url = str_replace('administrator/', '', $this->res_url);
            }
            if ($this->success_url == '')
            {
				$this->success_url = JURI::base() . "index.php?option=com_billing&task=ok&pid=$this->id";
				$this->success_url = str_replace('administrator/', '', $this->success_url);
            }
            if ($this->fail_url == '')
            {
				$this->fail_url = JURI::base() . "index.php?option=com_billing&task=fail&pid=$this->id";
				$this->fail_url = str_replace('administrator/', '', $this->fail_url);
            }

			$lang = JFactory::getLanguage();
			$lang->load('com_billing');
        }
        
	function Init()
	{
		parent::Init();
		return true;
	}

	function StartPayment($amnt, $uid = '')
	{
		parent::StartPayment($amnt, $uid);
	
        if($uid == '')
		{
			$user = JFactory::getUser();
			$uid = $user->id;
		}

		if ($uid == 0) 
		{
		   JFactory::getApplication()->enqueueMessage( JText::_('COM_BILIING_AUTH_NEED') );
			return false;
		}            
		parent::StartPayment($amnt);
		
		$mrh_login = $this->login;
		$mrh_pass1 = $this->pass1;
		$inv_desc = JText::_('COM_BILLING_ADD2_MONEY');
		$out_summ = $amnt;

		//ID Вашего магазина:Сумма платежа:Секретное слово:Номер заказа
		$crc  = md5("$mrh_login:$out_summ:$mrh_pass1:$uid");

		$surl = 'https://www.free-kassa.ru/merchant/cash.php';
			
		if ($this->currency_id != '')
		{
			$cid = $this->currency_id;
			echo '<p align="center">'.JText::_('COM_BILLING_WILL_PAY'). ' ' . FormatCurrency($cid, $amnt) . '</p>';
		}
		else
		{
			$cid = -1;
			echo '<p align="center">'.JText::_('COM_BILLING_WILL_PAY'). ' ' . FormatDefaultCurrency($amnt) . '</p>';	
		}

		echo $this->instructions;

		$form = "<form action='$surl' method=GET>".
			  "<input type=hidden name='m' value='$mrh_login'>".
			  "<input type=hidden name='oa' value='$out_summ'>".
			  "<input type=hidden name='o' value='$uid'>".
			  "<input type=hidden name='lang' value='ru'>".
			  "<input type=hidden name='s' value='$crc'>".
			  "<input type=submit value='".JText::_('COM_BILLING_GOTO_PAY')."'></p>".
			  "</form>";			

		echo $form;
		return true;
	}
	
	function getIP() {
		if(isset($_SERVER['HTTP_X_REAL_IP'])) return $_SERVER['HTTP_X_REAL_IP'];
		   return $_SERVER['REMOTE_ADDR'];
	}
	
	function Process()
	{
		parent::Process();
		$this->Log('Start processing');

		/*
		if (!in_array($this->getIP(), array('136.243.38.147', '136.243.38.149', '136.243.38.150', '136.243.38.151', '136.243.38.189'))) {
			$this->Log('Incorrect IP: ' . $this->getIP());
			return false;
		}
		*/
		
		$db = JFactory::getDBO();
		$query = "select * from `#__billing_settings` where paysystem='freekassa'";
		$result = $db->setQuery($query);
		$row = $db->loadAssoc();
		
		$this->Log(print_r($_REQUEST, true));
		
		$mrh_pass2 = $row['pass2'];
		$MERCHANT_ID = $_REQUEST["MERCHANT_ID"];
		$intid = $_REQUEST["intid"];
		$uid = $_REQUEST["MERCHANT_ORDER_ID"];
		$P_EMAIL = $_REQUEST["P_EMAIL"];
		$P_PHONE = $_REQUEST["P_PHONE"];
		$SIGN = $_REQUEST["SIGN"];		
		$AMOUNT = $_REQUEST['AMOUNT'];

		$crc = strtoupper($SIGN);
		$my_crc = strtoupper(md5("$MERCHANT_ID:$AMOUNT:$mrh_pass2:$uid"));	
		
		if ($my_crc == $crc)
		{
			$this->Log("Success: $intid");
			$this->Log("Making payment for order $intid...");
			$this->Success2($AMOUNT, $uid, $intid);
			echo 'YES';
			return;
		}
		else
		{	$this->Log("Fail: $crc != $my_crc");
			echo 'fail';
			return;
		}
	}
	
	function Success2($amount, $uid, $pid)
	{
		$app = JFactory::getApplication();
		if (preg_match('/^\-/', $uid))
		{
			// TODO: $app->getSession()->set('casino_redirect', 1);
			try
			{
				include JPATH_ROOT . '/casino/lobby/fk_conf.php';
				include JPATH_ROOT . '/casino/setup.php';
				include JPATH_ROOT . '/casino/lobby/fk_result.php';
			}
			catch (Exception $e)
			{

			}
			return;
		}
		else
		{
			// TODO: $app->getSession()->set('casino_redirect', null);
		}

		$db = JFactory::getDBO();
		$query = "select count(id) from `#__billing` where payment_id = $pid";
		$result = $db->setQuery($query);
		$exists = $db->loadResult();
		if($exists > 0)
		{
			echo 'YES';
			return;
		}
	
		$UA = new UserAccount;
		$this->Log("$uid, $amount, $pid, $this->currency_id");
		$this->Log("Before comission: $amount");
		if($this->ps_comission != '' and $this->ps_comission > 0 and $this->ps_comission < 100)
		{			
			$amount = $amount - $amount / 100 * $this->ps_comission;
			$this->Log("After comission: $amount");
		}
		$UA->AddMoneyEasyCurr($uid, $amount, $pid, JText::_('COM_BILLING_ACC_MONEY').' Freekassa. ID: ' . $pid, $this->currency_id);
		$this->Log('ok');
		
		$this->uid = $uid;
		$this->amount = $amount;
		$this->Success(false);
	}
	
	function Fail()
	{
	    JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLING_PAY_PROBLEM') );
		return false;
	}
}

?>