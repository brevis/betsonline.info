<?php

defined('_JEXEC') or die('Restricted access');

include_once (JPATH_ROOT . '/components/com_billing/useraccount.php');
include_once (JPATH_ROOT . '/components/com_billing/plugin.php');

Class Reserved extends BillingPlugin 
    {
        //var $testmode = true;
        
        function Reserved($pname)
		{
            BillingPlugin::BillingPlugin($pname);
			$lang = JFactory::getLanguage();
			$lang->load('com_billing');
        }
        
	function Init()
	{
		parent::Init();
		return true;
	}

	function StartPayment($amnt, $uid = '')
	{
        if($uid == '')
			{
				$user = JFactory::getUser();
				$uid = $user->id;
			}
	
        	if ($uid == 0) 
        	{
                JFactory::getApplication()->enqueueMessage( JText::_('COM_BILIING_AUTH_NEED') );
                return false;
            }            
            parent::StartPayment($amnt);
			
			if ($this->comission != '' and $this->comission > 0 and $this->comission < 100 )
			{				
				$x = ($amnt / 100 * $this->comission) + $amnt;
				$x = round($x, 2);
				echo '<p>'.JText::_('COM_BILLING_COMISSION1'). ' '.$this->comission. ' %. '.JText::_('COM_BILLING_COMISSION2'). ' '. FormatCurrency($this->currency_id, $amnt) .' '.JText::_('COM_BILLING_COMISSION3').' '. FormatDefaultCurrency($x) . '</p>';	
			}
			else
			{
				$x = $amnt;
				//echo '<p>'.JText::_('COM_BILLING_RES1').' '.FormatCurrency($this->currency_id, $amnt).'</p>';
			}

			echo "<p>".JText::_('COM_BILLING_RES1')." ". FormatCurrency($this->currency_id, $amnt) .". ".JText::_('COM_BILLING_RES2')." ". FormatCurrency($this->currency_id, $x) . '</p>';
			echo "<p>".JText::_('COM_BILLING_RES3')."</p>";
			echo "<p>".JText::_('COM_BILLING_RES4')."</p>";
			
			echo $this->instructions;
			
			if ($this->comission != '' and $this->comission > 0 and $this->comission < 100)
			{
				$value = $x;
			}
			else
			{
				$value = $amnt;
			}
			
			$pid = date('His') .$uid;
			
			$UA = new UserAccount;
			$UA->AddMoneyEasyCurr($uid, $value, $pid, JText::_('COM_BILLING_RES5'), $this->currency_id, '', 1);

			return true;
	}
	
	function Process()
	{
		parent::Process();
	}
	
	function Success()
	{
		parent::Success();
	}
	
	function Fail()
	{
		parent::Fail();
	    JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLING_PAY_PROBLEM') );
		return false;
	}
    }

?>