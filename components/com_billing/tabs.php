<?php
defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();
$document->addStyleSheet('/components/com_billing/tabs.css');
jimport( 'joomla.html.pane' );

class BillingTabs
{
	var $pane;
	var $tabType = 0;
	
	public function StartTabs($tabno = 0, $tabType = 0)	{
		$options = array(
			'onActive' => 'function(title, description){
				description.setStyle("display", "block");
				title.addClass("open").removeClass("closed");
			}',
			'onBackground' => 'function(title, description){
				description.setStyle("display", "none");
				title.addClass("closed").removeClass("open");
			}',
			'startOffset' => $tabno,  // 0 starts on the first tab, 1 starts the second, etc...
			'useCookie' => false, // this must not be a string. Don't use quotes.
		);
		if($tabType == 1)
		{
			$this->$tabType = $tabType;
			return 
			'<script type="text/javascript">    
			$(function(){
				$("dl.tabs dt").click(function(){
					$(this)
						.siblings().removeClass("active").end()
						.next("dd").andSelf().addClass("active");
				});
			});
			</script> <dl class="tabs">';
		}
	
		if (JVERSION >= '3.0') {
			return JHtml::_('tabs.start', 'tabs', $options);
		} 
		else {
			$this->pane = JPane::getInstance('tabs', $options);
			return $this->pane->startPane('pane');
		}
	}
	
	public function EndTabs() {
		if($this->tabType == 1)
		{
			return '</dl>';
		}
		
		if (JVERSION >= '3.0') {
			return JHtml::_('tabs.end');	
		}
		else {
			return $this->pane->endPane();
		}
	}

	public function StartTab($title, $name, $active = false) {
		
		if($this->tabType == 1)
		{
			if($active)
			{
				return "<dt class='active'>$title</dt><dd class='active'><div class='intab'>";
			}
			else
			{
				return "<dt>$title</dt><dd><div class='intab'>";
			}
		}
		
		if (JVERSION >= '3.0') {
			return JHtml::_('tabs.panel', $title, $name);
		}
		else {
			return $this->pane->startPanel($title, $name);
		}
	}
	
	public function EndTab() {
		if($this->tabType == 1)
		{
			return "</div></dd>";
		}
	
		if (JVERSION < '3.0') {
			return $this->pane->endPanel();
		}
	}
}

?>