<?php
defined('_JEXEC') or die('Restricted access');

function UseAccounts()
{
	$db = JFactory::getDBO();
	$query = "select value from `#__billing_options` where `option` = 'useaccounts'";
	$result = $db->setQuery($query);  
	$result = $db->loadResult();
	
	return $result;
}

function GetAccountTypeByAccId($acc_id)
{
	$db = JFactory::getDBO();
	$query = "select account_type_name from `#__billing_account_type` where id in (select account_type from `#__billing_account` where id = $acc_id)";
	$result = $db->setQuery($query);   
	$result = $db->loadResult(); 	
	
	return $result;
}

function GetUserAccountSum($acc_type_id, $uid)
{
	$db = JFactory::getDBO();
	$query = "select sum(val) from `#__billing` where uid = $uid and waiting = 0 and acc in (select id from `#__billing_account` where account_type = $acc_type_id and uid = $uid)";
	$result = $db->setQuery($query);   
	$result = $db->loadResult(); 	
	
	return $result;
}

function GetUserAccount($acc_type_id, $uid)
{
	$db = JFactory::getDBO();
	$query = "select id from `#__billing_account` where uid = $uid and account_type = $acc_type_id LIMIT 0, 1";
	$result = $db->setQuery($query);   
	$result = $db->loadResult(); 	
	
	return $result;
}

function GetAccountCurrency($acc_id)
{
	$db = JFactory::getDBO();
	$query = "select currency_id from `#__billing_account_type` where id in (select account_type from `#__billing_account` where id = $acc_id)";
	$result = $db->setQuery($query);   
	$result = $db->loadResult(); 	
	
	return $result;
}

function GetDefaultCurrency()
{
	if(isset($_SESSION['default_currency']))
	{
		return $_SESSION['default_currency'];
	}
	$db = JFactory::getDBO();
	$query = "select id from `#__billing_currency` where is_base = 1 limit 0,1";
	$result = $db->setQuery($query);   
	$result = $db->loadResult(); 	
	$_SESSION['default_currency'] = $result;
	return $result;	
}

function GetPluginCurrency($paysystem)
{
	$db = JFactory::getDBO();
	$query = "select currency_id from `#__billing_settings` where paysystem = '$paysystem'";
	$result = $db->setQuery($query);   
	$result = $db->loadResult(); 	
	
	return $result;	
}

function FormatCurrency($cid, $val, $fd = 2)
{
	if($cid == '' or $cid == 0)
	{
		return $val;
	}
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_currency` where id = $cid";
	$result = $db->setQuery($query);   
	$currency = $db->loadAssoc();
	if ($currency['before_money'] > 0 )
	{
		if($val < 0 )
		{
			return '-' . $currency['abbreviation'] . abs(round($val, $fd));
		}
		else
		{
			return $currency['abbreviation'] . round($val, $fd); 
		}
	}
	else
	{
		return round($val, $fd) . ' ' . $currency['abbreviation']; 
	}
}

function FormatDefaultCurrency($val)
{
	return FormatCurrency(GetDefaultCurrency(), $val);
}

function FormatBillingDate($date)
{
	if(isset($_SESSION['date_format']))
	{
		$result = $_SESSION['date_format'];
	}
	else
	{
		$db = JFactory::getDBO();
		$query = "select value from `#__billing_options` where `option` = 'date_format'";
		$result = $db->setQuery($query);  
		$result = $db->loadResult();
		if($result == '')
		{
			$result = 'd.m.y H:i:s';
		}
		$_SESSION['date_format'] = $result;
	}
	return date($result, strtotime($date));
}

function GetCurencyRate($cid)
{
	if($cid == '' or $cid == 0)
	{
		return 1;
	}
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_currency` where id = $cid";
	$result = $db->setQuery($query);   
	$currency = $db->loadAssoc();
	return $currency['base_rate'];
}

function GetCurrencyRate($cid)
{
	if($cid == '' or $cid == 0)
	{
		return 1;
	}
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_currency` where id = $cid";
	$result = $db->setQuery($query);   
	$currency = $db->loadAssoc();
	return $currency['base_rate'];
}

function ConvertCurrency($cid1, $cid2, $val)
{
	if ($cid1 == $cid2)
	{
		return $val;
	}
	if($cid1 == '' or $cid1 == 0 or $cid2 == '' or $cid2 == 0)
	{
		return $val;
	}
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_currency` where id = $cid1";
	$result = $db->setQuery($query);   
	$currency = $db->loadAssoc();
	$query = "select * from `#__billing_currency` where id = $cid2";
	$result = $db->setQuery($query);   
	$currency2 = $db->loadAssoc();
	
	if($currency2['base_rate'] == 0)
	{
		return '';
	}
	
	if ($currency['is_base'] == 1)
	{
		return $val / $currency2['base_rate'];
	}
	
	if ($currency2['is_base'] == 1 and $currency['base_rate'] != 0)
	{
		return $val * $currency['base_rate'];
	}

	if ($currency2['base_rate'] == 0 or $currency['base_rate'] == 0)
	{
		return $val;
	}
	$val = $val / $currency['base_rate'];
	$val = $val * $currency2['base_rate'];
	return $val;
}

function GetDefaultCurrencyAbbr()
{
	if(isset($_SESSION['default_currency_abbr']))
	{
		return $_SESSION['default_currency_abbr'];
	}
	$db = JFactory::getDBO();
	$query = "select abbreviation from `#__billing_currency` where is_base = 1 limit 0,1";
	$result = $db->setQuery($query);   
	$result = $db->loadResult(); 	
	$_SESSION['default_currency_abbr'] = $result;
	return $result;	
}

function GetCurrencyAbbr($currency_id)
{
	if($currency_id == '')
	{
		return GetDefaultCurrencyAbbr();
	}
	
	$db = JFactory::getDBO();
	$query = "select abbreviation from `#__billing_currency` where id = $currency_id";
	$result = $db->setQuery($query);   
	$result = $db->loadResult(); 	
	
	return $result;	
}

function GetAccountCurrencyAbbr($acc_type_id)
{
	$db = JFactory::getDBO();
	$query = "select abbreviation from `#__billing_currency` where id in (select currency_id from `#__billing_account_type` where id = $acc_type_id)";
	$result = $db->setQuery($query);   
	$result = $db->loadResult(); 	
	
	return $result;	
}

function GetCurrencyAbbr3($currency_id)
{
	$db = JFactory::getDBO();
	$query = "select abbr3 from `#__billing_currency` where id = $currency_id";
	$result = $db->setQuery($query);   
	$result = $db->loadResult(); 	
	
	return $result;	
}

function GetAccountList($show_all = 1, $field_name = 'acc_id', $acc_id = 0, $income = 0, $out = 0, $partner = 0, $money = 0, $deposit = 0, $credit = 0, $fromuser = 0, $subscribe = 0, $x_flag_1 = 0, $x_flag_2 = 0, $x_flag_3 = 0, $x_flag_4 = 0, $x_flag_5 = 0)
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_account_type`";
	if ($show_all != 1)
	{
		$query .= ' where ';
		$and = '';
		if ($income != 0) {$query .= ' income_flag = 1 '; $and = ' and ';}
		if ($out != 0) {$query .= " $and out_flag = 1 "; $and = ' and ';}
		if ($partner != 0) {$query .= " $and partner_flag = 1 "; $and = ' and ';}
		if ($money != 0) {$query .= " $and money_flag = 1 "; $and = ' and ';}
		if ($deposit != 0) {$query .= " $and deposit_flag = 1 "; $and = ' and ';}
		if ($credit != 0) {$query .= " $and credit_flag = 1 "; $and = ' and ';}
		if ($fromuser != 0) {$query .= " $and fromuser_flag = 1 "; $and = ' and ';}
		if ($subscribe != 0) {$query .= " $and subscribe_flag = 1 "; $and = ' and ';}
		if ($x_flag_1 != 0) {$query .= " $and x_flag_1 = 1 "; $and = ' and ';}
		if ($x_flag_2 != 0) {$query .= " $and x_flag_2 = 1 "; $and = ' and ';}
		if ($x_flag_3 != 0) {$query .= " $and x_flag_3 = 1 "; $and = ' and ';}
		if ($x_flag_4 != 0) {$query .= " $and x_flag_4 = 1 "; $and = ' and ';}
		if ($x_flag_5 != 0) {$query .= " $and x_flag_5 = 1 ";}			
	}
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 		
	
	$out = "<select name='$field_name'>";
	if (count($rows) > 0)
	{
		foreach ( $rows as $row ) 
		{
			if ($acc_id != 0 and $acc_id == $row->id) {$c = 'selected';} else {$c = '';}
			$out .= "<option value='$row->id' $c>$row->account_type_name (".GetAccountCurrencyAbbr($row->id).")</option>";
		}
	}
	$out .= "</select>";
	return $out;
}

function GetCurrencyList($field_name = 'currency_id', $currency_id = '', $useabbr = false)
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_currency` where is_active = 1 order by id";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 		
	
	$out = "<select name='$field_name'>";
	if (count($rows) > 0)
	{
		foreach ( $rows as $row ) 
		{
			if ($currency_id != '' and $currency_id == $row->id) {$c = 'selected';} else {$c = '';}
			if($useabbr)
			{
				$out .= "<option value='$row->id' $c>$row->abbreviation</option>";
			}
			else
			{
				$out .= "<option value='$row->id' $c>$row->curency_name</option>";
			}
		}
	}
	$out .= "</select>";
	return $out;

}

function GetCashOutCurrency()
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_currency` where CashOut = 1";
	$result = $db->setQuery($query);   
	$currency = $db->loadAssoc();
	return $currency['id'];
}

function GetCashOutCurrencyWallet()
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_currency` where CashOut = 1";
	$result = $db->setQuery($query);   
	$currency = $db->loadAssoc();
	return $currency['WMW'];
}

function GetCashOutCurrencyLetter()
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_currency` where CashOut = 1";
	$result = $db->setQuery($query);   
	$currency = $db->loadAssoc();
	return $currency['WMLETTER'];
}

?>