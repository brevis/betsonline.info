<?php

defined('_JEXEC') or die('Restricted access');

//include_once (JPATH_ROOT . '/components/com_billing/billing_err.php');

define ("VIP", 'VIP');
define ("MV", "руб.");
define ("_BILLING", "true");
define ("BILLING_EXT", true);
define ("BILLING_VER", '2.9.2');
define ("multy", false);

$lang = JFactory::getLanguage();
$lang->load('com_billing');
$lang->load('com_billing', JPATH_ADMINISTRATOR );

//error_reporting(E_ALL) ;
//ini_set('display_errors', 'On');

if(BILLING_EXT)
{
	if(file_exists(JPATH_ROOT . '/components/com_billing/userpoints.php'))
	{
		include_once (JPATH_ROOT . '/components/com_billing/userpoints.php');
	}	
}

include_once (JPATH_ROOT . '/components/com_billing/account.php');
include_once (JPATH_ROOT . '/components/com_billing/pluginfactory.php');

$db     = JFactory::getDBO();
$query  = "select * from `#__billing_updates` order by id desc limit 0, 1";
$result = $db->setQuery($query);   
$bet    = $db->loadAssoc();
if ($bet['db_ver'] != BILLING_VER)
{
	BillingLogMessage('System', 'DB update', 'Update: ' . $bet['db_ver'] . '->' . BILLING_VER);
	if(file_exists(JPATH_ROOT . '/components/com_billing/update.php'))
	{
		BillingLogMessage('System', 'DB update', 'File exists. Start update');
		include_once (JPATH_ROOT . '/components/com_billing/update.php');
		UpdateBilling(BILLING_EXT);
		AddBillingData(BILLING_EXT);
		$d = date('Y-m-d H:i:s');
		$query = "insert into `#__billing_updates` (`db_ver`, `oper_date`) values ('".BILLING_VER."', '$d')";
		$result = $db->setQuery($query);   
		$db->query(); 
	}
}

function JoomlaVersion($long = false)
{
	$version = new JVersion;
    $joomla = $version->getShortVersion();
	if ($long)
	{
		return $version->getLongVersion();
	}
    else
	{
		return substr($joomla,0,3);
	}
}

function GetBillingIp()
{
	 if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
	 {
		$ip=$_SERVER['HTTP_CLIENT_IP'];
	 }
	 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
	 {
		$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	 }
	 else
	 {
		$ip=$_SERVER['REMOTE_ADDR'];
	 }
	 return $ip;
}

function BillingCheckUserLogin($login, $password)
{
	$db = JFactory::getDBO();
	$query = "select * from `#__users` where username = '$login'";
	$result = $db->setQuery($query);  
	$user = $db->loadAssoc();
	if($user == '')
	{
		return false;
	}
	$pass = $user['password'];
	BillingLogMessage('Useraccount', 'CheckUserLogin', "Joomla hash: $pass");
	BillingLogMessage('Useraccount', 'CheckUserLogin', "User data: $login, $password");
	$s = explode(':', $pass);
	$salt = $s[1];
	BillingLogMessage('Useraccount', 'CheckUserLogin', "Joomla salt: $salt");
	$crypt = JUserHelper::getCryptedPassword($password, $salt);
	$hash = $crypt.':'.$salt;
	BillingLogMessage('Useraccount', 'CheckUserLogin', "New hash: $pass");
	if($hash == $pass)	
	{
		return true;
	}
	else
	{
		return false;
	}		
}

function BillingGetOption($option, $text = false)
{
	if(isset($_SESSION[$option]))
	{
		return $_SESSION[$option];
	}
	$db = JFactory::getDBO();
	if($text)
	{
		$query = "select text_value from `#__billing_options` where `option` = '$option'";
	}
	else
	{
		$query = "select value from `#__billing_options` where `option` = '$option'";
	}
	$result = $db->setQuery($query);  
	$result = $db->loadResult();
	$_SESSION[$option] = $result;
	return $result;
}

function EmailToAdmin($message, $subj = 'Site system message', $attach = '')
{
	$db = JFactory::getDBO();
	$subject = $subj;
	$body = $message;
	$mode = 1;
	$config = JFactory::getConfig();

	$query = "select * from `#__users` where sendEmail = 1";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList();
	if(count($rows) > 0)
	{
		foreach($rows as $row)
		{
			$aname = $row->name;
			$aemail = $row->email;
			$from = $config->get('mailfrom');
			$fromname = $config->get('fromname');
			$recipient[] = $aemail;
			if (JoomlaVersion() == '1.5')
			{
				JUtility::sendMail($from, $fromname, $recipient, $subject, $body, $mode, '', '', $attach);
			}
			else
			{
				JFactory::getMailer()->sendMail($from, $fromname, $recipient, $subject, $body, $mode, '', '', $attach);
			}
		}
		
	}
	
	if(file_exists(JPATH_ROOT . '/components/com_jsms/jsmsapi.php'))
	{
		if(BillingGetOption('smsalladmin'))
		{
			include_once (JPATH_ROOT . '/components/com_jsms/jsmsapi.php');
			jsms_send_sms2admins($message);
		}
		if(BillingGetOption('smsadmin') and BillingGetOption('adminphone') != '')
		{
			include_once (JPATH_ROOT . '/components/com_jsms/jsmsapi.php');
			$adminphone = BillingGetOption('adminphone');
			jsms_send_sms_phone($adminphone, $message);
		}
	}
	return;
}

function EmailToUser1($uid, $message, $subj = 'Billing notification', $backemail = '', $attach = '')
{
	$db = JFactory::getDBO();
	$config = JFactory::getConfig();
	if (JoomlaVersion() == '1.5')
	{
		$query = "select * from `#__users` where id = 62";
	}
	else
	{
		$query = "select * from `#__users` where id = 42";
	}
	$result = $db->setQuery($query);   
	$row = $db->loadAssoc(); 
	$aname = $row['name'];
	if($backemail != '')
	{
		$aemail = $backemail;
	}
	else
	{
		$aemail = $config->get('mailfrom');
	}
	
	$query = 'select * from `#__users` where id = '. $uid;
	$result = $db->setQuery($query);   
	$row = $db->loadAssoc(); 
	
	$name = $row['name'];
	$email = $row['email'];
	
	$message = JText::_('COM_BILLING_HELLO') . ' ' . $name .'!<br>'. $message . '<br><br> ' . JText::_('COM_BILLING_AUTO_MESSAGE');
	
	$from = $aemail;
	$fromname = $config->get('fromname');
	$recipient[] = $email; 
	$subject = $subj;
	$body = $message;
	$mode = 1;
	 
	if (JoomlaVersion() == '1.5')
	{
		JUtility::sendMail($from, $fromname, $recipient, $subject, $body, $mode, '', '', $attach);
	}
	else
	{
		JFactory::getMailer()->sendMail($from, $fromname, $recipient, $subject, $body, $mode, '', '', $attach);
	}
	if(file_exists(JPATH_ROOT . '/components/com_jsms/jsmsapi.php'))
	{
		if(BillingGetOption('smsuser'))
		{
			$UA = new UserAccount();
			if(BillingGetOption('smspayd'))
			{				
				$b = $UA->GetBalance($uid);
				if(BillingGetOption('smsprice') > $b)
				{
					return;
				}
			}
			include_once (JPATH_ROOT . '/components/com_jsms/jsmsapi.php');
			if (jsms_send_sms($uid, $message) and BillingGetOption('smspayd'))
			{
				$pid = date('His') . $uid;
				$descr = 'SMS Notification';
				$UA->WithdrawMoney($uid, BillingGetOption('smsprice'), $pid, $descr);
			}
		}
	}
	return;
}

function IsAlreadySend($send_date, $days, $uid)
{
	$db = JFactory::getDBO();
	$query = "select count(*) from `#__billing_mailhistory` where send_mail = '$send_date' and days_before = $days and uid = $uid";
	$result = $db->setQuery($query);   
	$n = $db->loadResult();
	if ($n >0 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function NotifyCron($days, $message)
{
	$db = JFactory::getDBO();
	
	$nowdate = date("Y-m-d");	
	$unixdate = strtotime($nowdate);
	$newdate = $unixdate + ($days + 1)*86400;
	$newdate = date("Y-m-d", $newdate);
	
	$query = "select * from `#__billing_subscription` where enddate = '$newdate' and frozen = 0";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 	
	foreach ( $rows as $row ) 
	{
		$uid = $row->uid;
		if (!IsAlreadySend($nowdate, $days, $uid))
		{
			EmailToUser1($uid, $message);
			$query = "insert into `#__billing_mailhistory` (uid, endsubscription, days_before, send_mail)".
			" values ($uid, '$newdate', $days, '$nowdate')";
			$result = $db->setQuery($query);
			$result = $db->query();
		}
	}
}

function GetUserGroupDiscount($uid)
{
      $db = JFactory::getDBO();
	  if($uid == 0) {return 0;}
	  
	  $query = "select value from `#__billing_options` where `option` = 'usegroups'";
	  $result = $db->setQuery($query);  
	  $ok = $db->loadResult();

	  $query = "select count(*) from `#__billing_user_group` where uid = $uid";
	  $result = $db->setQuery($query);  
	  $exists = $db->loadResult();	
	  
	  if($exists > 0)
	  {	
		$query = "select discount_enabled from `#__billing_user_group` where uid = $uid";
		$result = $db->setQuery($query);  
		$ok1 = $db->loadResult();	  
	  }
	  else
	  {
		$ok1 = 0;
	  }
	  
	  if ($ok == true and $ok1 == 1)
	  {
		  $query = "select discount from `#__billing_group` where id in (select group_id from `#__billing_user_group` where uid = $uid)";
		  $result = $db->setQuery($query);   
		  $result = $db->loadResult();
		  return $result; 
	  }
	  else
	  {
			return 0;
	  }
}

function GetUserGroup($uid, $show_discount = 0)
{
      $db = JFactory::getDBO();
      $query = "select * from `#__billing_group` where id in (select group_id from `#__billing_user_group` where uid = $uid)";
      $result = $db->setQuery($query);   
	  $row = $db->loadAssoc();
	  if ($row['group_name'] == '')
	  {
			$query = 'select id from `#__billing_group` where is_default = 1 limit 0, 1';
			$result = $db->setQuery($query);   
			$defid = $db->loadResult();
			
			if($defid != '')
			{
				$add_date = date('Y-m-d H:i:s');
				$user = JFactory::getUser();
				$myid = $user->id;
				$query = "insert into `#__billing_user_group` (group_id, uid, add_date, add_by, discount_enabled) values($defid, $uid, '$add_date', $myid, 1)";
				$result = $db->setQuery($query); 
				$db->query();
			}
			$query = "select * from `#__billing_group` where id in (select group_id from `#__billing_user_group` where uid = $uid)";
			$result = $db->setQuery($query);   
			$row = $db->loadAssoc();
	  }
	  if ($show_discount != 0)
	  {
			return $row['group_name'] . ' (' . $row['discount'] . '%)';
	  }
	  else
	  {
			return $row['group_name'];
	  }
}

function GetUserGroupId($uid)
{
      $db = JFactory::getDBO();
      $query = "select group_id from `#__billing_user_group` where uid = $uid order by id limit 1";
      $result = $db->setQuery($query);   
	  return $db->loadResult();
}

function BillingLogMessage($type, $name, $text, $ext = '')
{
	$db = JFactory::getDBO();
    $query = "select value from `#__billing_options` where `option` = 'logenabled'";
	$result = $db->setQuery($query);  
	$ok = $db->loadResult();

	if ($ok)
	{
		$user = JFactory::getUser();
		$uid = $user->id;
		$rt = date('Y-m-d H:i:s');
		$text = addslashes($text);
		$ext = addslashes($ext);
		$query = "insert into `#__billing_log` (`uid`, `messagetype`, `objectname`, `textmessage`, `description`, `record_time`) values ($uid, '$type', '$name', '$text', '$ext', '$rt')";
		$result = $db->setQuery($query);   
		$row = $db->query();	
	}
}

function GetUserNameBy_Id($id)
{
      $db = JFactory::getDBO();
      $query = "select username from `#__users` where id = $id";
      $result = $db->setQuery($query);   
	  $result = $db->loadResult();
	  return $result; 
}

function CanReadArticle($uid, $id, $price = '', $k2 = false)
{
	$code = 0;
	BillingLogMessage('system - article', 'CanReadArticle', "id: $id uid: $uid");
	if (GetUserGroupDiscount($uid) == 100)
	{
		$code = 1;
		BillingLogMessage('system - article', 'CanReadArticle', "discount 100% code: $code");
		return true;
	}
	$db = JFactory::getDBO();
	$m = 0;			
	$query = "select count(*) from `#__billing_articles` where article_id = $id and is_active = 1";
	if($k2)
	{
		$query .= ' and k2 = 1';
	}
	$result = $db->setQuery($query);   	
	$m = $db->loadResult(); // платная		
	
	$query = "select count(*) from `#__billing_articles_sms` where article_id = $id and is_active = 1";
	if($k2)
	{
		$query .= ' and k2 = 1';
	}
	$result = $db->setQuery($query);   	
	$sms = $db->loadResult(); // доступна по смс 
	if ($sms > 0)// если статья за смс
	{
		$code = 1;
		BillingLogMessage('system - article', 'CanReadArticle', "by sms code: $code");
		return false;
	}

	if ($m > 0)// если статья платная
	{
		$query = "select * from `#__billing_articles` where article_id = $id and is_active = 1";
		if($k2)
		{
			$query .= ' and k2 = 1';
		}		
		$result = $db->setQuery($query);   	
		$row = $db->loadAssoc();	
		if ($price == '')
		{
			$price = $row['price'];
			if ($price == 0 or $price == '')
			{
				$code = 3;
				BillingLogMessage('system - article', 'CanReadArticle', "price is null code: $code");
				return true;
			}
		}
		$lockbefore = $row['lockbefore'];
					
		if ($lockbefore != '0000-00-00 00:00:00' and $lockbefore != '')
		{
			if (date('Y-m-d H:i:s') > $lockbefore) 
			{
				BillingLogMessage('system - article', 'CanReadArticle', "accessible by time");
				return true; // ограничение по времени прошло и статья бесплатна
			}
		}
		
		if ($uid == 0)
		{
			$code = 4;
			BillingLogMessage('system - article', 'CanReadArticle', "not autorized code: $code");
			return false;
		}
		
		$query = "select count(*) from `#__billing_articles_user` where article_id = $id and uid = $uid";
		if($k2)
		{
			$query .= ' and k2 = 1';
		}
		$result = $db->setQuery($query);   	
		$x = $db->loadResult(); 
		
		if ($x > 0) //Пользователь уже оплачивал эту статью, показываем ее
		{
			$code = 5;
			BillingLogMessage('system - article', 'CanReadArticle', "user has access code: $code");
			return true;
		}
		else
		{
			if(GetSubscribeType($id, $k2) == -1)
			{
				$code = 6;
				BillingLogMessage('system - article', 'CanReadArticle', "user has not access code: $code");
				return false;
			}
		}			
	}

	if ($uid == 0)
	{
		// смотрим по подписке
		if($k2)
		{
			$query = "select count(*) from `#__billing_subscription_access` where item_type = 11 and item_id = $id and subscription_id in (select id from `#__billing_subscription_type` where is_active = 1)";
		}
		else
		{
			$query = "select count(*) from `#__billing_subscription_access` where item_type = 1 and item_id = $id and subscription_id in (select id from `#__billing_subscription_type` where is_active = 1)";
		}
		$result = $db->setQuery($query);   
		$sn = $db->loadObjectList(); 
		if ($sn > 0)
		{
			$code = 7;
			BillingLogMessage('system - article', 'CanReadArticle', "By subscription. Not autorithed user code: $code");
			return false;
		}
	}
	
	$go = true;
	
	// смотрим по подписке для этого пользователя
	if($k2)
	{
		$it = 11;
	}
	else
	{
		$it = 1;
	}
	$query = "select * from `#__billing_subscription_access` where item_type = $it and item_id = ".$id.' and subscription_id in (select subscription_id from `#__billing_subscription` where uid = '.$uid.' and frozen = 0 and enddate > NOW()) and subscription_id in (select id from `#__billing_subscription_type` where is_active = 1)';
	//BillingLogMessage('system - article', 'CanReadArticle', $query);
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 
	if (count($rows) > 0)
	{
		BillingLogMessage('system - article', 'CanReadArticle', "Article included in subscription");
		foreach ( $rows as $row ) 
		{
			$nowdate = date("Y-m-d");	
			$query = "select count(*) from `#__billing_subscription` where enddate > '$nowdate' and subscription_id = $row->subscription_id and frozen = 0";
			$result = $db->setQuery($query);
			$n = $db->loadResult();
			if ($n > 0) // и дата укладывается в рамки оплаченной подписки
			{
				$code = 8;
				BillingLogMessage('system - article', 'CanReadArticle', "by subscription code: $code");
				return true;
			}
		}
		$go = false;
		BillingLogMessage('system - article', 'CanReadArticle', "Subscription access denied");
	}
	if($k2)
	{
		$it = 11;
	}
	else
	{
		$it = 1;
	}
	$query = "select count(*) from `#__billing_subscription_access` where item_type = $it and item_id = ".$id.' and subscription_id in (select id from `#__billing_subscription_type` where is_active = 1)';
	//BillingLogMessage('system - article', 'CanReadArticle', $query);
	$result = $db->setQuery($query);   
	$sub_exists = $db->loadResult(); 
	if($sub_exists > 0)
	{
		$go = false;
	}

	$sid = GetSubscribeType($id, $k2);
	BillingLogMessage('system - article', 'CanReadArticle', "SID: $sid");
	if($sid == -1)
	{
		$code = 9;
		BillingLogMessage('system - article', 'CanReadArticle', "Subscription access - ok, Sub type -1: $code");
		return true;
	}
	
	if($sid == 13 and $k2)
	{
		$category_id = GetK2ArticleCategory($id);
		BillingLogMessage('system - article', 'CanReadArticle', "K2 Category: $category_id");
		if ($category_id != '')
		{
			$query = 'select * from `#__billing_subscription_access` where item_id = '.$category_id.' and item_type = 13 and subscription_id in (select subscription_id from `#__billing_subscription` where uid = '.$uid.' and frozen = 0 and enddate > NOW()) and subscription_id in (select id from `#__billing_subscription_type` where is_active = 1)';
			$result = $db->setQuery($query);   
			$rows = $db->loadObjectList(); 
			if (count($rows) > 0) // есть подписка на эту категорию у этого пользователя
			{
				foreach ( $rows as $row ) 
				{
					$nowdate = date("Y-m-d");	
					$query = "select count(*) from `#__billing_subscription` where enddate > NOW() and subscription_id = $row->subscription_id and frozen = 0 and uid = $uid";
					$result = $db->setQuery($query);
					$n = $db->loadResult();
					if ($n > 0) // и дата укладывается в рамки оплаченной подписки
					{
						$code = 10;
						BillingLogMessage('system - article', 'CanReadArticle', "category subscription code: $code");
						return true;
					}
				}
			}
			$query = 'select count(*) from `#__billing_subscription_access` where item_id = '.$category_id.' and item_type = 13 and subscription_id in (select id from `#__billing_subscription_type` where is_active = 1)';
			$result = $db->setQuery($query);   
			$cn = $db->loadResult(); 
			if ($cn > 0)
			{
				$code = 13;
				BillingLogMessage('system - article', 'CanReadArticle', "There is no subscription for user $uid code $code");
				return false;
			}
		}
	}
	
	if(!$k2)
	{
		// находим категорию для статьи
		$category_id = GetArticleCategory($id);

		BillingLogMessage('system - article', 'CanReadArticle', "Category: $category_id");
		
		if ($category_id != '')
		{
			$query = 'select * from `#__billing_subscription_access` where item_id = '.$category_id.' and item_type = 7 and subscription_id in (select subscription_id from `#__billing_subscription` where uid = '.$uid.' and frozen = 0 and enddate > NOW()) and subscription_id in (select id from `#__billing_subscription_type` where is_active = 1)';
			$result = $db->setQuery($query);   
			$rows = $db->loadObjectList(); 
			if (count($rows) > 0) // есть подписка на эту категорию у этого пользователя
			{
				foreach ( $rows as $row ) 
				{
					$nowdate = date("Y-m-d");	
					$query = "select count(*) from `#__billing_subscription` where enddate > NOW() and subscription_id = $row->subscription_id and frozen = 0 and uid = $uid";
					$result = $db->setQuery($query);
					$n = $db->loadResult();
					if ($n > 0) // и дата укладывается в рамки оплаченной подписки
					{
						$code = 10;
						BillingLogMessage('system - article', 'CanReadArticle', "category subscription code: $code");
						return true;
					}
				}
			}
			$query = 'select count(*) from `#__billing_subscription_access` where item_id = '.$category_id.' and item_type = 7 and subscription_id in (select id from `#__billing_subscription_type` where is_active = 1)';
			$result = $db->setQuery($query);   
			$cn = $db->loadResult(); 
			if ($cn > 0)
			{
				BillingLogMessage('system - article', 'CanReadArticle', "There is no subscription for user $uid");
				$go = false;
			}
		}
		// находим раздел для статьи
		if (JoomlaVersion() == '1.5')
		{
			$section_id = GetArticleSection($id);
			
			if ($section_id != 0)
			{
				$query = 'select * from `#__billing_subscription_access` where item_id = '.$section_id.' and item_type = 6 and subscription_id in (select subscription_id from `#__billing_subscription` where uid = '.$uid.' and frozen = 0 and enddate > NOW()) and subscription_id in (select id from `#__billing_subscription_type` where is_active = 1)';
				$result = $db->setQuery($query);   
				$rows = $db->loadObjectList(); 
				if (count($rows) > 0) // есть подписка на этот раздел у этого пользователя
				{
					foreach ( $rows as $row ) 
					{
						$nowdate = date("Y-m-d");	
						$query = "select count(*) from `#__billing_subscription` where enddate > '$nowdate' and subscription_id = $row->subscription_id and frozen = 0";
						$result = $db->setQuery($query);
						$n = $db->loadResult();
						if ($n > 0) // и дата укладывается в рамки оплаченной подписки
						{
							$code = 11;
							BillingLogMessage('system - article', 'CanReadArticle', "section subscription code: $code");
							return true;
						}
					}
				}
				$query = 'select * from `#__billing_subscription_access` where item_id = '.$section_id.' and item_type = 6';
				$result = $db->setQuery($query);   
				$cn = $db->loadResult(); 
				if ($cn > 0)
				{
					BillingLogMessage('system - article', 'CanReadArticle', "There is no subscription for user $uid");
					$go = false;
				}
							
			}

		}
	
	}
	
	//BillingLogMessage('system - article', 'CanReadArticle', "there are no restrictions");
	$code = 12;
	BillingLogMessage('system - article', 'CanReadArticle', "code: $code $go");
	return $go;
}

function IsArticleForFee($id)
{
	$db = JFactory::getDBO();
	$query = "select count(*) from `#__billing_articles` where article_id = $id and is_active = 1";
	$result = $db->setQuery($query);   	
	$y = $db->loadResult();	
	if ($y == 0)
	{
		return false;
	}
	
	$user = JFactory::getUser();
	$uid = $user->id;
	
	if ($uid == 0 and $y > 0)
	{
		return true;
	}
	if ($uid == 0 and $y == 0)
	{
		return false;
	}

	$query = "select count(*) from `#__billing_articles_user` where article_id = $id and uid = $uid";
	$result = $db->setQuery($query);   	
	$z = $db->loadResult();
	
	if ($z > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function GetArticlePrice($id)
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_articles` where article_id = $id and is_active = 1";
	$result = $db->setQuery($query);   	
	$y = $db->loadAssoc();	
	return $y['price'];
}

function GetArticleCategory($id)
{
	$db = JFactory::getDBO();	
	if (JoomlaVersion() == '1.5')
	{
		$db->setQuery('SELECT cat.id FROM #__categories cat LEFT JOIN #__content cont ON cat.id = cont.catid LEFT JOIN #__sections sec ON sec.id = cont.sectionid WHERE cont.id='.$id); 
		$category_id = $db->loadResult();
	}
	else
	{
		$db->setQuery("SELECT catid FROM `#__content` where id = $id");
		$category_id = $db->loadResult();
	}

	if (strpos($category_id, ':') > 0)
	{
		$x = strpos($category_id, ':');
		$category_id = substr($category_id, 0, $x);
	}
	
	return $category_id;
	
}

function GetK2ArticleCategory($id)
{
	$db = JFactory::getDBO();	
	$db->setQuery("SELECT catid FROM `#__k2_items` where id = $id");
	return $db->loadResult();
}

function GetArticleSection($id)
{
	if (JoomlaVersion() == '1.5')
	{
		$db = JFactory::getDBO();	
		$query = "select * from `#__content` where id = $id";
		$result = $db->setQuery($query);   	
		$row = $db->loadAssoc();
		$section_id = $row['sectionid'];
		return $section_id;
	}
	else
	{
		return '';
	}
}

function GetSubscribeType($id, $k2 = false)
{
	$db = JFactory::getDBO();	
	$query = 'select count(*) from `#__billing_subscription_access` where item_type = 1 and item_id = '.$id;
	$result = $db->setQuery($query);   
	$n1 = $db->loadResult(); 
	
	if($k2)
	{
		$query = 'select count(*) from `#__billing_subscription_access` where item_type = 11 and item_id = '.$id;
		$result = $db->setQuery($query);   
		$n2 = $db->loadResult(); 
		if($n2 > 0)
		{
			return 11;
		}
		
		$sid = GetK2ArticleCategory($id);
		if($sid != '')
		{
			$query = 'select count(*) from `#__billing_subscription_access` where item_type = 13 and item_id = '.$sid;
			$result = $db->setQuery($query);   
			$n2 = $db->loadResult(); 
			if($n2 > 0)
			{
				return 13;
			}
		}
	}

	$sid = GetArticleSection($id);
	if ($sid != '')
	{
		$query = 'select count(*) from `#__billing_subscription_access` where item_type = 6 and item_id = '.$sid;
		$result = $db->setQuery($query);   
		$n2 = $db->loadResult(); 
		if($n2 == 0)
		{
			$sid = 0;
		}
	}
	
	$cid = GetArticleCategory($id);
	if ($cid != '')
	{
		$query = 'select count(*) from `#__billing_subscription_access` where item_type = 7 and item_id = '.$cid;
		$result = $db->setQuery($query);   
		$n2 = $db->loadResult(); 
		if($n2 == 0)
		{
			$cid = 0;
		}
	}
	
	if($k2 and $n2 > 0)
	{
		return 11;
	}
	
	if (($sid != '' or $cid != '') and $n1 > 0)
	{
		return -3;
	}

	if ($sid != '' and $cid != '')
	{
		return -2;
	}
	
	if ($n1 > 0)
	{
		return 1;
	}	
	
	if ($sid != '')
	{
		return 6;
	}
	
	if ($cid != '')
	{
		return 7;
	}

	return -1;
}

Class UserAccount 
{
	function UserPointsEnabled()
	{
		if(!BILLING_EXT)
		{
			return false;
		}
		return UserPointsEnabled();
	}

	function GetOption($option, $text = false)
	{
		if(isset($_SESSION[$option]))
		{
			return $_SESSION[$option];
		}
		$db = JFactory::getDBO();
		if($text)
		{
			$query = "select text_value from `#__billing_options` where `option` = '$option'";
		}
		else
		{
			$query = "select value from `#__billing_options` where `option` = '$option'";
		}
		$result = $db->setQuery($query);  
		$result = $db->loadResult();
		$_SESSION[$option] = $result;
		return $result;
	}

	function Partnership()
	{
		$db = JFactory::getDBO();
		$query = "select value from `#__billing_options` where `option` = 'partner'";
		$result = $db->setQuery($query);  
		$partner = $db->loadResult();	
		if ($partner == '' or $partner == false) {$partner = false;} else {$partner == true;}
		return $partner;
	}
	
	function HasDaddy($uid)
	{
		if ($this->Partnership()) 
		{
			$db = JFactory::getDBO();
			$query = "select uid from `#__billing_partner` where partner_id = $uid";
			$result = $db->setQuery($query);  
			$partner = $db->loadResult();
			if ($partner == '')	{return 0;} else {return $partner;}
		}
		else
		{
			return 0;
		}
	}

	function AccountExists($uid)
	{
		$db = JFactory::getDBO();
		$query = 'select count(*) from `#__billing` where uid = ' . $uid;
		$result = $db->setQuery($query);   
		$n = $db->loadResult(); 
		
		if ($n > 0 )
		{
			return true;
		}
		else
		{
			return false;
		}
    }

	function GetBalance($uid)
	{
	   $db = JFactory::getDBO();
	   $query = 'select sum(val) from `#__billing` where waiting <> 1 and uid = ' . $uid;
	   $result = $db->setQuery($query);   
	   $acc = $db->loadResult(); 
	   
	   if ($acc == '')
	   {
			$acc = 0;
	   }

		return $acc;
	}
	
	function GetBalanceEx($uid, $val)
	{
	   $db = JFactory::getDBO();
	   $query = 'select sum(val) from `#__billing` where waiting <> 1 and uid = ' . $uid;
	   $result = $db->setQuery($query);   
	   $acc = $db->loadResult(); 
	   
	   if ($acc == '')
	   {
			$acc = 0;
	   }

		$acc = $acc + $val;
		return $acc;
	}
	
	function GetAccountTypeByAccId($acc_id)
	{
		$db = JFactory::getDBO();
		$query = "select account_type_name from `#__billing_account_type` where id in (select account_type from `#__billing_account` where id = $acc_id)";
		$result = $db->setQuery($query);   
		$result = $db->loadResult(); 	
		
		return $result;
	}
	
	function GetAccountHistory($uid, $page = 0, $count = 30, $from_date = '', $to_date = '' )
	{
		$user = JFactory::getUser();
		$zuid = $user->id;
		
		$out = '';
		
		/*if ($zuid == 0) 
		{
			//$out = "Вы не авторизованы.";
			JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLIING_AUTH_NEED') );
			return $out;
		}*/
		
		if ($page > 1)
		{
			$page = ($page - 1) * $count;
		}
		
		if ($page == 1)
		{
			$page = 0;
		}
		if($count == '' or $count < 1)
		{
			$count = 30;
		}

		$out .= "<h2 class='h2_billing'>".JText::_('COM_BILLING_HISTORY_PAY')."</h2>";

		if($from_date != '' and $to_date != '')
		{
			$where = " and adate between '$from_date' and '$to_date' ";
		}
		elseif($from_date != '' and $to_date == '')
		{
			$where = " and adate >= '$from_date' ";
		}	
		elseif($from_date == '' and $to_date != '')
		{
			$where = " and adate <= '$to_date' ";
		}	
		elseif($from_date == '' and $to_date == '')
		{
			$where = '';
		}
		
	   $db = JFactory::getDBO();
	   $query = "select * from `#__billing` where uid = $uid $where order by id desc limit $page, $count";
	   $result = $db->setQuery($query);   
	   $rows = $db->loadObjectList(); 	
	   $n = 1;
		
		if(count($rows) == 0)
		{
			return;
		}
		
		$out .= '<table width="100%" class="adminlist table billing_table" cellspacing="1">';
		$out .= "<thead class='billing_head'>";
		$out .= "  <th class='billing_th'>Id</th>";
		$out .= "  <th class='billing_th'>".JText::_('COM_BILLING_DATE')."</th>";
		$out .= "  <th class='billing_th'>".JText::_('COM_BILLING_ACTIONS')."</th>";
		$out .= "  <th class='billing_th'>".JText::_('COM_BILLING_SUM')." ( ". GetDefaultCurrencyAbbr() .")</th>";
		if ($this->MultiAccount())
		{
			$out .= "  <th class='billing_th'>".JText::_('COM_BILLING_ACCOUNTS')."</th>";
		}
		if ($this->GetOption('showip'))
		{
			$out .= "  <th class='billing_th'>IP</th>";
		}
		$out .= "</thead>";
		
		foreach ( $rows as $row ) 
		{
			if (($n & 1) == 0)
			{
				$out .= '<tr class="trodd">';
			}
			else
			{
				$out .= '<tr class="treven">';
			}
			$n = $n + 1;
			
			$out .= "  <td class='billing_td'>$row->id</td>";
			$out .= "  <td class='billing_td'>". FormatBillingDate($row->adate)."</td>";
			if ($row->waiting == 1)
			{
				$out .= "  <td class='billing_td'>$row->data1 (<a href='index.php?option=com_billing&task=confirm&id=$row->id&uid=$uid'>".JText::_('COM_BILLING_CONFIRM_WAIT')."</a> <a href='index.php?option=com_billing&task=rejectwait&id=$row->id&uid=$uid'>".JText::_('COM_BILLING_REJECT_WAIT')."</a>)</td>";
			}
			elseif($row->data3 == '1')
			{
				$out .= "  <td class='billing_td'>$row->data1 <a href='index.php?option=com_billing&task=cancelcashout&id=$row->id&uid=$uid'>".JText::_('COM_BILLING_RETURN_MONEY')."</a></td>";
			}
			else
			{
				$out .= "  <td class='billing_td'>$row->data1</td>";
			}
			
			$fd = $this->GetOption('formatdecimals');
			if ($fd == '') {
				$fd = 2;
			}
			$out .= "  <td class='billing_td' align='right'>". number_format($row->val, $fd, '.', ' ')."</td>";  
			if ($this->MultiAccount())
			{
				if ($row->acc != '')
				{
					$out .= "  <td class='billing_td'>".$this->GetAccountTypeByAccId($row->acc)."</td>";
				}
				else
				{
					$out .= "  <td class='billing_td'></td>";
				}
			}
			if ($this->GetOption('showip'))
			{
				$out .= "  <td class='billing_td'>$row->data2</td>";
			}
			$out .= "</tr>";
		}
		$out .= '</table>';
		
		
		$query = "select count(*) from `#__billing` where uid = $uid";
		$result = $db->setQuery($query);   
	    $n = $db->loadResult();
		
		if ($n % $count == 0)
		{
			$n = (int) $n/$count;
		}
		else
		{
			$n = (int) $n/$count + 1; 
		}
		
		$out .= '<p align="center">';
		
		for($i=1; $i <= $n; $i++)
		{
			if ($page == $i)
			{
				$out .= " $i ";
			}
			else
			{
				$out .= " <a href='index.php?option=com_billing&view=billing&page=$i'>$i</a> ";
			}
		}
		
		$out .= '</p>';
		
		return $out;
	}

	function MultiAccount()
	{
		$db = JFactory::getDBO();
		$query = "select value from `#__billing_options` where `option` = 'useaccounts'";
		$result = $db->setQuery($query);  
		$result = $db->loadResult();
		
		return $result;
	}

	function AddMoney($uid, $value, $pid = '', $descr = '', $wait = 0, $acc = '', $nopoints = false, $echo = false)
	{
		$user = JFactory::getUser();
		$zuid = $user->id;
		
		if ($zuid == 0) 
		{
			//echo "Вы не авторизованы.";
			JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLIING_AUTH_NEED') );
			return;
		}
		
		if($pid == '')
		{
			$pid = date('His') . $uid . rand(999, 9999);
		}

		$date = date('Y-m-d H:i:s');
		$ip = GetBillingIp(); 

		if ($descr == '')
		{
			$descr = JText::_('COM_BILLING_ADD2_MONEY');
		}
		$descr = addslashes($descr);
		
		$db = JFactory::getDBO();
		
		$query = "select count(*) from `#__billing` where `payment_id` = '$pid' and `val` = $value";
		$result = $db->setQuery($query);   
		$n = $db->loadResult();
		
		if ($n > 0 )
		{
			if (!$echo)
			{
				echo "<p>".JText::_('COM_BILLING_ALREADYX')."</p>";
			}
			BillingLogMessage('component', 'User Account', JText::sprintf('COM_BILLING_ALREADY_DONE', $pid, $value));
			return;
		}
		
		if ($this->MultiAccount() and $acc != '')
		{
			$query = "insert into `#__billing` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`, `waiting`, `acc`) values('$date', $uid, '$pid', '".GetAccountCurrencyAbbr($acc)."', $value, '$descr', '$ip', $wait, $acc)";
		}
		else
		{	
			$query = "insert into `#__billing` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`, `waiting`) values('$date', $uid, '$pid', '".GetDefaultCurrencyAbbr()."', $value, '$descr', '$ip', $wait)";
		}
		BillingLogMessage('System', 'Add money 1', $query);
		$result = $db->setQuery($query);   
		$n = $db->query(); 
		if ($this->UserPointsEnabled() and !$nopoints and BILLING_EXT)
		{
			$action_id = GetActionIdByCode('addmoney');
			AddUserPoints($uid, $action_id);
		}
		if($wait == 1)
		{
			// Надо понять, что платеж не отложенный, а должен быть проверен вручную. Как?
			//EmailToAdmin("Поступил платеж от пользователя. Платеж ожидает проверки.", "Оповежение о платеже, ожидающем проверки");
		}
	}
	
	function AddMoneyEasy($uid, $value, $pid = '', $descr = '', $wait = 0, $acc = '', $order_id = '', $item_id = '', $date = '')
	{
		if($date == '')
		{
			$date = date('Y-m-d H:i:s');
		}
		$ip = GetBillingIp(); 
		if($pid == '')
		{
			$pid = date('His') . $uid . rand(999, 9999);
		}

		if ($descr == '')
		{
			$descr = JText::_('COM_BILLING_ADD2_MONEY');
		}
		$descr = addslashes($descr);
		
		$db = JFactory::getDBO();
		
		$query = "select count(*) from `#__billing` where `payment_id` = '$pid' and `val` = $value";
		$result = $db->setQuery($query);   
		$n = $db->loadResult();
		
		if ($n > 0 )
		{
			echo "<p>".JText::_('COM_BILLING_ALREADYX')."</p>";
			return false;
		}
		if ($order_id == '') {$order_id = 0;}
		if ($item_id == '') {$item_id = 0;}
		if ($this->MultiAccount() and $acc != '')
		{
			$query = "insert into `#__billing` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`, `waiting`, `acc`, `order_id`, `item_id`) values('$date', $uid, '$pid', '".GetAccountCurrencyAbbr($acc)."', $value, '$descr', '$ip', $wait, $acc, $order_id, $item_id)";
		}
		else
		{	
			$query = "insert into `#__billing` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`, `waiting`, `order_id`, `item_id`) values('$date', $uid, '$pid', '".GetDefaultCurrencyAbbr()."', $value, '$descr', '$ip', $wait, $order_id, $item_id)";
		}
		BillingLogMessage('System', 'Add money 2', $query);
		$result = $db->setQuery($query);   
		$n = $db->query(); 
		if ($this->UserPointsEnabled() and BILLING_EXT)
		{
			$action_id = GetActionIdByCode('addmoney');
			AddUserPoints($uid, $action_id);
		}
		return true;
	}

	// зачисляем на основной счет с пересчетом валюты, для плагинов
	function AddMoneyEasyCurr($uid, $value, $pid = '', $descr, $curr_id, $acc = '', $wait = 0, $f_1= '', $v_1= '', $f_2= '', $v_2= '', $f_3= '', $v_3= '', $echo = false)
	{
		// $value - сумма в валюте плагина
		// $curr_id - валюта плагина
		// $acc - валюта счета для зачисления может не совпадать с валютой плагина
		// зачисляем на основной счет с пересчетом валюты
		//BillingLogMessage('component', 'User Account', 'AddMoneyEasyCurr');
		$date = date('Y-m-d H:i:s');
		$ip = GetBillingIp(); 

		if ($descr == '')
		{
			$descr = JText::_('COM_BILLING_ADD2_MONEY');
		}
		$descr = addslashes($descr);
		
		if ($acc == '')
		{
			$def_curr = GetDefaultCurrency();
			$value = ConvertCurrency($curr_id, $def_curr, $value);
		}
		else
		{
			$def_curr = GetAccountCurrency($acc);
			$value = ConvertCurrency($curr_id, $def_curr, $value);
		}
		if($pid == '')
		{
			$pid = date('His') . $uid . rand(999,9999);
		}
		
		$db = JFactory::getDBO();
		
		$query = "select count(*) from `#__billing` where `payment_id` = '$pid' and `val` = $value";
		$result = $db->setQuery($query);   
		$n = $db->loadResult();
		
		if ($n > 0 )
		{
			if($echo)
			{
				echo "<p>".JText::_('COM_BILLING_ALREADYX')."</p>";
			}
			BillingLogMessage('component', 'User Account', JText::sprintf('COM_BILLING_ALREADY_DONE', $pid, $value));
			return false;
		}
		//BillingLogMessage('component', 'User Account', 'processing...');
		$fields = '';
		$values = '';
		
		if ($f_1 != '')
		{
			$fields = ", `$f_1`"; 
			$values = ", '$v_1'"; 
		}
		if ($f_2 != '')
		{
			$fields .= ", `$f_2`"; 
			$values .= ", '$v_2'"; 
		}
		if ($f_3 != '')
		{
			$fields .= ", `$f_3`"; 
			$values .= ", '$v_3'"; 
		}
		//BillingLogMessage('component', 'User Account', 'prepare query....');
		if ($this->MultiAccount() and $acc != '')
		{
			$query = "insert into `#__billing` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`, `waiting`, `acc`, `order_name` $fields) values('$date', $uid, '$pid', '".GetAccountCurrencyAbbr($acc)."', $value, '$descr', '$ip', $wait, $acc, '$pid' $values)";
		}	
		else
		{
			$query = "insert into `#__billing` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`, `waiting`, `order_name` $fields) values('$date', $uid, '$pid', '".GetDefaultCurrencyAbbr()."', $value, '$descr', '$ip', $wait, '$pid'  $values)";
		}
		BillingLogMessage('System', 'Add money 3', $query);
		//BillingLogMessage('component', 'User Account', addslashes($query));
		$result = $db->setQuery($query);   
		$n = $db->query();
		if ($this->UserPointsEnabled() and BILLING_EXT)
		{
			$action_id = GetActionIdByCode('addmoney');
			AddUserPoints($uid, $action_id);
		}
		return true;
	}

	function ConfirmMoney($transaction_id)
	{
		$user = JFactory::getUser();
		$zuid = $user->id;
		
		if ($zuid == 0) 
		{
			//echo "Вы не авторизованы.";
			JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLIING_AUTH_NEED') );
			return;
		}
		
		$date = date('Y-m-d H:i:s');
		
		$db = JFactory::getDBO();
	    $query = "select `waiting` from `#__billing` where id = $transaction_id";
		$result = $db->setQuery($query);   
		$n = $db->loadResult(); 
		
		if ($n == 1)
		{		
			$query = "update `#__billing` set `waiting` = 0, `waiting_date` = '$date', waiting_comment = '".JText::_('COM_BILLING_CONFIRMED_BY_ADMIN')." $user->name' where id = $transaction_id";
			$result = $db->setQuery($query);   
			$n = $db->query(); 
		}
		
	}
	
	function ConfirmationNeeded($uid)
	{
		$user = JFactory::getUser();
		$zuid = $user->id;
		$db = JFactory::getDBO();
	    $query = "select count(*) from `#__billing` where uid = $uid and `waiting` = 1";
		$result = $db->setQuery($query);   
		$n = $db->loadResult();
		
		return $n;
	}
	
	function CashOutMoney($uid, $sum, $wallet, $phone, $paysys_id)
	{
		$balance = $this->GetBalance($uid);
		
		if ($sum > $balance)
		{
			return false;
		}
	
		if (!$this->GetOption('withdrawal') and !$this->GetOption('cashouttab'))
		{
			return false;
		}
		$minsum = $this->GetOption('minsum');
		$maxsum = $this->GetOption('maxsum');
		if($minsum != '' and $minsum != 0 and $sum < $minsum)
		{
			JFactory::getApplication()->enqueueMessage(JText::_('COM_BILLING_OUTOFRANGE'));
			return false;
		}

		if($maxsum != '' and $maxsum != 0 and $sum > $maxsum)
		{
			JFactory::getApplication()->enqueueMessage(JText::_('COM_BILLING_OUTOFRANGE'));
			return false;
		}

		$db = JFactory::getDBO();
		
		$query = "select count(*) from `#__billing_group`  where enabled = 1";
		$result = $db->setQuery($query);
		$res = $db->loadResult();
		$go = false;		
		if($res > 0)
		{
			$query = "select count(*) from `#__billing_user_group` where uid = $uid and group_id in (select id from `#__billing_group` where option3 = 1 and enabled = 1)";
			$result = $db->setQuery($query);
			$res = $db->loadResult();
			if($res > 0)
			{
				$go = true;
			}
		}
		else
		{
			$go = true;
		}
		
		if($go)
		{
			$date = date('Y-m-d H:i:s');
			$ip = GetBillingIp(); 
			$value = - $sum;	
			$pid = date('his') . $uid . rand(999, 9999);
			$ab = GetDefaultCurrencyAbbr();
			$descr = JText::_('COM_BILLING_CASHOUT_WAIT');
			
			$query = "select data3 from `#__billing_settings` where id = $paysys_id";
			$result = $db->setQuery($query);
			$comm = $db->loadResult();
			if($comm != '' and $comm > 0)
			{
				$sum1 = $sum - $sum / 100 * $comm;
				$descr .= ' '. FormatDefaultCurrency($sum) . " -$comm %  = " . FormatDefaultCurrency($sum1);
			}
			$descr = addslashes($descr);

			$query = "insert into `#__billing` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`, `acc`, `waiting`, `data3`, `data4`, `data5`, `order_id`) values('$date', $uid, '$pid', '$ab', $value, '$descr', '$ip', 0, 0, '1', '$wallet', '$phone', $paysys_id)";
			$result = $db->setQuery($query);   
			$n = $db->query(); 		
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function AddUserToBillingGroup($uid, $group_id)
	{
		$db = JFactory::getDBO();
		$query = "select count(*) from `#__billing_user_group` where group_id = $group_id and uid = $uid";
		$result = $db->setQuery($query);  
		$x = $db->loadResult();
		if($x > 0)
		{
			return;
		}
		$dt = date('Y-m-d H:i:s');
		$query = "delete from `#__billing_user_group` where uid = $uid";
		$result = $db->setQuery($query);  
		$result = $db->query();
		$query = "insert into `#__billing_user_group` (group_id, uid, add_date) values ($group_id, $uid, '$dt')";
		$result = $db->setQuery($query);  
		$result = $db->query();
	}
	
	function WithdrawMoney($uid, $value, $pid = '', $descr = '', $acc = 0, $order_id = 0, $order_name = '', $no_partner = false, $wait = 0)
	{
		$balance = $this->GetBalance($uid);
		
		if($value < 0)
		{
			return false;
		}
		
		if ($balance < abs($value))
		{
			JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLING_NO_MONEY_ACCESS'));
			return false;
		}
		
		if($pid == '')
		{
			$pid = date('His') . $uid;
		}

		$date = date('Y-m-d H:i:s');
		$ip = GetBillingIp(); 
		$value = - $value;
		
		if ($descr == '')
		{
			$descr = JText::_('COM_BILLING_WITHDR_MONEY');
		}		
		$descr = addslashes($descr);
		
		$db = JFactory::getDBO();
		if ($acc != 0)
		{
			$ab = GetAccountCurrencyAbbr($acc);
		}
		else
		{
			$ab = GetDefaultCurrencyAbbr();
		}
	    $query = "insert into `#__billing` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`, `acc`, `order_id`, `order_name`, `waiting`) values('$date', $uid, '$pid', '$ab', $value, '$descr', '$ip', $acc, $order_id, '$order_name', $wait)";
		$result = $db->setQuery($query);   
		$n = $db->query(); 
		
		$query = "select * from `#__billing_group` where data1 <> '' order by data1 desc";
		$result = $db->setQuery($query);  
		$groups = $db->loadObjectList();
		if(count($groups) > 0)
		{
			$query = "select sum(val) from `#__billing` where uid = $uid and val < 0";
			$result = $db->setQuery($query);  
			$spent = - $db->loadResult();

			$group_id = GetUserGroupId($uid);
			
			if($group_id != '')
			{
				$query = "select * from `#__billing_group` where id = $group_id";
				$result = $db->setQuery($query);  
				$grp = $db->loadAssoc();
				$dsc = $grp['data1']; // сколько надо потратить, чтобы попасть в группу

				if($dsc != '0' and $dsc != '')
				{
					if($dsc > 0 and $dsc < $spent) // если текущая групповая скидка меньше потраченной суммы
					{
						foreach($groups as $group)
						{
							if($spent > $group->data1)
							{
								$this->AddUserToBillingGroup($uid, $group->id);
								break;
							}
						}
					}
				}
			}

		}
		
		$partner_id = $this->HasDaddy($uid);
		if ($partner_id > 0 and !$no_partner)
		{
			if(multy)
			{
				$this->MLPercent($uid, $value, $descr);
			}
			else
			{
				$query = "select value from `#__billing_options` where `option` = 'partnerpercent'";
				$result = $db->setQuery($query);  
				$percent = $db->loadResult();				
				
				$value2 = - $value / 100 * $percent;
				$username = GetUserNameBy_Id($uid);
				$descr = JText::_('COM_BILLING_PARTNER_PROFIT') . " $username (id:$uid)";
				$descr = addslashes($descr);
				
				if(BillingGetOption('partpoint') and $this->UserPointsEnabled() and BILLING_EXT)
				{
					//AddUPAction(JText::_('COM_BILLING_PARTNER_PROFIT'), $descr, $value2); 
					$action_id = GetActionIdByCode('paypartner');
					AddUserPoints($uid, $action_id, JText::_('COM_BILLING_PARTNER_PROFIT'));
				}
				else
				{				
					$query = "insert into `#__billing` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`) values('$date', $partner_id, '3', '".GetDefaultCurrencyAbbr()."', $value2, '$descr', '')";
					$result = $db->setQuery($query);   
					$n = $db->query();
				}
				
				$query = "insert into `#__billing_partner_history` (uid, partner_id, operation_id, val, oper_date, data1) values($partner_id, $uid, 2, $value2, '$date', '$descr')";
				$result = $db->setQuery($query);  
				$result = $db->query();
			}
			
			if ($this->UserPointsEnabled() and BILLING_EXT)
			{
				$action_id = GetActionIdByCode('paypartner');
				AddUserPoints($partner_id, $action_id);
			}
		}
		
		return true;
	}

	function MoveMoney($uid1, $uid2, $value, $descr = '')
	{
		if($uid1 == 0 or $uid2 == 0 or $uid1 == '' or $uid2 == '')
		{
			return false;
		}
		$b1 = $this->GetBalance($uid1);
		$b2 = $this->GetBalance($uid2);
		if ($value > $b1)
		{
			return false; 
		}
		$descr = addslashes($descr);
		$db = JFactory::getDBO();
		$query = "select count(*) from `#__billing_group` where enabled = 1";
		$result = $db->setQuery($query);
		$res = $db->loadResult();
		$go = false;		
		if($res > 0)
		{
			$query = "select count(*) from `#__billing_user_group` where uid = $uid1 and group_id in (select id from `#__billing_group` where option2 = 1 and enabled = 1)";
			$result = $db->setQuery($query);
			$res1 = $db->loadResult();
			$query = "select count(*) from `#__billing_user_group` where uid = $uid2 and group_id in (select id from `#__billing_group` where option2 = 1 and enabled = 1)";
			$result = $db->setQuery($query);
			$res2 = $db->loadResult();
			if($res1 > 0 and $res2 > 0)
			{
				$go = true;
			}
		}
		else
		{
			$go = true;
		}
		if($go)
		{
			$pid1 = date('ymdhis') . $uid1;
			$pid2 = date('ymdhis') . $uid2;
			
			$comission = BillingGetOption('comission');
			
			$query = "select * from `#__users` where id = $uid2";
			$result = $db->setQuery($query);  
			$user = $db->loadAssoc();
			$this->WithdrawMoney($uid1, $value, $pid1, $descr . ' '. JText::_('COM_BILLING_MOVE_TO'). ' ' . $user['name'] . ' (' . $user['email'].')', 0, 0, '', true);
			if($comission != '' and $comission > 0)
			{
				$value = $value - $value /100 * $comission;
			}
			$query = "select * from `#__users` where id = $uid1";
			$result = $db->setQuery($query);  
			$user = $db->loadAssoc();
			$this->AddMoney($uid2, round($value, 2), $pid2, $descr . ' '. JText::_('COM_BILLING_MOVE_FROM'). ' ' . $user['name'] . ' (' . $user['email'].')');
			return true;
		}
		else
		{
			return false;
		}
	}
	
	// перевод денег с указанием счетов
	function MoveMoneyAcc($uid1, $uid2, $value, $descr = '', $acc_id1 = '', $acc_id2 = '')
	{
		$b1 = $this->GetBalance($uid1);
		$b2 = $this->GetBalance($uid2);
		if ($value > $b1)
		{
			return false; 
		}
		$descr = addslashes($descr);
		$pid1 = date('ymdhis') . $uid1;
		$pid2 = date('ymdhis') . $uid2;
		
		$this->WithdrawMoney($uid1, $value, $pid1, $descr . ' '. JText::_('COM_BILLING_MOVE_TO'). ' ' . $uid2, $acc_id1);
		$this->AddMoney($uid2, $value, $pid2, $descr . ' '. JText::_('COM_BILLING_MOVE_FROM'). ' ' . $uid1, $acc_id2);
		return true;
	}

	function AddSubscription($uid, $days, $subscription_id, $free = false, $list_id = 0, $comment = '')
	{
		$db = JFactory::getDBO();
		$query = 'select * from `#__billing_subscription_type` where id = '. $subscription_id;
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc();       

		$discount = GetUserGroupDiscount($uid);		

		$subscription_days = $row['subscription_days'];
		if ($subscription_days == 0) return false;
		if ($subscription_days == '') return false;
		$price = $row['price'];
		if ($discount > 0)
		{
			$price = $price - $price/100 * $discount;
			$price = round($price, 2);
		}
		if ($discount == 100)
		{
			$price = 0;
			$free = true;
		}
		$price_aday = ($price / $subscription_days);
		$sum = $days * $price_aday;
		
		$query = 'select price from `#__billing_subscription_period` where subscription_id = ' . $subscription_id . ' and days = '. $days;
		$result = $db->setQuery($query);   
		$price2 = $db->loadResult();
		
		if($price2 != '')
		{
			$sum = $price2;
			$sum = $sum - $sum/100 * $discount;
			$sum = round($sum, 2);
		}
		
		$frozen = $this->IsSubscriptionFrozen($uid, $subscription_id, $list_id);
		if($frozen == 1)
		{
			JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLING_SUB_IS_FROZEN') );
			return false;
		}
		
		$query = "select * from `#__billing_subscription` where subscription_id = $subscription_id and uid = $uid and list_id = $list_id";
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc();           
		$enddate = $row['enddate'];
		
		$dt = date('Y-m-d H:i:s');	
		
		if(!$free)
		{
		   $res = $this->WithdrawMoney($uid, $sum, $subscription_id, JText::_('COM_BILLING_RENEW_SUB') . ' ' . $days . ' ' . JText::_('COM_BILLING_DAYS'));
		   if (!$res)
		   {
				return false;
		   }
		}
		else
		{
		   $sum = 0;
		   $this->WithdrawMoney($uid, 0, $subscription_id, JText::_('COM_BILLING_RENEW_SUB') . ' ' . $days . ' ' . JText::_('COM_BILLING_DAYS') . ' ' . JText::_('COM_BILLING_FOR_FREE'));
		   		   
		}

		if ($enddate == '')
		{
			// подписки не было, считаем от текущей даты
			$enddate = date("Y-m-d H:i:s");
			$unixdate = strtotime($enddate);
		}
		else
		{
			// подписка была
			$unixdate = strtotime($enddate);
		}
		$newdate = $unixdate + $days*86400;
		$newdate = date("Y-m-d", $newdate);
		$nowdate = date("Y-m-d H:i:s");
		$nowdatetime = strtotime($nowdate);
		if($unixdate < $nowdatetime)
		{
			//подписка истекла в прошлом.
			$newdate = $nowdatetime + $days*86400;
			$newdate = date("Y-m-d", $newdate);
		}
		
		$query = "select count(*) from `#__billing_subscription` where subscription_id = $subscription_id and uid = $uid and list_id = $list_id";
		$result = $db->setQuery($query);   
		$n = $db->loadResult();     

		if ($n > 0)
		{
			$query = "update `#__billing_subscription` set enddate = '$newdate', data1 = '$nowdate', data2 = '$sum', last_payment = '$dt', last_payment_sum = $sum";
			if($comment != '')
			{
				$query .= ", data3 = '$comment' ";
			}
			$query .= " where uid = $uid and subscription_id = $subscription_id and list_id = $list_id";
		}
		else
		{
			$query = "insert into `#__billing_subscription` (enddate, uid, payment_id, data1, subscription_id, data2, list_id, data3, start_date, last_payment, last_payment_sum) values('$newdate', $uid, '0', '$nowdate', $subscription_id, '$sum', $list_id, '$comment', '$dt', '$dt', $sum)"; 
		}
		
		$result = $db->setQuery($query);
		$db->query();
		
		
		$query = "select id from `#__billing_subscription` where subscription_id = $subscription_id and uid = $uid and list_id = $list_id";
		$result = $db->setQuery($query);   
		$sid = $db->loadResult();  
		
		$query = "insert into `#__billing_sub_history` (sub_id, uid, payment_date, payment_sum, subscription_id) values ($sid, $uid, '$dt', $sum, $subscription_id)";
	    $result = $db->setQuery($query);
	    $db->execute();
		
		if ($this->UserPointsEnabled() and BILLING_EXT)
		{
			$action_id = GetActionIdByCode('paysub');
			AddUserPoints($uid, $action_id);
		}
		
		//Добавляем пользователя в группы
		$query = "select * from `#__billing_subscription_access` where item_type = 9 and subscription_id = $subscription_id";
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList();
		foreach($rows as $row)
		{
			$this->AddUserToGroup($uid, $row->item_id);
		}
		
		//Добавляем пользователя в группы Биллинга
		$query = "select * from `#__billing_subscription_access` where item_type = 14 and subscription_id = $subscription_id";
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList();
		foreach($rows as $row)
		{
			$this->AddUserToBillingGroup($uid, $row->item_id);
		}
		
		//Добавляем пользователя в JS группы
		$query = "select * from `#__billing_subscription_access` where item_type = 12 and subscription_id = $subscription_id";
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList();
		foreach($rows as $row)
		{
			$this->AddUserToJSGroup($uid, $row->item_id);
		}
		return true;

	}	

	function ProlongPersonalTariff($uid, $days, $free = false)
	{
		$db = JFactory::getDBO();
		$query = "select sum(price) from `#__billing_services` where id in ( select service_id from `#__billing_services_user` where uid = $uid)";
		$result = $db->setQuery($query); 
		$price = $db->loadResult();    

		$discount = GetUserGroupDiscount($uid);		

		if ($discount > 0)
		{
			$price = $price - $price/100 * $discount;
			$price = round($price, 2);
		}
		if ($discount == 100)
		{
			$price = 0;
			$free = true;
		}
		$price_aday = ($price / 30);
		$sum = $days * $price_aday;
		
		if(!$free)
		{
		   $res = $this->WithdrawMoney($uid, $sum, -120, JText::_('COM_BILLING_TARIFF_UPDATED') . ' ' . $days . ' ' . JText::_('COM_BILLING_DAYS'));
		   if (!$res)
		   {
				return false;
		   }
		}
		else
		{
		   $sum = 0;
		   $this->WithdrawMoney($uid, 0, -120, JText::_('COM_BILLING_TARIFF_UPDATED') . ' ' . $days . ' ' . JText::_('COM_BILLING_DAYS') . ' ' . JText::_('COM_BILLING_FOR_FREE'));		 
		}
		
		$query = "select enddate from `#__billing_personal_tariff` where uid = $uid";
		$result = $db->setQuery($query);
		$enddate = $db->loadResult();

		if ($enddate == '')
		{
			// подписки не было, считаем от текущей даты
			$enddate = date("Y-m-d H:i:s");
			$unixdate = strtotime($enddate);
		}
		else
		{
			$unixdate = strtotime($enddate);
		}
		$newdate = $unixdate + $days*86400;
		$newdate = date("Y-m-d", $newdate);
		$nowdate = date("Y-m-d H:i:s");
		
		$query = 'select count(*) from `#__billing_personal_tariff` where uid = ' . $uid;
		$result = $db->setQuery($query);   
		$n = $db->loadResult();           
		
		if ($n > 0)
		{
			$query = "update `#__billing_personal_tariff` set enddate = '$newdate', data1 = '$nowdate', price = $sum where uid = " . $uid;
		}
		else
		{
			$query = "insert into `#__billing_personal_tariff` (enddate, uid, data1, price) values('$newdate', $uid, '$nowdate', $sum)"; 
		}
		$result = $db->setQuery($query);
		$db->query();
		if ($this->UserPointsEnabled() and BILLING_EXT)
		{
			$action_id = GetActionIdByCode('paysub');
			AddUserPoints($uid, $action_id);
		}

		return true;
	}	

	function GetSubscriptionFinish($uid, $subscription_id, $list_id = 0)
	{
		$db = JFactory::getDBO();
		$query = "select * from `#__billing_subscription` where subscription_id = $subscription_id and uid = $uid and list_id = $list_id";
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc();           
		$enddate = $row['enddate'];	
		return $enddate;
	}

	function GetSubscriptionStart($uid, $subscription_id, $list_id = 0)
	{
		$db = JFactory::getDBO();
		$query = "select * from `#__billing_subscription` where subscription_id = $subscription_id and uid = $uid and list_id = $list_id";
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc();           
		$data1 = $row['data1'];	
		return $data1;
	}
	
	function GetSubscriptionLastPayment($uid, $subscription_id, $list_id = 0)
	{
		$db = JFactory::getDBO();
		$query = "select * from `#__billing_subscription` where subscription_id = $subscription_id and uid = $uid and list_id = $list_id";
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc();           
		$data2 = $row['data2'];	
		return $data2;
	}
	
	function IsSubscriptionActual($uid, $subscription_id, $list_id = 0)
	{
		$end = $this->GetSubscriptionFinish($uid, $subscription_id, $list_id);
		$frozen = $this->IsSubscriptionFrozen($uid, $subscription_id, $list_id);
		if($end == '')
		{
			return false;
		}
		if($end > date('Y-m-d H:i:s') and !$frozen)
		{
			return true;
		}
		return false;
	}
	
	function GetSubscriptionDays($subscription_id, $price)
	{
		$price_per_day = $this->GetSubscriptionPrice($subscription_id, 1);
		if($price_per_day == 0)
		{
			return 0;
		}
		return round($price / $price_per_day, 2);
	}
	
	function GetSubscriptionPrice($subscription_id, $days, $uid = '')
	{
		$db = JFactory::getDBO();
		if ($uid != '')
		{
			$discount = GetUserGroupDiscount($uid);	
		}
		else
		{
			$discount = 0;
		}
		
		$query = 'select * from `#__billing_subscription_type` where id = '. $subscription_id;
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc();       

		$subscription_days = $row['subscription_days'];
		if ($subscription_days == 0) return 0;
		if ($subscription_days == '') return 0;
		$price = $row['price'];
		if ($discount > 0)
		{
			$price = $price - $price/100 * $discount;
			$price = round($price, 2);
		}
		if ($discount == 100)
		{
			$price = 0;
			return 0;
		}
		$price_aday = ($price / $subscription_days);
		$sum = $days * $price_aday;
		
		$query = 'select price from `#__billing_subscription_period` where subscription_id = ' . $subscription_id . ' and days = '. $days;
		$result = $db->setQuery($query);   
		$price2 = $db->loadResult();
		
		if($price2 != '')
		{
			return $price2;
		}		
		else
		{
			return $price;
		}
	}
	
	function StopSubscription($uid, $subscription_id, $list_id = 0)
	{
		$db = JFactory::getDBO();
		$now = date('Y-m-d H:i:s');
		$yesterday = strtotime($now) - 86400;
		$now = date('Y-m-d H:i:s', $yesterday);
		$query = "update `#__billing_subscription` set enddate = '$now', frozen = 0 where subscription_id = $subscription_id and uid = $uid and list_id = $list_id";
		$result = $db->setQuery($query);   
		$result = $db->query(); 
		return $result;
	}
	
	function UpdateSubscription($uid, $subscription_id, $finish_date, $free = false, $list_id = 0)
	{
		if($this->IsSubscriptionActual($uid, $subscription_id))
		{
			$finish = $this->GetSubscriptionFinish($uid, $subscription_id);
		}
		else
		{
			$finish = date('Y-m-d');
		}
		if ($finish_date < $finish) {return false;}
		$start_time = strtotime($finish);
		$end_time = strtotime($finish_date);
		$d = $end_time - $start_time;
		$days = max(round($d/(24*60*60)),1);
		return AddSubscription($uid, $days, $subscription_id, $free, $list_id);
	}
	
	function IsSubscriptionFrozen($uid, $subscription_id, $list_id = 0)
	{
		$db = JFactory::getDBO();
		$query = "select * from `#__billing_subscription` where subscription_id = $subscription_id and uid = $uid and list_id = $list_id";
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc();           
		if($row['frozen'] == 1)
		{		
			return true;
		}
		else
		{
			return false;
		}
	}

	function FrozeSubscription($uid, $subscription_id, $list_id = 0)
	{
		if($this->IsSubscriptionFrozen($uid, $subscription_id))
		{
			return true;	
		}
		$db = JFactory::getDBO();
		$now = date('Y-m-d H:i:s');
		$query = "update `#__billing_subscription` set frozen = 1, froze_date = '$now' where subscription_id =  $subscription_id and uid = $uid and list_id = $list_id";
		$result = $db->setQuery($query);   
		$result = $db->query(); 
		return $result;
	}
	
	function UnfreezeSubscription($uid, $subscription_id, $list_id = 0)
	{
		if(!$this->IsSubscriptionFrozen($uid, $subscription_id))
		{
			return true;
		}
		$db = JFactory::getDBO();
		$query = "select * from `#__billing_subscription` where subscription_id = $subscription_id and uid = $uid and list_id = $list_id";
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc(); 
		$froze_date = $row['froze_date'];
		$enddate = $row['enddate'];
		if($enddate > $froze_date)
		{
			$end = strtotime($enddate);	
			$froze = strtotime($froze_date);
			$nowdate = strtotime(date('Y-m-d'));
			$days = $end - $froze;
			$newdate = $nowdate + $days;
			$enddate = date('Y-m-d', $newdate);			
			$now = date('Y-m-d H:i:s');
			$query = "update `#__billing_subscription` set frozen = 0, enddate = '$enddate' where subscription_id =  $subscription_id and uid = $uid";
			$result = $db->setQuery($query);   
			$result = $db->query(); 
			return $result;
		}	
		else
		{	
			return false;
		}		
	}	
	
	function GetAuthorId($article_id, $k2 = false)
	{
		$db = JFactory::getDBO();
		if($k2)
		{
			$query = 'select created_by from `#__k2_items` where id = '. $article_id;
			$result = $db->setQuery($query);   
			return $db->loadResult();    		
		}
		else
		{
			$query = 'select created_by from `#__content` where id = '. $article_id;
			$result = $db->setQuery($query);   
			return $db->loadResult();    		
		}
	}
	
	function AddArticleAccess($uid, $article_id, $free = false, $description = '', $access_price = '', $k2 = false)
	{
		BillingLogMessage('System', 'UserAccount', "Add article access started");
		if($k2)
		{
			$is_k2 = 1;
			$w_k2 = ' and k2 = 1';
		}
		else
		{
			$is_k2 = 0;
			$w_k2 = '';
		}

		$date = date('Y-m-d H:i:s');	
		$db = JFactory::getDBO();
	
		$query = "select * from `#__billing_articles` where article_id =  $article_id $w_k2";
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc();    
		
		//
		$query = "select count(*) from `#__billing_articles_user` where article_id =  $article_id  and uid = $uid $w_k2";
		$result = $db->setQuery($query);   
		$n = $db->loadResult();  
		$setnull = 0;		
		// Если статью уже купили
		if ($n > 0) // нужно проверить, установлено ли в ней кол-во просмотров
		{
			BillingLogMessage('System', 'UserAccount', "Already paid");
			$query = "select * from `#__billing_articles_user` where article_id = $article_id and uid = $uid $w_k2";
			$result = $db->setQuery($query);   
			$row2 = $db->loadAssoc(); 
			
			BillingLogMessage('System', 'UserAccount', "Counters: " .  $row['data3']. ' ' . $row2['data4']);
			
			if($row2['data4'] != '' and $row['data3'] != '' and $row['data3'] <= $row2['data4'] and $row['data3'] != '0')
			{
				$setnull = 1;
				BillingLogMessage('System', 'UserAccount', "Show max times");
			}
			else
			{
				echo '<p class="billing_p">'.JText::_('COM_BILLING_ALREADY_PAID_ARTICLE').'</p>';
				return;
			}
		}		
				 
		if ($row['is_active'] == 1)
		{		
			if ($row['price'] == '')
			{
				if($k2)
				{
					$query = "select introtext, `fulltext` from `#__k2_items` where id = $article_id";
				}
				else
				{
					$query = "select introtext, `fulltext` from `#__content` where id = $article_id";
				}
				$result = $db->setQuery($query);
				$txt = $db->loadAssoc();
				$content = $txt['introtext'] . $txt['fulltext'];
				if(preg_match_all("/{price:.+?}/", $content, $matches, PREG_PATTERN_ORDER)) 
				{
					foreach ($matches as $match) 
					{
						foreach ($match as $m) 
						{
						  $price = intval(str_replace('}', '', str_replace('{price:', '', $m)));
						}
					}
				}		
			}
			else
			{
				$price = $row['price'];
			}
			if ($access_price != '')
			{
				$price = $access_price;
			}
			$discount = GetUserGroupDiscount($uid);	
			if ($discount > 0)
			{
				$price = $price - $price/100 * $discount;
				$price = round($price, 2);
			}
			if ($discount == 100 and $setnull == 0)
			{
				$price = 0;
				$date = date('Y-m-d H:i:s');
				$query = "insert into `#__billing_articles_user` (article_id, state, uid, paymentdate, price, k2) values ($article_id, 1, $uid, '$date', $price, $is_k2)";
				$result = $db->setQuery($query);
				$result = $db->query();
				return;
			}
			$b = $this->GetBalance($uid);
			$price = round($price, 2);
			
			if(!$k2)
			{
				$query = "select created_by from `#__content` where id =  $article_id";
				$result = $db->setQuery($query);   
				$created = $db->loadAssoc();    
				if($created == $uid)
				{
					$query = "insert into `#__billing_articles_user` (article_id, state, uid, paymentdate, price, data4, k2) values ($article_id, 1, $uid, '$date', $price, '0', $is_k2)";
					$result = $db->setQuery($query);   
					$row = $db->query();
					BillingLogMessage('System', 'UserAccount', "User is owner");
					return;
				}
			}
			else
			{
				$query = "select created_by from `#__k2_items` where id =  $article_id";
				$result = $db->setQuery($query);   
				$created = $db->loadAssoc();    
				if($created == $uid)
				{
					$query = "insert into `#__billing_articles_user` (article_id, state, uid, paymentdate, price, data4, k2) values ($article_id, 1, $uid, '$date', $price, '0', $is_k2)";
					$result = $db->setQuery($query);   
					$row = $db->query();
					BillingLogMessage('System', 'UserAccount', "User is owner");
					return;
				}
			}

		if ($b < $price)
			{
				echo '<p>'.JText::_('COM_BILLING_NO_MONEY_ACCESS').' <a href="index.php?option=com_billing&view=pay">'.JText::_('COM_BILLING_ADD_MONEY').'</a></p>';
				return;
			}
			BillingLogMessage('System', 'UserAccount', "Is update view counter?  $setnull");
			if($setnull == 0)
			{
				BillingLogMessage('System - access', 'AddArticleAccess', 'Update entries');
				$date = date('Y-m-d H:i:s');
				$query = "insert into `#__billing_articles_user` (article_id, state, uid, paymentdate, price, data4, k2) values ($article_id, 1, $uid, '$date', $price, '0', $is_k2)";
				$result = $db->setQuery($query);
				$result = $db->query();
			}
			
			if($row['counter'] > 0 and $row['sales'] >= $row['counter'])
			{
				BillingLogMessage('System - access', 'AddArticleAccess', 'Sales counter is done...');
				return false;
			}

			if($setnull == 1)
			{
				BillingLogMessage('System - access', 'AddArticleAccess', 'Drop all entries');
				$query = "update `#__billing_articles_user` set data4 = '0' where article_id = $article_id and uid = $uid $w_k2";
				$result = $db->setQuery($query);
				$result = $db->query();
			}
			
			if (!$free)
			{
				if($this->GetOption('author'))
				{
					if($row['data2'] != '')
					{
						$auth_price = $price/100 * $row['data2'];
						//$price = $price - $auth_price;
					}
				}
				if($description == '')
				{
					$this->WithdrawMoney($uid, $price, 0, JText::_('COM_BILLING_PAY_ARTICLE'));
				}
				else
				{
					$this->WithdrawMoney($uid, $price, 0, $description);
				}
				if($this->GetOption('author'))
				{
					if ($auth_price != '')
					{
						$auth_id = $this->GetAuthorId($article_id, $k2);
						$pid = date('His') . $auth_id;
						$this->AddMoneyEasy($auth_id, $auth_price, $pid, JText::_('COM_BILLING_ADD2_MONEY') . ' Article id ' . $article_id, 0, '', 10, $article_id);
					}
				}

			}
			else
			{
				if($description == '')
				{
					$this->WithdrawMoney($uid, 0, 0, JText::_('COM_BILLING_ART_ACC') . ' ' . JText::_('COM_BILLING_FOR_FREE'));
				}
				else
				{
					$this->WithdrawMoney($uid, 0, 0, $description);
				}
			}
			$description = addslashes($description);
			
			if($row['counter'] > 0 and $row['sales'] < $row['counter'])
			{
				$counter = $row['counter'];
				$sales = $row['sales'];
				$sales = $sales + 1;
				if($sales == $counter)
				{
					$query = "update `#__billing_articles` set `sales` = $sales, is_active = 0 where article_id = $article_id $w_k2";
				}
				else
				{
					$query = "update `#__billing_articles` set `sales` = $sales where article_id = $article_id $w_k2";
				}
				$result = $db->setQuery($query);   
				$row = $db->query(); 
			}
			
			if ($this->UserPointsEnabled() and BILLING_EXT)
			{
				$action_id = GetActionIdByCode('payart');
				AddUserPoints($uid, $action_id);
			}
			return true;
			
		}
		else
		{
			echo '<p>'.JText::_('COM_BILLING_FREE_ACCESS').'</p>';
		}

	}

	function GetUserPayments($uid)
	{
	   $db = JFactory::getDBO();
	   $query = 'select sum(val) from `#__billing` where waiting <> 1 and val < 0 and uid = ' . $uid;
	   $result = $db->setQuery($query);   
	   $acc = $db->loadResult(); 
	   
	   if ($acc == '')
	   {
			$acc = 0;
	   }

		return $acc;		
	}	

	function GetProjectName($project_id)
	{
	   $db = JFactory::getDBO();
	   $query = 'select project_name from `#__billing_project` where id = ' . $project_id;
	   $result = $db->setQuery($query);   
	   return $db->loadResult(); 	
	}
	
	function SendMoneyToProject($uid, $project_id, $amount, $comment = '')	
	{
	   $db = JFactory::getDBO();
	   $b = $this->GetBalance($uid);
	   $amount = $amount;
	   if (($b - $amount) < 0)
	   {
			BillingLogMessage('system', 'SendMoneyToProject', $b - $amount);
			BillingLogMessage('system', 'SendMoneyToProject', "Amount is more than balance");
			return false;
	   }
	   if ($amount <= 0)
	   {
			BillingLogMessage('system', 'SendMoneyToProject', "Amount is <= 0");
			return false;
	   }
	   $pid = date('mdHis') . $uid . rand(999, 9999);
	   $this->WithdrawMoney($uid, $amount, $pid, JText::_('COM_BILLING_SEND_PROJECT') . ': '. $this->GetProjectName($project_id) . ' ' . $comment);
	   
	   $date = date('Y-m-d H:i:s');
	   $query = "insert into `#__billing_project_user` (uid, project_id, psum, send_date, `comment`) values ($uid, $project_id, $amount, '$date', '$comment')";
	   $result = $db->setQuery($query);   
	   $result = $db->query();
	   
	   $sum1 = $this->GetProjectSum($project_id);
	   $csum1 = $this->GetProjectCurrentSum($project_id);
	   
	   if(($sum1 != '') and ($csum1 != '') and ($sum1 > 0) and ($csum1 > $sum1))
	   {
			$query = 'update `#__billing_project` set is_active = 0 where id = ' . $project_id;
			$result = $db->setQuery($query);   
			$acc = $db->query(); 
	   }
	   
	   return true;
	}
	
	function GetProjectCurrentSum($project_id)
	{
	   $db = JFactory::getDBO();
	   $query = 'select sum(psum) from `#__billing_project_user` where project_id = ' . $project_id;
	   $result = $db->setQuery($query);   
	   $acc = $db->loadResult(); 
	   
	   if ($acc == '')
	   {
			$acc = 0;
	   }

		return $acc;				
	}
	
	function GetProjectSum($project_id)
	{
	   $db = JFactory::getDBO();
	   $query = 'select project_sum from `#__billing_project` where id = ' . $project_id;
	   $result = $db->setQuery($query);   
	   $acc = $db->loadResult(); 
	   
	   if ($acc == '')
	   {
			$acc = 0;
	   }

		return $acc;				
	}	

	function GetAuthorProfit($uid, $start = '', $finish = '')
	{
	   $db = JFactory::getDBO();
	   $where = '';
	   if ($start != '')
	   {
			$where = " and adate > = '$start' ";
	   }
	   if ($finish != '')
	   {
			$where = " and adate <= '$finish' ";
	   }
	   $query = "select sum(val) from `#__billing` where waiting <> 1 and order_id = 10 and uid = $uid $where";
	   $result = $db->setQuery($query);   
	   $acc = $db->loadResult(); 
	   
	   if ($acc == '')
	   {
			$acc = 0;
	   }

		return $acc;				
	}	

	function GetArticleProfit($article_id)
	{
	   $db = JFactory::getDBO();
	   $query = 'select sum(val) from `#__billing` where item_id = ' . $article_id;
	   $result = $db->setQuery($query);   
	   $acc = $db->loadResult(); 
	   
	   if ($acc == '')
	   {
			$acc = 0;
	   }

		return $acc;			
	}

	function AddFileToArticle($article_id, $tmp_name, $filename, $filesize = 0, $description = '')
	{
		$s = uniqid(rand (), true);
		$new_name = JPATH_ROOT . '/components/com_billing/files/' . $s . '_' . $filename;
		$url = JURI::base() . 'components/com_billing/files/' . $s . '_' . $filename;
		$url = str_replace('administrator/', '', $url);
	
		$x = move_uploaded_file($tmp_name, $new_name);
		$new_name = $s . '_' . $filename;
		$new_name = addslashes($new_name);
		$url = addslashes($url);
	
		if ($x)
		{
			$description = addslashes($description);
			 $db = JFactory::getDBO();
			 $d = date('Y-m-d H:i:s');
			 $query = "insert into `#__billing_articles_file` (article_id, filename, fileurl, filesize, add_date, newname, description) values ($article_id, '$filename', '$url', $filesize, '$d', '$new_name', '$description')";
			 $result = $db->setQuery($query);  
			 $result = $db->query();
		}
		 
		 return $filename;
	}	

	function AddSubscriptionToDate($uid, $date, $price, $subscription_id, $free = false)
	{
		$frozen = $this->IsSubscriptionFrozen($uid, $subscription_id);
		if($frozen == 1)
		{
			JFactory::getApplication()->enqueueMessage( JText::_('COM_BILLING_SUB_IS_FROZEN') );
			return false;
		}
		$db = JFactory::getDBO();
		$query = 'select * from `#__billing_subscription_type` where id = '. $subscription_id;
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc();       

		$discount = GetUserGroupDiscount($uid);		

		if ($discount > 0)
		{
			$price = $price - $price/100 * $discount;
			$price = round($price, 2);
		}
		if ($discount == 100)
		{
			$price = 0;
			$free = true;
		}
				
		if(!$free)
		{
		   $res = $this->WithdrawMoney($uid, $price, $subscription_id, JText::_('COM_BILLING_RENEW_SUB') .' : ' . FormatBillingDate($date));
		   if (!$res)
		   {
				return false;
		   }
		   $sum = $price;
		}
		else
		{
		   $sum = 0;
		   $this->WithdrawMoney($uid, 0, $subscription_id, JText::_('COM_BILLING_RENEW_SUB') . FormatBillingDate($date));		 
		}
		
		$newdate = $date;
		$nowdate = date("Y-m-d H:i:s");
		
		$query = 'select count(*) from `#__billing_subscription` where uid = ' . $uid . ' and subscription_id = ' . $subscription_id;
		$result = $db->setQuery($query);   
		$n = $db->loadResult();           
		
		if ($n > 0)
		{
			$query = "update `#__billing_subscription` set enddate = '$newdate', data1 = '$nowdate', data2 = '$sum' where uid = " . $uid . ' and subscription_id = ' . $subscription_id;
		}
		else
		{
			$query = "insert into `#__billing_subscription` (enddate, uid, payment_id, data1, subscription_id, data2) values('$newdate', $uid, '0', '$nowdate', $subscription_id, '$sum')"; 
		}
		$result = $db->setQuery($query);
		$db->query();
		if ($this->UserPointsEnabled() and BILLING_EXT)
		{
			$action_id = GetActionIdByCode('paysub');
			AddUserPoints($uid, $action_id);
		}
		
		return true;

	}	
	
	function CreateArticle($title, $text, $catid, $uid, $price, $fulltext = '', $state = 1, $alias = '', $lang_id = '', $counter = 0)
	{
		if(trim($title) == '')
		{
			$this->SiteMessage(JText::_('COM_BILLING_BLANK_TITLE'));
			return;
		}
		if(trim($text) == '')
		{
			$this->SiteMessage(JText::_('COM_BILLING_BLANK_BODY'));
			return;
		}
	
		$db = JFactory::getDBO();
		$tblContent = JTable::getInstance('content');
		// для нового элемента ID устанавливаем в 0
		$tblContent->id = 0;
		// это название нашего материала
		$tblContent->title = $title;
		// уникальный алиас для данного материала, содержащий текущее время
		if($alias == '')
		{
			$tblContent->alias = 'article_' . date('Y-m-d-H-i-s', time());
		}
		else
		{
			$tblContent->alias = $alias;
		}
		// Интро-текст для материала
		$tblContent->introtext = $text;
		// Полный текст материала
		$tblContent->fulltext = $fulltext;
		// state, равный 1  означает, что материал будет опубликован
		$tblContent->state = $state;
		// устанавливаем дату создания материала
		$tblContent->created = date('Y-m-d H:i:s', time());
		// получаем ID юзера, добавившего материал
		//$user = JFactory::getUser();
		$tblContent->created_by = $uid;
		$tblContent->modified_by = $uid; // по желанию
		// устанавливаем счетчик кликов
		$tblContent->hits = 1;
		//$tblContent->parentid = 0;
		// доступ материала - всем пользователям 
		//$tblContent->access = 0; 
		$tblContent->catid = $catid;
		if (JoomlaVersion() > '1.7')
		{
			$tblContent->language = $lang_id;
		}
 
		 // Check the data.
		if ( !$tblContent->check() ) 
		{
			BillingLogMessage('System', 'Create article', 'Error: ' .  $tblContent->getError() );
			$this->SiteMessage(JText::_('COM_BILLING_PUBLISH_ERROR'));
			return false;
		}
		if(!$tblContent->store()) 
		{
			// тут действия в случае неуспешности сохранения...
			BillingLogMessage('System', 'Create article', 'Error: ' .  $tblContent->getError() );
			$this->SiteMessage(JText::_('COM_BILLING_PUBLISH_ERROR'));
			return false;
		}
		
		$authorpercent = $this->GetOption('authorpercent');
		if($authorpercent == '' or $authorpercent <= 0)
		{
			$authorpercent = 90;
		}
		$id = $tblContent->id;
		$query = "insert into `#__billing_articles` (article_id, is_active, price, data2, counter) values ($id, 1, $price, $authorpercent, $counter)";
		$result = $db->setQuery($query);
		$result = $db->query();		
		
		EmailToAdmin(" <a fref='".JURI::base()."index.php?option=com_content&view=article&id=$tblContent->id'>".JText::_('COM_BILLING_NEW_ARTICLE')."</a>");
		
		if ($this->UserPointsEnabled() and BILLING_EXT)
		{
			$action_id = GetActionIdByCode('addart');
			AddUserPoints($uid, $action_id);
		}

		return $tblContent->id;
	}

	function GetNextPinCode($pin_type)
	{
		$db = JFactory::getDBO();
		$query = "select * from `#__billing_pin` where activated = 0 and pin_type_id = $pin_type order by id limit 1";
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc(); 

		$query = "select * from `#__billing_pin_type` where id = $pin_type";
		$result = $db->setQuery($query);   
		$pin = $db->loadAssoc(); 
		if($pin['php_entry'] != '' and $pin['pin_type'] == 3)
		{
			/*require_once(JPATH_ROOT . '/components/com_billing/pins/' . $pin['php_entry'] . '/' . $pin['php_filename']);
			eval('$Plugin = new '. $pin['php_class'] . '('.$pin_type.');');*/
			$Plugin = BillingPluginFactory::GetPinPlugin($pin_type, $pin['php_class'], $pin['php_entry'], $pin['php_filename']);
			return $Plugin->OnGetCode($row['pin_code'], $row['id']);
		}
		else
		{
			return $row['pin_code'];
		}		
	}	
	
	function ActivatePinCode($pin, $uid, $price = '')
	{
		$db = JFactory::getDBO();
		$query = "select * from `#__billing_pin` where pin_code = '$pin'";
		$result = $db->setQuery($query);   
		$row = $db->loadAssoc();
		$id = $row['id'];
		if($row['activated'] == 1)
		{
			return false;
		}
		$d = date('Y-m-d H:i:s');
		if($price == '')
		{
			$query = "select * from `#__billing_pin_type` where id = " . $row['pin_type_id'];
			$result = $db->setQuery($query);   
			$xrow = $db->loadAssoc();
			$price = $xrow['price'];
		}
		$query = "update `#__billing_pin` set activated = 1, uid = $uid, activation_date = '$d', price = $price where id = $id";
		$result = $db->setQuery($query);   
		$row = $db->query();
		return true;
	}
	
	function GetSuperUserGroupId()
	{
		if (JoomlaVersion() < '1.6') {return false;}
		$db = JFactory::getDBO();
		$query = "select id from `#__usergroups` where title = 'Super Users'";
		$result = $db->setQuery($query);
		return $db->loadResult();
	}

	function GetRegisteredGroupId()
	{
		if (JoomlaVersion() < '1.6') {return false;}
		$db = JFactory::getDBO();
		$query = "select id from `#__usergroups` where title = 'Registered'";
		$result = $db->setQuery($query);
		return $db->loadResult();
	}
	
	function GetPublisherGroupId()
	{
		if (JoomlaVersion() < '1.6') {return false;}
		$db = JFactory::getDBO();
		$query = "select id from `#__usergroups` where title = 'Publisher'";
		$result = $db->setQuery($query);
		return $db->loadResult();
	}
	
	function IsSuperUser($uid)
	{
		if (JoomlaVersion() < '1.6') {return false;}
		$gid = $this->GetSuperUserGroupId();
		$db = JFactory::getDBO();
		$query = "select count(*) from `#__user_usergroup_map` where user_id = $uid and group_id = $gid";
		$result = $db->setQuery($query);
		if($db->loadResult() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function IsUserInGroup($uid, $id)
	{
		if (JoomlaVersion() < '1.6') {return false;}
		$db = JFactory::getDBO();
		$query = "select count(*) from `#__user_usergroup_map` where user_id = $uid and group_id = $id";
		$result = $db->setQuery($query);   
		if($db->loadResult() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}		
	}

	function IsUserInJSGroup($uid, $id)
	{
		if (JoomlaVersion() < '1.6') {return false;}
		$db = JFactory::getDBO();
		$query = "select count(*) from `#__community_groups_members` where memberid = $uid and groupid = $id and approved = 1";
		$result = $db->setQuery($query);   
		if($db->loadResult() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}		
	}
	
	function CountOfUserGroups($uid)
	{
		if (JoomlaVersion() < '1.6') {return false;}
		$db = JFactory::getDBO();
		$query = "select count(*) from `#__user_usergroup_map` where user_id = $uid";
		$result = $db->setQuery($query);  
		return $db->loadResult();
	}
	
	function AddUserToGroup($uid, $id)
	{
		if (JoomlaVersion() < '1.6') {return false;}
		if($this->IsUserInGroup($uid, $id))
		{
			return true;
		}
		$db = JFactory::getDBO();
		$query = "insert into `#__user_usergroup_map`(user_id, group_id) values ($uid, $id)";
		$result = $db->setQuery($query);  
		$result = $db->query();
		return $result;
	}
	
	function AddUserToJSGroup($uid, $id)
	{
		if($this->IsUserInJSGroup($uid, $id))
		{
			return true;
		}
		$db = JFactory::getDBO();
		$query = "insert into `#__community_groups_members`(memberid, groupid, approved) values ($uid, $id, 1)";
		$result = $db->setQuery($query);  
		$result = $db->query();
		return $result;
	}

	
	function RemoveUserFromGroup($uid, $id, $newid = '')
	{
		if (JoomlaVersion() < '1.6') {return false;}
		if($this->CountOfUserGroups($uid) == 1 and $newid == '')
		{
			return false;
		}
		$db = JFactory::getDBO();
		$query = "delete from `#__user_usergroup_map` where user_id = $uid and group_id = $id";
		$result = $db->setQuery($query);  
		$result = $db->query();
		if($newid != '' and $this->CountOfUserGroups($uid) == 0)
		{
			$result = $this->AddUserToGroup($uid, $newid);
		}
		return $result;
	}

	function RemoveUserFromJSGroup($uid, $id)
	{
		$db = JFactory::getDBO();
		$query = "delete from `#__community_groups_members` where memberid = $uid and groupid = $id";
		$result = $db->setQuery($query);  
		$result = $db->query();
		return $result;
	}

	function GetGroupSub($uid)
	{
		if (JoomlaVersion() < '1.6') {return false;}
		$arr = array();
		$db = JFactory::getDBO();
		$query = "select distinct subscription_id, item_id from `#__billing_subscription_access` where item_type = 9";
		$result = $db->setQuery($query);  
		$rows = $db->loadObjectList();
		foreach($rows as $row)
		{
			$query = "select * from `#__billing_subscription` where subscription_id = $row->subscription_id and uid = $uid and frozen = 0";
			$result = $db->setQuery($query);  
			$sub = $db->loadAssoc();
			if($this->IsUserInGroup($uid, $row->item_id))
			{
				$arr[] = $row->subscription_id;
			}
		}
		return $arr;
	}
	
	function GetJSGroupSub($uid)
	{
		$arr = array();
		$db = JFactory::getDBO();
		$query = "select distinct subscription_id, item_id from `#__billing_subscription_access` where item_type = 12";
		$result = $db->setQuery($query);  
		$rows = $db->loadObjectList();
		foreach($rows as $row)
		{
			$query = "select * from `#__billing_subscription` where subscription_id = $row->subscription_id and uid = $uid and frozen = 0";
			$result = $db->setQuery($query);  
			$sub = $db->loadAssoc();
			if($this->IsUserInJSGroup($uid, $row->item_id))
			{
				$arr[] = $row->subscription_id;
			}
		}
		return $arr;
	}

	function GetActualUserSubscriptions($uid)
	{
		$arr = array();
		$db = JFactory::getDBO();
		$query = "select * from `#__billing_subscription` where uid = $uid and frozen = 0 and enddate > NOW() and subscription_id in (select id from `#__billing_subscription_type` where is_active = 1)";
		$result = $db->setQuery($query);  
		$rows = $db->loadObjectList();
		foreach($rows as $row)
		{
			$arr[] = $row->subscription_id;
		}
		return $arr;
	}
	
	function GetActualUserSubByType($uid, $item_type)
	{
		$arr = array();
		$db = JFactory::getDBO();
		$query = "select * from `#__billing_subscription` where uid = $uid and frozen = 0 and enddate > NOW() and subscription_id in (select distinct subscription_id from `#__billing_subscription_access` where item_type = $item_type) and subscription_id in (select id from `#__billing_subscription_type` where is_active = 1)";
		$result = $db->setQuery($query);  
		$rows = $db->loadObjectList();
		foreach($rows as $row)
		{
			$arr[] = $row->subscription_id;
		}
		return $arr;
	}
	
	function GetCustomItemData($sub_id, $row_id = '')
	{
		$arr = array();
		$db = JFactory::getDBO();
		$query = "select * from `#__billing_subscription_access` where subscription_id = $sub_id";
		if($row_id != '')
		{
			$query .= ' and id = ' . $row_id;
		}
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList();
		foreach($rows as $row)
		{
			$field_name = $row->file_path;
			$id_name = $row->data3;
			$item_id = $row->item_id;
			$table = $row->data4;
			$query1 = "select * from $table where $id_name = $item_id";
			$rr = $db->setQuery($query1); 
			$xxx = $db->loadAssoc();
			$arr[] = $xxx;
		}	

		return $arr;
	}

	function UpdateGroupSub($subscription_id)
	{
		$db = JFactory::getDBO();
		$query = "select * from `#__billing_subscription_access` where subscription_id = $subscription_id and item_type = 9";
		$result = $db->setQuery($query);  
		$groups = $db->loadObjectList();
		
		$query = "select * from `#__billing_subscription` where subscription_id = $subscription_id and enddate > NOW() and frozen = 0";
		$result = $db->setQuery($query);  
		$users = $db->loadObjectList();

		if(count($users) > 0 and count($groups) > 0)	
		{
			foreach($users as $user)
			{
				foreach($groups as $group)
				{
					$this->AddUserToGroup($user->uid, $group->item_id);
				}
			}
		}

		$query = "select * from `#__billing_subscription_access` where subscription_id = $subscription_id and item_type = 12";
		$result = $db->setQuery($query);  
		$groups = $db->loadObjectList();
		
		$query = "select * from `#__billing_subscription` where subscription_id = $subscription_id and enddate > NOW() and frozen = 0";
		$result = $db->setQuery($query);  
		$users = $db->loadObjectList();

		if(count($users) > 0 and count($groups) > 0)	
		{
			foreach($users as $user)
			{
				foreach($groups as $group)
				{
					$this->AddUserToJSGroup($user->uid, $group->item_id);
				}
			}
		}

	}

	function SiteMessage($message)
	{
		JFactory::getApplication()->enqueueMessage( $message );
	}	

	function GetAuthorPercent($uid, $article_id = '')
	{
		$db = JFactory::getDBO();
		if($article_id != '')
		{
			$query = "select * from `#__billing_articles` where article_id = $article_id";
			$result = $db->setQuery($query);  
			$row = $db->loadAssoc();
			$persent = $row['data2'];
			if($persent != '' and $persent > 0 and $persent <= 100)
			{
				return $persent;
			}
		}
		$authorpercent = $this->GetOption('authorpercent');
		$gid = GetUserGroupId($uid);
		if($gid != '')
		{
			$query = "select * from `#__billing_group` where id = $gid";
			$result = $db->setQuery($query);  
			$group = $db->loadAssoc();
			
			if($group['option1'])
			{
				$discount = $group['discount'];
				if($discount != '')
				{
					return $discount;
				}
			}
		}
		if($authorpercent != '')
		{
			return $authorpercent;
		}
		return 100;
	}

	function UnlockUser($uid)
	{
		$db = JFactory::getDBO();
		$pass = JUserHelper::genRandomPassword();
		$salt = JUserHelper::genRandomPassword();
		$crypt = JUserHelper::getCryptedPassword($pass, $salt);
		$query = "update `#__users` set block = 0, activation = '', password = '$crypt:$salt' where id = $uid";
		$result = $db->setQuery($query);  
		$row = $db->query();

		$query = "select * from `#__users` where id = $uid";
		$result = $db->setQuery($query);  
		$user = $db->loadAssoc();
		
		$lang = JFactory::getLanguage();
		$lang->load('com_user');
		$lang->load('com_users');
		$email = $user['email'];
		
		if(! JMailHelper::isEmailAddress( $email ) ){
			JError::raiseWarning('', JText::_( 'COM_USERS_EMAIL_NOT_VALID'));
			return false;
		}
		$message = JText::_('COM_USERS_REGISTRATION_COMPLETE_ACTIVATE');
		$config = JFactory::getConfig();
		$data['fromname']	= $config->get('fromname');
		$data['mailfrom']	= $config->get('mailfrom');
		$data['sitename']	= $config->get('sitename');
		$data['siteurl']	= JUri::root();
		$data['password_clear'] = $pass;
		$data['email'] = $email;
		$data['name'] = $email;
		$data['username'] = $email;
		$emailBody = "Здравствуйте ".$data['name'].",\n\nВаша учётная запись активирована. Вы можете войти на сайт «".$data['sitename']."». \n Ваш логин ".$data['username']."\n Пароль ".$data['password_clear'];
		/*$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY',
					$data['name'],
					$data['sitename'],
					$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear']
				);*/
		$emailSubject	= JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);
		$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);
	}

	
	function TableExists($table)
	{
		$db = JFactory::getDBO();
		$db->setQuery("SHOW TABLES LIKE '$table'");
		$n = $db->loadResult();
		if($n != '')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function ExtensionExists($ext)
	{
		$db = JFactory::getDBO();
		$db->setQuery("select count(*) from `#__extensions` where `name` = '$ext'");
		$n = $db->loadResult();
		if($n > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function MultiPayment($uid, $value, $pid = '', $descr = '', $percents, $acc = 0, $order_id = 0, $order_name = '')
	{
		$balance = $this->GetBalance($uid);
		if ($balance < abs($value))
		{
			echo '<p>'.JText::_('COM_BILLING_NO_MONEY_ACCESS').'</p>';
			return false;
		}
		if($pid == '')
		{
			$pid = date('His') . $uid . rand(999, 9999);
		}
		$date = date('Y-m-d H:i:s');
		$ip = GetBillingIp(); 
		$value = - $value;
		
		if ($descr == '')
		{
			$descr = JText::_('COM_BILLING_WITHDR_MONEY');
		}		
		$descr = addslashes($descr);
		
		$db = JFactory::getDBO();
		if ($acc != 0)
		{
			$ab = GetAccountCurrencyAbbr($acc);
		}
		else
		{
			$ab = GetDefaultCurrencyAbbr();
		}
	    $query = "insert into `#__billing` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`, `acc`, `order_id`, `order_name`) values('$date', $uid, '$pid', '$ab', $value, '$descr', '$ip', $acc, $order_id, '$order_name')";
		$result = $db->setQuery($query);   
		$n = $db->query(); 
		
		$query = "select * from `#__billing_group` where data1 <> '' order by data1 desc";
		$result = $db->setQuery($query);  
		$groups = $db->loadObjectList();
		if(count($groups) > 0)
		{
			$query = "select sum(val) from `#__billing` where uid = $uid and val < 0";
			$result = $db->setQuery($query);  
			$spent = - $db->loadResult();

			$group_id = GetUserGroupId($uid);

			$query = "select data1 from `#__billing_group` where id = $group_id";
			$result = $db->setQuery($query);  
			$dsc = $db->loadResult();

			if($dsc < $spent) // если текущая групповая скидка меньше потраченной суммы
			{
				foreach($groups as $group)
				{
					if($group->data1 > $spent)
					{
						$this->AddUserToBillingGroup($uid, $group->id);
						break;
					}
				}
			}
		}
		
		$partner_id = $this->HasDaddy($uid);
		$oid = $uid;
		$i = 0;
		$cnt = count($percents) - 1;
		while ($partner_id > 0)
		{
			if($i <= $cnt)
			{
				$percent = $percents[$i];	
				$i++;
				$value2 = - $value / 100 * $percent;
				$username = GetUserNameBy_Id($oid);
				$descr = JText::_('COM_BILLING_PARTNER_PROFIT') . " $username (id:$oid)";
				$descr = addslashes($descr);
				$query = "insert into `#__billing` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`) values('$date', $partner_id, '3', '".GetDefaultCurrencyAbbr()."', $value2, '$descr', '')";
				$result = $db->setQuery($query);   
				$n = $db->query();
				
				$query = "insert into `#__billing_partner_history` (uid, partner_id, operation_id, val, oper_date, data1) values($partner_id, $oid, 2, $value2, '$date', '$descr')";
				$result = $db->setQuery($query);  
				$result = $db->query();
				$oid = $partner_id;
				$partner_id = $this->HasDaddy($oid);
			}
			else
			{
				break;
			}
		}
		if ($this->UserPointsEnabled() and BILLING_EXT and $partner_id > 0)
		{
			$action_id = GetActionIdByCode('paypartner');
			AddUserPoints($partner_id, $action_id);
		}
		
		return true;	
	}

function AddK2Article($title, $text, $catid, $uid, $price, $fulltext = '', $state = 1, $alias = '', $lang_id = '', $counter = 0)
{
	/*if($alias == '')
	{
		$alias = 'article_' . date('Y-m-d-H-i-s', time());
	}*/
	$DT = date('Y-m-d H:i:s');
	$db = JFactory::getDBO();
	$query = "insert into `#__k2_items` (`title`, `alias`, `catid`, `published`, `introtext`, `fulltext`, `created`, `created_by`, `language`) 
		values ('$title', '$alias', $catid, 1, '$text', '$fulltext', '$DT', $uid, '$lang_id')";
	//echo $query ;	
	$result = $db->setQuery($query);  
	$result = $db->query();
	$id = $db->insertid();
	
	$authorpercent = BillingGetOption('authorpercent');
	if($authorpercent == '' or $authorpercent <= 0)
	{
		$authorpercent = 90;
	}
	$query = "insert into `#__billing_articles` (article_id, is_active, price, data2, counter, k2) values ($id, 1, $price, $authorpercent, $counter, 1)";
	$result = $db->setQuery($query);
	$result = $db->query();		
	
	EmailToAdmin(" <a fref='".JURI::base()."index.php?option=com_k2&view=item&layout=item&id=$id'>".JText::_('COM_BILLING_NEW_ARTICLE')."</a>");
	
	if (UserPointsEnabled() and BILLING_EXT)
	{
		$action_id = GetActionIdByCode('addart');
		AddUserPoints($uid, $action_id);
	}
	return $id;

}

//*************************************** Pharaoh section ************************************************************************/
	function GetDividendBalance($uid)
	{
	   $db = JFactory::getDBO();
	   $query = 'select sum(val) from `#__billing_dividend` where waiting <> 1 and uid = ' . $uid;
	   $result = $db->setQuery($query);   
	   $acc = $db->loadResult(); 
	   
	   if ($acc == '')
	   {
			$acc = 0;
	   }

		return $acc;
	}

	function GetDividendHistory($uid, $page = 0, $count = 30)
	{
		$user = JFactory::getUser();
		$zuid = $user->id;
		
		$out = '';
		
		if ($page > 1)
		{
			$page = ($page - 1) * $count;
		}
		
		if ($page == 1)
		{
			$page = 0;
		}

		$out .= "<h2 class='h2_billing'>".JText::_('COM_BILLING_DIVIDEND_PAY')."</h2>";

	   $db = JFactory::getDBO();
	   $query = "select * from `#__billing_dividend` where uid = $uid order by id desc limit $page, $count";
	   $result = $db->setQuery($query);   
	   $rows = $db->loadObjectList(); 	
	   $n = 1;
		
		if(count($rows) == 0)
		{
			return;
		}
		
		$out .= '<table width="100%" class="adminlist table billing_table" cellspacing="1">';
		$out .= "<thead class='billing_head'>";
		$out .= "  <th class='billing_th'>Id</th>";
		$out .= "  <th class='billing_th'>".JText::_('COM_BILLING_DATE')."</th>";
		$out .= "  <th class='billing_th'>".JText::_('COM_BILLING_ACTIONS')."</th>";
		$out .= "  <th class='billing_th'>".JText::_('COM_BILLING_SUM')." ( ". GetDefaultCurrencyAbbr() .")</th>";
		if ($this->MultiAccount())
		{
			$out .= "  <th class='billing_th'>".JText::_('COM_BILLING_ACCOUNTS')."</th>";
		}
		if ($this->GetOption('showip'))
		{
			$out .= "  <th class='billing_th'>IP</th>";
		}
		$out .= "</thead>";
		
		foreach ( $rows as $row ) 
		{
			if (($n & 1) == 0)
			{
				$out .= '<tr class="trodd">';
			}
			else
			{
				$out .= '<tr class="treven">';
			}
			$n = $n + 1;
			
			$out .= "  <td class='billing_td'>$row->id</td>";
			$out .= "  <td class='billing_td'>". FormatBillingDate($row->adate)."</td>";
			if ($row->waiting == 1)
			{
				$out .= "  <td class='billing_td'>$row->data1 (<a href='index.php?option=com_billing&view=billing&task=confirm&id=$row->id&uid=$uid'>".JText::_('COM_BILLING_CONFIRM_WAIT')."</a>)</td>";
			}
			elseif($row->data3 == '1')
			{
				$out .= "  <td class='billing_td'>$row->data1 <a href='index.php?option=com_billing&view=billing&task=cancelcashout&id=$row->id&uid=$uid'>".JText::_('COM_BILLING_RETURN_MONEY')."</a></td>";
			}
			else
			{
				$out .= "  <td class='billing_td'>$row->data1</td>";
			}
			$out .= "  <td class='billing_td' align='right'>". number_format($row->val, 2, '.', ' ')."</td>";  
			if ($this->MultiAccount())
			{
				if ($row->acc != '')
				{
					$out .= "  <td class='billing_td'>".$this->GetAccountTypeByAccId($row->acc)."</td>";
				}
				else
				{
					$out .= "  <td class='billing_td'></td>";
				}
			}
			if ($this->GetOption('showip'))
			{
				$out .= "  <td class='billing_td'>$row->data2</td>";
			}
			$out .= "</tr>";
		}
		$out .= '</table>';
		
		
		$query = "select count(*) from `#__billing_dividend` where uid = $uid";
		$result = $db->setQuery($query);   
	    $n = $db->loadResult();
		
		if ($n % $count == 0)
		{
			$n = (int) $n/$count;
		}
		else
		{
			$n = (int) $n/$count + 1; 
		}
		
		$out .= '<p align="center">';
		
		for($i=1; $i <= $n; $i++)
		{
			if ($page == $i)
			{
				$out .= " $i ";
			}
			else
			{
				$out .= " <a href='index.php?option=com_billing&view=billing&task=pages&page=$i'>$i</a> ";
			}
		}
		
		$out .= '</p>';
		
		return $out;
	}
			
	function AddDividendsEasy($uid, $value, $pid = '', $descr = '', $wait = 0, $acc = '', $order_id = '', $item_id = '')
	{
		$date = date('Y-m-d H:i:s');
		$ip = GetBillingIp(); 

		if ($descr == '')
		{
			$descr = JText::_('COM_BILLING_ADD2_MONEY');
		}
		if($pid == '')
		{
			$pid = date('His') . $uid . rand(999, 9999);
		}
		$descr = addslashes($descr);
		
		$db = JFactory::getDBO();
		
		$query = "select count(*) from `#__billing_dividend` where `payment_id` = '$pid' and `val` = $value";
		$result = $db->setQuery($query);   
		$n = $db->loadResult();
		
		if ($n > 0 )
		{
			BillingLogMessage('MLM', 'Dividends', JText::_('COM_BILLING_ALREADYX') . " $pid, $value");
			echo "<p>".JText::_('COM_BILLING_ALREADYX')."</p>";
			return;
		}
		if ($order_id == '') {$order_id = 0;}
		if ($item_id == '') {$item_id = 0;}
		if ($this->MultiAccount() and $acc != '')
		{
			$query = "insert into `#__billing_dividend` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`, `waiting`, `acc`, `order_id`, `item_id`) values('$date', $uid, '$pid', '".GetAccountCurrencyAbbr($acc)."', $value, '$descr', '$ip', $wait, $acc, $order_id, $item_id)";
		}
		else
		{	
			$query = "insert into `#__billing_dividend` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`, `waiting`, `order_id`, `item_id`) values('$date', $uid, '$pid', '".GetDefaultCurrencyAbbr()."', $value, '$descr', '$ip', $wait, $order_id, $item_id)";
		}
		BillingLogMessage('MLM', 'Dividends', urlencode($query));
		$result = $db->setQuery($query);   
		$n = $db->query(); 
		$id = $db->insertid();
		if ($this->UserPointsEnabled() and BILLING_EXT)
		{
			$action_id = GetActionIdByCode('addmoney');
			AddUserPoints($uid, $action_id);
		}
		return $id;
	}

	function WithdrawDividendMoney($uid, $value, $pid = '', $descr = '', $acc = 0, $order_id = 0, $order_name = '', $no_partner = false, $wait = 0)
	{
		$balance = $this->GetDividendBalance($uid);
		
		if ($balance < abs($value))
		{
			echo '<p>'.JText::_('COM_BILLING_NO_MONEY_ACCESS').'</p>';
			return false;
		}
		if($pid == '')
		{
			$pid = date('His') . $uid . rand(999, 9999);
		}

		$date = date('Y-m-d H:i:s');
		$ip = GetBillingIp(); 
		$value = - $value;
		
		if ($descr == '')
		{
			$descr = JText::_('COM_BILLING_WITHDR_MONEY');
		}		
		$descr = addslashes($descr);
		
		$db = JFactory::getDBO();
		if ($acc != 0)
		{
			$ab = GetAccountCurrencyAbbr($acc);
		}
		else
		{
			$ab = GetDefaultCurrencyAbbr();
		}
	    $query = "insert into `#__billing_dividend` (`adate`, `uid`, `payment_id`, `curr`, `val`, `data1`, `data2`, `acc`, `order_id`, `order_name`, `waiting`) values('$date', $uid, '$pid', '$ab', $value, '$descr', '$ip', $acc, $order_id, '$order_name', $wait)";
		$result = $db->setQuery($query);   
		$n = $db->query(); 

		return true;
	}
	
	function MLParent($uid)
	{
		$db = JFactory::getDBO();
		
		$query = "select uid from `#__billing_partner` where partner_id = $uid";
		$result = $db->setQuery($query);   
		return $db->loadResult();
	}
	
	function MLPercent($uid, $price, $descr)
	{	
		$db = JFactory::getDBO();
		$p_percent = $this->GetOption('p_percent');
		$p_depth = $this->GetOption('p_depth');
		if(multy)
		{
			BillingLogMessage('System', 'Percent', "IsMulty");
			$p_depth = 5;
			$p_percent = BillingGetOption("level1");
		}
		BillingLogMessage('System', 'Percent', "percent: $p_percent depth: $p_depth");
		if($p_percent != '' and $p_percent > 0 and $p_depth != '' and $p_depth > 1)
		{
			$price2 = $price /100 * $p_percent;
			$parent = $uid;
			BillingLogMessage('System', 'Percent', "price: $price2 uid: $uid");
			//$depth = 0l
			$p_depth = 1;
			while($p_depth <= 6)
			{		
				$old = $parent;
				//$parent = $this->MLParent($parent); // тот, кто нас пригласил
				$query = "select uid from `#__billing_partner` where partner_id = $parent";
				$result = $db->setQuery($query);   
				$parent = $db->loadResult();				
				
				if ($parent != '' and $parent > 0)
				{
					if(multy)
					{
						$price2 = $price /100 * BillingGetOption("level$p_depth");
						$price2 = abs(round($price2, 2));
						BillingLogMessage('System', 'Percent', "Level: $p_depth; price: $price2 uid: $uid; percent: " . BillingGetOption("level$p_depth"));
					}
					if(BillingGetOption('partpoint') and $this->UserPointsEnabled() and BILLING_EXT)
					{
						AddUPAction(JText::_('COM_BILLING_PARTNER_PROFIT'), $descr, $price2); 
					}
					else
					{	
						$pid = date('His') . $parent . rand(999, 9999);
						$this->AddMoneyEasy($parent, $price2, $pid, JText::_('COM_BILLING_PARTNER_PAYMENT'). ' '. $descr);
						$date = date('Y-m-d H:i:s');
					}
					$descr = addslashes($descr);
					$query = "insert into `#__billing_partner_history` (uid, partner_id, operation_id, val, oper_date, data1) values($parent, $old, 2, $price2, '$date', '".JText::_('COM_BILLING_PARTNER_PAYMENT')." $descr')";
					$result = $db->setQuery($query);  
					$result = $db->query();
					
					BillingLogMessage('System', 'Percent', "uid: $parent partner: $old");

					$p_depth = $p_depth + 1;
				}
				else
				{
					break;
				}
			}	
		}
	}

}
	
?>