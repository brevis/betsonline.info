<?php

defined('_JEXEC') or die('Restricted access');

require_once JPATH_ROOT . '/components/com_billing/tabplugin.php';
require_once JPATH_ROOT . '/components/com_bets/helpers/bet.php';
require_once JPATH_ROOT . '/components/com_bets/helpers/results.php';
require_once JPATH_ROOT . '/components/com_bets/helpers/line.php';

class Bets extends TabPlugin
{
	const TAB_NO = 3;

	function Bets($pname)
	{
		TabPlugin::TabPlugin($pname);

		$lang = JFactory::getLanguage();
		$lang->load('com_bets', JPATH_SITE, $lang->getTag(), true);
	}

	function Tooltip($text)
	{
		return '';
	}

	function Show()
	{
		$app = JFactory::getApplication();
		$display = $app->input->getString('display');

		switch ($display)
		{
			case 'end':
				return $this->displayEndBets();
				break;

			default:
				return $this->displayCurrentBets();
				break;
		}
	}

	function Init()
	{
		parent::Init();
	}

	protected function displayCurrentBets()
	{
		$user = JFactory::getUser();
		$tabno = self::TAB_NO;
		$bets = BetHelper::getUserBets($user->id);

		ob_start();
		include __DIR__ . '/currentbets.tpl.php';
		$tpl = ob_get_contents();
		ob_clean();

		return $tpl;
	}

	protected function displayEndBets()
	{
		$user = JFactory::getUser();
		$tabno = self::TAB_NO;
		$bets = BetHelper::getUserBets($user->id, ResultsHelper::STATUS_END);

		$tabno = self::TAB_NO;

		ob_start();
		include __DIR__ . '/endbets.tpl.php';
		$tpl = ob_get_contents();
		ob_clean();

		return $tpl;
	}
}