<?php  defined('_JEXEC') or die('Restricted access');

?>
<ul class="nav nav-pills">
	<li role="presentation" class="active"><a href="<?php echo JRoute::_('index.php?option=com_billing&tabno=' . $tabno); ?>">Текущие</a></li>
	<li role="presentation"><a href="<?php echo JRoute::_('index.php?option=com_billing&tabno=' . $tabno . '&display=end'); ?>">Завершенные</a></li>
</ul>

<table class="table table-striped">
	<thead>
	<tr>
		<th>#</th>
		<th>Дата ставки</th>
		<th>Событие</th>
		<th>Ставка</th>
	</tr>
	</thead>
	<tbody>
	<?php if (is_array($bets) && count($bets) > 0) : ?>
		<?php foreach($bets as $i=>$bet) : ?>
		<tr>
			<td><?php echo $bet->bet_id; ?></td>
			<td><?php echo LineHelper::formatDate($bet->bet_created); ?></td>
			<td><?php echo LineHelper::formatDate($bet->datetime); ?><br><?php echo htmlspecialchars(LineHelper::getEventTitle($bet)); ?></td>
			<td>
				<?php echo !empty($bet->bet_title) ? $bet->bet_title : BetHelper::stringifyBet($bet, $bet->bet_type); ?><br>
				<span class="bet_sum">
					<span><?php echo JText::_('COM_BETS_BET_SUM'); ?>:</span> <b><?php echo FormatDefaultCurrency($bet->bet_sum); ?></b>
				</span>
			</td>
		</tr>
		<?php endforeach; ?>
	<?php else: ?>
	<tr>
		<td colspan="10">Ставок нет</td>
	</tr>
	</tbody>
	<?php endif; ?>
</table>