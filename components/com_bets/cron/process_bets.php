<?php
// We are a valid entry point.
const _JEXEC = 1;

$base_dir = realpath(__DIR__ . '/../../../');

// Load system defines
if (file_exists($base_dir . '/defines.php'))
{
	require_once $base_dir . '/defines.php';
}

if (!defined('_JDEFINES'))
{
	define('JPATH_BASE', $base_dir);
	require_once JPATH_BASE . '/includes/defines.php';
}

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

// Load the configuration
require_once JPATH_CONFIGURATION . '/configuration.php';

// Configure error reporting to maximum for CLI output.
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Load Library language
$lang = JFactory::getLanguage();

// Try the files_joomla file in the current language (without allowing the loading of the file in the default language)
$lang->load('files_joomla.sys', JPATH_SITE, null, false, false)
// Fallback to the files_joomla file in the default language
|| $lang->load('files_joomla.sys', JPATH_SITE, null, true);

require_once JPATH_ROOT . '/components/com_bets/helpers/line.php';
require_once JPATH_ROOT . '/components/com_bets/helpers/results.php';
require_once JPATH_ROOT . '/components/com_bets/helpers/bet.php';

if (!isset($_SERVER['REMOTE_ADDR']))
{
	$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
}

/**
 * A command line cron job to attempt to remove files that should have been deleted at update.
 *
 * @since  3.0
 */
class ProcessBetsCli extends JApplicationCli
{
	/**
	 * Entry point for CLI script
	 *
	 * @return  void
	 *
	 * @since   3.0
	 */
	public function doExecute()
	{
		@set_time_limit(0);

		// Import the dependencies
		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');

		$lang = JFactory::getLanguage();
		$lang->load('com_bets', JPATH_SITE, 'ru-RU', true);

		$db = JFactory::getDbo();

		echo 'Start process bets...' . PHP_EOL;

		$unprocessedBets = BetHelper::getUnprocessedBets();
		if (is_array($unprocessedBets) && $count = count($unprocessedBets) > 0)
		{
			$ua = new UserAccount();

			foreach($unprocessedBets as $i=>$bet)
			{
				$win = BetHelper::getWin($bet);
				if ($win)
				{
					// take coef from `users_bet` table
					$win = floatval($bet->bet_sum) * floatval($bet->bet_coef);

					$betTitle = '#' . $bet->bet_id . ': ' . $bet->event_date . ', '
							. LineHelper::getEventTitle($bet) . ', ' . $bet->bet_title;
					$message = JText::sprintf('COM_BETS_WIN_ADD_MONEY_NOTIFY', $betTitle);
					$pid = $this->getPid($bet->bet_uid);
					$ua->AddMoneyEasy($bet->bet_uid, $win, $pid, $message);
				}

				$db->setQuery("UPDATE users_bets SET processed = 1, win = " . ($win ? 1 : 0) . " WHERE id=" . (int)$bet->bet_id)->execute();

				echo ($i+1) . '/' . $count . ': ' . ($win ? '+' : '-') . PHP_EOL;
			}
		}

		echo 'Done.' . PHP_EOL;
	}

	protected function getPid($uid)
	{
		list($whole, $decimal) = explode(' ', microtime());
		return $decimal . $uid;
	}

}

// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('ProcessBetsCli')->execute();

