<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Bets Component Results Helper
 *
 * @package     Bets
 * @subpackage  Helpers
 */
abstract class ResultsHelper
{
	const STATUS_NOT_START = 1;
	const STATUS_CURRENT = 2;
	const STATUS_END = 3;

	const SORT_ID = 1;
	const SORT_TIME = 2;

	const RESULT_END = 'итог';

	static protected $eventTypes = array(
			1 => 'Футбол',
			2 => 'Баскетбол',
			3 => 'Волейбол',
			4 => 'Гандбол',
			5 => 'Футзал',
			6 => 'Регби',
			7 => 'Бейсбол',
			8 => 'Теннис',
			9 => 'Наст. теннис',
			10 => 'Дартс',
			11 => 'Смешанные единоборства',
			12 => 'Жен. Смешанные единоборства',
			13 => 'Бокс',
			14 => 'Киберспорт',
			15 => 'Пляжный волейбол',
			16 => 'Смешанные единоборства',
			17 => 'Бадминтон',
			18 => 'Водное поло',
			19 => 'Шахматы',
			20 => 'Снукер',
			21 => 'Хоккей',
	);

	public static function getAllStatuses()
	{
		return array(
			self::STATUS_NOT_START,
			self::STATUS_CURRENT,
			self::STATUS_END,
		);
	}

	public static function getAllSorts()
	{
		return array(
			self::SORT_ID,
			self::SORT_TIME,
		);
	}

	public static function getAllTypes()
	{
		return self::$eventTypes;
	}

	public static function getTypeBySection($section)
	{
		if (is_numeric($section))
		{
			$section = JFactory::getDbo()
					->setQuery("SELECT title FROM bets_sections WHERE sectionid = " . (int) $section)
					->loadResult();

			if (empty($section)) return null;

			foreach (self::$eventTypes as $id => $type)
			{
				if (preg_match('/^' . preg_quote($type) . '/Uis', $section))
				{
					return $id;
				}
			}
		}
		elseif (is_array($section))
		{
			$section = array_filter($section, function($item){
				return is_numeric($item);
			});
			if (count($section) < 1) return null;

			$sections = JFactory::getDbo()
					->setQuery("SELECT sectionid, title FROM bets_sections WHERE sectionid IN (".implode(",", $section).")")
					->loadObjectList();
			if (!is_array($sections) || count($sections) < 1) return null;

			$types = array();
			foreach($sections as $s)
			{
				foreach (self::$eventTypes as $typeId => $type)
				{
					if (preg_match('/^' . preg_quote($type) . '/Uis', $s->title))
					{
						$types[$s->sectionid] = $typeId;
					}
				}
			}

			return $types;
		}

		return null;
	}

	public static function getSectionsByType($type, $events = false)
	{
		if (!is_array($type))
		{
			$type = explode(',', $type);
		}

		$allTypes = self::$eventTypes;
		$type = array_filter($type, function($item) use ($allTypes){
			return is_numeric($item) && isset($allTypes[$item]);
		});

		if (!is_array($type) || count($type) < 1) return null;

		// TODO: think how to optimize (change DB structure)

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('s.*');
		if ($events)
		{
			$query->from('bets_sections s');
		}
		else
		{
			$query->from('results_sections s');
		}
		foreach($type as $t)
		{
			$query->where('s.title LIKE \'' . $db->escape(self::$eventTypes[$t]) . '%\'', "OR");
		}

		return $db->setQuery($query)->loadObjectList();
	}

	public static function getResults($filter = array())
	{
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('r.*, r2.title as parent_title')->from('results r');
		$query->join('LEFT', 'results r2 ON (r.parent = r2.id)');

		$query->select('s.title as section_title');
		$query->join('LEFT', 'results_sections s ON (r.sectionid = s.id)');

		// date
		$query->where('STR_TO_DATE(`r`.`datetime`, \'%d.%m.%Y\') = \'' . $query->escape($filter['date']) . '\'');

		// type & section
		if (count($filter['section']) > 0)
		{
			$query->where('r.sectionid IN (' . implode(',', $filter['section']) . ')');
		}
		elseif (count($filter['type']) > 0)
		{
			$sections = self::getSectionsByType($filter['type']);
			if (is_array($sections) && count($sections) > 0)
			{
				$ids = array();
				foreach ($sections as $section)
				{
					$ids[] = (int) $section->id;
				}

				$query->where('r.sectionid IN (' . implode(',', $ids) . ')');
			}
		}

		// status
		$status = array();
		if (in_array(self::STATUS_NOT_START, $filter['status']))
		{
			$status[] = 'STR_TO_DATE(`r`.`datetime`, \'%H.%i\') > \'' . date('H:i') . '\' AND r.status <> \'итог\'';
		}
		if (in_array(self::STATUS_CURRENT, $filter['status']))
		{
			$status[] = 'STR_TO_DATE(`r`.`datetime`, \'%H.%i\') <= \'' . date('H:i') . '\' AND r.status <> \'итог\'';
		}
		if (in_array(self::STATUS_END, $filter['status']))
		{
			$status[] = 'r.status = \'' . self::RESULT_END . '\'';
		}
		if (count($status) > 0)
		{
			$query->where('(' . implode(' OR ', $status) . ')');
		}

		// sort
		if ($filter['sort'] == self::SORT_ID)
		{
			$query->order('r.parser_betid DESC');
		}
		else
		{
			$query->order('`r`.`datetime` DESC');
		}

		$results = array();
		foreach($db->setQuery($query)->loadObjectList() as $row)
		{
			if (!isset($results[$row->sectionid]))
			{
				$results[$row->sectionid] = array();
			}

			$results[$row->sectionid][] = $row;
		}

		return $results;
	}

	public static function getTime($date)
	{
		$date = new DateTime($date);
		return $date->format('H:i');
	}

}
