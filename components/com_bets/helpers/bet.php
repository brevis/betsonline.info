<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");

require_once __DIR__ . '/results.php';
$uaFileName = JPATH_ROOT . '/components/com_billing/useraccount.php';
if (!is_file($uaFileName))
{
	throw new Exception('Billing not installed. ["useraccount.php" not found.]');
}

require_once $uaFileName;

/**
 * Bets Component Bet Helper
 *
 * @package     Bets
 * @subpackage  Helpers
 */
abstract class BetHelper
{
	const BET_TYPE_1 = 1;
	const BET_TYPE_X = 2;
	const BET_TYPE_2 = 3;
	const BET_TYPE_1X = 4;
	const BET_TYPE_12 = 5;
	const BET_TYPE_2X = 6;
	const BET_TYPE_FORA1 = 7;
	const BET_TYPE_FORA2 = 8;
	const BET_TYPE_B = 9;
	const BET_TYPE_M = 10;
	const BET_TYPE_ADDITIONAL = 11;

	const RESULT_END = 'итог';

	public static function getAllBetTypes()
	{
		return array(
				self::BET_TYPE_1,
				self::BET_TYPE_X,
				self::BET_TYPE_2,
				self::BET_TYPE_1X,
				self::BET_TYPE_12,
				self::BET_TYPE_2X,
				self::BET_TYPE_FORA1,
				self::BET_TYPE_FORA2,
				self::BET_TYPE_B,
				self::BET_TYPE_M,
				self::BET_TYPE_ADDITIONAL,
		);
	}

	public static function stringifyBet($bet, $type, $result = '')
	{
		$type = (int) $type;
		if (!is_object($bet) || !in_array($type, self::getAllBetTypes(), true))
		{
			return '';
		}

		$stringifyBet = '';

		switch($type)
		{
			case self::BET_TYPE_1:
				$stringifyBet = JText::sprintf('COM_BETS_BET_TYPE_1', '<b>' . $bet->p1 . '</b>');
				break;

			case self::BET_TYPE_X:
				$stringifyBet = JText::sprintf('COM_BETS_BET_TYPE_X', '<b>' . $bet->X . '</b>');
				break;

			case self::BET_TYPE_2:
				$stringifyBet = JText::sprintf('COM_BETS_BET_TYPE_2', '<b>' . $bet->p2 . '</b>');
				break;

			case self::BET_TYPE_1X:
				$stringifyBet = JText::sprintf('COM_BETS_BET_TYPE_1X', '<b>' . $bet->X1 . '</b>');
				break;

			case self::BET_TYPE_12:
				$stringifyBet = JText::sprintf('COM_BETS_BET_TYPE_12', '<b>' . $bet->p12 . '</b>');
				break;

			case self::BET_TYPE_2X:
				$stringifyBet = JText::sprintf('COM_BETS_BET_TYPE_2X', '<b>' . $bet->X2 . '</b>');
				break;

			case self::BET_TYPE_FORA1:
				$stringifyBet = JText::sprintf('COM_BETS_BET_TYPE_FORA1', $bet->fora, '<b>' . $bet->s_1 . '</b>');
				break;

			case self::BET_TYPE_FORA2:
				$stringifyBet = JText::sprintf('COM_BETS_BET_TYPE_FORA2', $bet->fora_s, '<b>' . $bet->s_2 . '</b>');
				break;

			case self::BET_TYPE_B:
				$stringifyBet = JText::sprintf('COM_BETS_BET_TYPE_B', $bet->total, '<b>' . $bet->B . '</b>');
				break;

			case self::BET_TYPE_M:
				$stringifyBet = JText::sprintf('COM_BETS_BET_TYPE_B', $bet->total, '<b>' . $bet->M . '</b>');
				break;

			case self::BET_TYPE_ADDITIONAL:
				$data = self::getAdditionalResultData($bet->id, $result);
				if (is_array($data))
				{
					$stringifyBet = JText::sprintf('COM_BETS_BET_TYPE_CUSTOM', $data['title'], '<b>' . $data['coef'] . '</b>');
				}
				else
				{
					$stringifyBet = $data;
				}
				break;
		}

		return $stringifyBet;
	}

	public static function getEventCoef($bet, $type, $result = '')
	{
		$type = (int) $type;
		if (!is_object($bet) || !in_array($type, self::getAllBetTypes(), true))
		{
			return 0;
		}

		$coef = 0;

		switch($type)
		{
			case self::BET_TYPE_1:
				$coef = $bet->p1;
				break;

			case self::BET_TYPE_X:
				$coef = $bet->X;
				break;

			case self::BET_TYPE_2:
				$coef = $bet->p2;
				break;

			case self::BET_TYPE_1X:
				$coef = $bet->X1;
				break;

			case self::BET_TYPE_12:
				$coef = $bet->p12;
				break;

			case self::BET_TYPE_2X:
				$coef = $bet->X2;
				break;

			case self::BET_TYPE_FORA1:
				$coef = $bet->s_1;
				break;

			case self::BET_TYPE_FORA2:
				$coef = $bet->s_2;
				break;

			case self::BET_TYPE_B:
				$coef = $bet->B;
				break;

			case self::BET_TYPE_M:
				$coef = $bet->M;
				break;

			case self::BET_TYPE_ADDITIONAL:
				$data = self::getAdditionalResultData($bet->id, $result);
				if (is_array($data))
				{
					$coef = $data['coef'];
				}
				else
				{
					$coef = 0;
				}
				break;
		}

		return $coef;
	}

	public static function getAdditionalResultData($eventId, $result)
	{
		$eventId = (int) $eventId;
		$result = explode('.', $result);

		$strResult = array();

		if ($eventId < 1 || count($result) < 3)
		{
			return '-';
		}

		$adtResults = LineHelper::getAdditionalResultsByEvent($eventId);

		if (!is_array($adtResults) || !isset($adtResults[$result[0]]))
		{
			return '-';
		}

		$adtResults = $adtResults[$result[0]];

		if (!empty($adtResults->tablename))
		{
			$strResult[] = $adtResults->tablename;
		}

		if (!isset($adtResults->data) || !is_array($adtResults->data) || !isset($adtResults->data[$result[1]]))
		{
			return '-';
		}

		$keys = array_keys($adtResults->data[0]);
		$titles = array_keys($adtResults->data[$result[1]]);
		$values = array_values($adtResults->data[$result[1]]);
		if (!isset($keys[$result[2]]) || !isset($titles[$result[2]]))
		{
			return '-';
		}

		$key = '';
		$keyActual = LineHelper::getAdditionalResultTitle($titles[$result[2]]);

		$keyPrev = isset($keys[$result[2]-1]) ? LineHelper::getAdditionalResultTitle($keys[$result[2]-1]) : '';
		if (preg_match('/тотал|total|фора|hand/ui', $keyPrev))
		{
			if (preg_match('/^б|м|o|u$/ui', $keyActual) || is_numeric($keyActual))
			{
				$key .= $keyPrev . ' ' . $keyActual . ' (' . $values[$result[2]-1] . ')';
			}
			else
			{
				$key .= $keyPrev . ', ' . $values[$result[2]-1] . ' (' . $keyActual . ')';
			}
		}
		else
		{
			$keyPrev = isset($keys[$result[2]-2]) ? LineHelper::getAdditionalResultTitle($keys[$result[2]-2]) : '';
			if (preg_match('/тотал|total|фора|hand/ui', $keyPrev))
			{
				if (preg_match('/^б|м|o|u$/ui', $keyActual) || is_numeric($keyActual))
				{
					$key .= $keyPrev . ' ' . $keyActual . ' (' . $values[$result[2]-2] . ')';
				}
				else
				{
					$key .= $keyPrev . ', ' . $values[$result[2]-2] . ' (' . $keyActual . ')';
				}
			}
		}

		if ($key == '')
		{
			$rowTitle = LineHelper::getAdditionalResultTitle($titles[0]);
			if (!is_numeric($rowTitle) && !preg_match('/матч|тайм|сет|счет|score|HT|Halves|FT/ui', $adtResults->tablename))
			{
				$key = $rowTitle;
			}

			$valuePrev = isset($values[$result[2]-1]) ? $values[$result[2]-1] : '';
			$value0 = isset($values[0]) ? $values[0] : '';
			if ($valuePrev == '')
			{
				$valuePrev = $value0;
			}
			elseif ($value0 != $valuePrev)
			{
				$tmp = $valuePrev;
				$valuePrev = '';
				if (!is_numeric($tmp))
				{
					$valuePrev .= ' ' . $value0 . ', ' . $tmp;
				}
				else
				{
					$valuePrev = $value0;
				}
			}

			if (!empty($valuePrev) && !is_numeric($valuePrev))
			{
				$key .= ', ' . $valuePrev . ' (' . $keyActual . ')';
			}
			else
			{
				$key .= ' (' . $keyActual . ')';
			}
		}

		$strResult[] = trim($key);

		return array(
			'title' => implode('; ', $strResult),
			'coef' => $values[$result[2]]
		);
	}

	public static function getBetById($betId, $full = false)
	{
		$betId = (int) $betId;

		if ($full)
		{
			$db = JFactory::getDbo();

			$query = $db->getQuery(true);
			$query
					->select('b.*, b.1 as p1, b.2 as p2, b.12 as p12, b.1X as X1, b.1_s as s_1, b.1_s as s_2, ub.id as bet_id, ub.uid as bet_uid, ub.created as bet_created, ub.type as bet_type, ub.result as bet_result, ub.bet_sum as bet_sum, ub.title as bet_title, ub.coef as bet_coef, ub.processed as bet_processed, ub.win as bet_win, b2.title as parent_title, s.title as section_title, r.id as result_id, r.status as result_status, r.result as result_result, u.username')
					->from('users_bets ub');

			$query->join('LEFT', 'bets b ON (ub.event = b.id)');
			$query->join('LEFT', 'bets b2 ON (b2.id = b.parent)');
			$query->join('LEFT', 'bets_sections s ON (b.sectionid = s.sectionid)');
			$query->join('LEFT', 'results r ON (b.id = r.parser_betid)');
			$query->join('LEFT', '#__users u ON (ub.uid = u.id)');

			$query->where('ub.id = ' . $betId);

			return $db->setQuery($query)->loadObject();
		}
		else
		{
			return JFactory::getDbo()
					->setQuery("SELECT * FROM users_bets WHERE id = " . $betId)
					->loadObject();
		}
	}

	public static function getUserBets($uid, $status = null)
	{
		$db = JFactory::getDbo();

		$uid = (int) $uid;

		$query = $db->getQuery(true);

		$query
			->select('b.*, b.1 as p1, b.2 as p2, b.12 as p12, b.1X as X1, b.1_s as s_1, b.1_s as s_2, ub.id as bet_id, ub.created as bet_created, ub.type as bet_type, ub.bet_sum as bet_sum, ub.title as bet_title, ub.coef as bet_coef, b2.title as parent_title, s.title as section_title, r.status as result_status, r.result as result_result')
			->from('users_bets ub');

		$query->join('LEFT', 'bets b ON (ub.event = b.id)');
		$query->join('LEFT', 'bets b2 ON (b2.id = b.parent)');
		$query->join('LEFT', 'bets_sections s ON (b.sectionid = s.sectionid)');
		$query->join('LEFT', 'results r ON (b.id = r.parser_betid)');

		if ($status == ResultsHelper::STATUS_END)
		{
			$query->where('r.status = \'' . ResultsHelper::RESULT_END . '\'');
		}
		else
		{
			$query->where('(r.status <> \'' . ResultsHelper::RESULT_END . '\' OR r.status IS NULL)');
		}

		$query->where('ub.uid = ' . $uid);
		$query->order('ub.id DESC');

		$bets = $db->setQuery($query)->loadObjectList();
		return $bets;
	}

	public static function makeBet($eventId, $type, $bet_sum, $result = '', $uid = 0)
	{
		if ($uid == 0)
		{
			$user = JFactory::getUser();
			$uid = $user->id;
		}

		$event = LineHelper::getEventById($eventId);
		if (!$event)
		{
			return false;
		}

		if (!in_array($type, self::getAllBetTypes()))
		{
			return false;
		}

		if (!$uid)
		{
			return false;
		}

		$bet = new stdClass();
		$bet->event = $event->id;
		$bet->type = $type;
		$bet->uid = $uid;
		$bet->bet_sum = $bet_sum;
		$bet->created = date('Y-m-d H:i:s');
		$bet->processed = 0;
		$bet->result = $result;
		$bet->title = strip_tags(self::stringifyBet($event, $type, $result));
		$bet->coef = floatval(self::getEventCoef($event, $type, $result));
		$bet->event_date = $event->datetime;
		$bet->win = 0;

		if (JFactory::getDbo()->insertObject('users_bets', $bet))
		{
			$bet->id = JFactory::getDbo()->insertid();

			$ua = new UserAccount();
			$pid = date('His') . $uid;
			$betTitle = JText::sprintf('COM_BETS_BET_CREATED_BILLING_NOTIFY',
					'#' . $bet->id . ': ' . $bet->event_date . ', ' . LineHelper::getEventTitle($event) . ', ' . $bet->title);
			$ua->WithdrawMoney($uid, $bet_sum, $pid, $betTitle);

			return $bet;
		}

		return null;
	}

	public static function getWin($bet, $checkResult = true)
	{
		if (!is_object($bet))
		{
			return false;
		}

		if ($checkResult && $bet->result_status != ResultsHelper::RESULT_END)
		{
			return false;
		}

		$resultRaw = trim($bet->result_result);
		if (empty($resultRaw))
		{
			return false;
		}

		if (!preg_match('/^([\d\.\,]+):([\d\.\,]+)/', $resultRaw, $m))
		{
			return false;
		}

		$firstTeamScore = floatval(str_replace(',', '.', $m[1]));
		$secondTeamScore = floatval(str_replace(',', '.', $m[2]));

		$win = false;

		switch($bet->bet_type)
		{
			case self::BET_TYPE_1:
				if ($firstTeamScore > $secondTeamScore)
				{
					$win = $bet->p1 * $bet->bet_sum;
				}
				break;

			case self::BET_TYPE_X:
				if ($firstTeamScore == $secondTeamScore)
				{
					$win = $bet->X * $bet->bet_sum;
				}
				break;

			case self::BET_TYPE_2:
				if ($firstTeamScore < $secondTeamScore)
				{
					$win = $bet->p2 * $bet->bet_sum;
				}
				break;

			case self::BET_TYPE_1X:
				if ($firstTeamScore >= $secondTeamScore)
				{
					$win = $bet->X1 * $bet->bet_sum;
				}
				break;

			case self::BET_TYPE_12:
				if ($firstTeamScore != $secondTeamScore)
				{
					$win = $bet->p12 * $bet->bet_sum;
				}
				break;

			case self::BET_TYPE_2X:
				if ($firstTeamScore <= $secondTeamScore)
				{
					$win = $bet->X2 * $bet->bet_sum;
				}
				break;

			case self::BET_TYPE_FORA1:
				if ($firstTeamScore + floatval($bet->fora) > $secondTeamScore)
				{
					$win = floatval($bet->s_1) * $bet->bet_sum;
				}
				break;

			case self::BET_TYPE_FORA2:
				if ($firstTeamScore < $secondTeamScore + floatval($bet->fora_s))
				{
					$win = floatval($bet->s_2) * $bet->bet_sum;
				}
				break;

			case self::BET_TYPE_B:
				if ($firstTeamScore + $secondTeamScore > floatval($bet->total))
				{
					$win = floatval($bet->B) * $bet->bet_sum;
				}
				break;

			case self::BET_TYPE_M:
				if ($firstTeamScore + $secondTeamScore < floatval($bet->total))
				{
					$win = floatval($bet->M) * $bet->bet_sum;
				}
				break;
		}

		return $win;
	}

	public static function stringifyWin($bet)
	{
		$win = self::getWin($bet);
		if ($win)
		{
			return '<span class="win">' . FormatDefaultCurrency($win) . '</span>';
		}

		return '-';
	}

	public static function getUnprocessedBets($all = false)
	{
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query
				->select('b.*, b.1 as p1, b.2 as p2, b.12 as p12, b.1X as X1, b.1_s as s_1, b.1_s as s_2, ub.id as bet_id, ub.uid as bet_uid, ub.created as bet_created, ub.type as bet_type, ub.bet_sum as bet_sum, ub.title as bet_title, ub.coef as bet_coef, b2.title as parent_title, s.title as section_title, r.status as result_status, r.result as result_result')
				->from('users_bets ub');

		$query->join('LEFT', 'bets b ON (ub.event = b.id)');
		$query->join('LEFT', 'bets b2 ON (b2.id = b.parent)');
		$query->join('LEFT', 'bets_sections s ON (b.sectionid = s.sectionid)');
		$query->join('LEFT', 'results r ON (b.id = r.parser_betid)');

		$query->where('r.status = \'' . ResultsHelper::RESULT_END . '\'');
		$query->where('ub.processed = 0');
		$query->where('ub.type <> ' . BetHelper::BET_TYPE_ADDITIONAL);

		$bets = $db->setQuery($query)->loadObjectList();
		return $bets;
	}
}