<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Bets Component Line Helper
 *
 * @package     Bets
 * @subpackage  Helpers
 * @since
 */
abstract class LineHelper
{
	const LINE_CACHE_TIME = 300; // in seconds

	public static function getLine($startDate = null, $endDate = null)
	{
		$cacheTime = JFactory::getUser()->id == 0 ? self::LINE_CACHE_TIME : 0;

		$cacheFileName = JPATH_ROOT . '/cache/line.cache';
		if ($cacheTime > 0 && is_file($cacheFileName) && filemtime($cacheFileName) > time() - $cacheTime)
		{
			return unserialize(file_get_contents($cacheFileName));
		}

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('b.*, b.1 as p1, b.2 as p2, b.12 as p12, b.1X as X1, b.1_s as s_1, b.1_s as s_2');
		$query->from('bets b');

		$query->select('bs.title as section_title');
		$query->join('LEFT', 'bets_sections bs ON (b.sectionid=bs.sectionid)');

		$child = "(SELECT COUNT(*) FROM bets b2 WHERE b2.parent=b.id AND (b2.1 <> '' OR b2.X <> '' OR b2.2 <> '' OR b2.1X <> '' OR b2.12 <> '' OR b2.X2 <> '' OR b2.1_s <> '' OR b2.2_s <> '' OR b2.B <> '' OR b2.M <> ''))";
		$query->select($child . ' as child');

		$adtresults = "(SELECT COUNT(*) FROM bets_details bd WHERE bd.fonbetid=b.fonbetid AND b.fonbetid > 0)";
		$query->select($adtresults . ' as adtresults');

		$query->where('b.parent = 0');

		if (!$startDate)
		{
			$startDate = date('Y-m-d H:i:00');
			$endDate = null;
		}
		$query->where('STR_TO_DATE(`b`.`datetime`, \'%d.%m.%Y %H:%i\') >= \'' . $query->escape($startDate) . '\'');

		if ($endDate)
		{
			$endDate = DateTime::createFromFormat('Y-m-d', $endDate);
			$interval = new DateInterval('P1D');
			$endDate->add($interval);

			$query->where('STR_TO_DATE(`b`.`datetime`, \'%d.%m.%Y %H:%i\') < \'' . $endDate->format('Y-m-d') . '\'');
		}

		$query->order('STR_TO_DATE(`b`.`datetime`, \'%d.%m.%Y %H:%i\') ASC, b.id ASC');

		$query->where("(b.1 <> '' OR b.X <> '' OR b.2 <> '' OR b.1X <> '' OR b.12 <> '' OR b.X2 <> '' OR b.1_s <> '' OR b.2_s <> '' OR b.B <> '' OR b.M <> '' OR $child > 0 OR $adtresults > 0)");

		$line = array();
		foreach($db->setQuery($query)->loadObjectList() as $row)
		{
			if (!isset($line[$row->sectionid]))
			{
				$line[$row->sectionid] = array();
			}

			$line[$row->sectionid][] = $row;
		}

		file_put_contents($cacheFileName, serialize($line));

		return $line;
	}

	public static function getEventById($id)
	{
		$id = (int) $id;
		$bet = JFactory::getDbo()->setQuery("SELECT b.*, b.1 as p1, b.2 as p2, b.12 as p12, b.1X as X1, b.1_s as s_1, b.1_s as s_2, b2.title as parent_title, s.title as section_title FROM bets b LEFT JOIN bets b2 ON (b.parent = b2.id) LEFT JOIN bets_sections s ON (b.sectionid = s.sectionid) WHERE b.id=" . $id)->loadObject();
		if ($bet)
		{
			// rawdate
			$bet->rawDate = new DateTime($bet->datetime);

			// status
			$now = new DateTime();
			$bet->status = $bet->rawDate > $now ? ResultsHelper::STATUS_NOT_START : ResultsHelper::STATUS_CURRENT;
		}

		return $bet;
	}

	public static function getEventTitle($event)
	{
		if (!is_object($event)) return $event;

		$title = '';

		if ($event->parent_title != '')
		{
			$title = $event->parent_title . ', ';
		}

		$title .= $event->title;

		return $title;
	}

	public static function getLineByParent($parent)
	{
		$parent = (int) $parent;
		if ($parent < 1) return array();

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('b.*, b.1 as p1, b.2 as p2, b.12 as p12, b.1X as X1, b.1_s as s_1, b.1_s as s_2');
		$query->from('bets b');

		$query->select('bs.title as section_title');
		$query->join('LEFT', 'bets_sections bs ON (b.sectionid=bs.sectionid)');

		$query->where('b.parent = ' . $parent);

		$query->where("(b.1 <> '' OR b.X <> '' OR b.2 <> '' OR b.1X <> '' OR b.12 <> '' OR b.X2 <> '' OR b.1_s <> '' OR b.2_s <> '' OR b.B <> '' OR b.M <> '')");

		$query->order('b.id ASC');

		return $db->setQuery($query)->loadObjectList();
	}

	public static function getAdditionalResultsByEvent($eventId)
	{
		$eventId = (int) $eventId;
		if ($eventId < 1) return array();

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('bd.*');
		$query->from('bets b');
		$query->join('LEFT', 'bets_details bd ON (bd.fonbetid=b.fonbetid)');
		$query->where('b.id = ' . $eventId);

		$adtResults = $db->setQuery($query)->loadObjectList();

		foreach ($adtResults as $i=>$result)
		{
			$jsons = explode('}{', $result->json);
			foreach ($jsons as $x=>$json)
			{
				if ($x > 0)
				{
					$jsons[$x] = '{' . $jsons[$x];
				}

				if ($x < count($jsons) - 1)
				{
					$jsons[$x] .= '}';
				}

				$jsons[$x] = self::normalizeAdditionalResultsData($jsons[$x]);
			}
			$result->data = $jsons;
		}

		return $adtResults;
	}

	public static function normalizeAdditionalResultsData($json)
	{
		$data = array();
		if (preg_match_all('/\"(.*)\"/Uis', $json, $m))
		{
			$key = '';
			foreach ($m[1] as $i=>$v)
			{
				$v = trim($v);

				if ($i % 2 == 0)
				{
					$key = $v . '|' . $i;
				}
				else
				{
					$data[$key] = $v;
				}
			}
		}
		return $data;
	}

	public static function getAdditionalResultTitle($title)
	{
		$title = explode('|', $title);
		return $title[0];
	}

	public static function getSectionColor($sectionTitle)
	{
		$titles = array(
				'Футбол',
				'Баскетбол',
				'Волейбол',
				'Гандбол',
				'Футзал',
				'Регби',
				'Бейсбол',
				'Теннис',
				'Наст. теннис',
				'Дартс',
				'Смешанные единоборства',
				'Жен. Смешанные единоборства',
				'Бокс',
				'Киберспорт',
				'Шахматы',
		);

		$maxColorId = 6;
		foreach ($titles as $i=>$title)
		{
			if (preg_match('/^'.preg_quote($title).'/Uis', $sectionTitle))
			{
				return $i % $maxColorId + 1;
			}
		}

		return 1;
	}

	public static function formatDate($date)
	{
		$ru_months = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');

		try
		{
			$date = new DateTime($date);
		}
		catch (\Exception $e)
		{
			return $date;
		}

		return $date->format('d') . ' ' . $ru_months[(int)$date->format('m') - 1] . ', ' . $date->format('H:i');
	}

	public static function betLink($eventId, $betType, $coef, $betTitle = '', $path = '')
	{
		$user = JFactory::getUser();
		if (!$user->id)
		{
			return htmlspecialchars_decode($coef);
		}

		$eventId = (int) $eventId;
		$betType = (int) $betType;

		if ($betType == BetHelper::BET_TYPE_ADDITIONAL)
		{
			if (!is_numeric($coef) || preg_match('/тотал|total|фора|hand/ui', $betTitle))
			{
				return htmlspecialchars_decode($coef);
			}
			else
			{
				return '<a href="' . JRoute::_('index.php?option=com_bets&view=makebet&event=' . $eventId . '&type=' . $betType . '&result=' . $path, false) . '" class="makebet">' . htmlspecialchars($coef) . '</a>';
			}
		}
		else
		{
			return '<a href="' . JRoute::_('index.php?option=com_bets&view=makebet&event=' . $eventId . '&type=' . $betType, false) . '" class="makebet">' . htmlspecialchars($coef) . '</a>';
		}
	}

	public static function stringifyEventStatus($event)
	{
		$interval = new DateInterval('PT2H30M');
		$eventDate = new DateTime($event->datetime);

		$currentDate = new DateTime();

		if ($eventDate > $currentDate)
		{
			return '<span class="label label-default" style="background: #777;">' . JText::_('COM_BETS_STATUS_NOT_START') . '</span>';
		}
		elseif ($currentDate > $eventDate->add($interval))
		{
			return '<span class="label label-warning">' . JText::_('COM_BETS_STATUS_PROBABLY_END') . '</span>';
		}
		else
		{
			return '<span class="label label-default">' . JText::_('COM_BETS_STATUS_UNKNOWN') . '</span>';
		}
	}
}
