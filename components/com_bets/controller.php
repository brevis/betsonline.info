<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

require_once JPATH_ROOT . '/components/com_bets/helpers/line.php';
require_once JPATH_ROOT . '/components/com_bets/helpers/results.php';
require_once JPATH_ROOT . '/components/com_bets/helpers/bet.php';

class BetsController extends JControllerLegacy
{
	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $default_view = 'line';

}
