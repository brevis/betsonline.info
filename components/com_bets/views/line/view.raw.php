<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Line view class.
 *
 * @package     Bets
 * @subpackage  Views
 */
class BetsViewLine extends JViewLegacy
{
	protected $line;

	public function display($tpl = null)
	{
		$app = JFactory::getApplication();

		$layout = $app->input->getCmd('layout');

		switch ($layout)
		{
			case 'adtbets':
				$this->bet = $app->input->getInt('bet');
				$this->line = LineHelper::getLineByParent($this->bet);
				break;

			case 'adtresults':
				$this->bet = $app->input->getInt('bet');
				$this->results = LineHelper::getAdditionalResultsByEvent($this->bet);
				break;
		}

		parent::display($tpl);
	}
}
