<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");

?>
<?php if (is_array($this->results) && count($this->results) > 0) : ?>
<tr id="collapse_info_grid_row_<?php echo $this->bet; ?>" class="collapse grid_row_details">
	<td colspan="16" class="row_details_wrapper">
		<?php foreach ($this->results as $i=>$result) : ?>
		<table class="table table-bordered table-condensed">
			<thead>
			<tr>
				<th colspan="999"><?php echo $result->tablename; ?>&nbsp;</th>
			</tr>
			<?php foreach ($result->data as $d=>$data) : ?>
				<?php if ($d == 0) : ?>
					<tr>
						<?php foreach ($data as $k=>$v) : ?>
							<th><?php echo htmlspecialchars_decode(LineHelper::getAdditionalResultTitle($k)); ?></th>
						<?php endforeach; ?>
					</tr>
					</thead>
					<tbody>
				<?php endif; ?>
				<tr>
					<?php
					$n = 0; $keys = array_keys($result->data[0]);
					foreach ($data as $k=>$v) : ?>
						<td><?php echo LineHelper::betLink($this->bet, BetHelper::BET_TYPE_ADDITIONAL, $v, $keys[$n], $i.'.'.$d.'.'.$n); ?></td>
					<?php
					$n++;
					endforeach; ?>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<?php endforeach; ?>
	</td>
</tr>
<?php endif; ?>
