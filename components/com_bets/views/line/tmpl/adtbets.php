<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");

?>
<?php if (is_array($this->line) && count($this->line) > 0) : ?>
<tr id="collapse_grid_row_<?php echo $this->bet; ?>" class="collapse in level_1">
	<td colspan="16">
		<table class="table table-condensed">
			<tbody>
			<?php foreach($this->line as $bet) : ?>
			<tr class="level_0">
				<td class="td_1"></td>
				<td class="td_2"></td>
				<td class="td_3"><?php echo $this->escape($bet->title); ?></td>
				<td class="td_4"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_1, $bet->p1); ?></td>
				<td class="td_5"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_X, $bet->X); ?></td>
				<td class="td_6"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_2, $bet->p2); ?></td>
				<td class="td_7"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_1X, $bet->X1); ?></td>
				<td class="td_8"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_12, $bet->p12); ?></td>
				<td class="td_9"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_2X, $bet->X2); ?></td>
				<td class="td_10"><?php echo $this->escape($bet->fora); ?></td>
				<td class="td_11"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_FORA1, $bet->s_1); ?></td>
				<td class="td_12"><?php echo $this->escape($bet->fora_s); ?></td>
				<td class="td_13"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_FORA2, $bet->s_2); ?></td>
				<td class="td_14"><?php echo $this->escape($bet->total); ?></td>
				<td class="td_15"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_B, $bet->B); ?></td>
				<td class="td_16"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_M, $bet->M); ?></td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</td>
</tr>
<?php endif; ?>