<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

jimport( 'joomla.html.html' );
JHtml::_('jquery.framework');
$document = JFactory::getDocument();
$document->addStyleSheet(Juri::base() .'media/com_bets/css/jquery-ui/jquery-ui.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/jquery-ui/jquery-ui.structure.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/jquery-ui/jquery-ui.theme.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/bootstrap/bootstrap.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/bootstrap/bootstrap-theme.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/font-awesome.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/bootstrap/bootstrap-datetimepicker.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/bets.css?v1');
$document->addScript(Juri::base() .'media/com_bets/js/jquery-ui/jquery-ui.min.js');
$document->addScript(Juri::base() .'media/com_bets/js/bootstrap/bootstrap.min.js');
$document->addScript(Juri::base() .'media/com_bets/js/moment/moment.js');
$document->addScript(Juri::base() .'media/com_bets/js/moment/ru.js');
$document->addScript(Juri::base() .'media/com_bets/js/bootstrap/bootstrap-datetimepicker.min.js');
$document->addScript(Juri::base() .'media/com_bets/js/1vtemplate.js');
$document->addScript(Juri::base() .'media/com_bets/js/bets.js?v4');

$user = JFactory::getUser();
?>
<h2><?php echo JText::_('COM_BETS_LINE'); ?></h2>

<?php if (is_array($this->line) && count($this->line) > 0) : ?>
<div class="tables_grid">
	<div id="common_tables_grid_thead" class="table-responsive">
		<table class="bckgrnd_mainc_1 table table-bordered table-condensed">
			<thead>
			<tr class="common_tables_grid_thead">
				<th class="th_1"></th>
				<th class="th_2"></th>
				<th class="th_3"><?php echo JText::_('COM_BETS_LINE_EVENT'); ?></th>
				<th class="th_4">1</th>
				<th class="th_5">X</th>
				<th class="th_6">2</th>
				<th class="th_7">1X</th>
				<th class="th_8">12</th>
				<th class="th_9">X2</th>
				<th class="th_10"><?php echo JText::_('COM_BETS_LINE_FORA'); ?></th>
				<th class="th_11">1</th>
				<th class="th_12"><?php echo JText::_('COM_BETS_LINE_FORA'); ?></th>
				<th class="th_13">2</th>
				<th class="th_14"><?php echo JText::_('COM_BETS_LINE_TOTAL'); ?></th>
				<th class="th_15"><?php echo JText::_('COM_BETS_LINE_B'); ?></th>
				<th class="th_16"><?php echo JText::_('COM_BETS_LINE_M'); ?></th>
			</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<?php foreach($this->line as $sid=>$section) : ?>
	<div class="table-responsive">
		<table class="bckgrnd_mainc_<?php echo LineHelper::getSectionColor($section[0]->section_title); ?> table table-bordered table-condensed">
			<thead>
			<tr id="grid_row_1">
				<th colspan="16"><span class="tables_grid_header"><?php echo $this->escape($section[0]->section_title); ?></span></th>
			</tr>
			<tr id="grid_row_2" class="tables_grid_thead">
				<th class="th_1"></th>
				<th class="th_2"></th>
				<th class="th_3"><?php echo JText::_('COM_BETS_LINE_EVENT'); ?></th>
				<th class="th_4">1</th>
				<th class="th_5">X</th>
				<th class="th_6">2</th>
				<th class="th_7">1X</th>
				<th class="th_8">12</th>
				<th class="th_9">X2</th>
				<th class="th_10"><?php echo JText::_('COM_BETS_LINE_FORA'); ?></th>
				<th class="th_11">1</th>
				<th class="th_12"><?php echo JText::_('COM_BETS_LINE_FORA'); ?></th>
				<th class="th_13">2</th>
				<th class="th_14"><?php echo JText::_('COM_BETS_LINE_TOTAL'); ?></th>
				<th class="th_15"><?php echo JText::_('COM_BETS_LINE_B'); ?></th>
				<th class="th_16"><?php echo JText::_('COM_BETS_LINE_M'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($section as $i=>$bet) : ?>
			<tr id="grid_row_<?php echo $bet->id; ?>">
				<td class="td_1">
					<?php if ((int)$bet->child > 0) : ?>
						<a role="button" data-toggle="collapse" href="#collapse_grid_row_<?php echo $bet->id; ?>" aria-expanded="false" aria-controls="collapse_grid_row_<?php echo $bet->id; ?>" class="toggleadtbet" data-bet="<?php echo $bet->id; ?>">
							<span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
						</a>
					<?php endif; ?>
				</td>
				<td class="td_2">
					<a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
				</td>
				<td class="td_3">
					<!--<?php echo $bet->id; ?>-->
					<?php echo $bet->title; ?>

					<?php if ((int)$bet->adtresults > 0) : ?>
						<a role="button" data-toggle="collapse" href="#collapse_info_grid_row_<?php echo $bet->id; ?>" aria-expanded="false" aria-controls="collapse_info_grid_row_<?php echo $bet->id; ?>" class="collapsed adtresults" data-bet="<?php echo $bet->id; ?>">
							<span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
						</a>
					<?php endif; ?>

					<div class="item_info_wrapper"><span class="label label-info"><?php echo LineHelper::formatDate($bet->datetime); ?></span></div>
				</td>
				<td class="td_4"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_1, $bet->p1); ?></td>
				<td class="td_5"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_X, $bet->X); ?></td>
				<td class="td_6"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_2, $bet->p2); ?></td>
				<td class="td_7"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_1X, $bet->X1); ?></td>
				<td class="td_8"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_12, $bet->p12); ?></td>
				<td class="td_9"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_2X, $bet->X2); ?></td>
				<td class="td_10"><?php echo $this->escape($bet->fora); ?></td>
				<td class="td_11"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_FORA1, $bet->s_1); ?></td>
				<td class="td_12"><?php echo $this->escape($bet->fora_s); ?></td>
				<td class="td_13"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_FORA2, $bet->s_2); ?></td>
				<td class="td_14"><?php echo $this->escape($bet->total); ?></td>
				<td class="td_15"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_B, $bet->B); ?></td>
				<td class="td_16"><?php echo LineHelper::betLink($bet->id, BetHelper::BET_TYPE_M, $bet->M); ?></td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php endforeach; ?>
</div>
<?php else: ?>
	<p><?php echo JText::_('COM_BETS_BETS_NOT_FOUND'); ?></p>
<?php endif; ?>
<script>
	var Bets = {
		adtbetsUrl: '<?php echo JRoute::_('index.php?option=com_bets&view=line&layout=adtbets&format=raw', false); ?>',
		adtresultsUrl: '<?php echo JRoute::_('index.php?option=com_bets&view=line&layout=adtresults&format=raw', false); ?>'
	};
</script>