<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Line view class.
 *
 * @package     Bets
 * @subpackage  Views
 */
class BetsViewLine extends JViewLegacy
{
	protected $line;

	public function display($tpl = null)
	{
		$app = JFactory::getApplication();

		$this->line = LineHelper::getLine($app->input->getString('date'));

		parent::display($tpl);
	}
}
