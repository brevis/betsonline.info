<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Results view class.
 *
 * @package     Bets
 * @subpackage  Views
 */
class BetsViewResults extends JViewLegacy
{
	protected $sections;

	protected $selectedSections;

	public function display($tpl = null)
	{
		$app = JFactory::getApplication();

		$layout = $app->input->getCmd('layout');

		// getsections
		if ($layout == 'getsections')
		{
			$this->sections = ResultsHelper::getSectionsByType($app->input->getString('type'));

			$this->selectedSections = explode(',', $app->input->getString('selectedSections'));
			$this->selectedSections = array_filter($this->selectedSections, function($item){
				return is_numeric($item);
			});
		}

		parent::display($tpl);
	}

}
