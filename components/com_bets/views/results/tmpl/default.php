<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

jimport( 'joomla.html.html' );
JHtml::_('jquery.framework');
$document = JFactory::getDocument();
$document->addStyleSheet(Juri::base() .'media/com_bets/css/jquery-ui/jquery-ui.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/jquery-ui/jquery-ui.structure.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/jquery-ui/jquery-ui.theme.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/bootstrap/bootstrap.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/bootstrap/bootstrap-theme.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/font-awesome.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/bootstrap/bootstrap-datetimepicker.min.css');
$document->addStyleSheet(Juri::base() .'media/com_bets/css/bets.css');
$document->addScript(Juri::base() .'media/com_bets/js/jquery-ui/jquery-ui.min.js');
$document->addScript(Juri::base() .'media/com_bets/js/bootstrap/bootstrap.min.js');
$document->addScript(Juri::base() .'media/com_bets/js/moment/moment.js');
$document->addScript(Juri::base() .'media/com_bets/js/moment/ru.js');
$document->addScript(Juri::base() .'media/com_bets/js/bootstrap/bootstrap-datetimepicker.min.js');
$document->addScript(Juri::base() .'media/com_bets/js/1vtemplate.js');
$document->addScript(Juri::base() .'media/com_bets/js/bets.js');
?>
<h2><?php echo JText::_('COM_BETS_RESULTS'); ?></h2>

<form action="<?php echo JRoute::_('index.php'); ?>" method="get" class="margin_b20" id="resultsFilterForm">
	<input type="hidden" name="option" value="com_bets">
	<input type="hidden" name="view" value="results">
	<input type="hidden" id="hdnDate" name="filter[date]" value="">
	<input type="hidden" id="hdnType" name="filter[type]" value="">
	<input type="hidden" id="hdnSection" name="filter[section]" value="<?php echo implode(',', $this->filter['section']); ?>">
	<input type="hidden" id="hdnStatus" name="filter[status]" value="">
	<div class="row">
		<div class="col-sm-4">
			<div style="overflow:hidden;">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div id="datetimepicker"></div>
						</div>
					</div>
				</div>
				<script type="text/javascript">
					jQuery(function () {
						jQuery('#datetimepicker').datetimepicker({
							format: 'YYYY-MM-DD',
							inline: true,
							sideBySide: false,
							defaultDate: '<?php echo addslashes($this->filter['date']); ?>'
						});
					});
				</script>
			</div>
		</div>
		<div class="col-sm-8">
			<div class="row">
				<div class="col-sm-4 form-group">
					<label for="type">Виды спорта</label>
					<select id="type" multiple class="form-control">
						<?php foreach(ResultsHelper::getAllTypes() as $id=>$type) : ?>
						<option value="<?php echo $id; ?>"<?php if (in_array($id, $this->filter['type'])) : ?> selected="selected"<?php endif; ?>><?php echo $this->escape($type); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col-sm-8 form-group">
					<label for="section">Лиги</label>
					<select id="section" multiple class="form-control"></select>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<span class="help-block">Отображать:</span>
						<label for="status_1" class="checkbox-inline">
							<input type="checkbox" id="status_1" value="<?php echo ResultsHelper::STATUS_NOT_START; ?>"<?php if (in_array(ResultsHelper::STATUS_NOT_START, $this->filter['status'])) : ?> checked="checked"<?php endif; ?>>не начавшиеся
						</label>
						<label for="status_2" class="checkbox-inline">
							<input type="checkbox" id="status_2" value="<?php echo ResultsHelper::STATUS_CURRENT; ?>"<?php if (in_array(ResultsHelper::STATUS_CURRENT, $this->filter['status'])) : ?> checked="checked"<?php endif; ?>>текущие
						</label>
						<label for="status_3" class="checkbox-inline">
							<input type="checkbox" id="status_3" value="<?php echo ResultsHelper::STATUS_END; ?>"<?php if (in_array(ResultsHelper::STATUS_END, $this->filter['status'])) : ?> checked="checked"<?php endif; ?>>закончившиеся
						</label>
					</div>
					<div class="form-group">
						<span class="help-block">Сортировать по:</span>
						<label class="radio-inline">
							<input type="radio" id="sort_1" name="filter[sort]" value="<?php echo ResultsHelper::SORT_ID; ?>"<?php if ($this->filter['sort'] == ResultsHelper::SORT_ID) : ?> checked="checked"<?php endif; ?>># события
						</label>
						<label class="radio-inline">
							<input type="radio" id="sort_2" name="filter[sort]" value="<?php echo ResultsHelper::SORT_TIME; ?>"<?php if ($this->filter['sort'] == ResultsHelper::SORT_TIME) : ?> checked="checked"<?php endif; ?>>времени
						</label>
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-default">Обновить</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<?php if (is_array($this->results) && count($this->results) > 0) : ?>
<div class="results_table">
	<div class="table-responsive">
		<table class="bckgrnd_mainc_1 table table-bordered table-condensed">
			<thead>
				<tr>
					<th class="th_1">#</th>
					<th class="th_2">Время</th>
					<th class="th_3">Событиие</th>
					<th class="th_4">Счёт</th>
					<th class="th_5">Статус</th>
					<th class="th_6">Коментарии</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($this->results as $section) : ?>
					<tr>
						<td class="group_header" colspan="6"><?php echo $this->escape($section[0]->section_title); ?></td>
					</tr>
					<?php foreach($section as $i=>$res) : ?>
						<tr>
							<td class="td_1"><?php echo $this->escape($res->parser_betid); ?></td>
							<td class="td_2"><?php echo ResultsHelper::getTime($res->datetime); ?></td>
							<td class="td_3">
								<?php if ($res->parent_title != '') : ?>
									<?php echo $this->escape(trim($res->parent_title)); ?>:
								<?php endif; ?>
								<?php echo $this->escape($res->title); ?>
							</td>
							<td class="td_4"><?php echo $this->escape($res->result); ?></td>
							<td class="td_5"><?php echo $this->escape($res->status); ?></td>
							<td class="td_6"><?php echo $res->comment; ?></td>
						</tr>
					<?php endforeach; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php else: ?>
	<p><?php echo JText::_('COM_BETS_RESULTS_NOT_FOUND'); ?></p>
<?php endif; ?>
<script>
	var Bets = {
		getSectionsUrl: '<?php echo JRoute::_('index.php?option=com_bets&view=results&layout=getsections&format=raw', false); ?>'
	};
</script>