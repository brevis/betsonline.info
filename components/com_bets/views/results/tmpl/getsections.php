<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");

?>
<?php if (is_array($this->sections) && count($this->sections) > 0) : ?>
	<?php foreach($this->sections as $section) : ?>
		<option value="<?php echo $section->id; ?>"<?php if (in_array($section->id, $this->selectedSections)) : ?> selected="selected"<?php endif; ?>><?php echo $this->escape($section->title); ?></option>
	<?php endforeach; ?>
<?php endif; ?>