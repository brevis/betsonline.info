<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Results view class.
 *
 * @package     Bets
 * @subpackage  Views
 */
class BetsViewResults extends JViewLegacy
{
	protected $results;

	protected $filter;

	public function display($tpl = null)
	{
		$app = JFactory::getApplication();

		$this->filter = $this->getFilterFromRequest();

		$this->results = ResultsHelper::getResults($this->filter);

		parent::display($tpl);
	}

	protected function getFilterFromRequest()
	{
		$app = JFactory::getApplication();

		$filter = $app->input->get('filter', array(), 'ARRAY');

		// date
		if (!isset($filter['date']))
		{
			$filter['date'] = date('Y-m-d');
		}

		// type
		$types = array_keys(ResultsHelper::getAllTypes());
		if (!isset($filter['type']))
		{
			$filter['type'] = '';
		}
		$filter['type'] = explode(',', strval($filter['type']));
		$filter['type'] = array_filter($filter['type'], function($item) use ($types) {
			return in_array((int)$item, $types, true);
		});

		// section
		if (!isset($filter['section']))
		{
			$filter['section'] = '';
		}
		$filter['section'] = explode(',', strval($filter['section']));
		$filter['section'] = array_filter($filter['section'], function($item) {
			return is_numeric($item);
		});

		// status
		if (!isset($filter['status']))
		{
			$filter['status'] = ResultsHelper::getAllStatuses();
		}
		else
		{
			$filter['status'] = explode(',', strval($filter['status']));
			$filter['status'] = array_filter($filter['status'], function($item) {
				return in_array((int)$item, ResultsHelper::getAllStatuses(), true);
			});
		}

		// sort
		if (!isset($filter['sort']) || !in_array((int)$filter['sort'], ResultsHelper::getAllSorts(), true))
		{
			$filter['sort'] = ResultsHelper::SORT_ID;
		}

		return $filter;
	}
}
