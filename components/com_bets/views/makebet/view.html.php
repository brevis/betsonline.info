<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

$uaFileName = JPATH_ROOT . '/components/com_billing/useraccount.php';
if (!is_file($uaFileName))
{
	throw new Exception('Billing not installed. ["useraccount.php" not found.]');
}

require_once $uaFileName;

/**
 * Makebet view class.
 *
 * @package     Bets
 * @subpackage  Views
 */
class BetsViewMakebet extends JViewLegacy
{
	protected $event;

	protected $type;

	protected $result;

	protected $balance;

	protected $bet_sum;

	protected $errors = array();

	protected $bet;

	public function display($tpl = null)
	{
		$app = JFactory::getApplication();

		if ($app->input->getInt('popup') == 1)
		{
			JRequest::setVar('tmpl', 'component');
		}

		$user = JFactory::getUser();
		if (!$user->id)
		{
			parent::display($tpl);
			return;
		}

		$ua = new UserAccount();
		$this->balance = $ua->GetBalance($user->id);

		$eventId     = $app->input->getInt('event');
		$this->event = LineHelper::getEventById($eventId);
		if (!$this->event)
		{
			parent::display($tpl);
			return;
		}

		$this->type = $app->input->getInt('type');
		if (!in_array($this->type, BetHelper::getAllBetTypes()))
		{
			parent::display($tpl);
			return;
		}

		$this->result = $app->input->getString('result');

		if ($this->event->status != ResultsHelper::STATUS_NOT_START)
		{
			$this->errors['event_status'] = JText::_('COM_BETS_MAKEBET_ERROR_EVENT_NOT_ALLOWED');
		}

		if (count($this->errors) == 0 && $app->input->getString('confirm') == 'ok')
		{
			$this->bet_sum = $app->input->getFloat('bet_sum');

			if ($this->bet_sum <= 0)
			{
				$this->errors['bet_sum'] = JText::_('COM_BETS_MAKEBET_ERROR_INCORRECT_BET');
			}

			if ($this->bet_sum > $this->balance)
			{
				$this->errors['bet_sum'] = JText::_('COM_BETS_MAKEBET_ERROR_BET_TOO_BIG');
			}

			if (count($this->errors) == 0)
			{
				$bet = BetHelper::makeBet($eventId, $this->type, $this->bet_sum, $this->result);
				if ($bet)
				{
					$app->redirect(JRoute::_('index.php?option=com_bets&view=makebet&event=' . $eventId . '&type=' . $this->type . '&popup=' . $app->input->getInt('popup') . '&result=' . $this->result . '&bet=' . $bet->id, false));
				}
				else
				{
					$app->enqueueMessage(JText::_('COM_BETS_SOMETHING_WENT_WRONG'), 'error');
				}
			}
		}

		$this->bet = BetHelper::getBetById($app->input->getInt('bet'));
		if ($this->bet)
		{
			$this->bet_sum = money_format('%.2n', $this->bet->bet_sum);
		}

		$document = JFactory::getDocument();
		$document->setTitle(
				JText::sprintf('COM_BETS_MAKE_BET_TITLE', htmlspecialchars($this->event->title . '. ' . LineHelper::formatDate($this->event->datetime)))
		);

		parent::display($tpl);
	}
}
