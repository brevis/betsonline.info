<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

jimport( 'joomla.html.html' );
JHtml::_('jquery.framework');
$document = JFactory::getDocument();
$document->addStyleSheet(Juri::base() .'media/com_bets/css/bets.css');

$app = JFactory::getApplication();
$user = JFactory::getUser();

$popup = $app->input->getInt('popup');
?>
<?php if ($popup == 1) : ?>
	<script>jQuery('body').removeClass('modal');</script>
<?php endif; ?>
<?php if ($this->event && in_array($this->type, BetHelper::getAllBetTypes())) : ?>
	<div class="makebetcontainer">

		<h4><?php echo htmlspecialchars($this->event->section_title); ?></h4>
		<h2><?php echo htmlspecialchars(LineHelper::getEventTitle($this->event)); ?></h2>

		<div class="balance">
			<?php echo JText::_('COM_BETS_YOUR_BALANCE'); ?>: <b><?php echo FormatDefaultCurrency($this->balance); ?></b>
		</div>

		<?php if ($this->bet) : ?>
			<div class="alert alert-success">
				<?php echo JText::_('COM_BETS_YOUR_BET_ACCEPTED'); ?>
			</div>
		<?php endif; ?>

		<?php if (isset($this->errors['event_status'])) : ?>
			<div class="alert alert-danger">
				<?php echo $this->escape($this->errors['event_status']); ?>
			</div>
		<?php endif; ?>

		<form action="<?php echo JRoute::_('index.php?option=com_bets&view=makebet&event=' . $this->event->id . ' &type=' . $this->type . '&result=' . $this->result . '&popup=' . $popup); ?>" method="post">
			<input type="hidden" name="confirm" value="ok">
			<table class="table">
				<tr>
					<td><label><?php echo JText::_('COM_BETS_EVENT_DATE'); ?></label></td>
					<td><?php echo LineHelper::formatDate($this->event->datetime); ?></td>
				</tr>
				<tr>
					<td><label><?php echo JText::_('COM_BETS_BET'); ?></label></td>
					<td><?php echo BetHelper::stringifyBet($this->event, $this->type, $this->result); ?></td>
				</tr>
				<tr>
					<td><label><?php echo JText::_('COM_BETS_BET_SUM'); ?></label></td>
					<td>
						<input type="text" name="bet_sum" value="<?php echo $this->escape($this->bet_sum); ?>" class="form-control" style="max-width: 100px;" required<?php if ($this->bet || isset($this->errors['event_status'])) : ?> disabled<?php endif; ?>>
						<?php echo GetDefaultCurrencyAbbr(); ?>
						<?php if (isset($this->errors['bet_sum'])) : ?>
							<p style="color: red;"><?php echo $this->escape($this->errors['bet_sum']); ?></p>
						<?php endif; ?>
					</td>
				</tr>
				<?php if (!$this->bet && !isset($this->errors['event_status'])) : ?>
				<tr>
					<td colspan="2" style="text-align:right"><button type="submit" class="btn btn-primary"><?php echo JText::_('COM_BETS_MAKE_BET'); ?></button></td>
				</tr>
				<?php endif; ?>
			</table>
		</form>
	</div>
<?php else: ?>
	<p><?php echo JText::_('COM_BETS_BET_NOT_FOUND'); ?></p>
<?php endif; ?>