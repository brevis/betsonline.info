<?php defined('_JEXEC') or die( 'Restricted access' );

require_once JPATH_ROOT . '/administrator/components/com_extraregistration/helper.php';

jimport('joomla.application.component.controller');
jimport('joomla.html.parameter' );

function GetOption($option)
{
	$db = JFactory::getDBO();
	$query = "select value from `#__extrareg_options` where `option` = '$option'";
	$result = $db->setQuery($query);  
	$result = $db->loadResult();
	
	return $result;
}

function GetTextOption($option)
{
	$db = JFactory::getDBO();
	$query = "select text_data from `#__extrareg_options` where `option` = '$option'";
	$result = $db->setQuery($query);  
	$result = $db->loadResult();
	
	return $result;
}

# For compatibility with older versions of Joola 2.5
if (!class_exists('JControllerLegacy')){
    class JControllerLegacy extends JController {

    }
}

class ExtraregistrationController extends JControllerLegacy
{
	// show registration form
	function display($cachable = false, $urlparams = array())
	{
		if ( ! JRequest::getCmd( 'view' ) ) {
			JRequest::setVar('view', 'registrationform' );
		}
        
		parent::display(false);
	}
	
	// create new user
	function save()
	{
		jimport('joomla.application.component.helper');    
		jimport('joomla.filter.filterinput');
		jimport('joomla.mail.helper');
		jimport('joomla.user.helper');		

		// Load the users plugin group.
		JPluginHelper::importPlugin('user');		

		$billing_enabled = true;

		if(file_exists(JPATH_ROOT . '/components/com_billing/useraccount.php'))
		{
			include_once (JPATH_ROOT . '/components/com_billing/useraccount.php');
			$billing_enabled = true;
		}
		if(file_exists(JPATH_ROOT . '/components/com_billing/account.php'))
		{
			include_once (JPATH_ROOT . '/components/com_billing/account.php');
		}
		if(file_exists(JPATH_ROOT . '/components/com_billing/plugin.php'))
		{
			include_once (JPATH_ROOT . '/components/com_billing/plugin.php');
		}		
		if(file_exists(JPATH_ROOT . '/components/com_jsms/jsmsapi.php'))
		{
			include_once (JPATH_ROOT . '/components/com_jsms/jsmsapi.php');
		}

		
		if (GetOption('recaptcha')) 
		{
			/*$captcha = JCaptcha::getInstance('recaptcha');		
			$checkCaptcha = $captcha->checkAnswer(JRequest::getVar('recaptcha_response_field'));
			if($checkCaptcha==false)
			{
				echo(JText::_('PLG_RECAPTCHA_ERROR_INCORRECT_CAPTCHA_SOL'));
				return false;
			}*/
			JPluginHelper::importPlugin('captcha');
			$post = JFactory::getApplication()->input->post;
			//print_r($_POST); die;
			$dispatcher = JEventDispatcher::getInstance();
			if(isset($_POST['g-recaptcha-response']))
				{$res = $dispatcher->trigger('onCheckAnswer',$_POST['g-recaptcha-response']);
				if(!$res[0]){
					echo(JText::_('PLG_RECAPTCHA_ERROR_INCORRECT_CAPTCHA_SOL'));
					return false;
				}
			}
		}		
		
		$lang = JFactory::getLanguage();
		$lang->load('com_user');
		$lang->load('com_users');
		
		$filter = JFilterInput::getInstance();
		$phone = $filter->clean( JRequest::getVar( 'phone' ) );
		if($phone != '')
		{
			$phone = str_replace('+', '', $phone);
			$phone = str_replace('-', '', $phone);
			$phone = str_replace(' ', '', $phone);
			$phone = str_replace(')', '', $phone);
			$phone = str_replace('(', '', $phone);
			$parse =  parse_url($_SERVER['SERVER_NAME']);
			$email = $phone . '@' . $_SERVER['SERVER_NAME'];
			$sms = true;
		}
		else
		{
			$email = $filter->clean( JRequest::getVar( 'email' ) );
			$sms = false;
		}
		$username = $filter->clean( JRequest::getVar( 'username' ) );

		if(! JMailHelper::isEmailAddress( $email ) ){
			JError::raiseWarning('', JText::_( 'EXTRAEGISTRATION_EMAIL_NOT_VALID') . ' ' . $email);
			return false;
		}
		
		$acl = JFactory::getACL();
		$user = JFactory::getUser(0);
		$db = JFactory::getDBO();
		
		$query = "select count(*) from `#__users` where email = '$email'";
		$result = $db->setQuery($query);   
		$n = $db->loadResult();
		if($n > 0)
		{
			if($sms)
			{
				JFactory::getApplication()->enqueueMessage(JText::_('ALREADY_REGISTERED_FORM_TITLE_SMS'));
			}
			else
			{
				JFactory::getApplication()->enqueueMessage(JText::_('ALREADY_REGISTERED_FORM_TITLE'));
			}
			return false;
		}

		if(GetOption('use_usergroup'))
		{
			$userGroup = array(JRequest::getInt('group_id'));
		}
		else
		{
			$params = JComponentHelper::getParams('com_users');
			$userGroup = array($params->get('new_usertype'));
		}
		$query = 'select * from `#__extrareg_fields` where published = 1 and reg_form = 1 and required = 1 order by id';
		$db->setQuery($query);
		$rows = ComExtraregistrationHelper::filterFieldsByUserGroups($db->loadObjectList(), $userGroup);

		//$query = 'select * from `#__extrareg_fields` where published = 1 and reg_form = 1 and required = 1 order by id';
		//$result = $db->setQuery($query);
		//$rows = $db->loadObjectList();
		if (count($rows) > 0)	
		{
			foreach ( $rows as $row ) 
			{		
				$val = JRequest::getVar("field_$row->id");
				if($val == '')
				{
					JFactory::getApplication()->enqueueMessage(JText::_('COM_EXTRAREGISTRATION_REQ'));
					return false;
				}
				if($row->field_type == 9)
				{
					$val = strtolower(JRequest::getVar("field_$row->id"));
					$query = "select count(*) from `#__users` where LOWER(username) = '$val' or LOWER(email) = '$val'";
					$result = $db->setQuery($query);   
					$usr = $db->loadResult();
					if($usr == 0)
					{
						JFactory::getApplication()->enqueueMessage(JText::_('COM_EXTRAREGISTRATION_REQ'));
						return false;
					}
				}
			}
		}

		$usersParams = JComponentHelper::getParams( 'com_users' );
		$usertype = $usersParams->get('new_usertype');

		$data = array();

		if($sms)
		{
			$data['name'] 		= $phone;
			$data['username'] 	= $username;
		}
		else
		{
			$data['name'] 		= $email;
			$data['username'] 	= $username;
		}
		$data['email'] 		= $email;
		$data['email1'] 	= $email;
		
		$jversion = new JVersion;
		if( $jversion->isCompatible( '1.6' ) ) {
			$data['gid']	= $usertype;
		} else {
			$data['gid'] 	= $acl->get_group_id( '', $usertype, 'ARO' );
		}
		$data['sendEmail'] 	= 0;

		if($sms)
		{
			$pass = rand(1000, 9999) . '-' . rand(1000, 9999);
		}
		else
		{
			$pass = JUserHelper::genRandomPassword();
		}
		$data['password'] 	= $pass;
		$data['password1'] 	= $pass;
		$data['password2'] 	= $pass;
		$data['password_clear'] = $pass;
		

			
			require_once( JPATH_SITE.'/components/com_users/models/registration.php' ); 
			$user_model = new UsersModelRegistration();
			//$activation = $user_model->register( $data );
			$activation = $this->registerUser($data, $sms, $phone);
			$query = "update `#__users` set block = 0, activation = '' where email = '$email'";
			$result = $db->setQuery($query);   
			$result = $db->query();
			if($sms)
			{
				$message = JText::_('COM_EXTRAREGISTRATION_SENDPASS_SMS');
			}
			else
			{
				$message = JText::_('COM_EXTRAREGISTRATION_SENDPASS');
			}
			
			/*switch( $activation ) {
				case 'useractivate':
					$message = JText::_('COM_USERS_REGISTRATION_COMPLETE_ACTIVATE');
					break;
				case 'adminactivate':
					$message = JText::_('COM_USERS_REGISTRATION_COMPLETE_VERIFY');
					break;
				default:
					$message = JText::_('COM_USERS_REGISTRATION_ACTIVATE_SUCCESS');
			}*/
			
		
		$query = "select id from `#__users` where email = '$email'";
		$result = $db->setQuery($query);   
		$uid = $db->loadResult();
		
		if(GetOption('use_usergroup'))
		{
			$group_id = JRequest::getInt('group_id');
			
			if($group_id != '' and $group_id != 0)
			{
				$reg_id = $data['gid'];
				$query = "delete from `#__user_usergroup_map` where user_id = $uid";
				$result = $db->setQuery($query);  
				$result = $db->query();
				
				$query = "insert into `#__user_usergroup_map`(user_id, group_id) values ($uid, $group_id)";
				$result = $db->setQuery($query);  
				$result = $db->query();
			}
		}
		
		$query = 'select * from `#__extrareg_fields` where published = 1 and reg_form = 1 order by id';
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList(); 
		if (count($rows) > 0)	
		{
			foreach ( $rows as $row ) 
			{
				if($row->field_type == 1 or $row->field_type == 2 or $row->field_type == 4 or $row->field_type == 6 or $row->field_type == '')
				{
					$val = JRequest::getVar("field_$row->id");
					$query = "insert into `#__extrareg_fields_values` (field_id, uid, `value`) values($row->id, $uid, '$val')";
					$result = $db->setQuery($query);   
					$result = $db->query();
				}
				if ($row->field_type == 3)
				{
					if($_FILES["field_$row->id"]['tmp_name'] != '')
					{
						$url = '';
						$imageinfo = getimagesize($_FILES["field_$row->id"]['tmp_name']);
						if($imageinfo['mime'] == 'image/gif' or $imageinfo['mime'] == 'image/jpeg' or $imageinfo['mime'] == 'image/png') 
						{

							$tmp_name = $_FILES["field_$row->id"]['tmp_name'];
							$file_name = $_FILES["field_$row->id"]['name'];
							$path_info = pathinfo($file_name);
							$ext = $path_info['extension'];
							$s = uniqid(rand (), true);
							$new_name = JPATH_ROOT . '/media/com_extraregistration/' . $s . '.' . $ext;		
							$x = move_uploaded_file($tmp_name, $new_name);
							$url = JURI::base() . 'media/com_extraregistration/' . $s . '.' . $ext;
							$query = "insert into `#__extrareg_fields_values` (field_id, uid, `value`) values($row->id, $uid, '$url')";
							$result = $db->setQuery($query);   
							$result = $db->query();
						}
					}
				}
				if ($row->field_type == 5)
				{
					$city_id = JRequest::getInt("field_$row->id");
					$query = "select * from `#__extrareg_city` where city_id = $city_id";
					$result = $db->setQuery($query);   
					$city = $db->loadAssoc();
					$cityname = $city['name'];
					$query = "insert into `#__extrareg_fields_values` (field_id, uid, `value`, item_id) values($row->id, $uid, '$cityname', $city_id)";
					$result = $db->setQuery($query);   
					$result = $db->query();
				}
				if ($row->field_type == 7)
				{
					$list = JRequest::getInt("field_$row->id");
					$list_id = $row->list_id;
					$query = "select title from `#__extrareg_list_value` where list_id = $list_id and id = $list";
					$result = $db->setQuery($query);   
					$title = $db->loadResult();
					$query = "insert into `#__extrareg_fields_values` (field_id, uid, `value`, item_id, item2_id) values($row->id, $uid, '$title', $list, $list_id)";
					$result = $db->setQuery($query);   
					$result = $db->query();
				}
				if($row->field_type == 8)
				{
					$val = md5(JRequest::getVar("field_$row->id-1"));
					$query = "insert into `#__extrareg_fields_values` (field_id, uid, `value`) values($row->id, $uid, '$val')";
					$result = $db->setQuery($query);   
					$result = $db->query();
				}				
				if($row->field_type == 9)
				{
					$val = strtolower(JRequest::getVar("field_$row->id"));
					$query = "select * from `#__users` where LOWER(username) = '$val' or LOWER(email) = '$val'";
					$result = $db->setQuery($query);   
					$usr = $db->loadAssoc();
					if($usr['id'] != '')
					{
						$val = $usr['id'];
						$query = "insert into `#__extrareg_fields_values` (field_id, uid, `value`) values($row->id, $uid, '$val')";
						$result = $db->setQuery($query);   
						$result = $db->query();
						
						//Billing integration
						setcookie('partner_id', $usr['id'], time() + 3600 * 24 * 14, '/');
					}
				}
				if($row->field_type == 11)
				{
					$val = JRequest::getVar("field_$row->id");
					$query = "insert into `#__extrareg_fields_values` (field_id, uid, `value`) values($row->id, $uid, '$val')";
					$result = $db->setQuery($query);   
					$result = $db->query();
				}
				if($row->field_type == 12)
				{
					$value = '';
					$items = JRequest::getVar("field_$row->id");
					for($i = 0; $i <count($items); $i++)
					{
						$query = "select title from `#__extrareg_list_value` where list_id = $row->list_id and id = $items[$i] order by id";
						$result = $db->setQuery($query);   
						$v = $db->loadResult();
						if($value != '')
						{
							$value .= ','; 
						}
						$value .= $v;
					}
					$query = "insert into `#__extrareg_fields_values` (field_id, uid, `value`) values($row->id, $uid, '$value')";
					$result = $db->setQuery($query);   
					$result = $db->query();
				}
				if($row->field_type == 13)
				{
					$value = '';
					$items = JRequest::getVar("field_$row->id");
					for($i = 0; $i <count($items); $i++)
					{
						$query = "select title from `#__extrareg_list_value` where list_id = $row->list_id and id = $items[$i] order by id";
						$result = $db->setQuery($query);   
						$v = $db->loadResult();
						if($value != '')
						{
							$value .= ','; 
						}
						$value .= $v;
					}
					$query = "insert into `#__extrareg_fields_values` (field_id, uid, `value`) values($row->id, $uid, '$value')";
					$result = $db->setQuery($query);   
					$result = $db->query();
				}

			}
		}
		if(GetOption('integration') and $billing_enabled)
		{
			if(GetOption('payreg'))
			{
				BillingLogMessage('Extra Registration', 'Controller', "Billing enabled");
				
				$plugin_id = JRequest::getInt('plugin_id');
				$sub_id = JRequest::getInt('sub_id');
				
				
				if(GetOption('pricetype') == 2)
				{
					$sum = GetOption('price');
				}
				else
				{
					$query = "select price from `#__billing_subscription_type` where id = $sub_id";
					$result = $db->setQuery($query);   
					$sum = $db->loadResult();
				}
				BillingLogMessage('Extra Registration', 'Controller', "Registration price $sum");
				
				$query = 'select * from `#__billing_settings` where id = ' . $plugin_id;
				$result = $db->setQuery($query);   
				$row = $db->loadAssoc();
				$paysystem = $row['paysystem'];
				$php_class = $row['php_class'];
				$php_entry = $row['php_entry'];
				
				$partner_id = $_COOKIE['partner_id'];
				BillingLogMessage('Extra Registration', 'Controller', 'COOKIE[partner_id]: ' . $partner_id);
				if ($partner_id != '')
				{
					$partner_id = (int)$partner_id;
					BillingLogMessage('Extra Registration', 'Controller', 'Trying to add user partner. id '. $partner_id);
					$query = "delete from `#__extrareg_partners` where uid = $uid";
					$result = $db->setQuery($query);   
					$result = $db->query();
					$query = "insert into `#__extrareg_partners` (uid, partner_id) values ($uid, $partner_id)";
					$result = $db->setQuery($query);   
					$result = $db->query();
				}
				
				if(GetOption('pricetype') == 2)
				{
					$query = "update `#__users` set activation = '$sum', block = 1 where id = $uid";
				}
				else
				{
					$query = "update `#__users` set activation = '$sub_id', block = 1 where id = $uid";
				}
				$result = $db->setQuery($query);   
				$result = $db->query();
				BillingLogMessage('Extra Registration', 'Controller', "Activation for id $uid blocked");
				
				require_once(JPATH_ROOT . '/components/com_billing/plugins/' . $paysystem . '/' . $php_entry);
		
				eval('$Plugin = new '. $php_class . '("'.$paysystem.'");');
				$Plugin->Init();
				BillingLogMessage('Extra Registration', 'Controller', "Go to payment");
				$Plugin->StartPayment($sum, $uid);
			}
			if(GetOption('billgroup') and GetOption('billgroup_id') != '')
			{
				$billgroup_id = GetOption('billgroup_id');
				$UA = new UserAccount();
				$UA->AddUserToBillingGroup($uid, $billgroup_id);
			}
			
			if(GetOption('setsub'))
			{
				$sid2 = GetOption('sid2');
				if($sid2 != '')
				{
					$UA = new UserAccount();
					
					$query = "select subscription_days from `#__billing_subscription_type` where id = $sid2";
					$result = $db->setQuery($query);
					$days = $db->loadResult();
					
					$UA->AddSubscription($uid, $days, $sid2, true);
				}
			}

		}
		else
		{
			$url = JRoute::_( 'index.php' );
			JControllerLegacy::setRedirect( $url, $message);
		}
	}

	
	function registerUser($data, $sms = false, $phone = '')
	{
		jimport('joomla.application.component.helper');
		jimport('joomla.filter.filterinput');
		jimport('joomla.mail.helper');
		jimport('joomla.user.helper');		

		$config = JFactory::getConfig();
		$db		= JFactory::getDBO();
		$params = JComponentHelper::getParams('com_users');
		$group_id = $params->get('new_usertype');

		$lang = JFactory::getLanguage();
		$lang->load('com_user');
		$lang->load('com_users');
		
		// Initialise the table with JUser.
		$user = new JUser;

		// Prepare the data for the user object.
		$data['email']		= $data['email1'];
		$data['password']	= $data['password1'];
		$useractivation = $params->get('useractivation');
		$sendpassword = $params->get('sendpassword', 1);

		// Check if the user needs to activate their account.
		if (($useractivation == 1) || ($useractivation == 2)) {
			$data['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());
			$data['block'] = 1;
		}

		// Bind the data.
		if (!$user->bind($data)) {
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_BIND_FAILED', $user->getError()));
			return false;
		}

		// Load the users plugin group.
		JPluginHelper::importPlugin('user');

		// Store the data.
		if (!$user->save()) {
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $user->getError()));
			return false;
		}

		//add to group	
		$query = "select id from `#__users` where email = '".$data['email']."'";
		$result = $db->setQuery($query);   
		$uid = $db->loadResult();
		
		$query = "insert into `#__user_usergroup_map`(user_id, group_id) values ($uid, $group_id)";
		$result = $db->setQuery($query);  
		$result = $db->query();
		
		if($sms)
		{
			$sms_text = JText::sprintf('COM_EXTRAREGISTRATION_SMSTEXT', $_SERVER['SERVER_NAME'], $data['password_clear']);
			$uid = $user->id;
			$query = 'select * from `#__jsms_provider` where published = 1 order by id';
			$result = $db->setQuery($query);  
			$rows = $db->loadObjectList(); 	
			if(count($rows) > 0)
			{
				$date = date('Y-md H:i:s');
				$timezone = 4;
				foreach($rows as $row)
				{
					require_once(JPATH_ROOT . '/components/com_jsms/providers/' . $row->php_file);
					eval('$Provider = new '. $row->php_class . '("'.$row->title.'");');
					$tariff = $Provider->GetTariff($phone, $sms_text);
					$balance = $Provider->GetBalance();
					if($balance > $tariff)
					{
						$result_code = $Provider->SendSMS($phone, $sms_text);
						if ($result_code)
						{
							$query = "insert into `#__jsms` 
								(
									`phone`,
									`comment`,
									`created`,
									`created_by`,
									`uid`,
									`timezone`
								)
								values
								(
									'$phone',
									'',
									'$date',
									$uid,
									$uid,
									$timezone
								)";	
								$result = $db->setQuery($query);   
								$result = $db->query();		
								$smsid = $db->insertid();	
								$Provider->AddToHistory($phone, $sms_text, $uid, 100, 1, $smsid);		
							break;
						}
						else
						{
							//SystemMessage($result_code);
						}
					}
				}
			}
		}
		else
		{
			// Compile the notification mail values.
			$data = $user->getProperties();
			$data['fromname']	= $config->get('fromname');
			$data['mailfrom']	= $config->get('mailfrom');
			$data['sitename']	= $config->get('sitename');
			$data['siteurl']	= JUri::root();

			$emailSubject = GetOption('origemailsub');
			$emailSubject = str_replace('%id%', $user->id, $emailSubject);
			$emailSubject = str_replace('%sitename%', $data['sitename'], $emailSubject);
			$emailSubject = str_replace('%name%', $data['name'], $emailSubject);
			$emailSubject = str_replace('%username%', $data['username'], $emailSubject);
			$emailSubject = str_replace('%siteurl%', $data['siteurl'], $emailSubject);

			$emailBody = GetTextOption('email');
			$emailBody = str_replace('%id%', $user->id, $emailBody);
			$emailBody = str_replace('%sitename%', $data['sitename'], $emailBody);
			$emailBody = str_replace('%name%', $data['name'], $emailBody);
			$emailBody = str_replace('%username%', $data['username'], $emailBody);
			$emailBody = str_replace('%siteurl%', $data['siteurl'], $emailBody);
			$emailBody = str_replace('%email%', $data['email'], $emailBody);
			$emailBody = str_replace('%password%', $data['password_clear'], $emailBody);
			$query = 'select * from `#__extrareg_fields` where published = 1 and reg_form = 1 order by id';
			$result = $db->setQuery($query);   
			$rows = $db->loadObjectList(); 
			if (count($rows) > 0)	
			{
				foreach ( $rows as $row ) 
				{
					$emailBody = str_replace("%field_$row->id%", JRequest::getVar("field_$row->id"), $emailBody);
				}
			}		
			
			// Send the registration email.
			$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody, 1);
		}
		//Send Notification mail to administrators
		if (($params->get('useractivation') < 2) && ($params->get('mail_to_admin') == 1)) {
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			$emailBodyAdmin = JText::sprintf(
				'COM_USERS_EMAIL_REGISTERED_NOTIFICATION_TO_ADMIN_BODY',
				$data['name'],
				$data['username'],
				$data['siteurl']
			);

			// get all admin users
			$query = 'SELECT name, email, sendEmail' .
					' FROM #__users' .
					' WHERE sendEmail=1';

			$db->setQuery( $query );
			$rows = $db->loadObjectList();

			// Send mail to all superadministrators id
			foreach( $rows as $row )
			{
				$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBodyAdmin);

				// Check for an error.
				if ($return !== true) {
					$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
					return false;
				}
			}
		}
		// Check for an error.
		if ($return !== true) {
			$this->setError(JText::_('COM_USERS_REGISTRATION_SEND_MAIL_FAILED'));

			// Send a system message to administrators receiving system mails
			$db = JFactory::getDBO();
			$q = "SELECT id
				FROM #__users
				WHERE block = 0
				AND sendEmail = 1";
			$db->setQuery($q);
			$sendEmail = $db->loadColumn();
			if (count($sendEmail) > 0) {
				$jdate = new JDate();
				// Build the query to add the messages
				$q = "INSERT INTO ".$db->quoteName('#__messages')." (".$db->quoteName('user_id_from').
				", ".$db->quoteName('user_id_to').", ".$db->quoteName('date_time').
				", ".$db->quoteName('subject').", ".$db->quoteName('message').") VALUES ";
				$messages = array();

				foreach ($sendEmail as $userid) {
					$messages[] = "(".$userid.", ".$userid.", '".$jdate->toSql()."', '".JText::_('COM_USERS_MAIL_SEND_FAILURE_SUBJECT')."', '".JText::sprintf('COM_USERS_MAIL_SEND_FAILURE_BODY', $return, $data['username'])."')";
				}
				$q .= implode(',', $messages);
				$db->setQuery($q);
				$db->query();
			}
			return false;
		}

		if ($useractivation == 1)
			return "useractivate";
		elseif ($useractivation == 2)
			return "adminactivate";
		else
			return $user->id;	
	}

	public function ajaxgetfields()
	{
		header("Content-Type: text/html; charset=UTF-8", true);

		$app  = JFactory::getApplication();

		$db = JFactory::getDBO();
		$query = 'select * from `#__extrareg_fields` where published = 1 and reg_form = 1 order by id';
		$db->setQuery($query);

		$groupId = JRequest::getInt('groupId');
		$fields = ComExtraregistrationHelper::filterFieldsByUserGroups($db->loadObjectList(), array($groupId));
		ComExtraregistrationHelper::printFieldsHtml($fields);

		$app->close(0);
		return;
	}
}
