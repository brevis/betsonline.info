<?php defined('_JEXEC') or die('Restricted access'); 

$task = JRequest::getVar('task');
$db = JFactory::getDBO();
$user = JFactory::getUser();
$uid = $user->id;
$date = date('Y-m-d H:i:s');

function CheckImage($image_id, $uid)
{
	if($image_id == '' or $uid == '')
	{
		return false;
	}
	$db = JFactory::getDBO();
	$query = "select count(id) from `#__extrareg_user_image` where id = $image_id and uid = $uid";
	$result = $db->setQuery($query);   
	$result = $db->loadResult();
	if($result > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

if($task == 'saveimage')
{

	if($_FILES["foto"]['tmp_name'] != '')
	{
		$url = '';
		$imageinfo = getimagesize($_FILES["foto"]['tmp_name']);
		if($imageinfo['mime'] == 'image/gif' or $imageinfo['mime'] == 'image/jpeg' or $imageinfo['mime'] == 'image/png') 
		{

			$tmp_name = $_FILES["foto"]['tmp_name'];
			$file_name = $_FILES["foto"]['name'];
			$path_info = pathinfo($file_name);
			$ext = $path_info['extension'];
			$s = uniqid(rand (), true);
			$new_name = JPATH_ROOT . '/media/com_extraregistration/' . $s . '.' . $ext;		
			$x = move_uploaded_file($tmp_name, $new_name);
			$url = JURI::base() . 'media/com_extraregistration/' . $s . '.' . $ext;
			
			$title = JRequest::getVar('title');
			$access = JRequest::getInt('access', 1);
			$query = "insert into `#__extrareg_user_image` (`uid`, `title`, `comment`, `url`, `full_size`, `user_access`) values ($uid, '$title', '', '$url', '$url', $access)";
			$result = $db->setQuery($query);   
			$result = $db->query();
			
			$msg = JText::_( 'COM_EXTRAREGISTRATION_OPTIONS_SAVED' );
		}
		else
		{
			$msg = "Ошибка при загрузке файла";
		}
	}

	
	$url = 'index.php?option=com_billing&view=billing';
	$app = JFactory::getApplication();
	$app->redirect($url, $msg); 
}

if($task == 'getaccess')
{
	$id = JRequest::getInt('id');
	if($id > 0)
	{
		$query = "select count(id) from `#__extrareg_access_image` where image_id = $id and owner_id = $uid";
		$result = $db->setQuery($query);   
		$result1 = $db->loadResult();
		$query = "select count(id) from `#__extrareg_request_image` where image_id = $id and uid = $uid";
		$result = $db->setQuery($query);   
		$result2 = $db->loadResult();
		if($result1 == 0 and $result2 == 0)
		{
			$query = "insert into `#__extrareg_request_image` (image_id, request_date, uid) values ($id, '$date', $uid)";
			$result = $db->setQuery($query);
			$result = $db->query();
			$msg = JText::_( 'COM_EXTRAREGISTRATION_REQUEST_ADDED' );
		}
		else
		{
			$msg = JText::_( 'COM_EXTRAREGISTRATION_REQUEST_EXISTS' );
		}
		
	}
	else
	{
		$msg = JText::_( 'COM_EXTRAREGISTRATION_REQUEST_EXISTS' );
	}
	
	$url = 'index.php?option=com_billing&view=billing';
	$app = JFactory::getApplication();
	$app->redirect($url, $msg); 
}

if($task == 'addreq')
{
	$id = JRequest::getInt('id');
	$query = "select * from `#__extrareg_request_image` where id = $id";
	$result = $db->setQuery($query);   
	$req = $db->loadObject();
	
	if(isset($req) and !CheckImage($req->image_id, $uid))
	{	
		$msg = JText::_( 'COM_EXTRAREGISTRATION_REQUEST_DECLINE' );
		$url = 'index.php?option=com_billing&view=billing';
		$app = JFactory::getApplication();
		$app->redirect($url, $msg); 
		return;
	}
	
	$query = "insert into `#__extrareg_access_image` (`image_id`, `owner_id`, `access_start`) values ($req->image_id, $req->uid, '$date')";
	$result = $db->setQuery($query);   
	$result2 = $db->query();	
	$query = "delete from `#__extrareg_request_image` where id = $req->id";
	$result = $db->setQuery($query);   
	$result2 = $db->query();
	$msg = JText::_( 'COM_EXTRAREGISTRATION_REQUEST_ACCEPT' );

	$url = 'index.php?option=com_billing&view=billing';
	$app = JFactory::getApplication();
	$app->redirect($url, $msg); 
}

if($task == 'delreq')
{
	$id = JRequest::getInt('id');
	$query = "delete from `#__extrareg_request_image` where id = $id";
	$result = $db->setQuery($query);   
	$result2 = $db->query();	
	JText::_( 'COM_EXTRAREGISTRATION_REQUEST_DEL' );
	$url = 'index.php?option=com_billing&view=billing';
	$app = JFactory::getApplication();
	$app->redirect($url, $msg); 
	
}

?>
