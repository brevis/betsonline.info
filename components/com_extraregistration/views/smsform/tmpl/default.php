<?php defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();
$baseurl = JURI::base();
$document->addScript($baseurl . "components/com_extraregistration/extra.js");
$document->addStyleSheet('/templates/system/css/system.css');
$document->addStyleSheet('/components/com_extraregistration/tooltip_style.css');
$document->addScript('/components/com_extraregistration/tooltip.js');
$document->addScript('includes/js/joomla.javascript.js');

//error_reporting(E_ALL) ;
//ini_set('display_errors', 'On');

$billing_enabled = false;
if(file_exists(JPATH_ROOT . '/components/com_billing/useraccount.php'))
{
	include_once (JPATH_ROOT . '/components/com_billing/useraccount.php');
	$billing_enabled = true;
}
if(file_exists(JPATH_ROOT . '/components/com_billing/account.php'))
{
	include_once (JPATH_ROOT . '/components/com_billing/account.php');
}
if(file_exists(JPATH_ROOT . '/components/com_jsms/jsmsapi.php'))
{
	include_once (JPATH_ROOT . '/components/com_jsms/jsmsapi.php');
}
else
{
	$url = JURI::base() . 'index.php?option=com_extraregistration&view=registrationform';
	$msg = JText::_('COM_EXTRAREGISTRATION_NOSMS');
	$app = JFactory::getApplication();
	$app->redirect($url, $msg);
	return;
}

function Tooltip($text)
{
	return "<img src='/media/com_extraregistration/images/help.png' onmouseover=\"tooltip.show('$text');\" onmouseout=\"tooltip.hide();\">";
}

function GetSubPrice($id)
{
	$db = JFactory::getDBO();
	$query = "select price from `#__billing_subscription_type` where id = $id";
	$result = $db->setQuery($query);
	return $db->loadResult();
}

function GetSubList($id = '')
{
	$sbs = GetOption('excludesub');
	$db = JFactory::getDBO();
	if($sbs != '')
	{
		$sbs = "and id not in ($sbs)";
	}
	$query = "select distinct * from `#__billing_subscription_type` where is_active = 1 $sbs order by subscription_type";
	$result = $db->setQuery($query);
	$rows = $db->loadObjectList();
	$out = '';
	$out .= '<select name="sub_id">';
	foreach ( $rows as $row )
	{
		if ($row->id == $id) {$s = 'selected';} else {$s = '';}
		$out .= "<option value='$row->id' $s>$row->subscription_type (".FormatDefaultCurrency($row->price).")</option>";
	}
	$out .= '</select>';
	return $out;
}

function GetPlugins($id = '', $label = '')
{
	$db = JFactory::getDBO();
	$query = 'select * from `#__billing_settings` where is_active = 1 order by list_title, description';
	$result = $db->setQuery($query);
	$rows = $db->loadObjectList();
	$out = '';
	$script = '<script type="text/javascript"> var curr = {';
	$script2 = "var rate = {";
	if ($label != '')
	{
		$onchange = " onchange = 'UpdateCurr();' onclick = 'UpdateCurr();' onselect = 'UpdateCurr();'";
	}
	else
	{
		$onchange = '';
	}
	$out .= '<select class="select" name="plugin_id" id="plugin_id" '.$onchange.'>';
	if(count($rows) > 0)
	{
		foreach ( $rows as $row )
		{
			if ($row->id == $id) {$s = 'selected';} else {$s = '';}
			if($row->list_title != '')
			{
				$out .= "<option value='$row->id' $s >$row->list_title</option>";
			}
			else
			{
				$out .= "<option value='$row->id' $s >$row->description</option>";
			}
			$script .= " '$row->id': '" . GetCurrencyAbbr($row->currency_id) . "',";
			$script2 .= " '$row->id': '" . GetCurencyRate($row->currency_id) . "',";
		}
	}
	$script = substr($script, 0, strlen($script)-1);
	$script .= '};';
	$script2 = substr($script2, 0, strlen($script2)-1);
	$script2 .= '};';
	$script .= $script2;
	$script .= 'function UpdateCurr() {';
	$script .= "
		var x = document.getElementById('plugin_id').value; 
		var y = document.getElementById('sm').value; 
		document.getElementById('$label').innerHTML = curr[x];
		document.getElementById('basemoney').innerHTML = (parseFloat(y) * parseFloat(rate[x]));";
	$script .= '}</script>';
	$out .= '</select>';
	if ($label != '')
	{
		$out .= $script;
	}
	return $out;
}

function NewPass($id)
{
	jimport('joomla.mail.helper');
	jimport('joomla.user.helper');

	$uid = JRequest::getInt('uid');
	$pass = JUserHelper::genRandomPassword();
	$config = JFactory::getConfig();

	$db = JFactory::getDBO();
	$query = "select email from `#__users` where id = $uid";
	$result = $db->setQuery($query);
	$recipient[] = $db->loadResult();

	$query = "select title from `#__extrareg_fields` where id = $id";
	$result = $db->setQuery($query);
	$title = $db->loadResult();
	$query = "update `#__extrareg_fields_values` set `value` = '".md5($pass)."' where field_id = $id and uid = $uid";
	$result = $db->setQuery($query);
	$result = $db->query();

	$subject = JText::_('COM_EXTRAREGISTRATION_PASS_RECOVER');
	$body = JText::sprintf('COM_EXTRAREGISTRATION_PASS_MES', $title, $pass);
	JFactory::getMailer()->sendMail($config->get('mailfrom'), $config->get('fromname'), $recipient, $subject, $body, 1);

}

$task = JRequest::getVar('task');
if($task == 'newpass')
{
	$id = JRequest::getVar('id');
	NewPass($id);
	if(file_exists(JPATH_ROOT . '/components/com_billing/useraccount.php'))
	{
		$url = JURI::base() . 'index.php?option=com_billing&view=billing';
		$msg = JText::_('COM_EXTRAREGISTRATION_PASS_CHANGED');
		$app = JFactory::getApplication();
		$app->redirect($url, $msg);
	}
	return;
}
?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton)
	{
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
		}

		varEmail1 = $('phone').value;

		if ( varEmail1 == "" ) {
			alert( "Неверно указан телефон" );
		} else if (! document.formvalidator.isValid(form) ) {
			var msg = 'Некоторые значения указаны неверно. Попробуйте еще раз.';
			if($('phone').hasClass('invalid')){msg += '\n\n\t* Некорректный телефон';}
			alert(msg);
		} else {
			submitform( pressbutton );
		}
	}

	function ValidateNumber(a)
	{
		b = '';	c = '0123456789.';
		for(i=0; i<a.value.length; i++)
		{
			if(a.value[i] == ',')
			{
				b=b+'.';
			}
			else if (c.indexOf(a.value[i])!=-1)
			{b=b+a.value[i]}
		}
		a.value=b.split(',').join('.');
		if (a.value.split('.').length>2)
		{ b=a.value.split('.'); a.value=b[0]+'.'+b[1]}
		if(a.value == '') {a.value = '0'}

	}
	jQuery(document).ready(function() {
		jQuery('select[name="group_id"]').change(function(event){
			var groupId = jQuery(this).val();
			jQuery.ajax({
				method: 'POST',
				url   : '<?php echo JRoute::_('index.php?option=com_extraregistration&task=ajaxgetfields&format=raw', false); ?>',
				dataType: 'html',
				data: {groupId: groupId}
			})
			.done(function (data) {
				jQuery('#extrareg_fields').html(data);
				setupCalendar('.inputbox.calendar');
			});
		});
	});
</script>

<style type="text/css">
	.invalid {border: red;color:red;}
</style>

<?php if ( $this->params->def( 'show_page_title', 1 ) ) : ?>
	<h1 class="componentheading<?php echo $this->escape($this->params->get('pageclass_sfx')); ?>">
		<?php echo $this->escape($this->params->get('page_title')); ?>
	</h1>
<?php endif; ?>

<!-- div names and label classes are equivalent to those of com_user registration form -->
<div class="joomla">
	<div class="registration">
		<form action="index.php" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">
			<div>
				<div class="form-group">
					<label for="phone"><?php echo JText::_( 'PHONE' ); ?> : <span style="color: red;">*</span> </label>
					<input type="text" name="phone" id="phone" class="validate-phone required" placeholder="<?php echo JText::_( 'PHONE' ); ?>" required='required'>
				</div>
				<div class="form-group">
					<label for="username"><?php echo JText::_( 'COM_EXTRAREGISTRATION_USERNAME' ); ?> : <span style="color: red;">*</span> </label>
					<input type="text" class="form-control validate-username required" id="username" name="username" placeholder="<?php echo JText::_( 'COM_EXTRAREGISTRATION_USERNAME' ); ?>" required="required">
				</div>

				<?php
				JHTML::_('behavior.calendar');

				if(GetOption('use_usergroup'))
				{
					$userGroups = explode(',', GetOption('usergroup'));
					$userGroups = array($userGroups[0]);

					echo "<div class='form-group'>";
					echo "<label for='group'>".JText::_('COM_EXTRAREGISTRATION_SELECT_GROUP')."</label>";
					$grp = GetOption('usergroup');
					$query = "select * from `#__usergroups` where id in ($grp)";
					$result = $db->setQuery($query);
					$rows = $db->loadObjectList();
					echo "<select name='group_id' class='form-control'>";
					if(count($rows) > 0)
					{
						foreach($rows as $row)
						{
							echo "<option value='$row->id'>$row->title</option>";
						}
					}
					echo "</select>";
					echo "</div>";
				}
				else
				{
					$params = JComponentHelper::getParams('com_users');
					$userGroups = array($params->get('new_usertype'));
				}


				$db = JFactory::getDBO();
				$query = 'select * from `#__extrareg_fields` where published = 1 and reg_form = 1 order by id';
				$result = $db->setQuery($query);
				$rows = ComExtraregistrationHelper::filterFieldsByUserGroups($db->loadObjectList(), $userGroups);

				echo '<div id="extrareg_fields">';
				if (count($rows) > 0)
				{
					ComExtraregistrationHelper::printFieldsHtml($rows);
				}
				echo '</div>';

				if(GetOption('enable_terms'))
				{
					$text = GetOption('terms_header');
					echo "<div class='checkbox'>";
					echo "<label><input type='checkbox' id='terms' name='terms' onclick='this.value = this.checked; if(this.checked){document.getElementById(\"submit\").disabled = false;}else{document.getElementById(\"submit\").disabled = true;}' value='false'> <a href='index.php?option=com_extraregistration&view=terms&task=show' target='_blank'>$text</a>";
					echo '</label></div>';
				}
				else
				{
					echo "<input type='hidden' id='terms' name='terms' value='true'>";
				}

				if(GetOption('integration') and GetOption('payreg') and $billing_enabled)
				{
					if(GetOption('pricetype') == 2)
					{
						$price = GetOption('price');
						echo JText::_('COM_EXTRAREGISTRATION_PAID_REGISTER') . ' ' . FormatDefaultCurrency($price) . "<br>";
					}
					else
					{
						$price = GetSubPrice(GetOption('sid'));
						echo JText::_('COM_EXTRAREGISTRATION_PAID_REGISTER') . ' ' . GetSubList(GetOption('sid')) . '<br>';
					}
					echo JText::_('COM_EXTRAREGISTRATION_PAID_PAY');

					$db = JFactory::getDBO();
					$query = 'select currency_id from `#__billing_settings` where is_active = 1 order by list_title, description limit 1';
					$result = $db->setQuery($query);
					$currency_id = $db->loadResult();
					if($currency_id == '')
					{
						$currency_id = GetDefaultCurrency();
					}

					$tabs = '';
					$tabs .= '<p class="billing_p" align="center"><input class="hidden" type="hidden" name="sum" value="'.$price.'" id="sm"> ' . GetPlugins('', 'curlab');
					echo '</p>' . $tabs;
				}

				jimport ( 'cms.captcha.captcha' );

				//$enabledRecaptcha = JFactory::getConfig ()->get ( 'captcha' );

				if (GetOption('recaptcha'))
				{
					// 	create instance captcha, get recaptcha
					$recaptchaPlg = JPluginHelper::getPlugin ( 'captcha', 'recaptcha' );

					if (JVERSION>='3.0'){
						$jparams = new JRegistry();

						$jparams->loadString($recaptchaPlg->params);
						$publicKey = $jparams->get ( 'public_key' );
					}
					else{
						$recaptchaPlgParams = new JParameter ( $recaptchaPlg->params );
						$publicKey = $recaptchaPlgParams->get ( 'public_key' );
					}

					$captcha = JCaptcha::getInstance ( 'recaptcha' );
					$reCaptcha = $captcha->display ( $publicKey, '' );

					echo $reCaptcha;

				}

				?>
				<div>
					<input type="submit" name="submit" class="btn btn-success" id="submit" value="<?php echo JText::_('JSAVE') ?>" onclick="submitbutton('save');" <?php if(GetOption('enable_terms')) {echo 'disabled="true"';}?>>
				</div>
				<input type="hidden" name="option" value="com_extraregistration" />
				<input type="hidden" name="task" value="" />
				<?php echo JHTML::_( 'form.token' ); ?>
		</form>
	</div>
</div>
