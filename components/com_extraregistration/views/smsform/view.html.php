<?php defined('_JEXEC') or die( 'Restriction access' );

jimport( 'joomla.application.component.view');


if (!class_exists('JViewLegacy')){
    class JViewLegacy extends JView {

    }
}


class ExtraregistrationViewSmsform extends JViewLegacy
{
	function display($tpl = null)
	{
		$app = JFactory::getApplication();
		$pathway	= $app->getPathway();
		$params 	= $app->getParams();
		$document	= JFactory::getDocument();
		$user		= JFactory::getUser();
		
		if ( $user->guest ) {
			$params->set('page_title', JText::_('REGISTRATION_FORM_TITLE_SMS') );
		} else {			
			$params->set('page_title', JText::_('ALREADY_REGISTERED_FORM_TITLE_SMS') );
		}
		
		$menus	= JSite::getMenu();
		$menu	= $menus->getActive();

        if( is_object( $menu ) ) {
            $menu_params = new JRegistry( $menu->params );
            if( $menu_params->get( 'page_title') ) {
                $params->set('page_title', $menu_params->get( 'page_title') );
            }
        }

        $document->setTitle( $params->get( 'page_title' ) );
        
		$document->addScript(  '/includes/js/joomla.javascript.js' );
		$pathway->addItem(JText::_('New'), '');
		
		JHTML::_('behavior.formvalidation');
		
		$this->assignRef('params', $params);
		parent::display($tpl);
	}
}
?>
