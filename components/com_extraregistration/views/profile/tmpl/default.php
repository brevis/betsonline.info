<?php defined('_JEXEC') or die('Restricted access');

require_once JPATH_ROOT . '/administrator/components/com_extraregistration/helper.php';

$document = JFactory::getDocument();
$document->addStyleSheet('/components/com_extraregistration/views/profile/tmpl/profile.css');

$task = JRequest::getVar('task');
$id = JRequest::getVar('id');
$db = JFactory::getDBO();
$user = JFactory::getUser();
$uid = $user->id;

$document->addStyleDeclaration($style);

function GetFieldValue($field_id, $uid)
{
	$db = JFactory::getDBO();
    $query = "select value from `#__extrareg_fields_values` where field_id = $field_id and uid = $uid";
	$result = $db->setQuery($query);
	return $db->loadResult();
}

function GetAvatar($uid)
{
	$ava_id = GetOption('pic_id');
	if($ava_id != '')
	{
		$scr = GetFieldValue($ava_id, $uid);
		if($scr != '')
		{
			return "<img src='$scr' class='avatar'>";
		}
		else
		{
			return '';
		}
	}
	else
	{
		return '';
	}
}

function GetUserFields($uid)
{
	$db = JFactory::getDBO();
	$q = "select * from `#__extrareg_fields` order by order_id";
	$result = $db->setQuery($q);

	$rows = ComExtraregistrationHelper::filterFieldsByUserGroups($db->loadObjectList(), JFactory::getUser($uid)->get('groups'));
	foreach($rows as $row)
	{
		$div = "<div class='field'><strong>$row->title :</strong> ";
		switch ($row->field_type)
		{
			case 1:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;
			case 2:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
			case 4:
				$time = strtotime(GetFieldValue($row->id, $uid));
				$div .= "<span class='value'>". date('d.m.Y', $time) . '</span></div>';
				echo $div;
			break;			
			case 5:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
			case 6:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . ' ' . GetOption('abbr') . '</span></div>';
				echo $div;
			break;			
			case 10:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
			case 11:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
			case 12:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
			case 13:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
			case 14:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
		}	
	}
	return '';
}

function GetGallery($uid, $owner_id)
{
		$db = JFactory::getDBO();
		$query = "select i.*, 
			(select access_end from `#__extrareg_access_image` where owner_id = $owner_id and image_id = i.id) as access_end,
			(select count(id) from `#__extrareg_access_image` where owner_id = $owner_id and image_id = i.id) as has_access
			from `#__extrareg_user_image` i where uid = $uid and published = 1";
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList();
		if(count($rows) > 0)
		{
			$out .= "<div class='hovergallery'>";
			foreach($rows as $row)
			{
				if($row->user_access == 1)
				{
					$out .= "<img src='$row->url'>";
				}
				if($row->user_access == 2 and $row->has_access > 0)
				{
					$out .= "<img src='$row->url'>";
				}
				if($row->user_access == 2 and $row->has_access == 0)
				{
					$out .= "<a href='index.php?option=com_extraregistration&view=images&task=getaccess&id=$row->id'><img src='/components/com_billing/tabs/tab_images/lock.png' border='0'></a>";
				}	
				//$out .= "<p>$row->title</p>";
			}
			$out .= "</div>";
		}
	return $out;	
}

?>

<div class='profile'>
	<div class='avatar'>
		<?php echo GetAvatar($id); ?>
	</div>
	<div class='info'>
		<?php echo GetUserFields($id); ?>
	</div>
	<div class='gallery'>
		<?php echo GetGallery($id, $uid); ?>
	</div>
</div>
