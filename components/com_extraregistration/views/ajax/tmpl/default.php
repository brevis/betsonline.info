<?php defined('_JEXEC') or die('Restricted access'); 

$task = JRequest::getVar('task');
$id = JRequest::getInt('id');
$timezone = JRequest::getInt('timezone');
$phone = JRequest::getVar('phone');
$db = JFactory::getDBO();
$fieldid = JRequest::getVar('fieldid');
$user = JFactory::getUser();
$uid = $user->id;

if($task == 'phone')
{
	if (file_exists(JPATH_ROOT . '/components/com_jsms/jsmsapi.php'))
	{
		include_once (JPATH_ROOT . '/components/com_jsms/jsmsapi.php');
		
		$arr = jsms_activate_phone($uid, $phone, $timezone);
	}

	return;
}

if($task == 'getregions')
{
    $query = "select * from `#__extrareg_region` where country_id = $id order by name";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 
	if (count($rows) > 0)	
	{
		echo "<select name='region_$fieldid' id='region_$fieldid' onchange='ajaxfunction(1, $fieldid);'>";
		foreach ( $rows as $row ) 
		{
			echo "<option value='$row->region_id'>$row->name</option>";
		}
		echo "</select>";
	}
	return;
}


if($task == 'getcities')
{
	$query = "select * from `#__extrareg_city` where region_id = $id order by name";
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 
	if (count($rows) > 0)	
	{
		echo "<select name='field_$fieldid'>";
		foreach ( $rows as $row ) 
		{
			echo "<option value='$row->city_id'>$row->name</option>";
		}
		echo "</select>";
	}
	return;
}

if($task='checkuser')
{
	$username = strtolower(JRequest::getVar('username'));
	$query = "select count(*) from `#__users` where LOWER(username) = '$username'  or LOWER(email) = '$username'";
	$result = $db->setQuery($query);   
	$n = $db->loadResult();
	if($n > 0)
	{
		echo "<img src='/administrator/templates/bluestork/images/admin/icon-16-allow.png'>";
	}
	else
	{
		echo "<img src='/administrator/templates/bluestork/images/admin/icon-16-notice-note.png' title='".JText::_('COM_EXTRAREGISTRATION_NOT_FOUND')."' alt='".JText::_('COM_EXTRAREGISTRATION_NOT_FOUND')."'>";
	}
	return;
}

?>
