<?php defined('_JEXEC') or die('Restricted access'); 

$document = JFactory::getDocument();
$document->addStyleSheet('/components/com_extraregistration/views/filter/tmpl/filter.css');


$task = JRequest::getVar('task');
$filter_id = JRequest::getInt('filter_id');

$db = JFactory::getDBO();
$user = JFactory::getUser();
$uid = $user->id;

function GetFieldValue($field_id, $uid)
{
	$db = JFactory::getDBO();
    $query = "select value from `#__extrareg_fields_values` where field_id = $field_id and uid = $uid";
	$result = $db->setQuery($query);
	return $db->loadResult();
}

function GetAvatar($uid)
{
	$ava_id = GetOption('pic_id');
	if($ava_id != '')
	{
		$scr = GetFieldValue($ava_id, $uid);
		if($scr != '')
		{
			return "<img src='$scr' class='avatar'>";
		}
		else
		{
			return '';
		}
	}
	else
	{
		return '';
	}
}

function GetUserFields($uid)
{
	$db = JFactory::getDBO();
	$q = "select * from `#__extrareg_fields` order by order_id";
	$result = $db->setQuery($q);   
	$rows = $db->loadObjectList();
	foreach($rows as $row)
	{
		$div = "<div class='field'><strong>$row->title :</strong> ";
		switch ($row->field_type)
		{
			case 1:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;
			case 2:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
			case 4:
				$time = strtotime(GetFieldValue($row->id, $uid));
				$div .= "<span class='value'>". date('d.m.Y', $time) . '</span></div>';
				echo $div;
			break;			
			case 5:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
			case 6:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . ' ' . GetOption('abbr') . '</span></div>';
				echo $div;
			break;			
			case 10:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
			case 11:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
			case 12:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
			case 13:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
			case 14:
				$div .= "<span class='value'>".GetFieldValue($row->id, $uid) . '</span></div>';
				echo $div;
			break;			
		}	
	}
	return '';
}

function GetFilterValue($field_id, $cid, $no = 1)
{
	return JRequest::getVar("filter_$field_id"."_$no");
}

function GetFilterValueX($field)
{
	return JRequest::getVar("$field");
}

function GetCheckedX($field, $val = 0)
{
	if($val == 1)
	{
		return JRequest::getVar("$field");
	}
	if(JRequest::getVar("$field"))
	{
		return 'checked';
	}
	else
	{
		return '';
	}
}

function GetChecked($field_id, $val = 0)
{
	if($val == 1)
	{
		return JRequest::getVar("cb_$field_id");
	}
	if(JRequest::getVar("cb_$field_id"))
	{
		return 'checked';
	}
	else
	{
		return '';
	}
}

function BuildFilter($filter_id)
{
	$filter_id = JRequest::getInt('filter_id');
	$layout = JRequest::getInt('layout');
	$db = JFactory::getDBO();
	$val = '';
	$out = '';
	$out .=  "<form action='index.php?option=com_extraregistration&view=filter&filter_id=$filter_id&layout=$layout' method='post'><div class='filter_div'>";
		$r = 'filter';
		$r1 = '';
		$query = "select * from `#__extrareg_fields` where id in (select field_id from `#__extrareg_filter_field` where filter_id = $filter_id) order by order_id";
		$result = $db->setQuery($query);   
		$rows = $db->loadObjectList();
		$ss = 0;
		foreach($rows as $row)
		{
			if($ss % 2 == 0)
			{
				$open = true;
				$out .=  "<div class='filter_row'>";
			}
			else
			{
				$open = false;
			}

			$out .=  "<div class='filter_item'>";
			$out .=  "<div class='filter_label'> <input type='checkbox' name='cb_$row->id' ".GetChecked($row->id)." onclick='this.value=this.checked;' value='".GetChecked($row->id, 1)."'> ". $row->title ."</div> <div class='filter_control'>";
			switch ($row->field_type)
			{
				case 1:
						$out .=  "<input type='text' name='filter_$row->id"."_1' id='filter_$row->id' class='$r' $r1 value='".GetFilterValue($row->id, $val)."' size='5' style='margin-left: 17px;'> ";
				break;
				case 2:
						$out .=  JText::_('COM_EXTRAREGISTRATION_FROM')." <input type='text' name='filter_$row->id"."_1' id='filter_$row->id"."_1' class='$r' $r1 value='".GetFilterValue($row->id, $val, 1)."' onblur='ValidateNumber(this);' size='5' style='width: 70px;'> " .
						JText::_('COM_EXTRAREGISTRATION_TO') .
						" <input type='text' name='filter_$row->id"."_2' id='filter_$row->id"."_2' class='$r' $r1 value='".GetFilterValue($row->id, $val, 2)."' onblur='ValidateNumber(this);' size='5' style='width: 70px;'> ";
				break;
				case 4:
						$out .=  JText::_('COM_EXTRAREGISTRATION_FROM'). ' ' . JHTML::_('calendar', GetFilterValue($row->id, $val, 1), "filter_$row->id"."_1", "filter_$row->id"."_1", '%Y-%m-%d', array('class'=>"inputbox $r", 'size'=>'20', 'maxlength'=>'19', 'style' => 'width: 100px;')) . ' ' .
						JText::_('COM_EXTRAREGISTRATION_TO') . ' ' .
						JHTML::_('calendar', GetFilterValue($row->id, $val, 2), "filter_$row->id"."_2", "filter_$row->id"."_2", '%Y-%m-%d', array('class'=>"inputbox $r", 'size'=>'20',  'maxlength'=>'19', 'style' => 'width: 100px;')) . ' ' ;
				break;
				case 6:
						$out .=  JText::_('COM_EXTRAREGISTRATION_FROM'). " <input type='text' name='filter_$row->id"."_1' id='filter_$row->id"."_1' class='$r' $r1 onblur='ValidateNumber(this);' value='".GetFilterValue($row->id, $val, 1)."' size='5' style='width: 70px;'> " . 
						JText::_('COM_EXTRAREGISTRATION_TO') .
						" <input type='text' name='filter_$row->id"."_2' id='filter_$row->id"."_2' class='$r' $r1 onblur='ValidateNumber(this);' value='".GetFilterValue($row->id, $val, 2)."' size='5' style='width: 70px;'> " .	GetDefaultCurrencyAbbr() . ' ';
				break;
				case 7:
						$out .=  "<select name='filter_$row->id"."_1' id='filter_$row->id"."_1' class='$r' $r1 style='margin-left: 17px;'>";

						$query = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
						$result = $db->setQuery($query);   
						$items = $db->loadObjectList();
						if(count($items) > 0)
						{
							foreach($items as $item)
							{
								if($val == $item->id and $val != -1) {$s = 'selected';} else {$s = '';}
								$out .=  "<option value='$item->id' $s>$item->title</option>";									
							}
						}
						$out .=  "</select> ";
				break;
				case 9:
						$out .=  GetUserList2(GetFilterValue($row->id, $val), "filter_$row->id"."_1") . ' ';
				break;
				case 12:
						$out .=  "<br><select multiple='multiple' name='filter_$row->id"."_1". '[]' ."' id='filter_$row->id' style='margin-left: 17px;'>";
						$query = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
						$result = $db->setQuery($query);   
						$items = $db->loadObjectList();
						if(count($items) > 0)
						{
							foreach($items as $item)
							{
								$s = '';
								if(count($val) != 0)
								{
									for($i = 0; $i < count($val); $i++)
									{
										if($val[$i] == $item->id) {$s = 'selected';}
									}
								}
								$out .=  "<option value='$item->id' $s>$item->title</option>";									
							}
						}
						$out .=  "</select> ";
				break;
				case 13:
					
						$out .=  "<br>";
						$query = "select * from `#__extrareg_list_value` where list_id = $row->list_id order by id";
						$result = $db->setQuery($query);   
						$items = $db->loadObjectList();
						if(count($items) > 0)
						{
							foreach($items as $item)
							{
								$s = '';
								if(count($val) > 0)
								{
									foreach($val as $value)
									{
										if($value == $item->id) {$s = 'checked';} 
									}
								}
								$out .=  "<input type='checkbox' value='$item->id' name='filter_$row->id"."_1". '[]' ."' $s style='margin-left: 17px;'> $item->title <br>";
							}
						}
						$out .=  " ";
				break;
				case 14:
						$out .=  "<textarea name='filter_$row->id"."_1' id='filter_$row->id"."_1' class='$r' $r1 style='margin-left: 17px;'>".GetFilterValue($row->id, $val)."</textarea> <br>";
				break;
				default:
						$out .=  "<input type='text' name='filter_$row->id"."_1' id='filter_$row->id"."_1' class='$r' $r1 value='".GetFilterValue($row->id, $val)."' style='margin-left: 17px;'> ";
				break;
			}
			$out .=  "</div></div>";
			
			if($ss % 2 == 1)
			{
				$out .=  "</div>";
			}
			
			$ss = $ss + 1;
		}
		
		if($open)
		{
			$out .=  "</div>";
		}	
		$out .=  "<div class='filter_row'>";
		$out .=  "<input type='submit' value='Применить'>";
		$out .=  "</div>";
	$out .=  "</div></form>";

	return $out;
} 

function GetFieldParam($field_id)
{
	return "(select value from `#__extrareg_fields_values` where field_id = $field_id and uid = p.id) as val_$field_id";
}

function GetFieldParamExt($field_id, $single = true, $like = false)
{
	if($single)
	{
		$val = strtoupper(JRequest::getVar("filter_$field_id"."_1"));
		if($like)
		{
			return "val_$field_id UPPER(value) LIKE '%$val%'";
		}
		else
		{
			return "val_$field_id UPPER(value) = '$val'";
		}
	}
	else
	{
		$val1 = JRequest::getVar("filter_$field_id"."_1");	
		$val2 = JRequest::getVar("filter_$field_id"."_2");	
		return "val_$field_id between '$val1' and '$val2' ";
	}
}

function BuildQuery($filter_id)
{
	$db = JFactory::getDBO();
	$user = JFactory::getUser();
	$uid = $user->id;
	$layout = JRequest::getVar('layout');

	$q = "select * from `#__extrareg_fields` where id in (select field_id from `#__extrareg_filter_field` where filter_id = $filter_id) order by order_id";
	$result = $db->setQuery($q);   
	$rows = $db->loadObjectList();
	
	$query = $db->getQuery(true);
	$query->from('`#__users` p');
	$query->select('p.*');
	if($layout == 'favorites')
	{
		$query->where("id in (select uid from `#__extrareg_favorites` where owner_id = $uid)");
	}
	foreach($rows as $row)
	{
		$query->select("(select `value` from `#__extrareg_fields_values` where uid = p.id and field_id = $row->id) as val_$row->id");
	}
	$q = "CREATE TEMPORARY TABLE IF NOT EXISTS `#__extrareg_filter_tmp` " . $query;
	$db->setQuery($q);
	$db->execute();
	
	$query = $db->getQuery(true);
	$query->from('`#__extrareg_filter_tmp` p');
	$query->select('p.*');
	
	$q = "select * from `#__extrareg_fields` order by order_id";
	$result = $db->setQuery($q);   
	$rows = $db->loadObjectList();
	foreach($rows as $row)
	{
		$check = JRequest::getVar("cb_$row->id");
		if($check)
		{
			switch ($row->field_type)
			{
				case 1:
					$param = GetFieldParamExt($row->id, true, true);
					$query->where($param);
				break;	
				case 2:
					$param = GetFieldParamExt($row->id, false, false);
					$query->where($param);
				break;	
				case 4:
					$param = GetFieldParamExt($row->id, false, false);
					$query->where($param);
				break;	
				case 6:
					$param = GetFieldParamExt($row->id, false, false);
					$query->where($param);
				break;	
			}
		}	
	}
	
	$db->setQuery($query);
	$rows = $db->loadObjectList();
	return $rows;
}

	$db = JFactory::getDBO();
	echo BuildFilter($filter_id);
	$rows = BuildQuery($filter_id);
	if(count($rows) > 0)
	{
	?>
		<div class='userlist'>
			<?php foreach($rows as $row) 
				{?>
				<div class='userinfo'>
					<div class='avatar'>
						<?php echo GetAvatar($row->id); ?>
					</div>
					<div class='about'>
						<?php echo GetUserFields($row->id); ?>
					</div>
					<div class='details'>
						<p><a href="index.php?option=com_extraregistration&view=profile&id=<?php echo $row->id; ?>"><?php echo JText::_('COM_EXTRAREGISTRATION_DETAILS');?></a></p>
						<p><a href="index.php?option=com_extraregistration&view=favorites&task=add&id=<?php echo $row->id; ?>"><?php echo JText::_('COM_EXTRAREGISTRATION_FAVORITES_ADD');?></a></p>
					</div>
				</div>
			<?php
				}
			?>
		</div>
	<?php	
	}

?>
