<?php defined('_JEXEC') or die('Restricted access'); 

$task = JRequest::getVar('task');
$db = JFactory::getDBO();
$user = JFactory::getUser();
$uid = $user->id;

function SetUserValue($field_id, $uid, $value, $item_id = 0)
{
	$db = JFactory::getDBO();
	$query = "select count(*) from `#__extrareg_fields_values` where field_id = $field_id and uid = $uid";
	$result = $db->setQuery($query);   
	$result = $db->loadResult();
	if($result > 0)
	{
		$query = "update `#__extrareg_fields_values` set `value` = '$value', item_id = $item_id where field_id = $field_id and uid = $uid";
	}
	else
	{
		$query = "insert into `#__extrareg_fields_values` (uid, field_id, `value`, item_id) values ($uid, $field_id, '$value', $item_id)";
	}
	$result = $db->setQuery($query);   
	$result = $db->query();
}

if($task == 'show')
{
	$query = "select text_data from `#__extrareg_options` where `option` = 'terms'";
	$result = $db->setQuery($query);  
	$result = $db->loadResult();
	
	echo $result;
}

if($task == 'saveprofile')
{
	$query = 'select * from `#__extrareg_fields` where published = 1 order by order_id';
	$result = $db->setQuery($query);   
	$rows = $db->loadObjectList(); 
	if (count($rows) > 0)	
	{
		foreach ( $rows as $row ) 
		{
			if($row->can_edit != 1) {continue;}
			if($row->field_type == 1 or $row->field_type == 2 or $row->field_type == 4 or $row->field_type == 6 or $row->field_type == 14 or $row->field_type == '')
			{
				$val = JRequest::getVar("field_$row->id");
				SetUserValue($row->id, $uid, $val);
			}
			if ($row->field_type == 3)
			{
				if($_FILES["field_$row->id"]['tmp_name'] != '')
				{
					$url = '';
					$imageinfo = getimagesize($_FILES["field_$row->id"]['tmp_name']);
					if($imageinfo['mime'] == 'image/gif' or $imageinfo['mime'] == 'image/jpeg' or $imageinfo['mime'] == 'image/png') 
					{

						$tmp_name = $_FILES["field_$row->id"]['tmp_name'];
						$file_name = $_FILES["field_$row->id"]['name'];
						$path_info = pathinfo($file_name);
						$ext = $path_info['extension'];
						$s = uniqid(rand (), true);
						$new_name = JPATH_ROOT . '/media/com_extraregistration/' . $s . '.' . $ext;		
						$x = move_uploaded_file($tmp_name, $new_name);
						$url = JURI::base() . 'media/com_extraregistration/' . $s . '.' . $ext;
						SetUserValue($row->id, $uid, $url);
					}
				}
			}
			if ($row->field_type == 5)
			{
				$city_id = JRequest::getInt("field_$row->id");
				$query = "select * from `#__extrareg_city` where city_id = $city_id";
				$result = $db->setQuery($query);   
				$city = $db->loadAssoc();
				$cityname = $city['name'];
				SetUserValue($row->id, $uid, $cityname, $city_id);
			}
			if ($row->field_type == 7)
			{
				$list = JRequest::getInt("field_$row->id");
				$list_id = $row->list_id;
				$query = "select title from `#__extrareg_list_value` where list_id = $list_id and id = $list";
				$result = $db->setQuery($query);   
				$title = $db->loadResult();
	
				$query = "select count(*) from `#__extrareg_fields_values` where field_id = $row->id and uid = $uid";
				$result = $db->setQuery($query);   
				$result = $db->loadResult();
				if($result > 0)
				{
					$query = "update `#__extrareg_fields_values` set value = '$title', item_id = $list, item2_id = $list_id where field_id = $row->id and uid = $uid";
				}
				else
				{
					$query = "insert into `#__extrareg_fields_values` (field_id, uid, `value`, item_id, item2_id) values($row->id, $uid, '$title', $list, $list_id)";
				}
				$result = $db->setQuery($query);   
				$result = $db->query();
			}
			if ($row->field_type == 12)
			{
				$value = '';
				$items = JRequest::getVar("field_$row->id");
				for($i = 0; $i <count($items); $i++)
				{
					$query = "select title from `#__extrareg_list_value` where list_id = $row->list_id and id = $items[$i] order by id";
					$result = $db->setQuery($query);   
					$v = $db->loadResult();
					if($value != '')
					{
						$value .= ','; 
					}
					$value .= $v;
				}
				$query = "select count(*) from `#__extrareg_fields_values` where field_id = $row->id and uid = $uid";
				$result = $db->setQuery($query);   
				$result = $db->loadResult();
				if($result > 0)
				{
					$query = "update `#__extrareg_fields_values` set value = '$value' where field_id = $row->id and uid = $uid";
				}
				else
				{
					$query = "insert into `#__extrareg_fields_values` (field_id, uid, `value`) values($row->id, $uid, '$value')";
				}
				$result = $db->setQuery($query);   
				$result = $db->query();
			}	
			if ($row->field_type == 13)
			{
				$value = '';
				$items = JRequest::getVar("field_$row->id");
				for($i = 0; $i <count($items); $i++)
				{
					$query = "select title from `#__extrareg_list_value` where list_id = $row->list_id and id = $items[$i] order by id";
					$result = $db->setQuery($query);   
					$v = $db->loadResult();
					if($value != '')
					{
						$value .= ','; 
					}
					$value .= $v;
				}
				$query = "select count(*) from `#__extrareg_fields_values` where field_id = $row->id and uid = $uid";
				$result = $db->setQuery($query);   
				$result = $db->loadResult();
				if($result > 0)
				{
					$query = "update `#__extrareg_fields_values` set value = '$value' where field_id = $row->id and uid = $uid";
				}
				else
				{
					$query = "insert into `#__extrareg_fields_values` (field_id, uid, `value`) values($row->id, $uid, '$value')";
				}
				$result = $db->setQuery($query);   
				$result = $db->query();
			}	

		}
	}
	$msg = JText::_( 'COM_EXTRAREGISTRATION_OPTIONS_SAVED' );
	
	$oldpass = JRequest::getVar('oldpass');
	$password1 = JRequest::getVar('password1');
	$password2 = JRequest::getVar('password2');
	if($oldpass != '' and $password1 != '' and $password2 != '')
	{
		$lang = JFactory::getLanguage();
		$lang->load('com_users');
		if($password1 == $password2)
		{
			$query = "select password from `#__users` where id = $uid";
			$result = $db->setQuery($query);   
			$hash = $db->loadResult(); 
			//echo $hash . '<br>';
			$hash = explode(':', $hash);
			$md5 = $hash[0];
			$salt = $hash[1]; 
			$password = JUserHelper::getCryptedPassword($oldpass, $salt); 
			if($password == $md5)
			{
				$user = JUser::getInstance($uid);
				$salt = JUserHelper::genRandomPassword(32);
				$crypted	= JUserHelper::getCryptedPassword($password1, $salt);
				$password	= $crypted.':'.$salt; 				
				$user->password			= $password;
				$user->activation		= '';
				$user->password_clear	= $password1;
				if (!$user->save(true)) 
				{
					return new JException(JText::sprintf('COM_USERS_USER_SAVE_FAILED', $user->getError()), 500);
				}
			}
			else
			{
				$msg = JText::_( 'COM_USERS_FIELD_RESET_PASSWORD2_MESSAGE' );
			}
		}
		else
		{
			$msg = JText::_( 'COM_USERS_FIELD_RESET_PASSWORD1_MESSAGE' );
		}
	}
	
	$url = 'index.php?option=com_billing&view=billing';
	$app = JFactory::getApplication();
	$app->redirect($url, $msg); 
}

?>
