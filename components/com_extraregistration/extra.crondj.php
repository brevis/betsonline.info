<?php

define( '_JEXEC', 1 );


// Require the base controller
if(!defined('DS')){
    define('DS',DIRECTORY_SEPARATOR);
}

define('JPATH_BASE', substr(__FILE__,0,strrpos(__FILE__, DS."components")));

require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );

$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

$lang = JFactory::getLanguage();
$language_tag = 'ru-RU';
$lang->load('com_extraregistration', JPATH_ADMINISTRATOR, $language_tag, true);

function GetOption($option)
{
	$db = JFactory::getDBO();
	$query = "select value from `#__extraregistration_options` where `option` = '$option'";
	$result = $db->setQuery($query);  
	$result = $db->loadResult();
	
	return $result;
}

function GetFieldValue($field_id, $uid)
{
	$db = JFactory::getDBO();
    $query = "select value from `#__extrareg_fields_values` where field_id = $field_id and uid = $uid";
	$result = $db->setQuery($query);
	return $db->loadResult();
}

function SendMailDJ($uid, $subject, $body)
{
	$db = JFactory::getDBO();
	$query = "select * from `#__users` where id = $uid";
	$result = $db->setQuery($query);  
	$user = $db->loadObject();

	$config = JFactory::getConfig();
	$from = $config->get('mailfrom');
	$fromname = $config->get('fromname');
	$recipient[] = $user->email; 
	$mode = 1;
	JFactory::getMailer()->sendMail($from, $fromname, $recipient, $subject, $body, $mode);
}

function GetCategoryName($cat_id)
{
	$db = JFactory::getDBO();

	$query = "select name from `#__djcf_categories` where id = $cat_id";
	$result = $db->setQuery($query);  
	return $db->loadResult();
}

header('Content-Type: text/html; charset=utf-8'); 
$db = JFactory::getDBO();

$query = "select sent_date from `#__extra_djmail` order by id desc limit 1";
$result = $db->setQuery($query);  
$date = $db->loadResult();

$query = "select * from `#__users` where block = 0 order by id";
$result = $db->setQuery($query);  
$rows = $db->loadObjectList();
if(count($rows)> 0)
{
	foreach($rows as $row)
	{
		$body = '<h1>Дайджест объявлений за последние сутки</h1>';
		echo "user " . $row->id . '<br>';
		$catid = '';
		$query = "select * from `#__extra_djcats` where uid = $row->id";
		$result = $db->setQuery($query);  
		$cats = $db->loadObjectList();
		if(count($cats) > 0)
		{
			foreach($cats as $cat)
			{
				if($catid == '')
				{
					$catid = $cat->category_id;
				}
				else
				{
					$catid .= ', ' . $cat->category_id;
				}
			}
			if($catid != '')
			{
				echo "category " . $catid . '<br>';
				$last_cat = '';
				if($date != '')
				{
					$query = "select * from `#__djcf_items` where cat_id in ($catid) where date_start > '$date' cat_id, date_start";
				}
				else
				{
					$query = "select * from `#__djcf_items` where cat_id in ($catid) order by cat_id, date_start";
				}
				$result = $db->setQuery($query);  
				$items = $db->loadObjectList();
				if(count($items)> 0)
				{
					foreach($items as $item)
					{
						if($last_cat == '')
						{
							$last_cat = $item->cat_id;
							$body .= "<h2>".GetCategoryName($last_cat)."</h2>";
							$n = 0;
						}
						if($last_cat != $item->cat_id)
						{
							$last_cat = $item->cat_id;
							$body .= "<h2>".GetCategoryName($last_cat)."</h2>";
							$n = 0;
						}
						$body .= "<p><a href='/index.php?option=com_djclassifieds&view=item&id=$item->id'>$item->name ";
						if($item->price != '' and $item->price != 0)
						{
							$body .=" ($item->price )";
						}
						$body .="</a><br/>";
						$body .= "$item->intro_desc</p>";
						$n++;
					}
				}
			}

			echo $body;
			SendMailDJ($row->id, "Дайджест объявлений за день", $body);
			echo '===========================================================================================<br>';
		}
	}
	$query = "insert into `#__extra_djcats` (sent_date) values ('".date('Y-m-d H:i:s')."')";
	$result = $db->setQuery($query); 
}

?>