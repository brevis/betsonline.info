<?php defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.helper');
require_once(JPATH_COMPONENT.'/controller.php');

$controller = new ExtraregistrationController();
$controller->execute(JRequest::getVar('task', null, 'default', 'cmd'));
$controller->redirect();
