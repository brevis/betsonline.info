<?php

define( '_JEXEC', 1 );


// Require the base controller
if(!defined('DS')){
    define('DS',DIRECTORY_SEPARATOR);
}

define('JPATH_BASE', substr(__FILE__,0,strrpos(__FILE__, DS."components")));

require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );

$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

$lang = JFactory::getLanguage();
$lang->load('com_extraregistration');
$language_tag = 'ru-RU';
$lang->load('com_extraregistration', JPATH_ADMINISTRATOR, $language_tag, true);

function GetOption($option)
{
	$db = JFactory::getDBO();
	$query = "select value from `#__extraregistration_options` where `option` = '$option'";
	$result = $db->setQuery($query);  
	$result = $db->loadResult();
	
	return $result;
}

function GetFieldValue($field_id, $uid)
{
	$db = JFactory::getDBO();
    $query = "select value from `#__extrareg_fields_values` where field_id = $field_id and uid = $uid";
	$result = $db->setQuery($query);
	return $db->loadResult();
}


$db = JFactory::getDBO();
$query = "select count(id) from `#__extraregistration_mail_history` where sent_result = 0";
$result = $db->setQuery($query);  
if($db->loadResult() == 0)
{
	return;
}

$query = "select * from `#__extraregistration_mail_history` where sent_result = 0 order by id limit 1";
$result = $db->setQuery($query);  
$user = $db->loadObject();

$query = "select * from `#__extraregistration_mail` where id = $user->mail_id";
$result = $db->setQuery($query);  
$mail = $db->loadObject();

$config = JFactory::getConfig();
$from = $config->get('mailfrom');
$fromname = $config->get('fromname');
$recipient[] = $user->email; 
$subject = $mail->title;
$body = $mail->comment;
$body = str_replace('%username%', $user->username, $body);
$mode = 1;
JFactory::getMailer()->sendMail($from, $fromname, $recipient, $subject, $body, $mode);

$query = "update `#__extraregistration_mail_history` set sent_result = 1, sent_date = '".date('Y-m-d H:i:s')."' where id = $user->id";
$result = $db->setQuery($query);  
$result = $db->execute();  

echo 'done';

?>