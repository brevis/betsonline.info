function ValidateNumber(a)
{
	b = '';
	c = '0123456789.,';
	
	for(i=0; i<a.value.length; i++)
	{
		if (c.indexOf(a.value[i])!=-1) {b=b+a.value[i]}
	}
	a.value=b.split(',').join('.');
	if (a.value.split('.').length>2)
	{
		b=a.value.split('.'); 
		a.value=b[0]+'.'+b[1]
	}	
}

function ValidatePass(id)
{
	f1 = 'field_'+id+'-1';
	f2 = 'field_'+id+'-2';
	p1 = document.getElementById(f1).value;
	p2 = document.getElementById(f2).value;
	if(p1 != p2)
	{
		document.getElementById('field_'+id+'-1').style.borderColor = 'red';
		document.getElementById('field_'+id+'-2').style.borderColor = 'red';
	}else
	{
		document.getElementById('field_'+id+'-1').style.borderColor = 'green';
		document.getElementById('field_'+id+'-2').style.borderColor = 'green';
	}
	
}

function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}
 
function ValidateUser(a, fieldid, rq)
{
	var username = a.value;
	var req = getXmlHttp();
	rq.onreadystatechange = function() {  
        if (req.readyState == 4) { 
            if(req.status == 200) { 
					var newElement = document.getElementById('img_'+fieldid);
				}
 				newElement.innerHTML = req.responseText;
				if(rq == 1 && req.responseText != "<img src='/administrator/templates/bluestork/images/admin/icon-16-allow.png'>")
				{
					document.getElementById('field_'+fieldid).value = '';
				}
            }
        }
	req.open('GET', 'index.php?option=com_extraregistration&task=checkuser&view=ajax&tmpl=component&format=raw&username='+username, true);  	
	req.send(null);
} 
  
function ajaxfunction(num, fieldid) {
    var req = getXmlHttp()  
	if(num == null)
	{
		var id = document.getElementById('country_'+fieldid).value;
	}
	if(num == 1)
	{
		var id = document.getElementById('region_'+fieldid).value;
	}
 
    req.onreadystatechange = function() {  
 
        if (req.readyState == 4) { 
            if(req.status == 200) { 
				if(num == null)
				{
					var newElement = document.getElementById('sregion_'+fieldid);
				}
				if(num == 1)
				{
					var newElement = document.getElementById('scity_'+fieldid);
				}
 				newElement.innerHTML = req.responseText
            }
        }
 
    }
	if(num == null)
	{   
		req.open('GET', 'index.php?option=com_extraregistration&task=getregions&view=ajax&tmpl=component&format=raw&id='+id+'&fieldid='+fieldid, true);  
	}
	if(num == 1)
	{
		req.open('GET', 'index.php?option=com_extraregistration&task=getcities&view=ajax&tmpl=component&format=raw&id='+id+'&fieldid='+fieldid, true);  
	}
 
    req.send(null);  // отослать запрос
}

function CheckNumber(ctrl, id)
{
	val = ctrl.value;
	spn = document.getElementById('check_'+id);
	spn.display = 'block';
	ctrl.readOnly = true;
	var req = getXmlHttp()  ;
	req.onreadystatechange = function() {   
        if (req.readyState == 4) { 
            if(req.status == 200) { 
					var newElement = document.getElementById('check_'+id);
				newElement.innerHTML = req.responseText
            }
        }
     }
	req.open('GET', 'index.php?option=com_extraregistration&task=phone&view=ajax&tmpl=component&format=raw&id='+id+'&phone='+val, true);  
	req.send(null);
}

function setupCalendar(selector) {
	jQuery(selector).each(function(){
		Calendar.setup({
			inputField  : jQuery(this).attr('id'),
			ifFormat    : "%Y-%m-%d",
			button      : jQuery(this).attr('id') + '_img',
			align       : "Tl",
			singleClick : true
		});
	});
}
