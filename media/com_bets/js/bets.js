jQuery(function($) {
	$(document).ready(function(){

		// additional bets
		$('.toggleadtbet').on('click', function(e){
			var $btn = $(this),
				bet = $btn.data('bet'),
				$container = $('#collapse_grid_row_' + bet);

			if ($container.length > 0) {
				if ($container.css('display') == 'none') {
					$container.show();
					$btn.find('span').css('transform', 'rotate(90deg)');
				} else {
					$container.hide();
					$btn.find('span').css('transform', 'rotate(0deg)');
				}
				return false;
			}

			$.ajax({
				url: Bets.adtbetsUrl,
				data: {bet: bet},
				dataType: 'html'
			}).done(function(html) {
				$btn.find('span').css('transform', 'rotate(90deg)');
				$(html).insertAfter('#grid_row_' + bet);
				$('#collapse_grid_row_' + bet).css('display', 'table-row');
			});

			e.stopPropagation();
			e.preventDefault();
			return false;
		});

		$('.adtresults').on('click', function(e){
			var $btn = $(this),
				bet = $btn.data('bet'),
				$container = $('#collapse_info_grid_row_' + bet);

			if ($container.length > 0) {
				if ($container.css('display') == 'none') {
					$container.show();
					$btn.find('span').css('transform', 'rotate(90deg)');
				} else {
					$container.hide();
					$btn.find('span').css('transform', 'rotate(0deg)');
				}
				return false;
			}

			$.ajax({
				url: Bets.adtresultsUrl,
				data: {bet: bet},
				dataType: 'html'
			}).done(function(html) {
				$btn.find('span').css('transform', 'rotate(90deg)');
				$(html).insertAfter('#grid_row_' + bet);
				$('#collapse_info_grid_row_' + bet).css('display', 'table-row');
			});

			return false;
		});

		// load type -> sections
		$('#section').change(function(){
			var $hdnSection = $('#hdnSection');
			$hdnSection.val(($(this).val() || []).join());
		});

		$('#type').change(function(){
			var $type = $('#type'),
				$section = $('#section'),
				$hdnSection = $('#hdnSection'),
				selectedSections = $hdnSection.val();

			$section.find('option').remove();

			if (($type.val() || []).length < 1) {
				return;
			}

			$section.append('<option value="">...</option>');
			$.ajax({
				url: Bets.getSectionsUrl,
				method: 'POST',
				data: {
					type: ($type.val() || []).join(),
					selectedSections: selectedSections
				},
				dataType: 'html'
			}).done(function(html) {
				$section.find('option').remove();
				$section.append(html);
			});


		}).trigger('change');

		// filter form submit
		$('#resultsFilterForm').submit(function(){
			$('#hdnDate').val($('#datetimepicker').data('date'));

			$('#hdnType').val(($('#type').val() || []).join());

			$('#hdnSection').val(($('#section').val() || []).join());

			var statuses = [];
			for(var i=1; i<=3; i++) {
				var $status = $('#status_' + i);
				if ($status.prop('checked')) {
					statuses.push($status.val());
				}
			}
			$('#hdnStatus').val(statuses.join());
		});

		// make bet link
		$('body').on('click', 'a.makebet', function(){
			var href = $(this).attr('href') + '&popup=1',
				w = 660,
				h = 400,
				left = (screen.width/2)-(w/2),
				top = (screen.height/2)-(h/2);

			window.open(href, 'makebet', 'width='+w+', height='+h+', top='+top+', left='+left);
			return false;
		});

	});
}(jQuery));