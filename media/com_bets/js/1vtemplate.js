jQuery(document).ready(function(){
	/* Parallax sript */
	jQuerywindow = jQuery(window); // cache the window object
	jQuery('.parallax_wrapper[data-type="background"]').each(function(){
		var jQueryscroll = jQuery(this); // declare the variable to affect the defined data-type
		jQuery(window).scroll(function(){
			var yPos = -(jQuerywindow.scrollTop() / jQueryscroll.data('speed')); // HTML5 proves useful for helping with creating JS functions! Also, negative value because we're scrolling upwards
			var coords = '50% '+ yPos + 'px'; // background position
			jQueryscroll.css({ backgroundPosition: coords }); // move the background
		}); // end window scroll
	}); // end section function
	document.createElement("section"); // initialization for earlier IE
	/* End of Parallax sript */

	/* Bootstrap 3 Carousel for video */
	jQuery('#bs3_video_carousel').carousel({
		pause:true,
		interval:false
	});
	/* End of Bootstrap 3 Carousel for video */

	/* Bootstrap 3 Responsive tables hint */
	jQuery('.table-responsive').each(function(){
		if(this.offsetWidth < this.scrollWidth){
			jQuery('.table-responsive_hint').removeClass('hidden');
			jQuery('.table-responsive_hint').addClass('vsbl_block');
		}else{
			jQuery('.table-responsive_hint').addClass('hidden');
			jQuery('.table-responsive_hint').removeClass('vsbl_block');
		}
	});
	/* End of Bootstrap 3 Responsive tables hint */

	/* Bootstrap 3 - Input file hack */
	jQuery('.btn-file :file').on('fileselect', function(event, numFiles, label) {
		console.log(numFiles);
		console.log(label);
	});
	/* End of Bootstrap 3 - Input file hack */
});
/* Bootstrap 3 Responsive tables hint */
jQuery(window).resize(function(){
	jQuery('.table-responsive').each(function(){
		if(this.offsetWidth < this.scrollWidth){
			jQuery('.table-responsive_hint').removeClass('hidden');
			jQuery('.table-responsive_hint').addClass('vsbl_block');
		}else{
			jQuery('.table-responsive_hint').addClass('hidden');
			jQuery('.table-responsive_hint').removeClass('vsbl_block');
		}
	});
});
/* End of Bootstrap 3 Responsive tables hint */

/* Bootstrap 3 - Input file hack */
jQuery(document).on('change', '.btn-file :file', function() {
	var input = jQuery(this),
		numFiles = input.get(0).files ? input.get(0).files.length : 1,
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});
/* End of Bootstrap 3 - Input file hack */