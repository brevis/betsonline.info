<?
/*
	ExchangMoney_Merchant class

	Last update 2010.04.07

	��������� ExchangeMoney Merchant �� ����� �����.
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	�������� ����� em_merchant.inc.php � em_merchant.site.php � ���� �����.

	��������� em_merchant.site.php � ����������� �����������

	function On_Payment_Success($order_nr, $amount, $currency, $description, $info, $params)
	{
	 	���������� ��� �������� ����������� �������� ������.
	 	���������� ���������������� �����, ���������� �����...

	 	��������� ��������� ��-�� ��������, ��� ���� �������� ����� ����� GetFormHtml().

		� $params ��������� ������ ���������� ������� (��������� ���� - ��������� ���������)
			'MERCHANT_ID'
			'ORDER_NR'
			'AMOUNT'
			'CURRENCY'
			'SYSTEM'
			'DESC'
			'INVOICE_ID'
			'INVOICE_TIME'
			'TRANSFER_ID'
			'TRANSFER_TIME'
			'STATUS'

	}

	function On_Payment_Cancel($order_nr, $amount, $currency, $description, $info, $params)
	{
	 	���������� � ������ ������ ������� ��� ������������� ������ �� ����� ������.
	 	������������ (���� �����) ���� ��������� ��������
	}

	function Open_Order_Page()
	{
		���������� ���������� �������� � ������� ������.

		��� ������������ HTML ���� � ������� ����������� �����
			$this->GetFormHtml($order_nr, $amount, $currency, $description, $info=array(), $html='');

		���� ��������� ��� ������ GetFormHtml ���������� � On_Payment_Success � On_Payment_Cancel
		�����������.
	}

	function Open_Success_Page()
	{
		���������� ���������� �������� � ���������� �� ������� ����������� ������.
	}

	function Open_Cancel_Page()
	{
		���������� ���������� �������� � ���������� �� ������ �������� ������.
	}

*/

class EM_Merchant
{
	var $merchantUrl			= 'https://www.exchang.ru/merchant/payment';
	var $merchantId 			= '';
	var $merchantSign 			= '';
	var $paymentSystems			= '';		// 'EM;PP;WM;MM;RBK;MB;LP;...'
	var $site_answer_word		= 'OK';
	var $site_charset			= 'UTF-8';
	var $site_currency			= 'RUR';	// may be null

	// urls started with '/' will be completed automaticaly by $site_base_url
	var $site_result_url		= null;		// Will be filled automaticaly. eg '/scripts/em_result.php';
	var $site_result_method		= 'POST';
	var $site_success_url		= null;		// Will be filled automaticaly. eg '/scripts/success.php';
	var $site_success_method	= 'POST';
	var $site_cancel_url		= null;		// Will be filled automaticaly. eg '/scripts/cancel.php';
	var $site_cancel_method		= 'POST';
	var $site_base_url			= null;		// Will be filled automaticaly

	var $params_tr		= array(
	//	'merchant_field'=> 'local_field',
	//	'MERCHANT_ID'	=>'',
	//	'ORDER_NR'		=>'',
	//	'AMOUNT'		=>'',
	//	'CURRENCY'		=>'',
	//	'SYSTEM'		=>'',
	//	'DESC'			=>'',
	//	'INVOICE_ID'	=>'',
	//	'INVOICE_TIME'	=>'',	// "YYYY-MM-DD HH:MM::SS"
	//	'TRANSFER_ID'	=>'',
	//	'TRANSFER_TIME'	=>'',   // "YYYY-MM-DD HH:MM::SS"
	//	'STATUS'		=>''	// 1|0|-1
	);

	//------------------------------------------------------------------------------
	//
	function Run($action=null)
	{
		if (!isset($action)) $action = $this->bydef($_GET, '-a', 'order');

		switch($action) {
		case 'result':	$this->HandleResultRequest(); break;
		case 'order':	$this->Open_Order_Page(); break;
		case 'success':	$this->Open_Success_Page(); break;
		case 'cancel':	$this->Open_Cancel_Page(); break;

		default:
			if (method_exists($this,$f='Open_'.$action.'_Page')) {
				call_user_func_array(aray(&$this, $f), array());
			}
		}
	}
	//------------------------------------------------------------------------------
	//
	function On_Payment_Success($order_nr, $amount, $currency, $description, $info, $params)
	{

	}
	//------------------------------------------------------------------------------
	//
	function On_Payment_Cancel($order_nr, $amount, $currency, $description, $info, $params)
	{

	}
	//------------------------------------------------------------------------------
	//
	function Open_Order_Page()
	{

	}
	//------------------------------------------------------------------------------
	//
	function Open_Success_Page()
	{

	}
	//------------------------------------------------------------------------------
	//
	function Open_Cancel_Page()
	{

	}

	//------------------------------------------------------------------------------
	//
	function GetFormHtml($order_nr, $amount, $currency, $description, $info=array(), $html='')
	{
		$amount 	= sprintf('%0.2f',$amount);
		$order_nr	= $order_nr;
		$description= $description;
		$HASH 		= md5("$this->merchantId|$order_nr|$amount|$currency|$this->paymentSystems|$this->merchantSign");


		$form = '<form method="POST" action="'.$this->merchantUrl.'" target="_parent">'
			.'<input type="hidden" name="PAYMENT_MERCHANT_ID" value="'.$this->merchantId.'">'
			.'<input type="hidden" name="PAYMENT_ORDER_NR" value="'.htmlspecialchars($order_nr).'">'
			.'<input type="hidden" name="PAYMENT_AMOUNT" value="'.$amount.'">'
			.'<input type="hidden" name="PAYMENT_CURRENCY" value="'.$currency.'">'
			.'<input type="hidden" name="PAYMENT_SYSTEM" value="'.$this->paymentSystems.'">'
			.'<input type="hidden" name="PAYMENT_DESC" value="'.htmlspecialchars($description).'">'
			.'<input type="hidden" name="PAYMENT_SITE_CHARSET" value="'.$this->site_charset.'">'
			.'<input type="hidden" name="PAYMENT_SITE_CURRENCY" value="'.$this->site_currency.'">'
			.'<input type="hidden" name="PAYMENT_RESULT_URL" value="'.$this->SiteUrl($this->site_result_url ?$this->site_result_url : $_SERVER['PHP_SELF']."?-a=result").'">'
			.'<input type="hidden" name="PAYMENT_RESULT_METHOD" value="'.$this->site_result_method.'">'
			.'<input type="hidden" name="PAYMENT_SUCCESS_URL" value="'.$this->SiteUrl($this->site_success_url ?$this->site_success_url :$_SERVER['PHP_SELF']."?-a=success").'">'
			.'<input type="hidden" name="PAYMENT_SUCCESS_METHOD" value="'.$this->site_success_method.'">'
			.'<input type="hidden" name="PAYMENT_CANCEL_URL" value="'.$this->SiteUrl($this->site_cancel_url ?$this->site_cancel_url :$_SERVER['PHP_SELF']."?-a=cancel").'">'
			.'<input type="hidden" name="PAYMENT_CANCEL_METHOD" value="'.$this->site_cancel_method.'">'
			.'<input type="hidden" name="PAYMENT_HASH" value="'.$HASH.'">';

		foreach($info as $n=>$v) {
			$form .= '<input type="hidden" name="PAYMENT_INFO_'.$n.'" value="'.htmlspecialchars($v).'">';
		}
		$form .= $html.'</form>';

		return $form;
	}
	//------------------------------------------------------------------------------
	//
	function HandleResultRequest()
	{
		$PARAMS =  isset($_POST) ?$_POST :$_GET;

		if (ini_get('magic_quotes_gpc')) {
			array_walk_recursive($PARAMS, create_function('&$v,$n','$v=stripslashes($v);'));
		}

		// check request
		$REQs = array(
			'MERCHANT_ID', 'ORDER_NR', 'AMOUNT', 'CURRENCY', 'SYSTEM', 'DESC',
			'INVOICE_ID', 'INVOICE_TIME', 'TRANSFER_ID', 'TRANSFER_TIME', 'STATUS','HASH'
		);
		$reqs = $this->vget($PARAMS, $REQs);
		if (sizeof($reqs)!=sizeof($REQs))  {
			return $this->On_Request_Fail($PARAMS, 'INVALID_REQUEST');
		}
		if ($PARAMS['MERCHANT_ID'] != $this->merchantId) {
			return $this->On_Request_Fail($PARAMS, 'INVALID_MERCHANT');
		}

    	$hash = join('|',$this->vget($PARAMS,array(
    		'MERCHANT_ID','ORDER_NR','AMOUNT','CURRENCY','SYSTEM','INVOICE_ID','TRANSFER_ID'
    	)));
    	$hash.= '|'.$this->merchantSign;
		$hash	= md5($hash);

		if ($hash != $PARAMS['HASH']) {
			return $this->On_Request_Fail($params, 'INVALID_HASH');
		}

		// Prepare params
		$info 	= array();
		$params = array();
		foreach($PARAMS as $i=>$v) {
			if (substr($i,0,5)=='INFO_') {
				$info[substr($i,5)] = $v;
			} else {
				$params[isset($this->params_tr[$i]) ?$this->params_tr[$i] :$i] = $v;
			}
		}

		// Call handlers
		if ($PARAMS['STATUS']>0) {
			$this->On_Payment_Success(
				$PARAMS['ORDER_NR'], $PARAMS['AMOUNT'], $PARAMS['CURRENCY'],
				$PARAMS['DESC'], $info, $params
			);
			exit($this->site_answer_word);
		} else {
			$this->On_Payment_Cancel(
				$PARAMS['ORDER_NR'], $PARAMS['AMOUNT'], $PARAMS['CURRENCY'],
				$PARAMS['DESC'], $info, $params
			);
			exit($this->site_answer_word);
		}
	}
	//------------------------------------------------------------------------------
	//
	function On_Request_Fail($params, $code) {
		exit('FAIL');
	}

	//------------------------------------------------------------------------------
	//
	function SiteUrl($url) {

		return ($url && $url[0]=='/') ?$this->SiteBaseUrl().$url :$url;
	}
	//------------------------------------------------------------------------------
	//
	function SiteBaseUrl() {
		if (empty($this->site_base_url)) {
			$http = (empty($_SERVER['HTTPS']) || strtolower($_SERVER['HTTPS'])=='off') ?'http' :'https';
			$this->site_base_url = $http.'://'.$this->bydef($_SERVER, 'HTTP_HOST', $this->bydef($_SERVER, 'SERVER_NAME'));
		}
		return $this->site_base_url;
	}
	//------------------------------------------------------------------------------
	//
	function bydef($a, $id, $def=null) {
		return isset($a[$id]) ?$a[$id] :$def;
	}
	//------------------------------------------------------------------------------
	//
	function vget($vars, $ids, $def=null)
	{
		if (!isset($ids)) return $vars;
		if (is_array($ids)) {
			if (!isset($vars)) return null;
			$res = array();
			foreach($ids as $as=>$id) {
				if (is_scalar($id)) {
					if (isset($vars[$id]) || array_key_exists($id,$vars))
						$res[is_int($as)?$id:$as] = $vars[$id];
					else
					if (isset($def))
						$res[is_int($as)?$id:$as] = $def;
				} else
				if (is_array($id)) {
					$res[$as] = vget($vars, $id, $def);
				}
			}
			return $res;
		}
		return isset($vars[$ids]) ?$vars[$ids] :$def;
	}

}
