$(function() {

    var show = false

    var to_top_button = $('<div class="to_top" ></div>')

    $('body').append(to_top_button);

    $('.to_top').click(function(){
        $.scrollTo( $('body') , 500,  { axis: 'y' } );
    })

    $(window).scroll(function () {
        show_or_hide()
    })

    function show_or_hide(){
        if( window.pageYOffset > 400){
            if(!show){
                to_top_button.show()
                show = true
            }
        }else{
            if(show){
                to_top_button.hide()
                show = false
            }
        }
    }
    show_or_hide()

});