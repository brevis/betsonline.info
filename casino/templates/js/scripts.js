function sitebar_initCallback(carusel)
{
    $('#sitebar #but-up').bind('click', function() {
        carusel.prev();
        return false;
    });
    
    $('#sitebar #but-down').bind('click', function() {
        carusel.next();
        return false;
    });
}

function showInfo(div)
{
    $("#sitebar-carusel").css("width", "215px");
    var li = $(div).parent("li");
    $(".sitebar-info").hide();
    $(li).children(".sitebar-info").show();
}


function hideInfo(div)
{
    $("#sitebar-carusel").css("width", "77px");
    $(div).hide();
}

function ShowTab(tab)
{
    var num = $(tab).attr("data-tab");
    var parenttab = $(tab).parents(".tabs");
    
    $(parenttab).find(".tab-body").each(function(){
        $(this).removeClass("active");
    });
    
    $(".tab-body[data-tab="+num+"]").addClass("active");
    
    
    $(parenttab).find(".tab-head").each(function(){
        $(this).removeClass("active");
    });
    
    $(tab).addClass("active");
    oScrollbar.tinyscrollbar_update();
}


function loadHist(year)
{
    $('#history #hist-content').load('/lobby/ajax/hist-tour.php?'+year, function(){$('#history').show(); $('#hist-table-wrap').tinyscrollbar();});
    return false;
}

function editUserinfo()
{
    $(".tbltd-inp").each(function(){
        var text = $(this).html().replace(/(^\s+)|(\s+$)/g, "");
        $(this).attr("data-text", text)
        $(this).html("<input name='"+$(this).attr("data-name")+"' value='"+text+"'>");
        
    });
    $(".userinfo-edit").hide();
    $("#userinfo-edit").show();
}


function editUserinfoHide()
{
    $(".tbltd-inp").each(function(){
        var text = $(this).attr("data-text");
        $(this).html(text);
    });
    $(".userinfo-edit").show();
    $("#userinfo-edit").hide();
}

function reloadMailList(type, div)
{
    if (type == 'all')
    {
        $(".mail-list li").show();
    }else
    {
        $(".mail-list li").hide();
        $(".mail-list li.show-mail").show();
        $(".mail-list li.new-mail").show();
    }
    
    
    $(".viewtype-active").removeClass("viewtype-active");
    $(div).addClass("viewtype-active");
    $('#mail-list').tinyscrollbar();
}


function showMail(li)
{
    $("#mail-list .show-mail").removeClass("show-mail");
    $(li).addClass("show-mail").removeClass("new-mail").css("display", "block");
}


function openKassa()
{

    if ( jQuery.browser.msie && jQuery.browser.version == '7.0') { 
        alert("���������� �������� ���� �������!"); 
        return;
    }

    if (!$("#kassa-dialog").html())
    {
        $("#middle-content").append("<div id='kassa-dialog'></div>");
    }
    
    $("#kassa-dialog").load("/lobby/ajax/kassa.php",
                            function(){
                                $("#kassa-dialog").show();
                                $(".tab-head").click(function(){ShowTab(this);});
                                oScrollbar = $('#hist-table-wrap2');
                                oScrollbar.tinyscrollbar();
                                
                            });
}


function saveChPass()
{
    var pass = $("#password").val();
    var pass2 = $("#password2").val();
    
    
    if (pass != pass2)
    {
        alert("�������� ������ �� ���������");
    } else {
	
		$.ajax({
			type: 'POST',
			url:'/lobby/profile.php',
			data: {
				'action': 'password',
				'value': [pass, pass2]
			},
			success:function(data){
                            if (data == '������ ��������!')
                            {
				$('#pass_change').toggle('quick');
                                $('#pass_change input').val('');
                            }
                            alert(data);
			}
		});
	
	}
    
}

function AddFun()
{
    $.ajax({
    cache: false,
    url: '/lobby/actions.php',
    data: {
        'action': 'fun'
    },
    success: function(response) {
        if (response) {
            if (response.substring(0, 2) == 'ok') {
                $('.funcash').text(response.substring(2));
                $('.funcash_show').text(response.substring(2));
                alert('������� ���� ��������');
            } else {
                alert('������� ���� ����� ��������� ������ ���� ��� � �����.');
            }
        } else {
            alert('������ �� �������. ���������� �����');
        }
    }
    });
}


function getCounters()
{
    $.ajax({
	url: "/templates/counters.html",
	cache: false,
	success: function(html){
	    $("body").append(html);
	}
    });

}

function color1()
{
    $("#newkassa2").stop().animate({ backgroundColor: "#1f8930"}, 1500, function(){color2()});
}

function color2()
{
    $("#newkassa2").stop().animate({ backgroundColor: "#2ab542"}, 1500, function(){color1()});
}

var oScrollbar;
$(document).ready(function(){

    
   // setTimeout(getCounters, 2000);
    
    color1();
    
    $(".tab-head").click(function(){ShowTab(this);});
    
    
    
    $(".tur-prizi .arrow").click(function(){$(this).parent(".tur-prizi").toggleClass("tur-prizi-show")});
    
    //$('#mail-list').tinyscrollbar();
    
    $('#mail-list .mail-list li').click(function(li){showMail(this)});
    
    
    $("#kassa").hover(function(){$("#newkassa2").stop().css("background-color", "#2ab542")}, function(){color1()})
    
    $("#kassa").click(function(){openKassa();});
    
    /*
    $('.sel_imul').live('click', function() {
        $('.sel_imul').removeClass('act');
        $(this).addClass('act');
        if ($(this).children('.sel_options').is(':visible')) {
            $('.sel_options').hide();
        }
        else {
            $('.sel_options').hide();
            $(this).children('.sel_options').show();
        }
    });
    
    $('.sel_option').live('click', function() {
        var tektext = $(this).html();
        $(this).parent('.sel_options').parent('.sel_imul').children('.sel_selected').children('.selected-text').html(tektext);
        $(this).parent('.sel_options').children('.sel_option').removeClass('sel_ed');
        $(this).addClass('sel_ed');
        
    });*/
    //$(".game-hall img").attr('is_b', 0);
    //$(".game-hall img").mouseenter (function() {
    //  
    //    //$(this).stop(function(){
    //        console.log('stop calback')
    //        if ($(this).attr('is_b')==0 || $(this).attr('is_b')=='undefined')
    //        {
    //            $(this).effect('bounce',300, function(){$(this).attr('is_b', 1)});
    //            console.log("in")
    //        }
    //    //});
    //    
    //});
    //
    //$(".game-hall img").mouseleave(function(){
    //        $(this).attr('is_b', 0)
    //        $(this).stop();
    //        console.log("out")
    //    });
    
    $(".game-hall img").jrumble({
	x: 1,
	y: 2,
	rotation: 1,
        speed: 55
    });
    
    var time_zalip;
    
    $(".game-hall img").hover(function(){
	$(this).trigger('startRumble');
        time_zalip = setTimeout(Zalip, 5000, $(this));
    }, function(){
            $(this).trigger('stopRumble');
            clearTimeout(time_zalip);
    });
    
    $(".game-hall li").mousemove(function(){$(".zalip").hide();});
    
    
    $("#balans #up div:first, #balans #down div:first").click(function(){openKassa()});
    
    $("#balans #up div:eq(1), #balans #down>div:eq(1)").click(function(){location.href = '/lobby/shop.php'; });
});

shuffle = function(o){ //v1.0
	for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
};

function Zalip(event)
{
    var coords = $(event).offset();
    var alerts = shuffle(['���� �����? ���!', '���� �� ��� ������ ������, ������ - ������ ����������!', '�� ������ ������� ����? �������� ���!', '���� �� ��������� ���� - ���� ������� �������!',]);
    
    console.log (coords);
    
    $(".zalip p").html(alerts[1]);
    $(".zalip").css("top", coords.top).css("left", coords.left - 100).show();
}