$(function(){

  var gamelist = $('#gamelist');
  
  gamelist.find('.item-layer').hover(function(){
  
    $(this)
      .find('.item-controls-layer')
      .stop()
      .animate({'top': 0}, 100);
  
  },
  function(){
  
    $(this)
      .find('.item-controls-layer')
      .stop()
      .animate({'top': 165}, 150);
  
  })
  .find('.values li')
  .click(function(){
  
    var index = $(this).index();
    
    gamelist.find('.item-layer .values').each(function(){
    
      var el = $(this).find('input');
      
      el.removeAttr('checked')
      el.eq(index).attr('checked', true);
      setCookie('denomination', index);
      
    });
   
    
  });
  
  $('#group-list a').click(function(){
    
    if($(this).hasClass('active')) return false;
    
    $(this).parent().find('a').removeClass('active');
    $(this).addClass('active');
    
    var group = $(this).attr('id');
    
    setCookie('group', group);
    
    gamelist.animate({'opacity': 0}, 150, function(){
    
      gamelist.animate({'opacity': 1}, 100);
      if(group == 'group-all'){
        gamelist.find('.item-layer').show();
        return false;
      }
      
      gamelist.find('.item-layer').hide();
      gamelist.find('.' + group).show();
      
      
    });
    
    return false;
  });

});