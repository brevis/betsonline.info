-- MySQL dump 10.13  Distrib 5.5.23, for Linux (x86_64)
--
-- Host: localhost    Database: 1cazino
-- ------------------------------------------------------
-- Server version	5.5.23-cll

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admiral_gm`
--

DROP TABLE IF EXISTS `admiral_gm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admiral_gm` (
  `g_name` varchar(20) NOT NULL DEFAULT '',
  `g_bank` decimal(20,2) NOT NULL DEFAULT '0.00',
  `g_proc` decimal(3,0) NOT NULL DEFAULT '100',
  `g_shansbon` varchar(10) NOT NULL DEFAULT '',
  `g_shanswin` text NOT NULL,
  `g_rezerv` varchar(5) NOT NULL DEFAULT '10',
  `g_rezim` char(1) NOT NULL DEFAULT '2'
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admiral_gm`
--

LOCK TABLES `admiral_gm` WRITE;
/*!40000 ALTER TABLE `admiral_gm` DISABLE KEYS */;
INSERT INTO `admiral_gm` VALUES ('attila',30250.04,99,'|||','25|25|25|25|25|','3','5'),('aztectreasure',183.07,99,'|||','25|25|25|25|25|','2','5'),('bananasplash',255.96,99,'|||','25|25|25|25|25|','3','5'),('bananas',261.30,99,'|||','25|25|25|25|25|','3','5'),('bookofra',58.31,99,'|||','25|25|25|25|25|','3','5'),('columbus',260.24,99,'|||','25|25|25|25|25|','3','5'),('columbusdeluxe',19.95,99,'|||','25|25|25|25|25|','3','5'),('dolphins',15139.52,99,'|||','25|25|25|25|25|','3','5'),('dynasty',10.75,99,'|||','25|25|25|25|25|','3','5'),('goldenplanet',34.67,99,'|||','25|25|25|25|25|','3','5'),('gryphonsgold',53.05,99,'|||','25|25|25|25|25|','3','5'),('kingofcards',340.39,99,'|||','25|25|25|25|25|','3','5'),('luckylady',42.22,99,'|||','25|25|25|25|25|','3','5'),('magicprincess',408.14,99,'|||','25|25|25|25|25|','2','5'),('markopolo',43.65,99,'|||','25|25|25|25|25|','3','5'),('mermaidspearl',27.72,99,'|||','25|25|25|25|25|','2','5'),('moneygame',400.29,99,'|||','25|25|25|25|25|','2','5'),('panthermoon',278.51,99,'|||','25|25|25|25|25|','2','5'),('pharaonsgoldll',68.43,99,'|||','25|25|25|25|25|','2','5'),('pharaonsgoldlll',298.73,99,'|||','25|25|25|25|25|','3','5'),('polarfox',2798.35,99,'|||','25|25|25|25|25|','3','5'),('queenof',2515.48,99,'|||','25|25|25|25|25|','3','5'),('ramsesll',1418.07,99,'|||','25|25|25|25|25|','2','5'),('royaltreasures',10.00,99,'|||','6|6|6|4|3|','3','5'),('safariheat',179.35,99,'|||','25|25|25|25|25|','2','5'),('secretforest',90.57,99,'|||','25|25|25|25|25|','3','5'),('sharky',37.18,99,'|||','25|25|25|25|25|','3','5'),('sparta',24.71,99,'|||','25|25|25|25|25|','3','5'),('unicorn',6.93,99,'|||','25|25|25|25|25|','3','5'),('wonderful',40.75,99,'|||','25|25|25|25|25|','3','5'),('attila',30250.04,99,'|||','25|25|25|25|25|','3','5'),('aztectreasure',183.07,99,'|||','25|25|25|25|25|','2','5'),('bananasplash',255.96,99,'|||','25|25|25|25|25|','3','5'),('bananas',261.30,99,'|||','25|25|25|25|25|','3','5'),('bookofra',58.31,99,'|||','25|25|25|25|25|','3','5'),('columbus',260.24,99,'|||','25|25|25|25|25|','3','5'),('columbusdeluxe',19.95,99,'|||','25|25|25|25|25|','3','5'),('dolphins',15139.52,99,'|||','25|25|25|25|25|','3','5'),('dynasty',10.75,99,'|||','25|25|25|25|25|','3','5'),('goldenplanet',34.67,99,'|||','25|25|25|25|25|','3','5'),('gryphonsgold',53.05,99,'|||','25|25|25|25|25|','3','5'),('kingofcards',340.39,99,'|||','25|25|25|25|25|','3','5'),('luckylady',42.22,99,'|||','25|25|25|25|25|','3','5'),('magicprincess',408.14,99,'|||','25|25|25|25|25|','2','5'),('markopolo',43.65,99,'|||','25|25|25|25|25|','3','5'),('mermaidspearl',27.72,99,'|||','25|25|25|25|25|','2','5'),('moneygame',400.29,99,'|||','25|25|25|25|25|','2','5'),('panthermoon',278.51,99,'|||','25|25|25|25|25|','2','5'),('pharaonsgoldll',68.43,99,'|||','25|25|25|25|25|','2','5'),('pharaonsgoldlll',298.73,99,'|||','25|25|25|25|25|','3','5'),('polarfox',2958.35,99,'|||','25|25|25|25|25|','3','5'),('queenof',2515.48,99,'|||','25|25|25|25|25|','3','5'),('ramsesll',1418.07,99,'|||','25|25|25|25|25|','2','5'),('royaltreasures',10.00,99,'|||','6|6|6|4|3|','3','5'),('safariheat',179.35,99,'|||','25|25|25|25|25|','2','5'),('secretforest',90.57,99,'|||','25|25|25|25|25|','3','5'),('sharky',37.18,99,'|||','25|25|25|25|25|','3','5'),('sparta',24.71,99,'|||','25|25|25|25|25|','3','5'),('unicorn',6.93,99,'|||','25|25|25|25|25|','3','5'),('wonderful',40.75,99,'|||','25|25|25|25|25|','3','5');
/*!40000 ALTER TABLE `admiral_gm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crzsettings`
--

DROP TABLE IF EXISTS `crzsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crzsettings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL DEFAULT '',
  `value` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crzsettings`
--

LOCK TABLES `crzsettings` WRITE;
/*!40000 ALTER TABLE `crzsettings` DISABLE KEYS */;
INSERT INTO `crzsettings` VALUES (1,'bonus_freq','50'),(2,'banana_freq','50'),(3,'bonus_min','22'),(4,'bonus_max','50'),(5,'bonus2_50','50'),(6,'bonus2_100','100'),(7,'bonus2_150','150'),(8,'bonus2_200','200'),(9,'bonus2_250','250'),(11,'wl,1,1','500'),(12,'wl,1,2','400'),(13,'wl,1,3','500'),(14,'wl,2,1','800'),(15,'wl,2,2','900'),(16,'wl,2,3','1000'),(17,'wl,3,1','300'),(18,'wl,3,2','800'),(19,'wl,3,3','900'),(20,'wl,4,1','900'),(21,'wl,4,2','1000'),(22,'wl,4,3','900'),(23,'wl,5,1','900'),(24,'wl,5,2','170'),(25,'wl,5,3','300'),(26,'wl,6,1','60'),(27,'wl,6,2','70'),(28,'wl,6,3','80'),(29,'wl,7,1','70'),(30,'wl,7,2','90'),(31,'wl,7,3','100'),(32,'wl,8,1','70'),(33,'wl,8,2','123'),(34,'wl,8,3','345'),(35,'wl,9,1','50'),(36,'wl,9,2','51'),(37,'wl,9,3','52'),(38,'double_freq','7'),(39,'get_more_than_bet_freq','52'),(40,'max_part_of_bank_at_once','7'),(41,'max_gain_at_once','52'),(42,'max_gain_in_double','7'),(43,'max_gain_on_rope','52'),(44,'max_super_prize','7');
/*!40000 ALTER TABLE `crzsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `g_set_new`
--

DROP TABLE IF EXISTS `g_set_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `g_set_new` (
  `g_name` varchar(20) NOT NULL DEFAULT '',
  `g_bank` decimal(20,2) NOT NULL DEFAULT '0.00',
  `g_proc` decimal(3,0) NOT NULL DEFAULT '100',
  `g_shansbon` text NOT NULL,
  `g_shanswin` text NOT NULL,
  `g_rezerv` varchar(5) NOT NULL DEFAULT '10',
  `g_rezim` char(1) NOT NULL DEFAULT '2'
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `g_set_new`
--

LOCK TABLES `g_set_new` WRITE;
/*!40000 ALTER TABLE `g_set_new` DISABLE KEYS */;
INSERT INTO `g_set_new` VALUES ('keks',11.11,99,'17||10|','9|8|7|6|5|','4','1'),('rezident',279.00,50,'230|210|150|','9|8|7|6|5|','4','1'),('chukcha',10462.12,99,'17|15|15|','8|7|7|8|8|','4','1'),('fairy_land',125.68,99,'17','9|5|7|6|5|','4','1'),('lucky_drink',109.51,99,'45','9|8|7|6|5|','4','1'),('lucky_haunter',183.16,99,'65','9|7|7|6|5|','4','1'),('rockclimber',120.16,10,'54|||','9|6|7|6|4|','4','1'),('cocktail',894.19,99,'55','15|6|6|5|4|','3','1');
/*!40000 ALTER TABLE `g_set_new` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `g_set_new_cr`
--

DROP TABLE IF EXISTS `g_set_new_cr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `g_set_new_cr` (
  `g_name` varchar(20) NOT NULL DEFAULT '',
  `g_bank` decimal(20,2) NOT NULL DEFAULT '0.00',
  `g_proc` decimal(3,0) NOT NULL DEFAULT '100',
  `g_shansbon` varchar(10) NOT NULL DEFAULT '',
  `g_shanswin` text NOT NULL,
  `g_rezerv` varchar(5) NOT NULL DEFAULT '10',
  `g_rezim` char(1) NOT NULL DEFAULT '2',
  `g_cask` decimal(20,2) NOT NULL DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `g_set_new_cr`
--

LOCK TABLES `g_set_new_cr` WRITE;
/*!40000 ALTER TABLE `g_set_new_cr` DISABLE KEYS */;
INSERT INTO `g_set_new_cr` VALUES ('crmonkey',140.91,99,'1000','25|25|25|25|25|','3','1',15.00),('gnome',104.70,99,'17','10|9|8|8|8|','4','1',15.00);
/*!40000 ALTER TABLE `g_set_new_cr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `g_set_new_g`
--

DROP TABLE IF EXISTS `g_set_new_g`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `g_set_new_g` (
  `g_name` varchar(20) NOT NULL DEFAULT '',
  `g_bank` decimal(20,2) NOT NULL DEFAULT '0.00',
  `g_proc` decimal(3,0) NOT NULL DEFAULT '100',
  `g_shansbon` text NOT NULL,
  `g_shanswin` text NOT NULL,
  `g_rezerv` varchar(5) NOT NULL DEFAULT '10',
  `g_rezim` char(1) NOT NULL DEFAULT '2',
  `g_bon1_1` varchar(7) NOT NULL DEFAULT '0',
  `g_bon1_2` varchar(7) NOT NULL DEFAULT '0',
  `g_bon1_3` varchar(7) NOT NULL DEFAULT '0',
  `g_bon1` varchar(7) NOT NULL DEFAULT '0',
  `g_bon2` varchar(7) NOT NULL DEFAULT '0',
  `g_bon3` varchar(7) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `g_set_new_g`
--

LOCK TABLES `g_set_new_g` WRITE;
/*!40000 ALTER TABLE `g_set_new_g` DISABLE KEYS */;
INSERT INTO `g_set_new_g` VALUES ('garage',5726.40,70,'2|10|2|','6|5|7|6|5|','4','1','0','0','0','0','0','0');
/*!40000 ALTER TABLE `g_set_new_g` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_bank`
--

DROP TABLE IF EXISTS `game_bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_bank` (
  `name` varchar(10) NOT NULL DEFAULT 'ttuz',
  `bank` decimal(12,2) NOT NULL DEFAULT '0.00',
  `proc` decimal(3,0) NOT NULL DEFAULT '0',
  `winmax` decimal(12,2) NOT NULL DEFAULT '0.00',
  `income` decimal(12,2) NOT NULL DEFAULT '0.00',
  `bonus` decimal(12,2) NOT NULL DEFAULT '0.00',
  `bonus2` decimal(12,2) NOT NULL DEFAULT '0.00',
  `bonusready` decimal(12,2) NOT NULL DEFAULT '0.00',
  `bonusproc` decimal(12,2) NOT NULL DEFAULT '0.00',
  `bonusmode` decimal(12,2) NOT NULL DEFAULT '0.00',
  `jackpot` decimal(12,2) NOT NULL DEFAULT '0.00',
  `jpotproc` decimal(12,2) NOT NULL DEFAULT '0.00',
  `mode` varchar(128) DEFAULT NULL,
  `mode2` varchar(128) DEFAULT NULL,
  `mode3` varchar(128) DEFAULT NULL,
  `mode4` varchar(128) DEFAULT NULL,
  `incash` decimal(12,2) NOT NULL DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_bank`
--

LOCK TABLES `game_bank` WRITE;
/*!40000 ALTER TABLE `game_bank` DISABLE KEYS */;
INSERT INTO `game_bank` VALUES ('ttuz',0.00,0,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,NULL,NULL,0.00);
/*!40000 ALTER TABLE `game_bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_sessions`
--

DROP TABLE IF EXISTS `game_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_sessions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(12) DEFAULT NULL,
  `state` varchar(250) DEFAULT NULL,
  `cr_tstamp` datetime DEFAULT NULL,
  `cashin` decimal(12,2) DEFAULT NULL,
  `cashchange` decimal(12,2) DEFAULT NULL,
  `lastact_tstamp` datetime DEFAULT NULL,
  `gameid` bigint(20) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1366 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_sessions`
--

LOCK TABLES `game_sessions` WRITE;
/*!40000 ALTER TABLE `game_sessions` DISABLE KEYS */;
INSERT INTO `game_sessions` VALUES (1364,'admin','1,0,9','2012-01-17 01:16:54',10940.00,-88.00,'2012-01-17 01:17:45',1,0),(1365,'admin','1,0,1','2012-01-19 09:20:57',14209.70,-5.00,'2012-01-19 09:21:06',1,1);
/*!40000 ALTER TABLE `game_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` varchar(8) NOT NULL DEFAULT '',
  `news` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'18.01.12','Партнерская программа нашего интернет-казино 50%!!!'),(11,'25.01.13','Сделай депозит на 500руб и получи БОНУС!!!'),(12,'27.01.13','Только в нашем казино партнёрская программа 50%');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner` (
  `pus` varchar(50) DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `data` varchar(8) DEFAULT NULL,
  `cash` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pin`
--

DROP TABLE IF EXISTS `pin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pin` (
  `code` varchar(30) DEFAULT NULL,
  `data` varchar(30) DEFAULT NULL,
  `summa` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pin`
--

LOCK TABLES `pin` WRITE;
/*!40000 ALTER TABLE `pin` DISABLE KEYS */;
/*!40000 ALTER TABLE `pin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seting`
--

DROP TABLE IF EXISTS `seting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seting` (
  `alog` varchar(10) NOT NULL DEFAULT 'admin',
  `apas` varchar(10) NOT NULL DEFAULT 'admin',
  `adm_email` varchar(200) NOT NULL,
  `cas_url` varchar(200) NOT NULL,
  `cas_name` varchar(40) NOT NULL DEFAULT '',
  `pcash` char(3) NOT NULL DEFAULT '20',
  `paymail` char(3) NOT NULL DEFAULT 'yes',
  `regmail` char(3) NOT NULL DEFAULT 'yes',
  `zakmail` char(3) NOT NULL DEFAULT 'yes',
  `icq` varchar(10) NOT NULL DEFAULT '',
  `cas_bon` char(1) NOT NULL DEFAULT '0',
  `mrh_login` varchar(250) NOT NULL,
  `mrh_pass1` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seting`
--

LOCK TABLES `seting` WRITE;
/*!40000 ALTER TABLE `seting` DISABLE KEYS */;
INSERT INTO `seting` VALUES ('admin','admin','jskript@mail.ru','http://ToGeTo.Ru','ToGeTo.Ru','50','yes','yes','yes','1245','','','');
/*!40000 ALTER TABLE `seting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stat_game`
--

DROP TABLE IF EXISTS `stat_game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stat_game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` varchar(8) NOT NULL DEFAULT '',
  `vrem` time NOT NULL DEFAULT '00:00:00',
  `login` varchar(20) NOT NULL DEFAULT '',
  `balans` varchar(10) NOT NULL DEFAULT '',
  `stav` char(3) NOT NULL DEFAULT '',
  `win` varchar(6) NOT NULL DEFAULT '',
  `game` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14669 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stat_game`
--

LOCK TABLES `stat_game` WRITE;
/*!40000 ALTER TABLE `stat_game` DISABLE KEYS */;
/*!40000 ALTER TABLE `stat_game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stat_pay`
--

DROP TABLE IF EXISTS `stat_pay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stat_pay` (
  `user` varchar(20) NOT NULL DEFAULT '',
  `data` varchar(8) NOT NULL DEFAULT '',
  `vremya` time NOT NULL DEFAULT '00:00:00',
  `inm` varchar(12) NOT NULL DEFAULT '',
  `outm` varchar(12) NOT NULL DEFAULT '',
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stat_pay`
--

LOCK TABLES `stat_pay` WRITE;
/*!40000 ALTER TABLE `stat_pay` DISABLE KEYS */;
/*!40000 ALTER TABLE `stat_pay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_pin`
--

DROP TABLE IF EXISTS `stats_pin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_pin` (
  `code` varchar(40) NOT NULL DEFAULT '',
  `data` varchar(30) DEFAULT NULL,
  `summa` varchar(30) DEFAULT NULL,
  `login` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_pin`
--

LOCK TABLES `stats_pin` WRITE;
/*!40000 ALTER TABLE `stats_pin` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_pin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(12) DEFAULT NULL,
  `pass` varchar(12) DEFAULT NULL,
  `cash` decimal(12,2) DEFAULT '0.00',
  `cashin` decimal(12,2) DEFAULT '0.00',
  `cashout` decimal(12,2) DEFAULT '0.00',
  `email` varchar(50) DEFAULT NULL,
  `WMR` varchar(50) DEFAULT NULL,
  `WMID` varchar(50) DEFAULT NULL,
  `date` varchar(12) DEFAULT NULL,
  `pcash` varchar(6) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=219 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (205,'admin','R28J75uY8l',112.00,2378.00,31.00,'tyrboflai@ya.ru','','','18.05.13','0.00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vp_set`
--

DROP TABLE IF EXISTS `vp_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vp_set` (
  `vp_nomer` varchar(4) NOT NULL DEFAULT '',
  `vp_bank` decimal(20,2) NOT NULL DEFAULT '0.00',
  `vp_proc` decimal(3,0) NOT NULL DEFAULT '100',
  `vp_shanswin1` varchar(255) NOT NULL DEFAULT '3|3|3|3|3|',
  `vp_shanswin2` varchar(255) NOT NULL DEFAULT '3|3|3|3|3|',
  `vp_shansdouble` varchar(255) NOT NULL DEFAULT '2',
  PRIMARY KEY (`vp_nomer`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vp_set`
--

LOCK TABLES `vp_set` WRITE;
/*!40000 ALTER TABLE `vp_set` DISABLE KEYS */;
INSERT INTO `vp_set` VALUES ('01',0.00,95,'3|3|3|3|3|','3|3|3|3|3|','2');
/*!40000 ALTER TABLE `vp_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zakaz`
--

DROP TABLE IF EXISTS `zakaz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zakaz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(12) DEFAULT NULL,
  `cash` varchar(30) DEFAULT NULL,
  `rekvizit` varchar(20) DEFAULT NULL,
  `sumout` varchar(10) DEFAULT NULL,
  `flag` char(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zakaz`
--

LOCK TABLES `zakaz` WRITE;
/*!40000 ALTER TABLE `zakaz` DISABLE KEYS */;
/*!40000 ALTER TABLE `zakaz` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-08-31 20:20:19
